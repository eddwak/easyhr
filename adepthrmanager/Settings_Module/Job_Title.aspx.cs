﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Job_Title : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayJobTitles();//load job titles
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddJobTitleControls();
            ModalPopupExtenderAddJobTitle.Show();
            return;
        }
        //clear add job title controls
        private void ClearAddJobTitleControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddJobTitle);
            HiddenFieldJobTitleID.Value = "";
            lnkBtnSaveJobTitle.Visible = true;
            lnkBtnUpdateJobTitle.Enabled = false;
            lnkBtnUpdateJobTitle.Visible = true;
            lnkBtnClearJobTitle.Visible = true;
            lbAddJobTitleHeader.Text = "Add New Job Title";
            ImageAddJobTitleHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add job titles controls
        private string ValidateAddJobTitleControls()
        {
            if (txtJobTitle.Text.Trim() == "")
            {
                txtJobTitle.Focus();
                return "Enter job title!";
            }
            else return "";
        }
        //save new job title
        private void SaveNewJobTitle()
        {
            ModalPopupExtenderAddJobTitle.Show();
            try
            {
                string _error = ValidateAddJobTitleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new job title details
                    const int _listingMasterID = 7000;//Master ID 7000 for job title
                    const string _listingType = "job title";
                    string _jobTitle = "";
                    _jobTitle = txtJobTitle.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _jobTitle, ""))//check if the job titleexists
                    {
                        lbError.Text = "Save failed, the job title you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new job title
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _jobTitle, null, null, null, null, null, null, null,null, HiddenFieldJobTitleID);
                        DisplayJobTitles();
                        lbError.Text = "";
                        lbInfo.Text = "Job title has been successfully saved.";
                        lnkBtnUpdateJobTitle.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateJobTitle()
        {
            ModalPopupExtenderAddJobTitle.Show();
            try
            {
                string _error = ValidateAddJobTitleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _jobTitleID = Convert.ToInt32(HiddenFieldJobTitleID.Value);
                    const int _listingMasterID = 7000;//Master ID 7000 for job title
                    const string _listingType = "job title";
                    string _jobTitle = "";
                    _jobTitle = txtJobTitle.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _jobTitleID, _jobTitle, ""))//check if the department exists
                    {
                        lbError.Text = "Save failed, the job title you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _jobTitleID, _listingType, _jobTitle, null, null, null, null, null, null, null,null);
                        DisplayJobTitles();
                        lbError.Text = "";
                        lbInfo.Text = "Job title has been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveJobTitle_Click(object sender, EventArgs e)
        {
            SaveNewJobTitle();
            return;
        }

        protected void lnkBtnUpdateJobTitle_Click(object sender, EventArgs e)
        {
            UpdateJobTitle();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearJobTitle_Click(object sender, EventArgs e)
        {
            ClearAddJobTitleControls();
            ModalPopupExtenderAddJobTitle.Show();
            return;
        }
        //load job titles
        private void DisplayJobTitles()
        {
            try
            {
                object _display;
                _display = db.JobTitles_Views.Select(p => p).OrderBy(p => p.JobTitle);
                gvJobTitles.DataSourceID = null;
                gvJobTitles.DataSource = _display;
                Session["gvJobTitlesData"] = _display;
                gvJobTitles.DataBind();
                return;
            }
            catch { }
        }
        protected void gvJobTitles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvJobTitles.PageIndex = e.NewPageIndex;
            Session["gvJobTitlesPageIndex"] = e.NewPageIndex;//get page index
            gvJobTitles.DataSource = (object)Session["gvJobTitlesData"];
            gvJobTitles.DataBind();
            return;
        }
        protected void gvJobTitles_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvJobTitlesPageIndex"] == null)//check if page index is empty
                {
                    gvJobTitles.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvJobTitlesPageIndex"]);
                    gvJobTitles.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        // populate the edit job title controls
        private void LoadEditJobTitleControls(int _jobTitleID)
        {
            ClearAddJobTitleControls();
            JobTitles_View getJobTitle = db.JobTitles_Views.Single(p => p.JobTitleID == _jobTitleID);
            HiddenFieldJobTitleID.Value = _jobTitleID.ToString();
            txtJobTitle.Text = getJobTitle.JobTitle;
            lnkBtnSaveJobTitle.Visible = false;
            lnkBtnClearJobTitle.Visible = true;
            lnkBtnUpdateJobTitle.Enabled = true;
            lbAddJobTitleHeader.Text = "Edit Job Title";
            ImageAddJobTitleHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddJobTitle.Show();
            return;
        }
        //check if a job title is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobTitles))//check if a job title is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a job title to edit !", this, PanelJobTitles);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobTitles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one job title record. Select only one job title to edit at a time !", this, PanelJobTitles);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobTitles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobTitleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit job title
                            LoadEditJobTitleControls(Convert.ToInt32(_HFJobTitleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading job title for edit! " + ex.Message.ToString(), this, PanelJobTitles);
            }
        }
        //check if a job title is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobTitles))//check if a job title is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a job title to view!", this, PanelJobTitles);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobTitles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one job title record!. Select only one job title record to view the details at a time !", this, PanelJobTitles);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobTitles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobTitleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view job title
                            LoadViewJobTitleControls(Convert.ToInt32(_HFJobTitleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the job title for view purpose ! " + ex.Message.ToString(), this, PanelJobTitles);
                return;
            }
        }
        // populate the view job title
        private void LoadViewJobTitleControls(int _jobTitleID)
        {
            JobTitles_View getJobTitle = db.JobTitles_Views.Single(p => p.JobTitleID == _jobTitleID);
            HiddenFieldJobTitleID.Value = _jobTitleID.ToString();
            txtJobTitle.Text = getJobTitle.JobTitle;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddJobTitle);
            lnkBtnSaveJobTitle.Visible = false;
            lnkBtnClearJobTitle.Visible = false;
            lnkBtnUpdateJobTitle.Visible = false;
            lbAddJobTitleHeader.Text = "View Job Title";
            ImageAddJobTitleHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddJobTitle.Show();
        }
        //load delete job titledetails
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobTitles))//check if there is any job title record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a job title to delete !", this, PanelJobTitles);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvJobTitles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected job title?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected job titles?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelJobTitles);
                return;
            }
        }
        //delete job title record
        private void DeleteJobTitle()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvJobTitles.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFJobTitleID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _jobTitleID = Convert.ToInt32(_HFJobTitleID.Value);

                        ListingItem deleteJobTitle = db.ListingItems.Single(p => p.ListingItemIID == _jobTitleID);
                        //check if there is any brach used

                        //do not delete --- to be done

                        //delete the job title if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteJobTitle);
                        db.SubmitChanges();
                    }
                }
                //refresh job titles after the delete is done
                DisplayJobTitles();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected job title has been deleted.", this, PanelJobTitles);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected job titles have been deleted.", this, PanelJobTitles);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelJobTitles);
                return;
            }
        }
        //delete the selected job title(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteJobTitle();//delete selected jib title(s)
            return;
        }
    }
}