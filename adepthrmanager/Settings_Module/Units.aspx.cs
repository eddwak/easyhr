﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Units : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //_hrClass.GetListingItems(ddlDepartmentName, 3000, "Department");//display departments(3000) available
                //_hrClass.GetStaffNames(ddlHeadOfUnit, "Unit Head");//populate head of unit dropdown
                _hrClass.GetListingItems(ddlHeadOfUnit, 7000, "Unit Head");//populate head of unit dropdown
                DisplayUnits();//load units
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddUnitControls();
            ModalPopupExtenderAddUnit.Show();
            return;
        }
        //clear add unit controls
        private void ClearAddUnitControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddUnit);
            HiddenFieldUnitID.Value = "";
            lnkBtnSaveUnit.Visible = true;
            lnkBtnUpdateUnit.Enabled = false;
            lnkBtnUpdateUnit.Visible = true;
            lnkBtnClearUnit.Visible = true;
            lbAddUnitHeader.Text = "Add New Unit";
            ImageAddUnitHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbUnitsError.Text = "";
            lbUnitsInfo.Text = "";
        }
        //validate add unit controls
        private string ValidateAddUnitControls()
        {
            //if (ddlDepartmentName.SelectedIndex == 0)
            //{
            //    ddlDepartmentName.Focus();
            //    return "Select department name!";
            //}
            //if (txtUnitCode.Text.Trim() == "")
            //{
            //    txtUnitCode.Focus();
            //    return "Enter unit code!";
            //}
            //else 
            if (txtUnitName.Text.Trim() == "")
            {
                txtUnitName.Focus();
                return "Enter unit name!";
            }
            //else if (ddlHeadOfUnit.SelectedIndex == 0)
            //{
            //    ddlHeadOfUnit.Focus();
            //    return "Select head of the unit!";
            //}
            else return "";
        }
        //save new unit
        private void SaveNewUnit()
        {
            ModalPopupExtenderAddUnit.Show();
            try
            {
                string _error = ValidateAddUnitControls();
                if (_error != "")
                {
                    lbUnitsError.Text = _error;
                    return;
                }
                else
                {
                    //save the new unit details
                    const int _listingMasterID = 4000;//Master ID 4000 for unit
                    const string _listingType = "unit";
                    //int _departmentID = 0;
                    string _unitCode = "", _unitName = "", _unitDescription = "";
                    int _unitHeadID = 7001;//default job title
                    //_departmentID = Convert.ToInt32(ddlDepartmentName.SelectedValue);
                    _unitCode = txtUnitCode.Text.Trim();
                    _unitName = txtUnitName.Text.Trim();
                    if (ddlHeadOfUnit.SelectedIndex != 0)
                        _unitHeadID = Convert.ToInt32(ddlHeadOfUnit.SelectedValue);
                    _unitDescription = txtUnitDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _unitName, _unitCode))//check if the unit exists
                    {
                        lbUnitsError.Text = "Save failed, the unit you are trying to add already exists!";
                        lbUnitsInfo.Text = "";
                    }
                    else
                    {
                        //save the new unit
                        //_hrClass.AddNewListingItem(_listingMasterID, _listingType, _unitName, _unitCode, null, null, null, null, _departmentID, _unitDescription, _unitHeadID, HiddenFieldUnitID);
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _unitName, _unitCode, null, _unitHeadID, null, null, null, _unitDescription, null, HiddenFieldUnitID);
                        DisplayUnits();
                        lbUnitsError.Text = "";
                        lbUnitsInfo.Text = "Unit details have been successfully saved.";
                        lnkBtnUpdateUnit.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbUnitsError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING AN UNIT
        private void UpdateUnit()
        {
            ModalPopupExtenderAddUnit.Show();
            try
            {
                string _error = ValidateAddUnitControls();
                if (_error != "")
                {
                    lbUnitsError.Text = _error;
                    return;
                }
                else
                {
                    int _unitID = Convert.ToInt32(HiddenFieldUnitID.Value);
                    const int _listingMasterID = 4000;//Master ID 4000 for units
                    const string _listingType = "unit";
                    //int _departmentID = 0;
                    string _unitCode = "", _unitName = "", _unitDescription = "";
                    int _unitHeadID = 7001;//default job title
                    //_departmentID = Convert.ToInt32(ddlDepartmentName.SelectedValue);
                    _unitCode = txtUnitCode.Text.Trim();
                    _unitName = txtUnitName.Text.Trim();
                    if (ddlHeadOfUnit.SelectedIndex != 0)
                        _unitHeadID = Convert.ToInt32(ddlHeadOfUnit.SelectedValue);
                    _unitDescription = txtUnitDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _unitID, _unitName, _unitCode))//check if the unit exists
                    {
                        lbUnitsError.Text = "Save failed, the unit you are trying to update with already exists!";
                        lbUnitsInfo.Text = "";
                    }
                    else
                    {
                        //_hrClass.UpdateListingItem(_listingMasterID, _unitID, _listingType, _unitName, _unitCode, null, null, null, null, _departmentID, _unitDescription, _unitHeadID);
                        _hrClass.UpdateListingItem(_listingMasterID, _unitID, _listingType, _unitName, _unitCode, null, _unitHeadID, null, null, null, _unitDescription, null);
                        DisplayUnits();
                        lbUnitsError.Text = "";
                        lbUnitsInfo.Text = "Unit details have been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbUnitsError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveUnit_Click(object sender, EventArgs e)
        {
            SaveNewUnit();
            return;
        }

        protected void lnkBtnUpdateUnit_Click(object sender, EventArgs e)
        {
            UpdateUnit();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearUnit_Click(object sender, EventArgs e)
        {
            ClearAddUnitControls();
            ModalPopupExtenderAddUnit.Show();
            return;
        }
        //display units
        private void DisplayUnits()
        {
            try
            {
                object _display;
                _display = db.Units_Views.Select(p => p).OrderBy(p => p.UnitCode);
                gvUnits.DataSourceID = null;
                gvUnits.DataSource = _display;
                Session["gvUnitsData"] = _display;
                gvUnits.DataBind();
                return;
            }
            catch { }
        }
        protected void gvUnits_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvUnits.PageIndex = e.NewPageIndex;
            Session["gvUnitsPageIndex"] = e.NewPageIndex;//get page index
            gvUnits.DataSource = (object)Session["gvUnitsData"];
            gvUnits.DataBind();
            return;
        }
        protected void gvUnits_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvUnitsPageIndex"] == null)//check if page index is empty
                {
                    gvUnits.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvUnitsPageIndex"]);
                    gvUnits.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        // populate the edit controls
        private void LoadEditUnitControls(int _unitID)
        {
            ClearAddUnitControls();
            Units_View getUnit = db.Units_Views.Single(p => p.UnitID == _unitID);
            HiddenFieldUnitID.Value = _unitID.ToString();
            //ddlDepartmentName.SelectedValue = getUnit.DepartmentID.ToString();
            txtUnitCode.Text = getUnit.UnitCode;
            txtUnitName.Text = getUnit.UnitName;
            ddlHeadOfUnit.SelectedValue = getUnit.UnitHeadID.ToString();
            txtUnitDescription.Text = getUnit.UnitDescription;

            lnkBtnSaveUnit.Visible = false;
            lnkBtnClearUnit.Visible = true;
            lnkBtnUpdateUnit.Enabled = true;
            lbAddUnitHeader.Text = "Edit Unit Details";
            ImageAddUnitHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbUnitsError.Text = "";
            lbUnitsInfo.Text = "";
            ModalPopupExtenderAddUnit.Show();
            return;
        }
        //check if an unit is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUnits))//check if a unit is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select unit to edit !", this, PanelUnits);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUnits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one unit!. Select only one unit to edit at a time!", this, PanelUnits);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUnits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUnitID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit unit details
                            LoadEditUnitControls(Convert.ToInt32(_HFUnitID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the selected unit details for edit! " + ex.Message.ToString(), this, PanelUnits);
            }
        }
        //check if a unit is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUnits))//check if a units is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select unit to view!", this, PanelUnits);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUnits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one unit!. Select only one unit to view the details at a time !", this, PanelUnits);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUnits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUnitID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view unit details
                            LoadViewUnitControls(Convert.ToInt32(_HFUnitID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the selected unit details for view purpose ! " + ex.Message.ToString(), this, PanelUnits);
                return;
            }
        }
        // populate the view unit controls
        private void LoadViewUnitControls(int _unitID)
        {
            Units_View getUnit = db.Units_Views.Single(p => p.UnitID == _unitID);
            HiddenFieldUnitID.Value = _unitID.ToString();
            //ddlDepartmentName.SelectedValue = getUnit.DepartmentID.ToString();
            txtUnitCode.Text = getUnit.UnitCode;
            txtUnitName.Text = getUnit.UnitName;
            ddlHeadOfUnit.SelectedValue = getUnit.UnitHeadID.ToString();
            txtUnitDescription.Text = getUnit.UnitDescription;

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddUnit);
            lnkBtnSaveUnit.Visible = false;
            lnkBtnClearUnit.Visible = false;
            lnkBtnUpdateUnit.Visible = false;
            lbAddUnitHeader.Text = "View Unit Details";
            ImageAddUnitHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddUnit.Show();
        }
        //load delete unit details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUnits))//check if there is any unit record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select unit to delete !", this, PanelUnits);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvUnits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected unit?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected units?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelUnits);
                return;
            }
        }
        //delete unitt record
        private void DeleteUnit()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvUnits.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFUnitID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _unitID = Convert.ToInt32(_HFUnitID.Value);

                        ListingItem deleteUnit = db.ListingItems.Single(p => p.ListingItemIID == _unitID);
                        //check if there is any unit used

                        //do not delete --- to be done

                        //delete the unit if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteUnit);
                        db.SubmitChanges();
                    }
                }
                //refresh unit after the delete is done
                DisplayUnits();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected unit has been deleted.", this, PanelUnits);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected units have been deleted.", this, PanelUnits);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelUnits);
                return;
            }
        }
        //delete the selected unit
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteUnit();//delete selected unit
            return;
        }
        ////load units
        //private void DisplayUnits()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("UnitID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("UnitCode", typeof(string)));
        //    dt.Columns.Add(new DataColumn("UnitName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("DepartmentName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("HeadOfUnit", typeof(string)));
        //    DataRow dr = dt.NewRow();
        //    dr[0] = 1;
        //    dr[1] = "U0001";
        //    dr[2] = "Unit One";
        //    dr[3] = "I.T";
        //    dr[4] = "Edwin Wambua";
        //    dt.Rows.Add(dr);

        //    DataRow dr2 = dt.NewRow();
        //    dr2[0] = 2;
        //    dr2[1] = "U0002";
        //    dr2[2] = "Unit Two";
        //    dr2[3] = "Marketing and Sales";
        //    dr2[4] = "Dickson Kiriinya";
        //    dt.Rows.Add(dr2);

        //    DataRow dr3 = dt.NewRow();
        //    dr3[0] = 3;
        //    dr3[1] = "U0003";
        //    dr3[2] = "Unit Three";
        //    dr3[3] = "Accounts";
        //    dr3[4] = "Austin Mwangi";
        //    dt.Rows.Add(dr3);

        //    DataRow dr4 = dt.NewRow();
        //    dr4[0] = 3;
        //    dr4[1] = "U0004";
        //    dr4[2] = "Unit Four";
        //    dr4[3] = "Supplies and Logistics";
        //    dr4[4] = "Ngari Richard";
        //    dt.Rows.Add(dr4);

        //    gvUnits.DataSource = dt;
        //    gvUnits.DataBind();
        //}
    }
}