﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class General_Settings : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerMailClass _hrMail = new HRManagerMailClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEmailSettingsDetails();//load existing email setting details
            }
        }
        //validate save email seetings controls'
        private string ValidateEmailSettingsControls()
        {
            if (txtSMTPServerAddress.Text.Trim() == "")
            {
                txtSMTPServerAddress.Focus();
                return "Enter the SMTP server address";
            }
            else if (txtPortNumber.Text.Trim() != "" && _hrClass.isNumberValid(txtPortNumber.Text.Trim()) == false)
            {
                txtPortNumber.Focus();
                return "Invalid port number entered!";
            }
            else if (txtEmailAddress.Text.Trim() == "")
            {
                txtEmailAddress.Focus();
                return "Enter email address!";
            }
            else if (_hrClass.IsValidEmail(txtEmailAddress.Text.Trim()) == false)
            {
                txtEmailAddress.Focus();
                return "Invalid email address entered!";
            }
            else if (txtEmailPassword.Text.Trim() == "")
            {
                txtEmailPassword.Focus();
                return "Enter email address password!";
            }
            else if (txtConfirmEmailPassword.Text.Trim() == "")
            {
                txtConfirmEmailPassword.Focus();
                return "Confirm the email address password!";
            }
            else if (txtEmailPassword.Text.Trim() != txtConfirmEmailPassword.Text.Trim())
            {
                txtConfirmEmailPassword.Focus();
                return "Confirmation password entered does not match with the email password entered!";
            }
            else return "";
        }
        //method for saving the email setting details
        private void SaveEmailSettingsDetails()
        {
            try
            {
                string _error = ValidateEmailSettingsControls();//check for errors
                string _smtpServer = txtSMTPServerAddress.Text.Trim(), _emailAdress = txtEmailAddress.Text.Trim();
                string _emailPassword = txtEmailPassword.Text.Trim(), _confirmEmailPassword = txtConfirmEmailPassword.Text.Trim();
                txtEmailPassword.Attributes.Add("value", _emailPassword);//maintain the password entered in case of a postback
                txtConfirmEmailPassword.Attributes.Add("value", _confirmEmailPassword);//maintain the password
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelGeneralSettings);
                    return;
                }
                else
                {
                    //save the details
                    int? _portNumber = null;
                    if (txtPortNumber.Text.Trim() != "")
                        _portNumber = Convert.ToInt32(txtPortNumber.Text.Trim());
                    //check of its new record
                    if (HiddenFieldEmailSettingID.Value.ToString() == "")
                    {
                        //save new details
                        EmailSetting newEmailSetting = new EmailSetting();
                        newEmailSetting.SMTPServerAddress = _smtpServer;
                        newEmailSetting.PortNumber = _portNumber;
                        newEmailSetting.EmailAddress = _emailAdress;
                        newEmailSetting.EmailAddressPassword = _emailPassword;
                        db.EmailSettings.InsertOnSubmit(newEmailSetting);
                        db.SubmitChanges();
                        HiddenFieldEmailSettingID.Value = newEmailSetting.EmailSettingID.ToString();
                        _hrClass.LoadHRManagerMessageBox(2, "Email settings details have been successfuly saved!", this, PanelGeneralSettings);
                        return;
                    }
                    else
                    {
                        //update existing records
                        int _emailSettingID = Convert.ToInt32(HiddenFieldEmailSettingID.Value);
                        EmailSetting updateEmailSetting = db.EmailSettings.Single(p => p.EmailSettingID == _emailSettingID);
                        updateEmailSetting.SMTPServerAddress = _smtpServer;
                        updateEmailSetting.PortNumber = _portNumber;
                        updateEmailSetting.EmailAddress = _emailAdress;
                        updateEmailSetting.EmailAddressPassword = _emailPassword;
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "Email settings details have been successfuly updated!", this, PanelGeneralSettings);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . " + ex.Message.ToString() + ". Please try again!", this, PanelGeneralSettings);
                return;
            }
        }
        //save email settings
        protected void lnkBtnSaveEmailSettings_Click(object sender, EventArgs e)
        {
            SaveEmailSettingsDetails(); return;
        }
        //load email setting details
        private void LoadEmailSettingsDetails()
        {
            try
            {
                if (db.EmailSettings.Any())
                {
                    //get the the first record
                    EmailSetting getEmailSetting = db.EmailSettings.FirstOrDefault();
                    HiddenFieldEmailSettingID.Value = getEmailSetting.EmailSettingID.ToString();
                    txtSMTPServerAddress.Text = getEmailSetting.SMTPServerAddress;
                    txtPortNumber.Text = getEmailSetting.PortNumber.ToString();
                    txtEmailAddress.Text = getEmailSetting.EmailAddress;
                    txtEmailPassword.Attributes.Add("value", getEmailSetting.EmailAddressPassword);
                    txtConfirmEmailPassword.Attributes.Add("value", getEmailSetting.EmailAddressPassword);
                    return;
                }

            }
            catch { }
        }
    }
}