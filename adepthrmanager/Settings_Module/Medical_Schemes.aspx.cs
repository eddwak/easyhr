﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Medical_Schemes : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlMedicalBenefit, 16000, "Medical Benefit");//display medical benefit(16000) available
            }
            //add confirm medical scheme event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            //add confirm delete medical detail record
            ucConfirmDeleteMedicalSchemeDetail.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnConfirmDeleteMedicalSchemeDetail_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddMedicalSchemeControls();
            ModalPopupExtenderAddMedicalScheme.Show();
            return;
        }
        //clear add medical scheme controls
        private void ClearAddMedicalSchemeControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddMedicalScheme);
            HiddenFieldMedicalSchemeID.Value = "";
            lnkBtnSaveMedicalScheme.Visible = true;
            lnkBtnUpdateMedicalScheme.Enabled = false;
            lnkBtnUpdateMedicalScheme.Visible = true;
            lnkBtnClearMedicalScheme.Visible = true;
            lbAddMedicalSchemeHeader.Text = "Add New Insurance Scheme";
            ImageAddMedicalSchemeHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            gvMedicalSchemeDetails.DataSourceID = null;
            gvMedicalSchemeDetails.DataBind();
        }
        //validate add add medical scheme details
        private string ValidateAddMedicalSchemeEntryControls()
        {
            if (txtMedicalSchemeName.Text.Trim() == "")
            {
                txtMedicalSchemeName.Text.Trim();
                return "Enter the insurance scheme name!";
            } 
            else if (ddlMedicalBenefit.SelectedIndex == 0)
            {
                ddlMedicalBenefit.Focus();
                return "Select medical benefit!";
            }
            else if (RadioButtonListAplicableTo.SelectedIndex == -1)
            {
                RadioButtonListAplicableTo.Focus();
                return "Select to who is the medical benefit selected appliacble to!";
            }
            else if (txtAmount.Text.Trim() == "")
            {
                txtAmount.Focus();
                return "Enter amount for the medical benefit selected!";
            }
            else if (_hrClass.IsCurrencyValid(txtAmount.Text.Trim()) == false)
            {
                txtAmount.Focus();
                return "Enter valid numeric value for the amount!";
            }
            else return "";
        }
        //method for saving a medical scheme 
        private void SaveMedicalScheme()
        {
            try
            {
                string _error = ValidateAddMedicalSchemeEntryControls();//check if there are any errors]
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelMedicalSchemes);
                    return;
                }

                else
                {
                    if (HiddenFieldMedicalSchemeID.Value == "")
                    {
                        //check if the medical scheme being entered already exists
                        string _medicalSchemeName = txtMedicalSchemeName.Text.Trim();
                        if (db.MedSchemes.Any(p => p.medscheme_vName.ToLower() == _medicalSchemeName.ToLower()))
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Insurance scheme entered already exists!", this, PanelMedicalSchemes);
                            txtMedicalSchemeName.Focus();
                            return;
                        }
                        else
                        {
                            //create new medical scheme
                            MedScheme newMedicalScheme = new MedScheme();
                            newMedicalScheme.medscheme_vName = _medicalSchemeName;
                            newMedicalScheme.medscheme_vDescription = txtDescription.Text.Trim();
                            db.MedSchemes.InsertOnSubmit(newMedicalScheme);
                            db.SubmitChanges();
                            HiddenFieldMedicalSchemeID.Value = newMedicalScheme.medscheme_siMedSchemeID.ToString();
                            //display medical schemes
                            DisplayMedicalSchemes();
                        }
                    }
                    short _medicalSchemeID = Convert.ToInt16(HiddenFieldMedicalSchemeID.Value);//get medical scheme id
                    int _medicalBenefitID = Convert.ToInt32(ddlMedicalBenefit.SelectedValue);
                    //check if the medical benefit selected exists in the medical scheme details
                    if (db.MedSchmDetails.Any(p => p.medschmdetails_siMedSchemeID == _medicalSchemeID && p.medschmdetails_iMedBenefitID == _medicalBenefitID && p.medschmdetails_vApplicableTo == RadioButtonListAplicableTo.SelectedValue))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Medical benefit selected is already assigned to " + RadioButtonListAplicableTo.Text.ToLower() + " in insurance scheme details. Only an update can be made to the medical benefit!", this, PanelMedicalSchemes);
                        ddlMedicalBenefit.Focus();
                        return;
                    }
                    else
                    {
                        //add new medical scheme detail
                        MedSchmDetail newMedicalSchemeDetail = new MedSchmDetail();
                        newMedicalSchemeDetail.medschmdetails_siMedSchemeID = _medicalSchemeID;
                        newMedicalSchemeDetail.medschmdetails_iMedBenefitID = _medicalBenefitID;
                        newMedicalSchemeDetail.medschmdetails_vApplicableTo = RadioButtonListAplicableTo.SelectedValue;
                        newMedicalSchemeDetail.medschmdetails_mAmount = Convert.ToDecimal(txtAmount.Text.Trim());
                        db.MedSchmDetails.InsertOnSubmit(newMedicalSchemeDetail);
                        db.SubmitChanges();
                        HiddenFieldMedicalSchemeDetailsID.Value = newMedicalSchemeDetail.medschmdetails_siMedSchmDetailsID.ToString();
                        lnkBtnUpdateMedicalScheme.Enabled = true;
                    }

                    //display medical scheme details
                    DisplayMedicalSchemeDetails();
                    _hrClass.LoadHRManagerMessageBox(2, "Insurance scheme details have been saved!", this, PanelMedicalSchemes);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString(), this, PanelMedicalSchemes);
                return;
            }
        }
        //method for updating a medical scheme
        private void UpdateMedicalScheme(short _medicalSchemeID)
        {
            try
            {
                string _error = ValidateAddMedicalSchemeEntryControls();//check if there are any errors]
                if (txtMedicalSchemeName.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter insurance scheme name!", this, PanelMedicalSchemes);
                    txtMedicalSchemeName.Focus();
                    return;
                }
                else if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelMedicalSchemes);
                    return;
                }
                else
                {
                    //update medical scheme
                    if (db.MedSchemes.Any(p => p.medscheme_vName.ToLower() == txtMedicalSchemeName.Text.Trim().ToLower() && p.medscheme_siMedSchemeID != _medicalSchemeID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Update Failed. Insurance scheme name you are trying to update with already exists!", this, PanelMedicalSchemes);
                        txtMedicalSchemeName.Focus();
                        return;
                    }
                    else
                    {
                        //update medical scheme
                        MedScheme updateMedicalScheme = db.MedSchemes.Single(p => p.medscheme_siMedSchemeID == _medicalSchemeID);
                        updateMedicalScheme.medscheme_vName = txtMedicalSchemeName.Text.Trim();
                        updateMedicalScheme.medscheme_vDescription = txtDescription.Text.Trim();
                        db.SubmitChanges();
                    }

                    //update medical scheme detail if hiddenfieldmedicalschemedetails has a value
                    if (HiddenFieldMedicalSchemeDetailsID.Value != "")
                    {
                        short _medicalSchemeDetailsID = Convert.ToInt16(HiddenFieldMedicalSchemeDetailsID.Value);
                        int _medicalBenefitID = Convert.ToInt32(ddlMedicalBenefit.SelectedValue);
                        if (db.MedSchmDetails.Any(p => p.medschmdetails_siMedSchemeID == _medicalSchemeID && p.medschmdetails_iMedBenefitID == _medicalBenefitID && p.medschmdetails_vApplicableTo == RadioButtonListAplicableTo.SelectedValue && p.medschmdetails_siMedSchmDetailsID != _medicalSchemeDetailsID))
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Update Failed!. The medical benefit selected is already allocated to " + RadioButtonListAplicableTo.SelectedValue + ". Only an update can be done to the medical benefit detail!", this, PanelMedicalSchemes);
                            ddlMedicalBenefit.Focus();
                            return;
                        }
                        else
                        {
                            MedSchmDetail updateMedicalSchemeDetail = db.MedSchmDetails.Single(p => p.medschmdetails_siMedSchmDetailsID == _medicalSchemeDetailsID);
                            updateMedicalSchemeDetail.medschmdetails_vApplicableTo = RadioButtonListAplicableTo.SelectedValue;
                            updateMedicalSchemeDetail.medschmdetails_mAmount = Convert.ToDecimal(txtAmount.Text.Trim());
                            db.SubmitChanges();
                            //display medical scheme details
                            DisplayMedicalSchemeDetails();
                        }
                    }
                    //display medical schemes
                    DisplayMedicalSchemes();

                    _hrClass.LoadHRManagerMessageBox(2, "Insurance scheme details have been updated!", this, PanelMedicalSchemes);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelMedicalSchemes);
                return;
            }
        }
        //save
        protected void lnkBtnSaveMedicalScheme_Click(object sender, EventArgs e)
        {
            SaveMedicalScheme();
            ModalPopupExtenderAddMedicalScheme.Show();
        }
        //update
        protected void lnkBtnUpdateMedicalScheme_Click(object sender, EventArgs e)
        {
            UpdateMedicalScheme(Convert.ToInt16(HiddenFieldMedicalSchemeID.Value.ToString()));
            ModalPopupExtenderAddMedicalScheme.Show();
        }
        //clear contols
        protected void lnkBtnClearMedicalScheme_Click(object sender, EventArgs e)
        {
            ClearAddMedicalSchemeControls();
            ModalPopupExtenderAddMedicalScheme.Show();
        }
        //display medical schemes
        public void DisplayMedicalSchemes()
        {
            try
            {
                object _display;
                _display = db.MedSchemes.OrderBy(p => p.medscheme_vName);
                gvMedicalSchemes.DataSourceID = null;
                gvMedicalSchemes.DataSource = _display;
                gvMedicalSchemes.DataBind();
                return;
            }
            catch { }
        }
        //display medical scheme details
        public void DisplayMedicalSchemeDetails()
        {
            try
            {
                //  HiddenFieldMedicalSchemeID.Value = "1";
                int medicalSchemeID = Convert.ToInt32(HiddenFieldMedicalSchemeID.Value);//get medical scheme id
                object _display;
                _display = db.MedicalSchemeDetail_Views.Where(p => p.medschmdetails_siMedSchemeID == medicalSchemeID).OrderBy(p => p.medschmdetails_vMedBenefitName);
                gvMedicalSchemeDetails.DataSourceID = null;
                gvMedicalSchemeDetails.DataSource = _display;
                gvMedicalSchemeDetails.DataBind();
                return;
            }
            catch { }
        }
        //check if any medical scheme is selected before editing
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvMedicalSchemes))//check if any medical scheme is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no insurance scheme selected. Select the insurance scheme that you want to edit!", this, PanelMedicalSchemes);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvMedicalSchemes.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one insurance scheme. Select only one insurance scheme to edit at a time!", this, PanelMedicalSchemes);
                        return;
                    }
                    foreach (GridViewRow _grv in gvMedicalSchemes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFMedicalSchemeD = (HiddenField)_grv.FindControl("HiddenField1");
                            LoadEditMedicalSchemeControls(Convert.ToInt16(_HFMedicalSchemeD.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        //load values for the medical scheme selected
        private void LoadEditMedicalSchemeControls(short _medicalSchemeID)
        {
            //get medical scheme
            MedScheme getMedicalScheme = db.MedSchemes.Single(p => p.medscheme_siMedSchemeID == _medicalSchemeID);
            HiddenFieldMedicalSchemeID.Value = _medicalSchemeID.ToString();
            txtMedicalSchemeName.Text = getMedicalScheme.medscheme_vName;
            txtDescription.Text = getMedicalScheme.medscheme_vDescription;
            //display medical scheme details
            DisplayMedicalSchemeDetails();
            lnkBtnUpdateMedicalScheme.Enabled = true;
            lbAddMedicalSchemeHeader.Text = "Edit Insurance Scheme";
            ImageAddMedicalSchemeHeader.ImageUrl = "~/Images/icons/Medium/Modify.png";
            ModalPopupExtenderAddMedicalScheme.Show();
            txtMedicalSchemeName.Focus();
            return;
        }
        //load medical scheme details for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvMedicalSchemeDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ModalPopupExtenderAddMedicalScheme.Show();

            try
            {
                if (e.CommandName.CompareTo("EditMedicalSchemeDetail") == 0 || e.CommandName.CompareTo("DeleteMedicalSchemeDetail") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFMedicalSchemeDetailID = (HiddenField)gvMedicalSchemeDetails.Rows[ID].FindControl("HFID");

                    if (e.CommandName.CompareTo("EditMedicalSchemeDetail") == 0)//check if we are editing
                    {
                        LoadMedicalSchemeDetailsForEdit(Convert.ToInt32(_HFMedicalSchemeDetailID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteMedicalSchemeDetail") == 0)//check if we are deleting
                    {
                        Session["DeleteMedicalSchemeDetailID"] = _HFMedicalSchemeDetailID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmDeleteMedicalSchemeDetail, "Are you sure you what to delete the insurance scheme detail?");
                        return;
                    }
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured", this, PanelMedicalSchemes);
                return;
            }
        }
        //load medical scheme details to edit
        private void LoadMedicalSchemeDetailsForEdit(int _medicalSchemeDetailID)
        {
            //get medical scheme detail
            MedSchmDetail getMedicalSchemeDetail = db.MedSchmDetails.Single(p => p.medschmdetails_siMedSchmDetailsID == _medicalSchemeDetailID);
            HiddenFieldMedicalSchemeDetailsID.Value = _medicalSchemeDetailID.ToString();
            ddlMedicalBenefit.SelectedValue = getMedicalSchemeDetail.medschmdetails_iMedBenefitID.ToString();
            RadioButtonListAplicableTo.SelectedValue = getMedicalSchemeDetail.medschmdetails_vApplicableTo;
            txtAmount.Text = _hrClass.ConvertToCurrencyValue((decimal)getMedicalSchemeDetail.medschmdetails_mAmount);
            lnkBtnUpdateMedicalScheme.Enabled = true;
        }
        //click eb=vent for deleteing a medical scheme
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvMedicalSchemes))//check if there is any medical scheme that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no insurance scheme selected. Select insurance scheme that you want to delete!", this, PanelMedicalSchemes);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvMedicalSchemes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, "Are you sure you want to delete the insurance scheme selected?");
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelMedicalSchemes);
                return;
            }

        }
        //method for deleting medical scheme details
        private void DeleteMedicalSchemeDetails()
        {
            ModalPopupExtenderAddMedicalScheme.Show();
            try
            {
                //get medical scheme detail id to delete
                short medicalSchemeDetailID = Convert.ToInt16(Session["DeleteMedicalSchemeDetailID"]);
                MedSchmDetail deleteMedicalSchemeDetail = db.MedSchmDetails.Single(p => p.medschmdetails_siMedSchmDetailsID == medicalSchemeDetailID);
                db.MedSchmDetails.DeleteOnSubmit(deleteMedicalSchemeDetail);
                db.SubmitChanges();
                //display medical scheme details
                DisplayMedicalSchemeDetails();
                _hrClass.LoadHRManagerMessageBox(2, "Insurance scheme detail has been deleted!", this, PanelMedicalSchemes);

                //clear add medical scheme details if details on the add controls are the details of the deleted medical scheme detail
                if (HiddenFieldMedicalSchemeDetailsID.Value != "" && Convert.ToInt16(HiddenFieldMedicalSchemeDetailsID.Value) == medicalSchemeDetailID)
                {
                    HiddenFieldMedicalSchemeDetailsID.Value = "";
                    ddlMedicalBenefit.SelectedIndex = 0;
                    txtAmount.Text = "";
                    lnkBtnUpdateMedicalScheme.Enabled = false;
                }
                Session["DeleteMedicalSchemeDetailID"] = null;
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelMedicalSchemes);
                return;
            }
        }
        //delete medical scheme detail click event
        protected void lnkBtnConfirmDeleteMedicalSchemeDetail_Click(object sender, EventArgs e)
        {
            DeleteMedicalSchemeDetails(); return;
        }
        //method for deleting medical scheme
        private void DeleteMedicalScheme()
        {
            try
            {
                int x = 0;
                foreach (GridViewRow _grv in gvMedicalSchemes.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) ++x;

                    if (_cb.Checked)
                    {
                        HiddenField _HFMedicalSchemeID = (HiddenField)_grv.FindControl("HiddenField1");
                        int MedicalSchemeID = Convert.ToInt32(_HFMedicalSchemeID.Value);
                        // check there are any empoyees assigned the Medical Scheme selected
                        //if (db.EmpTerms.Any(p => p.empterms_siMedicalSchemeID == MedicalSchemeID))
                        //{
                        //    //get employees asigned to the medical scheme
                        //    string _employees = "";
                        //    int countRecord = 1;
                        //    foreach (EmpTerm getEmployees in db.EmpTerms.Where(p => p.empterms_siMedicalSchemeID == MedicalSchemeID))
                        //    {
                        //        _employees += countRecord + "." + db.StaffMasters.Single(p => p.StaffID == getEmployees.empterms_uiStaffID).StaffName + ",<br>";
                        //    }
                        //    _hrClass.LoadHRManagerMessageBox(1, "Delete Failed!. The medical scheme selected has :<br>" + _employees + " who fall under the medical scheme.", this, PanelMedicalSchemes);
                        //    return;
                        //}
                        //delete medical scheme details first
                        foreach (MedSchmDetail getMedicalSchemedetail in db.MedSchmDetails.Where(p => p.medschmdetails_siMedSchemeID == MedicalSchemeID))
                        {
                            MedSchmDetail deleteMedicalSchemeDetail = db.MedSchmDetails.Single(p => p.medschmdetails_siMedSchmDetailsID == getMedicalSchemedetail.medschmdetails_siMedSchmDetailsID);
                            db.MedSchmDetails.DeleteOnSubmit(deleteMedicalSchemeDetail);
                            db.SubmitChanges();
                        }
                        //delete medical scheme]
                        MedScheme deleteMedicalScheme = db.MedSchemes.Single(p => p.medscheme_siMedSchemeID == MedicalSchemeID);
                        db.MedSchemes.DeleteOnSubmit(deleteMedicalScheme);
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "The insurance scheme selected has been deleted.", this, PanelMedicalSchemes);
                        //refresh medical schemes after delete is done
                    }
                    DisplayMedicalSchemes();
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelMedicalSchemes);
                return;
            }

        }

        //delete the selected medical scheme(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteMedicalScheme();//delete selected medical scheme(s)
            return;
        }
    }
}