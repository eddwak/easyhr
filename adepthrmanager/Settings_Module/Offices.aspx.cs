﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Offices : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlLocation, 12000, "Location");//display location
                _hrClass.GetListingItems(ddlHeadOfOffice, 7000, "Head of the Office");//display head of the office
                // _hrClass.GetStaffNames(ddlHeadOfOffice, "Head of the Office");//populate head of the office dropdown
                DisplayCompanyOffices();//load company offices
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddOfficeControls();
            ModalPopupExtenderAddOffice.Show();
            return;
        }
        //clear add office controls
        private void ClearAddOfficeControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddOffice);
            HiddenFieldOfficeID.Value = "";
            lnkBtnSaveOffice.Visible = true;
            lnkBtnUpdateOffice.Enabled = false;
            lnkBtnUpdateOffice.Visible = true;
            lnkBtnClearOffice.Visible = true;
            lbAddOfficeHeader.Text = "Add New Office";
            ImageAddOfficeHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add office controls
        private string ValidateAddOfficeControls()
        {
            if (txtOfficeCode.Text.Trim() == "")
            {
                txtOfficeCode.Focus();
                return "Enter office code !";
            }
            else if (txtOfficeName.Text.Trim() == "")
            {
                txtOfficeName.Focus();
                return "Enter office name !";
            }
            else if (ddlLocation.SelectedIndex == 0)
            {
                ddlLocation.Focus();
                return "Select office location !";
            }
            //else if (ddlHeadOfOffice.SelectedIndex == 0)
            //{
            //    ddlHeadOfOffice.Focus();
            //    return "Select head of office!";
            //}
            else return "";
        }
        //save new office
        private void SaveNewOffice()
        {
            ModalPopupExtenderAddOffice.Show();
            try
            {
                string _error = ValidateAddOfficeControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new office details
                    const int _listingMasterID = 2000;//Master ID 2000 for office
                    const string _listingType = "office";
                    string _officeCode = "", _officeName = "";
                    int _locationID = 0;
                    int _hobID = 7001;
                    _officeCode = txtOfficeCode.Text.Trim();
                    _officeName = txtOfficeName.Text.Trim();
                    _locationID = Convert.ToInt32(ddlLocation.SelectedValue);
                    if (ddlHeadOfOffice.SelectedIndex != 0)
                        _hobID = Convert.ToInt32(ddlHeadOfOffice.SelectedValue);
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _officeName, _officeCode))//check if the code exists
                    {
                        lbError.Text = "Save failed, the office you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new office
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _officeName, _officeCode, null, _locationID, _hobID, null, null, null, null, HiddenFieldOfficeID);
                        DisplayCompanyOffices();
                        lbError.Text = "";
                        lbInfo.Text = "Office details have been successfully saved.";
                        lnkBtnUpdateOffice.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateCompanyOffice()
        {
            ModalPopupExtenderAddOffice.Show();
            try
            {
                string _error = ValidateAddOfficeControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _officeID = Convert.ToInt32(HiddenFieldOfficeID.Value);
                    const int _listingMasterID = 2000;//Master ID 2000 for office
                    const string _listingType = "branch";
                    string _officeCode = "", _officeName = "";
                    int _locationID = 0;
                    int _hobID = 7001;
                    _officeCode = txtOfficeCode.Text.Trim();
                    _officeName = txtOfficeName.Text.Trim();
                    _locationID = Convert.ToInt32(ddlLocation.SelectedValue);
                    if (ddlHeadOfOffice.SelectedIndex != 0)
                        _hobID = Convert.ToInt32(ddlHeadOfOffice.SelectedValue);
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _officeID, _officeName, _officeCode))//check if the office code exists
                    {
                        lbError.Text = "Save failed, the office you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _officeID, _listingType, _officeName, _officeCode, null, _locationID, _hobID, null, null, null, null);
                        DisplayCompanyOffices();
                        lbError.Text = "";
                        lbInfo.Text = "Office details have been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveOffice_Click(object sender, EventArgs e)
        {
            SaveNewOffice();
            return;
        }

        protected void lnkBtnUpdateOffice_Click(object sender, EventArgs e)
        {
            UpdateCompanyOffice();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearOffice_Click(object sender, EventArgs e)
        {
            ClearAddOfficeControls();
            ModalPopupExtenderAddOffice.Show();
            return;
        }
        //load offices
        private void DisplayCompanyOffices()
        {
            object _display;
            _display = db.Branches_Views.Select(p => p).OrderBy(p => p.BranchCode);
            gvOffices.DataSourceID = null;
            gvOffices.DataSource = _display;
            gvOffices.DataBind();
            return;
        }
        // populate the edit office controls
        private void LoadEditOfficeControls(int _officeID)
        {
            ClearAddOfficeControls();
            Branches_View getOffice = db.Branches_Views.Single(p => p.BranchID == _officeID);
            HiddenFieldOfficeID.Value = _officeID.ToString();
            txtOfficeCode.Text = getOffice.BranchCode;
            txtOfficeName.Text = getOffice.BranchName;
            ddlLocation.SelectedValue = getOffice.LocationID.ToString();
            ddlHeadOfOffice.SelectedValue = getOffice.HOBID.ToString();

            lnkBtnSaveOffice.Visible = false;
            lnkBtnClearOffice.Visible = true;
            lnkBtnUpdateOffice.Enabled = true;
            lbAddOfficeHeader.Text = "Edit Office Details";
            ImageAddOfficeHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddOffice.Show();
            return;
        }
        //check if a user is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvOffices))//check if a office is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a office to edit !", this, PanelOffices);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvOffices.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one office !. Select only one office to edit at a time !", this, PanelOffices);
                        return;
                    }
                    foreach (GridViewRow _grv in gvOffices.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFOfficeID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit office details
                            LoadEditOfficeControls(Convert.ToInt32(_HFOfficeID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading office details for edit! " + ex.Message.ToString(), this, PanelOffices);
            }
        }
        //check if a office is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvOffices))//check if a offices is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a office to view!", this, PanelOffices);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvOffices.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one office !. Select only one office to view the details at a time !", this, PanelOffices);
                        return;
                    }
                    foreach (GridViewRow _grv in gvOffices.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFOfficeID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view office details
                            LoadViewOfficeControls(Convert.ToInt32(_HFOfficeID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the office details for view purpose ! " + ex.Message.ToString(), this, PanelOffices);
                return;
            }
        }
        // populate the view office controls
        private void LoadViewOfficeControls(int _officeID)
        {
            Branches_View getOffice = db.Branches_Views.Single(p => p.BranchID == _officeID);
            HiddenFieldOfficeID.Value = _officeID.ToString();
            txtOfficeCode.Text = getOffice.BranchCode;
            txtOfficeName.Text = getOffice.BranchName;
            ddlLocation.SelectedValue = getOffice.LocationID.ToString();
            ddlHeadOfOffice.SelectedValue = getOffice.HOBID.ToString();

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddOffice);
            lnkBtnSaveOffice.Visible = false;
            lnkBtnClearOffice.Visible = false;
            lnkBtnUpdateOffice.Visible = false;
            lbAddOfficeHeader.Text = "View Office Details";
            ImageAddOfficeHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddOffice.Show();
        }
        //load delete office details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvOffices))//check if there is any office record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a office to delete !", this, PanelOffices);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvOffices.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected office?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected offices?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelOffices);
                return;
            }
        }
        //delete office record
        private void DeleteOffice()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvOffices.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFOfficeID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _officeID = Convert.ToInt32(_HFOfficeID.Value);

                        ListingItem deleteOffice = db.ListingItems.Single(p => p.ListingItemIID == _officeID);
                        //check if there is any brach used

                        //do not delete --- to be done

                        //delete the office if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteOffice);
                        db.SubmitChanges();
                    }
                }
                //refresh offices after the delete is done
                DisplayCompanyOffices();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected company office has been deleted.", this, PanelOffices);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected offices have been deleted.", this, PanelOffices);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelOffices);
                return;
            }
        }
        //delete the selected offices
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteOffice();//delete selected offices
            return;
        }
    }
}