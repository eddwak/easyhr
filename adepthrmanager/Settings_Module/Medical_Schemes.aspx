﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Medical_Schemes.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Medical_Schemes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelMedicalSchemes" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMedicalSchemes" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new medical scheme"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit medical scheme details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" ToolTip="View medical scheme details" CausesValidation="false"
                        runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete medical scheme" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvMedicalSchemes" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" DataSourceID="LinqDataSourceMedicalSchemes" AllowSorting="True"
                        PageSize="23">
                        <Columns>
                            <asp:TemplateField HeaderText="S.No" ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="4px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("medscheme_siMedSchemeID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="medscheme_vName" HeaderText="Insurance Scheme" SortExpression="medscheme_vName" />
                            <asp:BoundField DataField="medscheme_vDescription" HeaderText="Description" SortExpression="medscheme_vDescription" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                        <AlternatingRowStyle CssClass="alt" />
                    </asp:GridView>
                    <asp:LinqDataSource ID="LinqDataSourceMedicalSchemes" runat="server" ContextTypeName="AdeptHRManager.HRManagerClasses.HRManagerDataContext"
                        TableName="MedSchemes" OrderBy="medscheme_vName">
                    </asp:LinqDataSource>
                </div>
                <asp:Panel ID="PanelAddMedicalScheme" Style="display: none; width: 1100px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddMedicalScheme" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddMedicalSchemeHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddMedicalSchemeHeader" runat="server" Text="Add New Insurance Scheme"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddMedicalScheme" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend> Scheme</legend>
                            <table>
                                <tr valign="top">
                                    <td>
                                        Insurance Scheme Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMedicalSchemeName" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescription" Width="350px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <hr />
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td colspan="4">
                                        <asp:Panel ID="PanelMedicalSchemeDetails" runat="server">
                                            <table>
                                                <tr style="white-space: nowrap;">
                                                    <td>
                                                        Medical Benefit:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlMedicalBenefit" Width="135px" runat="server">
                                                            <asp:ListItem Value="0">Select Medical Benefit</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        Applicable To:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonListAplicableTo" RepeatDirection="Horizontal"
                                                            runat="server">
                                                            <asp:ListItem Value="Self">Self</asp:ListItem>
                                                            <asp:ListItem Value="Spouse">Spouse</asp:ListItem>
                                                            <asp:ListItem Value="Child">Child</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                        Amount:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAmount" Width="70px" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <div class="linkBtn">
                                                            <asp:LinkButton ID="lnkBtnSaveMedicalScheme" OnClick="lnkBtnSaveMedicalScheme_Click"
                                                                ToolTip="Save medical benefit" runat="server">
                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                                Save</asp:LinkButton>
                                                            <asp:LinkButton ID="lnkBtnUpdateMedicalScheme" OnClick="lnkBtnUpdateMedicalScheme_Click"
                                                                CausesValidation="false" ToolTip="Update Branch" Enabled="false" runat="server">
                                                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                                    ImageAlign="AbsMiddle" />
                                                                Update</asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <fieldset>
                                            <legend>Insurance Scheme Details </legend>
                                            <div id="divMedicalSchemeDetails" style="height: 220px; overflow: auto;">
                                                <asp:GridView ID="gvMedicalSchemeDetails" OnRowCommand="gvMedicalSchemeDetails_RowCommand"
                                                    runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                                    AllowSorting="True" PageSize="20">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No" ItemStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1+"." %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="medschmdetails_vMedBenefitName" HeaderText="Medical Benefit"
                                                            SortExpression="medschmdetails_vMedBenefitName" />
                                                        <asp:BoundField DataField="medschmdetails_vApplicableTo" HeaderText="Applicable To"
                                                            SortExpression="medschmdetails_vApplicableTo" />
                                                        <asp:BoundField DataField="medschmdetails_mAmount" HeaderText="Amount" SortExpression="medschmdetails_mAmount"
                                                            DataFormatString="{0:N2}" />
                                                        <asp:ButtonField HeaderText="Edit" CommandName="EditMedicalSchemeDetail" ItemStyle-CssClass="gridButtons"
                                                            ItemStyle-Width="4px" Text="Edit" />
                                                        <asp:ButtonField HeaderText="Del" CommandName="DeleteMedicalSchemeDetail" ItemStyle-CssClass="gridButtons"
                                                            ItemStyle-Width="4px" Text="Delete" />
                                                        <asp:TemplateField ItemStyle-Width="1px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("medschmdetails_siMedSchmDetailsID") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="PagerStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldMedicalSchemeID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldMedicalSchemeDetailsID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddMedicalSchemePopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnClearMedicalScheme" OnClick="lnkBtnClearMedicalScheme_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddMedicalScheme" TargetControlID="HiddenFieldAddMedicalSchemePopup"
                    PopupControlID="PanelAddMedicalScheme" RepositionMode="RepositionOnWindowResizeAndScroll"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <%--    <asp:DragPanelExtender ID="DragPanelExtenderAddMedicalScheme" TargetControlID="PanelAddMedicalScheme"
                    runat="server">
                </asp:DragPanelExtender>--%>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeleteMedicalSchemeDetail" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
