﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Teams.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Teams" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelTeams" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelTeams" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new team"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit team details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View team details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete team" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvTeams" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No team record available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("TEamID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="TeamCode" HeaderText="Team Code" />
                            <asp:BoundField DataField="TeamName" HeaderText="Team Name" />
                            <asp:BoundField DataField="HeadOfTeam" HeaderText="Head Of Team" />
                            <asp:BoundField DataField="TeamDescription" HeaderText="Description" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddTeam" Style="display: none; width: 750px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddTeam" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddTeamHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddTeamHeader" runat="server" Text="Add New Team"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddTeam" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Team Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Team Code:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamCode" Width="200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Team Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamName" Width="200px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Head Of Team:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlHeadOfTeam" Width="200px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Team Description:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamDescription" Width="200px" Height="50px" TextMode="MultiLine"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbTeamError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbTeamInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldTeamID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddTeamPopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <div class="linkBtn">
                                <asp:LinkButton ID="lnkBtnSaveTeam" OnClick="lnkBtnSaveUnit_Click" ToolTip="Save team"
                                    runat="server">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                    Save</asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnUpdateTeam" OnClick="lnkBtnUpdateTeam_Click" CausesValidation="false"
                                    ToolTip="Update team" Enabled="false" runat="server">
                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                        ImageAlign="AbsMiddle" />
                                    Update</asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnClearTeam" OnClick="lnkBtnClearTeam_Click" CausesValidation="false"
                                    ToolTip="Clear" runat="server">
                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                        ImageAlign="AbsMiddle" />
                                    Clear</asp:LinkButton>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Team Members</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbTeamMember" runat="server" Text="Team Member:"></asp:Label>
                                        <asp:Label ID="lbTeamMemberMsg" CssClass="errorMessage" runat="server" Text="*"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlTeamMember" runat="server">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Label ID="lbTeamMemberError" runat="server" CssClass="errorMessage" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <div class="linkBtn">
                                            <asp:HiddenField ID="HiddenFieldTeamMemberID" runat="server" />
                                            <asp:LinkButton ID="lnkBtnAddTeamMember" OnClick="lnkBtnAddTeamMember_Click" ToolTip="Add Team Member"
                                                runat="server">
                                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Create.png"
                                                    ImageAlign="AbsMiddle" />
                                                Add Team Member</asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="gvTeamMembers" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                            CssClass="GridViewStyle" OnRowCommand="gvTeamMembers_RowCommand" AllowSorting="True"
                                            PageSize="15" EmptyDataText="No team members available!">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1+"." %>
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("teammember_iTeamMemberID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="4px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TeamMember" HeaderText="Team Member" />
                                                <asp:BoundField DataField="DepartmentName" HeaderText="Department Name" />
                                                <asp:ButtonField HeaderText="Delete" CommandName="DeleteTeamMember" ItemStyle-Width="4px"
                                                    Text="Delete" />
                                            </Columns>
                                            <FooterStyle CssClass="PagerStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddTeam" RepositionMode="None" X="200"
                    Y="5" TargetControlID="HiddenFieldAddTeamPopup" PopupControlID="PanelAddTeam"
                    CancelControlID="ImageButtonCloseAddTeam" BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddTeam" TargetControlID="PanelAddTeam"
                    DragHandleID="PanelDragAddTeam" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucDeleteTeamMember" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
