﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Departments : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlHeadOfDepartment, 7000, "Head of Department");//populate head of department dropdown
                //_hrClass.GetStaffNames(ddlHeadOfDepartment, "Head of Department");//populate head of department dropdown
                DisplayDepartments();//load departments

            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddDepartmentControls();
            ModalPopupExtenderAddDepartment.Show();
            return;
        }
        //clear add department controls
        private void ClearAddDepartmentControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddDepartment);
            HiddenFieldDepartmentID.Value = "";
            lnkBtnSaveDepartment.Visible = true;
            lnkBtnUpdateDepartment.Enabled = false;
            lnkBtnUpdateDepartment.Visible = true;
            lnkBtnClearDepartment.Visible = true;
            lbAddDepartmentHeader.Text = "Add New Department";
            ImageAddDepartmentHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add department controls
        private string ValidateAddDepartmentControls()
        {
            if (txtDepartmentCode.Text.Trim() == "")
            {
                txtDepartmentCode.Focus();
                return "Enter department code!";
            }
            else if (txtDepartmentName.Text.Trim() == "")
            {
                txtDepartmentName.Focus();
                return "Enter department name!";
            }
            //else if (ddlHeadOfDepartment.SelectedIndex == 0)
            //{
            //    ddlHeadOfDepartment.Focus();
            //    return "Select head of department!";
            //}
            else return "";
        }
        //save new department
        private void SaveNewDepartment()
        {
            ModalPopupExtenderAddDepartment.Show();
            try
            {
                string _error = ValidateAddDepartmentControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new department details
                    const int _listingMasterID = 3000;//Master ID 3000 for department
                    const string _listingType = "department";
                    string _departmentCode = "", _departmentName = "";
                    int _hodID = 7001;//default title
                    _departmentCode = txtDepartmentCode.Text.Trim();
                    _departmentName = txtDepartmentName.Text.Trim();
                    if (ddlHeadOfDepartment.SelectedIndex != 0)
                        _hodID = Convert.ToInt32(ddlHeadOfDepartment.SelectedValue);
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _departmentName, _departmentCode))//check if the department exists
                    {
                        lbError.Text = "Save failed, the department you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new branch
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _departmentName, _departmentCode, null, _hodID, null, null, null, null, null, HiddenFieldDepartmentID);
                        DisplayDepartments();
                        lbError.Text = "";
                        lbInfo.Text = "Department details have been successfully saved.";
                        lnkBtnUpdateDepartment.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING A DEPARTMENT
        private void UpdateDepartment()
        {
            ModalPopupExtenderAddDepartment.Show();
            try
            {
                string _error = ValidateAddDepartmentControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _departmentID = Convert.ToInt32(HiddenFieldDepartmentID.Value);
                    const int _listingMasterID = 3000;//Master ID 3000 for branch
                    const string _listingType = "department";
                    string _departmentCode = "", _departmentName = "";
                    int _hodID = 7001;//default job title
                    _departmentCode = txtDepartmentCode.Text.Trim();
                    _departmentName = txtDepartmentName.Text.Trim();
                    if (ddlHeadOfDepartment.SelectedIndex != 0)
                        _hodID = Convert.ToInt32(ddlHeadOfDepartment.SelectedValue);
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _departmentID, _departmentName, _departmentCode))//check if the department exists
                    {
                        lbError.Text = "Save failed, the department you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _departmentID, _listingType, _departmentName, _departmentCode, null, _hodID, null, null, null, null, null);
                        DisplayDepartments();
                        lbError.Text = "";
                        lbInfo.Text = "Department details have been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveDepartment_Click(object sender, EventArgs e)
        {
            SaveNewDepartment();
            return;
        }

        protected void lnkBtnUpdateDepartment_Click(object sender, EventArgs e)
        {
            UpdateDepartment();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearDepartment_Click(object sender, EventArgs e)
        {
            ClearAddDepartmentControls();
            ModalPopupExtenderAddDepartment.Show();
            return;
        }
        //load departments
        private void DisplayDepartments()
        {
            try
            {
                object _display;
                _display = db.Departments_Views.Select(p => p).OrderBy(p => p.DepartmentCode);
                gvDepartments.DataSourceID = null;
                gvDepartments.DataSource = _display;
                gvDepartments.DataBind();
                return;
            }
            catch { }
        }
        // populate the edit  controls
        private void LoadEditDepartmentControls(int _departmentID)
        {
            ClearAddDepartmentControls();
            Departments_View getDepartment = db.Departments_Views.Single(p => p.DepartmentID == _departmentID);
            HiddenFieldDepartmentID.Value = _departmentID.ToString();
            txtDepartmentCode.Text = getDepartment.DepartmentCode;
            txtDepartmentName.Text = getDepartment.DepartmentName;
            ddlHeadOfDepartment.SelectedValue = getDepartment.HODID.ToString();

            lnkBtnSaveDepartment.Visible = false;
            lnkBtnClearDepartment.Visible = true;
            lnkBtnUpdateDepartment.Enabled = true;
            lbAddDepartmentHeader.Text = "Edit Department Details";
            ImageAddDepartmentHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddDepartment.Show();
            return;
        }
        //check if a department is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvDepartments))//check if a department is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a department to edit !", this, PanelDepartments);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvDepartments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one department !. Select only one department to edit at a time !", this, PanelDepartments);
                        return;
                    }
                    foreach (GridViewRow _grv in gvDepartments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFDepartmentID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit department details
                            LoadEditDepartmentControls(Convert.ToInt32(_HFDepartmentID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading department details for edit! " + ex.Message.ToString(), this, PanelDepartments);
            }
        }
        //check if a department is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvDepartments))//check if a departments is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a department to view!", this, PanelDepartments);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvDepartments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one department !. Select only one department to view the details at a time !", this, PanelDepartments);
                        return;
                    }
                    foreach (GridViewRow _grv in gvDepartments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFDepartmentID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view department details
                            LoadViewDepartmentControls(Convert.ToInt32(_HFDepartmentID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the department details for view purpose ! " + ex.Message.ToString(), this, PanelDepartments);
                return;
            }
        }
        // populate the view department controls
        private void LoadViewDepartmentControls(int _departmentID)
        {
            Departments_View getDepartment = db.Departments_Views.Single(p => p.DepartmentID == _departmentID);
            HiddenFieldDepartmentID.Value = _departmentID.ToString();
            txtDepartmentCode.Text = getDepartment.DepartmentCode;
            txtDepartmentName.Text = getDepartment.DepartmentName;
            ddlHeadOfDepartment.SelectedValue = getDepartment.HODID.ToString();

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddDepartment);
            lnkBtnSaveDepartment.Visible = false;
            lnkBtnClearDepartment.Visible = false;
            lnkBtnUpdateDepartment.Visible = false;
            lbAddDepartmentHeader.Text = "View Department Details";
            ImageAddDepartmentHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddDepartment.Show();
        }
        //load delete department details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvDepartments))//check if there is any department record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a department to delete !", this, PanelDepartments);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvDepartments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected department?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected departments?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelDepartments);
                return;
            }
        }
        //delete department record
        private void DeleteDepartment()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvDepartments.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFDepartmentID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _departmentID = Convert.ToInt32(_HFDepartmentID.Value);

                        ListingItem deleteDepartment = db.ListingItems.Single(p => p.ListingItemIID == _departmentID);
                        //check if there is any  department used

                        //do not delete --- to be done

                        //delete the department if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteDepartment);
                        db.SubmitChanges();
                    }
                }
                //refresh deparment after the delete is done
                DisplayDepartments();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected department has been deleted.", this, PanelDepartments);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected departments have been deleted.", this, PanelDepartments);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelDepartments);
                return;
            }
        }
        //delete the selected department
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteDepartment();//delete selected department
            return;
        }
    }
}