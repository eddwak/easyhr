﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Locations.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Locations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelLocations" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLocations" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new job title"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit job title details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete job title" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvLocations" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No locations available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("LocationID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Location" HeaderText="Location" />
                            <asp:BoundField DataField="Country" HeaderText="Country" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddLocation" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddLocation" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddLocationHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddLocationHeader" runat="server" Text="Add New Location"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddLocation" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Job Location Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Country:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountryLocation" runat="server">
                                            <asp:ListItem Value="0">-Select Country-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Location<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtJobLocationName" runat="server"></asp:TextBox><span class="errorMessage"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image4" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label6" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label7" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label8" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldLocationID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddLocationPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveLocation" OnClick="lnkBtnSaveLocation_Click" ToolTip="Save"
                                runat="server">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateLocation" OnClick="lnkBtnUpdateLocation_Click" CausesValidation="false"
                                ToolTip="Update" Enabled="false" runat="server">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearLocation" OnClick="lnkBtnClearLocation_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddLocation" RepositionMode="None"
                    X="270" Y="80" TargetControlID="HiddenFieldAddLocationPopup" PopupControlID="PanelAddLocation"
                    CancelControlID="ImageButtonCloseAddLocation" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddLocation" TargetControlID="PanelAddLocation"
                    DragHandleID="PanelDragAddLocation" runat="server"></asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
