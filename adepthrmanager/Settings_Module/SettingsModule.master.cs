﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class SettingsModule : System.Web.UI.MasterPage
    {
        HRManagerClass _hrManagerClass = new HRManagerClass();
        protected void Page_Init(object sender, EventArgs e)
        {
            _hrManagerClass.ChangeModuleMainMenuActiveLinkCSS(this.Page, PanelSettingsModuleMainMenu);//change the css of the active menu
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}