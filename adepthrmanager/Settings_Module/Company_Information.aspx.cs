﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Company_Information : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlCountry, 1000, "Country");//display countries available
                GetCompanyInformation();//get company information
            }
        }
        //save company information details
        private void SaveCompanyInfo()
        {
            try
            {
                if (txtCompanyName.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter company name !", this, PanelCompanyInformation);
                    return;
                }
                else if (txtHQAddress.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter headquaters address !", this, PanelCompanyInformation);
                    return;
                }
                else if (txtLocation.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter location !", this, PanelCompanyInformation);
                    return;
                }
                else if (ddlCountry.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select country !", this, PanelCompanyInformation);
                    return;
                }
                else
                {
                    //check if we are saving new details or updating the existing details
                    if (HiddenFieldCompanyID.Value.ToString() == "")
                    {
                        //save new details
                        CompanyInformation newCompanyInfo = new CompanyInformation();
                        newCompanyInfo.CompanyName = txtCompanyName.Text.Trim();
                        newCompanyInfo.HQAddress = txtHQAddress.Text.Trim();
                        newCompanyInfo.Location = txtLocation.Text.Trim();
                        newCompanyInfo.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                        db.CompanyInformations.InsertOnSubmit(newCompanyInfo);
                        db.SubmitChanges();
                        HiddenFieldCompanyID.Value = newCompanyInfo.CompanyID.ToString();
                    }
                    else
                    {
                        //update existing details
                        int _companyID = Convert.ToInt32(HiddenFieldCompanyID.Value);
                        CompanyInformation updateCompanyInfo = db.CompanyInformations.Single(p => p.CompanyID == _companyID);
                        updateCompanyInfo.CompanyName = txtCompanyName.Text.Trim();
                        updateCompanyInfo.HQAddress = txtHQAddress.Text.Trim();
                        updateCompanyInfo.Location = txtLocation.Text.Trim();
                        updateCompanyInfo.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                        db.SubmitChanges();
                    }
                    _hrClass.LoadHRManagerMessageBox(2, "Company information details have been successfully saved.", this, PanelCompanyInformation);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed . " + ex.Message.ToString() + ". Please try again!", this, PanelCompanyInformation); return;
            }
        }
        //get company informatio
        private void GetCompanyInformation()
        {
            try
            {
                CompanyInformation getCompanyInfo = db.CompanyInformations.Select(p => p).FirstOrDefault();
                HiddenFieldCompanyID.Value = getCompanyInfo.CompanyID.ToString();
                txtCompanyName.Text = getCompanyInfo.CompanyName;
                txtHQAddress.Text = getCompanyInfo.HQAddress;
                txtLocation.Text = getCompanyInfo.Location;
                ddlCountry.SelectedValue = getCompanyInfo.CountryID.ToString();
                return;
            }
            catch { }
        }
        protected void lnkBtnSave_Click(object sender, EventArgs e)
        {
            SaveCompanyInfo();
            return;
        }
    }
}