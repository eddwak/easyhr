﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Locations : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlCountryLocation, 1000, "Country");//display countries(1000) available
                DisplayLocations();//load locations
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //load and new location
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddJobLocationControls();
            ModalPopupExtenderAddLocation.Show();
            return;
        }
        //clear add job location controls
        private void ClearAddJobLocationControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddLocation);
            HiddenFieldLocationID.Value = "";
            lnkBtnSaveLocation.Visible = true;
            lnkBtnUpdateLocation.Enabled = false;
            lnkBtnUpdateLocation.Visible = true;
            lnkBtnClearLocation.Visible = true;
            lbAddLocationHeader.Text = "Add New Location";
            ImageAddLocationHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        }
        //validate add job location controls
        private string ValidateAddJobLocationControls()
        {
            if (ddlCountryLocation.SelectedIndex == 0)
                return "Select country!";
            else if (txtJobLocationName.Text.Trim() == "")
                return "Enter location name!";
            else return "";
        }
        //meathod for saving a job location
        private void SaveNewJobLocation()
        {
            ModalPopupExtenderAddLocation.Show();
            try
            {
                string _error = ValidateAddJobLocationControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLocations);
                    return;
                }
                else
                {
                    //save the new role details
                    const int _listingMasterID = 12000;//Master ID 12000 for locations
                    const string _listingType = "location";
                    int _countryID = Convert.ToInt32(ddlCountryLocation.SelectedValue);
                    string _locationName = txtJobLocationName.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _locationName))//check if the location name exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the location name you are trying to add already exists!", this, PanelLocations);
                        return;
                    }
                    else
                    {
                        //save the new location
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _locationName, null, null, null, null, null, _countryID, null, null, HiddenFieldLocationID);
                        DisplayLocations();
                        _hrClass.LoadHRManagerMessageBox(2, "New job location has been successfully saved.", this, PanelLocations);
                        lnkBtnUpdateLocation.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again !", this, PanelLocations);
                return;
            }
        }
        //METHOD FOR UPDATING A JOB LOCATION
        private void UpdateJobLocation()
        {
            ModalPopupExtenderAddLocation.Show();
            try
            {
                string _error = ValidateAddJobLocationControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLocations);
                    return;
                }
                else
                {
                    int _locationID = Convert.ToInt32(HiddenFieldLocationID.Value);
                    const int _listingMasterID = 12000;//Master ID 12000 for locations
                    const string _listingType = "location";
                    int _countryID = Convert.ToInt32(ddlCountryLocation.SelectedValue);
                    string _locationName = txtJobLocationName.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _locationID, _locationName))//check if the location already exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the location you are trying to update with already exists!", this, PanelLocations);
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _locationID, _listingType, _locationName, null, null, null, null, null, _countryID, null, null);
                        DisplayLocations();
                        _hrClass.LoadHRManagerMessageBox(2, "Location has been successfully updated", this, PanelLocations);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLocations);
                return;
            }
        }
        //save location
        protected void lnkBtnSaveLocation_Click(object sender, EventArgs e)
        {
            SaveNewJobLocation();
            return;
        }
        //update location
        protected void lnkBtnUpdateLocation_Click(object sender, EventArgs e)
        {
            UpdateJobLocation();
            return;
        }
        //clearlocation  entry controls
        protected void lnkBtnClearLocation_Click(object sender, EventArgs e)
        {
            ClearAddJobLocationControls();
            ModalPopupExtenderAddLocation.Show();
            return;
        }
        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLocations))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a location to edit!", this, PanelLocations);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one location to edit at a time!", this, PanelLocations);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFLocationID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit location
                            LoadEditLocationControls(Convert.ToInt32(_HFLocationID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelLocations);
            }
        }
        // populate the edit location controls
        private void LoadEditLocationControls(int _locationID)
        {
            ClearAddJobLocationControls();
            Locations_View getLocation = db.Locations_Views.Single(p => p.LocationID == _locationID);
            HiddenFieldLocationID.Value = _locationID.ToString();
            ddlCountryLocation.SelectedValue = getLocation.CountryID.ToString();
            txtJobLocationName.Text = getLocation.Location;
            lnkBtnSaveLocation.Visible = false;
            lnkBtnClearLocation.Visible = true;
            lnkBtnUpdateLocation.Enabled = true;
            lbAddLocationHeader.Text = "Edit Location";
            ImageAddLocationHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ModalPopupExtenderAddLocation.Show();
            return;
        }
        //display locations
        private void DisplayLocations()
        {
            try
            {
                object _display;
                _display = db.Locations_Views.Select(p => p).OrderBy(p => p.Country).ThenBy(p => p.Location);
                gvLocations.DataSourceID = null;
                gvLocations.DataSource = _display;
                gvLocations.DataBind();
                return;
            }
            catch { }
        }
        //load delete location details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLocations))//check if there is any  LOCATIONrecord that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a location to delete !", this, PanelLocations);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvLocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected location?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected locations?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLocations);
                return;
            }
        }
        //delete job title record
        private void DeleteLocation()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvLocations.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFLocationID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _LocationID = Convert.ToInt32(_HFLocationID.Value);

                        ListingItem deleteLocation = db.ListingItems.Single(p => p.ListingItemIID == _LocationID);
                        //check if there is any brach used

                        //do not delete --- to be done

                        //delete the job title if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteLocation);
                        db.SubmitChanges();
                    }
                }
                //refresh job titles after the delete is done
                DisplayLocations();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected location has been deleted.", this, PanelLocations);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected location have been deleted.", this, PanelLocations);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelLocations);
                return;
            }
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
          DeleteLocation();//delete selected records(s)
            return;
        }
    }
}