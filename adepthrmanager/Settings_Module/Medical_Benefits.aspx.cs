﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Medical_Benefits : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                DisplayMedicalBenefits();//load medical benefits
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddMedicalBenefitControls();
            ModalPopupExtenderAddMedicalBenefit.Show();
            return;
        }
        //clear add medical benefits controls
        private void ClearAddMedicalBenefitControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddMedicalBenefit);
            HiddenFieldMedicalBenefitID.Value = "";
            lnkBtnSaveMedicalBenefit.Visible = true;
            lnkBtnUpdateMedicalBenefit.Enabled = false;
            lnkBtnUpdateMedicalBenefit.Visible = true;
            lnkBtnClearMedicalBenefit.Visible = true;
            lbAddMedicalBenefitHeader.Text = "Add New Medical Benefit";
            ImageAddMedicalBenefitHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add medical benefits controls
        private string ValidateAddMedicalBenefitControls()
        {
            if (txtMedicalBenefit.Text.Trim() == "")
            {
                txtMedicalBenefit.Focus();
                return "Enter medical benefit!";
            }
            else return "";
        }
        //save new medical benefit
        private void SaveNewMedicalBenefit()
        {
            ModalPopupExtenderAddMedicalBenefit.Show();
            try
            {
                string _error = ValidateAddMedicalBenefitControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new medical benefit details
                    const int _listingMasterID = 16000;//Master ID 16000 for medical benefits
                    const string _listingType = "medical benefit";
                    string _MedicalBenefit = "";
                    _MedicalBenefit = txtMedicalBenefit.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _MedicalBenefit, ""))//check if the medical benefit exists
                    {
                        lbError.Text = "Save failed, the medical benefit you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new job title
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _MedicalBenefit, null, null, null, null, null, null, txtDescription.Text.Trim(), null, HiddenFieldMedicalBenefitID);
                        DisplayMedicalBenefits();
                        lbError.Text = "";
                        lbInfo.Text = "Job title has been successfully saved.";
                        lnkBtnUpdateMedicalBenefit.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateMedicalBenefit()
        {
            ModalPopupExtenderAddMedicalBenefit.Show();
            try
            {
                string _error = ValidateAddMedicalBenefitControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _MedicalBenefitID = Convert.ToInt32(HiddenFieldMedicalBenefitID.Value);
                    const int _listingMasterID = 16000;//Master ID 16000 for medical benefit
                    const string _listingType = "medical benefit";
                    string _MedicalBenefit = "";
                    _MedicalBenefit = txtMedicalBenefit.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _MedicalBenefitID, _MedicalBenefit, ""))//check if the department exists
                    {
                        lbError.Text = "Save failed, the medical benefit you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _MedicalBenefitID, _listingType, _MedicalBenefit, null, null, null, null, null, null, txtDescription.Text.Trim(), null);
                        DisplayMedicalBenefits();
                        lbError.Text = "";
                        lbInfo.Text = "Medical benefit has been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveMedicalBenefit_Click(object sender, EventArgs e)
        {
            SaveNewMedicalBenefit();
            return;
        }

        protected void lnkBtnUpdateMedicalBenefit_Click(object sender, EventArgs e)
        {
            UpdateMedicalBenefit();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearMedicalBenefit_Click(object sender, EventArgs e)
        {
            ClearAddMedicalBenefitControls();
            ModalPopupExtenderAddMedicalBenefit.Show();
            return;
        }
        //load medical benefits
        private void DisplayMedicalBenefits()
        {
            object _display;
            _display = db.MedicalBenefits_Views.Select(p => p).OrderBy(p => p.MedicalBenefit);
            gvMedicalBenefits.DataSourceID = null;
            gvMedicalBenefits.DataSource = _display;
            gvMedicalBenefits.DataBind();
            return;
        }
        // populate the edit job title controls
        private void LoadEditMedicalBenefitControls(int _MedicalBenefitID)
        {
            ClearAddMedicalBenefitControls();
            MedicalBenefits_View getMedicalBenefit = db.MedicalBenefits_Views.Single(p => p.MedicalBenefitID == _MedicalBenefitID);
            HiddenFieldMedicalBenefitID.Value = _MedicalBenefitID.ToString();
            txtMedicalBenefit.Text = getMedicalBenefit.MedicalBenefit;
            txtDescription.Text = getMedicalBenefit.MedicalBenefitDescription;
            lnkBtnSaveMedicalBenefit.Visible = false;
            lnkBtnClearMedicalBenefit.Visible = true;
            lnkBtnUpdateMedicalBenefit.Enabled = true;
            lbAddMedicalBenefitHeader.Text = "Edit Medical Benefit";
            ImageAddMedicalBenefitHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddMedicalBenefit.Show();
            return;
        }
        //check if a medical benefit is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvMedicalBenefits))//check if a medical benefit is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a medical benefit to edit !", this, PanelMedicalBenefits);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one medical benefit record. Select only one medical benefit to edit at a time !", this, PanelMedicalBenefits);
                        return;
                    }
                    foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFMedicalBenefitID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit medical benefit
                            LoadEditMedicalBenefitControls(Convert.ToInt32(_HFMedicalBenefitID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading medical benefit for edit! " + ex.Message.ToString(), this, PanelMedicalBenefits);
            }
        }
        //check if a medical benefit is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvMedicalBenefits))//check if a medical benefit is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a medical benefit to view!", this, PanelMedicalBenefits);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one medical benefit record!. Select only one medical benefit record to view the details at a time !", this, PanelMedicalBenefits);
                        return;
                    }
                    foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFMedicalBenefitID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view jmedical benefit
                            LoadViewMedicalBenefitControls(Convert.ToInt32(_HFMedicalBenefitID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the  medical benefit for view purpose ! " + ex.Message.ToString(), this, PanelMedicalBenefits);
                return;
            }
        }
        // populate the view medical benefit
        private void LoadViewMedicalBenefitControls(int _MedicalBenefitID)
        {
            MedicalBenefits_View getMedicalBenefit = db.MedicalBenefits_Views.Single(p => p.MedicalBenefitID == _MedicalBenefitID);
            HiddenFieldMedicalBenefitID.Value = _MedicalBenefitID.ToString();
            txtMedicalBenefit.Text = getMedicalBenefit.MedicalBenefit;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddMedicalBenefit);
            lnkBtnSaveMedicalBenefit.Visible = false;
            lnkBtnClearMedicalBenefit.Visible = false;
            lnkBtnUpdateMedicalBenefit.Visible = false;
            lbAddMedicalBenefitHeader.Text = "View Medical Benefit";
            ImageAddMedicalBenefitHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddMedicalBenefit.Show();
        }
        //load delete medical benefit 
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvMedicalBenefits))//check if there is any medical benefit record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a medical benefit to delete !", this, PanelMedicalBenefits);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected medical benefit?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected medical benefits?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelMedicalBenefits);
                return;
            }
        }
        //delete medical benefit record
        private void DeleteMedicalBenefit()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvMedicalBenefits.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFMedicalBenefitID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _MedicalBenefitID = Convert.ToInt32(_HFMedicalBenefitID.Value);

                        ListingItem deleteMedicalBenefit = db.ListingItems.Single(p => p.ListingItemIID == _MedicalBenefitID);
                        //check if there is any medical scheme that is associated with the medical benefit

                        //do not delete --- to be done

                        //delete the medical benefit if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteMedicalBenefit);
                        db.SubmitChanges();
                    }
                }
                //refresh medical benefits after the delete is done
                DisplayMedicalBenefits();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected medical benefit has been deleted.", this, PanelMedicalBenefits);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected medical benefits have been deleted.", this, PanelMedicalBenefits);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelMedicalBenefits);
                return;
            }
        }
        //delete the selected medical benefit(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteMedicalBenefit();//delete selected medical benefit(s)
            return;
        }
    }
}