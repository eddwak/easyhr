﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Teams : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetStaffNames(ddlHeadOfTeam, "Team Head");//populate head of team dropdown
                _hrClass.GetStaffNames(ddlTeamMember, "Team Member");//populate team member dropdown
                DisplayTeams();//load teams
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            //add delete team member event
            ucDeleteTeamMember.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnDeleteTeamMember_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddTeamControls();
            ModalPopupExtenderAddTeam.Show();
            return;
        }
        //clear add unit controls
        private void ClearAddTeamControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddTeam);
            HiddenFieldTeamID.Value = "";
            lnkBtnSaveTeam.Visible = true;
            lnkBtnUpdateTeam.Enabled = false;
            lnkBtnUpdateTeam.Visible = true;
            lnkBtnClearTeam.Visible = true;
            lnkBtnAddTeamMember.Visible = true; ;
            lbAddTeamHeader.Text = "Add New Team";
            ImageAddTeamHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbTeamError.Text = "";
            lbTeamInfo.Text = "";
            lbTeamMemberError.Text = "";
            DisplayTeamMembers(0);//clear gridview containg team members
            lbTeamMember.Visible = true;
            lbTeamMemberMsg.Visible = true;
            ddlTeamMember.Visible = true;
        }
        //validate add unit controls
        private string ValidateAddTeamControls()
        {

            if (txtTeamCode.Text.Trim() == "")
            {
                txtTeamCode.Focus();
                return "Enter team code!";
            }
            else if (txtTeamName.Text.Trim() == "")
            {
                txtTeamName.Focus();
                return "Enter team name!";
            }
            else if (ddlHeadOfTeam.SelectedIndex == 0)
            {
                ddlHeadOfTeam.Focus();
                return "Select head of the team!";
            }
            else return "";
        }
        //save new team
        private void SaveNewTeam()
        {
            ModalPopupExtenderAddTeam.Show();
            try
            {
                string _error = ValidateAddTeamControls();
                if (_error != "")
                {
                    lbTeamError.Text = _error;
                    return;
                }
                else
                {
                    //save the new team details
                    const int _listingMasterID = 5000;//Master ID 5000 for teams
                    const string _listingType = "team";
                    string _teamCode = "", _teamName = "", _teamDescription = "";
                    Guid _teamHeadID = Guid.Empty;
                    _teamCode = txtTeamCode.Text.Trim();
                    _teamName = txtTeamName.Text.Trim();
                    _teamHeadID = _hrClass.ReturnGuid(ddlHeadOfTeam.SelectedValue);
                    _teamDescription = txtTeamDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _teamName, _teamCode))//check if the team exists
                    {
                        lbTeamError.Text = "Save failed, the team you are trying to add already exists!";
                        lbTeamInfo.Text = "";
                    }
                    else
                    {
                        //save the new team
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _teamName, _teamCode, null, null, null, null, null, _teamDescription, _teamHeadID, HiddenFieldTeamID);
                        DisplayTeams();
                        lbTeamError.Text = "";
                        lbTeamInfo.Text = "Team details have been successfully saved.";
                        lnkBtnUpdateTeam.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbTeamError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING A Team
        private void UpdateTeam()
        {
            ModalPopupExtenderAddTeam.Show();
            try
            {
                string _error = ValidateAddTeamControls();
                if (_error != "")
                {
                    lbTeamError.Text = _error;
                    return;
                }
                else
                {
                    int _teamID = Convert.ToInt32(HiddenFieldTeamID.Value);
                    const int _listingMasterID = 5000;//Master ID 5000 for units
                    const string _listingType = "team";
                    string _teamCode = "", _teamName = "", _teamDescription = "";
                    Guid _teamHeadID = Guid.Empty;
                    _teamCode = txtTeamCode.Text.Trim();
                    _teamName = txtTeamName.Text.Trim();
                    _teamHeadID = _hrClass.ReturnGuid(ddlHeadOfTeam.SelectedValue);
                    _teamDescription = txtTeamDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _teamID, _teamName, _teamCode))//check if the team exists
                    {
                        lbTeamError.Text = "Save failed, the team you are trying to update with already exists!";
                        lbTeamInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _teamID, _listingType, _teamName, _teamCode, null, null, null, null, null, _teamDescription, _teamHeadID);
                        DisplayTeams();
                        lbTeamError.Text = "";
                        lbTeamInfo.Text = "Team details have been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbTeamError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        protected void lnkBtnSaveUnit_Click(object sender, EventArgs e)
        {
            SaveNewTeam();
            return;
        }

        protected void lnkBtnUpdateTeam_Click(object sender, EventArgs e)
        {
            UpdateTeam();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearTeam_Click(object sender, EventArgs e)
        {
            ClearAddTeamControls();
            ModalPopupExtenderAddTeam.Show();
            return;
        }
        //display teams
        private void DisplayTeams()
        {
            object _display;
            _display = db.Teams_Views.Select(p => p).OrderBy(p => p.TeamCode);
            gvTeams.DataSourceID = null;
            gvTeams.DataSource = _display;
            gvTeams.DataBind();
            return;
        }
        // populate the edit controls
        private void LoadEditTeamControls(int _teamID)
        {
            ClearAddTeamControls();
            Teams_View getTeam = db.Teams_Views.Single(p => p.TeamID == _teamID);
            HiddenFieldTeamID.Value = _teamID.ToString();
            txtTeamCode.Text = getTeam.TeamCode;
            txtTeamName.Text = getTeam.TeamName;
            ddlHeadOfTeam.SelectedValue = getTeam.TeamHeadID.ToString();
            txtTeamDescription.Text = getTeam.TeamDescription;

            DisplayTeamMembers(_teamID);//load team members
            lnkBtnSaveTeam.Visible = false;
            lnkBtnClearTeam.Visible = true;
            lnkBtnUpdateTeam.Enabled = true;
            lbAddTeamHeader.Text = "Edit Team Details";
            ImageAddTeamHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbTeamError.Text = "";
            lbTeamInfo.Text = "";
            ModalPopupExtenderAddTeam.Show();
            return;
        }
        //check if a team is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTeams))//check if a team is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select team to edit !", this, PanelTeams);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvTeams.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one team!. Select only one team to edit at a time!", this, PanelTeams);
                        return;
                    }
                    foreach (GridViewRow _grv in gvTeams.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFTeamID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit team details
                            LoadEditTeamControls(Convert.ToInt32(_HFTeamID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the selected team details for edit! " + ex.Message.ToString(), this, PanelTeams);
            }
        }
        //check if a team is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTeams))//check if a teams is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select team to view!", this, PanelTeams);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvTeams.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one team!. Select only one team to view the details at a time !", this, PanelTeams);
                        return;
                    }
                    foreach (GridViewRow _grv in gvTeams.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFTeamID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view team details
                            LoadViewTeamControls(Convert.ToInt32(_HFTeamID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the the selected team details for view purpose ! " + ex.Message.ToString(), this, PanelTeams);
                return;
            }
        }
        // populate the view team controls
        private void LoadViewTeamControls(int _teamID)
        {
            Teams_View getTeam = db.Teams_Views.Single(p => p.TeamID == _teamID);
            HiddenFieldTeamID.Value = _teamID.ToString();
            txtTeamCode.Text = getTeam.TeamCode;
            txtTeamName.Text = getTeam.TeamName;
            ddlHeadOfTeam.SelectedValue = getTeam.TeamHeadID.ToString();
            txtTeamDescription.Text = getTeam.TeamDescription;

            DisplayTeamMembers(_teamID);//load team members
            lbTeamMember.Visible = false;
            lbTeamMemberMsg.Visible = false;
            ddlTeamMember.Visible = false;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddTeam);
            lnkBtnSaveTeam.Visible = false;
            lnkBtnClearTeam.Visible = false;
            lnkBtnUpdateTeam.Visible = false;
            lnkBtnAddTeamMember.Visible = false; ;
            lbAddTeamHeader.Text = "View Team Details";
            ImageAddTeamHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddTeam.Show();
        }
        //load delete team details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTeams))//check if there is any team record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select team to delete !", this, PanelTeams);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvTeams.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected team?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected teams?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelTeams);
                return;
            }
        }
        //delete team record
        private void DeleteTeam()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvTeams.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFTeamID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _teamID = Convert.ToInt32(_HFTeamID.Value);

                        ListingItem deleteTeam = db.ListingItems.Single(p => p.ListingItemIID == _teamID);
                        //check if there is any team used
                        if (db.PROC_TeamMembers(_teamID).Any())
                        {
                            //delete team members
                            foreach (TeamMember _teamMember in db.TeamMembers.Where(p => p.teammember_iTeamID == _teamID))
                            {
                                db.TeamMembers.DeleteOnSubmit(_teamMember);
                                db.SubmitChanges();
                            }
                        }

                        //do not delete --- to be done

                        //delete the team if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteTeam);
                        db.SubmitChanges();
                    }
                }
                //refresh team after the delete is done
                DisplayTeams();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected team has been deleted.", this, PanelTeams);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected teams have been deleted.", this, PanelTeams);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelTeams);
                return;
            }
        }
        //delete the selected team
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteTeam();//delete selected team
            return;
        }
        //save team member
        private void SaveTeamMember()
        {
            ModalPopupExtenderAddTeam.Show();
            try
            {
                if (ddlTeamMember.SelectedIndex == 0)
                {
                    lbTeamMemberError.Text = "Select team member!";
                    ddlTeamMember.Focus();
                }
                else if (HiddenFieldTeamID.Value == "")
                {
                    lbTeamMemberError.Text = "Save the team details so that you can add a team member!";
                    return;
                }
                else
                {
                    int _teamID = Convert.ToInt32(HiddenFieldTeamID.Value);
                    Guid _staffID = Guid.Empty;
                    _staffID = _hrClass.ReturnGuid(ddlTeamMember.SelectedValue);
                    //check if the selected emember is allready added
                    if (db.TeamMembers.Any(p => p.teammember_iTeamID == _teamID && p.teammember_uiStaffID == _staffID))
                    {
                        lbTeamMemberError.Text = "The selected staff is already added as a team member!";
                        return;
                    }
                    else
                    {
                        //save the team member
                        TeamMember newTeamMember = new TeamMember();
                        newTeamMember.teammember_iTeamID = _teamID;
                        newTeamMember.teammember_uiStaffID = _staffID;
                        db.TeamMembers.InsertOnSubmit(newTeamMember);
                        db.SubmitChanges();
                        //dispaly team members
                        DisplayTeamMembers(_teamID);

                        lbTeamMemberError.Text = "Team member has been added!";
                        return;
                    }
                }
            }
            catch (Exception)
            {
                lbTeamMemberError.Text = "An error occured while tring to add the team member!";
                return;
            }
        }
        //save team member

        protected void lnkBtnAddTeamMember_Click(object sender, EventArgs e)
        {
            SaveTeamMember(); return;
        }
        //display team members
        private void DisplayTeamMembers(int _teamID)
        {
            try
            {
                object _display;
                _display = db.PROC_TeamMembers(_teamID);
                gvTeamMembers.DataSourceID = null;
                gvTeamMembers.DataSource = _display;
                gvTeamMembers.DataBind();
                return;

            }
            catch { }
        }
        //load team members details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvTeamMembers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ModalPopupExtenderAddTeam.Show();
            try
            {
                if (e.CommandName.CompareTo("EditTeamMember") == 0 || e.CommandName.CompareTo("DeleteTeamMember") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFTeamMemberID = (HiddenField)gvTeamMembers.Rows[ID].FindControl("HiddenField1");
                    int _teamMemberID = Convert.ToInt32(_HFTeamMemberID.Value);
                    if (e.CommandName.CompareTo("EditTeamMember") == 0)//check if we are editing the team member
                    {
                        //load edit team member details
                        //LoadEditTeamMemberDetails(_teamMemberID);
                        return;
                    }
                    else if (e.CommandName.CompareTo("DeleteTeamMember") == 0)//check if we are are deleting the team member
                    {
                        HiddenFieldTeamMemberID.Value = _teamMemberID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the team member?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucDeleteTeamMember, _deleteMessage);
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //method for deleting a team member
        private void DeleteTeamMember()
        {
            ModalPopupExtenderAddTeam.Show();
            try
            {
                int _teamID = Convert.ToInt32(HiddenFieldTeamID.Value);
                int _teamMemberID = Convert.ToInt32(HiddenFieldTeamMemberID.Value);
                TeamMember deleteTeamMember = db.TeamMembers.Single(p => p.teammember_iTeamMemberID == _teamMemberID);
                db.TeamMembers.DeleteOnSubmit(deleteTeamMember);
                db.SubmitChanges();
                DisplayTeamMembers(_teamID);
                _hrClass.LoadHRManagerMessageBox(2, "Team member selected has been successfully deleted.", this, PanelTeams);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelTeams);
                return;
            }
        }
        //delete team meber
        protected void lnkBtnDeleteTeamMember_Click(object sender, EventArgs e)
        {
            DeleteTeamMember();
            return;
        }
    }
}