﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace AdeptHRManager.Settings_Module
{
    public partial class Additional_Fields : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //load staff additional controls settings
                LoadStaffAdditionalControlsXML();
                //load applicant additinal controls settings
                LoadApplicantAdditionalControlsXML();
            }
        }
        //load staff xml additional fileds
        private void LoadStaffAdditionalControlsXML()
        {
            try
            {
                XDocument xmlDoc = XDocument.Load(Server.MapPath("~/xml_files/Staff_AdditionalInformation.xml"));//read the document
                var staffAdditionalControls = from r in xmlDoc.Descendants("control")//get the required details
                                              select new
                                          {
                                              ID = r.Attribute("id").Value.ToString().Trim(),
                                              ControlID = r.Element("controlid").Value.Trim(),
                                              ControlName = r.Element("controlname").Value.Trim(),
                                              IsNumeric = r.Element("isnumeric").Value.Trim(),
                                              Active = r.Element("active").Value.Trim(),
                                          };
                //GridView testGV = new GridView();
                //testGV.DataSource = staffAdditionalControls;
                //testGV.DataBind();
                //PanelAdditionalFields.Controls.Add(testGV);
                foreach (var _control in staffAdditionalControls)
                {
                    if (_control.Active == "Yes")
                    {
                        //get set text box
                        TextBox _textbox = (TextBox)PanelStaffAdditionalFields.FindControl(_control.ControlID);
                        _textbox.Text = _control.ControlName;
                        //check if the control is numeric
                        if (Convert.ToBoolean(_control.IsNumeric) == true)
                        {
                            //get the associated checkbox
                            CheckBox _checkBox = (CheckBox)PanelStaffAdditionalFields.FindControl(_control.ControlID.Replace("txt", "cb"));
                            _checkBox.Checked = true;
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                lbStaffError.Text = "Failed to load staff additional fields settings. " + ex.Message.ToString();
                return;
            }
        }

        //update staff additional fields xml documet
        private void UpdateStaffXMLAdditionalControls()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Server.MapPath("~/xml_files/Staff_AdditionalInformation.xml"));//read the document
                //get textbox cotrols for additiona staff fields
                foreach (Control _control in PanelStaffAdditionalFields.Controls)
                {
                    // Check and see if it's a textbox
                    if ((_control.GetType() == typeof(TextBox)))
                    {
                        //get the textbox
                        TextBox _textbox = ((TextBox)(_control));
                        //get the check box associated with the control
                        CheckBox _checkBox = (CheckBox)PanelStaffAdditionalFields.FindControl(_textbox.ID.Replace("txt", "cb"));

                        //get id last 2 index values
                        string _id = _textbox.ID.Substring(_textbox.ID.Length - 2);
                        //get the control details based on the id 
                        XmlNodeList nodeList = xmlDoc.SelectNodes("/staffcontrols/control[@id=" + _id + "]");
                        if (_textbox.Text.Trim() != "")
                        {
                            // update control name 
                            nodeList[0].SelectSingleNode("controlname").InnerText = _textbox.Text.Trim();
                            // update is numeric
                            nodeList[0].SelectSingleNode("isnumeric").InnerText = _checkBox.Checked.ToString();
                            //set if it is active
                            nodeList[0].SelectSingleNode("active").InnerText = "Yes";
                        }
                        else
                        { // update control name 
                            nodeList[0].SelectSingleNode("controlname").InnerText = "HR Description " + _id;
                            // update is numeric
                            nodeList[0].SelectSingleNode("isnumeric").InnerText = "False";
                            //set if it is active
                            nodeList[0].SelectSingleNode("active").InnerText = "No";
                        }

                    }
                }
                //save the file
                xmlDoc.Save(Server.MapPath("~/xml_files/Staff_AdditionalInformation.xml"));
                lbStaffError.Text = "Staff additional controls have been successfully set!";
                LoadStaffAdditionalControlsXML();
                return;
            }
            catch (Exception ex)
            {
                lbStaffError.Text = "Save failed. " + ex.Message.ToString();
                return;
            }

        }
        protected void lnkBtnSaveStaffRecordAdditionalFields_Click(object sender, EventArgs e)
        {
            UpdateStaffXMLAdditionalControls();
            return;
        }
        //load applicant xml additional fileds
        private void LoadApplicantAdditionalControlsXML()
        {
            try
            {
                XDocument xmlDoc = XDocument.Load(Server.MapPath("~/xml_files/applicant_AdditionalInformation.xml"));//read the document
                var applicantAdditionalControls = from r in xmlDoc.Descendants("control")//get the required details
                                                  select new
                                                  {
                                                      ID = r.Attribute("id").Value.ToString().Trim(),
                                                      ControlID = r.Element("controlid").Value.Trim(),
                                                      ControlName = r.Element("controlname").Value.Trim(),
                                                      IsNumeric = r.Element("isnumeric").Value.Trim(),
                                                      Active = r.Element("active").Value.Trim(),
                                                  };
                //GridView testGV1 = new GridView();
                //testGV1.DataSource = applicantAdditionalControls;
                //testGV1.DataBind();
                //PanelAdditionalFields.Controls.Add(testGV1);
                foreach (var _control in applicantAdditionalControls)
                {
                    if (_control.Active == "Yes")
                    {
                        //get set text box
                        TextBox _textbox = (TextBox)PanelApplicantAdditionalFields.FindControl(_control.ControlID);
                        _textbox.Text = _control.ControlName;
                        //check if the control is numeric
                        if (Convert.ToBoolean(_control.IsNumeric) == true)
                        {
                            //get the associated checkbox
                            CheckBox _checkBox = (CheckBox)PanelApplicantAdditionalFields.FindControl(_control.ControlID.Replace("txt", "cb"));
                            _checkBox.Checked = true;
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                lbApplicantError.Text = "Failed in loading applicant additional fiedls settings. " + ex.Message.ToString();
                return;
            }
        }
        //update applicant additional fields xml document
        private void UpdateApplicantXMLAdditionalControls()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Server.MapPath("~/xml_files/applicant_AdditionalInformation.xml"));//read the document
                //get textbox cotrols for additiona staff fields
                foreach (Control _control in PanelApplicantAdditionalFields.Controls)
                {
                    // Check and see if it's a textbox
                    if ((_control.GetType() == typeof(TextBox)))
                    {
                        //get the textbox
                        TextBox _textbox = ((TextBox)(_control));
                        //get the check box associated with the control
                        CheckBox _checkBox = (CheckBox)PanelApplicantAdditionalFields.FindControl(_textbox.ID.Replace("txt", "cb"));

                        //get id last 2 index values
                        string _id = _textbox.ID.Substring(_textbox.ID.Length - 2);
                        //get the control details based on the id 
                        XmlNodeList nodeList = xmlDoc.SelectNodes("/applicantcontrols/control[@id=" + _id + "]");
                        if (_textbox.Text.Trim() != "")
                        {
                            // update control name 
                            nodeList[0].SelectSingleNode("controlname").InnerText = _textbox.Text.Trim();
                            // update is numeric
                            nodeList[0].SelectSingleNode("isnumeric").InnerText = _checkBox.Checked.ToString();
                            //set if it is active
                            nodeList[0].SelectSingleNode("active").InnerText = "Yes";
                        }
                        else
                        { // update control name 
                            nodeList[0].SelectSingleNode("controlname").InnerText = "RE-Description-" + _id;
                            // update is numeric
                            nodeList[0].SelectSingleNode("isnumeric").InnerText = "False";
                            //set if it is active
                            nodeList[0].SelectSingleNode("active").InnerText = "No";
                        }

                    }
                }

                //save the file
                xmlDoc.Save(Server.MapPath("~/xml_files/Applicant_AdditionalInformation.xml"));
                lbApplicantError.Text = "Applicant additional controls have been successfully set!";
                LoadApplicantAdditionalControlsXML();
                return;
            }
            catch (Exception ex)
            {
                lbApplicantError.Text = "Save failed. " + ex.Message.ToString();
                return;
            }

        }
        protected void lnkBtnSaveApplicantAdditionalFields_Click(object sender, EventArgs e)
        {
            UpdateApplicantXMLAdditionalControls();
            return;
        }
    }
}