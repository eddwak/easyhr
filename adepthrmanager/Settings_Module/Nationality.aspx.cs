﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Settings_Module
{
    public partial class Nationality : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayNationalities();//load countries/nationalities
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddNationalityControls();
            ModalPopupExtenderAddNationality.Show();
            return;
        }
        //clear add nationality controls
        private void ClearAddNationalityControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddNationality);
            HiddenFieldNationalityID.Value = "";
            lnkBtnSaveNationality.Visible = true;
            lnkBtnUpdateNationality.Enabled = false;
            lnkBtnUpdateNationality.Visible = true;
            lnkBtnClearNationality.Visible = true;
            lbAddNationalityHeader.Text = "Add New Nationality";
            ImageAddNationalityHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add nationality controls
        private string ValidateAddNationalityControls()
        {
            if (txtCountry.Text.Trim() == "")
            {
                txtCountry.Focus();
                return "Enter country name!";
            }
            else if (txtNationality.Text.Trim() == "")
            {
                txtNationality.Focus();
                return "Enter nationality!";
            }
            else return "";
        }
        //save new nationality
        private void SaveNewNationality()
        {
            ModalPopupExtenderAddNationality.Show();
            try
            {
                string _error = ValidateAddNationalityControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new nationality details
                    const int _listingMasterID = 1000;//Master ID 1000 for countries and nationality
                    const string _listingType = "nationality";
                    string _countryName = "", _nationality = "";
                    _countryName = txtCountry.Text.Trim();
                    _nationality = txtNationality.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _countryName,_nationality))//check if the country exists
                    {
                        lbError.Text = "Save failed, the country or nationality you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new country/nationality
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _countryName, _nationality, null, null, null, null, null, null,null, HiddenFieldNationalityID);
                        DisplayNationalities();
                        lbError.Text = "";
                        lbInfo.Text = "Country and  nationality has been successfully saved.";
                        lnkBtnUpdateNationality.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateNationality()
        {
            ModalPopupExtenderAddNationality.Show();
            try
            {
                string _error = ValidateAddNationalityControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _nationalityID = Convert.ToInt32(HiddenFieldNationalityID.Value);
                    const int _listingMasterID = 1000;//Master ID 1000 for nationality and country
                    const string _listingType = "nationality";
                    string _countryName = "", _nationality = "";
                    _countryName = txtCountry.Text.Trim();
                    _nationality = txtNationality.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _nationalityID, _countryName,_nationality))//check if the country already exists
                    {
                        lbError.Text = "Save failed, the contry or nationality you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _nationalityID, _listingType, _countryName, _nationality, null, null, null, null, null, null,null);
                        DisplayNationalities();
                        lbError.Text = "";
                        lbInfo.Text = "Country and nationality has been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again!";
                return;
            }
        }
        protected void lnkBtnSaveNationality_Click(object sender, EventArgs e)
        {
            SaveNewNationality();
            return;
        }

        protected void lnkBtnUpdateNationality_Click(object sender, EventArgs e)
        {
            UpdateNationality();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearNationality_Click(object sender, EventArgs e)
        {
            ClearAddNationalityControls();
            ModalPopupExtenderAddNationality.Show();
            return;
        }
        //load nationalities
        private void DisplayNationalities()
        {
            object _display;
            _display = db.Nationalities_Views.Select(p => p).OrderBy(p => p.Country);
            gvNationalities.DataSourceID = null;
            gvNationalities.DataSource = _display;
            gvNationalities.DataBind();
            return;
        }
        // populate the edit natinality controls
        private void LoadEditNationalityControls(int _countryID)
        {
            ClearAddNationalityControls();
            Nationalities_View getNationality = db.Nationalities_Views.Single(p => p.CountryID == _countryID);
            HiddenFieldNationalityID.Value = _countryID.ToString();
            txtCountry.Text = getNationality.Country;
            txtNationality.Text = getNationality.Nationality;
            lnkBtnSaveNationality.Visible = false;
            lnkBtnClearNationality.Visible = true;
            lnkBtnUpdateNationality.Enabled = true;
            lbAddNationalityHeader.Text = "Edit Country/Nationality";
            ImageAddNationalityHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddNationality.Show();
            return;
        }
        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvNationalities))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelNationality);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvNationalities.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelNationality);
                        return;
                    }
                    foreach (GridViewRow _grv in gvNationalities.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFNationalityID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit country/nationality
                            LoadEditNationalityControls(Convert.ToInt32(_HFNationalityID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelNationality);
            }
        }
        //check if a couontry/ nationality record is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvNationalities))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to view the details!", this, PanelNationality);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvNationalities.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record!. Select only one record to view the details at a time !", this, PanelNationality);
                        return;
                    }
                    foreach (GridViewRow _grv in gvNationalities.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFNationalityID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view nationality
                            LoadViewNationalityControls(Convert.ToInt32(_HFNationalityID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the record for view purpose ! " + ex.Message.ToString(), this, PanelNationality);
                return;
            }
        }
        // populate the view nationality
        private void LoadViewNationalityControls(int _countryID)
        {
            Nationalities_View getNationality = db.Nationalities_Views.Single(p => p.CountryID == _countryID);
            HiddenFieldNationalityID.Value = _countryID.ToString();
            txtCountry.Text = getNationality.Country;
            txtNationality.Text = getNationality.Nationality;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddNationality);
            lnkBtnSaveNationality.Visible = false;
            lnkBtnClearNationality.Visible = false;
            lnkBtnUpdateNationality.Visible = false;
            lbAddNationalityHeader.Text = "View Country/Nationality";
            ImageAddNationalityHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddNationality.Show();
        }
        //load delete country/nationality
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvNationalities))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelNationality);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvNationalities.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelNationality);
                return;
            }
        }
        //delete copuntry/record record
        private void DeleteNationalities()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvNationalities.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFNationalityID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _nationalityID = Convert.ToInt32(_HFNationalityID.Value);

                        ListingItem deleteNationality = db.ListingItems.Single(p => p.ListingItemIID == _nationalityID);
                        //check if there is any nationality used

                        //do not delete --- to be done

                        //delete the country/nationality if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteNationality);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayNationalities();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected record has been deleted.", this, PanelNationality);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected records have been deleted.", this, PanelNationality);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelNationality);
                return;
            }
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteNationalities();//delete selected records(s)
            return;
        }
    }
}