﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Additional_Fields.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Additional_Fields" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelAdditionalFields" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelAdditionalFields" runat="server">
                <asp:Panel ID="PanelStaffAdditionalFields" runat="server">
                    <div class="panel-details-header">
                        Staff Record Additional Fields
                    </div>
                    <div class="panel-details">
                        <table>
                            <tr>
                                <td>
                                    HR Description 01:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription01" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription01" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    HR Description 06:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription06" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription06" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HR Description 02:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription02" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription02" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    HR Description 07:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription07" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription07" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HR Description 03:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription03" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription03" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    HR Description 08:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription08" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription08" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HR Description 04:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription04" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription04" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    HR Description 09:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription09" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription09" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HR Description 05:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription05" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription05" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    HR Description 10:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription10" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbHRDescription10" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lbStaffError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <div class="linkBtn">
                                        <asp:LinkButton ID="lnkBtnSaveStaffRecordAdditionalFields" OnClick="lnkBtnSaveStaffRecordAdditionalFields_Click"
                                            ToolTip="Save staff record additional fields" runat="server">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/process_add.png"
                                                ImageAlign="AbsMiddle" />
                                            Set Staff Additional Fields</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelApplicantAdditionalFields" runat="server">
                    <div class="panel-details-header">
                        Applicant Record Additional Fields
                    </div>
                    <div class="panel-details">
                        <table>
                            <tr>
                                <td>
                                    RE-Description-01:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription01" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription01" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    RE-Description-06:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription06" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription06" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    RE-Description-02:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription02" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription02" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    RE-Description-07:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription07" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription07" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    RE-Description-03:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription03" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription03" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    RE-Description-08:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription08" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription08" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    RE-Description-04:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription04" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription04" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    RE-Description-09:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription09" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription09" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    RE-Description-05:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription05" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription05" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                                <td>
                                    RE-Description-10:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReDescription10" Width="230px" runat="server"></asp:TextBox>
                                    <asp:CheckBox ID="cbReDescription10" Text="Is numeric?" TextAlign="Left" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lbApplicantError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <div class="linkBtn">
                                        <asp:LinkButton ID="lnkBtnSaveApplicantAdditionalFields" OnClick="lnkBtnSaveApplicantAdditionalFields_Click"
                                            ToolTip="Save applicant record additional fields" runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/process_add.png"
                                                ImageAlign="AbsMiddle" />
                                            Set Applicant Additional Fields</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
