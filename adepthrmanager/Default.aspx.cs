﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;


namespace AdeptHRManager
{
    public partial class _Default : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayDashBoard();
        }
        //diaplay dashboard
        private void DisplayDashBoard()
        {
            try
            {
                var _staffSession = HttpContext.Current.Session["LoggedStaffID"];
                //check if the staff is logged in
                if (_staffSession == null)
                {
                    //hide the dashboard
                    PanelDashBoard.Visible = false;
                }
                else
                {
                    //display the dashboard
                    DisplayNationalities();//populate nationality
                    DisplayEmployeesPerBranches();//populate employees per branChes
                    DisplayEmployeesPerDepartments();//populate employees per departments
                    OverallGenderGraph();//populate overall gender graph
                    GenderDistributionPerOfficeGraph();//gender distribution per offices
                    PanelDashBoard.Visible = true;
                    return;
                }
            }
            catch { }
        }
        //DISPLAY EMPLOYEES PER NATIONALITIES
        private void DisplayNationalities()
        {
            try
            {
                object _display;
                _display = db.PROC_CountNationalities();
                gvNationalities.DataSourceID = null;
                gvNationalities.DataSource = _display;
                gvNationalities.DataBind();
                return;
            }
            catch { }
        }
        //DISPLAY EMPLOYEES PER BRANCHES
        private void DisplayEmployeesPerBranches()
        {
            try
            {
                object _display;
                _display = db.PROC_CountEmployeesPerBranches();
                gvEmpoyeesPerBranches.DataSourceID = null;
                gvEmpoyeesPerBranches.DataSource = _display;
                gvEmpoyeesPerBranches.DataBind();
                return;
            }
            catch { }
        }
        //DISPLAY EMPLOYEES PER DEPARTMENTS
        private void DisplayEmployeesPerDepartments()
        {
            try
            {
                object _display;
                _display = db.PROC_CountEmployeesPerDepartments();
                gvEmployeesPerDepartments.DataSourceID = null;
                gvEmployeesPerDepartments.DataSource = _display;
                gvEmployeesPerDepartments.DataBind();
                return;
            }
            catch { }
        }
        //DISPLay OVERALL GENDER GRAPH
        private void OverallGenderGraph()
        {
            try
            {
                object _display;
                _display = db.PROC_CountOverallGender();
                ChartOverallGender.DataSourceID = null;
                ChartOverallGender.DataSource = _display;
                ChartOverallGender.DataBind();
                this.ChartOverallGender.Legends["Default"].Enabled = false;//hide the current legend

                //DataTable dt = db.PROC_CountOverallGender().AsEnumerable();
                string[] x = new string[db.PROC_CountOverallGender().Count()];
                decimal[] y = new decimal[db.PROC_CountOverallGender().Count()];
                int i = 0;
                Legend legend1 = new Legend("Legend1");//create a new legend
                foreach (PROC_CountOverallGenderResult _ROE in db.PROC_CountOverallGender())
                {

                    x[i] = _ROE.Gender;
                    y[i] = Convert.ToInt32(_ROE.CountGender);
                    i++;
                    if (_ROE.Gender.ToLower() == "female")//create the female legend item
                    {
                        LegendItem _femaleLegendItem = new LegendItem("Female:" + _ROE.CountGender, ColorTranslator.FromHtml("#3469BA"), "");
                        legend1.CustomItems.Add(_femaleLegendItem);
                    }
                    else
                    {
                        LegendItem _maleLegendItem = new LegendItem("Male:    " + _ROE.CountGender, ColorTranslator.FromHtml("#E60000"), "");
                        legend1.CustomItems.Add(_maleLegendItem);
                    }
                }
                legend1.Alignment = StringAlignment.Center;
                ChartOverallGender.Legends.Add(legend1);
                ChartOverallGender.Series[0].Points.DataBindXY(x, y);
                //ChartOverallGender.Series[0].Label = "#PERCENT{P2}" + "\n" + "#VALX";//percentage to 2 decimal places, plus label
                ChartOverallGender.Series[0].Label = "#PERCENT{P2}";//percentage to 2 decimal places

                //ChartOverallGender.Legends[0].Title= "xxx"; 



                ChartOverallGender.Series[0].Color = System.Drawing.ColorTranslator.FromHtml("#ff3333");
                ChartOverallGender.Series[0].ChartType = SeriesChartType.Column;
                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                ChartOverallGender.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#DADADA");
                ChartOverallGender.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#DADADA");
                ChartOverallGender.ChartAreas["ChartArea1"].AxisX.LineColor = ColorTranslator.FromHtml("#C6C6C6");
                ChartOverallGender.ChartAreas["ChartArea1"].AxisY.LineColor = ColorTranslator.FromHtml("#C6C6C6");
                ChartOverallGender.ChartAreas["ChartArea1"].AxisY.Title = "NUMBER OF EMPLOYEES";
                ChartOverallGender.ChartAreas["ChartArea1"].AxisX.Title = "GENDER";
                for (i = 0; i < x.Length; i++) //xValues.Lenght = 4 in this case where you have 4 Data number
                {
                    if (i == 0) // Don't forget xValues[0] is Data4 in your case
                    {
                        ChartOverallGender.Series[0].Points[i].Color = ColorTranslator.FromHtml("#3469BA");
                    }
                    if (i == 1)
                    {
                        ChartOverallGender.Series[0].Points[i].Color = ColorTranslator.FromHtml("#E60000");
                    }
                }
                //ChartOverallGender.Legends[0].CustomItems.Add(_maleLegendItem);
                //this.ChartOverallGender.Legends["Default"].Enabled = false;
                //Legend legend1 = new Legend("Legend1");//create a new legend
                ////add female legend
                //LegendItem _femaleLegendItem = new LegendItem("Female", ColorTranslator.FromHtml("#3469BA"), "");
                ////ChartOverallGender.Legends[0].CustomItems.Add(_femaleLegendItem);
                //legend1.CustomItems.Add(_femaleLegendItem);
                ////add male legend
                //LegendItem _maleLegendItem = new LegendItem("Male", ColorTranslator.FromHtml("#E60000"), "");
                //legend1.CustomItems.Add(_maleLegendItem);

                //legend1.Alignment = StringAlignment.Center;
                //ChartOverallGender.Legends.Add(legend1);

                //ChartOverallGender.Legends[0].CustomItems.Add(_maleLegendItem);
                //this.ChartOverallGender.Legends["Default"].Enabled = false;
                //Legend legend1 = new Legend("Legend1");
                //LegendItem legendItem1 = new LegendItem("Item1", Color.Blue, "");
                //legend1.CustomItems.Add(legendItem1);
                //ChartOverallGender.Legends.Add(legend1);

                // string[] x = new string[dt.Rows.Count];
                //decimal[] y = new decimal[dt.Rows.Count];
                //    decimal[] yy = new decimal[db.PROC_CountOverallGender().Count()];
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    x[i] = dt.Rows[i][0].ToString();
                //    y[i] = Convert.ToInt32(dt.Rows[i][1]);
                //}
                //BarChart1.Series.Add(new AjaxControlToolkit.BarChartSeries { Data = y });
                //BarChart1.CategoriesAxis = string.Join(",", x);
                //for (i = 0; i < x.Length; i++) //xValues.Lenght = 4 in this case where you have 4 Data number
                //{
                //    if (i == 0) // Don't forget xValues[0] is Data4 in your case
                //    {
                //       BarChart1.Series[0].BarColor= "#ff3333";
                //    }
                //    if (i == 1)
                //    {
                //        BarChart1.Series[1].BarColor ="#ffFF33";
                //        //Chart1.Series[0].Legend = "male";

                //    }
                //}
                //BarChart1.ChartTitle = "Overall gender distribution";
                //if (x.Length > 3)
                //{
                //    BarChart1.ChartWidth = (x.Length * 100).ToString();
                //}
            }
            catch { }
        }
        //DISPLAY GENDER DISTRIBUTION PER OFFICE GRAPH
        private void GenderDistributionPerOfficeGraph()
        {
            try
            {
                object _display;
                _display = db.PROC_CountGenderPerOffice();
                ChartGenderPerOffices.DataSourceID = null;
                ChartGenderPerOffices.DataSource = _display;
                ChartGenderPerOffices.DataBind();

                //Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth=0;
                ChartGenderPerOffices.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = ColorTranslator.FromHtml("#DADADA");
                ChartGenderPerOffices.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = ColorTranslator.FromHtml("#DADADA");
                ChartGenderPerOffices.ChartAreas["ChartArea1"].AxisX.LineColor = ColorTranslator.FromHtml("#C6C6C6");
                ChartGenderPerOffices.ChartAreas["ChartArea1"].AxisY.LineColor = ColorTranslator.FromHtml("#C6C6C6");
                ChartGenderPerOffices.ChartAreas["ChartArea1"].AxisY.Title = "NUMBER OF EMPLOYEES";
                return;
            }
            catch { }
        }
    }
}
