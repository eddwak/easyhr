﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Recruitment_Settings.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Recruitment_Settings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelRecruitmentSettings" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelRecruitmentSettings" runat="server">
                <asp:TabContainer ID="TabContainerRecruitmentSettings" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelJobType" runat="server" CssClass="ajax-tab" HeaderText="Job Types">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewJobType" ToolTip="Add new job type" OnClick="LinkButtonNewRecruitmentSetting_Click"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewJobType" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewJobType" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditJobType" OnClick="LinkButtonEditRecruitmentSetting_Click"
                                    ToolTip="Edit job type" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditJobType" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditJobType" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteJobType" OnClick="LinkButtonDeleteRecruitmentSetting_Click"
                                    CausesValidation="false" ToolTip="Delete job type" runat="server">
                                    <asp:Image ID="ImageDeleteJobType" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteJobType" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvJobTypes" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No job types available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobTypeID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="JobType" HeaderText="Job Type" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelJobCategory" runat="server" CssClass="ajax-tab" HeaderText="Job Categories">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewJobCategory" ToolTip="Add new job category" OnClick="LinkButtonNewRecruitmentSetting_Click"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewJobCategory" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditJobCategory" OnClick="LinkButtonEditRecruitmentSetting_Click"
                                    ToolTip="Edit job category" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditJobCategory" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditJobCategory" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteJobCategory" OnClick="LinkButtonDeleteRecruitmentSetting_Click"
                                    CausesValidation="false" ToolTip="Delete job category" runat="server">
                                    <asp:Image ID="ImageDeleteJobCategory" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteJobCategory" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvJobCategories" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                                    EmptyDataText="No job categories available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobCategoryID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="JobCategory" HeaderText="Job Category" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelJobLocation" runat="server" CssClass="ajax-tab" HeaderText="Job Locations">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewJobLocation" ToolTip="Add new job location" OnClick="LinkButtonNewJobLocation_Click"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewJobLocation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="Label1" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditJobLocation" OnClick="LinkButtonEditJobLocation_Click"
                                    ToolTip="Edit job location" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditJobLocation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditJobLocation" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteJobLocation" OnClick="LinkButtonDeleteRecruitmentSetting_Click"
                                    CausesValidation="false" ToolTip="Delete job location" runat="server">
                                    <asp:Image ID="ImageDeleteJobLocation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteJobLocation" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvLocations" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No locations available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("LocationID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Country" HeaderText="Country" />
                                        <asp:BoundField DataField="Location" HeaderText="Location" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelJobStatus" runat="server" CssClass="ajax-tab" HeaderText="Job Status">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewJobStatus" OnClick="LinkButtonNewRecruitmentSetting_Click"
                                    ToolTip="Add new job status" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewJobStatus" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="Label2" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditJobStatus" OnClick="LinkButtonEditRecruitmentSetting_Click"
                                    ToolTip="Edit job status" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditJobStatus" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditJobStatus" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteJobStatus" OnClick="LinkButtonDeleteRecruitmentSetting_Click"
                                    CausesValidation="false" ToolTip="Delete job status" runat="server">
                                    <asp:Image ID="ImageDeleteJobStatus" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteJobStatus" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvJobStatus" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No job status available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobStatusID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="JobStatus" HeaderText="Job Status" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelCurrency" runat="server" CssClass="ajax-tab" HeaderText="Currency">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewCurrency" ToolTip="Add new currency" OnClick="LinkButtonNewRecruitmentSetting_Click"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewCurrencye" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="Label3" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditCurrency" OnClick="LinkButtonEditRecruitmentSetting_Click"
                                    ToolTip="Edit currency" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditCurrency" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditCurrency" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteCurrency" OnClick="LinkButtonDeleteRecruitmentSetting_Click"
                                    CausesValidation="false" ToolTip="Delete currency" runat="server">
                                    <asp:Image ID="ImageDeleteCurrency" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteCurrency" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvCurrencies" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No currencies available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("CurrencyID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Currency" HeaderText="Currency" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelAddRecruitmentSetting" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddRecruitmentSetting" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddRecruitmentSettingHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddRecruitmentSettingHeader" runat="server" Text="Add New Setting"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddRecruitmentSetting" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend id="legendRecruitmentSettings" runat="server">Setting Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbRecruitmentSettingName" runat="server" Text="Label"></asp:Label><span
                                            class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRecruitmentName" runat="server"></asp:TextBox><span class="errorMessage"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldListingMasterID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldRecruitmentSettingID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddRecruitmentSettingPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveRecruitmentSetting" OnClick="lnkBtnSaveRecruitmentSetting_Click"
                                ToolTip="Save" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateRecruitmentSetting" OnClick="lnkBtnUpdateRecruitmentSetting_Click"
                                CausesValidation="false" ToolTip="Update" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearRecruitmentSetting" OnClick="lnkBtnClearRecruitmentSetting_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddRecruitmentSetting" RepositionMode="None"
                    X="270" Y="80" TargetControlID="HiddenFieldAddRecruitmentSettingPopup" PopupControlID="PanelAddRecruitmentSetting"
                    CancelControlID="ImageButtonCloseAddRecruitmentSetting" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddRecruitmentSetting" TargetControlID="PanelAddRecruitmentSetting"
                    DragHandleID="PanelDragAddRecruitmentSetting" runat="server"></asp:DragPanelExtender>
                <asp:Panel ID="PanelAddLocation" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddLocation" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddLocationHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddLocationHeader" runat="server" Text="Add New Location"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddLocation" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Job Location Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Country:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountryLocation" runat="server">
                                            <asp:ListItem Value="0">-Select Country-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Location<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtJobLocationName" runat="server"></asp:TextBox><span class="errorMessage"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image4" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label6" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label7" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label8" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldLocationID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddLocationPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveLocation" OnClick="lnkBtnSaveLocation_Click" ToolTip="Save"
                                runat="server">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateLocation" OnClick="lnkBtnUpdateLocation_Click" CausesValidation="false"
                                ToolTip="Update" Enabled="false" runat="server">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearLocation" OnClick="lnkBtnClearLocation_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddLocation" RepositionMode="None"
                    X="270" Y="80" TargetControlID="HiddenFieldAddLocationPopup" PopupControlID="PanelAddLocation"
                    CancelControlID="ImageButtonCloseAddLocation" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddLocation" TargetControlID="PanelAddLocation"
                    DragHandleID="PanelDragAddLocation" runat="server"></asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
