﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Recruitment_Report.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Recruitment_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelRecruitmentReport" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelRecruitmentReports" runat="server">
                <asp:TabContainer ID="TabContainerRecruitmentReports" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelRecruitmentReports" runat="server" CssClass="ajax-tab"
                        HeaderText="Recruitment Reports">
                        <HeaderTemplate>
                            Recruitment Reports
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table class="GridViewStyle">
                                    <tr class="AltRowStyle">
                                        <th>
                                        </th>
                                        <th>
                                            <b>Report Type</b>
                                        </th>
                                        <th>
                                            <b>Description</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="2%">
                                            1.
                                        </td>
                                        <td width="40%">
                                            <asp:LinkButton ID="lnkBtnInterviewReportPerJob" runat="server">Interview Per Job Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays an interview report for a particular job
                                        </td>
                                    </tr>
                                    <tr class="AltRowStyle">
                                        <td>
                                            2.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnSingleApplicantCVReport" runat="server">Single Applicant C.V.</asp:LinkButton>
                                        </td>
                                        <td>
                                            Generates CV for a selected applicant.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnReport" runat="server"></asp:LinkButton>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelReportViewer" runat="server" CssClass="ajax-tab" HeaderText="Report Viewer">
                        <HeaderTemplate>
                            Report Viewer
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divReportViewer" class="divReportViewerCSS">
                                <CR:CrystalReportViewer ID="CrystalReportViewerRecruitmentReports" runat="server"
                                    AutoDataBind="true" ToolPanelView="None" HasToggleGroupTreeButton="false" HasPageNavigationButtons="True"
                                    HasDrilldownTabs="False" SeparatePages="False" BestFitPage="False" GroupTreeStyle-ShowLines="False"
                                    PageZoomFactor="100" HasCrystalLogo="False" PrintMode="Pdf" Enabled="true" ToolbarStyle-CssClass="report-ToolbarStyle"
                                    BorderWidth="0px" ViewStateMode="Enabled" DisplayStatusbar="false" Width="100%" />
                                <CR:CrystalReportSource ID="CrystalReportSourceRecruitmentReports" runat="server">
                                </CR:CrystalReportSource>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelInterviewReportPerJob" Style="display: none; width: 600px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragInterviewReportPerJob" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageInterviewReportPerJobHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbInterviewReportPerJobHeader" runat="server" Text="Interview Report Per job"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseInterviewReportPerJob" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Job Reference & Title:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlInterviewReportPerJobJobReferenceAndTitle" Width="400px"
                                            runat="server">
                                            <asp:ListItem Value="0">Select Job Reference & Title</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress3" runat="server" />
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldInterviewReportPerJobPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnViewInterviewReportPerJob" OnClick="lnkBtnViewInterviewReportPerJob_Click"
                                ToolTip="View single job interview report" runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderInterviewReportPerJob" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerRecruitmentReports$TabPanelRecruitmentReports$lnkBtnInterviewReportPerJob"
                    PopupControlID="PanelInterviewReportPerJob" CancelControlID="ImageButtonCloseInterviewReportPerJob"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderInterviewReportPerJob" TargetControlID="PanelInterviewReportPerJob"
                    DragHandleID="PanelDragInterviewReportPerJob" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelSingleApplicantCVReport" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragSingleApplicantCVReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageSingleApplicantCVReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbSingleApplicantCVReportHeader" runat="server" Text="Single Applicant CV Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseSingleApplicantCVReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Applicant Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleApplicantCVApplicantName" Width="400px" runat="server">
                                            <asp:ListItem Value="0">Select Applicant Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldSingleApplicantCVReportPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnViewSingleApplicantCVReport" OnClick="lnkBtnViewSingleApplicantCVReport_Click"
                                ToolTip="View single applicant CV" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderSingleApplicantCVReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerRecruitmentReports$TabPanelRecruitmentReports$lnkBtnSingleApplicantCVReport"
                    PopupControlID="PanelSingleApplicantCVReport" CancelControlID="ImageButtonCloseSingleApplicantCVReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderSingleApplicantCVReport" TargetControlID="PanelSingleApplicantCVReport"
                    DragHandleID="PanelDragSingleApplicantCVReport" runat="server">
                </asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
