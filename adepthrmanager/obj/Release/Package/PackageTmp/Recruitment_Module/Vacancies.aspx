﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Vacancies.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Vacancies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelVacancies" runat="server">
        <ContentTemplate>
            <div class="content-menu-bar">
                <asp:LinkButton ID="LinkButtonNew" ToolTip="Add new vacancy" CausesValidation="false"
                    runat="server">
                    <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                    Add New</asp:LinkButton>
                <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonEdit" ToolTip="Edit vacancy details" CausesValidation="false"
                    runat="server">
                    <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                    Edit</asp:LinkButton>
                <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonView" ToolTip="View vacancy details" CausesValidation="false"
                    runat="server">
                    <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                    View</asp:LinkButton>
                <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonDelete" CausesValidation="false" ToolTip="Delete vacancy"
                    runat="server">
                    <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                    Delete</asp:LinkButton>
            </div>
            <div class="panel-details">
                <table>
                    <tr>
                        <td>
                            <b>Vacancies:</b>
                        </td>
                        <td>
                            Total Vacancies:
                            <asp:TextBox ID="txtTotalVacancies" Width="100px" Text="4" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gvVacancies" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No vacancies available!">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1+"." %>
                            </ItemTemplate>
                            <ItemStyle Width="4px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="4px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="VacancyRef" HeaderText="Reference" />
                        <asp:BoundField DataField="JobRef" HeaderText="Job Ref:" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                        <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                    </Columns>
                    <FooterStyle CssClass="PagerStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                </asp:GridView>
            </div>
            <asp:Panel ID="PanelAddInterviewVacancy" runat="server">
                <div class="panel-details-header">
                    Add New Vacancy
                </div>
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                Vacancy Reference:
                            </td>
                            <td>
                                <asp:TextBox ID="txtVacancyReference" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Job Reference:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlLocation" runat="server">
                                    <asp:ListItem Value="0">-Select Job Reference-</asp:ListItem>
                                    <asp:ListItem Value="Kenya">J0001</asp:ListItem>
                                    <asp:ListItem Value="Kenya">J0002</asp:ListItem>
                                    <asp:ListItem Value="Kenya">J0003</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Job Title:
                            </td>
                            <td>
                                <asp:TextBox ID="txtJobTitle" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Status:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblStatus" RepeatDirection="Horizontal" runat="server">
                                    <asp:ListItem Value="Vacant">Vacant</asp:ListItem>
                                    <asp:ListItem Value="On Progress">On Progress</asp:ListItem>
                                    <asp:ListItem Value="Filled">Filled</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="linkBtn">
                                    <asp:LinkButton ID="lnkBtnSave" ToolTip="Save Vacancy" runat="server">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                        Save</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnUpdate" CausesValidation="false" ToolTip="Update Vacancy"
                                        Enabled="false" runat="server">
                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                            ImageAlign="AbsMiddle" />
                                        Update</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnClear" CausesValidation="false" ToolTip="Clear" runat="server">
                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                            ImageAlign="AbsMiddle" />
                                        Clear</asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
