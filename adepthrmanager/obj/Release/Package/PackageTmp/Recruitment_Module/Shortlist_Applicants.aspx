﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Shortlist_Applicants.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Applicants" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelShortlistApplicants" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelShortlistApplicants" runat="server">
                <asp:Panel ID="PanelJobList" Visible="true" runat="server">
                    <div class="content-menu-bar">
                        <asp:LinkButton ID="LinkButtonPullJobApplicants" OnClick="LinkButtonPullJobApplicants_Click"
                            ToolTip="Pull job applicants form Adept Systems jobsite" CausesValidation="false"
                            runat="server">
                            <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Down.png" />
                            Pull Job Applicants</asp:LinkButton>
                        <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonViewJobApplicants" OnClick="LinkButtonViewJobApplicants_Click"
                            ToolTip="View job applicants" CausesValidation="false" runat="server">
                            <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                            View Job Applicants</asp:LinkButton>
                    </div>
                    <div class="search-div">
                        Search:
                        <asp:TextBox ID="txtSearch" OnTextChanged="txtSearch_TextChanged" Width="550px" AutoPostBack="true"
                            runat="server"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                            Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by job reference/type/category/title:"
                            WatermarkCssClass="water-mark-text-extender">
                        </asp:TextBoxWatermarkExtender>
                        <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                            Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetJobsAdvertisedAndNotClosed"
                            TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                            CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                            CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                        </asp:AutoCompleteExtender>
                        <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                            width: 543px !important;">
                        </div>
                    </div>
                    <div class="panel-details">
                        <asp:GridView ID="gvJobsAdvertised" runat="server" AllowPaging="True" Width="100%"
                            AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                            EmptyDataText="No advertised job available!">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1+"." %>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="JobReference" HeaderText="Job Ref." />
                                <asp:BoundField DataField="JobType" HeaderText="Job Type" />
                                <asp:BoundField DataField="JobCategory" HeaderText="Job Category" />
                                <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                                <asp:BoundField DataField="AdvertFromDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Advert From Date" />
                                <asp:BoundField DataField="AdvertToDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Advert To Date" />
                            </Columns>
                            <FooterStyle CssClass="PagerStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelApplicantDetails" Visible="false" runat="server">
                    <div class="panel-details-header">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 98%;">
                                    <asp:Image ID="ImageApplicantDetailsHeader" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Medium/Male.png" />
                                    <asp:Label ID="lbApplicantDetailsHeader" runat="server" Text="Applicant Details"></asp:Label>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonCloseApplicantDetails" OnClick="ImageButtonCloseApplicantDetails_Click"
                                        ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-details">
                        <fieldset>
                            <legend>Applicant's Basic Information</legend>
                            <table>
                                <tr>
                                    <td>
                                        Applicant Reference:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApplicantReference" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Application Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApplicationDate" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Title:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApplicantTitle" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Applicant Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApplicantName" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ID Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtIDNumber" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Passport Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPassportNumber" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date of Birth:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDateOfBirth" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Gender:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtGender" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Primary Email Address:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPrimaryEmailAddress" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Alternative Email Address:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAlternativeEmailAddress" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Country Residence/Work:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCountryOfResidence" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        City/Town:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCityOrTown" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Citizenship:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFirstCitizenship" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        2<sup>nd</sup> Citizenship:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSecondCitizenship" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1<sup>st</sup> Phone Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneNumber1" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        2<sup>nd</sup> Phone Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneNumber2" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Postal Code:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPostalCode" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Postal Address:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPostalAddress" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Place of Residence:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPlaceOfResidence" ReadOnly="true" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend>Applicant's Employment History</legend>
                            <asp:GridView ID="gvApplicantEmploymentHistory" runat="server" AllowPaging="True"
                                Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                                PageSize="15" EmptyDataText="No employement history available!">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1+"." %>
                                        </ItemTemplate>
                                        <ItemStyle Width="4px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EmployerName" HeaderText="Employer" />
                                    <asp:BoundField DataField="JobPosition" HeaderText="Position/Title" />
                                    <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                                    <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                                    <asp:BoundField DataField="YearsOfExperience" HeaderText="Years of Experience" />
                                </Columns>
                                <FooterStyle CssClass="PagerStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </fieldset>
                        <fieldset>
                            <legend>Applicant's Education Background</legend>
                            <asp:GridView ID="gvApplicantEducationBackground" runat="server" AllowPaging="True"
                                Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                                PageSize="15" EmptyDataText="No education background details available!">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1+"." %>
                                        </ItemTemplate>
                                        <ItemStyle Width="4px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="QualificationType" HeaderText="Qualification" />
                                    <asp:BoundField DataField="InstitutionName" HeaderText="Institution" />
                                    <asp:BoundField DataField="ProgramCourse" HeaderText="Program/Course" />
                                    <asp:BoundField DataField="QualificationStatus" HeaderText="Status" />
                                    <asp:BoundField DataField="CompletionDate" ItemStyle-Width="107px" HeaderText="Completion Date" />
                                    <asp:BoundField DataField="GradeAttained" HeaderText="Grade/GPA" />
                                </Columns>
                                <FooterStyle CssClass="PagerStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </fieldset>
                        <div class="linkBtn">
                        <asp:HiddenField ID="HiddenFieldJobID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldJobApplicationID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldApplicantID" runat="server" />
                            <asp:LinkButton ID="lnkBtnShortlistApplicantAndGoBack" CausesValidation="false" OnClick="lnkBtnShortlistApplicantAndGoBack_Click"
                                ToolTip="Shortlist applicant & go back" runat="server">
                                Shortlist Applicant & Go Back</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnShortlistApplicant" OnClick="lnkBtnShortlistApplicant_Click"
                                CausesValidation="false" ToolTip="Shortlist applicant" runat="server">
                                Shortlist Applicant</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnShortlistGoBack" OnClick="ImageButtonCloseApplicantDetails_Click"
                                CausesValidation="false" ToolTip="Go back" runat="server">
                                Go Back</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelJobApplicationReceived" runat="server" Style="display: none;
                    width: 900px">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragJobApplicationReceived" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="97%">
                                        <asp:Image ID="Image8" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Download.png" />
                                        <asp:Label ID="lbJobApplicationReceivedHeader" runat="server" Text="Job Applications Received"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:ImageButton ID="ImageCloseJobApplicationReceived" ImageUrl="~/images/icons/Small/Remove.png"
                                            runat="server" ToolTip="Close" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <asp:HiddenField ID="HiddenFieldJobApplicationReceivedPopup" runat="server" />
                        <table width="100%">
                            <tr>
                                <td>
                                    <div class="div-app-pages">
                                        <asp:GridView ID="gvJobApplicationReceived" OnRowCommand="gvJobApplicationReceived_RowCommand"
                                            Width="100%" runat="server" CssClass="GridViewStyle" AllowPaging="True" AutoGenerateColumns="False"
                                            PageSize="100">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1+"." %>
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("ApplicantID") %>' />
                                                        <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Bind("JobApplicationID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="4px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ApplicantReference" HeaderText="Applicant Ref." />
                                                <asp:BoundField DataField="ApplicantName" HeaderText="Name" />
                                                <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                                                <asp:BoundField DataField="HighestLevelOfEducation" HeaderText="Highest Level of Education" />
                                                <asp:BoundField DataField="IsShortlisted" HeaderText="Is Shortlisted?" />
                                                <asp:ButtonField HeaderText="View CV" ControlStyle-CssClass="linkBtn" CommandName="ViewCV"
                                                    ItemStyle-Width="4px" Text="View C.V." />
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <FooterStyle CssClass="PagerStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderJobApplicationReceived" RepositionMode="None"
                    X="100" Y="10" BackgroundCssClass="modalback" TargetControlID="HiddenFieldJobApplicationReceivedPopup"
                    PopupControlID="PanelJobApplicationReceived" CancelControlID="ImageCloseJobApplicationReceived"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderJobApplicationReceived" TargetControlID="PanelJobApplicationReceived"
                    DragHandleID="PanelDragJobApplicationReceived" runat="server">
                </asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
