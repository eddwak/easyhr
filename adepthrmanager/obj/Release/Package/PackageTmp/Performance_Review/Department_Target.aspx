﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Performance_Review/PerformanceReview.master"
    AutoEventWireup="true" CodeBehind="Department_Target.aspx.cs" Inherits="AdeptHRManager.Performance_Review.Department_Target" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PerformanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelDepartmentTarget" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelDepartmentTarget" runat="server">
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                <b>Year:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlYear" OnSelectedIndexChanged="ddlYearOrDepartment_SelectedIndexChanged"
                                    AutoPostBack="true" Width="100px" runat="server">
                                    <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>Department:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" OnSelectedIndexChanged="ddlYearOrDepartment_SelectedIndexChanged"
                                    AutoPostBack="true" Width="200px" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <asp:Panel ID="PanelAddDepartmentTargetDetails" runat="server">
                        <table class="GridViewStyle">
                            <tr class="AltRowStyle">
                                <th>
                                </th>
                                <th>
                                    <b>Target</b>
                                </th>
                                <th>
                                    <b>Actual</b>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    Quarter 1:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter1Target" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter1Actual" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <trclass="AltRowStyle">
                                <td>
                                    Quarter 2:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter2Target" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter2Actual" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Quarter 3:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter3Target" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter3Actual" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="AltRowStyle">
                                <td>
                                    Quarter 4:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter4Target" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter4Actual" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbDepartmentName" runat="server" Text="Overall"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDepartmentOverallTarget" ReadOnly="true" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDepartmentOverallActual" ReadOnly="true" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="AltRowStyle">
                                <td>
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTargetComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtActualComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:HiddenField ID="HiddenFieldDepartmentTargetID" runat="server" />
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                    <asp:Label ID="lbRequiredFields" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div class="linkBtn" style="text-align: right;">
                        <asp:LinkButton ID="lnkBtnSave" OnClick="lnkBtnSave_Click" ToolTip="Save" runat="server">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                            Save</asp:LinkButton></div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
