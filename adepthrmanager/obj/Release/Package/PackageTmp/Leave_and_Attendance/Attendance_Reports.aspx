﻿<%@ Page Title="Attendance Reports" Language="C#" MasterPageFile="/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Attendance_Reports.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Attendance_Reports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelAttendanceReports" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelAttendanceReports" runat="server">
                <asp:TabContainer ID="TabContainerAttendanceReports" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelAttendanceReports" runat="server" CssClass="ajax-tab" HeaderText="Attendance Reports">
                        <HeaderTemplate>
                            Attendance Reports
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table class="GridViewStyle">
                                    <tr class="AltRowStyle">
                                        <th>
                                        </th>
                                        <th>
                                            <b>Report Type</b>
                                        </th>
                                        <th>
                                            <b>Description</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="2%">
                                            1.
                                        </td>
                                        <td width="40%">
                                            <asp:LinkButton ID="lnkBtnSingleStaffAttendanceReport" runat="server">Single Staff Attendance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays a single staff Attendance report for a particular duration.
                                        </td>
                                    </tr>
                                    <tr class="AltRowStyle">
                                        <td>
                                            2.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnEmployeesDailyAttendanceReport" runat="server">Daily Attendance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays employees' daily attendance report either for all employees or filters
                                            for employees within a particular branch or department.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnEmployeesWeeklyAttendanceReport" runat="server">Weekly Attendance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays employees' weekly attendance report for all employess or filters for employees
                                            within a particular branch or department.
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelReportViewer" runat="server" CssClass="ajax-tab" HeaderText="Report Viewer">
                        <HeaderTemplate>
                            Report Viewer
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divReportViewer" class="divReportViewerCSS">
                                <CR:CrystalReportViewer ID="CrystalReportViewerAttendanceReports" runat="server"
                                    AutoDataBind="true" ToolPanelView="None" HasToggleGroupTreeButton="false" HasPageNavigationButtons="True"
                                    HasDrilldownTabs="False" SeparatePages="False" BestFitPage="False" GroupTreeStyle-ShowLines="False"
                                    PageZoomFactor="100" HasCrystalLogo="False" PrintMode="Pdf" Enabled="true" ToolbarStyle-CssClass="report-ToolbarStyle"
                                    BorderWidth="0px" ViewStateMode="Enabled" DisplayStatusbar="false" Width="100%" />
                                <CR:CrystalReportSource ID="CrystalReportSourceAttendanceReports" runat="server">
                                </CR:CrystalReportSource>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelSingleStaffAttendanceReport" Style="display: none; width: 550px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragSingleStaffAttendanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageSingleStaffAttendanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbSingleStaffAttendanceReportHeader" runat="server" Text="Single Staff Attendance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseSingleStaffAttendanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Staff Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleStaffAttendanceReportStaffName" runat="server">
                                            <asp:ListItem Value="0">Select Staff Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Between Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSingleStaffAttendanceFromDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonSingleStaffAttendanceFromDate" ToolTip="Pick from date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtSingleStaffAttendanceFromDate_CalendarExtender" runat="server"
                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtSingleStaffAttendanceFromDate"
                                            PopupButtonID="ImageButtonSingleStaffAttendanceFromDate" PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtSingleStaffAttendanceFromDate_MaskedEditExtender"
                                            runat="server" ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtSingleStaffAttendanceFromDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtSingleStaffAttendanceFromDate_MaskedEditValidator"
                                            runat="server" ControlExtender="txtSingleStaffAttendanceFromDate_MaskedEditExtender"
                                            ControlToValidate="txtSingleStaffAttendanceFromDate" CssClass="errorMessage"
                                            ErrorMessage="txtSingleStaffAttendanceFromDate_MaskedEditValidator" InvalidValueMessage="<br>Invalid from date entered"
                                            Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        To Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSingleStaffAttendanceToDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonSingleStaffAttendanceToDate" ToolTip="Pick to date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtSingleStaffAttendanceToDate_CalendarExtender" runat="server"
                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtSingleStaffAttendanceToDate"
                                            PopupButtonID="ImageButtonSingleStaffAttendanceToDate" PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtSingleStaffAttendanceToDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtSingleStaffAttendanceToDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtSingleStaffAttendanceToDate_MaskedEditValidator"
                                            runat="server" ControlExtender="txtSingleStaffAttendanceToDate_MaskedEditExtender"
                                            ControlToValidate="txtSingleStaffAttendanceToDate" CssClass="errorMessage" ErrorMessage="txtSingleStaffAttendanceToDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid to date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldSingleStaffAttendanceReportPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnViewSingleStaffAttendanceReport" OnClick="lnkBtnViewSingleStaffAttendanceReport_Click"
                                ToolTip="View single staff attendance report" runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderSingleStaffAttendanceReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerAttendanceReports$TabPanelAttendanceReports$lnkBtnSingleStaffAttendanceReport"
                    PopupControlID="PanelSingleStaffAttendanceReport" CancelControlID="ImageButtonCloseSingleStaffAttendanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderSingleStaffAttendanceReport" TargetControlID="PanelSingleStaffAttendanceReport"
                    DragHandleID="PanelDragSingleStaffAttendanceReport" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelEmployeesDailyAttendanceReport" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragEmployeesDailyAttendanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageEmployeesDailyAttendanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbEmployeesDailyAttendanceReportHeader" runat="server" Text="Daily Attendance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseEmployeesDailyAttendanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Attendance Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmployeesDailyAttendanceDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonEmployeesDailyAttendanceDate" ToolTip="Pick attendance date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtEmployeesDailyAttendanceDate_CalendarExtender" runat="server"
                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEmployeesDailyAttendanceDate"
                                            PopupButtonID="ImageButtonEmployeesDailyAttendanceDate" PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtEmployeesDailyAttendanceDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEmployeesDailyAttendanceDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtEmployeesDailyAttendanceDate_MaskedEditValidator"
                                            runat="server" ControlExtender="txtEmployeesDailyAttendanceDate_MaskedEditExtender"
                                            ControlToValidate="txtEmployeesDailyAttendanceDate" CssClass="errorMessage" ErrorMessage="txtEmployeesDailyAttendanceDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid attendance date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Daily Attendance For?
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblEmployeesDailyAttendanceReportsFor" OnSelectedIndexChanged="rblEmployeesDailyAttendanceReportsFor_SelectedIndexChanged"
                                            RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                            <asp:ListItem Selected="True">All Employees</asp:ListItem>
                                            <asp:ListItem>Posting Location/Branch</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Posting Location/Branch:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployeesDailyAttendanceReportPostingLocation" Enabled="false"
                                            AppendDataBoundItems="True" runat="server">
                                            <asp:ListItem Value="0">Select Company Branch</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbEmployeesDailyAttendanceReportPerDepartment" OnCheckedChanged="cbEmployeesDailyAttendanceReportPerDepartment_CheckedChanged"
                                            Text="View employees' report per department?" AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Department:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployeesDailyAttendanceReportDepartment" Enabled="false"
                                            runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress2" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewEmployeesDailyAttendanceReport" OnClick="lnkBtnViewEmployeesDailyAttendanceReport_Click"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderEmployeesDailyAttendanceReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerAttendanceReports$TabPanelAttendanceReports$lnkBtnEmployeesDailyAttendanceReport"
                    PopupControlID="PanelEmployeesDailyAttendanceReport" CancelControlID="ImageButtonCloseEmployeesDailyAttendanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderEmployeesDailyAttendanceReport" TargetControlID="PanelEmployeesDailyAttendanceReport"
                    DragHandleID="PanelDragEmployeesDailyAttendanceReport" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelEmployeesWeeklyAttendanceReport" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragEmployeesWeeklyAttendanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageEmployeesWeeklyAttendanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbEmployeesWeeklyAttendanceReportHeader" runat="server" Text="Weekly Attendance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseEmployeesWeeklyAttendanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Week Start Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmployeesWeeklyAttendanceWeekStartDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonEmployeesWeeklyAttendanceWeekStartDate" ToolTip="Pick week start date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtEmployeesWeeklyAttendanceWeekStartDate_CalendarExtender"
                                            runat="server" Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEmployeesWeeklyAttendanceWeekStartDate"
                                            PopupButtonID="ImageButtonEmployeesWeeklyAttendanceWeekStartDate" PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtEmployeesWeeklyAttendanceWeekStartDate_MaskedEditExtender"
                                            runat="server" ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEmployeesWeeklyAttendanceWeekStartDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtEmployeesWeeklyAttendanceWeekStartDate_MaskedEditValidator"
                                            runat="server" ControlExtender="txtEmployeesWeeklyAttendanceWeekStartDate_MaskedEditExtender"
                                            ControlToValidate="txtEmployeesWeeklyAttendanceWeekStartDate" CssClass="errorMessage"
                                            ErrorMessage="txtEmployeesWeeklyAttendanceWeekStartDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid week start date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Weekly Attendance For?
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblEmployeesWeeklyAttendanceReportsFor" OnSelectedIndexChanged="rblEmployeesWeeklyAttendanceReportsFor_SelectedIndexChanged"
                                            RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                            <asp:ListItem Selected="True">All Employees</asp:ListItem>
                                            <asp:ListItem>Posting Location/Branch</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Posting Location/Branch:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployeesWeeklyAttendanceReportPostingLocation" Enabled="false"
                                            AppendDataBoundItems="True" runat="server">
                                            <asp:ListItem Value="0">Select Company Branch</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbEmployeesWeeklyAttendanceReportPerDepartment" OnCheckedChanged="cbEmployeesWeeklyAttendanceReportPerDepartment_CheckedChanged"
                                            Text="View employees' report per department?" AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Department:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployeesWeeklyAttendanceReportDepartment" Enabled="false"
                                            runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress3" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewEmployeesWeeklyAttendanceReport" OnClick="lnkBtnViewEmployeesWeeklyAttendanceReport_Click"
                                runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderEmployeesWeeklyAttendanceReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerAttendanceReports$TabPanelAttendanceReports$lnkBtnEmployeesWeeklyAttendanceReport"
                    PopupControlID="PanelEmployeesWeeklyAttendanceReport" CancelControlID="ImageButtonCloseEmployeesWeeklyAttendanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderEmployeesWeeklyAttendanceReport" TargetControlID="PanelEmployeesWeeklyAttendanceReport"
                    DragHandleID="PanelDragEmployeesWeeklyAttendanceReport" runat="server">
                </asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
