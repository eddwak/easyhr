﻿<%@ Page Title="Leave Applications" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Leave_Applications.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Leave_Applications" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaveApplications" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeaves" runat="server">
                <asp:TabContainer ID="TabContainerLeaves" CssClass="ajax-tab" runat="server" ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelLeaveApplication" runat="server" CssClass="ajax-tab" HeaderText="Leave Application">
                        <HeaderTemplate>
                            Leave Application
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:Panel ID="PanelStaffLeaveApplication" runat="server">
                                    <fieldset>
                                        <legend>Leave Application Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Staff Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStaffName" OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtLeaveSerialNumber" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave Type:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLeaveType" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbSingleOrMultipleDays" runat="server" Text="Single Or Multiple Day[s]"></asp:Label>
                                                    <span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblSingleOrMultipleDays" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem>Single Day</asp:ListItem>
                                                        <asp:ListItem>Multiple Days</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave From Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td style="vertical-align: bottom;">
                                                    <asp:TextBox ID="txtLeaveFromDate" OnTextChanged="txtLeaveFromDate_TextChanged" AutoPostBack="true"
                                                        Width="270px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLeaveFromDate" ToolTip="Pick leave starts from which date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLeaveFromDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtLeaveFromDate" PopupButtonID="ImageButtonLeaveFromDate"
                                                        PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:TextBox ID="txtEffectiveFromDate" Width="160px" Visible="false" runat="server"></asp:TextBox>
                                                    <td>
                                                        <asp:RadioButtonList ID="rblLeaveFromSession" OnSelectedIndexChanged="rblLeaveFromSession_SelectedIndexChanged"
                                                            AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                            <asp:ListItem Value="M">Morning</asp:ListItem>
                                                            <asp:ListItem Value="A">Afternoon</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave To Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLeaveToDate" OnTextChanged="txtLeaveToDate_TextChanged" AutoPostBack="true"
                                                        Width="270px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLeaveToDate" ToolTip="Pick leave to which date" CssClass="date-image"
                                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLeaveToDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtLeaveToDate" PopupButtonID="ImageButtonLeaveToDate"
                                                        PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:TextBox ID="txtEffectiveTillDate" Width="270px" Visible="false" runat="server"></asp:TextBox>
                                                    <td>
                                                        <asp:RadioButtonList ID="rblLeaveToSession" OnSelectedIndexChanged="rblLeaveToSession_SelectedIndexChanged"
                                                            AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                            <asp:ListItem Value="M">Morning</asp:ListItem>
                                                            <asp:ListItem Value="A">Afternoon</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave Days:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLeaveDays" Width="150px" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Remarks / Description:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                                    <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbleaveStatusInfo" runat="server" CssClass="info-message" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldLeaveID" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveLeave" OnClick="lnkBtnSaveLeave_Click" ToolTip="Save leave application"
                                            runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Leave</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateLeave" OnClick="lnkBtnUpdateLeave_Click" CausesValidation="false"
                                            ToolTip="Update leave application" Enabled="false" runat="server">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update Leave</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClear" OnClick="lnkBtnClear_Click" CausesValidation="false"
                                            ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Reset</asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLeaveApplicationsMade" runat="server" CssClass="ajax-tab"
                        HeaderText="Leave Applications Made">
                        <HeaderTemplate>
                            Leave Applications Made
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="tab-search-div">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Search:
                                            <asp:TextBox ID="txtSearch" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"
                                                runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                                                Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by reference number/name/id number/phone number"
                                                WatermarkCssClass="water-mark-text-extender">
                                            </asp:TextBoxWatermarkExtender>
                                            <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                                                Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetStaffRecords"
                                                TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                                                CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                                                CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                                            </asp:AutoCompleteExtender>
                                            <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                width: 343px !important;">
                                            </div>
                                        </td>
                                        <td align="right">
                                            <asp:DropDownList ID="ddlSearchLeaveApplicationPerYear" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchLeaveApplicationPerYear_SelectedIndexChanged"
                                                CssClass="drpDownYear" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="content-menu-bar">
                                <asp:Label ID="lbLeaveApplicationsMade" Font-Bold="true" runat="server" Text="Leave Applications Approved"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvLeavesApplicationsMade" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    OnPageIndexChanging="gvLeavesApplicationsMade_PageIndexChanging" EmptyDataText="No leave applications records found!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="4px" ItemStyle-Font-Size="10px">
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="ddlHeader" Width="25px" runat="server" CssClass="dropDownMore"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlGvLeavesApplicationsMadeHeaderDropDown_SelectedIndexChanged">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem Value="Edit" Text="Edit Leave Application"></asp:ListItem>
                                                    <asp:ListItem Value="Erase" Text="Erase Leave Application"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff:" />
                                        <asp:BoundField DataField="StaffRef" HeaderText="No:" />
                                        <asp:BoundField DataField="DepartmentName" HeaderText="Department" />
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vAppliedBetween" HeaderText="Applied Between" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" HeaderText="Days" />
                                        <asp:BoundField DataField="AuthorizedByStaffName" HeaderText="Authorized By" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason / Remarks" />
                                        <asp:BoundField DataField="leave_vDateCreated" HeaderText="Date Created" />
                                        <asp:BoundField DataField="leave_vDateApproved" HeaderText="Date Approved" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLeavesDueForApproval" runat="server" CssClass="ajax-tab"
                        HeaderText="Leaves Due For Approval">
                        <HeaderTemplate>
                            Leaves Due For Approval
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvLeavesDueForApproval" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    EmptyDataText="There are no leave applications due for approval available">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="4px" ItemStyle-Font-Size="10px">
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="ddlHeader" Width="25px" runat="server" CssClass="dropDownMore"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlGvLeavesDueForApprovalHeaderDropDown_SelectedIndexChanged">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem Value="Edit" Text="Approve or Disapprove Leave"></asp:ListItem>
                                                    <asp:ListItem Value="Preview" Text="Edit Leave Application"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff Name:" ItemStyle-Width="220px" />
                                        <asp:BoundField DataField="StaffRef" HeaderText="No:" ItemStyle-Width="50px" />
                                        <asp:BoundField DataField="DepartmentName" HeaderText="Department" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" ItemStyle-Width="150px" />
                                        <asp:BoundField DataField="leave_vFromDate" HeaderText="From" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="leave_vToDate" HeaderText="Until" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" ItemStyle-Width="4px" HeaderText="Days" />
                                        <asp:BoundField DataField="leave_vDateCreated" ItemStyle-Width="120px" HeaderText="Date Created" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason / Remarks" ItemStyle-Width="300px" />
                                        <asp:BoundField DataField="ReportsTo" HeaderText="Reports To:" ItemStyle-Width="220px" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelCanceledOrRejectedLeaves" runat="server" CssClass="ajax-tab"
                        HeaderText="Leaves Cancelled Or Rejected">
                        <HeaderTemplate>
                            Leaves Cancelled Or Rejected
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="tab-search-div">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Search:
                                            <asp:TextBox ID="txtSearchRejectedLeaves" OnTextChanged="txtSearchRejectedLeaves_TextChanged"
                                                AutoPostBack="true" runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtSearchRejectedLeaves_TextBoxWatermarkExtender"
                                                runat="server" Enabled="True" TargetControlID="txtSearchRejectedLeaves" WatermarkText="Search by reference number/name/id number/phone number"
                                                WatermarkCssClass="water-mark-text-extender">
                                            </asp:TextBoxWatermarkExtender>
                                            <asp:AutoCompleteExtender ID="txtSearchRejectedLeaves_AutoCompleteExtender" runat="server"
                                                DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                ServiceMethod="GetStaffRecords" TargetControlID="txtSearchRejectedLeaves" CompletionInterval="0"
                                                CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                CompletionListElementID="divSearchRejectedLeaves">
                                            </asp:AutoCompleteExtender>
                                            <div id="divSearchRejectedLeaves" style="max-height: 200px; display: none; overflow: auto;
                                                width: 343px !important;">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvRejectedLeaveHistory" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" OnPageIndexChanging="gvRejectedLeaveHistory_PageIndexChanging"
                                    OnLoad="gvRejectedLeaveHistory_OnLoad" CssClass="GridViewStyle" AllowSorting="True"
                                    PageSize="10" EmptyDataText="There are no rejected leaves available">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff Name:" />
                                        <asp:BoundField DataField="StaffRef" HeaderText="No:" />
                                        <asp:BoundField DataField="DepartmentName" HeaderText="Department" />
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vAppliedBetween" HeaderText="Applied Between" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" HeaderText="Days" />
                                        <asp:BoundField DataField="leave_vDateCreated" HeaderText="Date Created" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason for Applying" />
                                        <asp:BoundField DataField="RejectedByStaffName" HeaderText="Rejected By:" />
                                        <asp:BoundField DataField="RejectionReasons" HeaderText="Rejection Reasons" />
                                        <asp:BoundField DataField="leave_vDateRejected" HeaderText="Date Rejected" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelEraseLeaveApplication" Style="display: none; width: 600px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragEraseLeaveApplication" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageEraseLeaveApplicationtHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Erase.png" />
                                        <asp:Label ID="lbEraseLeaveApplicationHeader" runat="server" Text="Erase Leave Application"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseEraseLeaveApplication" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Details</legend>
                            <table>
                                <tr>
                                    <td valign="top">
                                        Reason:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEraseLeaveApplicationReasons" Width="450px" TextMode="MultiLine"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldEraseLeaveApplicationPopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnEraseLeaveApplication" OnClick="lnkBtnEraseLeaveApplication_Click"
                                ToolTip="Erase leave appplication" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Erase.png" ImageAlign="AbsMiddle" />
                                Erase Leave Application</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderEraseLeaveApplication" RepositionMode="None"
                    X="250" Y="120" TargetControlID="HiddenFieldEraseLeaveApplicationPopup" PopupControlID="PanelEraseLeaveApplication"
                    CancelControlID="ImageButtonCloseEraseLeaveApplication" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderEraseLeaveApplication" TargetControlID="PanelEraseLeaveApplication"
                    DragHandleID="PanelDragEraseLeaveApplication" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelApproveOrCancelLeave" Style="display: none; width: 700px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragApproveOrCancelLeave" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageApproveOrCancelLeaveHeadeer" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Apply.png" />
                                        <asp:Label ID="lbApproveOrCancelLeaveHeader" runat="server" Text="Approve Or Cancel Leave Aplication"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseApproveOrCancelLeave" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Approve or Disapprove?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblApproveOrCancel" runat="server" CssClass="borderControl"
                                            RepeatDirection="Horizontal" OnSelectedIndexChanged="rblApproveOrCancel_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="Approve" Selected="True">Approve</asp:ListItem>
                                            <asp:ListItem Value="Disapprove">Disapprove</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Approved or Disapproved By?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblApprovedOrCancelledBy" runat="server" CssClass="borderControl"
                                            RepeatDirection="Horizontal" OnSelectedIndexChanged="rblApprovedOrCancelledBy_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Selected="True">Me</asp:ListItem>
                                            <asp:ListItem>On Behalf Of Another Manager</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbManagerName" runat="server" Text="Manager's Name" Visible="false"></asp:Label>
                                        <asp:Label ID="lbManagerNameError" runat="server" CssClass="errorMessage" Text="*"
                                            Visible="false"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlApprovedOrCanceledByManager" CssClass="drpDown" Width="250px"
                                            Visible="false" runat="server">
                                            <asp:ListItem Value="0">Select Manager</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        <asp:Label ID="lbCancelationReason" runat="server" Text="Disapproval Reason:" Visible="false"></asp:Label>
                                        <asp:Label ID="lbCancelationReasonError" runat="server" CssClass="errorMessage" Text="*"
                                            Visible="false"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLeaveCancelationReasons" Width="400px" Visible="false" TextMode="MultiLine"
                                            Rows="3" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image8" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label3" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldApproveOrCancelLeavePopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress2" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnApproveOrCancelLeave" OnClick="lnkBtnApproveOrCancelLeave_Click"
                                ToolTip="Approve leave appplication" runat="server">
                                Appprove Leave Application</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderApproveOrCancelLeave" RepositionMode="None"
                    X="250" Y="120" TargetControlID="HiddenFieldApproveOrCancelLeavePopup" PopupControlID="PanelApproveOrCancelLeave"
                    CancelControlID="ImageButtonCloseApproveOrCancelLeave" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderApproveOrCancelLeave" TargetControlID="PanelApproveOrCancelLeave"
                    DragHandleID="PanelDragApproveOrCancelLeave" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirmEraseLeaveApplication" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmLeaveApprovalOrDisapproval" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
