﻿<%@ Page Title="Holidays" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Holidays.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Holidays" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelHolidays" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelHolidays" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new holiday"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit holiday details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View holiday details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete holiday" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvHolidays" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="10" EmptyDataText="No holidays available!">
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="4px">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="4px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="HolidayDate" HeaderText="Holiday Date" />
                            <asp:BoundField DataField="Duration" HeaderText="Duration" />
                            <asp:BoundField DataField="ApplicableTo" HeaderText="Applicable To" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddHoliday" Style="display: none; width: 600px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddHoliday" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddHolidayHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddHolidayHeader" runat="server" Text="Add Holiday"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddHoliday" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Holiday Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Holiday Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtHolidayDate" Width="150px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonHolidayDate" ToolTip="Pick holiday date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtHolidayDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtHolidayDate" PopupButtonID="ImageButtonHolidayDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtHolidayDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtHolidayDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtHolidayDate_MaskedEditValidator" runat="server" ControlExtender="txtHolidayDate_MaskedEditExtender"
                                            ControlToValidate="txtHolidayDate" CssClass="errorMessage" ErrorMessage="txtHolidayDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid holiday date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                        <br />
                                        <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2014"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Holiday Duration:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblHolidayDuration" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="1">Full Day</asp:ListItem>
                                            <asp:ListItem Value="0.5">Half Day</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Holiday Applicable To:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlHolidayApplicableTo" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description/Comments:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldHolidayID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddHolidayPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveHoliday" OnClick="lnkBtnSaveHoliday_Click" ToolTip="Save holiday"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateHoliday" OnClick="lnkBtnUpdateHoliday_Click" CausesValidation="false"
                                ToolTip="Update holiday" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearHoliday" OnClick="lnkBtnClearHoliday_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddHoliday" RepositionMode="None" X="270"
                    Y="80" TargetControlID="HiddenFieldAddHolidayPopup" PopupControlID="PanelAddHoliday"
                    CancelControlID="ImageButtonCloseAddHoliday" BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddHoliday" TargetControlID="PanelAddHoliday"
                    DragHandleID="PanelDragAddHoliday" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
