﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Performance_Review.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Performance_Review" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffPerformanceReview" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffPerformanceReview" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                Performance Year:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAnnualPerformancePlanYear" OnSelectedIndexChanged="ddlAnnualPerformancePlanYear_SelectedIndexChanged"
                                    AutoPostBack="true" Width="100px" runat="server">
                                    <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PanelPerformanceObjectiveListing" Visible="true" runat="server">
                        <asp:Panel ID="PanelStaffToAppraiseHeader" Visible="false" runat="server">
                            <div class="popup-header">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 97%;">
                                            <asp:Label ID="lbStaffToAppraiseHeader" runat="server" Text="Performance Objective Plan And Achieved for xxxx"></asp:Label>
                                            Select Quarter to Review:
                                            <asp:DropDownList ID="ddlStaffQuarterToReview" OnSelectedIndexChanged="ddlStaffQuarterToReview_SelectedIndexChanged"
                                                AutoPostBack="true" Width="150px" runat="server">
                                                <asp:ListItem Text="----" Value="0" Selected="true"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <asp:GridView ID="gvPerformancePlanObjectives" OnRowCommand="gvPerformancePlanObjectives_RowCommand"
                            runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                            AllowSorting="True" PageSize="10" EmptyDataText="No performance plan available!">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1+"." %>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("PerformancePlanID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="PerformanceObjective" HeaderText="Performance Objective" />
                                <asp:TemplateField HeaderText="Annual Key Performance Indicator">
                                    <ItemTemplate>
                                        <asp:Label ID="lblResponse" runat="server" Text='<%# "<pre>"+ Eval("KeyPerformanceIndicators")+"</pre>"  %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="PercentageWeight" HeaderStyle-HorizontalAlign="Right"
                                    ItemStyle-HorizontalAlign="Right" HeaderText="% Weight" />
                                <asp:BoundField DataField="AnnualPercentageScore" HeaderStyle-HorizontalAlign="Right"
                                    ItemStyle-HorizontalAlign="Right" HeaderText="% Score" />
                                <asp:ButtonField HeaderText="" CommandName="QuarterlyReview" Text="OBJECTIVE REVIEW" />
                            </Columns>
                            <FooterStyle CssClass="PagerStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Panel ID="PanelAddPerformanceObjective" Visible="false" runat="server">
                        <div class="popup-header">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Label ID="lbAddPerformanceObjectiveHeader" runat="server" Text="Add Performance Objective for the Year"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddPerformanceObjective" OnClick="lnkBtnClosePerformancePlan_Click"
                                            ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:Panel ID="PanelAnnualPerformanceObjectiveDetails" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        Performance Objective:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        Annual KPI/ /Target (Perfomance Indicator):<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        % Weight:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        % Score:
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        <asp:TextBox ID="txtPerformanceObjective" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtKeyPerformanceIndicator" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAnnualPercentageWeight" Width="100px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAnnualPercentageScore" Width="100px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <hr />
                        <table width="100%" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;" cellpadding="0"
                            cellspacing="0">
                            <td width="25%">
                                <div class="performance-div-header">
                                    QUARTER 1:
                                </div>
                                <asp:Panel ID="PanelQuarter1PerformanceDetails" CssClass="performance-div" runat="server">
                                    <p>
                                        Target:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1Target" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Achieved:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1Achieved" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1Rating" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Average Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1AverageRating" runat="server"></asp:TextBox></p>
                                    <p>
                                        Comments:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1Comments" runat="server"></asp:TextBox></p>
                                    <p>
                                        Action Plan for Next Quarter:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter1NextActionPlan" runat="server"></asp:TextBox></p>
                                </asp:Panel>
                                <%--   <div class="performance-div-header">
                                                QUARTER 1 APPRAISAL DETAILS
                                            </div>
                                            <asp:Panel ID="PanelQuarter1AppraiserDetails" CssClass="performance-div" runat="server">
                                                <p>
                                                    Has quarter 1 been reviewed by supervisor?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter1BeenReviewedBySupervisor" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                                <p>
                                                    Appraised By:</p>
                                                <p>
                                                    <asp:DropDownList ID="ddlQuarter1AppraisedBy" runat="server">
                                                        <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                                <p>
                                                    Appraisal Date:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter1AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonQuarter1AppraisalDate" ToolTip="Pick quarter 1 appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtQuarter1AppraisalDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter1AppraisalDate"
                                                        PopupButtonID="ImageButtonQuarter1AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtQuarter1AppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter1AppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtQuarter1AppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtQuarter1AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter1AppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtQuarter1AppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid quarter 1 appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator></p>
                                                <p>
                                                    Appraiser's Comments:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter1AppraiserComments" runat="server"></asp:TextBox></p>
                                                <p>
                                                    Has quarter 1 been reviewed by HR?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter1BeenReviewedByHR" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                            </asp:Panel>--%>
                            </td>
                            <td width="25%">
                                <div class="performance-div-header">
                                    QUARTER 2:
                                </div>
                                <asp:Panel ID="PanelQuarter2PerformanceDetails" CssClass="performance-div" runat="server">
                                    <p>
                                        Target:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2Target" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Achieved:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2Achieved" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2Rating" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Average Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2AverageRating" runat="server"></asp:TextBox></p>
                                    <p>
                                        Comments:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2Comments" runat="server"></asp:TextBox></p>
                                    <p>
                                        Action Plan for Next Quarter:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter2NextActionPlan" runat="server"></asp:TextBox></p>
                                </asp:Panel>
                                <%--  <div class="performance-div-header">
                                                QUARTER 2 APPRAISAL DETAILS
                                            </div>
                                            <asp:Panel ID="PanelQuarter2AppraiserDetails" CssClass="performance-div" runat="server">
                                                <p>
                                                    Has quarter 2 been reviewed by supervisor?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter2BeenReviewedBySupervisor" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                                <p>
                                                    Appraised By:</p>
                                                <p>
                                                    <asp:DropDownList ID="ddlQuarter2AppraisedBy" runat="server">
                                                        <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                                <p>
                                                    Appraisal Date:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter2AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonQuarter2AppraisalDate" ToolTip="Pick quarter 2 appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtQuarter2AppraisalDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter2AppraisalDate"
                                                        PopupButtonID="ImageButtonQuarter2AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtQuarter2AppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter2AppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtQuarter2AppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtQuarter2AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter2AppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtQuarter2AppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid quarter 2 appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator></p>
                                                <p>
                                                    Appraiser's Comments:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter2AppraiserComments" runat="server"></asp:TextBox></p>
                                                <p>
                                                    Has quarter 2 been reviewed by HR?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter2BeenReviewedByHR" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                            </asp:Panel>--%>
                            </td>
                            <td width="25%">
                                <div class="performance-div-header">
                                    QUARTER 3:
                                </div>
                                <asp:Panel ID="PanelQuarter3PerformanceDetails" CssClass="performance-div" runat="server">
                                    <p>
                                        Target:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3Target" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Achieved:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3Achieved" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3Rating" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Average Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3AverageRating" runat="server"></asp:TextBox></p>
                                    <p>
                                        Comments:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3Comments" runat="server"></asp:TextBox></p>
                                    <p>
                                        Action Plan for Next Quarter:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter3NextActionPlan" runat="server"></asp:TextBox></p>
                                </asp:Panel>
                                <%--  <div class="performance-div-header">
                                                QUARTER 3 APPRAISAL DETAILS
                                            </div>
                                            <asp:Panel ID="PanelQuarter3AppraiserDetails" CssClass="performance-div" runat="server">
                                                <p>
                                                    Has quarter 3 been reviewed by supervisor?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter3BeenReviewedBySupervisor" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                                <p>
                                                    Appraised By:</p>
                                                <p>
                                                    <asp:DropDownList ID="ddlQuarter3AppraisedBy" runat="server">
                                                        <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                                <p>
                                                    Appraisal Date:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter3AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonQuarter3AppraisalDate" ToolTip="Pick quarter 3 appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtQuarter3AppraisalDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter3AppraisalDate"
                                                        PopupButtonID="ImageButtonQuarter3AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtQuarter3AppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter3AppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtQuarter3AppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtQuarter3AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter3AppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtQuarter3AppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid quarter 3 appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator></p>
                                                <p>
                                                    Appraiser's Comments:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter3AppraiserComments" runat="server"></asp:TextBox></p>
                                                <p>
                                                    Has quarter 3 been reviewed by HR?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter3BeenReviewedByHR" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                            </asp:Panel>--%>
                            </td>
                            <td width="25%">
                                <div class="performance-div-header">
                                    QUARTER 4:
                                </div>
                                <asp:Panel ID="PanelQuarter4PerformanceDetails" CssClass="performance-div" runat="server">
                                    <p>
                                        Target:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4Target" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Achieved:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4Achieved" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4Rating" TextMode="MultiLine" runat="server"></asp:TextBox></p>
                                    <p>
                                        Average Rating:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4AverageRating" runat="server"></asp:TextBox></p>
                                    <p>
                                        Comments:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4Comments" runat="server"></asp:TextBox></p>
                                    <p>
                                        Action Plan for Next Quarter:</p>
                                    <p>
                                        <asp:TextBox ID="txtQuarter4NextActionPlan" runat="server"></asp:TextBox></p>
                                </asp:Panel>
                                <%-- <div class="performance-div-header">
                                                QUARTER 4 APPRAISAL DETAILS
                                            </div>
                                            <asp:Panel ID="PanelQuarter4AppraiserDetails" CssClass="performance-div" runat="server">
                                                <p>
                                                    Has quarter 4 been reviewed by supervisor?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter4BeenReviewedBySupervisor" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                                <p>
                                                    Appraised By:</p>
                                                <p>
                                                    <asp:DropDownList ID="ddlQuarter4AppraisedBy" runat="server">
                                                        <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                                <p>
                                                    Appraisal Date:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter4AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonQuarter4AppraisalDate" ToolTip="Pick quarter 4 appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtQuarter4AppraisalDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter4AppraisalDate"
                                                        PopupButtonID="ImageButtonQuarter4AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtQuarter4AppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter4AppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtQuarter4AppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtQuarter4AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter4AppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtQuarter4AppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid quarter 4 appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator></p>
                                                <p>
                                                    Appraiser's Comments:</p>
                                                <p>
                                                    <asp:TextBox ID="txtQuarter4AppraiserComments" runat="server"></asp:TextBox></p>
                                                <p>
                                                    Has quarter 4 been reviewed by HR?
                                                </p>
                                                <p>
                                                    <asp:RadioButtonList ID="rblHasQuarter4BeenReviewedByHR" RepeatDirection="Horizontal"
                                                        runat="server" RepeatLayout="Table">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </p>
                                            </asp:Panel>--%>
                            </td>
                        </table>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldPerformancePlanID" runat="server" />
                            <%--   <asp:LinkButton ID="lnkBtnSavePerformancePlan" OnClick="lnkBtnSavePerformancePlan_Click"
                                            ToolTip="Save performance details" runat="server">
                                            Save Performance Details</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClaosePerformancePlan" OnClick="lnkBtnClosePerformancePlan_Click"
                                            CausesValidation="false" ToolTip="Close" runat="server">
                                            Close</asp:LinkButton>--%>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="PanelStaffPerformancePlanQuarteryReview" Visible="false" runat="server">
                        <div class="popup-header">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Label ID="lbReviewStaffQuarterHeader" runat="server" Text="Review Staff Quarter xxxx"></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldQuarter" runat="server" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseReviewStaffQuarter" OnClick="ImageButtonCloseReviewStaffQuarter_Click"
                                            ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:GridView ID="gvStaffQuarterlyPerformanceReview" runat="server" AllowPaging="True"
                            Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                            PageSize="10" EmptyDataText="No perfromance details available!">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1+"." %>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("PerformancePlanID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="PerformanceObjective" HeaderText="Performance Objective" />
                                <asp:TemplateField HeaderText="Annual Key Performance Indicator">
                                    <ItemTemplate>
                                        <asp:Label ID="lblResponse" runat="server" Text='<%# "<pre>"+ Eval("KeyPerformanceIndicators")+"</pre>"  %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="PercentageWeight" HeaderStyle-HorizontalAlign="Right"
                                    ItemStyle-HorizontalAlign="Right" HeaderText="% Weight" />
                                <asp:BoundField DataField="QuarterTarget" HeaderText="KPI Quarter Target" HtmlEncode="False" />
                                <asp:BoundField DataField="QuarterAchieved" HeaderText="Actual Achieved" HtmlEncode="False" />
                                <asp:BoundField DataField="QuarterKPIRating" HeaderText="Rating" HtmlEncode="False" />
                                <asp:BoundField DataField="AverageObjectiveRating" HeaderText="Average Rating" HeaderStyle-HorizontalAlign="Right"
                                    ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="QuarterComment" HeaderText="Comment" />
                                <asp:BoundField DataField="NextQuarterActionPlan" HeaderText="Action Plan fo Next Quarter" />
                            </Columns>
                            <FooterStyle CssClass="PagerStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                        <table>
                            <tr>
                                <td>
                                    Overall Average Rating:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarterOverallAverageRating" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr style="vertical-align: top;">
                                <td width="50%">
                                    <div class="performance-div-header">
                                        Supervisor Appraisal Details:
                                    </div>
                                    <div class="performance-div">
                                        <table>
                                            <tr>
                                                <td>
                                                    Supervisor Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSupervisorName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Supervisor Appraisal Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterAppraisalDate" Width="180px" runat="server"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="txtQuarterAppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarterAppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtQuarterAppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtQuarterAppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarterAppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtQuarterAppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid quarter appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Supervisor Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterAppraiserComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                </td>
                <td width="50%">
                    <div class="performance-div-header">
                        HR Appraisal Details:
                    </div>
                    <div class="performance-div">
                        <table>
                            <tr>
                                <td>
                                    HR Appraisal Date:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRQuarterAppraisalDate" Width="180px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonHRQuarterAppraisalDate" ToolTip="Pick quarter date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtHRQuarterAppraisalDate_CalendarExtender" runat="server"
                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtHRQuarterAppraisalDate"
                                        PopupButtonID="ImageButtonHRQuarterAppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtHRQuarterAppraisalDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtHRQuarterAppraisalDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtHRQuarterAppraisalDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtHRQuarterAppraisalDate_MaskedEditExtender" ControlToValidate="txtHRQuarterAppraisalDate"
                                        CssClass="errorMessage" ErrorMessage="txtHRQuarterAppraisalDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid quarter appraisal date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HR Comments:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRQuarterAppraiserComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <div class="linkBtn">
                                        <asp:LinkButton ID="lnkBtnSaveQuarterAppraisalDetails" OnClick="lnkBtnSaveQuarterAppraisalDetails_Click"
                                            ToolTip="Save appraisal details" runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Appraisal Details</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                </tr> </table>
            </asp:Panel>
            </div> </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
