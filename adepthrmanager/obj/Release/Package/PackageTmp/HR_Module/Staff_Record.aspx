﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Record.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Record" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffRecord" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffRecord" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HoverMenuExtender ID="ImageStaffPhoto_HoverMenuExtender" runat="server" DynamicServicePath=""
                                    Enabled="True" TargetControlID="ImageStaffPhoto" PopupControlID="LinkButtonUploadStaffphoto"
                                    PopupPosition="Bottom">
                                </asp:HoverMenuExtender>
                                <div class="image-uploader-linkBtn">
                                    <asp:LinkButton ID="LinkButtonUploadStaffphoto" OnClick="LinkButtonUploadStaffphoto_Click"
                                        Style="display: none;" CausesValidation="false" runat="server">
                                     Upload Staff Photo</asp:LinkButton>
                                </div>
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--    <asp:TabContainer ID="TabContainerStaffRecord" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelPersonalInformation" runat="server" CssClass="ajax-tab"
                        HeaderText="Personal Information">
                        <HeaderTemplate>
                            Personal Information
                        </HeaderTemplate>
                        <ContentTemplate>--%>
                <div class="panel-details">
                    <table width="100%">
  <tr>
                            <td>
                                Employee Reference:<span class="errorMessage">*</span>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStaffReference" runat="server"></asp:TextBox>
                            </td>                       
                        </tr>
						  <tr>
                            <td>
                               Salutation/Prefix:<span class="errorMessage">*</span>
                            </td>
                            <td>
                      <asp:DropDownList ID="ddlPrefix" runat="server">
                                    <asp:ListItem Value="0">-Select Salutation-</asp:ListItem>  
                         </asp:DropDownList>                         
                         <td>
                                Last Name: <span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                First Name:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Middle Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date of Birth:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDOB" Width="200px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageButtonDOB" ToolTip="Pick date of birth" CssClass="date-image"
                                    ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                <asp:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MMM/yyyy" TargetControlID="txtDOB" PopupButtonID="ImageButtonDOB"
                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="txtDOB_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                    Mask="99/LLL/9999" TargetControlID="txtDOB" UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="txtDOB_MaskedEditValidator" runat="server" ControlExtender="txtDOB_MaskedEditExtender"
                                    ControlToValidate="txtDOB" CssClass="errorMessage" ErrorMessage="txtDOB_MaskedEditValidator"
                                    InvalidValueMessage="<br>Invalid date of birth entered" Display="Dynamic">
                                </asp:MaskedEditValidator>
                            </td>
                            <td>
                                Gender:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblGender" RepeatDirection="Horizontal" runat="server">
                                    <asp:ListItem Value="Male">Male</asp:ListItem>
                                    <asp:ListItem Value="Female">Female</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Marital Status:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMaritalStatus" runat="server">
                                    <asp:ListItem Value="0">-Select Marital Status-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Mobile Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nationality:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlNationality" runat="server">
                                    <asp:ListItem Value="0">-Select Nationality-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Passport Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassportNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Passport Expiry Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassportExpiryDate" Width="200px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageButtonPassportExpiryDate" ToolTip="Pick passport expiry date"
                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                <asp:CalendarExtender ID="txtPassportExpiryDate_CalendarExtender" runat="server"
                                    Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtPassportExpiryDate" PopupButtonID="ImageButtonPassportExpiryDate"
                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="txtPassportExpiryDate_MaskedEditExtender" runat="server"
                                    ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtPassportExpiryDate"
                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="txtPassportExpiryDate_MaskedEditValidator" runat="server"
                                    ControlExtender="txtPassportExpiryDate_MaskedEditExtender" ControlToValidate="txtPassportExpiryDate"
                                    CssClass="errorMessage" ErrorMessage="txtPassportExpiryDate_MaskedEditValidator"
                                    InvalidValueMessage="<br>Invalid passport expiry date" Display="Dynamic">
                                </asp:MaskedEditValidator>
                            </td>
                            <td>
                                National ID Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNationalID" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Diplomatic ID Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiplomaticIDNumber" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Driving Permit Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDrivingPermitNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                NSSF Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNSSFNumber" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Pin Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPinNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                TIN Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTINNumber" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Telephone Contact:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTelephoneContact" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                Work Email Address:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Personal Email Address:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAlternativeEmailAddress" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Current Address and Residence (plot/ house number/ street, city, postal code, country):
                            </td>
                            <td>
                                <asp:TextBox ID="txtCurrentAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                                Permanent Address and Residence (plot/ house number/ street, city, postal code, country):
                            </td>
                            <td>
                                <asp:TextBox ID="txtPermanentAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>                       
                        <tr>
                            <td>
                                Postal Address:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPostalAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Languages:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLanguages" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                1<sup>st</sup> Next of Kin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKinName" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                1<sup>st</sup> Next of Kin Phone Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKinPhoneNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                1<sup>st</sup> Next of Kin Relationship:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKinRelationship" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                1<sup>st</sup> Next of Kin Address:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKinAddress" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2<sup>nd</sup> Next of Kin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKin2Name" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                2<sup>nd</sup> Next of Kin Phone Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKin2PhoneNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2<sup>nd</sup> Next of Kin Relationship:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKin2Relationship" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                2<sup>nd</sup> Next of Kin Address:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNextOfKin2Address" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Details of Any Disability:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDisabilityDetails" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Referee Details 1:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRefereeDetails1" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Referee Details 2:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRefereeDetails2" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Referee Details 3:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRefereeDetails3" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <%--    <tr>
                            <td>
                                Department: <span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" runat="server">
                                    <asp:ListItem Value="0">-Select Department-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Job Title:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlJobTitle" runat="server">
                                    <asp:ListItem Value="0">-Select Job TItle-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Posted At:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPostedAt" runat="server">
                                    <asp:ListItem Value="0">-Select Posted At-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Reports To:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlReportsTo" runat="server">
                                    <asp:ListItem Value="0">-Select Reports To-</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Joining Date:<span class="errorMessage">*</span>
                            </td>
                            <td style="vertical-align: bottom;">
                                <asp:TextBox ID="txtJoinDate" Width="200px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageButtonJoinDate" ToolTip="Pick join date" CssClass="date-image"
                                    ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                <asp:CalendarExtender ID="txtJoinDate_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MMM/yyyy" TargetControlID="txtJoinDate" PopupButtonID="ImageButtonJoinDate"
                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="txtJoinDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                    Mask="99/LLL/9999" TargetControlID="txtJoinDate" UserDateFormat="DayMonthYear">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="txtJoinDate_MaskedEditValidator" runat="server" ControlExtender="txtJoinDate_MaskedEditExtender"
                                    ControlToValidate="txtJoinDate" CssClass="errorMessage" ErrorMessage="txtJoinDate_MaskedEditValidator"
                                    InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                </asp:MaskedEditValidator>
                                <br />
                                <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2014"></asp:Label>
                            </td>
                            <td>
                                Exit Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtExitDate" Width="200px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageButtonExitDate" ToolTip="Pick exit date" CssClass="date-image"
                                    ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                <asp:CalendarExtender ID="txtExitDate_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MMM/yyyy" TargetControlID="txtExitDate" PopupButtonID="ImageButtonExitDate"
                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="txtExitDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                    Mask="99/LLL/9999" TargetControlID="txtExitDate" UserDateFormat="DayMonthYear">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="txtExitDate_MaskedEditValidator" runat="server" ControlExtender="txtExitDate_MaskedEditExtender"
                                    ControlToValidate="txtExitDate" CssClass="errorMessage" ErrorMessage="txtExitDate_MaskedEditValidator"
                                    InvalidValueMessage="<br>Invalid exit date entered" Display="Dynamic">
                                </asp:MaskedEditValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                NHIF Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNHIFNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Alternative Mobile Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAlternativeMobileNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Salary:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSalary" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Emergency Contact Person:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmergencyContactPerson" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Emergency Contact Person Phone Number:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmergencyContactPersonPhoneNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>--%>
                          </table>
                          
                            <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter language competency level</legend>
                           <table>
                            <tr>
                                <td colspan="9">                                    
                         
                                    </td>
                            </tr>
                            <tr>
                                <td>
                                    Language:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLanguageName" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                <td>
                                    Speaking:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLanguageSpeaking" runat="server" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    Reading:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLanguageReading" runat="server" Width="30px"></asp:TextBox>
                                </td>
                                <td>
                                    Writing:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLanguageWriting" runat="server" Width="30px"></asp:TextBox>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnAddLanguage" runat="server" Text="Save Language" OnClick="btnAddLanguage_Click" />
                                     <asp:HiddenField ID="HiddenFieldLanguageID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                    <asp:GridView ID="gvLanguage" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10"  OnRowCommand="gvLanguage_RowCommand" EmptyDataText="No Language Details!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffLanguageID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LanguageName" HeaderText="Language" />
                                          <asp:BoundField DataField="LanguageSpeaking" HeaderText="Speaking" />
                                            <asp:BoundField DataField="LanguageReading" HeaderText="Reading" />
                                            <asp:BoundField DataField="LanguageWriting" HeaderText="Writing" />
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditLanguage" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteLanguage" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                              <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter known allergies</legend>
                           <table width="50%">
                          
                            <tr>
                                <td>
                                    Allergy Details:
                                </td>
                                 <td align="left">
                                    <asp:TextBox ID="txtAllergyName" runat="server" Width="190px"></asp:TextBox>
                                </td>                             
                                <td align="right">
                                    <asp:Button ID="btnAddAllergy" runat="server" Text="Save Allergy" OnClick="btnAddAllergy_Click" />
                                     <asp:HiddenField ID="HiddenFieldAllergyID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvAllergy" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10" OnRowCommand="gvAllergy_RowCommand" EmptyDataText="No Allergy Details!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffAllergyID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllergyDetails" HeaderText="Allergy" />
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditAllergy" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteAllergy" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                                        <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter known chronic conditions</legend>
                           <table width="50%">
                          
                            <tr>
                                <td>
                                    Chronic Condition Details:
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtChronicConditionName" runat="server" Width="190px"></asp:TextBox>
                                </td>                             
                                <td align="right">
                                    <asp:Button ID="btnAddChronicCondition" runat="server" Text="Save Chronic Condition" OnClick="btnAddChronicCondition_Click" />
                                     <asp:HiddenField ID="HiddenFieldChronicConditionID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvChronicCondition" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10" OnRowCommand="gvChronicCondition_RowCommand" EmptyDataText="No Chronic Condition Details!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffChronicConditionID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="StaffChronicConditionDetails" HeaderText="ChronicCondition" />
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditChronicCondition" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteChronicCondition" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                                                    <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter bank details</legend>
                           <table width="100%">
                            <tr>
                                <td colspan="4">                                   
                         
                                    </td>
                            </tr>
                            <tr>
							<td>
                                    Bank Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBankName" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                <td>
                                    Branch:
                                </td>
								
                                <td>
                                    <asp:TextBox ID="txtBankBranch" runat="server" Width="190px"></asp:TextBox>
                                </td>
								</tr>
                            <tr>
                                <td>
                                   Account Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAccountName" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                <td>
                                    Account Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAccountNumber" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                </tr>
                            <tr>
							<td>
                                    Currency Percentage:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCurrencyPercentage" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnAddBankDetails" runat="server" Text="Save Bank Details" OnClick="btnAddBankDetails_Click" />
                                     <asp:HiddenField ID="HiddenFieldBankDetailID" runat="server" />

                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="gvBankDetails" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10" OnRowCommand="gvBankDetails_RowCommand" EmptyDataText="No Bank Details!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffBankDetailsID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BankName" HeaderText="Bank Name" />
                                          <asp:BoundField DataField="BankBranch" HeaderText="Bank Branch" />
                                            <asp:BoundField DataField="AccountName" HeaderText="Account Name" />
                                            <asp:BoundField DataField="AccountNumber" HeaderText="Account Number" />
											  <asp:BoundField DataField="CurrencyPercentage" HeaderText="Currency Percentage" />
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditBankDetails" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteBankDetails" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>

                   

                    <asp:Panel ID="PanelStaffAdditionalFields" runat="server">
                        <hr />
                        <b>Additional Information</b>
                        <hr />
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lbHRDescription01" Visible="false" runat="server" Text="HR Description 01:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription01" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription01" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lbHRDescription06" Visible="false" runat="server" Text="HR Description 06:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription06" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription06" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbHRDescription02" Visible="false" runat="server" Text="HR Description 02:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription02" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription02" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lbHRDescription07" Visible="false" runat="server" Text="HR Description 07:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription07" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription07" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbHRDescription03" Visible="false" runat="server" Text="HR Description 03:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription03" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription03" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lbHRDescription08" Visible="false" runat="server" Text="HR Description 08:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription08" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription08" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbHRDescription04" Visible="false" runat="server" Text="HR Description 04:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription04" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription04" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lbHRDescription09" Visible="false" runat="server" Text="HR Description 09:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription09" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription09" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbHRDescription05" Visible="false" runat="server" Text="HR Description 05:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription05" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription05" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lbHRDescription10" Visible="false" runat="server" Text="HR Description 10:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHRDescription10" Visible="false" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="HiddenFieldHRDescription10" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Remarks/Comments:
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtStaffRecordRemarks" Width="775px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Label ID="lbRequired" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                    <div class="linkBtn" style="text-align: right;">
                        <asp:Label ID="lbError" runat="server" CssClass="errorMessage" Text=""></asp:Label>
                        <asp:Label ID="lbInfo" runat="server" CssClass="info-message" Text=""></asp:Label>
                        <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                        <asp:LinkButton ID="lnkBtnSaveStaffRecord" OnClick="lnkBtnSaveStaffRecord_Click"
                            ToolTip="Save Staff Details" runat="server">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                            Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnUpdateStaffRecord" OnClick="lnkBtnUpdateStaffRecord_Click"
                            CausesValidation="false" ToolTip="Update Staff Details" Enabled="false" runat="server">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                ImageAlign="AbsMiddle" />
                            Update</asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnClearStaffRecord" OnClick="lnkBtnClearStaffRecord_Click"
                            CausesValidation="false" ToolTip="Clear" runat="server">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                ImageAlign="AbsMiddle" />
                            Clear</asp:LinkButton>
                    </div>
                </div>
                <%--</ContentTemplate>
                    </asp:TabPanel>--%>
                <%--<asp:TabPanel ID="TabPanelWorkExperience" runat="server" CssClass="ajax-tab" HeaderText="Work Experience">
                        <HeaderTemplate>
                            Work Experience
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewWorkExperience" OnClick="LinkButtonNewWorkExperience_Click"
                                    ToolTip="Add new work experience record" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditWorkExperience" OnClick="LinkButtonEditWorkExperience_Click"
                                    ToolTip="Edit work experience details" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditWorkExperience" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteWorkExperience" OnClick="LinkButtonDeleteWorkExperience_Click"
                                    CausesValidation="false" ToolTip="Delete work experience" runat="server">
                                    <asp:Image ID="ImageDeleteWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteWorkExperience" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvEmploymentHistory" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    OnLoad="gvEmploymentHistory_OnLoad" OnPageIndexChanging="gvEmploymentHistory_PageIndexChanging"
                                    EmptyDataText="No employement history available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffWorkExperienceID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                                        <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                                        <asp:BoundField DataField="EmployerName" HeaderText="Employer's Name" />
                                        <asp:BoundField DataField="PositionHeld" HeaderText="Position/Title Held" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddWorkExperience" Style="display: none; width: 550px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddWorkExperience" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddWorkExperienceHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddWorkExperienceHeader" runat="server" Text="Add Work Experience"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddWorkExperience" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Work Experience Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Employer Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtWorkExperienceEmployerName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Position/Title Held:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtWorkExperiencePositionHeld" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Start Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlWorkExperienceStartDateMonth" AppendDataBoundItems="true"
                                                        Width="95px" runat="server">
                                                        <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlWorkExperienceStartDateYear" Width="80px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    End Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlWorkExperienceEndDateMonth" Width="95px" runat="server">
                                                        <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlWorkExperienceEndDateYear" Width="80px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label1" CssClass="errorMessage" runat="server" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbWorkExperienceError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lbWorkExperienceInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldWorkExperienceID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddWorkExperiencePopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveWorkExperience" OnClick="lnkBtnSaveWorkExperience_Click"
                                            ToolTip="Save work experience" runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateWorkExperience" OnClick="lnkBtnUpdateWorkExperience_Click"
                                            CausesValidation="false" ToolTip="Update work experience" Enabled="false" runat="server">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearWorkExperience" OnClick="lnkBtnClearWorkExperience_Click"
                                            CausesValidation="false" ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddWorkExperience" RepositionMode="None"
                                X="270" Y="100" TargetControlID="HiddenFieldAddWorkExperiencePopup" PopupControlID="PanelAddWorkExperience"
                                CancelControlID="ImageButtonCloseAddWorkExperience" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                            <asp:DragPanelExtender ID="DragPanelExtenderAddWorkExperience" TargetControlID="PanelAddWorkExperience"
                                DragHandleID="PanelDragAddWorkExperience" runat="server"></asp:DragPanelExtender>
                        </ContentTemplate>
                    </asp:TabPanel>--%>
                <%-- <asp:TabPanel ID="TabPanelEducationBackground" runat="server" CssClass="ajax-tab"
                        HeaderText="Education Background">
                        <HeaderTemplate>
                            Education Background
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewEducationBackground" OnClick="LinkButtonNewEducationBackground_Click"
                                    ToolTip="Add education background record" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewEducationBackground" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewEducatoinBackground" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditEducationBackground" OnClick="LinkButtonEditEducationBackground_Click"
                                    ToolTip="Edit education background details" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditEducationBackground" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabeEditEducationBackground" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteEducation" OnClick="LinkButtonDeleteEducationBackground_Click"
                                    CausesValidation="false" ToolTip="Delete education background" runat="server">
                                    <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteEducationBackground" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvEducationBackground" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    EmptyDataText="No education background available!" OnPageIndexChanging="gvEducationBackground_PageIndexChanging"
                                    OnLoad="gvEducationBackground_OnLoad">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffEducationBackgroundID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="EducationQualificationType" HeaderText="Qualification" />
                                        <asp:BoundField DataField="InstitutionName" HeaderText="Institution" />
                                        <asp:BoundField DataField="ProgramCourse" HeaderText="Program/Course" />
                                        <asp:BoundField DataField="QualificationStatus" HeaderText="Status" />
                                        <asp:BoundField DataField="CompletionDate" HeaderText="CompletionDate" />
                                        <asp:BoundField DataField="GradeAttained" HeaderText="Grade/GPA" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddEducationBackground" Style="display: none; width: 620px;"
                                runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddEducationBackground" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddEducationBackgroundHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddEducationBackgroundHeader" runat="server" Text="Add Education Background"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddEducationBackground" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Education Background Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Qualification Type:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblEducationBackgroundQualificationType" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem Value="Academic" Selected="True">Academic</asp:ListItem>
                                                        <asp:ListItem Value="Professional">Professional</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Institution Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEducationBackgroundInstitutionName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Level Attained:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEducationBackgroundLevelAttained" runat="server">
                                                        <asp:ListItem Text="-Select Attained Level-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Programme/Course:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEducationBackgroundCourse" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Status:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblEducationBackgroundStatus" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem Value="Completed" Selected="True">Completed</asp:ListItem>
                                                        <asp:ListItem Value="Ongoing">Ongoing</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Completion Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEducationBackgroundCompletionDateMonth" Width="95px" runat="server">
                                                        <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlEducationBackgroundCompletionDateYear" Width="80px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Grade/GPA:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEducationBackgroundGrade" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label3" CssClass="errorMessage" runat="server" Text="Fields marked with asterisk(*) are required."></asp:Label><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbEducationBackgroundError" CssClass="errorMessage" runat="server"
                                                        Text=""></asp:Label>
                                                    <asp:Label ID="lbEducationBackgroundInfo" CssClass="info-message" runat="server"
                                                        Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldEducationBackgroundID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddEducationBackgroundPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveEducationBackground" OnClick="lnkBtnSaveEducationBackground_Click"
                                            ToolTip="Save education background" runat="server">
                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateEducationBackground" OnClick="lnkBtnUpdateEducationBackground_Click"
                                            CausesValidation="false" ToolTip="Update education background" Enabled="false"
                                            runat="server">
                                            <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearEducationBackground" OnClick="lnkBtnClearEducationBackground_Click"
                                            CausesValidation="false" ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image11" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddEducationBackground" RepositionMode="None"
                                X="270" Y="30" TargetControlID="HiddenFieldAddEducationBackgroundPopup" PopupControlID="PanelAddEducationBackground"
                                CancelControlID="ImageButtonCloseAddEducationBackground" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                            <asp:DragPanelExtender ID="DragPanelExtenderAddEducationBackground" TargetControlID="PanelAddEducationBackground"
                                DragHandleID="PanelDragAddEducationBackground" runat="server"></asp:DragPanelExtender>
                        </ContentTemplate>
                    </asp:TabPanel>--%>
                <%--    <asp:TabPanel ID="TabPanelFamilyDetails" runat="server" CssClass="ajax-tab" HeaderText="Family Details">
                        <HeaderTemplate>
                            Family Details
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <fieldset>
                                    <legend>Spouse Details </legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Spouse Name:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseName" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Date of Birth:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseDateOfBirth" Width="150px" Enabled="false" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonSpouseDateOfBirth" ToolTip="Pick spouse date of birth"
                                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtSpouseDateOfBirth_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MMM/yyyy" TargetControlID="txtSpouseDateOfBirth" PopupButtonID="ImageButtonSpouseDateOfBirth"
                                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtSpouseDateOfBirth_MaskedEditExtender" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtSpouseDateOfBirth"
                                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtSpouseDateOfBirth_MaskedEditValidator" runat="server"
                                                    ControlExtender="txtSpouseDateOfBirth_MaskedEditExtender" ControlToValidate="txtSpouseDateOfBirth"
                                                    CssClass="errorMessage" ErrorMessage="txtSpouseDateOfBirth_MaskedEditValidator"
                                                    InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>
                                                Employed At:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseEmployedAt" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Employment Details:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseEmploymentDetails" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Telephone Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseTelephoneNumber" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                E-Mail Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSpouseEmailID" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="HiddenFieldSpouseID" runat="server" />
                                                <asp:Button ID="btnSaveSpouse" OnClick="btnSaveSpouse_OnClick" runat="server" Text="Save" />
                                                <asp:Button ID="btnUpdateSpouse" OnClick="btnUpdateSpouse_OnClick" runat="server"
                                                    Text="Update" Enabled="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:GridView ID="gvSpouseDetails" OnRowCommand="gvSpouseDetails_RowCommand" runat="server"
                                                    AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                                    AllowSorting="True" PageSize="5" EmptyDataText="No spouse details available!">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1+"." %>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("spouse_iSpouseID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="4px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="spouse_vSpouseName" HeaderText="Spouse Name" />
                                                        <asp:BoundField DataField="spouse_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                                        <asp:BoundField DataField="spouse_vEmployedAt" HeaderText="Employed At" />
                                                        <asp:BoundField DataField="spouse_vEmploymentDetails" HeaderText="Employment Details" />
                                                        <asp:BoundField DataField="spouse_vPhoneNumber" HeaderText="Phone No:" />
                                                        <asp:BoundField DataField="spouse_vEmailID" HeaderText="Email" />
                                                        <asp:ButtonField HeaderText="Edit" CommandName="EditSpouse" ItemStyle-Width="4px"
                                                            Text="Edit" />
                                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteSpouse" ItemStyle-Width="4px"
                                                            Text="Delete" />
                                                    </Columns>
                                                    <FooterStyle CssClass="PagerStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend>Spouse Details </legend>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                Child Name:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtChildName" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Child Date of Birth:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtChildDOB" Width="150px" Enabled="false" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonChildDOB" ToolTip="Pick child date of birth" CssClass="date-image"
                                                    ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtChildDOB_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MMM/yyyy" TargetControlID="txtChildDOB" PopupButtonID="ImageButtonChildDOB"
                                                    PopupPosition="TopRight"></asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtChildDOB_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                                    Mask="99/LLL/9999" TargetControlID="txtChildDOB" UserDateFormat="DayMonthYear">
                                                </asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtChildDOB_MaskedEditValidator" runat="server" ControlExtender="txtChildDOB_MaskedEditExtender"
                                                    ControlToValidate="txtSpouseDateOfBirth" CssClass="errorMessage" ErrorMessage="txtChildDOB_MaskedEditValidator"
                                                    InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="HiddenFieldChildID" runat="server" />
                                                <asp:Button ID="btnSaveChild" OnClick="btnSaveChild_OnClick" runat="server" Text="Save" />
                                                <asp:Button ID="btnUpdateChild" OnClick="btnUpdateChild_OnClick" runat="server" Text="Update"
                                                    Enabled="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:GridView ID="gvChildDetails" OnRowCommand="gvChildDetails_RowCommand" runat="server"
                                                    AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                                    AllowSorting="True" PageSize="10" EmptyDataText="No child details available!">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1+"." %>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("child_iChildID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="4px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="child_vName" HeaderText="Child Name" />
                                                        <asp:BoundField DataField="child_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                                        <asp:ButtonField HeaderText="Edit" CommandName="EditChild" ItemStyle-Width="4px"
                                                            Text="Edit" />
                                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteChild" ItemStyle-Width="4px"
                                                            Text="Delete" />
                                                    </Columns>
                                                    <FooterStyle CssClass="PagerStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>--%>
                <%--<asp:TabPanel ID="TabPanelAttachedDocuments" runat="server" CssClass="ajax-tab" HeaderText="Attached Documents">
                        <HeaderTemplate>
                            Attached Documents
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table>
                                    <tr>
                                        <td>
                                            Document Title:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStaffDocumentTitle" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Select Document:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="FileUploadDocument" ToolTip="Browse for a document" runat="server" />
                                            <br />
                                            <asp:Label ID="lbD" CssClass="doc-info" runat="server" Text="File types allowed: Word and PDF(i.e .doc,.docx,.rtf & .pdf).<br>Maximum file size: 3mb."></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lbDocumentsError" runat="server" CssClass="errorMessage" Text=""></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUploadDocument" OnClick="btnUploadDocument_OnClick" runat="server"
                                                Text="Upload Document" ToolTip="Click to upload the docuement" />
                                            <asp:HiddenField ID="HiddenFieldDocumentID" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvStaffDocuments" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" OnRowCommand="gvStaffDocuments_RowCommand"
                                    AllowSorting="True" PageSize="10" EmptyDataText="No documents available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("staffdoc_iStaffDocID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="staffdoc_vDocTitle" HeaderText="Document Title" />
                                        <asp:ButtonField DataTextField="staffdoc_vDocName" HeaderText="Document Name" CommandName="ViewDocument" />
                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteDocument" ItemStyle-Width="4px"
                                            Text="Delete" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>--%>
                <%--<asp:TabPanel ID="TabPanelEmploymentTerms" runat="server" CssClass="ajax-tab" HeaderText="Employment Terms">
                        <HeaderTemplate>
                            Employment Terms
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <fieldset class="fieldsetDetails">
                                    <legend class="legendDetails">Employment Terms Details</legend>
                                    <table width="100%">
                                        <tr valign="top">
                                            <td>
                                                <asp:HiddenField ID="HiddenFieldEmploymentTermsID" runat="server" />
                                                <asp:HiddenField ID="HiddenFieldEmploymentTermsPerksDetailID" runat="server" />
                                                Date of Join:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsDateOfJoin" Enabled="false" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Date Until:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsDateUntil" Enabled="false" CssClass="textBoxDetails"
                                                    runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonEmploymentTermsDateUntil" ToolTip="Pick date until"
                                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtEmploymentTermsDateUntil_CalendarExtender" runat="server"
                                                    Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEmploymentTermsDateUntil"
                                                    PopupButtonID="ImageButtonEmploymentTermsDateUntil" PopupPosition="TopRight">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtEmploymentTermsDateUntil_MaskedEditExtender" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEmploymentTermsDateUntil"
                                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtEmploymentTermsDateUntil_MaskedEditValidator" runat="server"
                                                    ControlExtender="txtEmploymentTermsDateUntil_MaskedEditExtender" ControlToValidate="txtEmploymentTermsDateUntil"
                                                    CssClass="errorMessage" ErrorMessage="txtEmploymentTermsDateUntil_MaskedEditValidator"
                                                    InvalidValueMessage="<br>Invalid until date entered" Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Type of Employment:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmploymentTermsTypeOfEmployment" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                Basic Pay:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsBasicPay" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Notice Period:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsNoticePeriod" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Liquidated Damage:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsLiquidatedDamages" runat="server"></asp:TextBox>
                                            </td>
                                            <tr>
                                                <td>
                                                    Medical Scheme:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEmploymentTermsMedicalScheme" CssClass="drpDown" runat="server">
                                                        <asp:ListItem Value="0">Select Medical Scheme</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSaveEmployementTerms" OnClick="btnSaveEmployementTerms_Click"
                                                        runat="server" Text="Save Employment Terms" ToolTip="Save employment terms" />
                                                </td>
                                            </tr>
                                        </tr>
                                    </table>
                                </fieldset>
                                <asp:Panel ID="PanelSpouseToBenefit" Visible="false" runat="server">
                                    <fieldset>
                                        <legend>Spouse to Benefit </legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvSpouseToBenefit" runat="server" CssClass="GridViewStyle" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="SNo" HeaderText="#" ItemStyle-Width="4px" />
                                                            <asp:BoundField DataField="SpouseName" HeaderText="Spouse Name" />
                                                            <asp:BoundField DataField="SpouseDOB" HeaderText="Date Of Birth" />
                                                            <asp:BoundField DataField="PhoneNumber" HeaderText="Phone Number" />
                                                            <asp:TemplateField HeaderText="Is a Benefeciary?" ItemStyle-Width="4px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Checked") %>' />
                                                                    <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="PagerStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btnSaveSpouseToBenefit" OnClick="btnSaveSpouseToBenefit_OnClick"
                                                        runat="server" Text="Save Spouse To Benefit" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </asp:Panel>
                                <asp:Panel ID="PanelChildToBenefit" Visible="false" runat="server">
                                    <fieldset>
                                        <legend>Child to Benefit </legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvChildToBenefit" CssClass="GridViewStyle" runat="server" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="SNo" HeaderText="#" ItemStyle-Width="4px" />
                                                            <asp:BoundField DataField="ChildName" HeaderText="Child Name" />
                                                            <asp:BoundField DataField="ChildDOB" HeaderText="Date Of Birth" />
                                                            <asp:TemplateField HeaderText="Is a Beneficiary?" ItemStyle-Width="4px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Checked") %>' />
                                                                    <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="PagerStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btnSaveChildToBenefit" OnClick="btnSaveChildToBenefit_OnClick" runat="server"
                                                        Text="Save Child To Benefit" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </asp:Panel>
                                <fieldset class="fieldsetDetails">
                                    <legend class="legendDetails">Employment Allowances</legend>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                Perk:
                                            </td>
                                            <td>
                                                Perk Amount:
                                            </td>
                                            <td>
                                                Effective From:
                                            </td>
                                            <td>
                                                Effective Until:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlEmploymentTermsPerkName" runat="server">
                                                    <asp:ListItem Value="0">Select Perk</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsPerkAmount" Width="190px" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsEffectiveFrom" Enabled="false" Width="190px" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonEmploymentTermsEffectiveFrom" ToolTip="Pick perk effective"
                                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtEmploymentTermsEffectiveFrom_CalendarExtender" runat="server"
                                                    Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEmploymentTermsEffectiveFrom"
                                                    PopupButtonID="ImageButtonEmploymentTermsEffectiveFrom" PopupPosition="TopRight">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtEmploymentTermsEffectiveFrom_MaskedEditExtender" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEmploymentTermsEffectiveFrom"
                                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtEmploymentTermsEffectiveFrom_MaskedEditValidator"
                                                    runat="server" ControlExtender="txtEmploymentTermsEffectiveFrom_MaskedEditExtender"
                                                    ControlToValidate="txtEmploymentTermsEffectiveFrom" CssClass="errorMessage" ErrorMessage="txtEmploymentTermsEffectiveFrom_MaskedEditValidator"
                                                    InvalidValueMessage="<br>Invalid effective date entered" Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmploymentTermsEffectiveUntil" Enabled="false" Width="190px"
                                                    runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonEmploymentTermsEffectiveUntil" ToolTip="Pick perk effective until date"
                                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtEmploymentTermsEffectiveUntil_CalendarExtender" runat="server"
                                                    Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEmploymentTermsEffectiveUntil"
                                                    PopupButtonID="ImageButtonEmploymentTermsEffectiveUntil" PopupPosition="TopRight">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtEmploymentTermsEffectiveUntil_MaskedEditExtender"
                                                    runat="server" ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEmploymentTermsEffectiveUntil"
                                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtEmploymentTermsEffectiveUntil_MaskedEditValidator"
                                                    runat="server" ControlExtender="txtEmploymentTermsEffectiveUntil_MaskedEditExtender"
                                                    ControlToValidate="txtEmploymentTermsEffectiveUntil" CssClass="errorMessage"
                                                    ErrorMessage="txtEmploymentTermsEffectiveUntil_MaskedEditValidator" InvalidValueMessage="<br>Invalid effective untill date entered"
                                                    Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                            <td align="right">
                                                <asp:Button ID="btnEmploymentTermsAddPerk" OnClick="btnEmploymentTermsAddPerk_Click"
                                                    runat="server" Text="Add Perk" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:GridView ID="gvEmploymentTermsPerksDetails" runat="server" AllowPaging="True"
                                                    Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" OnRowCommand="gvEmploymentTermsPerksDetails_RowCommand"
                                                    AllowSorting="True" PageSize="10" EmptyDataText="No perk details!">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1+"." %>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("emptermsperkdetail_iEmpTermsPerkDetailID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="4px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="4px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="emptermsperkdetail_vPerk" HeaderText="Perk/Allowance" />
                                                        <asp:BoundField DataField="emptermsperkdetail_mPerkAmount" HeaderText="Amount" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="emptermsperkdetail_dtPerkDateFrom" HeaderText="Effective From"
                                                            DataFormatString="{0:dd/MMM/yyyy}" />
                                                        <asp:BoundField DataField="emptermsperkdetail_dtPerkDateUntil" HeaderText="Effective Until"
                                                            DataFormatString="{0:dd/MMM/yyyy}" />
                                                        <asp:ButtonField HeaderText="Edit" CommandName="EditPerkDetail" ItemStyle-Width="4px"
                                                            Text="Edit" />
                                                        <asp:ButtonField HeaderText="Delete" CommandName="DeletePerkDetail" ItemStyle-Width="4px"
                                                            Text="Delete" />
                                                    </Columns>
                                                    <FooterStyle CssClass="PagerStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>--%>
                <%--    </asp:TabContainer>--%>
                <asp:Panel ID="PanelUploadStaffImage" Style="display: none; width: 670px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragUploadStaffImage" runat="server">
                            <table>
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageUploadStaffImageHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAUploadStaffImageHeader" runat="server" Text="Select Staff Image, Upload, Crop & Save"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseUploadStaffImage" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <table width="100%">
                            <tr>
                                <td>
                                    <b>Select Staff Image File :</b>
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUploadStaffPhoto" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <div class="div-image-upload">
                            <asp:Panel ID="panCrop" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="staff-image-upload">
                                                <asp:Image ID="imgUpload" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <%-- Hidden field for store cror area --%>
                                            <asp:HiddenField ID="hfX" runat="server" />
                                            <asp:HiddenField ID="hfY" runat="server" />
                                            <asp:HiddenField ID="hfW" runat="server" />
                                            <asp:HiddenField ID="hfH" runat="server" />
                                            <asp:Button ID="btnCrop" Visible="false" runat="server" Text="Crop & Save" OnClick="btnCrop_Click" />
                                            <asp:HiddenField ID="HiddenFieldUploadStaffImagePopup" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderUploadStaffImage" RepositionMode="None"
                    X="170" Y="2" TargetControlID="HiddenFieldUploadStaffImagePopup" PopupControlID="PanelUploadStaffImage"
                    CancelControlID="ImageButtonCloseUploadStaffImage" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderUploadStaffImage" TargetControlID="PanelUploadStaffImage"
                    DragHandleID="PanelDragUploadStaffImage" runat="server"></asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeleteBank" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeleteAllergy" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeleteChronicConditions" runat="server" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="TabContainerStaffRecord$TabPanelAttachedDocuments$btnUploadDocument" />--%>
            <asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnCrop" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
    <link href="../Scripts/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/jscript"></script>
    <script src="../Scripts/jquery.Jcrop.js" type="text/jscript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#<%=imgUpload.ClientID%>').Jcrop({
                onSelect: SelectCropArea
            });
        });
        function SelectCropArea(c) {
            $('#<%=hfX.ClientID%>').val(parseInt(c.x));
            $('#<%=hfY.ClientID%>').val(parseInt(c.y));
            $('#<%=hfW.ClientID%>').val(parseInt(c.w));
            $('#<%=hfH.ClientID%>').val(parseInt(c.h));
        }</script>
</asp:Content>
