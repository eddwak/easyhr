﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Test_ImageUploder.aspx.cs" Inherits="AdeptHRManager.HR_Module.Test_ImageUploder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
    <link href="../Scripts/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/jscript"></script>
    <script src="../Scripts/jquery.Jcrop.js" type="text/jscript"></script>
    <script language="javascript">
        $(document).ready(function () {
            $('#<%=imgUpload.ClientID%>').Jcrop({
                onSelect: SelectCropArea
            });
        });
        function SelectCropArea(c) {
            $('#<%=hfX.ClientID%>').val(parseInt(c.x));
            $('#<%=hfY.ClientID%>').val(parseInt(c.y));
            $('#<%=hfW.ClientID%>').val(parseInt(c.w));
            $('#<%=hfH.ClientID%>').val(parseInt(c.h));
        }</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:Panel ID="PanelImageUploader" runat="server">
        <asp:HyperLink ID="HyperLink1" runat="server">Load Upload Image</asp:HyperLink>
        <asp:Panel ID="PanelUploadStaffImage" Style="display: none; width: 670px;" runat="server">
            <div class="popup-header">
                <asp:Panel ID="PanelDragUploadStaffImage" runat="server">
                    <table>
                        <tr>
                            <td style="width: 97%;">
                                <asp:Image ID="ImageUploadStaffImageHeader" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Images/icons/Medium/Create.png" />
                                <asp:Label ID="lbAUploadStaffImageHeader" runat="server" Text="Selct Staff Image, Upload, Crop & Save"></asp:Label>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImageButtonCloseUploadStaffImage" ImageUrl="~/images/icons/small/Remove.png"
                                    ToolTip="Close" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div class="popup-details">
                <table width="100%">
                    <tr>
                        <td>
                            <b>Select Staff Image File :</b>
                        </td>
                        <td>
                            <asp:FileUpload ID="FU1" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
                        </td>
                    </tr>
                </table>
                <hr />
                <div class="div-image-upload">
                    <asp:Panel ID="panCrop" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div class="staff-image-upload">
                                        <asp:Image ID="imgUpload" CssClass="" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <%-- Hidden field for store cror area --%>
                                    <asp:HiddenField ID="hfX" runat="server" />
                                    <asp:HiddenField ID="hfY" runat="server" />
                                    <asp:HiddenField ID="hfW" runat="server" />
                                    <asp:HiddenField ID="hfH" runat="server" />
                                    <asp:Button ID="btnCrop" Visible="false" runat="server" Text="Crop & Save" OnClick="btnCrop_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </div>
        </asp:Panel>
        <cc1:ModalPopupExtender ID="ModalPopupExtenderUploadStaffImage" RepositionMode="None"
            X="170" Y="2" TargetControlID="HyperLink1" PopupControlID="PanelUploadStaffImage"
            CancelControlID="ImageButtonCloseUploadStaffImage" BackgroundCssClass="modalback"
            runat="server">
        </cc1:ModalPopupExtender>
        <cc1:DragPanelExtender ID="DragPanelExtenderUploadStaffImage" TargetControlID="PanelUploadStaffImage"
            DragHandleID="PanelDragUploadStaffImage" runat="server">
        </cc1:DragPanelExtender>
    </asp:Panel>
</asp:Content>
