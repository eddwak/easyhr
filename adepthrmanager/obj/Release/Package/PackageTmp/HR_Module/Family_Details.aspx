﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Family_Details.aspx.cs" Inherits="AdeptHRManager.HR_Module.Family_Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelFamilyDetails" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelFamilyDetails" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_OnClick" ToolTip="Add"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_OnClick" ToolTip="Edit"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabeEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_OnClick" CausesValidation="false"
                        ToolTip="Delete" runat="server">
                        <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                    <asp:Label ID="LabelDeleteEducationBackground" runat="server" Text="|"></asp:Label>
                </div>
                <asp:TabContainer ID="TabContainerFamilyDetails" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelSpouseDetails" runat="server" CssClass="ajax-tab" HeaderText="Spouse Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvSpouseDetails" OnRowCommand="gvSpouseDetails_RowCommand" runat="server"
                                    AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                    AllowSorting="True" PageSize="5" EmptyDataText="No spouse details available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("spouse_iSpouseID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="spouse_vFirstName" HeaderText="First Name" />
                                         <asp:BoundField DataField="spouse_vSurname" HeaderText="Surname" />
                                         <asp:BoundField DataField="spouse_vCountry" HeaderText="Country" />
                                         <asp:BoundField DataField="spouse_vNationality" HeaderText="Nationality" />
                                        <asp:BoundField DataField="spouse_vPassNo" HeaderText="Pass No." />
                                        <asp:BoundField DataField="spouse_vPassExpiryDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Pass Expiry Date" />
                                        <asp:BoundField DataField="spouse_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                        <asp:BoundField DataField="spouse_vEmployedAt" HeaderText="Employed At" />
                                        <asp:BoundField DataField="spouse_vEmploymentDetails" HeaderText="Employment Details" visible="false" />
                                        <asp:BoundField DataField="spouse_vPhoneNumber" HeaderText="Phone No:" />
                                        <asp:BoundField DataField="spouse_vEmailID" HeaderText="Email" />
                                        <asp:BoundField DataField="spouse_vPostalAddress" HeaderText="Postal Address" />
                                        <asp:BoundField DataField="spouse_vMariageCerticateProvided" HeaderText="Marriage Certificate Provided?" />
                                        <asp:BoundField DataField="spouse_vComments" HeaderText="Comments" />
                                        <%--   <asp:ButtonField HeaderText="Edit" CommandName="EditSpouse" ItemStyle-Width="4px"
                                            Text="Edit" />
                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteSpouse" ItemStyle-Width="4px"
                                            Text="Delete" />--%>
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddSpouseDetails" Style="display: none; width: 950px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddSpouseDetails" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddSpouseDetailsHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddSpouseDetailsHeader" runat="server" Text="Add Spouse"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddSpouseDetails" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Spouse Details</legend>
                                        <table>
                                         <tr valign="top">
                                                <td>
                                                    First Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Surname:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                   <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                             <tr valign="top">
                                                <td>
                                                    Country:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Nationality:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                   <asp:TextBox ID="txtNationality" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                             <tr valign="top">
                                                <td>
                                                    Pass No:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassNo" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Pass Expiry Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                   <asp:TextBox ID="txtPassExpiryDate"  Width="150px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonPassExpiryDate" ToolTip="Pick Pass Expiry Date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtPassExpiryDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtPassExpiryDate" PopupButtonID="ImageButtonPassExpiryDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtPassExpiryDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtPassExpiryDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtPassExpiryDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtPassExpiryDate_MaskedEditExtender" ControlToValidate="txtPassExpiryDate"
                                                        CssClass="errorMessage" ErrorMessage="txtPassExpiryDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                  <td>
                                                    Is marriage certificate provided?<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblSpouseMarriageCertificateProvided" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem Value="true">Yes</asp:ListItem>
                                                        <asp:ListItem Value="false">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    Date of Birth:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSpouseDateOfBirth" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonSpouseDateOfBirth" ToolTip="Pick spouse date of birth"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtSpouseDateOfBirth_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtSpouseDateOfBirth" PopupButtonID="ImageButtonSpouseDateOfBirth"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtSpouseDateOfBirth_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtSpouseDateOfBirth"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtSpouseDateOfBirth_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtSpouseDateOfBirth_MaskedEditExtender" ControlToValidate="txtSpouseDateOfBirth"
                                                        CssClass="errorMessage" ErrorMessage="txtSpouseDateOfBirth_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                    Employed At:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSpouseEmployedAt" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Telephone Number:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                   <asp:TextBox ID="txtSpouseTelephoneNumber" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    E-Mail Address:
                                                </td>
                                                <td>
                                                   <asp:TextBox ID="txtSpouseEmailID" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Postal Address:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSpousePostalAddress" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>                                              
                                                <td>
                                                    Comments:
                                                </td>
                                                <td colspan="3">
                                                   <asp:TextBox ID="txtSpouseComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtSpouseEmploymentDetails" runat="server" visible="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldSpouseID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddSpouseDetailsPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveSpouse" OnClick="btnSaveSpouse_OnClick" ToolTip="Save spouse details"
                                            runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateSpouse" OnClick="btnUpdateSpouse_OnClick" CausesValidation="false"
                                            ToolTip="Update spouse details" Enabled="false" runat="server">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearSpouse" OnClick="lnkBtnClearSpouse_OnClick" CausesValidation="false"
                                            ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddSpouseDetails" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddSpouseDetailsPopup" PopupControlID="PanelAddSpouseDetails"
                                CancelControlID="ImageButtonCloseAddSpouseDetails" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelChildDetails" runat="server" CssClass="ajax-tab" HeaderText="Children">
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvChildDetails" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    EmptyDataText="No child details available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("child_iChildID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="child_vFirstName" HeaderText="First Name" />
                                        <asp:BoundField DataField="child_vSurname" HeaderText="Surname" />
                                        <asp:BoundField DataField="child_vCountryOfResidence" HeaderText="Country of Residence" />
                                        <asp:BoundField DataField="child_vNationality" HeaderText="Nationality" />
                                        <asp:BoundField DataField="child_vPassNo" HeaderText="Pass No" />
                                        <asp:BoundField DataField="child_vPassExpiryDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Pass Expiry Date" />
                                        <asp:BoundField DataField="child_vGender" HeaderText="Gender" />
                                        <asp:BoundField DataField="child_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                        <asp:BoundField DataField="child_vAge" HeaderText="Age" />                                       
                                        <asp:BoundField DataField="child_vBirthCertificateProvided" HeaderText="Birth / Adoption Certificate Provided?" />
                                        <%--  <asp:ButtonField HeaderText="Edit" CommandName="EditChild" ItemStyle-Width="4px"
                                            Text="Edit" />
                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteChild" ItemStyle-Width="4px"
                                            Text="Delete" />--%>
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddChildDetails" Style="display: none; width: 600px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddChildDetails" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddChildDetailsHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddChildDetailsHeader" runat="server" Text="Add Child"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddChildDetails" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Child Details</legend>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    First Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildFirstName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Surname:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildSurname" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                    Child Date of Birth:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildDOB" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonChildDOB" ToolTip="Pick child date of birth" CssClass="date-image"
                                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtChildDOB_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtChildDOB" PopupButtonID="ImageButtonChildDOB"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtChildDOB_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                                        Mask="99/LLL/9999" TargetControlID="txtChildDOB" UserDateFormat="DayMonthYear">
                                                    </asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtChildDOB_MaskedEditValidator" runat="server" ControlExtender="txtChildDOB_MaskedEditExtender"
                                                        ControlToValidate="txtChildDOB" CssClass="errorMessage" ErrorMessage="txtChildDOB_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td>
                                                    Pass No:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildPassNo" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Pass Expiry Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildPassExpiryDate" Width="150px" runat="server"></asp:TextBox>
                                                       <asp:ImageButton ID="ImageButtonChildPassExpiryDate" ToolTip="Pick child pass expiry date" CssClass="date-image"
                                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtChildPassExpiryDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtChildPassExpiryDate" PopupButtonID="ImageButtonChildPassExpiryDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtChildPassExpiryDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                                        Mask="99/LLL/9999" TargetControlID="txtChildPassExpiryDate" UserDateFormat="DayMonthYear">
                                                    </asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtChildPassExpiryDate_MaskedEditValidator" runat="server" ControlExtender="txtChildPassExpiryDate_MaskedEditExtender"
                                                        ControlToValidate="txtChildPassExpiryDate" CssClass="errorMessage" ErrorMessage="txtChildPassExpiryDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td>
                                                    Gender:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblChildGender" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Country of Residence:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlChildCountryOfResidence" runat="server">
                                                        <asp:ListItem Value="0">-Select Country of Residence-</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                    Nationality:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChildNationality" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td>
                                                    Has Birth /Adoption Certificate been Provided?<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblChildBirthCertificateProvided" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem Value="true">Yes</asp:ListItem>
                                                        <asp:ListItem Value="false">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldChildID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddChildDetailsPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveChild" OnClick="btnSaveChild_OnClick" ToolTip="Save child details"
                                            runat="server">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateChild" OnClick="btnUpdateChild_OnClick" CausesValidation="false"
                                            ToolTip="Update child details" Enabled="false" runat="server">
                                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearChild" OnClick="lnkBtnClearChild_OnClick" CausesValidation="false"
                                            ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddChildDetails" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddChildDetailsPopup" PopupControlID="PanelAddChildDetails"
                                CancelControlID="ImageButtonCloseAddChildDetails" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelOtherDependants" runat="server" CssClass="ajax-tab" Visible="false"
                        HeaderText="Other Dependants">
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvOtherDependantDetails" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                    EmptyDataText="No other dependants' details available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("OtherDependantID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DependantName" HeaderText="Dependant Name" />
                                        <asp:BoundField DataField="DependantDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                        <asp:BoundField DataField="Gender" HeaderText="Gender" />
                                        <asp:BoundField DataField="Relationship" HeaderText="Relationship" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddOtherDependantDetails" Style="display: none; width: 550px;"
                                runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddOtherDependantDetails" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddOtherDependantDetailsHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddOtherDependantDetailsHeader" runat="server" Text="Add Other Dependant"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddOtherDependantDetails" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Dependant Details</legend>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Dependant Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOtherDependantName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date of Birth:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOtherDependantDOB" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonOtherDependantDOB" ToolTip="Pick OtherDependant date of birth"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtOtherDependantDOB_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtOtherDependantDOB" PopupButtonID="ImageButtonOtherDependantDOB"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtOtherDependantDOB_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtOtherDependantDOB"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtOtherDependantDOB_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtOtherDependantDOB_MaskedEditExtender" ControlToValidate="txtOtherDependantDOB"
                                                        CssClass="errorMessage" ErrorMessage="txtOtherDependantDOB_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Gender:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblOtherDependantGender" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Relationship:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOtherDependantRelationship" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label3" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldOtherDependantID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddOtherDependantDetailsPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveOtherDependant" OnClick="lnkBtnSaveOtherDependant_OnClick"
                                            ToolTip="Save dependant" runat="server">
                                            <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateOtherDependant" OnClick="lnkBtnUpdateOtherDependant_OnClick"
                                            CausesValidation="false" ToolTip="Update job title" Enabled="false" runat="server">
                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearOtherDependant" OnClick="lnkBtnClearOtherDependant_OnClick"
                                            CausesValidation="false" ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddOtherDependantDetails" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddOtherDependantDetailsPopup" PopupControlID="PanelAddOtherDependantDetails"
                                CancelControlID="ImageButtonCloseAddOtherDependantDetails" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
