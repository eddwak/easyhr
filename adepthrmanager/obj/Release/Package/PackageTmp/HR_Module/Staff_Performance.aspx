﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Performance.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Performance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffPerformance" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffPerformance" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="panel-details">
                    <asp:TabContainer ID="TabContainerStaffPerformance" CssClass="ajax-tab" runat="server"
                        ActiveTabIndex="0">
                        <asp:TabPanel ID="TabPanelPerformanceTargets" runat="server" CssClass="ajax-tab"
                            HeaderText="Performance Targets">
                            <ContentTemplate>
                                <div class="panel-details">
                                    <div class="content-menu-bar">
                                        <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_OnClick" ToolTip="Add"
                                            CausesValidation="false" runat="server">
                                            <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                            Add New Perfomance Objective</asp:LinkButton>
                                        <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                                        <asp:LinkButton ID="LinkButtonEdit" ToolTip="Edit" CausesValidation="false" runat="server">
                                            <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                            Edit Performance Objective</asp:LinkButton>
                                        <asp:Label ID="LabeEdit" runat="server" Text="|"></asp:Label>
                                        <asp:LinkButton ID="LinkButtonDelete" CausesValidation="false" ToolTip="Delete" runat="server">
                                            <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                            Delete Performance Objective</asp:LinkButton>
                                        <asp:Label ID="LabelDeletePerformanceObjective" runat="server" Text="|"></asp:Label>
                                        <td>
                                            Year<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPerformanceYear" Width="80px" runat="server">
                                                <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </div>
                                    <asp:HiddenField ID="HiddenFieldAccessedgvAnnualPerformanceRowIndex" runat="server" />
                                    <asp:GridView ID="gvAnnualPerformance" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="GridViewStyle" DataKeyNames="AnnualPerformanceID"
                                        PageSize="10" OnRowDataBound="gvAnnualPerformance_RowDataBound" OnRowCommand="gvAnnualPerformance_RowCommand"
                                        EmptyDataText="No annual performance objectives available">
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                        <Columns>
                                            <asp:TemplateField ControlStyle-Width="18px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButtonExpandCollapseDetails" ImageUrl="~/images/plus.png"
                                                        runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="RadioButton1" runat="server" onclick="RadioCheck(this);" />
                                                    <asp:HiddenField ID="HiddenFieldAnnualPerformanceID" runat="server" Value='<%# Bind("AnnualPerformanceID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField DataField="PerformanceYear" HeaderText="Year" />--%>
                                            <asp:BoundField DataField="PerformanceObjective" HeaderText="Performance Objective" />
                                            <asp:BoundField DataField="Target" HeaderText="Target" />
                                            <asp:BoundField DataField="Achieved" HeaderText="Actual Achieved at Year End" />
                                            <asp:BoundField DataField="Rating" HeaderText="Rating" />
                                            <asp:BoundField DataField="Weight" HeaderText="% Weight" />
                                            <asp:BoundField DataField="Score" HeaderText="% Score" />
                                            <asp:BoundField DataField="Comments" HeaderText="Comments" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    </td> </tr>
                                                    <tr>
                                                        <td colspan="100%" style="padding-left: 28px; padding-bottom: 0px; padding-top: 0px;
                                                            padding-right: 0px; height: 0px;">
                                                            <asp:Panel ID="PanelExpandCollapseDetails" Style="display: none;" runat="server">
                                                                <asp:GridView ID="gvQuarterlyPerformance" runat="server" AllowPaging="True" AllowSorting="True"
                                                                    AutoGenerateColumns="False" CssClass="Inner-GridViewStyle" EmptyDataText="No key peformance indicators available"
                                                                    PageSize="5">
                                                                    <FooterStyle CssClass="PagerStyle" />
                                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                                <asp:HiddenField ID="HiddenFieldQuarterlyPerformanceID" runat="server" Value='<%# Bind("QuarterlyPerformanceID") %>' />
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="5px" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="KeyIndicator" HeaderText="Key Performance Indicator" />
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                Actual Achieved
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            Quarter 1
                                                                                        </td>
                                                                                        <td>
                                                                                            Quarter 2
                                                                                        </td>
                                                                                        <td>
                                                                                            Quarter 3
                                                                                        </td>
                                                                                        <td>
                                                                                            Quarter 4
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter1" runat="server" Text='<%# Bind("Quarter1Achieved") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter2" runat="server" Text='<%# Bind("Quarter2Achieved") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter3" runat="server" Text='<%# Bind("Quarter3Achieved") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter4" runat="server" Text='<%# Bind("Quarter4Achieved") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            Appraisal Date:
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter1AppraisalDate" runat="server" Text='<%# Bind("Quarter1AppraisalDate") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter2AppraisalDate" runat="server" Text='<%# Bind("Quarter2AppraisalDate") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter3AppraisalDate" runat="server" Text='<%# Bind("Quarter3AppraisalDate") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter4AppraisalDate" runat="server" Text='<%# Bind("Quarter4AppraisalDate") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            Appraised By:
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter1AppraisedBy" runat="server" Text='<%# Bind("Quarter1AppraisedBy") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter2AppraisedBy" runat="server" Text='<%# Bind("Quarter2AppraisedBy") %>'>></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter3AppraisedBy" runat="server" Text='<%# Bind("Quarter3AppraisedBy") %>'>></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter4AppraisedBy" runat="server" Text='<%# Bind("Quarter4AppraisedBy") %>'>></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            Comments:
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter1Comments" runat="server" Text='<%# Bind("Quarter1Comments") %>'>></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter2Comments" runat="server" Text='<%# Bind("Quarter2Comments") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter3Comments" runat="server" Text='<%# Bind("Quarter3Comments") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbQuarter4Comments" runat="server" Text='<%# Bind("Quarter4Comments") %>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:ButtonField DataTextField="KeyIndicator" HeaderText="Key Performance Indicator"
                                                                            CommandName="ViewKeyPerformanceIndicator" ItemStyle-CssClass="gridButtons" />--%>
                                                                        <%--<asp:BoundField DataField="PerformanceObjective" HeaderText="Performance Objective" />
                                                                    <asp:BoundField DataField="Target" HeaderText="Target" />
                                                                    <asp:BoundField DataField="Achieved" HeaderText="Achieved" />
                                                                    <asp:BoundField DataField="Rating" HeaderText="Rating" />--%>
                                                                    </Columns>
                                                                    <%--   <HeaderStyle Font-Size="11px" Height="10px" />
                                                    <RowStyle Font-Size="11px" />--%>
                                                                </asp:GridView>
                                                                <asp:Panel ID="PanelKeyPerformanceIndicatorsEntryButtons" CssClass="grid-linkBtn"
                                                                    runat="server">
                                                                    <asp:UpdatePanel ID="UpdatePanelApplicantsScheduledEntryButtons" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:Button ID="btnAddNewKeyPerformanceIndicator" runat="server" CssClass="buttonNavigations"
                                                                                CommandName="AddKeyPerformanceIndicator" Text="Add Key Performance Indicator"
                                                                                CausesValidation="false" />
                                                                            <asp:Button ID="btnEditKeyPerformanceIndicator" runat="server" CssClass="buttonNavigations"
                                                                                CommandName="EditKeyPerformanceIndicator" Text="Edit Key Performance Indicator"
                                                                                ToolTip="Edit interview appointment" CausesValidation="false" />
                                                                            <asp:Button ID="btnDeleteKeyPerformanceIndicator" runat="server" CssClass="buttonNavigations"
                                                                                Text="Delete" CommandName="DeleteKeyPerformanceIndicator" ToolTip="Delete Key Performance Indicator"
                                                                                CausesValidation="false" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </asp:Panel>
                                                            </asp:Panel>
                                                            <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtenderExpandCollapseDetails"
                                                                runat="Server" TargetControlID="PanelExpandCollapseDetails" CollapsedSize="0"
                                                                Collapsed="True" ExpandControlID="ImageButtonExpandCollapseDetails" CollapseControlID="ImageButtonExpandCollapseDetails"
                                                                AutoCollapse="False" AutoExpand="False" ScrollContents="false" TextLabelID="Label1"
                                                                CollapsedText="Show applicants scheduled for interview..." ExpandedText="Hide applicants scheduled for interview"
                                                                ImageControlID="ImageButtonExpandCollapseDetails" ExpandedImage="~/images/minus.png"
                                                                CollapsedImage="~/images/plus.png" ExpandDirection="Vertical" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <script language="javascript" type="text/javascript">
                                        select_first_gvInterviewAppointment();</script>
                                    <asp:Panel ID="PanelAddAnnualPerformance" Style="display: none; width: 650px;" runat="server">
                                        <div class="popup-header">
                                            <asp:Panel ID="PanelDragAddAnnualPerformance" runat="server">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 97%;">
                                                            <asp:Image ID="ImageAddAnnualPerformanceHeader" runat="server" ImageAlign="AbsMiddle"
                                                                ImageUrl="~/Images/icons/Medium/Create.png" />
                                                            <asp:Label ID="lbAddAnnualPerformanceHeader" runat="server" Text="Add Performance Objective"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="ImageButtonCloseAddAnnualPerformance" ImageUrl="~/images/icons/small/Remove.png"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                        <div class="popup-details">
                                            <fieldset>
                                                <legend>Performance Objective Details</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Performance Objective:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPerformanceObjective" TextMode="MultiLine" Style="height: 50px;"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <tr>
                                                            <td>
                                                                Annual Target:<span class="errorMessage">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualTarget" Width="100px" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Actual Achieved at Year End<span class="errorMessage">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualAchieved" Width="100px" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Rating:<span class="errorMessage">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualRating" Width="70px" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                % Weight:<span class="errorMessage">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualWeight" Width="70px" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                % Score:<span class="errorMessage">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualScore" Width="70px" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Comments:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnnualComments" Style="height: 50px;" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                </table>
                                            </fieldset>
                                            <div class="linkBtn">
                                                <asp:HiddenField ID="HiddenFieldAnnualPerformanceID" runat="server" />
                                                <asp:HiddenField ID="HiddenFieldAddAnnualPerformancePopup" runat="server" />
                                                <asp:Button ID="btnSaveAnnualPerformance" OnClick="btnSaveAnnualPerformance_Click"
                                                    runat="server" Text="Save Performance Objective" />
                                                <asp:LinkButton ID="lnkBtnUpdatePerformanceObjective" CausesValidation="false" ToolTip="Update"
                                                    Enabled="false" runat="server">
                                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                        ImageAlign="AbsMiddle" />
                                                    Update</asp:LinkButton>
                                                <asp:LinkButton ID="lnkBtnClearPerformanceObjective" CausesValidation="false" ToolTip="Clear"
                                                    runat="server">
                                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                        ImageAlign="AbsMiddle" />
                                                    Clear</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:ModalPopupExtender ID="ModalPopupExtenderAddAnnualPerformance" RepositionMode="RepositionOnWindowResize"
                                        TargetControlID="HiddenFieldAddAnnualPerformancePopup" PopupControlID="PanelAddAnnualPerformance"
                                        CancelControlID="ImageButtonCloseAddAnnualPerformance" BackgroundCssClass="modalback"
                                        runat="server">
                                    </asp:ModalPopupExtender>
                                    <asp:Panel ID="PanelAddKeyPerformanceIndicator" Style="display: none; width: 1180px;"
                                        runat="server">
                                        <div class="popup-header">
                                            <asp:Panel ID="PanelDragAddKeyPerformanceIndicator" runat="server">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 97%;">
                                                            <asp:Image ID="ImageAddKeyPerformanceIndicatorHeader" runat="server" ImageAlign="AbsMiddle"
                                                                ImageUrl="~/Images/icons/Medium/Create.png" />
                                                            <asp:Label ID="lbAddKeyPerformanceIndicatorHeader" runat="server" Text="Add Key Performance Indicator"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="ImageButtonCloseAddKeyPerformanceIndicator" ImageUrl="~/images/icons/small/Remove.png"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                        <div class="popup-details">
                                            <fieldset>
                                                <legend>Details</legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <b>Key Performance Indicator</b>
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <b>Actual Achieved</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <asp:TextBox ID="txtKeyPerformanceIndicator" Height="170px" Width="250px" TextMode="MultiLine"
                                                                runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Quarter 1
                                                                    </td>
                                                                    <td>
                                                                        Quarter 2
                                                                    </td>
                                                                    <td>
                                                                        Quarter 3
                                                                    </td>
                                                                    <td>
                                                                        Quarter 4
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter1" Width="150px" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter2" Width="150px" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter3" Width="150px" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter4" Width="150px" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Appraisal Date:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter1AppraisalDate" Width="120px" Enabled="true" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageButtonQuarter1AppraisalDate" ToolTip="Pick quarter 1 date"
                                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                                        <asp:CalendarExtender ID="txtQuarter1AppraisalDate_CalendarExtender" runat="server"
                                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter1AppraisalDate"
                                                                            PopupButtonID="ImageButtonQuarter1AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                                        <asp:MaskedEditExtender ID="txtQuarter1AppraisalDate_MaskedEditExtender" runat="server"
                                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter1AppraisalDate"
                                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                                        <asp:MaskedEditValidator ID="txtQuarter1AppraisalDate_MaskedEditValidator" runat="server"
                                                                            ControlExtender="txtQuarter1AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter1AppraisalDate"
                                                                            CssClass="errorMessage" ErrorMessage="txtQuarter1AppraisalDate_MaskedEditValidator"
                                                                            InvalidValueMessage="<br>Invalid quarter 1 date" Display="Dynamic">
                                                                        </asp:MaskedEditValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter2AppraisalDate" Width="120px" Enabled="true" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageButtonQuarter2AppraisalDate" ToolTip="Pick quarter 2 date"
                                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                                        <asp:CalendarExtender ID="txtQuarter2AppraisalDate_CalendarExtender" runat="server"
                                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter2AppraisalDate"
                                                                            PopupButtonID="ImageButtonQuarter2AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                                        <asp:MaskedEditExtender ID="txtQuarter2AppraisalDate_MaskedEditExtender" runat="server"
                                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter2AppraisalDate"
                                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                                        <asp:MaskedEditValidator ID="txtQuarter2AppraisalDate_MaskedEditValidator" runat="server"
                                                                            ControlExtender="txtQuarter2AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter2AppraisalDate"
                                                                            CssClass="errorMessage" ErrorMessage="txtQuarter2AppraisalDate_MaskedEditValidator"
                                                                            InvalidValueMessage="<br>Invalid quarter 2 date" Display="Dynamic">
                                                                        </asp:MaskedEditValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter3AppraisalDate" Width="120px" Enabled="true" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageButtonQuarter3AppraisalDate" ToolTip="Pick quarter 3 date"
                                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                                        <asp:CalendarExtender ID="txtQuarter3AppraisalDate_CalendarExtender" runat="server"
                                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter3AppraisalDate"
                                                                            PopupButtonID="ImageButtonQuarter3AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                                        <asp:MaskedEditExtender ID="txtQuarter3AppraisalDate_MaskedEditExtender" runat="server"
                                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter3AppraisalDate"
                                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                                        <asp:MaskedEditValidator ID="txtQuarter3AppraisalDate_MaskedEditValidator" runat="server"
                                                                            ControlExtender="txtQuarter3AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter3AppraisalDate"
                                                                            CssClass="errorMessage" ErrorMessage="txtQuarter3AppraisalDate_MaskedEditValidator"
                                                                            InvalidValueMessage="<br>Invalid quarter 3 date" Display="Dynamic">
                                                                        </asp:MaskedEditValidator>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter4AppraisalDate" Width="120px" Enabled="true" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageButtonQuarter4AppraisalDate" ToolTip="Pick quarter 4 date"
                                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                                        <asp:CalendarExtender ID="txtQuarter4AppraisalDate_CalendarExtender" runat="server"
                                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarter4AppraisalDate"
                                                                            PopupButtonID="ImageButtonQuarter4AppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                                        <asp:MaskedEditExtender ID="txtQuarter4AppraisalDate_MaskedEditExtender" runat="server"
                                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarter4AppraisalDate"
                                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                                        <asp:MaskedEditValidator ID="txtQuarter4AppraisalDate_MaskedEditValidator" runat="server"
                                                                            ControlExtender="txtQuarter4AppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarter4AppraisalDate"
                                                                            CssClass="errorMessage" ErrorMessage="txtQuarter4AppraisalDate_MaskedEditValidator"
                                                                            InvalidValueMessage="<br>Invalid quarter 4 date" Display="Dynamic">
                                                                        </asp:MaskedEditValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Appraised By:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlQuarter1AppraisedBy" Width="150px" runat="server">
                                                                            <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlQuarter2AppraisedBy" Width="150px" runat="server">
                                                                            <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlQuarter3AppraisedBy" Width="150px" runat="server">
                                                                            <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlQuarter4AppraisedBy" Width="150px" runat="server">
                                                                            <asp:ListItem Text="-Select Appraised By-" Value="0" Selected="true"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Comments:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter1Comments" Width="150px" Height="50px" TextMode="MultiLine"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter2Comments" Width="150px" Height="50px" TextMode="MultiLine"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter3Comments" Width="150px" Height="50px" TextMode="MultiLine"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtQuarter4Comments" Width="150px" Height="50px" TextMode="MultiLine"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                            <div class="linkBtn">
                                                <asp:HiddenField ID="HiddenFieldQuarterlyPerformanceID" runat="server" />
                                                <asp:HiddenField ID="HiddenFieldAddKeyPerformanceIndicatorPopup" runat="server" />
                                                <asp:LinkButton ID="lnkBtnSaveKeyPerformanceIndicator" OnClick="lnkBtnSaveKeyPerformanceIndicator_Click"
                                                    ToolTip="Save spouse details" runat="server">
                                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Key Performance Indicator</asp:LinkButton>
                                                <asp:LinkButton ID="lnkBtnUpdateKeyPerformanceIndicator" OnClick="lnkBtnUpdateKeyPerformanceIndicator_Click"
                                                    CausesValidation="false" ToolTip="Update" Enabled="false" runat="server">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                        ImageAlign="AbsMiddle" />
                                                    Update</asp:LinkButton>
                                                <asp:LinkButton ID="lnkBtnClearKeyPerformanceIndicator" OnClick="lnkBtnClearKeyPerformanceIndicator_Click"
                                                    CausesValidation="false" ToolTip="Clear" runat="server">
                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                        ImageAlign="AbsMiddle" />
                                                    Clear</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:ModalPopupExtender ID="ModalPopupExtenderAddKeyPerformanceIndicator" RepositionMode="RepositionOnWindowResize"
                                        TargetControlID="HiddenFieldAddKeyPerformanceIndicatorPopup" PopupControlID="PanelAddKeyPerformanceIndicator"
                                        CancelControlID="ImageButtonCloseAddKeyPerformanceIndicator" BackgroundCssClass="modalback"
                                        runat="server">
                                    </asp:ModalPopupExtender>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <%-- <asp:TabPanel ID="TabPanelPerformanceSummarry" runat="server" CssClass="ajax-tab"
                            HeaderText="Performance Summary">
                            <ContentTemplate>
                            </ContentTemplate>
                        </asp:TabPanel>--%>
                    </asp:TabContainer>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
