﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="True" CodeBehind="Alerts.aspx.cs" Inherits="AdeptHRManager.HR_Module.Alerts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffRecord" runat="server">
        <ContentTemplate>
        <asp:Panel ID="PanelAlerts" runat="server">
         <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HoverMenuExtender ID="ImageStaffPhoto_HoverMenuExtender" runat="server" DynamicServicePath=""
                                    Enabled="True" TargetControlID="ImageStaffPhoto" PopupControlID="LinkButtonUploadStaffphoto"
                                    PopupPosition="Bottom">
                                </asp:HoverMenuExtender>
                                <div class="image-uploader-linkBtn">
                                    <asp:LinkButton ID="LinkButtonUploadStaffphoto" OnClick="LinkButtonUploadStaffphoto_Click"
                                        Style="display: none;" CausesValidation="false" runat="server">
                                     Upload Staff Photo</asp:LinkButton>
                                </div>
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <div class="panel-details">
                    <fieldset>
                        <legend>Anniversary Reminders</legend>
                        <asp:Label ID="lblContractEnd" runat="server" Text="ContractEnd" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblProbationEnd" runat="server" Text="Probation End" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblRetirementEnd" runat="server" Text="Retirement End" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblTenancyAgreement" runat="server" Text="Tenancy Agreement" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblCarAgreement" runat="server" Text="Car Agreement" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblLeaveApplication" runat="server" Text="Leave Application" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblWorkPermitEnd" runat="server" Text="Work Permit End" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblVisaRenewal" runat="server" Text="Visa Renewal" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblDependentPassExpiry" runat="server" Text="Dependent Pass End" CssClass="alerts" Visible="false"></asp:Label>
                         </fieldset>

                  <asp:Panel ID="PanelUploadStaffImage" Style="display: none; width: 670px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragUploadStaffImage" runat="server">
                            <table>
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageUploadStaffImageHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAUploadStaffImageHeader" runat="server" Text="Select Staff Image, Upload, Crop & Save"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseUploadStaffImage" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <table width="100%">
                            <tr>
                                <td>
                                    <b>Select Staff Image File :</b>
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUploadStaffPhoto" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <div class="div-image-upload">
                            <asp:Panel ID="panCrop" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="staff-image-upload">
                                                <asp:Image ID="imgUpload" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <%-- Hidden field for store cror area --%>
                                            <asp:HiddenField ID="hfX" runat="server" />
                                            <asp:HiddenField ID="hfY" runat="server" />
                                            <asp:HiddenField ID="hfW" runat="server" />
                                            <asp:HiddenField ID="hfH" runat="server" />
                                            <asp:Button ID="btnCrop" Visible="false" runat="server" Text="Crop & Save" OnClick="btnCrop_Click" />
                                            <asp:HiddenField ID="HiddenFieldUploadStaffImagePopup" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>

                  </div>
                </asp:Panel>
                  <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                    <asp:ModalPopupExtender ID="ModalPopupExtenderUploadStaffImage" RepositionMode="None"
                    X="170" Y="2" TargetControlID="HiddenFieldUploadStaffImagePopup" PopupControlID="PanelUploadStaffImage"
                    CancelControlID="ImageButtonCloseUploadStaffImage" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderUploadStaffImage" TargetControlID="PanelUploadStaffImage"
                    DragHandleID="PanelDragUploadStaffImage" runat="server"></asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeletePerk" runat="server" />
                   </asp:Panel>
                </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Content>
