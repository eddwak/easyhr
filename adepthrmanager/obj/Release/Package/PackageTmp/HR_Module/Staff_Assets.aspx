﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Assets.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Assets" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffAssets" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffAssets" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:TabContainer ID="TabContainerStaffAssets" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelCarDetails" runat="server" CssClass="ajax-tab" HeaderText="Car Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%--  <asp:CheckBox ID="cbVehicleAssigned" OnCheckedChanged="cbVehicleAssigned_CheckedChanged"
                                                AutoPostBack="true" Text="" TextAlign="Left"
                                                runat="server" />--%>
                                            Has the employee been given a vehicle?<span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblVehicleAssigned" OnSelectedIndexChanged="rblVehicleAssigned_SelectedIndexChanged"
                                                AutoPostBack="True" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveVehicle" OnClick="lnkBtnSaveVehicle_Click" ToolTip="Save vehicle details"
                                                    runat="server"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />






                                                    Save Car Details</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddCarDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Type of Vehicle:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTypeOfVehicle" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Vehicle Make:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVehicleMake" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Color:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVehicleColor" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Registration Number:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVehicleRegistrationNumber" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Driving Details:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblVehicleDrivingDetails" RepeatDirection="Horizontal" runat="server">
                                                                <asp:ListItem Value="false" Text="Self-Drive"></asp:ListItem>
                                                                <asp:ListItem Value="true" Text="Driver Assigned"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            Assigned Driver:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVehicleAssignedDriver" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Year of Manufacture:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlVehicleYearOfManufacture" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Vehicle Agreement End Date:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVehicleAgreementEndDate" runat="server"></asp:TextBox>
                                                               <asp:ImageButton ID="ImageButtonVehicleAgreementEndDate" ToolTip="Pick vehicle agreement end date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtVehicleAgreementEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtVehicleAgreementEndDate" PopupButtonID="ImageButtonVehicleAgreementEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtVehicleAgreementEndDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtVehicleAgreementEndDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtVehicleAgreementEndDate_MaskedEditValidator" runat="server" ControlExtender="txtVehicleAgreementEndDate_MaskedEditExtender"
                                        ControlToValidate="txtVehicleAgreementEndDate" CssClass="errorMessage" ErrorMessage="txtVehicleAgreementEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                    <br />
                                    <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2015"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             Driving permit expiry date:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                          <asp:TextBox ID="txtDrivingPermitExpiryDate" runat="server"></asp:TextBox> 
                                                            <asp:ImageButton ID="ImageButtonDrivingPermitExpiryDate" ToolTip="Pick vehicle agreement end date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtDrivingPermitExpiryDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtDrivingPermitExpiryDate" PopupButtonID="ImageButtonDrivingPermitExpiryDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtDrivingPermitExpiryDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtDrivingPermitExpiryDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtDrivingPermitExpiryDate_MaskedEditValidator" runat="server" ControlExtender="txtDrivingPermitExpiryDate_MaskedEditExtender"
                                        ControlToValidate="txtDrivingPermitExpiryDate" CssClass="errorMessage" ErrorMessage="txtDrivingPermitExpiryDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                    <br />
                                    <asp:Label ID="Label2" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2015"></asp:Label>
                                                        </td>
                                                        <td>
                                                           Comments/Remarks:
                                                            </td>
                                                        <td>
                                                          <asp:TextBox ID="txtVehicleComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelHouseDetails" runat="server" CssClass="ajax-tab" HeaderText="House Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%--<asp:CheckBox ID="cbHouseAllocated" OnCheckedChanged="cbHouseAllocated_CheckedChanged"
                                                AutoPostBack="true" Text="Has the employee been allocated a house?" TextAlign="Left"
                                                runat="server" />--%>
                                            Has the employee been allocated a house?<span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblHouseAllocated" OnSelectedIndexChanged="rblHouseAllocated_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveHouse" OnClick="lnkBtnSaveHouse_Click" ToolTip="Save house details"
                                                    runat="server">
                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save House Details</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddHouseDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Block Number:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHouseBlockNumber" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Plot Number:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHousePlotNumber" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Road:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHouseRoad" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Rent Amount:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHouseRentAmount" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Year of Manufacture:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlHouseYearOfManufacture" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Tenure:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHouseTenure" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tenancy Agreement Received & Signed:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblHouseTenancyAgreement" RepeatDirection="Horizontal" runat="server">
                                                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                                                <asp:ListItem Value="False">No</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            Tenancy Agreement End Date:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTenancyAgreementEndDate" runat="server"></asp:TextBox>
                                                           <asp:ImageButton ID="ImageButtonTenancyAgreementEndDate" ToolTip="Pick tenancy agreement end date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtTenancyAgreementEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtTenancyAgreementEndDate" PopupButtonID="ImageButtonTenancyAgreementEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtTenancyAgreementEndDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtTenancyAgreementEndDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtTenancyAgreementEndDate_MaskedEditValidator" runat="server" ControlExtender="txtTenancyAgreementEndDate_MaskedEditExtender"
                                        ControlToValidate="txtTenancyAgreementEndDate" CssClass="errorMessage" ErrorMessage="txtTenancyAgreementEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                    <br />
                                    <asp:Label ID="Label1" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2015"></asp:Label>

                                                        </td>
                                                    </tr>
                                                     <td>
                                                            Comments/Remarks:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHouseComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelTelephone" runat="server" CssClass="ajax-tab" HeaderText="Telephone Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%-- <asp:CheckBox ID="cbTelephoneAssigned" OnCheckedChanged="cbTelephoneAllocated_CheckedChanged"
                                                AutoPostBack="true" Text="Has the employee been given a telephone?" TextAlign="Left"
                                                runat="server" />--%>
                                            Has the employee been given a telephone? <span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblTelephoneAssigned" OnSelectedIndexChanged="rblTelephoneAssigned_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveTelephoneDetails" OnClick="lnkBtnSaveTelephone_Click"
                                                    ToolTip="Save telephone details" runat="server">
                                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Telephone Details</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddTelephoneDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Type of Telephone:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelephoneType" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Make:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelephoneMake" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Serial Number:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelephoneSerialNumber" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Year of Manufacture:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTelephoneYearOfManufacture" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Other Accessories:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelephoneOtherAccessories" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Comments/Remarks:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelephoneComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLaptopDetails" runat="server" CssClass="ajax-tab" HeaderText="Laptop Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%--<asp:CheckBox ID="cbLaptopAssigned" OnCheckedChanged="cbLaptopAssigned_CheckedChanged"
                                                AutoPostBack="true" Text="Has the employee been given a laptop?" TextAlign="Left"
                                                runat="server" />--%>
                                            Has the employee been given a laptop? <span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblLaptopAssigned" OnSelectedIndexChanged="rblLaptopAssigned_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveLaptop" OnClick="lnkBtnSaveLaptop_Click" ToolTip="Save laptop details"
                                                    runat="server">
                                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Laptop Details</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddLaptopDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Make:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLaptopMake" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Serial Number:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLaptopSerialNumber" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Year of Manufacture:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlLaptopYearOfManufacture" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Other Accessories:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLaptopOtherAccessories" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Comments/Remarks:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLaptopComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelAssetFinanced" runat="server" CssClass="ajax-tab" HeaderText="Asset Finance Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%-- <asp:CheckBox ID="cbAssetFinanced" OnCheckedChanged="cbAssetFinanced_CheckedChanged"
                                                AutoPostBack="true" Text="Does the employee has a financed asset?" TextAlign="Left"
                                                runat="server" />--%>
                                            Does the employee has a financed asset?<span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblAssetFinanced" OnSelectedIndexChanged="rblAssetFinanced_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveAssetFinanced" OnClick="lnkBtnSaveAssetFinanced_Click"
                                                    ToolTip="Save asset financed" runat="server">
                                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Asset Financed</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddAssetFinancedDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Asset Financed:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinanced" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Date:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinancedDate" Width="200px" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="ImageButtonAssetFinancedDate" ToolTip="Pick asset fincanced date"
                                                                CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                            <asp:CalendarExtender ID="txtAssetFinancedDate_CalendarExtender" runat="server" Enabled="True"
                                                                Format="dd/MMM/yyyy" TargetControlID="txtAssetFinancedDate" PopupButtonID="ImageButtonAssetFinancedDate"
                                                                PopupPosition="TopRight"></asp:CalendarExtender>
                                                            <asp:MaskedEditExtender ID="txtAssetFinancedDate_MaskedEditExtender" runat="server"
                                                                ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtAssetFinancedDate"
                                                                UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                            <asp:MaskedEditValidator ID="txtAssetFinancedDate_MaskedEditValidator" runat="server"
                                                                ControlExtender="txtAssetFinancedDate_MaskedEditExtender" ControlToValidate="txtAssetFinancedDate"
                                                                CssClass="errorMessage" ErrorMessage="txtAssetFinancedDate_MaskedEditValidator"
                                                                InvalidValueMessage="<br>Invalid asset financed date entered" Display="Dynamic">
                                                            </asp:MaskedEditValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Amount:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinancedAmount" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Duration:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinancedDuration" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Interest Charged:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinancedInterestCharged" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Comments/Remarks:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAssetFinancedComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLoans" runat="server" CssClass="ajax-tab" HeaderText="Loan Details">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">
                                            <%--<asp:CheckBox ID="cbDoesStaffHaveALoan" OnCheckedChanged="cbDoesStaffHaveALoan_CheckedChanged"
                                                AutoPostBack="true" Text="Does the employee has any loan?" TextAlign="Left" runat="server" />--%>
                                            Does the employee has a financed asset?<span class="errorMessage">*</span>
                                        </td>
                                        <td width="15%">
                                            <asp:RadioButtonList ID="rblDoesStaffHaveALoan" OnSelectedIndexChanged="rblDoesStaffHaveALoan_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem>Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td width="60%">
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnSaveLoanDetails" OnClick="lnkBtnSaveLoanDetails_Click"
                                                    ToolTip="Save loan details" runat="server">
                                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Loan Details</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelAddLoanInformationDetails" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            Loan Amount:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLoanAmount" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Date:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLoanDate" Width="200px" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="ImageButtonLoanDate" ToolTip="Pick loan date" CssClass="date-image"
                                                                ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                            <asp:CalendarExtender ID="txtLoanDate_CalendarExtender" runat="server" Enabled="True"
                                                                Format="dd/MMM/yyyy" TargetControlID="txtLoanDate" PopupButtonID="ImageButtonLoanDate"
                                                                PopupPosition="TopRight"></asp:CalendarExtender>
                                                            <asp:MaskedEditExtender ID="txtLoanDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                                                Mask="99/LLL/9999" TargetControlID="txtLoanDate" UserDateFormat="DayMonthYear">
                                                            </asp:MaskedEditExtender>
                                                            <asp:MaskedEditValidator ID="txtLoanDate_MaskedEditValidator" runat="server" ControlExtender="txtLoanDate_MaskedEditExtender"
                                                                ControlToValidate="txtLoanDate" CssClass="errorMessage" ErrorMessage="txtLoanDate_MaskedEditValidator"
                                                                InvalidValueMessage="<br>Invalid loan date entered" Display="Dynamic">
                                                            </asp:MaskedEditValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Duration:<span class="errorMessage">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLoanDuration" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Comments/Remarks:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLoanComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
