﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRManager_UpdateProgress.ascx.cs"
    Inherits="AdeptHRManager.HRManagerClasses.HRManagerUserControls.HRManager_UpdateProgress" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="update-progress">
    <asp:UpdateProgress ID="UpdateProgressLoading" runat="server">
        <ProgressTemplate>
            <asp:Image ID="ImageLoading" runat="server" ImageUrl="~/Images/background/loading.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
