﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AdeptHRManager.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="~/Styles/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelLogin" runat="server">
        <ContentTemplate>
            <div class="header">
                <div class="topheader">
                    <div class="container">
                        <label id="lbLoggedUserName" runat="server">
                            &nbsp;
                        </label>
                    </div>
                </div>
                <div class="container">
                    <div class="top-header-bar-inner">
                        <div class="logo">
                          <span class="logo-span1">ADEPT</span><span class="logo-span2"> HR</span>
                            <%--<asp:Image ID="ImagLogo" ImageUrl="~/images/background/logo.png" runat="server" />--%>
                        </div>
                        <div class="version">
                            EADB HRMS Version 1.0
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu">
            </div>
            <div class="container">
                <div class="login-container">
                    <div class="main_content">
                        <div class="login-form">
                            <asp:TabContainer ID="TabContainerLogin" CssClass="ajax-tab" runat="server" ActiveTabIndex="0">
                                <asp:TabPanel ID="TabPanelUserLogin" runat="server">
                                    <HeaderTemplate>
                                        Login as Staff
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <p>
                                            Username</p>
                                        <p>
                                            <asp:TextBox ID="txtStaffUserName" runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtStaffUserName_TextBoxWatermarkExtender" runat="server"
                                                Enabled="True" TargetControlID="txtStaffUserName" WatermarkText="enter staff username"
                                                WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                        </p>
                                        <p>
                                            Password</p>
                                        <p>
                                            <asp:TextBox ID="txtStaffPassword" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtStaffPassword_TextBoxWatermarkExtender" runat="server"
                                                Enabled="True" TargetControlID="txtStaffPassword" WatermarkText="Enter password"
                                                WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                        </p>
                                        <p>
                                            <asp:CheckBox ID="cbStaffRememberMe" CssClass="remember-check" Text="Remember my login details"
                                                runat="server" />
                                        </p>
                                        <p>
                                            <asp:Label ID="lbStaffError" runat="server" CssClass="error-label" Text=""></asp:Label>
                                            <asp:Label ID="lbStaffLoginAttempts" runat="server" CssClass="login-attempts-label"
                                                Text=""></asp:Label>
                                        </p>
                                        <p>
                                            <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                                            <asp:Button ID="lnkBtnStaffLogin" OnClick="lnkBtnStaffLogin_Click" runat="server"
                                                Text="Staff Login" ToolTip="Click to login" />
                                        </p>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel ID="TabPanelAdminLogin" runat="server" CssClass="ajax-tab">
                                    <HeaderTemplate>
                                        Login as HR /Administrator
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <p>
                                            Username</p>
                                        <p>
                                            <asp:TextBox ID="txtAdminUserName" runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtAdminUserName_TextBoxWatermarkExtender" runat="server"
                                                Enabled="True" TargetControlID="txtAdminUserName" WatermarkText="enter username"
                                                WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                        </p>
                                        <p>
                                            Password</p>
                                        <p>
                                            <asp:TextBox ID="txtAdminPassword" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:TextBoxWatermarkExtender ID="txtAdminPassword_TextBoxWatermarkExtender" runat="server"
                                                Enabled="True" TargetControlID="txtAdminPassword" WatermarkText="Enter password"
                                                WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                        </p>
                                        <p>
                                            <asp:CheckBox ID="cbAdminRememberMe" CssClass="remember-check" Text="Remember my login details"
                                                runat="server" />
                                        </p>
                                        <p>
                                            <asp:Label ID="lbAdminError" runat="server" CssClass="error-label" Text=""></asp:Label>
                                            <asp:Label ID="lbAdminLoginAttempts" runat="server" CssClass="login-attempts-label"
                                                Text=""></asp:Label>
                                        </p>
                                        <p>
                                            <uc:HR_UpdateProgress ID="HR_UpdateProgress2" runat="server" />
                                            <asp:Button ID="lnkBtnAdminLogin" OnClick="lnkBtnAdminLogin_Click" Text="Admin Login"
                                                ToolTip="Click to login" runat="server" />
                                        </p>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="container">
                    Copyright &copy; 2014 -
                    <%=DateTime.Now.Year%>
                    Adept Systems. All rights reserved
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
