﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Training.aspx.cs" Inherits="AdeptHRManager.User_Module.Training" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel-details">
        <asp:GridView ID="gvTrainingAttended" runat="server" AllowPaging="True" Width="100%"
            AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
            EmptyDataText="No training records available!">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1+"." %>
                    </ItemTemplate>
                    <ItemStyle Width="4px" />
                </asp:TemplateField>
                <asp:BoundField DataField="StartDate" HeaderText="From Date" />
                <asp:BoundField DataField="EndDate" HeaderText="To Date" />
                <asp:BoundField DataField="CourseTitle" HeaderText="Course" />
                <asp:BoundField DataField="Certified" HeaderText="Certified?" />
                <asp:BoundField DataField="Certification" HeaderText="Certification" />
            </Columns>
            <FooterStyle CssClass="PagerStyle" />
            <AlternatingRowStyle CssClass="AltRowStyle" />
        </asp:GridView>
    </div>
</asp:Content>
