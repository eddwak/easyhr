﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Education_Background.aspx.cs" Inherits="AdeptHRManager.User_Module.Education_Background" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel-details">
        <asp:GridView ID="gvEducationBackground" runat="server" AllowPaging="True" Width="100%"
            AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
            EmptyDataText="No education background details available!">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1+"." %>
                    </ItemTemplate>
                    <ItemStyle Width="4px" />
                </asp:TemplateField>
                <asp:BoundField DataField="EducationQualificationType" HeaderText="Qualification" />
                <asp:BoundField DataField="InstitutionName" HeaderText="Institution" />
                <asp:BoundField DataField="LevelAttained" HeaderText="LevelAttained" />
                <asp:BoundField DataField="Award" HeaderText="Award" />
                <asp:BoundField DataField="QualificationStatus" HeaderText="Status" />
                <asp:BoundField DataField="AwardYear" HeaderText="Award Year" />
                <asp:BoundField DataField="GradeAttained" HeaderText="Grade/GPA" />
                <asp:BoundField DataField="ProfessionalMembership" HeaderText="Professional Membership" />
                <asp:BoundField DataField="IsCertificateSubmitted" HeaderText="Certificate Submitted?" />
                <asp:BoundField DataField="IsVerificationDone" HeaderText="Verification Done?" />
                <asp:BoundField DataField="IsAJobRequirement" HeaderText="Is A Job Requirement?" />
                <asp:BoundField DataField="IsHighestQualification" HeaderText="Is Highest Qualification?" />
            </Columns>
            <FooterStyle CssClass="PagerStyle" />
            <AlternatingRowStyle CssClass="AltRowStyle" />
        </asp:GridView>
    </div>
</asp:Content>
