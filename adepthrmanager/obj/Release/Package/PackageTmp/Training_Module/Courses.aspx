﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Training_Module/TrainingModule.master"
    AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="AdeptHRManager.Training_Module.Courses" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrainingModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelCourses" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelCourses" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new course"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit course details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View  course details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete course" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="search-div">
                    Search:
                    <asp:TextBox ID="txtSearch" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"
                        runat="server"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by reference number/course title:"
                        WatermarkCssClass="water-mark-text-extender">
                    </asp:TextBoxWatermarkExtender>
                    <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetCourses"
                        TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                        CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                        CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                    </asp:AutoCompleteExtender>
                    <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                        width: 343px !important;">
                    </div>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvCourses" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No courses available!"
                        OnPageIndexChanging="gvCourses_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("CourseID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CourseRef" HeaderText="Reference" />
                            <asp:BoundField DataField="CourseTitle" HeaderText="Course Title" />
                            <asp:BoundField DataField="Requirements" HeaderText="Requirements" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddCourse" Style="display: none; width: 600px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddCourse" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddCourseHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddCourseHeader" runat="server" Text="Add New Course"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddCourse" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="panel-details">
                        <fieldset>
                            <legend>Course Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Course Reference:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseReference" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Course Title:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseTitle" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Requirements:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseRequirements" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbCoursesError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldCourseID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddCoursePopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <div class="linkBtn">
                                <asp:LinkButton ID="lnkBtnSaveCourse" OnClick="lnkBtnSaveCourse_Click" ToolTip="Save Course"
                                    runat="server">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                    Save</asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnUpdateCourse" OnClick="lnkBtnUpdateCourse_Click" CausesValidation="false"
                                    ToolTip="Update Course" Enabled="false" runat="server">
                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                        ImageAlign="AbsMiddle" />
                                    Update</asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnClearCourse" OnClick="lnkBtnClearCourse_Click" CausesValidation="false"
                                    ToolTip="Clear Course" runat="server">
                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                        ImageAlign="AbsMiddle" />
                                    Clear</asp:LinkButton>
                            </div>
                        </fieldset>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddCourse" RepositionMode="None" X="200"
                    Y="30" TargetControlID="HiddenFieldAddCoursePopup" PopupControlID="PanelAddCourse"
                    CancelControlID="ImageButtonCloseAddCourse" BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddCourse" TargetControlID="PanelAddCourse"
                    DragHandleID="PanelDragAddCourse" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
