﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Training_Module/TrainingModule.master"
    AutoEventWireup="true" CodeBehind="Training_Schedule.aspx.cs" Inherits="AdeptHRManager.Training_Module.Training_Schedule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrainingModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelTrainingSchedule" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelTrainingSchedule" runat="server">
                <asp:Panel ID="PanelTrainingScheduleList" Visible="true" runat="server">
                    <div class="content-menu-bar">
                        <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new course"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                            Add New</asp:LinkButton>
                        <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit course details"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                            Edit</asp:LinkButton>
                        <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View  course details"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                            View</asp:LinkButton>
                        <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                            ToolTip="Delete course" runat="server">
                            <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                            Delete</asp:LinkButton>
                    </div>
                    <div class="search-div">
                        Search:
                        <asp:TextBox ID="txtSearch" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"
                            runat="server"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                            Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by reference number/course title:"
                            WatermarkCssClass="water-mark-text-extender">
                        </asp:TextBoxWatermarkExtender>
                        <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                            Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetCourses"
                            TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                            CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                            CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                        </asp:AutoCompleteExtender>
                        <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                            width: 343px !important;">
                        </div>
                    </div>
                    <div class="panel-details">
                        <asp:GridView ID="gvTrainingSchedules" runat="server" AllowPaging="True" Width="100%"
                            AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                            EmptyDataText="No training schedules available!" OnPageIndexChanging="gvTrainingSchedules_PageIndexChanging"
                            OnLoad="gvTrainingSchedules_OnLoad">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1+"." %>
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("TrainingScheduleID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="CourseRef" HeaderText="Course Ref." />
                                <asp:BoundField DataField="CourseTitle" HeaderText="Course Title" />
                                <asp:BoundField DataField="Trainer" HeaderText="Trainer" />
                                <asp:BoundField DataField="CourseVenue" HeaderText="Venue" />
                                <asp:BoundField DataField="Duration" HeaderText="Duration" />
                                <asp:BoundField DataField="TrainingSchedule" HeaderText="Training Schedule" />
                            </Columns>
                            <FooterStyle CssClass="PagerStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelAddTrainingSchedule" Visible="false" runat="server">
                    <div class="panel-details-header">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 57%;">
                                    <asp:Image ID="ImageAddTrainingScheduleHeader" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                    <asp:Label ID="lbAddTrainingScheduleHeader" runat="server" Text="Add New Training Schedule"></asp:Label>
                                </td>
                                <td>
                                    <asp:Image ID="Image6" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                    <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk (*) are required"></asp:Label>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonCloseAddTrainingSchedule" OnClick="ImageButtonCloseAddTrainingSchedule_Click"
                                        ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-details">
                        <fieldset>
                            <legend>Training Schedule Details</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Course Title:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCourseTitle" OnSelectedIndexChanged="ddlCourseTitle_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="0">-Select Course Title-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Course Reference:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseReference" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Requirements:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseRequirements" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Trainer:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTrainer" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Venue:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlTrainingVenue" runat="server">
                                            <asp:ListItem Value="0">-Select Training Venue-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Start Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTrainingStartDate" Width="150px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonTrainingStartDate" ToolTip="Pick training start date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtTrainingStartDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtTrainingStartDate" PopupButtonID="ImageButtonTrainingStartDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtTrainingStartDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtTrainingStartDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtTrainingStartDate_MaskedEditValidator" runat="server"
                                            ControlExtender="txtTrainingStartDate_MaskedEditExtender" ControlToValidate="txtTrainingStartDate"
                                            CssClass="errorMessage" ErrorMessage="txtTrainingStartDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid start date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                    <td>
                                        End Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTrainingEndDate" Width="150px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonTrainingEndDate" ToolTip="Pick training end date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtTrainingEndDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtTrainingEndDate" PopupButtonID="ImageButtonTrainingEndDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtTrainingEndDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtTrainingEndDate"
                                            UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtTrainingEndDate_MaskedEditValidator" runat="server"
                                            ControlExtender="txtTrainingStartDate_MaskedEditExtender" ControlToValidate="txtTrainingEndDate"
                                            CssClass="errorMessage" ErrorMessage="txtTrainingStartDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid end date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Training Status:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblTrainingStatus" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="Open">Open</asp:ListItem>
                                            <asp:ListItem Value="Closed">Closed</asp:ListItem>
                                            <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        Comments:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <div class="linkBtn">
                                <asp:HiddenField ID="HiddenFieldTrainingScheduleID" runat="server" />
                                <asp:LinkButton ID="lnkBtnSaveTrainingSchedule" OnClick="lnkBtnSaveTrainingSchedule_Click"
                                    ToolTip="Save training schedule" runat="server">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                    Save Training Schedule</asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnUpdateTrainingSchedule" OnClick="lnkBtnUpdateTrainingSchedule_Click"
                                    CausesValidation="false" ToolTip="Update Course" Enabled="false" runat="server">
                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                        ImageAlign="AbsMiddle" />
                                    Update Training Schedule</asp:LinkButton>
                            </div>
                        </fieldset>
                        <asp:Panel ID="PanelAddCourseTrainingAttendees" runat="server">
                            <fieldset>
                                <legend>Training Attendee Details</legend>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Department:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAttendeeDepartment" OnSelectedIndexChanged="ddlAttendeeDepartment_SelectedIndexChanged"
                                                AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0">-Select Department-</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            Staff Name:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAttendeeStaff" runat="server">
                                                <asp:ListItem Value="0">-Select Staff Name-</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Has Attended?<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rblAttendeeAttended" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            Certified?
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rblAttendeeCertified" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Certification:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCertification" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Comments:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAttendeeComments" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkBtn">
                                    <asp:HiddenField ID="HiddenFieldCourseTrainingAttendeeID" runat="server" />
                                    <asp:LinkButton ID="lnkBtnSaveCourseTrainingAttendee" OnClick="lnkBtnSaveCourseTrainingAttendee_Click"
                                        Enabled="false" ToolTip="Save Attendee" runat="server">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                        Save Attendee</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnUpdateCourseTrainingAttendee" OnClick="lnkBtnUpdateCourseTrainingAttendee_Click"
                                        CausesValidation="false" ToolTip="Update Attendee" Enabled="false" runat="server">
                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                            ImageAlign="AbsMiddle" />
                                        Update Attendee</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnClearCourseTrainingAttendee" OnClick="lnkBtnClearCourseTrainingAttendee_Click"
                                        Visible="false" CausesValidation="false" ToolTip="Clear course attendee" runat="server">
                                        <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                            ImageAlign="AbsMiddle" />
                                        Clear</asp:LinkButton>
                                </div>
                            </fieldset>
                        </asp:Panel>
                        <fieldset>
                            <legend>Training Attendees</legend>
                            <asp:GridView ID="gvCourseTrainingAttendees" runat="server" AllowPaging="True" Width="100%"
                                AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                EmptyDataText="No training attendees available!" OnRowCommand="gvCourseTrainingAttendees_RowCommand"
                                OnPageIndexChanging="gvCourseTrainingAttendees_PageIndexChanging" OnLoad="gvCourseTrainingAttendees_OnLoad">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1+"." %>
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("CourseTrainingAttendeeID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="4px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="StaffName" HeaderText="Name" />
                                    <asp:BoundField DataField="DepartmentName" HeaderText="Department" />
                                    <asp:BoundField DataField="Attended" HeaderText="Attended?" />
                                    <asp:BoundField DataField="Comments" HeaderText="Comments" />
                                    <asp:BoundField DataField="Certified" HeaderText="Certified?" />
                                    <asp:BoundField DataField="Certification" HeaderText="Certification" />
                                    <asp:ButtonField HeaderText="Edit" CommandName="EditCourseTrainingAttendee" ItemStyle-Width="4px"
                                        Text="Edit" />
                                    <asp:ButtonField HeaderText="Delete" CommandName="DeleteCourseTrainingAttendee" ItemStyle-Width="4px"
                                        Text="Delete" />
                                </Columns>
                                <FooterStyle CssClass="PagerStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnClearForm" OnClick="lnkBtnClearForm_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucDeleteCourseTrainingAttendee" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
