﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Medical_Benefits.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Medical_Benefits" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelMedicalBenefits" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMedicalBenefits" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new medical benefit"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit medical benefit details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View medical benefit details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete medical benefit" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvMedicalBenefits" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                        EmptyDataText="No medical benefits records available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("MedicalBenefitID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="MedicalBenefit" HeaderText="Medical Benefit" />
                            <asp:BoundField DataField="MedicalBenefitDescription" HeaderText="Description" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddMedicalBenefit" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddMedicalBenefit" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddMedicalBenefitHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddMedicalBenefitHeader" runat="server" Text="Add New Medical Benefit"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddMedicalBenefit" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Medical Benefit Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Medical Benefit:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMedicalBenefit" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldMedicalBenefitID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddMedicalBenefitPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveMedicalBenefit" OnClick="lnkBtnSaveMedicalBenefit_Click"
                                ToolTip="Save medical benefit" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateMedicalBenefit" OnClick="lnkBtnUpdateMedicalBenefit_Click"
                                CausesValidation="false" ToolTip="Update medical benefit" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearMedicalBenefit" OnClick="lnkBtnClearMedicalBenefit_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddMedicalBenefit" RepositionMode="None"
                    X="270" Y="110" TargetControlID="HiddenFieldAddMedicalBenefitPopup" PopupControlID="PanelAddMedicalBenefit"
                    CancelControlID="ImageButtonCloseAddMedicalBenefit" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddMedicalBenefit" TargetControlID="PanelAddMedicalBenefit"
                    DragHandleID="PanelDragAddMedicalBenefit" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
