﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="General_Settings.aspx.cs" Inherits="AdeptHRManager.Settings_Module.General_Settings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelGeneralSettings" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelGeneralSettings" runat="server">
                <asp:TabContainer ID="TabContainerGeneralSettings" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelEmailSettings" runat="server" CssClass="ajax-tab" HeaderText="Email Settings">
                        <HeaderTemplate>
                            Email Settings
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <fieldset>
                                    <legend>Email Sender Settings</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                SMTP Server Address:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSMTPServerAddress" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Port Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPortNumber" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Email Address:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Email Password:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmailPassword" TextMode="Password" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Confirm Email Password:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtConfirmEmailPassword" TextMode="Password" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:HiddenField ID="HiddenFieldEmailSettingID" runat="server" />
                                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                                <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div class="linkBtn">
                                                    <asp:LinkButton ID="lnkBtnSaveEmailSettings" OnClick="lnkBtnSaveEmailSettings_Click"
                                                        ToolTip="Save email settings" runat="server">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                        Save Email Settings</asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
