﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Nationality.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Nationality" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelNationality" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelNationality" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new nationality"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit nationality"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View nationality"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelView" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete nationality" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvNationalities" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                        EmptyDataText="No records available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("CountryID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Country" HeaderText="County" />
                            <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddNationality" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddNationality" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddNationalityHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddNationalityHeader" runat="server" Text="Add New Nationality"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddNationality" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Nationality Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Country:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nationality:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtNationality" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldNationalityID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddNationalityPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveNationality" OnClick=" lnkBtnSaveNationality_Click"
                                ToolTip="Save nationality" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateNationality" OnClick="lnkBtnUpdateNationality_Click"
                                CausesValidation="false" ToolTip="Update nationality" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearNationality" OnClick="lnkBtnClearNationality_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="ModalPopupExtenderAddNationality" RepositionMode="None"
                    X="270" Y="100" TargetControlID="HiddenFieldAddNationalityPopup" PopupControlID="PanelAddNationality"
                    CancelControlID="ImageButtonCloseAddNationality" BackgroundCssClass="modalback"
                    runat="server">
                </cc1:ModalPopupExtender>
                <cc1:DragPanelExtender ID="DragPanelExtenderAddNationality" TargetControlID="PanelAddNationality"
                    DragHandleID="PanelDragAddNationality" runat="server"></cc1:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
