﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.User_Module
{
    public partial class User_Document_Viewer : System.Web.UI.Page
    {

        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string _appraisalTemplateID = Request["doc_ref"].ToString();//
                    if (_appraisalTemplateID != null)
                    {
                        //download appraisal template documents
                        DownloadAppraisalTemplateDocument(Convert.ToInt32(_appraisalTemplateID));
                    }
                }
                catch { }
                try
                {
                    string _quarterlyReviewID = Request["quarterlyreview"].ToString();//
                    if (_quarterlyReviewID != null)
                    {
                        //download quaterly review document
                        DownloadQuarteltyReviewDocument(Convert.ToInt32(_quarterlyReviewID));
                    }
                }
                catch { }
            }
        }
        //download appraisal template document
        private void DownloadAppraisalTemplateDocument(int _appraisalTemplateID)
        {
            if (db.AppraisalTemplates.Any(p => p.appraisaltemplate_iAppraisalTemplateID == _appraisalTemplateID))
            {
                AppraisalTemplate getAppraisalTemplate = db.AppraisalTemplates.Single(p => p.appraisaltemplate_iAppraisalTemplateID == _appraisalTemplateID);
                if (getAppraisalTemplate.appraisaltemplate_vbDocument != null)
                {
                    string _docExtension = getAppraisalTemplate.appraisaltemplate_vDocExtension;
                    string _documentName = getAppraisalTemplate.appraisaltemplate_vDocName;
                    //get document content type as per the document's extension
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getAppraisalTemplate.appraisaltemplate_vbDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, "Download failed. There is no document available for the option selected. Contact the HR department!", this, PanelUserDocumentViewer);
                return;
            }

        }
        //download quaterly review document
        private void DownloadQuarteltyReviewDocument(int _quarterlyReviewID)
        {
            if (db.QuarterlyReviews.Any(p => p.QuarterlyReviewID == _quarterlyReviewID))
            {
                QuarterlyReview getQuarterlyReview = db.QuarterlyReviews.Single(p => p.QuarterlyReviewID == _quarterlyReviewID);
                if (getQuarterlyReview.StaffUploadedDocument != null)
                {
                    string _documentName = "", _docExtension = ".";
                    _documentName = getQuarterlyReview.StaffDocumentName;
                    _docExtension += _documentName.Split('.').LastOrDefault();
                    //get document content type as per the document's extension
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getQuarterlyReview.StaffUploadedDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, "Download failed. There is no document available!", this, PanelUserDocumentViewer);
                return;
            }
        }
    }
}