﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Family_Details : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayStaffSpouse();//display spouse details
                DisplayStaffChildren();//display children details
            }
        }
        //display staff spouse etails
        private void DisplayStaffSpouse()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                _display = db.PROC_StaffSpouse(_staffID);
                gvSpouseDetails.DataSourceID = null;
                gvSpouseDetails.DataSource = _display;
                gvSpouseDetails.DataBind();
                return;
            }
            catch { }
        }
        //display employee's child details
        public void DisplayStaffChildren()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                object _display;
                _display = db.PROC_StaffChildren(_staffID);
                gvChildDetails.DataSourceID = null;
                gvChildDetails.DataSource = _display;
                gvChildDetails.DataBind();
                return;
            }
            catch { }
        }
    }
}