﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Employment_Terms : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlJobTitle, 7000, "Job Title");//display job titles(7000) available
                _hrClass.GetListingItems(ddlContractType, 21000, "Contract Type");//display contract type(21000) available
                _hrClass.GetStaffNames(ddlReportsTo, "Reports To");//populate reports to dropdown
                LoadStaffEmploymentTerms();//Load Staff Personal Information
            }
        }
        private void LoadStaffEmploymentTerms()
        {
            try
            {
            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            StaffJobDetail getJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);
              int _JobDetailID = Convert.ToInt32(getJobDetail.JobDetailID.ToString());
            HttpContext.Current.Session["JobDetailID"] = _JobDetailID;
            if (getJobDetail.JoinDate != null)
                txtJoinDate.Text = _hrClass.ShortDateDayStart(getJobDetail.JoinDate.ToString());
            else txtJoinDate.Text = "";
            if (getJobDetail.JobTitleID != null)
                ddlJobTitle.SelectedValue = getJobDetail.JobTitleID.ToString();
            else ddlJobTitle.SelectedIndex = 0;
            if (getJobDetail.ContractTypeID != null)
                ddlContractType.SelectedValue = getJobDetail.ContractTypeID.ToString();
            else ddlContractType.SelectedIndex = 0;
            if (getJobDetail.ReportToStaffID != null)
                ddlReportsTo.SelectedValue = getJobDetail.ReportToStaffID.ToString();
            else ddlReportsTo.SelectedIndex = 0;
            txtJobRole.Text = getJobDetail.JobRole.ToString();

            EmpTerm getEmploymentTerms = db.EmpTerms.Single(p => p.empterms_uiStaffID == _staffID);
            txtEmploymentTermsBasicPay.Text = (getEmploymentTerms.empterms_mBasicPay).ToString();
            //txtEmploymentTermsBasicPay.Text = _hrClass.ConvertToCurrencyValue(_basicPay).Replace(",", "");
            JobContractDate getJobContractDate = db.JobContractDates.FirstOrDefault(p => p.JobDetailsID == _JobDetailID);
            if (getJobContractDate.StartContractDate != null)
                txtContractStartDate.Text = _hrClass.ShortDateDayStart(getJobContractDate.StartContractDate.ToString());
            else txtContractStartDate.Text = "";
            if (getJobContractDate.EndContractDate != null)
                txtContractEndDate.Text = _hrClass.ShortDateDayStart(getJobContractDate.EndContractDate.ToString());
            else txtContractEndDate.Text = "";



            }

            catch { }

        }
    }
}