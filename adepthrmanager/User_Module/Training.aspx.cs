﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Training : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayTrainingAttended();//display training attended by the staff
            }

        }
        //display training attended details
        private void DisplayTrainingAttended()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                _display = db.PROC_StaffTrainingAttended(_staffID);
                gvTrainingAttended.DataSourceID = null;
                gvTrainingAttended.DataSource = _display;
                gvTrainingAttended.DataBind();
                return;
            }
            catch { }
        }

        //    //load test training attended
        //    private void LoadTrainingAttended()
        //    {
        //        DataTable dt = new DataTable();
        //        dt.Columns.Add(new DataColumn("TrainingAttendedID", typeof(int)));
        //        dt.Columns.Add(new DataColumn("FromDate", typeof(string)));
        //        dt.Columns.Add(new DataColumn("ToDate", typeof(string)));
        //        dt.Columns.Add(new DataColumn("CourseName", typeof(string)));
        //        dt.Columns.Add(new DataColumn("Certification", typeof(string)));
        //        DataRow dr = dt.NewRow();
        //        dr[0] = 1;
        //        dr[1] = "01/Jan/2011";
        //        dr[2] = "31/Jun/2011";
        //        dr[3] = "Oracle";
        //        dr[4] = "OCA";
        //        dt.Rows.Add(dr);

        //        DataRow dr2 = dt.NewRow();
        //        dr2[0] = 2;
        //        dr2[1] = "11/Aug/2013";
        //        dr2[2] = "17/Aug/2014";
        //        dr2[3] = "Accounting";
        //        dr2[4] = "CPA";
        //        dt.Rows.Add(dr2);

        //        gvTrainingAttended.DataSource = dt;
        //        gvTrainingAttended.DataBind();
        //    }
    }
}