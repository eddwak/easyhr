﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;
using System.Data;
using System.Text;

namespace AdeptHRManager.User_Module
{
    public partial class Performance_Review : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropDowns();//populate dropdowns
                LoadStaffPerformanceReview();//display staff performance review details
                DisplayRatingScales();//load rating scale
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            //add confirm delete KPI event
            ucConfirmDeleteKPI.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmationDeleteKPI_Click);
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        //load staff perfomance reive
        private void LoadStaffPerformanceReview()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                DisplayStaffQuarterlyReviewDocuments(_staffID);//display quartely review documents
                string _performanceYear = DateTime.Now.Year.ToString();
                DisplayPerformanceObjectives(_staffID, _performanceYear);//populate perforfomance objectives plan for the current year
                DisplayPerformanceObjectivesForPrint(_staffID, _performanceYear);//populate gridview for print
                DisplayStaffToAppraise(_staffID);//display staff to appraise
            }
            catch { }
        }
        //populate drop downs
        private void PopulateDropDowns()
        {
            _hrClass.GenerateYears(ddlAnnualPerformancePlanYear);//populate annual years
            ddlAnnualPerformancePlanYear.SelectedValue = DateTime.Now.Year.ToString();//select current year
            //_hrClass.GenerateYears(ddlQuarterlyReviewYear);//display Quarterly review years
            _hrClass.GenerateYears(ddlAnnualAppraisalYear);//display annual appraisal years
            GetAppraisalTemplates();//get appraisal templates
            //_hrClass.GetStaffNames(ddlQuarter1AppraisedBy, "Appraised By");//populate  quarter 1 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter2AppraisedBy, "Appraised By");//populate  quarter 2 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter3AppraisedBy, "Appraised By");//populate  quarter 3 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter4AppraisedBy, "Appraised By");//populate  quarter 4 appraised by dropdown
        }
        //populate documents dropdown
        private void GetAppraisalTemplates()
        {
            try
            {
                ddlAppraisalTemplates.Items.Clear();
                object _getStaff;
                _getStaff = db.AppraisalTemplates.Select(p => p);
                ddlAppraisalTemplates.DataSource = _getStaff;
                ddlAppraisalTemplates.Items.Insert(0, new ListItem("--"));
                ddlAppraisalTemplates.DataTextField = "appraisaltemplate_vTemplateTitle";
                ddlAppraisalTemplates.DataValueField = "appraisaltemplate_iAppraisalTemplateID";
                ddlAppraisalTemplates.AppendDataBoundItems = true;
                ddlAppraisalTemplates.DataBind();
                return;
            }
            catch { }
        }
        //load staff performance details for the selected year
        protected void ddlAnnualPerformancePlanYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "An error occured. There is no performance year selected. Select a performance year!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")
                    {
                        _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                        string _staffName = _hrClass.getStaffNames(_staffID);
                    }
                    DisplayPerformanceObjectives(_staffID, _performanceYear);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        //download the appraisal template selected
        protected void ddlAppraisalTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _appraisalTemplateID = 0;

                if (ddlAppraisalTemplates.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No template selected. Select a template document that you want to downlod!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    _appraisalTemplateID = Convert.ToInt32(ddlAppraisalTemplates.SelectedValue);
                    //load the document that has been uploaded
                    string strPageUrl = "user_document_viewer.aspx?doc_ref=" + _appraisalTemplateID.ToString();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    return;
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        #region PERFORMANCE PLAN
        //add perfomance objective details
        protected void LinkButtonNewPerformanceObjective_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderAddPerformanceObjective.Show();
            ClearAddPerformanceObjectiveControls();
            return;
        }
        //clear add performance objective controls
        private void ClearAddPerformanceObjectiveControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddPerformanceObjective);
            HiddenFieldPerformanceObjectiveID.Value = "";
            lnkBtnSavePerformanceObjective.Visible = true;
            lnkBtnUpdatePerformanceObjective.Enabled = false;
            lnkBtnUpdatePerformanceObjective.Visible = true;
            lnkBtnClearPerformanceObjective.Visible = true;
            lbAddPerformanceObjectiveHeader.Text = "Add Performance Objective";
            ImageAddPerformanceObjectiveHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate performance objective controls
        private string ValidatePerformanceObjectiveControls()
        {
            if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
            {
                ddlAnnualPerformancePlanYear.Focus();
                return "Select annual performance plan year!";
            }
            else if (txtPerformanceObjective.Text.Trim() == "")
            {
                txtPerformanceObjective.Focus();
                return "Enter performance objective!";
            }
            else if (txtPerformanceObjectiveWeight.Text.Trim() == "")
            {
                txtPerformanceObjectiveWeight.Focus();
                return "Enter percentage weight for the perfomance objective!";
            }
            else if (_hrClass.isNumberValid(txtPerformanceObjectiveWeight.Text.Trim()) == false)
            {
                txtPerformanceObjectiveWeight.Focus();
                return "Invalid percentage weight entered. Enter a numeric value for the percentage weight!";
            }
            else if (Convert.ToDouble(txtPerformanceObjectiveWeight.Text.Trim()) > 100)
            {
                txtPerformanceObjectiveWeight.Focus();
                return "Invalid percentage weight entered. Weight percentage cannot be over 100%.";
            }
            else if (Convert.ToDouble(txtPerformanceObjectiveWeight.Text.Trim()) <= 0)
            {
                txtPerformanceObjectiveWeight.Focus();
                return "Invalid percentage weight entered. Weight percentage cannot be less than or equal to zero.";
            }
            else
                return "";
        }

        //save performance objective details
        private void SavePerformanceObjective()
        {
            ModalPopupExtenderAddPerformanceObjective.Show();
            try
            {
                string _error = ValidatePerformanceObjectiveControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelPerformanceReview); return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    string _performanceYear = "", _performanceObjective;
                    _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                    _performanceObjective = txtPerformanceObjective.Text.Trim();
                    Double? _annualWeight = null;
                    _annualWeight = Convert.ToDouble(txtPerformanceObjectiveWeight.Text.Trim());
                    //check if the performance objective being entered already exists
                    if (db.PerformanceObjectives.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceObjectiveName.ToLower() == _performanceObjective.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed . Performance objective you are trying to save already exists!", this, PanelPerformanceReview);
                        return;
                    }
                   
                    else
                    {//Check if the Sum of the annual weight is more than 100%
                        PROC_PerformanceObjectiveCheckSumResult getPerformanceObjective = db.PROC_PerformanceObjectiveCheckSum(_staffID, _performanceYear).FirstOrDefault();

                        double? _StoredSum = Convert.ToDouble(getPerformanceObjective.PerformanceObjectiveWeightSUM);
                        double? _CheckSum = (_StoredSum + _annualWeight);
                        if (_CheckSum > 100)
                        {
                             _hrClass.LoadHRManagerMessageBox(1, "Save failed.Please ensure the total percentage weight is not more than 100!", this, PanelPerformanceReview);
                            return;
                        }
                        else
                        {

                            //save the performance objective for the staff
                            PerformanceObjective updatePerformanceObjective = new PerformanceObjective();
                            updatePerformanceObjective.StaffID = _staffID;
                            updatePerformanceObjective.PerformanceYear = _performanceYear;
                            updatePerformanceObjective.PerformanceObjectiveName = _performanceObjective;
                            updatePerformanceObjective.PerformanceObjectiveWeight = _annualWeight;
                            db.PerformanceObjectives.InsertOnSubmit(updatePerformanceObjective);
                            db.SubmitChanges();
                            HiddenFieldPerformanceObjectiveID.Value = updatePerformanceObjective.PerformanceObjectiveID.ToString();
                            _hrClass.LoadHRManagerMessageBox(2, "Performance objective has been successfuly saved!", this, PanelPerformanceReview);
                            lnkBtnUpdatePerformanceObjective.Enabled = true;
                            //populate the performance objectives
                            if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")//check of were are appraising a staff
                                _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                            DisplayPerformanceObjectives(_staffID, _performanceYear);//refresh objectives
                            DisplayPerformanceObjectivesForPrint(_staffID, _performanceYear);//populate gridview for print
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }

        }

        //new performance objective details
        private void UpdatePerformanceObjective()
        {
            ModalPopupExtenderAddPerformanceObjective.Show();
            try
            {
                string _error = ValidatePerformanceObjectiveControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelPerformanceReview); return;
                }
                else
                {

                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    string _performanceYear = "", _performanceObjective;
                    _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                    _performanceObjective = txtPerformanceObjective.Text.Trim();
                    Double? _annualWeight = null;
                    _annualWeight = Convert.ToDouble(txtPerformanceObjectiveWeight.Text.Trim());
                    int _performanceObjectiveID = Convert.ToInt32(HiddenFieldPerformanceObjectiveID.Value);
                    //check if the performance objective being entered already exists
                    if (db.PROC_StaffPerformanceObjective(_staffID, _performanceYear).Any(p => p.PerformanceObjectiveID != _performanceObjectiveID && p.PerformanceObjectiveName.ToLower() == _performanceObjective.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Update failed . Performance objective you are tyring to new with already exists!", this, PanelPerformanceReview);
                        return;
                    }
                    else
                    {//Check if the Sum of the annual weight is more than 100%
                        PROC_PerformanceObjectiveCheckSumResult getPerformanceObjective = db.PROC_PerformanceObjectiveCheckSum(_staffID, _performanceYear).FirstOrDefault();

                        double? _StoredSum = Convert.ToDouble(getPerformanceObjective.PerformanceObjectiveWeightSUM);
                        double? _CheckSum = (_StoredSum + _annualWeight);
                        if (_CheckSum > 100)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed.Please ensure the total percentage weight is not more than 100!", this, PanelPerformanceReview);
                            return;
                        }
                        else
                        {
                            //save the performance objective for the staff
                            PerformanceObjective updatePerformanceObjective = db.PerformanceObjectives.Single(p => p.PerformanceObjectiveID == _performanceObjectiveID);
                            updatePerformanceObjective.StaffID = _staffID;
                            updatePerformanceObjective.PerformanceYear = _performanceYear;
                            updatePerformanceObjective.PerformanceObjectiveName = _performanceObjective;
                            updatePerformanceObjective.PerformanceObjectiveWeight = _annualWeight;
                            db.SubmitChanges();
                            _hrClass.LoadHRManagerMessageBox(2, "Performance objective has been successfuly updated!", this, PanelPerformanceReview);

                            //populate the perfomance objectives
                            if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")//check of were are appraising a staff
                                _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                            DisplayPerformanceObjectives(_staffID, _performanceYear);
                            DisplayPerformanceObjectivesForPrint(_staffID, _performanceYear);//populate gridview for print
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }

        }
        //save perfomance objective details
        protected void lnkBtnSavePerformanceObjective_Click(object sender, EventArgs e)
        {
            SavePerformanceObjective();//save performance objective details
            return;
        }
        //update  performance objective details
        protected void lnkBtnUpdatePerformanceObjective_Click(object sender, EventArgs e)
        {
            UpdatePerformanceObjective();//new performance details
            return;
        }
        //clear performance objective details
        protected void lnkBtnClearPerformanceObjective_Click(object sender, EventArgs e)
        {
            ClearAddPerformanceObjectiveControls();
            ModalPopupExtenderAddPerformanceObjective.Show();
            return;
        }


        // populate the edit performance objective controls
        private void LoadEditPerformanceObjectiveControls(int _performanceObjectiveID)
        {
            ClearAddPerformanceObjectiveControls();
            PerformanceObjective getPerfomanceObjective = db.PerformanceObjectives.Single(p => p.PerformanceObjectiveID == _performanceObjectiveID);
            HiddenFieldPerformanceObjectiveID.Value = _performanceObjectiveID.ToString();
            txtPerformanceObjective.Text = getPerfomanceObjective.PerformanceObjectiveName;
            txtPerformanceObjectiveWeight.Text = getPerfomanceObjective.PerformanceObjectiveWeight.ToString();

            lnkBtnSavePerformanceObjective.Visible = false;
            lnkBtnClearPerformanceObjective.Visible = true;
            lnkBtnUpdatePerformanceObjective.Enabled = true;
            lbAddPerformanceObjectiveHeader.Text = "Edit Performance Objective";
            ImageAddPerformanceObjectiveHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddPerformanceObjective.Show();
            return;
        }
        //check if a perfomance objective is selected before edit details controls are loaded
        protected void LinkButtonEditPerformanceObjective_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvPerformanceObjectives))//check if a perfomance objective is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a performance objective to edit !", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvPerformanceObjectives.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one performance objective record. Select only one performance objective to edit at a time !", this, PanelPerformanceReview);
                        return;
                    }
                    foreach (GridViewRow _grv in gvPerformanceObjectives.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFPerformanceObjectiveID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edsit performance objective
                            LoadEditPerformanceObjectiveControls(Convert.ToInt32(_HFPerformanceObjectiveID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading performance objective for edit! " + ex.Message.ToString(), this, PanelPerformanceReview);
            }
        }
        //display staff performance objective details
        private void DisplayPerformanceObjectives(Guid _staffID, string _year)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffPerformanceObjective(_staffID, _year);
                gvPerformanceObjectives.DataSourceID = null;
                gvPerformanceObjectives.DataSource = _display;
                Session["gvPerformanceObjectivesData"] = _display;
                gvPerformanceObjectives.DataBind();
                return;
            }
            catch { }
        }
        double sumFooterValue = 0;
        double avgFooterValue = 0;
        protected void gvPerformanceObjectives_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string _percentageWeight = ((Label)e.Row.FindControl("LabelWeight")).Text;
                sumFooterValue += Convert.ToDouble(_percentageWeight);
                avgFooterValue++;
            }
            //create 
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl = (Label)e.Row.FindControl("LabelTotalWeight");
                lbl.Text = "Total="+sumFooterValue.ToString();
                Label lblAvg = (Label)e.Row.FindControl("LabelAvgWeight");
                lblAvg.Text = "Avg=" + (sumFooterValue / avgFooterValue);
            }


            ////populate kpis
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    HiddenField _hfPerformanceObjectiveID = (HiddenField)e.Row.FindControl("HiddenField1");
            //    int _perfomanceObjectiveID = Convert.ToInt32(_hfPerformanceObjectiveID.Value);
            //    if (db.PROC_PerformanceObjectiveKPIsTargetPlan(_perfomanceObjectiveID).Any())
            //    {
            //        Label lblKPI = (Label)e.Row.FindControl("LabelKPIs");
            //        int count = 1;
            //        foreach (PROC_PerformanceObjectiveKPIsTargetPlanResult getKPI in db.PROC_PerformanceObjectiveKPIsTargetPlan(_perfomanceObjectiveID))
            //        {
            //            lblKPI.Text += count + ". " + getKPI.PerformanceKPI + "<br/>";
            //            count++;
            //        }
            //    }
            //}
        }


        //load perfomance details 
        protected void gvPerformanceObjectives_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("LoadPerformanceObjectiveKPIs") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFPerformanceObjectiveID = (HiddenField)gvPerformanceObjectives.Rows[ID].FindControl("HiddenField1");
                    int _performanceObjectiveID = Convert.ToInt32(_HFPerformanceObjectiveID.Value);
                    LoadPerformanceObjectiveKPIs(_performanceObjectiveID);//load KPIs associated with the performance objective
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //load delete performance objective plan details
        protected void LinkButtonDeletePerformanceObjective_Click(object sender, EventArgs e)
        {
            try
            {
                if (CBSignOff.Checked == true)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "The performance plan has been signed off and can't be deleted!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                if (!_hrClass.isGridviewItemSelected(gvPerformanceObjectives))//check if there is any record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the objective that you want to delete!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvPerformanceObjectives.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected performance objective?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected performance objectives?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
            

        //method for deleting an qperformance plan objective
        private void DeletePerformancePlanObjective()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvPerformanceObjectives.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;

                    if (_cb.Checked)
                    {
                        HiddenField _HFPerformanceObjectiveID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _performanceObjectiveID = Convert.ToInt32(_HFPerformanceObjectiveID.Value);
                        //check if the performance objective has gott any KPI under it
                        if (db.PerformanceObjectiveKPIs.Any(p => p.PerformanceObjectiveID == _performanceObjectiveID))
                        {
                            //delete the KPIs
                            foreach (PerformanceObjectiveKPI deleteKPI in db.PerformanceObjectiveKPIs.Where(p => p.PerformanceObjectiveID == _performanceObjectiveID))
                            {
                                db.PerformanceObjectiveKPIs.DeleteOnSubmit(deleteKPI);
                            }
                        }

                        PerformanceObjective deletePerformanceObjective = db.PerformanceObjectives.Single(p => p.PerformanceObjectiveID == _performanceObjectiveID);
                        db.PerformanceObjectives.DeleteOnSubmit(deletePerformanceObjective);
                        db.SubmitChanges();
                    }
                }
                //refresh performance objectives after delete
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                DisplayPerformanceObjectives(_staffID, DateTime.Now.Year.ToString());
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected performance objective has been deleted.", this, PanelPerformanceReview);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected performance objectives have been deleted.", this, PanelPerformanceReview);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed . " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //add perfomance objective KPI details
        protected void LinkButtonNewKPI_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderAddKPI.Show();
            ClearAddKPIControls();
            return;
        }
        //clear add performance objective KPI controls
        private void ClearAddKPIControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddKPI);
            HiddenFieldKPIID.Value = "";
            ActivateOrDisableDateEntryControls("");//clear
            lnkBtnSaveKPI.Visible = true;
            lnkBtnUpdateKPI.Enabled = false;
            lnkBtnUpdateKPI.Visible = true;
            lnkBtnClearKPI.Visible = true;
            lbAddKPIHeader.Text = "Add Performance Objective KPI";
            ImageAddKPIHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        }
        //load performance objective KPI plan for edit
        private void LoadPerformanceObjectiveKPIs(int _performanceObjectiveID)
        {
            PerformanceObjective getPerformanceObjective = db.PerformanceObjectives.Single(p => p.PerformanceObjectiveID == _performanceObjectiveID);
            HiddenFieldPerformanceObjectiveID.Value = _performanceObjectiveID.ToString();
            txtAnnualPerformanceObjective.Text = getPerformanceObjective.PerformanceObjectiveName;
            txtAnnualPerformanceObjectiveWeight.Text = getPerformanceObjective.PerformanceObjectiveWeight.ToString() + "%";

            //load KPIs already set
            DisplayPerformanceObjectiveKPIs(_performanceObjectiveID);
            lbAddPerformanceObjectiveKPIHeader.Text = getPerformanceObjective.PerformanceObjectiveName + " Key Performance Indicators";
            PanelAddPerformanceObjectiveKPI.Visible = true;
            PanelPerformanceObjectiveListing.Visible = false;
            return;
        }
        //validate add Key perfomance objective controls
        private string ValidateAddKPIControls()
        {
            if (txtKPIName.Text.Trim() == "")
            {
                txtKPIName.Focus();
                return "Enter key perfomance indicator!";
            }
            else if (ddlKPITargetMeasure.SelectedValue == "Date")
            {
                if (txtKPIAnnualTargetDate.Text.Trim() == "__/___/____")
                    return "Enter annual target date!";
                else if (txtKPIQuarter1TargetDate.Text.Trim() == "__/___/____")
                    return "Enter quarter 1 target date!";
                else if (txtKPIQuarter2TargetDate.Text.Trim() == "__/___/____")
                    return "Enter quarter 2 target date!";
                else if (txtKPIQuarter3TargetDate.Text.Trim() == "__/___/____")
                    return "Enter quarter 3 target date!";
                else if (txtKPIQuarter4TargetDate.Text.Trim() == "__/___/____")
                    return "Enter quarter 4 target date!";
                else if (_hrClass.isDateValid(txtKPIAnnualTargetDate.Text.Trim()) == false)
                    return "Invalid annual target date entered";
                else if (_hrClass.isDateValid(txtKPIQuarter1TargetDate.Text.Trim()) == false)
                    return "Invalid quarter 1 target date entered";
                else if (_hrClass.isDateValid(txtKPIQuarter2TargetDate.Text.Trim()) == false)
                    return "Invalid quarter 2 target date entered";
                else if (_hrClass.isDateValid(txtKPIQuarter3TargetDate.Text.Trim()) == false)
                    return "Invalid quarter 3 target date entered";
                else if (_hrClass.isDateValid(txtKPIQuarter4TargetDate.Text.Trim()) == false)
                    return "Invalid quarter 4 target date entered";
                else return "";

            }
            else
            {
                if (ddlKPITargetMeasure.SelectedIndex == 0)
                {
                    ddlKPITargetMeasure.Focus();
                    return "Select KPI target measure!";
                }
                else if (txtKPIAnnualTarget.Text.Trim() == "")
                {
                    txtKPIAnnualTarget.Focus();
                    return "Enter annual target!";
                }
                else if (txtKPIQuarter1Target.Text.Trim() == "")
                {
                    txtKPIQuarter1Target.Focus();
                    return "Enter quarter 1 target!";
                }
                else if (txtKPIQuarter2Target.Text.Trim() == "")
                {
                    txtKPIQuarter2Target.Focus();
                    return "Enter quarter 2 target!";
                }
                else if (txtKPIQuarter3Target.Text.Trim() == "")
                {
                    txtKPIQuarter3Target.Focus();
                    return "Enter quarter 3 target!";
                }
                else if (txtKPIQuarter4Target.Text.Trim() == "")
                {
                    txtKPIQuarter4Target.Focus();
                    return "Enter quarter 4 target!";
                }
                else if (ddlKPITargetMeasure.SelectedValue == "Number")
                {
                    if (_hrClass.isNumberValid(txtKPIAnnualTarget.Text.Trim()) == false || Convert.ToDecimal(txtKPIAnnualTarget.Text.Trim()) <= 0)
                        return "Invalid annnual tartget value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter1Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter1Target.Text.Trim()) <= 0)
                        return "Invalid quarter 1 target value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter2Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter2Target.Text.Trim()) <= 0)
                        return "Invalid quarter 2 target value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter3Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter3Target.Text.Trim()) <= 0)
                        return "Invalid quarter 3 target value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter4Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter4Target.Text.Trim()) <= 0)
                        return "Invalid quarter 4 target value entered!";
                    else return "";
                }
                else if (ddlKPITargetMeasure.SelectedValue == "Percentage")
                {
                    if (_hrClass.isNumberValid(txtKPIAnnualTarget.Text.Trim()) == false || Convert.ToDecimal(txtKPIAnnualTarget.Text.Trim()) <= 0 || Convert.ToDecimal(txtKPIAnnualTarget.Text.Trim()) > 100)
                        return "Invalid annnual target percentage value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter1Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter1Target.Text.Trim()) <= 0 || Convert.ToDecimal(txtKPIQuarter1Target.Text.Trim()) > 100)
                        return "Invalid quarter 1 target percentage value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter2Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter2Target.Text.Trim()) <= 0 || Convert.ToDecimal(txtKPIQuarter2Target.Text.Trim()) > 100)
                        return "Invalid quarter 2 target percentage value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter3Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter3Target.Text.Trim()) <= 0 || Convert.ToDecimal(txtKPIQuarter3Target.Text.Trim()) > 100)
                        return "Invalid quarter 3 target percentage value entered!";
                    else if (_hrClass.isNumberValid(txtKPIQuarter4Target.Text.Trim()) == false || Convert.ToDecimal(txtKPIQuarter4Target.Text.Trim()) <= 0 || Convert.ToDecimal(txtKPIQuarter4Target.Text.Trim()) > 100)
                        return "Invalid quarter 4 target percentage value entered!";
                    else return "";
                }
                else return "";
            }
        }
        //save new key perfomance objective details
        private void SaveNewKPI()
        {
            ModalPopupExtenderAddKPI.Show();
            try
            {
                string _error = ValidateAddKPIControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelPerformanceReview); return;
                }
                else
                {
                    string _KPIName = "", _targetMeasure;
                    _KPIName = txtKPIName.Text.Trim();
                    _targetMeasure = ddlKPITargetMeasure.SelectedValue;                   
                    int _performanceObjectiveID = Convert.ToInt32(HiddenFieldPerformanceObjectiveID.Value);
                    ////check if the performance objective being entered already exists
                    //if (db.PerformanceObjectives.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceObjectiveName.ToLower() == _performanceObjective.ToLower()))
                    //{
                    //    _hrClass.LoadHRManagerMessageBox(1, "Save failed . Performance objective you are tyring to save already exists!", this, PanelPerformanceReview);
                    //    return;
                    //}
                    //else
                    //{

                    //save KPI
                    PerformanceObjectiveKPI newKPI = new PerformanceObjectiveKPI();
                    newKPI.PerformanceObjectiveID = _performanceObjectiveID;
                    newKPI.PerformanceKPI = _KPIName;
                    newKPI.TargetMeasure = _targetMeasure;
                    newKPI.Unit = ddlKPIUnit.SelectedValue.Trim();
                    if (_targetMeasure == "Date")//check if the target measure is date
                    {
                        newKPI.AnnualTarget = txtKPIAnnualTargetDate.Text.Trim();
                        newKPI.Quarter1Target = txtKPIQuarter1TargetDate.Text.Trim();
                        newKPI.Quarter2Target = txtKPIQuarter2TargetDate.Text.Trim();
                        newKPI.Quarter3Target = txtKPIQuarter3TargetDate.Text.Trim();
                        newKPI.Quarter4Target = txtKPIQuarter4TargetDate.Text.Trim();
                    }
                    else if (_targetMeasure == "Percentage")
                    {
                        newKPI.AnnualTarget = txtKPIAnnualTarget.Text.Trim() + "%";
                        newKPI.Quarter1Target = txtKPIQuarter1Target.Text.Trim() + "%";
                        newKPI.Quarter2Target = txtKPIQuarter2Target.Text.Trim() + "%";
                        newKPI.Quarter3Target = txtKPIQuarter3Target.Text.Trim() + "%";
                        newKPI.Quarter4Target = txtKPIQuarter4Target.Text.Trim() + "%";
                    }
                    else
                    {
                        newKPI.AnnualTarget = txtKPIAnnualTarget.Text.Trim();
                        newKPI.Quarter1Target = txtKPIQuarter1Target.Text.Trim();
                        newKPI.Quarter2Target = txtKPIQuarter2Target.Text.Trim();
                        newKPI.Quarter3Target = txtKPIQuarter3Target.Text.Trim();
                        newKPI.Quarter4Target = txtKPIQuarter4Target.Text.Trim();
                    }
                    db.PerformanceObjectiveKPIs.InsertOnSubmit(newKPI);
                    db.SubmitChanges();
                    HiddenFieldKPIID.Value = newKPI.PerformanceObjectiveKPIID.ToString();
                    gvPerformanceObjectiveKPIs.DataBind();
                    _hrClass.LoadHRManagerMessageBox(2, "Key performance indicator details have been successfuly saved!", this, PanelPerformanceReview);
                    lnkBtnUpdateKPI.Enabled = true;
                    //display KPIs associated with the performance objective
                    DisplayPerformanceObjectiveKPIs(_performanceObjectiveID);
                    return;
                    //}
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }

        }
        //update key performance objective details
        private void UpdateKPI()
        {
            ModalPopupExtenderAddKPI.Show();
            try
            {
                string _error = ValidateAddKPIControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelPerformanceReview); return;
                }
                else
                {
                    string _KPIName = "", _targetMeasure;
                    _KPIName = txtKPIName.Text.Trim();
                    _targetMeasure = ddlKPITargetMeasure.SelectedValue;
                    int _performanceObjectiveID = Convert.ToInt32(HiddenFieldPerformanceObjectiveID.Value);
                    int _performanceObjectiveKPIID = Convert.ToInt32(HiddenFieldKPIID.Value);
                    ////check if the performance objective being entered already exists
                    //if (db.PerformanceObjectives.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceObjectiveName.ToLower() == _performanceObjective.ToLower()))
                    //{
                    //    _hrClass.LoadHRManagerMessageBox(1, "Save failed . Performance objective you are tyring to save already exists!", this, PanelPerformanceReview);
                    //    return;
                    //}
                    //else
                    //{

                    //save KPI
                    PerformanceObjectiveKPI updateKPI = db.PerformanceObjectiveKPIs.Single(p => p.PerformanceObjectiveKPIID == _performanceObjectiveKPIID);
                    updateKPI.PerformanceKPI = _KPIName;
                    updateKPI.TargetMeasure = _targetMeasure;
                    if (_targetMeasure == "Date")//check if the target measure is date
                    {
                        updateKPI.AnnualTarget = txtKPIAnnualTargetDate.Text.Trim();
                        updateKPI.Quarter1Target = txtKPIQuarter1TargetDate.Text.Trim();
                        updateKPI.Quarter2Target = txtKPIQuarter2TargetDate.Text.Trim();
                        updateKPI.Quarter3Target = txtKPIQuarter3TargetDate.Text.Trim();
                        updateKPI.Quarter4Target = txtKPIQuarter4TargetDate.Text.Trim();
                    }
                    else if (_targetMeasure == "Percentage")
                    {
                        updateKPI.AnnualTarget = txtKPIAnnualTarget.Text.Trim() + "%";
                        updateKPI.Quarter1Target = txtKPIQuarter1Target.Text.Trim() + "%";
                        updateKPI.Quarter2Target = txtKPIQuarter2Target.Text.Trim() + "%";
                        updateKPI.Quarter3Target = txtKPIQuarter3Target.Text.Trim() + "%";
                        updateKPI.Quarter4Target = txtKPIQuarter4Target.Text.Trim() + "%";
                    }
                    else
                    {
                        updateKPI.AnnualTarget = txtKPIAnnualTarget.Text.Trim();
                        updateKPI.Quarter1Target = txtKPIQuarter1Target.Text.Trim();
                        updateKPI.Quarter2Target = txtKPIQuarter2Target.Text.Trim();
                        updateKPI.Quarter3Target = txtKPIQuarter3Target.Text.Trim();
                        updateKPI.Quarter4Target = txtKPIQuarter4Target.Text.Trim();
                    }
                    db.SubmitChanges();
                    gvPerformanceObjectiveKPIs.DataBind();
                    _hrClass.LoadHRManagerMessageBox(2, "Key performance indicator details have been successfuly updated!", this, PanelPerformanceReview);
                    lnkBtnUpdateKPI.Enabled = true;
                    //display KPIs associated with the performance objective
                    DisplayPerformanceObjectiveKPIs(_performanceObjectiveID);
                    return;
                    //}
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }

        }
        //save KPI details
        protected void lnkBtnSaveKPI_Click(object sender, EventArgs e)
        {
            SaveNewKPI();//save KPI details
            return;
        }
        //save KPI details
        protected void lnkBtnUpdateKPI_Click(object sender, EventArgs e)
        {
            UpdateKPI();//updatee KPI details
            return;
        }
        //clear KPI details
        protected void lnkBtnClearKPI_Click(object sender, EventArgs e)
        {
            ClearAddKPIControls();
            ModalPopupExtenderAddKPI.Show();
            return;
        }
        //display KPI associated with performance objective details
        private void DisplayPerformanceObjectiveKPIs(int _performanceObjectiveID)
        {
            try
            {
                object _display;
                _display = db.PROC_PerformanceObjectiveKPIsTargetPlan(_performanceObjectiveID);
                gvPerformanceObjectiveKPIs.DataSourceID = null;
                gvPerformanceObjectiveKPIs.DataSource = _display;
                Session["gvPerformanceObjectiveKPIsData"] = _display;
                gvPerformanceObjectiveKPIs.DataBind();
                return;
            }
            catch { }
        }

        //activate date buttons & textboxe incase the target measure is date
        private void ActivateOrDisableDateEntryControls(string _targetMeasure)
        {
            if (_targetMeasure == "Date")
            {
                 //disable unit dropdown
                ddlKPIUnit.Items.Clear();
                LabelKPIUnit.Visible=false;
                ddlKPIUnit.Visible=false;

                //enable date fields
                txtKPIAnnualTargetDate.Visible = true;
                ImageButtonKPIAnnualTargetDate.Visible = true;
                txtKPIQuarter1TargetDate.Visible = true;
                ImageButtonKPIQuarter1TargetDate.Visible = true;
                txtKPIQuarter2TargetDate.Visible = true;
                ImageButtonKPIQuarter2TargetDate.Visible = true;
                txtKPIQuarter3TargetDate.Visible = true;
                ImageButtonKPIQuarter3TargetDate.Visible = true;
                txtKPIQuarter4TargetDate.Visible = true;
                ImageButtonKPIQuarter4TargetDate.Visible = true;

                //disbale normal entry field
                txtKPIAnnualTarget.Visible = false;
                txtKPIQuarter1Target.Visible = false;
                txtKPIQuarter2Target.Visible = false;
                txtKPIQuarter3Target.Visible = false;
                txtKPIQuarter4Target.Visible = false;
            }
            else if (_targetMeasure == "Amount")
            {
                 //enable unit dropdown
                ddlKPIUnit.Items.Clear();
                ddlKPIUnit.Items.Add("Euro");
                ddlKPIUnit.Items.Add("GBP");
                ddlKPIUnit.Items.Add("Ksh");
                ddlKPIUnit.Items.Add("RwF");
                ddlKPIUnit.Items.Add("Tsh");
                ddlKPIUnit.Items.Add("USD");              
                ddlKPIUnit.Items.Add("Ush");
                LabelKPIUnit.Visible= true;
                LabelKPIUnit.Text = "Currency";
                ddlKPIUnit.Visible= true;


                //disable date fields
                txtKPIAnnualTargetDate.Visible = false;
                ImageButtonKPIAnnualTargetDate.Visible = false;
                txtKPIQuarter1TargetDate.Visible = false;
                ImageButtonKPIQuarter1TargetDate.Visible = false;
                txtKPIQuarter2TargetDate.Visible = false;
                ImageButtonKPIQuarter2TargetDate.Visible = false;
                txtKPIQuarter3TargetDate.Visible = false;
                ImageButtonKPIQuarter3TargetDate.Visible = false;
                txtKPIQuarter4TargetDate.Visible = false;
                ImageButtonKPIQuarter4TargetDate.Visible = false;

                //enable normal entry fields
                txtKPIAnnualTarget.Visible = true;
                txtKPIQuarter1Target.Visible = true;
                txtKPIQuarter2Target.Visible = true;
                txtKPIQuarter3Target.Visible = true;
                txtKPIQuarter4Target.Visible = true;
            }
            else if (_targetMeasure == "Time")
            {
                //enable unit dropdown
                ddlKPIUnit.Items.Clear();
                ddlKPIUnit.Items.Add("Minutes");
                ddlKPIUnit.Items.Add("Hours");
                ddlKPIUnit.Items.Add("Days");
                ddlKPIUnit.Items.Add("Weeks");
                ddlKPIUnit.Items.Add("Months");
                LabelKPIUnit.Visible= true;
                LabelKPIUnit.Text = "Unit Time";
                ddlKPIUnit.Visible= true;

                //disable date fields
                txtKPIAnnualTargetDate.Visible = false;
                ImageButtonKPIAnnualTargetDate.Visible = false;
                txtKPIQuarter1TargetDate.Visible = false;
                ImageButtonKPIQuarter1TargetDate.Visible = false;
                txtKPIQuarter2TargetDate.Visible = false;
                ImageButtonKPIQuarter2TargetDate.Visible = false;
                txtKPIQuarter3TargetDate.Visible = false;
                ImageButtonKPIQuarter3TargetDate.Visible = false;
                txtKPIQuarter4TargetDate.Visible = false;
                ImageButtonKPIQuarter4TargetDate.Visible = false;

                //enable normal entry fields
                txtKPIAnnualTarget.Visible = true;
                txtKPIQuarter1Target.Visible = true;
                txtKPIQuarter2Target.Visible = true;
                txtKPIQuarter3Target.Visible = true;
                txtKPIQuarter4Target.Visible = true;
            }
            else 
            {
                //disable unit dropdown
                ddlKPIUnit.Items.Clear();
                LabelKPIUnit.Visible=false;
                ddlKPIUnit.Visible=false;
                //disable date fields
                txtKPIAnnualTargetDate.Visible = false;
                ImageButtonKPIAnnualTargetDate.Visible = false;
                txtKPIQuarter1TargetDate.Visible = false;
                ImageButtonKPIQuarter1TargetDate.Visible = false;
                txtKPIQuarter2TargetDate.Visible = false;
                ImageButtonKPIQuarter2TargetDate.Visible = false;
                txtKPIQuarter3TargetDate.Visible = false;
                ImageButtonKPIQuarter3TargetDate.Visible = false;
                txtKPIQuarter4TargetDate.Visible = false;
                ImageButtonKPIQuarter4TargetDate.Visible = false;

                //enable normal entry fields
                txtKPIAnnualTarget.Visible = true;
                txtKPIQuarter1Target.Visible = true;
                txtKPIQuarter2Target.Visible = true;
                txtKPIQuarter3Target.Visible = true;
                txtKPIQuarter4Target.Visible = true;
            }
          
        }
        //enable /disable the relevant entry fiels based on the target measure selected
        protected void ddlKPITargetMeasure_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderAddKPI.Show();
            try
            {
                string _targetMeasure = ddlKPITargetMeasure.SelectedValue;
                ActivateOrDisableDateEntryControls(_targetMeasure);//activate target controls based on the selection made
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        //check if any KPI is selected before editing
        protected void LinkButtonEditKPI_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvPerformanceObjectiveKPIs))//check if a KPIis selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no KPI selected!. Select the key performance indicator that you want to edit!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvPerformanceObjectiveKPIs.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one KPI!. Select only one KPI to edit at a time!", this, PanelPerformanceReview);
                        return;
                    }
                    foreach (GridViewRow _grv in gvPerformanceObjectiveKPIs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFKPIID = (HiddenField)_grv.FindControl("HiddenField1");
                            int _performanceObjectiveKPIID = Convert.ToInt32(_HFKPIID.Value);
                            LoadEditPerformanceObjectiveKPIControls(_performanceObjectiveKPIID);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading performance objective KPI details. " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }

        // populate the edit performance objective KPI controls
        private void LoadEditPerformanceObjectiveKPIControls(int _performanceObjectiveKPIID)
        {
            ClearAddKPIControls();
            PerformanceObjectiveKPI getKPI = db.PerformanceObjectiveKPIs.Single(p => p.PerformanceObjectiveKPIID == _performanceObjectiveKPIID);
            HiddenFieldKPIID.Value = _performanceObjectiveKPIID.ToString();

            txtKPIName.Text = getKPI.PerformanceKPI;
            string _targetMeasure = getKPI.TargetMeasure;
            ddlKPITargetMeasure.SelectedValue = _targetMeasure;
            ddlKPIUnit.ClearSelection();
            string _Unit = getKPI.Unit;
            if (_Unit != null)
            {
                ddlKPIUnit.SelectedValue = _Unit;
            }


            //active relevant target controls
            ActivateOrDisableDateEntryControls(_targetMeasure);
            //checi if its a date measure
            if (_targetMeasure == "Date")
            {
                //populate date textboxes
                txtKPIAnnualTargetDate.Text = _hrClass.ShortDateDayStart(getKPI.AnnualTarget);
                txtKPIQuarter1TargetDate.Text = _hrClass.ShortDateDayStart(getKPI.Quarter1Target);
                txtKPIQuarter2TargetDate.Text = _hrClass.ShortDateDayStart(getKPI.Quarter2Target);
                txtKPIQuarter3TargetDate.Text = _hrClass.ShortDateDayStart(getKPI.Quarter3Target);
                txtKPIQuarter4TargetDate.Text = _hrClass.ShortDateDayStart(getKPI.Quarter4Target);
            }
            else
            {
                //populate normal entry controls
                txtKPIAnnualTarget.Text = getKPI.AnnualTarget;
                txtKPIQuarter1Target.Text = getKPI.Quarter1Target;
                txtKPIQuarter2Target.Text = getKPI.Quarter2Target;
                txtKPIQuarter3Target.Text = getKPI.Quarter3Target;
                txtKPIQuarter4Target.Text = getKPI.Quarter4Target;
            }
            lnkBtnSaveKPI.Visible = false;
            lnkBtnClearKPI.Visible = true;
            lnkBtnUpdateKPI.Enabled = true;
            lbAddKPIHeader.Text = "Edit Key Performance Indicator";
            ImageAddKPIHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ModalPopupExtenderAddKPI.Show();
            return;
        }
        //load delete KPIdetails
        protected void LinkButtonDeleteKPI_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvPerformanceObjectiveKPIs))//check if there is any record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the KPI that you want to delete!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvPerformanceObjectiveKPIs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected KPI?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected KPIs?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmDeleteKPI, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //method for deleting KPI
        private void DeleteKeyPerformanceIndicator()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvPerformanceObjectiveKPIs.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;

                    if (_cb.Checked)
                    {
                        HiddenField _HFKPIID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _performanceObjectiveKPIID = Convert.ToInt32(_HFKPIID.Value);

                        PerformanceObjectiveKPI deleteKPI = db.PerformanceObjectiveKPIs.Single(p => p.PerformanceObjectiveKPIID == _performanceObjectiveKPIID);
                        db.PerformanceObjectiveKPIs.DeleteOnSubmit(deleteKPI);
                        db.SubmitChanges();
                    }
                }
                //refresh performance objectives KPIn after delete
                int _performanceObjectiveID = Convert.ToInt32(HiddenFieldPerformanceObjectiveID.Value);
                DisplayPerformanceObjectiveKPIs(_performanceObjectiveID);
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected KPI has been deleted.", this, PanelPerformanceReview);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected KPIes have been deleted.", this, PanelPerformanceReview);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed . " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }

        //delete the selected KPI
        protected void lnkBtnYesConfirmationDeleteKPI_Click(object sender, EventArgs e)
        {
            DeleteKeyPerformanceIndicator();
            return;
        }
        //close performance objective KPIs details
        protected void lnkBtnClosePerformanceObjectiveKPIs_Click(object sender, EventArgs e)
        {
            PanelPerformanceObjectiveListing.Visible = true;
            PanelAddPerformanceObjectiveKPI.Visible = false;
            _hrClass.ClearEntryControls(PanelAddPerformanceObjectiveKPI);
            return;
        }
        //populate perfomance plan doe printdetails associated with the staff for the selected year
        private void DisplayPerformanceObjectivesForPrint(Guid _staffID, string _year)
        {
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("PerformanceObjectiveID", typeof(int)));
                dt.Columns.Add(new DataColumn("PerformanceObjectiveName", typeof(string)));
                dt.Columns.Add(new DataColumn("PerformanceObjectiveWeight", typeof(float)));
                dt.Columns.Add(new DataColumn("PerformanceKPI", typeof(string)));
                dt.Columns.Add(new DataColumn("TargetMeasure", typeof(string)));
                dt.Columns.Add(new DataColumn("AnnualTarget", typeof(string)));
                dt.Columns.Add(new DataColumn("Quarter1Target", typeof(string)));
                dt.Columns.Add(new DataColumn("Quarter2Target", typeof(string)));
                dt.Columns.Add(new DataColumn("Quarter3Target", typeof(string)));
                dt.Columns.Add(new DataColumn("Quarter4Target", typeof(string)));
                DataRow dr;
                int count = 0;
                foreach (PROC_PerformanceObjectiveQuarterlyPlanReviewResult getPerformanceObjective in db.PROC_PerformanceObjectiveQuarterlyPlanReview(_staffID, _year))
                {

                    dr = dt.NewRow();

                    dr[0] = getPerformanceObjective.PerformanceObjectiveID;
                    dr[1] = getPerformanceObjective.PerformanceObjectiveName;
                    dr[2] = getPerformanceObjective.PerformanceObjectiveWeight;
                    dr[3] = getPerformanceObjective.PerformanceKPI;
                    dr[4] = getPerformanceObjective.TargetMeasure;
                    dr[5] = getPerformanceObjective.AnnualTarget;
                    dr[6] = getPerformanceObjective.Quarter1Target;
                    dr[7] = getPerformanceObjective.Quarter2Target;
                    dr[8] = getPerformanceObjective.Quarter3Target;
                    dr[9] = getPerformanceObjective.Quarter4Target;

                    dt.Rows.Add(dr);
                    count++;
                }
                gvPerformanceObjectivesPrint.DataSourceID = null;
                gvPerformanceObjectivesPrint.DataSource = dt;
                gvPerformanceObjectivesPrint.DataBind();
                GenerateGridViewRowUniqueData(gvPerformanceObjectivesPrint, 1);//get distinct performance objectives rows
                GenerateGridViewRowUniqueData(gvPerformanceObjectivesPrint, 2);//get destinct performance objective weight rows

                return;
            }
            catch { }
        }
        //print staff perfomance plan
        protected void LinkButtonPrintStaffPerformancePlan_Click(object sender, EventArgs e)
        {
            gvPerformanceObjectivesPrint.Visible = true;

            gvPerformanceObjectivesPrint.UseAccessibleHeader = true;
            gvPerformanceObjectivesPrint.HeaderRow.TableSection = TableRowSection.TableHeader;
            //gvPerformanceObjectivesPrint.FooterRow.TableSection = TableRowSection.TableFooter;
            gvPerformanceObjectivesPrint.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in gvPerformanceObjectivesPrint.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }

            StringWriter sw = new StringWriter();

            sw.WriteLine("<b>PERFORMANCE PLAN FOR THE YEAR " + ddlAnnualPerformancePlanYear.SelectedValue + "</b>");
            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            //get staffbasic info
            PROC_StaffBasicInfoResult getStaff = db.PROC_StaffBasicInfo(_staffID).FirstOrDefault();
            sw.WriteLine("<BR/><b>NAME:</b> " + getStaff.StaffName.ToUpper() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPLOYEE NO:</b> " + getStaff.StaffRef.ToUpper() + "<br/><b>JOB TITLE:</b> " + getStaff.JobTitle.ToUpper() + "<br/><b>DUTY STATION / DEPARTMENT:</b> " + getStaff.PostedOffice.ToUpper() + "/" + getStaff.Department.ToUpper());
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gvPerformanceObjectivesPrint.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            //hw.AddStyleAttribute(HtmlTextWriterStyle.Color, "Red");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //gvPerformanceObjectivesPrint.AllowPaging = true;
            //gvPerformanceObjectivesPrint.DataBind();
            gvPerformanceObjectivesPrint.Visible = false;
            return;
        }
        //print staff KPI
        protected void lnkBtnPrintPerformanceObjectiveKPIs_Click(object sender, EventArgs e)
        {
          /*  gvPerformanceObjectiveKPIs.UseAccessibleHeader = true;
            gvPerformanceObjectiveKPIs.HeaderRow.TableSection = TableRowSection.TableHeader;
            //gvPerformanceObjectiveKPIs.FooterRow.TableSection = TableRowSection.TableFooter;
            gvPerformanceObjectiveKPIs.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in gvPerformanceObjectiveKPIs.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }

            StringWriter sw = new StringWriter();

            sw.WriteLine("<b>PERFORMANCE PLAN FOR THE YEAR " + txtAnnualPerformanceObjective.Text + "</b>");
            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            //get staffbasic info
            PROC_StaffBasicInfoResult getStaff = db.PROC_StaffBasicInfo(_staffID).FirstOrDefault();
            sw.WriteLine("<BR/><b>NAME:</b> " + getStaff.StaffName.ToUpper() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPLOYEE NO:</b> " + getStaff.StaffRef.ToUpper() + "<br/><b>JOB TITLE:</b> " + getStaff.JobTitle.ToUpper() + "<br/><b>DUTY STATION / DEPARTMENT:</b> " + getStaff.PostedOffice.ToUpper() + "/" + getStaff.Department.ToUpper());
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gvPerformanceObjectiveKPIs.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            //hw.AddStyleAttribute(HtmlTextWriterStyle.Color, "Red");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //gvPerformanceObjectiveKPIs.AllowPaging = true;
            //gvPerformanceObjectiveKPIs.DataBind();
            gvPerformanceObjectiveKPIs.Visible = false;
            return;*/
        }
        #endregion

        #region  QUARTERLY REVIEW
        //load staff quarterly performance details for the selected year and quarter
        protected void ddlQuarterlyReviewQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. There is no performance year selected. Select a performance year!", this, PanelPerformanceReview);
                ddlQuarterlyReviewQuarter.SelectedIndex = 0;
                ddlAnnualPerformancePlanYear.Focus();
                return;
            }
            else if (ddlQuarterlyReviewQuarter.SelectedIndex == 0)
            {
                _hrClass.LoadHRManagerMessageBox(1, "No quarter selected selected. Select quarter under review!", this, PanelPerformanceReview);
                ddlQuarterlyReviewQuarter.Focus();
                return;
            }
            else
            {
                string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                int _quarter = Convert.ToInt32(ddlQuarterlyReviewQuarter.SelectedValue);
                //if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")
                //{
                //    _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                //    string _staffName = _hrClass.getStaffNames(_staffID);
                //    lbStaffToAppraiseHeader.Text = _staffName + " Performance Objective Details for the Year " + _performanceYear;
                //}

                //DisplayPerformanceObjectives(_staffID, _performanceYear);
                DisplayQuartelyReviewDetails(_staffID, _performanceYear, _quarter);//populate details for the selected quarter
                return;
            }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview);
            //    return;
            //}
        }
        //get staff quarter review datatable
        private DataTable GenerateStaffQuarterlyReviewDataTable(Guid _staffID, string _year, int _quarter)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("PerformanceObjectiveID", typeof(int)));
            dt.Columns.Add(new DataColumn("PerformanceObjectiveName", typeof(string)));
            dt.Columns.Add(new DataColumn("PerformanceObjectiveWeight", typeof(float)));
            dt.Columns.Add(new DataColumn("PerformanceKPI", typeof(string)));
            dt.Columns.Add(new DataColumn("TargetMeasure", typeof(string)));
            dt.Columns.Add(new DataColumn("AnnualTarget", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterTarget", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterAchieved", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterStaffRating", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterStaffComment", typeof(string)));
            dt.Columns.Add(new DataColumn("NextQuarterActionPlan", typeof(string)));
            dt.Columns.Add(new DataColumn("PerformanceObjectiveKPIID", typeof(int)));
            dt.Columns.Add(new DataColumn("QuarterSupervisorRating", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterSupervisorComment", typeof(string)));
            DataRow dr;
            foreach (PROC_PerformanceObjectiveQuarterlyPlanReviewResult getPerformanceObjective in db.PROC_PerformanceObjectiveQuarterlyPlanReview(_staffID, _year))
            {
                dr = dt.NewRow();

                dr[0] = getPerformanceObjective.PerformanceObjectiveID;
                dr[1] = getPerformanceObjective.PerformanceObjectiveName;
                dr[2] = getPerformanceObjective.PerformanceObjectiveWeight;
                dr[3] = getPerformanceObjective.PerformanceKPI;
                dr[4] = getPerformanceObjective.TargetMeasure;
                dr[5] = getPerformanceObjective.AnnualTarget;

                string _quarterTarget = "", _quarterAchieved = "", _quarterStaffRating = "", _quarterStaffComment = "", _nextQuarterActionPlan = "", _quarterSupervisorRating = "", _quarterSupervisorComments = "";
                if (_quarter == 1)//check if its quarter 1
                {
                    _quarterTarget = getPerformanceObjective.Quarter1Target;
                    _quarterAchieved = getPerformanceObjective.Quarter1Achieved;
                    _quarterStaffRating = Convert.ToString(getPerformanceObjective.Quarter1StaffRating);
                    _quarterStaffComment = getPerformanceObjective.Quarter1StaffComments;
                    _nextQuarterActionPlan = getPerformanceObjective.Quarter1NextQuarterActionPlan;
                    _quarterSupervisorRating = getPerformanceObjective.Quarter1SupervisorRating;
                    _quarterSupervisorComments = getPerformanceObjective.Quarter1SupervisorComments;
                }
                else if (_quarter == 2)//check if its quarter 2
                {
                    _quarterTarget = getPerformanceObjective.Quarter2Target;
                    _quarterAchieved = getPerformanceObjective.Quarter2Achieved;
                    _quarterStaffRating = Convert.ToString(getPerformanceObjective.Quarter2StaffRating);
                    _quarterStaffComment = getPerformanceObjective.Quarter2StaffComments;
                    _nextQuarterActionPlan = getPerformanceObjective.Quarter2NextQuarterActionPlan;
                    _quarterSupervisorRating = getPerformanceObjective.Quarter2SupervisorRating;
                    _quarterSupervisorComments = getPerformanceObjective.Quarter2SupervisorComments;
                }
                else if (_quarter == 3)//check if its quarter 3
                {
                    _quarterTarget = getPerformanceObjective.Quarter3Target;
                    _quarterAchieved = getPerformanceObjective.Quarter3Achieved;
                    _quarterStaffRating = Convert.ToString(getPerformanceObjective.Quarter3StaffRating);
                    _quarterStaffComment = getPerformanceObjective.Quarter3StaffComments;
                    _nextQuarterActionPlan = getPerformanceObjective.Quarter3NextQuarterActionPlan;
                    _quarterSupervisorRating = getPerformanceObjective.Quarter3SupervisorRating;
                    _quarterSupervisorComments = getPerformanceObjective.Quarter3SupervisorComments;
                }
                else if (_quarter == 4)//check if its quarter 4
                {
                    _quarterTarget = getPerformanceObjective.Quarter4Target;
                    _quarterAchieved = getPerformanceObjective.Quarter4Achieved;
                    _quarterStaffRating = Convert.ToString(getPerformanceObjective.Quarter4StaffRating);
                    _quarterStaffComment = getPerformanceObjective.Quarter4StaffComments;
                    _nextQuarterActionPlan = getPerformanceObjective.Quarter4NextQuarterActionPlan;
                    _quarterSupervisorRating = getPerformanceObjective.Quarter4SupervisorRating;
                    _quarterSupervisorComments = getPerformanceObjective.Quarter4SupervisorComments;
                }
                dr[6] = _quarterTarget;
                dr[7] = _quarterAchieved;
                dr[8] = _quarterStaffRating;
                dr[9] = _quarterStaffComment;
                dr[10] = _nextQuarterActionPlan;
                dr[11] = getPerformanceObjective.PerformanceObjectiveKPIID;
                dr[12] = _quarterSupervisorRating;
                dr[13] = _quarterSupervisorComments;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //populate quarterly review details associated with the staff for the selected year
        private void DisplayQuartelyReviewDetails(Guid _staffID, string _year, int _quarter)
        {
            try
            {
                //generate tha data table with quartery data
                DataTable dt = GenerateStaffQuarterlyReviewDataTable(_staffID, _year, _quarter);
                int count = dt.Rows.Count;//get the rows count

                HiddenFieldQuarter.Value = _quarter.ToString();

                gvQuarterlyReview.DataSourceID = null;
                gvQuarterlyReview.DataSource = dt;
                gvQuarterlyReview.DataBind();
                Session["gvQuarterlyReviewData"] = dt;
                GenerateGridViewRowUniqueData(gvQuarterlyReview, 1);//get distinct performance objectives rows
                GenerateGridViewRowUniqueData(gvQuarterlyReview, 2);//get destinct performance objective weight rows

                //populate gridview to be printed
                gvQuarterlyReviewPrint.DataSourceID = null;
                gvQuarterlyReviewPrint.DataSource = dt;
                gvQuarterlyReviewPrint.DataBind();
                GenerateGridViewRowUniqueData(gvQuarterlyReviewPrint, 1);//get distinct performance objectives rows
                GenerateGridViewRowUniqueData(gvQuarterlyReviewPrint, 2);//get destinct performance objective weight rows
                //GenerateGridViewRowUniqueData(0);//get distinct performance objectives rows
                PanelStaffQuarterlyReviewDetails.Visible = true;

                if (count > 0)
                {
                    txtQuarterlyReviewStaffComments.Visible = true;
                    lnkBtnSaveStaffQuarteryReview.Visible = true;
                    lnkBtnSubmitStaffQuarterlyReview.Visible = true;
                }
                else
                {
                    txtQuarterlyReviewStaffComments.Visible = false;
                    lnkBtnSaveStaffQuarteryReview.Visible = false;
                    lnkBtnSubmitStaffQuarterlyReview.Visible = false;
                }
                //check if the staff has saved a review for the quarter there review for appraisal
                if (db.QuarterlyPerformanceReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter))
                {
                    //get the quarter under review
                    QuarterlyPerformanceReview getQuarterlyReview = db.QuarterlyPerformanceReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter);
                    txtQuarterlyReviewStaffComments.Text = getQuarterlyReview.StaffComments;
                    if (getQuarterlyReview.HasStaffSubmitted == true)
                    {
                        //hide save & submit buttons
                        lnkBtnSaveStaffQuarteryReview.Visible = false;
                        lnkBtnSubmitStaffQuarterlyReview.Visible = false;
                        gvQuarterlyReview.Visible = false;
                        gvQuarterlyReviewPrint.Visible = true;
                    }
                    else
                    {
                        //show save & submit buttons
                        lnkBtnSaveStaffQuarteryReview.Visible = true;
                        lnkBtnSubmitStaffQuarterlyReview.Visible = true;
                        gvQuarterlyReview.Visible = true;
                        gvQuarterlyReviewPrint.Visible = false;
                    }
                }
                else
                {
                    txtQuarterlyReviewStaffComments.Text = "";
                    //show save & submit buttons
                    lnkBtnSaveStaffQuarteryReview.Visible = true;
                    lnkBtnSubmitStaffQuarterlyReview.Visible = true;
                    gvQuarterlyReview.Visible = true;
                    gvQuarterlyReviewPrint.Visible = false;
                }
                return;
            }
            catch { }
        }
        //dipaly unique gridview row data
        private void GenerateGridViewRowUniqueData(GridView _gridView, int cellno)
        {
            try
            {
                //Logic for unique names
                //Step 1:
                string initialnamevalue = _gridView.Rows[0].Cells[cellno].Text;
                //Step 2:      
                for (int i = 1; i < _gridView.Rows.Count; i++)
                {

                    if (_gridView.Rows[i].Cells[cellno].Text == initialnamevalue)
                        _gridView.Rows[i].Cells[cellno].Text = "";
                    else
                        initialnamevalue = _gridView.Rows[i].Cells[cellno].Text;
                }


                //display row number based on the distinct perfomance objective
                if (cellno == 1)
                {
                    int _rowNumber = 1;
                    for (int i = 0; i < _gridView.Rows.Count; i++)
                    {
                        if (_gridView.Rows[i].Cells[1].Text == "")//check if the row is empty
                        {
                            _gridView.Rows[i].Cells[0].Text = "";//make row 1 null
                        }
                        else
                        {
                            _gridView.Rows[i].Cells[0].Text = _rowNumber.ToString() + ".";

                            _rowNumber++;
                        }
                    }
                }

            }
            catch { }
        }
        //row data bound for quarterly review gridview
        protected void gvQuarterlyReview_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField _HiddenFieldQuarterStaffRating = (HiddenField)e.Row.FindControl("HiddenFieldQuarterStaffRating");

                DropDownList _ddlQuarterStaffRating = (DropDownList)e.Row.FindControl("ddlQuarterStaffRating");
                if (_HiddenFieldQuarterStaffRating.Value != "")
                {
                    _ddlQuarterStaffRating.SelectedValue = _HiddenFieldQuarterStaffRating.Value;
                }
                else
                    _ddlQuarterStaffRating.SelectedIndex = 0;
            }
            //DropDownList _ddlQuarterStaffRating = (DropDownList)gvQuarterlyReview.Rows[7].FindControl("ddlQuarterStaffRating");
            //_ddlQuarterStaffRating.SelectedValue = "1";


        }

        //update staff' quarter KPI acchieved details
        private void UpdateStaffQuarterlyKPIAchievedDetails(int _performanceObjectiveKPIID, short _quarter, string _quarterAchieved, string _quarterStaffRating, string _quarterStaffComment, string _nextQuarterActionPlan)
        {
            PerformanceObjectiveKPI updateKPI = db.PerformanceObjectiveKPIs.Single(p => p.PerformanceObjectiveKPIID == _performanceObjectiveKPIID);
            if (_quarter == 1)//update quarter 1
            {
                updateKPI.Quarter1Achieved = _quarterAchieved;
                updateKPI.Quarter1StaffRating = _quarterStaffRating;
                updateKPI.Quarter1StaffComments = _quarterStaffComment;
                updateKPI.Quarter1NextQuarterActionPlan = _nextQuarterActionPlan;
            }
            else if (_quarter == 2)//update quarter 2
            {
                updateKPI.Quarter2Achieved = _quarterAchieved;
                updateKPI.Quarter2StaffRating = _quarterStaffRating;
                updateKPI.Quarter2StaffComments = _quarterStaffComment;
                updateKPI.Quarter2NextQuarterActionPlan = _nextQuarterActionPlan;
            }
            else if (_quarter == 3)//update quarter 3
            {
                updateKPI.Quarter3Achieved = _quarterAchieved;
                updateKPI.Quarter3StaffRating = _quarterStaffRating;
                updateKPI.Quarter3StaffComments = _quarterStaffComment;
                updateKPI.Quarter3NextQuarterActionPlan = _nextQuarterActionPlan;
            }
            else if (_quarter == 4)//update quarter 4
            {
                updateKPI.Quarter4Achieved = _quarterAchieved;
                updateKPI.Quarter4StaffRating = _quarterStaffRating;
                updateKPI.Quarter4StaffComments = _quarterStaffComment;
                updateKPI.Quarter4NextQuarterActionPlan = _nextQuarterActionPlan;
            }
            db.SubmitChanges();
        }
        //save new quarterly review
        private void SaveNewQuarterlyReview(Guid _staffID, string _year, short _quarter, string _staffComments, bool _hasStaffSubmitted, DateTime? _dateStaffSubmited)
        {
            QuarterlyPerformanceReview newQuarterlyReview = new QuarterlyPerformanceReview();
            newQuarterlyReview.StaffID = _staffID;
            newQuarterlyReview.PerformanceYear = _year;
            newQuarterlyReview.QuarterUnderReview = _quarter;
            newQuarterlyReview.StaffComments = _staffComments;
            newQuarterlyReview.HasStaffSubmitted = _hasStaffSubmitted;
            newQuarterlyReview.DateStaffSubmitted = _dateStaffSubmited;
            db.QuarterlyPerformanceReviews.InsertOnSubmit(newQuarterlyReview);
            db.SubmitChanges();
        }
        //update existing quarterly review
        private void UpdateQuarterlyReview(Guid _staffID, string _year, short _quarter, string _staffComments, bool _hasStaffSubmitted, DateTime? _dateStaffSubmited)
        {
            QuarterlyPerformanceReview updateQuarterlyReview = db.QuarterlyPerformanceReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter);
            updateQuarterlyReview.PerformanceYear = _year;
            updateQuarterlyReview.QuarterUnderReview = _quarter;
            updateQuarterlyReview.StaffComments = _staffComments;
            updateQuarterlyReview.HasStaffSubmitted = _hasStaffSubmitted;
            updateQuarterlyReview.DateStaffSubmitted = _dateStaffSubmited;
            db.SubmitChanges();
        }
        //save staff quartely review
        private void SaveOverallQuarterlyReview(bool _hasStaffSubmitted, DateTime? _dateStaffSubmited)
        {
            try
            {
                bool _broken = false;
                short _quarter = Convert.ToInt16(ddlQuarterlyReviewQuarter.SelectedValue);
                foreach (GridViewRow _grv in gvQuarterlyReview.Rows)
                {
                    HiddenField _hfPerfomanceObjectiveKPIID = (HiddenField)_grv.FindControl("HiddenFieldPerformanceObjectiveKPIID");
                    TextBox _txtQuarterAchieved = (TextBox)_grv.FindControl("txtQuarterAchieved");
                    DropDownList _ddlQuarterStaffRating = (DropDownList)_grv.FindControl("ddlQuarterStaffRating");

                    TextBox _txtQuarterStaffComment = (TextBox)_grv.FindControl("txtQuarterStaffComment");
                    TextBox _txtNextQuarterActionPlan = (TextBox)_grv.FindControl("txtNextQuarterActionPlan");
                    //check if werer submit to validated whether the details required are entered
                    if (_hasStaffSubmitted == true && (_txtQuarterAchieved.Text.Trim() == "" || _ddlQuarterStaffRating.SelectedIndex == 0))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Some actual acheived and rating details have not been entered. Ensure that you have entered all actual achieved details and select rating for each and every KPI under review before submitting for review!", this, PanelPerformanceReview);
                        _broken = true;
                        return;
                    }
                    else
                    {
                        int _performanceObjectiveKPIID = Convert.ToInt32(_hfPerfomanceObjectiveKPIID.Value);
                        string _quarterAchieved = _txtQuarterAchieved.Text.Trim();
                        string _quarterStaffRating = "";
                        if (_ddlQuarterStaffRating.SelectedIndex != 0)
                            _quarterStaffRating = _ddlQuarterStaffRating.SelectedValue;
                        string _quarterStaffComment = _txtQuarterStaffComment.Text.Trim();
                        string _nextQuarterActionPlan = _txtNextQuarterActionPlan.Text.Trim();
                        //update the quarter under review
                        UpdateStaffQuarterlyKPIAchievedDetails(_performanceObjectiveKPIID, _quarter, _quarterAchieved, _quarterStaffRating, _quarterStaffComment, _nextQuarterActionPlan);
                    }
                }
                if (_broken == false)//check if the loop is brooken
                {
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    string _year = ddlAnnualPerformancePlanYear.SelectedValue;
                    string _staffComments = txtQuarterlyReviewStaffComments.Text.Trim();
                    //check if the quarterly review record exists
                    if (db.QuarterlyPerformanceReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter))
                    {
                        //update the existing record
                        UpdateQuarterlyReview(_staffID, _year, _quarter, _staffComments, _hasStaffSubmitted, _dateStaffSubmited);
                    }
                    else
                    {
                        //save new record
                        SaveNewQuarterlyReview(_staffID, _year, _quarter, _staffComments, _hasStaffSubmitted, _dateStaffSubmited);
                    }
                    _hrClass.LoadHRManagerMessageBox(2, "Quarter review details have been successfully saved. ", this, PanelPerformanceReview);
                    if (_hasStaffSubmitted == true)
                    {
                        //hide the buttons after submiting for review
                        lnkBtnSaveStaffQuarteryReview.Visible = false;
                        lnkBtnSubmitStaffQuarterlyReview.Visible = false;
                        gvQuarterlyReview.Visible = false;
                        gvQuarterlyReviewPrint.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //save the quater review
        protected void lnkBtnSaveStaffQuarteryReview_Click(object sender, EventArgs e)
        {
            SaveOverallQuarterlyReview(false, null); return;
        }
        //submit quater review for review
        protected void lnkBtnSubmitStaffQuarterlyReview_Click(object sender, EventArgs e)
        {
            SaveOverallQuarterlyReview(true, DateTime.Now); return;
        }

        //print quarterly review
        protected void LinkButtonPrintStaffQuarterlyReview_Click(object sender, EventArgs e)
        {
            //gvQuarterlyReview.AllowPaging = false;
            //gvQuarterlyReview.DataBind();
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            ////gvQuarterlyReview.RenderBeginTag(hw);
            ////gvQuarterlyReview.HeaderRow.RenderControl(hw);
            ////foreach (GridViewRow row in gvQuarterlyReview.Rows)
            ////{
            ////    row.RenderControl(hw);
            ////}
            ////gvQuarterlyReview.FooterRow.RenderControl(hw);
            ////gvQuarterlyReview.RenderEndTag(hw);

            //gvQuarterlyReview.RenderControl(hw);
            //string gridHTML = sw.ToString().Replace("\"", "'")
            //    .Replace(System.Environment.NewLine, "");
            //StringBuilder sb = new StringBuilder();
            //sb.Append("<script type = 'text/javascript'>");
            //sb.Append("window.onload = new function(){");
            //sb.Append("var printWin = window.open('', '', 'left=0");
            //sb.Append(",top=0,width=1000,height=600,status=0');");
            //sb.Append("printWin.document.write(\"");
            //sb.Append(gridHTML);
            //sb.Append("\");");
            //sb.Append("printWin.document.close();");
            //sb.Append("printWin.focus();");
            //sb.Append("printWin.print();");
            //sb.Append("printWin.close();};");
            //sb.Append("</script>");
            //ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //gvQuarterlyReview.DataBind();
            //return;

            gvQuarterlyReviewPrint.Visible = true;

            gvQuarterlyReviewPrint.UseAccessibleHeader = true;
            gvQuarterlyReviewPrint.HeaderRow.TableSection = TableRowSection.TableHeader;
            //gvQuarterlyReviewPrint.FooterRow.TableSection = TableRowSection.TableFooter;
            gvQuarterlyReviewPrint.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in gvQuarterlyReviewPrint.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }

            StringWriter sw = new StringWriter();

            sw.WriteLine("<b>PERFORMANCE REVIEW QUARTER " + ddlQuarterlyReviewQuarter.SelectedValue + "-" + ddlAnnualPerformancePlanYear.SelectedValue + "</b>");
            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            //get staffbasic info
            PROC_StaffBasicInfoResult getStaff = db.PROC_StaffBasicInfo(_staffID).FirstOrDefault();
            sw.WriteLine("<BR/><b>NAME:</b> " + getStaff.StaffName.ToUpper() + "<br/><b>DEPARTMENT:</b> " + getStaff.Department.ToUpper() + "<br/><b>DUTY STATION:</b> " + getStaff.PostedOffice.ToUpper());
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gvQuarterlyReviewPrint.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            //hw.AddStyleAttribute(HtmlTextWriterStyle.Color, "Red");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //gvQuarterlyReviewPrint.AllowPaging = true;
            //gvQuarterlyReviewPrint.DataBind();
            gvQuarterlyReviewPrint.Visible = false;
            return;
        }

        //upload quarterly review  document
        private void UploadQuarterlyReviewDocument()
        {
            try
            {
                if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
                {
                    ddlAnnualPerformancePlanYear.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Select performance year!", this, PanelPerformanceReview);
                    return;
                }
                else if (ddlQuarterlyReviewQuarter.SelectedIndex == 0)
                {
                    ddlQuarterlyReviewQuarter.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Select quarter being reviewed!", this, PanelPerformanceReview);
                    return;
                }
                else if (FileUploadQuarterlyReviewForm.HasFile)
                {
                    int _fileLength = FileUploadQuarterlyReviewForm.PostedFile.ContentLength;
                    //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
                    if (_hrClass.IsUploadedFileBig(_fileLength) == true)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "File uploaded exceeds the allowed limit of 3mb.", this, PanelPerformanceReview);
                        return;
                    }
                    else
                    {
                        string _uploadedDocFileName = FileUploadQuarterlyReviewForm.FileName;//get name of the uploaded file name
                        string _fileExtension = Path.GetExtension(_uploadedDocFileName);

                        _fileExtension = _fileExtension.ToLower();
                        string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".xls", ".xlsx", ".rtf" };

                        bool _isFileAccepted = false;
                        foreach (string _acceptedFileExtension in _acceptedFileTypes)
                        {
                            if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
                                _isFileAccepted = true;
                        }
                        if (_isFileAccepted == false)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "The file you are trying to upload is not a permitted file type! The file should be pdf, word or excel document.", this, PanelPerformanceReview);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                            string _year = ddlAnnualPerformancePlanYear.SelectedValue;
                            char _quarter = Convert.ToChar(ddlQuarterlyReviewQuarter.SelectedValue);

                            //save uploaded template document
                            SaveQuarterlyReviewDocument(_staffID, FileUploadQuarterlyReviewForm, _year, _quarter, _uploadedDocFileName, _fileExtension);
                            return;
                        }
                    }
                }
                else
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Browse for the appraisal template document to upload!", this, PanelPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }
        }

        //save quarterly review document
        private void SaveQuarterlyReviewDocument(Guid _staffID, FileUpload _fileUpload, string _year, char _quarter, string _uploadedDocFileName, string _fileExtension)
        {

            if (db.QuarterlyReviews.Any(p => p.StaffID == _staffID && p.Year == _year && p.Quarter == _quarter))
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Another quarterly review document for the same quarter and year selected already exists!", this, PanelPerformanceReview);
                return;
            }
            else
            {
                //save the document in the temp document
                _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
                string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
                //save the quartery review document into the database

                FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                //get document info
                FileInfo documentInfo = new FileInfo(_fileLocation);
                int fileSize = (int)documentInfo.Length;//get file size
                byte[] fileDocument = new byte[fileSize];
                fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

                //add the new document
                QuarterlyReview updateQuarterlyReview = new QuarterlyReview();
                updateQuarterlyReview.StaffID = _staffID;
                updateQuarterlyReview.Year = _year;
                updateQuarterlyReview.Quarter = _quarter;
                updateQuarterlyReview.StaffDocumentName = _uploadedDocFileName;
                updateQuarterlyReview.StaffUploadedDocument = fileDocument;
                updateQuarterlyReview.DateStaffUploaded = DateTime.Now;
                db.QuarterlyReviews.InsertOnSubmit(updateQuarterlyReview);
                db.SubmitChanges();
                //dispaly staff quartelry review documents
                DisplayStaffQuarterlyReviewDocuments(_staffID);
                _hrClass.LoadHRManagerMessageBox(2, "Quarterly review document has been successfully uploaded!", this, PanelPerformanceReview);
                return;
            }
        }
        //upload the filled quarterly review document
        protected void lnkBtnUploadQuarterlyReviewForm_OnClick(object sender, EventArgs e)
        {
            UploadQuarterlyReviewDocument(); return;
        }
        //display staff quarterly review documents
        private void DisplayStaffQuarterlyReviewDocuments(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffQuarterlyReview(_staffID);
                gvQuarterlyReviewDocuments.DataSourceID = null;
                gvQuarterlyReviewDocuments.DataSource = _display;
                gvQuarterlyReviewDocuments.DataBind();
                return;
            }
            catch { }
        }
        //load quartely review document details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvQuarterlyReviewDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFQuarterlyReviewID = (HiddenField)gvQuarterlyReviewDocuments.Rows[ID].FindControl("HiddenField1");
                    int _quarterlyReviewID = Convert.ToInt32(_HFQuarterlyReviewID.Value);
                    if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
                    {
                        HiddenFieldQuarterlyReviewFormID.Value = _quarterlyReviewID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the quarterly review document?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    }
                    else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
                    {
                        string strPageUrl = "user_document_Viewer.aspx?quarterlyreview=" + _HFQuarterlyReviewID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //method for deleting an quarterly review document
        private void DeleteQuarterlyReviewDocument()
        {
            try
            {
                int _quarterlyReviewID = Convert.ToInt32(HiddenFieldQuarterlyReviewFormID.Value);
                QuarterlyReview deleteQuarterlyReview = db.QuarterlyReviews.Single(p => p.QuarterlyReviewID == _quarterlyReviewID);
                db.QuarterlyReviews.DeleteOnSubmit(deleteQuarterlyReview);
                db.SubmitChanges();
                DisplayStaffQuarterlyReviewDocuments((Guid)deleteQuarterlyReview.StaffID);//reload quarterly review documents after deletion
                _hrClass.LoadHRManagerMessageBox(2, "Quarterly review document selected has been successfully deleted.", this, PanelPerformanceReview);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelPerformanceReview);
                return;
            }
        }
        //delete records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            if (TabContainerPerformanceReview.ActiveTabIndex == 0)//performance plan tab
                DeletePerformancePlanObjective();
            else if (TabContainerPerformanceReview.ActiveTabIndex == 1)//Quarterly review tab
                DeleteQuarterlyReviewDocument();
        }
        #endregion
        #region STAFF TO APPRAISE
        //chech the reveiw type
        protected void rblSupervisorReviewType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblSupervisorReviewType.SelectedIndex == 0)
            {
                LabelSupervisorReviewQuarter.Visible = true;
                LabelSupervisorReviewQuarterError.Visible = true;
                ddlSupervisorQuarterlyReviewQuarter.Visible = true;
                return;
            }
            else
            {
                LabelSupervisorReviewQuarter.Visible = false;
                LabelSupervisorReviewQuarterError.Visible = false;
                ddlSupervisorQuarterlyReviewQuarter.Visible = false;
                return;
            }
        }
        //display staff staff to appraise
        private void DisplayStaffToAppraise(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffUnderSupervisor(_staffID);
                gvStaffToAppraise.DataSourceID = null;
                gvStaffToAppraise.DataSource = _display;
                Session["gvStaffToAppraiseData"] = _display;
                gvStaffToAppraise.DataBind();
                return;
            }
            catch { }
        }
        //load staff to review performance details 
        protected void gvStaffToAppraise_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
            if (e.CommandName.CompareTo("ReviewPerformance") == 0)
            {
                //check if  performance year and review types are selected
                if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No performance year selected. Select year under review!", this, PanelPerformanceReview);
                    return;
                }
                else if (rblSupervisorReviewType.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select review type!", this, PanelPerformanceReview);
                    return;
                }
                else if (rblSupervisorReviewType.SelectedIndex == 0 && ddlSupervisorQuarterlyReviewQuarter.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select quarter under review!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFStaffID = (HiddenField)gvStaffToAppraise.Rows[ID].FindControl("HiddenField1");
                    Guid _staffID = _hrClass.ReturnGuid(_HFStaffID.Value);
                    int _reviewType = rblSupervisorReviewType.SelectedIndex;//get the review type index 0 for quarterly, 1 for annual review
                    LoadStaffToAppraiseDetails(_staffID, _reviewType);//load staff to appraise details
                    return;
                }
            }
            }
            catch (Exception)
            {
                return;
            }
        }
        //load staff to review perfomance details
        private void LoadStaffToAppraiseDetails(Guid _staffID, int _reviewType)
        {
            HiddenFieldStaffToAppraiseID.Value = _staffID.ToString();
            string _year = ddlAnnualPerformancePlanYear.SelectedValue;
            //check review type
            if (_reviewType == 0)//quarterly review
            {
                int _quarter = Convert.ToInt32(ddlSupervisorQuarterlyReviewQuarter.SelectedValue);
                //load staff review details for the quarter
                LoadStaffQuarterlyPerformanceForSupervisorReview(_staffID, _year, _quarter);
                return;
            }
            else if (_reviewType == 1)//annual review
            {
            }
            return;
        }

        //display staff quarter details for the selected quarter
        private void LoadStaffQuarterlyPerformanceForSupervisorReview(Guid _staffID, string _year, int _quarter)
        {
            HiddenFieldStaffToAppraiseID.Value = _staffID.ToString();
            string _staffName = _hrClass.getStaffNames(_staffID);
            //check if the staff has submitted thequareterly review
            if (db.QuarterlyPerformanceReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter))
            {

                //check if the staff has submitted the quarterly review
                QuarterlyPerformanceReview getQuarterlyReview = db.QuarterlyPerformanceReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter);
                if (getQuarterlyReview.HasStaffSubmitted == false)
                {
                    _hrClass.LoadHRManagerMessageBox(1, _staffName + " has not yet submited quarterly review for appraisal? ", this, PanelPerformanceReview);
                    return;
                }
                else
                {

                    string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                    string _reviewQuarterHeader = "Review " + _staffName + " Quarter " + _quarter + " Performance for the Year " + _performanceYear;
                    lbSupervisorQuarterlyReviewHeader.Text = _reviewQuarterHeader;
                    HiddenFieldQuarter.Value = _quarter.ToString();

                    double _overallAverageRating = 0, _percentageWeight = 0, _averageRating = 0;//for calculating average rating
                    //create a data table
                    //populate quarterly review details associated with the staff for the selected year

                    //generate tha data table with quartery data
                    DataTable dt = GenerateStaffQuarterlyReviewDataTable(_staffID, _year, _quarter);
                    int count = dt.Rows.Count;//get the rows count


                    gvSupervisorQuarterlyReview.DataSourceID = null;
                    gvSupervisorQuarterlyReview.DataSource = dt;
                    gvSupervisorQuarterlyReview.DataBind();
                    Session["gvSupervisorQuarterlyReviewData"] = dt;
                    GenerateGridViewRowUniqueData(gvSupervisorQuarterlyReview, 1);//get distinct performance objectives rows
                    GenerateGridViewRowUniqueData(gvSupervisorQuarterlyReview, 2);//get destinct performance objective weight rows

                    //populate gridview to be printed
                    gvSupervisorQuarterlyReviewPrint.DataSourceID = null;
                    gvSupervisorQuarterlyReviewPrint.DataSource = dt;
                    gvSupervisorQuarterlyReviewPrint.DataBind();
                    GenerateGridViewRowUniqueData(gvSupervisorQuarterlyReviewPrint, 1);//get distinct performance objectives rows
                    GenerateGridViewRowUniqueData(gvSupervisorQuarterlyReviewPrint, 2);//get destinct performance objective weight rows

                    //get staff comments and date submited
                    lbStaffQuarterlyReviewComment.Text = getQuarterlyReview.StaffComments;
                    lbStaffQuarterlyReviewSubmitionDate.Text = getQuarterlyReview.DateStaffSubmitted.ToString();
                    txtSupervisorQuarterlyReviewComments.Text = Convert.ToString(getQuarterlyReview.AppraiserComments);
                    //check if the appraiser has submited the review to the HR
                    if (getQuarterlyReview.HasAppraiserSubmitted == true)
                    {
                        //hide supervisor save & submit buttons
                        lnkBtnSaveSupervisorQuarterlyReview.Visible = false;
                        lnkBtnSubmitSupervisorQuarterlyReview.Visible = false;
                        gvSupervisorQuarterlyReview.Visible = false;
                        gvSupervisorQuarterlyReviewPrint.Visible = true;
                    }
                    else
                    {
                        //show supervisor save & submit buttons
                        lnkBtnSaveSupervisorQuarterlyReview.Visible = true;
                        lnkBtnSubmitSupervisorQuarterlyReview.Visible = true;
                        gvSupervisorQuarterlyReview.Visible = true;
                        gvSupervisorQuarterlyReviewPrint.Visible = false;
                    }

                    PanelSupervisorStaffToAppraiseHeader.Visible = false;
                    PanelSupervisorQuarterlyReview.Visible = true;
                    return;
                }

            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, _staffName + " has not yet started quarterly review process. There is no record to review? ", this, PanelPerformanceReview);
                return;
            }
        }

        //row data bound for supervior quarterly review gridview
        protected void gvSupervisorQuarterlyReview_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //find supervisor rating 
                HiddenField _HiddenFieldQuarterSupervisorRating = (HiddenField)e.Row.FindControl("HiddenFieldQuarterSupervisorRating");

                DropDownList _ddlQuarterSupervisorRating = (DropDownList)e.Row.FindControl("ddlQuarterSupervisorRating");
                if (_HiddenFieldQuarterSupervisorRating.Value != "")
                {
                    _ddlQuarterSupervisorRating.SelectedValue = _HiddenFieldQuarterSupervisorRating.Value;
                }
                else
                    _ddlQuarterSupervisorRating.SelectedIndex = 0;
            }

        }
        //update staff' quarter KPI achieved details review by supervisor
        private void UpdateStaffQuarterlyAchievedDetailsSupervisorReview(int _performanceObjectiveKPIID, short _quarter, string _quarterSupervisorRating, string _quarterSupervisorComment)
        {
            PerformanceObjectiveKPI updateKPI = db.PerformanceObjectiveKPIs.Single(p => p.PerformanceObjectiveKPIID == _performanceObjectiveKPIID);
            if (_quarter == 1)//update quarter 1
            {
                updateKPI.Quarter1SupervisorRating = _quarterSupervisorRating;
                updateKPI.Quarter1SupervisorComments = _quarterSupervisorComment;
            }
            else if (_quarter == 2)//update quarter 2
            {
                updateKPI.Quarter2SupervisorRating = _quarterSupervisorRating;
                updateKPI.Quarter2SupervisorComments = _quarterSupervisorComment;
            }
            else if (_quarter == 3)//update quarter 3
            {
                updateKPI.Quarter3SupervisorRating = _quarterSupervisorRating;
                updateKPI.Quarter3SupervisorComments = _quarterSupervisorComment;
            }
            else if (_quarter == 4)//update quarter 4
            {
                updateKPI.Quarter4SupervisorRating = _quarterSupervisorRating;
                updateKPI.Quarter4SupervisorComments = _quarterSupervisorComment;
            }
            db.SubmitChanges();
        }
        //update existing quarterly review with supervisor review details
        private void UpdateQuarterlyReviewWithSupervisorReview(Guid _staffID, string _year, short _quarter, string _supervisorComments, bool _hasSupervisorSubmitted, DateTime? _dateSupervisorSubmited)
        {
            QuarterlyPerformanceReview updateQuarterlyReview = db.QuarterlyPerformanceReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _year && p.QuarterUnderReview == _quarter);

            updateQuarterlyReview.AppraiserComments = _supervisorComments;
            updateQuarterlyReview.HasAppraiserSubmitted = _hasSupervisorSubmitted;
            updateQuarterlyReview.AppraisalDate = _dateSupervisorSubmited;
            db.SubmitChanges();
        }
        //save supervisor overall quartely review
        private void SaveSupervisorOverallQuarterlyReview(bool _hasSupervisorSubmitted, DateTime? _dateSupervisorSubmited)
        {
            try
            {
                bool _broken = false;
                short _quarter = Convert.ToInt16(ddlSupervisorQuarterlyReviewQuarter.SelectedValue);
                foreach (GridViewRow _grv in gvSupervisorQuarterlyReview.Rows)
                {
                    HiddenField _hfPerfomanceObjectiveKPIID = (HiddenField)_grv.FindControl("HiddenFieldPerformanceObjectiveKPIID");
                    DropDownList _ddlQuarterSupervisorRating = (DropDownList)_grv.FindControl("ddlQuarterSupervisorRating");
                    TextBox _txtQuarterSupervisorComment = (TextBox)_grv.FindControl("txtQuarterSupervisorComment");
                    //check if werer submit to validated whether the details required are entered
                    if (_hasSupervisorSubmitted == true && _ddlQuarterSupervisorRating.SelectedIndex == 0)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Supervisor rating details have not been set against some KPIs. Ensure that you have selected rating for each and every KPI under review before submitting to the HR for review!", this, PanelPerformanceReview);
                        _broken = true;
                        return;
                    }
                    else
                    {
                        int _performanceObjectiveKPIID = Convert.ToInt32(_hfPerfomanceObjectiveKPIID.Value);
                        string _quarterSupervisorRating = "";
                        if (_ddlQuarterSupervisorRating.SelectedIndex != 0)
                            _quarterSupervisorRating = _ddlQuarterSupervisorRating.SelectedValue;
                        string _quarterSupervisorComment = _txtQuarterSupervisorComment.Text.Trim();
                        //update the quarter under review KPI with the supervisor details
                        UpdateStaffQuarterlyAchievedDetailsSupervisorReview(_performanceObjectiveKPIID, _quarter, _quarterSupervisorRating, _quarterSupervisorComment);
                    }
                }
                if (_broken == false)//check if the loop is brooken
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                    string _year = ddlAnnualPerformancePlanYear.SelectedValue;
                    string _supervisorComments = txtSupervisorQuarterlyReviewComments.Text.Trim();
                    //check if the quarterly review record exists
                    UpdateQuarterlyReviewWithSupervisorReview(_staffID, _year, _quarter, _supervisorComments, _hasSupervisorSubmitted, _dateSupervisorSubmited);

                    if (_hasSupervisorSubmitted == true)
                    {
                        //hide the buttons after submiting for review
                        lnkBtnSaveStaffQuarteryReview.Visible = false;
                        lnkBtnSubmitStaffQuarterlyReview.Visible = false;
                        gvQuarterlyReview.Visible = false;
                        gvQuarterlyReviewPrint.Visible = true;
                        _hrClass.LoadHRManagerMessageBox(2, "Quarter review details have been successfully saved and submited to HR for review!", this, PanelPerformanceReview);
                    }
                    else
                    {
                        _hrClass.LoadHRManagerMessageBox(2, "Quarter review details have been successfully saved.", this, PanelPerformanceReview);

                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //save the supervisor quarter review
        protected void lnkBtnSaveSupervisorQuarterlyReview_Click(object sender, EventArgs e)
        {
            SaveSupervisorOverallQuarterlyReview(false, null); return;
        }
        //submit supervisor quarter review for review
        protected void lnkBtnSubmitSupervisorQuarterlyReview_Click(object sender, EventArgs e)
        {
            SaveSupervisorOverallQuarterlyReview(true, DateTime.Now); return;
        }

        //print quarterly review with supervisor details
        protected void LinkButtonBtnPrintSupervisorQuarterlyReview_Click(object sender, EventArgs e)
        {

            gvSupervisorQuarterlyReviewPrint.Visible = true;

            gvSupervisorQuarterlyReviewPrint.UseAccessibleHeader = true;
            gvSupervisorQuarterlyReviewPrint.HeaderRow.TableSection = TableRowSection.TableHeader;
            //gvSupervisorQuarterlyReviewPrint.FooterRow.TableSection = TableRowSection.TableFooter;
            gvSupervisorQuarterlyReviewPrint.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in gvSupervisorQuarterlyReviewPrint.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }

            StringWriter sw = new StringWriter();

            sw.WriteLine("<b>PERFORMANCE REVIEW QUARTER " + ddlQuarterlyReviewQuarter.SelectedValue + "-" + ddlAnnualPerformancePlanYear.SelectedValue + "</b>");
            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
            //get staffbasic info
            PROC_StaffBasicInfoResult getStaff = db.PROC_StaffBasicInfo(_staffID).FirstOrDefault();
            sw.WriteLine("<BR/><b>NAME:</b> " + getStaff.StaffName.ToUpper() + "<br/><b>DEPARTMENT:</b> " + getStaff.Department.ToUpper() + "<br/><b>DUTY STATION:</b> " + getStaff.PostedOffice.ToUpper());
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gvSupervisorQuarterlyReviewPrint.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            //hw.AddStyleAttribute(HtmlTextWriterStyle.Color, "Red");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //gvSupervisorQuarterlyReviewPrint.AllowPaging = true;
            //gvSupervisorQuarterlyReviewPrint.DataBind();
            gvSupervisorQuarterlyReviewPrint.Visible = false;
            return;
        }
        //close staff to appraise details
        protected void ImageButtonClosePanelSupervisorQuarterlyReview_Click(object sender, EventArgs e)
        {
            PanelSupervisorStaffToAppraiseHeader.Visible = true;
            PanelSupervisorQuarterlyReview.Visible = false;
            HiddenFieldStaffToAppraiseID.Value = "";
            return;
        }


        #endregion
        //load rating scale
        private void DisplayRatingScales()
        {
            object _display;
            _display = db.RatingScales.Select(p => p).OrderBy(p => p.Rate);
            gvRatingScales.DataSourceID = null;
            gvRatingScales.DataSource = _display;
            gvRatingScales.DataBind();
            return;
        }
    }
}