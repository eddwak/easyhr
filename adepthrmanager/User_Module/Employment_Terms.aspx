﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Employment_Terms.aspx.cs" Inherits="AdeptHRManager.User_Module.Employment_Terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelEmploymentTerms" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelEmploymentTerms" runat="server">
                <div class="panel-details">
                    <fieldset>
                        <legend>Employment Terms Details</legend>
                        <table width="100%">
                            <tr valign="top">
                                <td>
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsID" runat="server" />
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsPerksDetailID" runat="server" />
                                    Joining Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtJoinDate" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Job Title:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobTitle" Enabled="false" runat="server">
                                        <asp:ListItem Value="0">-Select Job TItle-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contract Type:
                                </td>                               
                                <td>
                                    <asp:DropDownList ID="ddlContractType" Enabled="false" runat="server">
                                        <asp:ListItem Value="0">-Select Contract Type-</asp:ListItem>
                                        <asp:ListItem Value="1">Full-Time</asp:ListItem>
                                        <asp:ListItem Value="2">Contract</asp:ListItem>
                                        <asp:ListItem Value="3">Temporary</asp:ListItem>
                                        <asp:ListItem Value="">Intern</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                  <td>
                                    Basic Pay:
                                </td> 
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsBasicPay" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    Contract Start Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContractStartDate" Enabled="false" CssClass="textBoxDetails" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Contract End Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContractEndDate"  CssClass="textBoxDetails" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Role (Summary):
                                </td>
                                <td>
                                    <asp:TextBox ID="txtJobRole" runat="server" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                                </td>
                               <td valign="top">
                                    Supervisor: 
                                </td>
                                <td valign="top">
                                    <asp:DropDownList ID="ddlReportsTo" Enabled="false"  runat="server">
                                        <asp:ListItem Value="0">-Select Reports To-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>                        
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
