﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Attendance : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        static Guid _loggedStaffID = Guid.Empty;
        string _employeeUnderStaffID = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                txtFromDate.Attributes.Add("readonly", "readonly");//make attendance from date texbox readony 
                txtToDate.Attributes.Add("readonly", "readonly");//make attendance to date text box readonly
                //try
                //{
                //get the logged in employee staff master id
                _loggedStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                //get the employee under staff master id(passed id on the url)
                try
                {
                    _employeeUnderStaffID = Request.QueryString["eunderid"].ToString();
                }
                catch { }
                //check if employee under id is null and load logged in employee print outs details
                if (_employeeUnderStaffID == null)
                {
                    DisplayEmployeeAttendanceForCurrentWeek(_loggedStaffID);
                }
                else//load employee under print outs details
                {
                    DisplayEmployeeAttendanceForCurrentWeek(_hrClass.ReturnGuid(_employeeUnderStaffID));
                    HiddenFieldEmployeeUnderStaffID.Value = _employeeUnderStaffID;
                }
                //}
                //catch { }

            }
        }

       
        //display employee attendance for the currnt week
        private void DisplayEmployeeAttendanceForCurrentWeek(Guid _staffID)
        {
            DayOfWeek day = DateTime.Now.DayOfWeek;//get current day
            int days = day - DayOfWeek.Monday;//get day wher monday of the current week falls
            DateTime weekStartDate = DateTime.Now.AddDays(-days);//get date for monday of the current week
            DateTime weekEndDate = weekStartDate.AddDays(6);//get date for sunday of the current week by adding s 6 days to week start date
            txtFromDate.Text = _hrClass.ShortDateDayStart(weekStartDate.ToString());
            txtToDate.Text = _hrClass.ShortDateDayStart(weekEndDate.ToString());
            //display attendance details for the employee during the current week
            LoadEmployeeAttendancePerDuration(_staffID, weekStartDate, weekEndDate);
            HiddenFieldStaffID.Value = _staffID.ToString();
            return;
        }

        //load employee attendance per duration 
        private void LoadEmployeeAttendancePerDuration(Guid _staffID, DateTime fromDate, DateTime toDate)
        {
            try
            {
                //get staff refference number asssoicated with the staff id
                string _staffRef = _hrClass.GetStaffReferenceByStaffID(_staffID);
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("Date", typeof(string)));//get leave types
                dt.Columns.Add(new DataColumn("CheckType", typeof(string)));
                dt.Columns.Add(new DataColumn("Time", typeof(string)));
                dt.Columns.Add(new DataColumn("CheckPlace", typeof(string)));
                dt.Columns.Add(new DataColumn("MinutesLate", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //}
                //get time and attendance for the logged employee
                foreach (PROC_SingleEmployeeCheckInCheckOutPerDurationResult _attendance in db.PROC_SingleEmployeeCheckInCheckOutPerDuration(_staffRef, fromDate.Date, toDate.Date))
                {
                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = _attendance.DATE___TIME.Value.ToLongDateString();
                    dr[2] = _attendance.CHECK_TYPE;
                    string _time = _attendance.DATE___TIME.Value.TimeOfDay.ToString();
                    if (_time == "00:00:00")
                        _time = "N/A";
                    dr[3] = _time;
                    string _checkPlace = "N/A";
                    if (_attendance.CHECK_PLACE != null)
                        _checkPlace = _hrClass.ConvertFirstStringLettersToUpper(_attendance.CHECK_PLACE);
                    dr[4] = _checkPlace;
                    dr[5] = _attendance.MINUTES_LATE.ToString();
                    dt.Rows.Add(dr);
                    countRows++;
                }
                Session["gvEmployeeAttendanceDetailsData"] = dt;
                gvEmployeeAttendanceDetails.DataSource = dt;
                gvEmployeeAttendanceDetails.DataBind();
                return;
            }
            catch { }
        }
        //page index changing
        protected void gvEmployeeAttendanceDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvEmployeeAttendanceDetails.PageIndex = e.NewPageIndex;
                Session["gvEmployeeAttendanceDetailsPageIndex"] = e.NewPageIndex;
                gvEmployeeAttendanceDetails.DataSource = (object)Session["gvEmployeeAttendanceDetailsData"];
                gvEmployeeAttendanceDetails.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }
        protected void gvEmployeeAttendanceDetails_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvEmployeeAttendanceDetailsPageIndex"] == null)//check if page index is empty
                {
                    gvEmployeeAttendanceDetails.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvEmployeeAttendanceDetailsPageIndex"]);
                    gvEmployeeAttendanceDetails.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //load all attendance details for the employee 
        private void LoadAllAttendanceDetailsForTheEmployee(Guid _staffID)
        {
            //get logged employee's number
            //get staff refference number asssoicated with the staff id
            string _staffRef = _hrClass.GetStaffReferenceByStaffID(_staffID);
            //get current date
            DateTime currentDate = DateTime.Now.Date;
            txtToDate.Text = _hrClass.ShortDateDayStart(currentDate.ToString());
            //get minimum date when the first ever check in or check out record was saved
            DateTime minimumDate = Convert.ToDateTime(db.CheckInCheckOuts.Where(p => p.checkincheckout_vEmployeeNo == _staffRef).Min(p => Convert.ToDateTime(p.checkincheckout_vCheckDate)));
            txtFromDate.Text = _hrClass.ShortDateDayStart(minimumDate.ToString());
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
            dt.Columns.Add(new DataColumn("Date", typeof(string)));//get leave types
            dt.Columns.Add(new DataColumn("CheckType", typeof(string)));
            dt.Columns.Add(new DataColumn("Time", typeof(string)));
            dt.Columns.Add(new DataColumn("CheckPlace", typeof(string)));
            dt.Columns.Add(new DataColumn("MinutesLate", typeof(string)));
            DataRow dr;
            int countRows = 1;
            //get time and attendance for the logged employee
            foreach (PROC_SingleEmployeeCheckInCheckOutPerDurationResult _attendance in db.PROC_SingleEmployeeCheckInCheckOutPerDuration(_staffRef, minimumDate.Date, currentDate.Date))
            {
                dr = dt.NewRow();
                dr[0] = countRows.ToString() + ".";
                dr[1] = _attendance.DATE___TIME.Value.ToLongDateString();
                dr[2] = _attendance.CHECK_TYPE;
                string _time = _attendance.DATE___TIME.Value.TimeOfDay.ToString();
                if (_time == "00:00:00")
                    _time = "N/A";
                dr[3] = _time;
                string _checkPlace = "N/A";
                if (_attendance.CHECK_PLACE != null)
                    _checkPlace = _hrClass.ConvertFirstStringLettersToUpper(_attendance.CHECK_PLACE);
                dr[4] = _checkPlace;
                dr[5] = _attendance.MINUTES_LATE.ToString();
                dt.Rows.Add(dr);
                countRows++;
            }
            Session["gvEmployeeAttendanceDetailsData"] = dt;
            gvEmployeeAttendanceDetails.DataSource = dt;
            gvEmployeeAttendanceDetails.DataBind();

        }
        protected void btnViewAllAttendance_Click(object sender, EventArgs e)
        {
            try
            {
                LoadAllAttendanceDetailsForTheEmployee(_hrClass.ReturnGuid(HiddenFieldStaffID.Value));
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }
        //display employee attendnce when the from date text is changed
        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    DateTime fromDate = Convert.ToDateTime(txtFromDate.Text).Date;
                    DateTime toDate = Convert.ToDateTime(txtToDate.Text).Date;
                    LoadEmployeeAttendancePerDuration(_hrClass.ReturnGuid(HiddenFieldStaffID.Value), fromDate, toDate);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }
        //load test attendance
        private void LoadTestAttendance()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("LeaveID", typeof(int)));
            dt.Columns.Add(new DataColumn("FromDate", typeof(string)));
            dt.Columns.Add(new DataColumn("ToDate", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfDays", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfHours", typeof(string)));
            dt.Columns.Add(new DataColumn("Reason", typeof(string)));
            DataRow dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "01/Oct/2014";
            dr[2] = "03/Oct/2014";
            dr[3] = "3";
            dr[4] = "4";
            dr[5] = "Sick";
            dt.Rows.Add(dr);

            DataRow dr2 = dt.NewRow();
            dr2[0] = 2;
            dr2[1] = "11/Aug/2014";
            dr2[2] = "17/Aug/2014";
            dr2[3] = "5";
            dr2[4] = "3";
            dr2[5] = "No Reason";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3[0] = 3;
            dr3[1] = "22/May/2014";
            dr3[2] = "29/May/2014";
            dr3[3] = "8";
            dr3[4] = "0";
            dr3[5] = "Attending Funeral";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4[0] = 4;
            dr4[1] = "25/Feb/2014";
            dr4[2] = "25/Feb/2014";
            dr4[3] = "1";
            dr4[4] = "0";
            dr4[5] = "No Reason";
            dt.Rows.Add(dr4);
            gvEmployeeAttendanceDetails.DataSource = dt;
            gvEmployeeAttendanceDetails.DataBind();
        }
    }
}