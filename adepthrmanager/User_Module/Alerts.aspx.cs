﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Alerts : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        DateTime _TodayDate = System.DateTime.Today;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadStaff();
            }

        }
        private void LoadStaff()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                DisplayContractEnd(_staffID);
                DisplayProbationEnd(_staffID);
                DisplayRetirementEnd(_staffID);
                DisplayTenancyEnd(_staffID);
                DisplayCarEnd(_staffID);
                DisplayWorkPermitEnd(_staffID);
                DisplayDependentPassExpiry(_staffID);
                DisplayVisaExpiry(_staffID);

            }

            catch { }
        }

        #region 'LOAD ALERTS'

      
        //Display Contract End Date
        private void DisplayContractEnd(Guid _staffID)
        {
            try
            {
                PROC_GetContractEndResult GetContractEnd = db.PROC_GetContractEnd(_staffID).FirstOrDefault();
                if (GetContractEnd.StaffID != null)
                {
                    DateTime _ContractEnd = Convert.ToDateTime(GetContractEnd.EndContractDate);
                    int _ContractEndDifference = (_ContractEnd - _TodayDate).Days;

                    if (_ContractEndDifference <= 120)
                    {

                        lblContractEnd.Text = "The contract end date is " + _hrClass.ShortDateDayStart(GetContractEnd.EndContractDate.ToString());
                        lblContractEnd.Visible = true;
                    }
                }
                else
                {
                    lblContractEnd.Visible = false;
                }
                return;
            }
            catch { }
        }
        //Probation End Date
        private void DisplayProbationEnd(Guid _staffID)
        {
            try
            {
                StaffJobDetail getStaffJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);


                if (getStaffJobDetail.StaffID != null)
                {
                    DateTime _ProbationEnd = Convert.ToDateTime(getStaffJobDetail.ProbationEndDate);
                    int _ProbationEndDifference = (_ProbationEnd - _TodayDate).Days;
                    if (_ProbationEndDifference <= 90)
                    {

                        lblProbationEnd.Text = "The probation end date is " + _hrClass.ShortDateDayStart(getStaffJobDetail.ProbationEndDate.ToString());
                        lblProbationEnd.Visible = true;
                    }
                }
                else
                {
                    lblProbationEnd.Visible = false;
                }
                return;
            }
            catch { }
        }
        //Retirement End Date
        private void DisplayRetirementEnd(Guid _staffID)
        {
            try
            {
                EmpTerm getEmpTerm = db.EmpTerms.FirstOrDefault(p => p.empterms_uiStaffID == _staffID);

                if (getEmpTerm.empterms_uiStaffID != null)
                {
                    DateTime _RetirementEnd = Convert.ToDateTime(getEmpTerm.empterms_RetirementDate);
                    int _RetirementEndDifference = (_RetirementEnd - _TodayDate).Days;
                    if (_RetirementEndDifference <= 210)
                    {

                        lblRetirementEnd.Text = "The retirement end date is " + _hrClass.ShortDateDayStart(getEmpTerm.empterms_RetirementDate.ToString());
                        lblRetirementEnd.Visible = true;
                    }
                }
                else
                {
                    lblRetirementEnd.Visible = false;
                }
                return;
            }
            catch { }
        }

        //Tenancy Agreement End Date
        private void DisplayTenancyEnd(Guid _staffID)
        {
            try
            {
                StaffHouse getStaffHouse = db.StaffHouses.Single(p => p.StaffID == _staffID);



                if (getStaffHouse.StaffID != null)
                {
                    DateTime _TenancyEnd = Convert.ToDateTime(getStaffHouse.TenancyAgreementEndDate);
                    int _TenancyEndDifference = (_TenancyEnd - _TodayDate).Days;
                    if (_TenancyEndDifference <= 60)
                    {

                        lblTenancyAgreement.Text = "The tenancy agreement end date is " + _hrClass.ShortDateDayStart(getStaffHouse.TenancyAgreementEndDate.ToString());
                        lblTenancyAgreement.Visible = true;
                    }
                    else
                    {
                        lblTenancyAgreement.Visible = false;
                    }
                }

                return;
            }
            catch { }
        }
        //Tenancy Agreement End Date
        private void DisplayCarEnd(Guid _staffID)
        {
            try
            {
                StaffVehicle getStaffVehicle = db.StaffVehicles.Single(p => p.StaffID == _staffID);

                if (getStaffVehicle.StaffID != null)
                {
                    DateTime _CarEnd = Convert.ToDateTime(getStaffVehicle.VehicleAgreementEndDate);
                    int _CarEndDifference = (_CarEnd - _TodayDate).Days;
                    if (_CarEndDifference <= 30)
                    {

                        lblCarAgreement.Text = "The vehicle agreement end date is " + _hrClass.ShortDateDayStart(getStaffVehicle.VehicleAgreementEndDate.ToString());
                        lblCarAgreement.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }

                return;
            }
            catch { }
        }
        //Display Work Permit End Date
        private void DisplayWorkPermitEnd(Guid _staffID)
        {
            try
            {
                PROC_GetContractWorkPermitExpiryResult GetWorkPermitEnd = db.PROC_GetContractWorkPermitExpiry(_staffID).FirstOrDefault();
                if (GetWorkPermitEnd.StaffID != null)
                {
                    DateTime _WorkPermitEnd = Convert.ToDateTime(GetWorkPermitEnd.JobWorkPermitExpiryDate);
                    int _WorkPermitEndDifference = (_WorkPermitEnd - _TodayDate).Days;
                    if (_WorkPermitEndDifference <= 60)
                    {
                        lblWorkPermitEnd.Text = "The work permit end date is " + _hrClass.ShortDateDayStart(GetWorkPermitEnd.JobWorkPermitExpiryDate.ToString());
                        lblWorkPermitEnd.Visible = true;
                    }
                    else
                    {
                        lblWorkPermitEnd.Visible = false;
                    }
                }

                return;
            }
            catch { }
        }

        //Display VISA expiry date 
        private void DisplayVisaExpiry(Guid _staffID)
        {
            try
            {
                StaffJobDetail getJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);

                if (getJobDetail.StaffID != null)
                {
                    DateTime _VisaExpiry = Convert.ToDateTime(getJobDetail.VisaExpiryDate);
                    int _VisaExpiryDifference = (_VisaExpiry - _TodayDate).Days;
                    if (_VisaExpiryDifference <= 60)
                    {

                        lblVisaRenewal.Text = "The Visa expiry date is " + _hrClass.ShortDateDayStart(getJobDetail.VisaExpiryDate.ToString());
                        lblVisaRenewal.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }

                return;
            }
            catch { }
        }


        //Display Dependants Pass Expiry Date
        private void DisplayDependentPassExpiry(Guid _staffID)
        {
            try
            {
                Spouse getSpouse = db.Spouses.FirstOrDefault(p => p.spouse_uiStaffID == _staffID);

                if (getSpouse.spouse_uiStaffID != null)
                {
                    DateTime _DependentPassExpiry = Convert.ToDateTime(getSpouse.spouse_vPassExpiryDate).Date;
                    int _DependentPassExpiryDifference = (_DependentPassExpiry - _TodayDate).Days;
                    if (_DependentPassExpiryDifference <= 60)
                    {

                        lblDependentPassExpiry.Text = "The dependents' expiry date is " + getSpouse.spouse_vPassExpiryDate.ToString();
                        lblDependentPassExpiry.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }

                return;
            }
            catch { }
        }
        #endregion
    }
}