﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class PersonalInformation : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            //display staff additional controls
            LoadStaffAdditionalControls();
            LoadStaffPErsonalInforamtionControls();//load personal info
            LoadStaffAdditionalDetails();//load staff additional fields
        }

        //display staff additional controls allowed
        private void LoadStaffAdditionalControls()
        {
            XDocument xmlDoc = XDocument.Load(Server.MapPath("~/xml_Files/Staff_AdditionalInformation.xml"));//read the document
            var staffAdditionalControls = from r in xmlDoc.Descendants("control")//get the required details
                                          select new
                                          {
                                              ControlID = r.Element("controlid").Value.Trim(),
                                              ControlName = r.Element("controlname").Value.Trim(),
                                              IsNumeric = r.Element("isnumeric").Value.Trim(),
                                              Active = r.Element("active").Value.Trim(),
                                          };
            foreach (var _control in staffAdditionalControls)
            {
                if (_control.Active == "Yes")
                {
                    //get text box associated with the conteol
                    TextBox _textbox = (TextBox)PanelStaffAdditionalFields.FindControl(_control.ControlID);
                    _textbox.Visible = true;
                    //get label associated with the control
                    Label _label = (Label)PanelStaffAdditionalFields.FindControl(_control.ControlID.Replace("txt", "lb"));
                    _label.Text = _control.ControlName + ":";
                    _label.Visible = true;
                }
            }
            return;

        }
        // populate the edit staff records controls
        private void LoadStaffPErsonalInforamtionControls()
        {
            try
            {

                //Panel _userModuleHeaderPanel = (Panel)Master.FindControl("PanelUserModuleHeader");
                //TextBox _txtStaffRef = (TextBox)_userModuleHeaderPanel.FindControl("txtStaffReference");//get staff reference textbox
                //TextBox _txtStaffName = (TextBox)_userModuleHeaderPanel.FindControl("txtEmployeeName");//get staff name textbox

                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                PROC_StaffPersonalInformationResult getStaff = db.PROC_StaffPersonalInformation(_staffID).FirstOrDefault();
                txtStaffReference.Text = getStaff.StaffRef;
                //txtStaffName.Text = getStaff.StaffName;
                txtLastName.Text = getStaff.LastName;
                txtFirstName.Text = getStaff.FirstName;
                txtMiddleName.Text = getStaff.MiddleName;
                //ddlDepartment.SelectedValue = getStaff.DepartmentID.ToString();
                //ddlJobTitle.SelectedValue = getStaff.JobTitleID.ToString();
                //ddlPostedAt.SelectedValue = getStaff.PostedAtID.ToString();
                //if (getStaff.ReportingToStaffID != null)
                //    ddlReportsTo.SelectedValue = getStaff.ReportingToStaffID.ToString();
                //txtJoinDate.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
                ////txtEmploymentTermsDateOfJoin.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
                //if (getStaff.ExitDate != null)
                //    txtExitDate.Text = _hrClass.ShortDateDayStart(getStaff.ExitDate.ToString());
                //else txtExitDate.Text = "";

                txtDOB.Text = _hrClass.ShortDateDayStart(getStaff.DateOfBirth.ToString());
                txtGender.Text = getStaff.Gender.ToString();
                txtMaritalStatus.Text = getStaff.MaritalStatus.ToString();
                txtMobileNumber.Text = getStaff.PhoneNumber;
                txtNationality.Text = getStaff.Nationality.ToString();
                txtPassportNumber.Text = getStaff.PassportNumber;
                if (getStaff.PassportExpiryDate != null)
                    txtPassportExpiryDate.Text = _hrClass.ShortDateDayStart(getStaff.PassportExpiryDate.ToString());
                else txtPassportExpiryDate.Text = "";
                txtNationalID.Text = getStaff.IDNumber.ToString();
                txtDiplomaticIDNumber.Text = getStaff.DiplomaticIDNumber;
                txtDrivingPermitNumber.Text = getStaff.DrivingPermitNumber;
                txtNSSFNumber.Text = getStaff.NSSFNumber;
                txtPinNumber.Text = getStaff.PinNumber;
                txtTINNumber.Text = getStaff.TINNumber;
                txtPermanentAddress.Text = getStaff.PermanentAddress;
                txtCurrentAddress.Text = getStaff.CurrentAddress;
                txtTelephoneContact.Text = getStaff.TelephoneContact;
                //txtNHIFNumber.Text = getStaff.NHIFNumber;
                //if (getStaff.AlternativePhoneNumber != null)
                //    txtAlternativeMobileNumber.Text = getStaff.AlternativePhoneNumber;
                txtEmailAddress.Text = getStaff.EmailAddress;
                if (Convert.ToString(getStaff.AlternativeEmailAddress) != "")
                    txtAlternativeEmailAddress.Text = getStaff.AlternativeEmailAddress;
                else txtAlternativeEmailAddress.Text = "";
                txtPostalAddress.Text = getStaff.PostalAddress;
                txtLanguages.Text = getStaff.Languages;
                //if (getStaff.Salary != null)
                //    txtSalary.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaff.Salary);
                //txtEmergencyContactPerson.Text = getStaff.EmergencyContactPerson;
                //txtEmergencyContactPersonPhoneNumber.Text = getStaff.EmergencyContactPhoneNo;
                txtNextOfKinName.Text = getStaff.NextOfKinName;
                txtNextOfKinPhoneNumber.Text = getStaff.NextOfKinPhone;
                txtNextOfKinRelationship.Text = getStaff.NextOfKinRelationship;
                txtNextOfKinAddress.Text = getStaff.NextOfKinAddress;
                txtNextOfKin2Name.Text = getStaff.NextOfKin2Name;
                txtNextOfKin2PhoneNumber.Text = getStaff.NextOfKin2Phone;
                txtNextOfKin2Relationship.Text = getStaff.NextOfKin2Relationship;
                txtNextOfKin2Address.Text = getStaff.NextOfKin2Address;
                txtDisabilityDetails.Text = getStaff.DisabilityDetails;
                txtRefereeDetails1.Text = getStaff.Referee1Details;
                txtRefereeDetails2.Text = getStaff.Referee2Details;
                txtRefereeDetails3.Text = getStaff.Referee3Details;
                txtRemarks.Text = getStaff.Remarks;
                _hrClass.DisableEntryControls(PanelStafPersonalInformation);
                _hrClass.DisableEntryControls(PanelStaffAdditionalFields);
                return;
            }
            catch { }
        }
        //load staff additional details
        private void LoadStaffAdditionalDetails()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                if (db.PROC_StaffMasterAdditional(_staffID).Any())
                {
                    PROC_StaffMasterAdditionalResult getStaffAdditionalFields = db.PROC_StaffMasterAdditional(_staffID).FirstOrDefault();
                    if (txtHRDescription01.Visible == true)
                        txtHRDescription01.Text = Convert.ToString(getStaffAdditionalFields.HRDescription01);
                    if (txtHRDescription02.Visible == true)
                        txtHRDescription02.Text = Convert.ToString(getStaffAdditionalFields.HRDescription02);
                    if (txtHRDescription03.Visible == true)
                        txtHRDescription03.Text = Convert.ToString(getStaffAdditionalFields.HRDescription03);
                    if (txtHRDescription04.Visible == true)
                        txtHRDescription04.Text = Convert.ToString(getStaffAdditionalFields.HRDescription04);
                    if (txtHRDescription05.Visible == true)
                        txtHRDescription05.Text = Convert.ToString(getStaffAdditionalFields.HRDescription05);
                    if (txtHRDescription06.Visible == true)
                        txtHRDescription06.Text = Convert.ToString(getStaffAdditionalFields.HRDescription06);
                    if (txtHRDescription07.Visible == true)
                        txtHRDescription07.Text = Convert.ToString(getStaffAdditionalFields.HRDescription07);
                    if (txtHRDescription08.Visible == true)
                        txtHRDescription08.Text = Convert.ToString(getStaffAdditionalFields.HRDescription08);
                    if (txtHRDescription09.Visible == true)
                        txtHRDescription09.Text = Convert.ToString(getStaffAdditionalFields.HRDescription09);
                    if (txtHRDescription10.Visible == true)
                        txtHRDescription10.Text = Convert.ToString(getStaffAdditionalFields.HRDescription10);
                }
            }
            catch { }
        }
    }
}