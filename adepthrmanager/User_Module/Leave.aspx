﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Leave.aspx.cs" Inherits="AdeptHRManager.User_Module.Leave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaves" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeaves" runat="server">
                <asp:TabContainer ID="TabContainerLeaves" CssClass="ajax-tab" runat="server" ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelLeaveApplication" runat="server" CssClass="ajax-tab" HeaderText="Leave Application">
                        <HeaderTemplate>
                            Leave Application
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:Panel ID="PanelStaffLeaveApplication" runat="server">
                                    <fieldset>
                                        <legend>Leave Application Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Leave Type:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLeaveType" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged"
                                                        AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtLeaveSerialNumber" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbSingleOrMultipleDays" Visible="false" runat="server" Text="Single / Multiple Day[s]"></asp:Label>
                                                    <asp:Label ID="lbSingleOrMultipleDaysError" Visible="false" CssClass="errorMessage"
                                                        runat="server" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblSingleOrMultipleDays" OnSelectedIndexChanged="rblSingleOrMultipleDays_SelectedIndexChanged"
                                                        AutoPostBack="true" Visible="false" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem>Single Day</asp:ListItem>
                                                        <asp:ListItem>Multiple Days</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbDayOrHourMode" Visible="false" runat="server" Text="Day / Hour Mode?"></asp:Label>
                                                    <asp:Label ID="lbDayOrHourModeError" Visible="false" CssClass="errorMessage" runat="server"
                                                        Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblDayOrHourMode" OnSelectedIndexChanged="rblDayOrHourMode_SelectedIndexChanged"
                                                        AutoPostBack="true" Visible="false" RepeatDirection="Horizontal" runat="server">
                                                        <asp:ListItem>Full Day</asp:ListItem>
                                                        <asp:ListItem>Half a Day</asp:ListItem>
                                                        <asp:ListItem>Hour Based</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave From Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td style="vertical-align: bottom;">
                                                    <asp:TextBox ID="txtLeaveFromDate" OnTextChanged="txtLeaveFromDate_TextChanged" AutoPostBack="true"
                                                        Width="270px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLeaveFromDate" ToolTip="Pick leave starts from which date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLeaveFromDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtLeaveFromDate" PopupButtonID="ImageButtonLeaveFromDate"
                                                        PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:TextBox ID="txtEffectiveFromDate" Width="160px" Visible="false" runat="server"></asp:TextBox>
                                                    <td>
                                                        <asp:RadioButtonList ID="rblLeaveFromSession" OnSelectedIndexChanged="rblLeaveFromSession_SelectedIndexChanged"
                                                            AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                            <asp:ListItem Value="M">Morning</asp:ListItem>
                                                            <asp:ListItem Value="A">Afternoon</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Leave To Date:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLeaveToDate" OnTextChanged="txtLeaveToDate_TextChanged" AutoPostBack="true"
                                                        Width="270px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLeaveToDate" ToolTip="Pick leave to which date" CssClass="date-image"
                                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLeaveToDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtLeaveToDate" PopupButtonID="ImageButtonLeaveToDate"
                                                        PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:TextBox ID="txtEffectiveTillDate" Width="270px" Visible="false" runat="server"></asp:TextBox>
                                                    <td>
                                                        <asp:RadioButtonList ID="rblLeaveToSession" OnSelectedIndexChanged="rblLeaveToSession_SelectedIndexChanged"
                                                            AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                                                            <asp:ListItem Value="M">Morning</asp:ListItem>
                                                            <asp:ListItem Value="A">Afternoon</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbLeaveDays" runat="server" Text="Leave Days:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLeaveDays" Width="150px" Enabled="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbLeaveHours" Visible="false" runat="server" Text="Leave Hours:"></asp:Label>
                                                    <asp:Label ID="lbLeaveHoursError" Visible="false" CssClass="errorMessage" runat="server"
                                                        Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLeaveHours" Visible="false" Width="150px" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Remarks / Description:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                                    <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldLeaveID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldIsLeaveApplicableInHours" Value="No" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSubmit" OnClick="lnkBtnSubmit_Click" ToolTip="Submit leave application"
                                            runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Submit Leave Application</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateLeave" OnClick="lnkBtnUpdateLeave_Click" CausesValidation="false"
                                            ToolTip="Update leave application" Visible="false" runat="server">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update Leave Application</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClear" OnClick="lnkBtnClear_Click" CausesValidation="false"
                                            ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Reset</asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLeaveStatus" runat="server" CssClass="ajax-tab" HeaderText="Leave Status">
                        <HeaderTemplate>
                            Leave Status
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvLeaveStatus" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                                    CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No leave status details available.">
                                    <Columns>
                                        <asp:BoundField DataField="SNo" ItemStyle-Width="4px" />
                                        <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" ItemStyle-Width="150px" />
                                        <asp:BoundField DataField="ApprovedEarned" HeaderText="Approved Earned" ItemStyle-Width="115px"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="ApprovedConsumed" HeaderText="Approved Consumed" ItemStyle-Width="135px"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="UnApprovedConsumed" HeaderText="UnApproved Consumed" ItemStyle-Width="152px"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Bal" HeaderText="Balance" ItemStyle-Width="40px" HeaderStyle-HorizontalAlign="Right"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="CarryFwd" HeaderText="Carry Fwd" ItemStyle-Width="75px"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <%--<asp:BoundField DataField="UnApprovedEarned" HeaderText="UnApproved Earned" ItemStyle-Width="135px"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />--%>
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLeaveDueForApproval" runat="server" CssClass="ajax-tab"
                        HeaderText="Leaves Due For Approval">
                        <HeaderTemplate>
                            Leaves Due For Approval
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvLeavesDueForApproval" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                                    OnRowCommand="gvLeavesDueForApproval_RowCommand" EmptyDataText="No leave due for approval records available.">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vFromDate" HeaderText="From" />
                                        <asp:BoundField DataField="leave_vToDate" HeaderText="Until" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" ItemStyle-Width="4px" HeaderText="Days" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason / Remarks" />
                                        <asp:ButtonField HeaderText="Edit" CommandName="EditLeaveApplication" ItemStyle-Width="4px"
                                            Text="Edit" />
                                        <asp:ButtonField HeaderText="Delete" CommandName="DeleteLeaveApplication" ItemStyle-Width="4px"
                                            Text="Delete" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelApprovedLeaves" runat="server" CssClass="ajax-tab" HeaderText="Approved Leaves">
                        <HeaderTemplate>
                            Approved Leaves
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvApprovedLeavesHistory" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                                    OnPageIndexChanging="gvApprovedLeavesHistory_PageIndexChanging" EmptyDataText="No approved leave records.">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vAppliedBetween" HeaderText="Applied Between" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" ItemStyle-Width="4px" HeaderText="Days" />
                                        <asp:BoundField DataField="ApprovedBy" HeaderText="Approved By" />
                                        <asp:BoundField DataField="leave_VDateCreated" HeaderText="Date Created" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelRejectedLeaves" runat="server" CssClass="ajax-tab" HeaderText="Rejected Leaves">
                        <HeaderTemplate>
                            Rejected Leaves
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvRejectedLeaveHistory" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                                    EmptyDataText="No rejected leave records available." OnPageIndexChanging="gvRejectedLeaveHistory_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vAppliedBetween" HeaderText="Applied Between" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" ItemStyle-Width="4px" HeaderText="Days" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason / Remarks" />
                                        <asp:BoundField DataField="leave_VDateCreated" HeaderText="Date Created" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Application Reason" />
                                        <asp:BoundField DataField="RejectedByStaffName" HeaderText="Cancelled By" />
                                        <asp:BoundField DataField="leave_vDateRejected" HeaderText="Date Cancelled" />
                                        <asp:BoundField DataField="RejectionReasons" HeaderText="Cancellation Reason" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelLeavesToApprove" runat="server" CssClass="ajax-tab" HeaderText="Leaves To Approve">
                        <HeaderTemplate>
                            Leaves To Approve
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvLeavesToApprove" runat="server" AllowPaging="True" Width="100%"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                                    OnRowCommand="gvLeavesToApprove_RowCommand" EmptyDataText="No leave records available for approval.">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("leave_iLeaveID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff Name" />
                                        <asp:BoundField DataField="leavetype_vName" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="leave_vAppliedBetween" HeaderText="Applied Between" />
                                        <asp:BoundField DataField="leave_fNoDaysAlloc" ItemStyle-Width="4px" HeaderText="Days" />
                                        <asp:BoundField DataField="leave_vRemarks" HeaderText="Reason / Remarks" />
                                        <asp:BoundField DataField="leave_VDateCreated" HeaderText="Date Created" />
                                        <asp:TemplateField ItemStyle-Width="4px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButtonApproveLeaveApplication" runat="server" CommandName="ApproveLeave">Approve</asp:LinkButton></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="4px" ItemStyle-Font-Size="10px">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlGridViewLeavesToApproveMore" OnSelectedIndexChanged="ddlGridViewLeavesToApproveMore_SelectedIndexChanged"
                                                    runat="server" Width="100px" CssClass="dropDownMore" AppendDataBoundItems="true"
                                                    AutoPostBack="true" DataValueField='<%# Eval("leave_iLeaveID") %>'>
                                                    <asp:ListItem Text="More Action...."></asp:ListItem>
                                                    <asp:ListItem Text="Disapprove"></asp:ListItem>
                                                    <asp:ListItem Text="Edit"></asp:ListItem>
                                                    <asp:ListItem Text="Keep On Hold"></asp:ListItem>
                                                    <asp:ListItem Text="View Leave Status"></asp:ListItem>
                                                    <%--<asp:ListItem Text="Attached Document"></asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelDeleteLeaveApplication" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragDeleteLeaveApplication" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageDeleteLeaveApplicationtHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Erase.png" />
                                        <asp:Label ID="lbDeleteLeaveApplicationHeader" runat="server" Text="Delete Leave Application"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseDeleteLeaveApplication" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Delete Details</legend>
                            <table>
                                <tr>
                                    <td valign="top">
                                        Reason:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDeleteLeaveApplicationReasons" Width="450px" TextMode="MultiLine"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbUnitsError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbUnitsInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldDeleteLeaveApplicationPopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnDeleteLeaveApplication" OnClick="lnkBtnDeleteLeaveApplication_Click"
                                ToolTip="Delete leave appplication" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Delete Leave Application</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderDeleteLeaveApplication" RepositionMode="None"
                    X="200" Y="10" TargetControlID="HiddenFieldDeleteLeaveApplicationPopup" PopupControlID="PanelDeleteLeaveApplication"
                    CancelControlID="ImageButtonCloseDeleteLeaveApplication" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderDeleteLeaveApplication" TargetControlID="PanelDeleteLeaveApplication"
                    DragHandleID="PanelDragDeleteLeaveApplication" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelCancelLeaveApplication" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragCancelLeaveApplication" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageCancelLeaveApplicationHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Delete.png" />
                                        <asp:Label ID="lbCancelLeaveApplicationHeader" runat="server" Text="CANCEL LEAVE APPLICATION"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseCancelLeaveApplication" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Cancellation Details</legend>
                            <table>
                                <tr>
                                    <td valign="top">
                                        Reason:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLeaveCancellationReason" Width="450px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image8" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label3" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label4" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label5" CssClass="info-message" runat="server" Text=""></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldCancelLeaveApplicationPopup" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnCancelLeaveApplication" OnClick="lnkBtnCancelLeaveApplication_Click"
                                ToolTip="Cancel leave appplication" runat="server">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Cancel Leave Application</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderCancelLeaveApplication" RepositionMode="None"
                    X="200" Y="10" TargetControlID="HiddenFieldCancelLeaveApplicationPopup" PopupControlID="PanelCancelLeaveApplication"
                    CancelControlID="ImageButtonCloseCancelLeaveApplication" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderCancelLeaveApplication" TargetControlID="PanelCancelLeaveApplication"
                    DragHandleID="PanelDragCancelLeaveApplication" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmLeaveApproval" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmCancelLeaveApplication" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
