﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;

namespace AdeptHRManager.User_Module
{
    public partial class Leave : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerMailWebService _hrMailWebService = new HRManagerMailWebService();
        static string todaysDate = string.Empty;
        static Guid _loggedStaffID = Guid.Empty;
        static short _escalationMasterID = 0;
        string _employeeUnderStaffID = null;
        DateTimeFormatInfo dateInfo = new DateTimeFormatInfo();
        static string companyInitials = ConfigurationManager.AppSettings.Get("CompanyInitials").ToString();//get comapany's intials
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetLeaveTypes(ddlLeaveType, "Leave Type");
                try
                {
                    //get the logged in staff id
                    _loggedStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());

                    //get the employee under staff master id(passed id on the url)
                    try
                    {
                        _employeeUnderStaffID = Request.QueryString["eunderid"].ToString();
                    }
                    catch { }
                    //check if employee under id is null and load logged in employee leave details
                    if (_employeeUnderStaffID == null)
                    {
                        LoadEmployeeLeaveDetails(_loggedStaffID);
                    }
                    //else//load employee under leave details
                    //{
                    //    LoadEmployeeLeaveDetails(Convert.ToInt32(_employeeUnderStaffID));
                    //    HiddenFieldEmployeeUnderStaffMasterID.Value = _employeeUnderStaffID;
                    //    //disable controls on the leave application page that the manager is no supposed to see when viweing employee under leave details
                    //    DisableControlsNotToBeSeen();
                    //}
                    txtLeaveFromDate.Attributes.Add("readonly", "readonly");//make leave from date texbox readony 
                    txtLeaveToDate.Attributes.Add("readonly", "readonly");//
                    txtLeaveSerialNumber.Attributes.Add("readonly", "readonly");
                    txtLeaveDays.Attributes.Add("readonly", "readonly");
                    //get todays date to short formart
                    todaysDate = _hrClass.ShortDateDayStart(DateTime.Now.ToString());
                    //GetEmployeeOnOrDueForLeave(Convert.ToDateTime(todaysDate));//getemployees on or due for leave(by use of current date)

                    try
                    {
                        //get escalation transaction master id
                        // _escalationMasterID = Convert.ToInt16(_essClasses.ReturnEscalationMasterID("Leave"));//for leave application
                        // _escalationMasterID = Convert.ToInt16(db.LeaveApprovalProcesses.Select(p => p).FirstOrDefault().leaveapprovalproc_siLeaveApprovalProcessID);//for leave application
                        _escalationMasterID = _hrClass.ReturnEscalationMasterID();//get the default escalation masater
                    }
                    catch { }
                }
                catch { }
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            //add confirm approve leave application event
            ucConfirmLeaveApproval.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesApproveLeave_Click);
            //add confirm cancel leave application event
            ucConfirmCancelLeaveApplication.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnConfirmCancelLeaveApplication_Click);
        }
        //method for loading employee leave Details
        private void LoadEmployeeLeaveDetails(Guid staffMasterID)
        {
            GetLeaveStatus(staffMasterID);//load leave status
            GetLeavesDueForApproval(staffMasterID);//load leaves due for approval
            GetApprovedLeavesHistory(staffMasterID);//load approved leave history
            GetRejectedLeavesHistory(staffMasterID);//load rejected leave history
            GetLeavesToApproveFromEscalationTransactions(staffMasterID);//load leaves to approve from the escalation transactons table that are not yet approved
        }
        //clear leave application details
        private void ClearLeaveApplicationControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelStaffLeaveApplication);
            HiddenFieldLeaveID.Value = "";
            lnkBtnSubmit.Visible = true;
            lnkBtnUpdateLeave.Visible = false;

            lbSingleOrMultipleDays.Visible = false;
            lbSingleOrMultipleDaysError.Visible = false;
            rblSingleOrMultipleDays.Visible = false;

            lbDayOrHourMode.Visible = false;
            lbDayOrHourModeError.Visible = false;
            rblDayOrHourMode.Visible = false;

            lbLeaveDays.Visible = true;
            txtLeaveDays.Visible = true;

            lbLeaveHours.Visible = false;
            lbLeaveHoursError.Visible = false;
            txtLeaveHours.Visible = false;
        }
        //
        //validate leave application controls
        private string ValidateLeaveApplicationControls()
        {
            if (ddlLeaveType.SelectedIndex == 0)
            {
                return "Select Leave type!";
            }
            else if (txtLeaveFromDate.Text.Trim() == "")
            {
                ImageButtonLeaveFromDate.Focus();
                return "Select from which date is the leave supposed to start on!";
            }
            //else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Date < DateTime.Now.Date)//check if from date is less than current date
            //{
            //    _errror = "Leave application from date cannot be before current date!";
            //    btnLeaveFromDate.Focus();
            //    return _errror;
            //}
            else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Year > DateTime.Now.Year)
            {
                ImageButtonLeaveFromDate.Focus();
                return "Date from year selected is not the current year. Leave can only be applied from the current year!";
            }
            else if (rblLeaveFromSession.SelectedIndex == -1)
            {

                rblLeaveFromSession.Focus();
                return "Please select the session when the leave is supposed to start from, i.e either in the morning or afternoon!";
            }

            else if (txtLeaveToDate.Text.Trim() == "")
            {
                ImageButtonLeaveToDate.Focus();
                return "Select date when the leave bieng applied for is expected to end!";
            }
            else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) > Convert.ToDateTime(txtLeaveToDate.Text.Trim()))
            {
                ImageButtonLeaveFromDate.Focus();
                return "'Leave From Date' cannot be after 'Leave To Date'!";
            }
            else if (rblLeaveToSession.SelectedIndex == -1)
            {
                rblLeaveToSession.Focus();
                return "Please select the session when the leave being applied for is expected to end, I.e either in the morning or afternoon!";
            }
            else if (txtLeaveFromDate.Text.Trim() == txtLeaveToDate.Text.Trim() && rblLeaveFromSession.SelectedIndex == 1 && rblLeaveToSession.SelectedIndex == 0)//check if the leave days are on the same day and the session from session selected is afternoon and to session is not afternon
            {
                rblLeaveToSession.Focus();
                return "Leave application failed! Leave application starting and ending on the same date cannot start in the afternoon and end in the morning. Select afternoon session.";
            }
            else if (rblSingleOrMultipleDays.Visible == true)
            {
                if (rblDayOrHourMode.SelectedIndex == 2 && txtLeaveHours.Text.Trim() == "")
                {
                    txtLeaveHours.Focus();
                    return "Enter " + ddlLeaveType.SelectedItem.ToString().ToLower() + " hours!";
                }
                else if (rblDayOrHourMode.SelectedIndex == 2 && _hrClass.isNumberValid(txtLeaveHours.Text.Trim()) == false)
                {
                    txtLeaveHours.Focus();
                    return "Invalid leave hours entered. Enter a valid numeric value!";
                }
                else if (rblDayOrHourMode.SelectedIndex == 2 && Convert.ToDouble(txtLeaveHours.Text.Trim()) > 8)
                {
                    txtLeaveHours.Focus();
                    return "Invalid leave hours entered. Leave hours cannot be greater than 8 hours!";
                }
                else if (rblDayOrHourMode.SelectedIndex == 2 && Convert.ToDouble(txtLeaveHours.Text.Trim()) <= 0)
                {
                    txtLeaveHours.Focus();
                    return "Invalid leave hours entered. Leave hours cannot be less than or equal to zero!";
                }
                else return "";
            }
            else
                return "";
        }
        //submit the leave application details
        private void SubmitLeaveApplication()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                int _staffPostedAtID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);

                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                    return;
                }


                else if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false) && ddlLeaveType.SelectedItem.ToString().ToLower().Contains("site") == false)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Leave application failed. Another leave application with the details entered is already made!", this, PanelLeaves);
                    ddlLeaveType.Focus();
                    return;
                }
                else if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_cEffFromSess == Convert.ToChar(rblLeaveFromSession.SelectedValue) && p.leave_cEffTillSess == Convert.ToChar(rblLeaveToSession.SelectedValue) && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Leave application failed. Another leave application with the details entered is already made!", this, PanelLeaves);
                    ddlLeaveType.Focus();
                    return;
                }
                //check if there are more than three unapproved for the current year
                else if (db.Leaves.Count(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_bLeaveStatus == false && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false && DateTime.Now.Year == Convert.ToDateTime(txtLeaveFromDate.Text).Year) >= 3)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Leave application failed. There are more than three unapproved leave applications.<br>Please contact your manager to first approve the unapproved leaves.!", this, PanelLeaves);
                    return;
                }
                else
                {
                    //check if the employee had earlier applied for the leave type selected
                    string currentYear = DateTime.Now.Year.ToString();

                    int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == (short)leaveTypeID);
                    string leaveTypeName = ddlLeaveType.SelectedItem.ToString().ToLower();

                    //check if the leave type has anapplication time limit and if it has been surpassed
                    if (getLeaveType.leavetype_bHasApplicationLimitTime == true && CheckApplicationHrsDifference((short)_staffPostedAtID, (short)leaveTypeID) == false)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Sorry, your leave application cannot be made.<br>" + getLeaveType.leavetype_vName + " application should be made at least " + getLeaveType.leavetype_fLimitHours.ToString() + " hours prior to the target date!", this, PanelLeaves);
                        return;
                    }
                    else
                    {
                        //check if leaveType is reaplicable
                        bool isLeaveReaplicable = (bool)getLeaveType.leavetype_bIsReapplicablePerYear;
                        //check if the leave had been earlier applied for and consumed
                        if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_siLeaveTypeID == leaveTypeID && p.leave_cTrType == 'U' && p.leave_bLeaveStatus == true) && isLeaveReaplicable == false)
                        {
                            //get the last leave id for the leave application that was made for the leave
                            int lastLeaveID = Convert.ToInt32(db.Leaves.Where(p => p.leave_bLeaveStatus == true && p.leave_cTrType == 'U' && p.leave_uiStaffID == _staffID).Max(p => p.leave_iLeaveID));
                            //get the date when the leave was applied
                            DateTime getLastLeaveDate = Convert.ToDateTime(db.Leaves.Single(p => p.leave_iLeaveID == lastLeaveID).leave_dtEffFrom);
                            //get the year applied for
                            string yearApplied = getLastLeaveDate.Year.ToString();
                            if (currentYear == yearApplied)//check if curent year is the same year the leave was appled for and stop leave application process
                            {
                                _hrClass.LoadHRManagerMessageBox(1, "Leave application cannot be processed because the type of leave selected cannot be re-applicable in the same year i.e. the leave can only be applied once in a year!", this, PanelLeaves);
                                return;
                            }
                        }
                        //check if more than two leave days applied on the same date
                        if (txtLeaveFromDate.Text.Trim() == txtLeaveToDate.Text.Trim())
                        {
                            //check if there another leave application made
                            int _countLeaveMade = db.Leaves.Count(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false);
                            if (_countLeaveMade > 0)
                            {
                                //sum leave days for that particular date
                                double _sumLeaveDaysAlreadyMade = (double)db.Leaves.Where(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false).Sum(p => p.leave_fNoDaysAlloc);
                                //get a total of the currently made and the one being made
                                double _sumLeaveDaysToBeMade = Convert.ToDouble(txtLeaveDays.Text.Trim()) + _sumLeaveDaysAlreadyMade;
                                //check if the summation of both will surpass one day
                                if (_sumLeaveDaysToBeMade > 1)
                                {
                                    _hrClass.LoadHRManagerMessageBox(1, "Leave application cannot be processed because the leave application is being made on the same day and the total number of days will be " + _sumLeaveDaysToBeMade + " days which will be more than 1 day!", this, PanelLeaves);
                                    return;
                                }
                            }
                        }

                        //count leave days
                        double leaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));//get leave days

                        DateTime effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), _staffPostedAtID);//get effective date when the leave begins
                        DateTime effectiveTilldate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), _staffPostedAtID);//get effective date when the leave ends
                        //generate leave serial number
                        string leaveSerialNumber = GenerateLeaveSerialNumber();
                        //create new leave application
                        HRManagerClasses.Leave newLeave = new HRManagerClasses.Leave();
                        newLeave.leave_uiStaffID = _staffID;
                        newLeave.leave_siLeaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);
                        newLeave.leave_cTrType = 'U';//U for used
                        newLeave.leave_dtFrom = Convert.ToDateTime(txtLeaveFromDate.Text.Trim());
                        newLeave.leave_dtEffFrom = effectiveFromDate;
                        //check if the  from session selected is morning or afternoon
                        if (rblLeaveFromSession.SelectedIndex == 0)//morning
                        {
                            newLeave.leave_cEffFromSess = 'M';
                        }
                        else newLeave.leave_cEffFromSess = 'A';
                        newLeave.leave_dtTill = Convert.ToDateTime(txtLeaveToDate.Text.Trim());
                        newLeave.leave_dtEffTill = effectiveTilldate;
                        //check if till session selected is morning or afternoon
                        if (rblLeaveToSession.SelectedIndex == 0)//morning
                        {
                            newLeave.leave_cEffTillSess = 'M';
                        }
                        else newLeave.leave_cEffTillSess = 'A';
                        DateTime _dateCreated = DateTime.Now;
                        //newLeave.leaves_iAuthBy
                        //check if the leave application being made does not require approval so that it can be approved automatically
                        if (getLeaveType.leavetype_bRequiresApproval == false)
                        {
                            newLeave.leave_bLeaveStatus = true;//set the as approved
                            newLeave.leave_uiAuthByStaffID = _staffID;//set the leave aplicant as the leave approver
                            newLeave.leave_dtDateApproved = _dateCreated;//set the date approved as the same as the one created
                        }
                        else
                            newLeave.leave_bLeaveStatus = false;//dont approved the leave
                        newLeave.leave_fNoDaysAlloc = leaveDays;
                        newLeave.leave_vRemarks = txtRemarks.Text.Trim();
                        newLeave.leave_dtDateCreated = _dateCreated;
                        newLeave.leave_bIsCancelled = false;
                        newLeave.leave_vSerialNumber = leaveSerialNumber;
                        newLeave.leave_bIsRemoved = false;

                        db.Leaves.InsertOnSubmit(newLeave);
                        db.SubmitChanges();


                        HiddenFieldLeaveID.Value = newLeave.leave_iLeaveID.ToString();
                        //check if the leave application requires any approval
                        if (getLeaveType.leavetype_bRequiresApproval == true)
                        {
                            //insert leave application details into the escalation transaction table for process to begin
                            InsertLeaveApplicationIntoEscalationProcess(_staffID, leaveSerialNumber);

                            _hrClass.LoadHRManagerMessageBox(2, "Leave application has been submited and is awaiting your manager's approval.", this, PanelLeaves);
                            txtLeaveSerialNumber.Text = leaveSerialNumber;
                            // GetLeaveStatus(staffMasterID);//refresh leave status for the logged in employee
                            GetLeavesDueForApproval(_staffID);//refresh leaves due for approval for the currently logged in employee

                            //create a maill sending transaction 
                            //case 1 for sending a leave application made mail
                            _hrMailWebService.CreateMailSendingTransaction(1, newLeave.leave_iLeaveID, _staffID, null);

                        }
                        else ///if it does not require any approval
                        ///
                        {
                            string _leaveTypeName = getLeaveType.leavetype_vName;
                            _hrClass.LoadHRManagerMessageBox(2, _leaveTypeName + " application has been submitted and a notification sent to your manager.", this, PanelLeaves);
                            txtLeaveSerialNumber.Text = leaveSerialNumber;
                            GetLeaveStatus(_staffID);//refresh leave status for the logged in employee
                            GetApprovedLeavesHistory(_staffID);//refresh approved leave applications records

                            //notify the employee's manager
                            //notify the leave appliant
                            //notify hr
                            //create a maill sending transaction 
                            //case7 for sending a leave application made notification to the leave applicant, the employee's manager and the HR to be kept informed
                            _hrMailWebService.CreateMailSendingTransaction(7, newLeave.leave_iLeaveID, _staffID, null);
                        }
                        return;
                    }
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while trying to submit the leave application. Please try again!", this, PanelLeaves);
                return;
            }
        }
        //update the leave application details
        private void UpdateLeaveApplication(int LeaveID)
        {
            try
            {
                string _error = ValidateLeaveApplicationControls();//check for errors
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                int _staffPostedAtID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);

                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                    return;
                }
                else if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_iLeaveID != LeaveID && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update Failed ! Leave details entered already exits!", this, PanelLeaves);
                    return;
                }
                else
                {
                    //check if the employee had earlier applied for the leave type selected
                    string currentYear = DateTime.Now.Year.ToString();
                    int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == (short)leaveTypeID);
                    string leaveTypeName = ddlLeaveType.SelectedItem.ToString().ToLower();
                    //check if the leave type has anapplication time limit and if it has been surpassed
                    if (getLeaveType.leavetype_bHasApplicationLimitTime == true && CheckApplicationHrsDifference((short)_staffPostedAtID, (short)leaveTypeID) == false)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Sorry, your leave application update cannot be made.<br>" + getLeaveType.leavetype_vName + " application should be made at least " + getLeaveType.leavetype_fLimitHours.ToString() + " hours prior to the target date !", this, PanelLeaves);
                        return;
                    }
                    else
                    {
                        //check if leaveType is reaplicable
                        bool isLeaveReaplicable = (bool)getLeaveType.leavetype_bIsReapplicablePerYear;
                        //check if the leave had been eralier applied for and consumed
                        if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_siLeaveTypeID == leaveTypeID && p.leave_cTrType == 'U' && p.leave_bLeaveStatus == true && p.leave_iLeaveID != LeaveID) && isLeaveReaplicable == false)
                        {
                            //get the last leave id for theleave application thet was made for the leave
                            int lastLeaveID = Convert.ToInt32(db.Leaves.Where(p => p.leave_bLeaveStatus == true && p.leave_cTrType == 'U' && p.leave_uiStaffID == _staffID).Max(p => p.leave_iLeaveID));
                            //get the date when the leave was applied
                            DateTime getLastLeaveDate = Convert.ToDateTime(db.Leaves.Single(p => p.leave_iLeaveID == lastLeaveID).leave_dtEffFrom);
                            //get the year applied for
                            string yearApplied = getLastLeaveDate.Year.ToString();
                            if (currentYear == yearApplied)
                            {
                                _hrClass.LoadHRManagerMessageBox(1, "Leave application cannot be processed because the type of leave selected cannot be re-applicable in the same year!", this, PanelLeaves);
                                return;
                            }
                        }
                        double leaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));//get leave days

                        DateTime effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), _staffPostedAtID);//get effective date when the leave begins
                        DateTime effectiveTilldate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), _staffPostedAtID);//get effective date when the leave ends

                        //check if there are changes that have been made before the update is done
                        bool _hasLeaveDetailsChanged = HasLeaveDetailsBeenChanged();
                        if (_hasLeaveDetailsChanged == false)//no changes made
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "No changes made!", this, PanelLeaves);
                            return;
                        }
                        else//update the changes
                        {
                            //update the leave detail
                            HRManagerClasses.Leave updateLeave = db.Leaves.Single(p => p.leave_iLeaveID == LeaveID);
                            updateLeave.leave_siLeaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);

                            updateLeave.leave_dtFrom = Convert.ToDateTime(txtLeaveFromDate.Text.Trim());
                            updateLeave.leave_dtEffFrom = effectiveFromDate;
                            //check if the  from session selected is morning or afternoon
                            updateLeave.leave_cEffFromSess = Convert.ToChar(rblLeaveFromSession.SelectedValue);
                            updateLeave.leave_dtTill = Convert.ToDateTime(txtLeaveToDate.Text.Trim());
                            updateLeave.leave_dtEffTill = effectiveTilldate;
                            updateLeave.leave_cEffTillSess = Convert.ToChar(rblLeaveToSession.SelectedValue);
                            //updateLeave.leaves_iAuthBy
                            updateLeave.leave_fNoDaysAlloc = leaveDays;
                            updateLeave.leave_vRemarks = txtRemarks.Text.Trim();
                            db.SubmitChanges();
                            //check if it a sick leave or day off and there is and attachment , then save the attachment document
                            //bool _hasDocumentAttachment = false;//to be used to check if there is an attachment for the leave application
                            //if (lbLeaveAttachmentsImportedFile.Text.Trim() != "")
                            //{
                            //    //check if there is a leave document for the leave application
                            //    if (db.LeaveDocs.Any(p => p.leavedocs_iLeavesID == LeaveID))//update the existing document
                            //    {
                            //        UpdateLeaveAttachmentDocument(LeaveID);
                            //    }
                            //    else //save a new document
                            //    {
                            //        //save the attachment
                            //        SaveLeaveAttachmentDocument(LeaveID);
                            //    }
                            //    _hasDocumentAttachment = true;
                            //}
                            _hrClass.LoadHRManagerMessageBox(2, "Leave application details have been updated.", this, PanelLeaves);
                            //refresh the leaves after updating
                            //GetLeaveStatus(staffMasterID);//refresh leave status for the currently logged in employee
                            GetLeavesDueForApproval(_staffID);//refresh leaves due for approval for the currently logged in employee

                            //create a mail sending transaction
                            //case 5 for sending a mail alert to the approving manager when leave details have been edited by a leave applicant
                            _hrMailWebService.CreateMailSendingTransaction(5, LeaveID, _staffID, null);

                            //create a session to be used if another update is to be done
                            DataTable dt = GetLeaveDetailsBeforeEditDataTable(LeaveID, ddlLeaveType.SelectedItem.ToString(), txtLeaveFromDate.Text, rblLeaveFromSession.SelectedItem.ToString(), txtLeaveToDate.Text, rblLeaveToSession.SelectedItem.ToString(), txtLeaveDays.Text, txtRemarks.Text);
                            Session["GetLeaveDetailsBeforeEdit"] = dt;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }

        //check if  hours difference in leave application between current date and from date is less than 36
        private bool CheckApplicationHrsDifference(short postingLocationID, short _leaveTypeID)
        {
            DateTime currentDate = DateTime.Now;
            DateTime fromdate = Convert.ToDateTime(txtLeaveFromDate.Text.Trim());

            //check the session being applied from
            if (rblLeaveFromSession.SelectedIndex == 0)//check if the session begins in the afternoon
                fromdate = fromdate.AddHours(8.5);  //add 8 and half hours because the session starts in the morning(8.30)
            else if (rblLeaveFromSession.SelectedIndex == 1) //check if the session begins in the afternoon
                fromdate = fromdate = fromdate.AddHours(14);//add 14 hours if session starts in the afternoon(2:00)

            double _hrsDifference = 0;
            double _leaveTypeLimitHours = Convert.ToDouble(db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == _leaveTypeID).leavetype_fLimitHours);
            _hrsDifference = fromdate.Subtract(currentDate).TotalHours;
            if (_hrsDifference < _leaveTypeLimitHours)//check if the hours difference is less than the limit hours for the leave type
                //return _hrsDifference;
                return false;
            else
            {
                //get dates between the current date and fromdate
                DateTime _checkDate = currentDate;
                while (_checkDate < fromdate)
                {
                    //check if the date between falls on sunday
                    if (_checkDate.DayOfWeek == DayOfWeek.Sunday)
                        _hrsDifference += -24;//reduce 24 hrs from the difference
                    //check if the date is a holiday and itz a full day
                    else if (db.Holidays.Any(p => p.holiday_dtDate.Value.Date == _checkDate.Date && p.holiday_fDuration == 1 && p.holiday_siApplicableToBranchID == postingLocationID))
                        _hrsDifference += -24;//reduce 24 hours from the difference
                    //check if the date is a holiday is and itz half a day
                    else if (db.Holidays.Any(p => p.holiday_dtDate.Value.Date == _checkDate.Date && p.holiday_fDuration == 0.5 && p.holiday_siApplicableToBranchID == postingLocationID))
                        _hrsDifference += -12;//reduce 12 hours form then difference

                    _checkDate = _checkDate.AddDays(1);
                }
                //check if the hours difference is less than (36) hrs or whatever has been set againist the leave
                if (_hrsDifference < _leaveTypeLimitHours)
                    return false;
                else return true;
            }
        }
        //method for inserting a new record into the escalation process table
        private void InsertLeaveApplicationIntoEscalationProcess(Guid _staffID, string leaveSerialNumber)
        {
            try
            {
                //get escalation master details fro approval level one
                LeaveApprovalProcess getEscalationMaster = db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == _escalationMasterID);
                Guid approvalStaffMasterID = (Guid)getEscalationMaster.leaveapprovalproc_uiApprovalLevel1;
                //check if itz immediate manager
                if (approvalStaffMasterID == Guid.Empty)
                {
                    //get leave applicant's manager id
                    approvalStaffMasterID = (Guid)db.StaffMasters.Single(p => p.StaffID == _staffID).ReportingToStaffID;
                }
                //escalation level 1 time
                int escalationTime = Convert.ToInt32(getEscalationMaster.leaveapprovalproc_iEscalationTimeAprvLv1);

                //get  next approval staff master id
                Guid nextApprovalMasterID = Guid.Empty;//empty for next manager
                if (getEscalationMaster.leaveapprovalproc_uiApprovalLevel2 != null)//check if the next level approval is not null
                {

                    nextApprovalMasterID = (Guid)getEscalationMaster.leaveapprovalproc_uiApprovalLevel2;
                }
                else nextApprovalMasterID = Guid.Empty;
                //check if the next approval level(2) is the next manager(0)
                if (nextApprovalMasterID == Guid.Empty)
                {
                    //get the leave applicant imendiate manager id
                    Guid leaveApplicantImediateManagerID = (Guid)db.StaffMasters.Single(p => p.StaffID == _staffID).ReportingToStaffID;
                    ////check if leaveApplicantImediate manager is the head
                    //if (leaveApplicantImediateManagerID == 1) //1 is is for the head
                    //{ nextApprovalMasterID = null; }
                    //else
                    try
                    {
                        nextApprovalMasterID = (Guid)db.StaffMasters.Single(p => p.StaffID == leaveApplicantImediateManagerID).ReportingToStaffID;
                    }
                    catch { }
                }

                //initiate the escalation process
                _hrClass.InsertRecordIntoEscalationTransactionsTable(_escalationMasterID, leaveSerialNumber, 1, approvalStaffMasterID, escalationTime, false, "Escalated", nextApprovalMasterID, Guid.Empty);
            }
            catch { }
        }

        //count leave days by substarticng the holidays defined
        private double CountLeaveDays(DateTime fromDate, DateTime toDate)
        {
            //get logged in employee postig location id
            int postingLocationID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);//get employee's posting location
            //get logged in employee department name
            //string employeeDepartment = db.StaffPerDepartments_Views.Single(p => p.StaffID == _loggedStaffID).DepartmentName.ToString();
            double leaveDays = 0;

            //get leave type id
            int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);
            //get the leave type selected
            LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeID);
            string leaveTypeName = getLeaveType.leavetype_vName.Trim();
            //get  leave type days(working or calendar)
            string _leaveDaysType = getLeaveType.leavetype_vDaysType.Trim();
            //check if the leave application is applicable on hourly basis and is for a single day, and appliying the leave based on hours
            if ((bool)getLeaveType.leavetype_bIsApplicableInHours == true && (rblSingleOrMultipleDays.SelectedIndex == 0 && rblDayOrHourMode.SelectedIndex == 2))
            {
                //get hours entered
                double _leaveHours = Convert.ToDouble(txtLeaveHours.Text.ToString());
                leaveDays = _leaveHours / 8;
                txtRemarks.Text = _leaveHours.ToString() + "<br>" + leaveDays.ToString();
            }
            else
            {
                //check if leave type is continous or not
                if ((bool)getLeaveType.leavetype_bIsContinuous == true)// if it is continous, get total leave days for the leave type
                {

                    leaveDays = (double)getLeaveType.leavetype_fMaxDays;
                    //leaveDays=leaveDays;// 1;reduce leave days by one
                    toDate = fromDate.AddDays(leaveDays - 1);//add the leave days to the fromDat to get the final leave date ingle date to get the end date
                    txtLeaveToDate.Text = _hrClass.ShortDateDayStart(toDate.ToString());
                    DateTime effectiveToDate = GetNextWorkingDate(toDate, postingLocationID);//get effective till working date
                    txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(effectiveToDate.ToString()));
                    txtLeaveDays.Text = leaveDays.ToString();
                    ImageButtonLeaveToDate.Enabled = false;

                }
                else if ((bool)getLeaveType.leavetype_bIsContinuous == false)//else claculate the leavedays by checking if they are claendar or working days
                {

                    ImageButtonLeaveToDate.Enabled = true;
                    //get all calendar day if leave type days are based on calendar days
                    if (_leaveDaysType == "Calendar")
                    {
                        TimeSpan _timeSpan = new TimeSpan();//for calculating date difference
                        _timeSpan = toDate.Subtract(fromDate);
                        leaveDays = _timeSpan.Days + 1;//plus add one day
                    }
                    //get workings days if leave type days calculation is based on working days
                    else if (_leaveDaysType == "Working")
                    {
                        //get total leave days by calling the PROC_CountLeaveDays procedure result) to get the total leave days
                        PROC_CountLeaveDaysResult getLeaveDays = db.PROC_CountLeaveDays(postingLocationID, fromDate, toDate).Single();
                        leaveDays = Convert.ToDouble(getLeaveDays.no_of_leaves_days);
                        DateTime dateIterator = fromDate;//start from the from date and loop till to date plus one day
                        while (dateIterator < toDate.AddDays(1))
                        {
                            //check if day of week is on a sunday, then reduce the total days by 1
                            if (dateIterator.DayOfWeek == DayOfWeek.Sunday)

                                leaveDays--;//reduce the leave days by one

                            dateIterator = dateIterator.AddDays(1);//add a day to the iterator after checking
                        }
                    }
                    //check if from date is the same with till date
                    if (fromDate == toDate)
                    {
                        //check if the dates are on a saturday or sunday or any other holiday
                        if (db.Holidays.Any(p => p.holiday_dtDate == fromDate))
                        {
                            //check if from session selected is morning  and session till is afternoon
                            if (rblLeaveFromSession.SelectedIndex == 0 && rblLeaveToSession.SelectedIndex == 1)
                            {
                                return leaveDays;//return the leave days
                            }
                            else
                            {
                                return 0.5;//return half day
                            }
                        }
                        //check if from date is exactly the same with to date and if sessions are the same, or the from session begins in the afternoon, then leave days is 0.5 days
                        else if (rblLeaveFromSession.SelectedIndex == rblLeaveToSession.SelectedIndex || rblLeaveFromSession.SelectedIndex == 1)
                        {
                            return leaveDays = 0.5;
                        }
                        //check if dates are the same but sessions are different, then its a full day
                        else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)
                        {
                            return 1;
                        }
                    }
                    //check if number of leave days =1,and if enddate is saturday or sunday
                    else if (leaveDays == 1)
                    {
                        //check if fron session selected is morning or afternoon
                        if (rblLeaveFromSession.SelectedIndex == 0)
                        {
                            return leaveDays;
                        }
                        else if (rblLeaveFromSession.SelectedIndex == 1)
                        {
                            return 0.5;
                        }
                    }

                    else if (leaveDays >= 1)
                    {
                        //   check if theleave days is greater than one and end date falls n working saturday(or  sunday- one day)

                        if ((toDate.DayOfWeek == DayOfWeek.Saturday || toDate.AddDays(-1).DayOfWeek == DayOfWeek.Saturday) && (db.Holidays.Any(p => p.holiday_dtDate == toDate || p.holiday_dtDate == toDate.AddDays(-1))))
                        {
                            if (rblLeaveFromSession.SelectedIndex == 0)//return the leave days as they are if the session selected is in the morning
                            {
                                return leaveDays;
                            }
                            else if (rblLeaveFromSession.SelectedIndex == 1)//reduce by 0.5 if the session is in the afternoon
                            {
                                return leaveDays - 0.5;
                            }
                        }
                        //check if leave days are greater than one and from date falls on a working saturday

                        else if (fromDate.DayOfWeek == DayOfWeek.Saturday)
                        {
                            if (rblLeaveToSession.SelectedIndex == 0)//if till session is in the morning reduce by 0.5 days
                            {
                                return leaveDays - 0.5;
                            }
                            else if (rblLeaveToSession.SelectedIndex == 1)//if till session is in the evening, return leave days as they are
                            {
                                return leaveDays;
                            }
                        }
                        //checking same sessions selected is morning and either from date session is on a morning or afternoon, then minus a half day from the total days counted
                        else if (rblLeaveFromSession.SelectedIndex == rblLeaveToSession.SelectedIndex && (rblLeaveFromSession.SelectedIndex == 0 || rblLeaveFromSession.SelectedIndex == 1))
                        {

                            return leaveDays - 0.5;
                        }
                        //check if different sesions are selected,and from leave session is in the morning and leave endsession is on an afternoon, then leave days are the total days counted 
                        else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex && rblLeaveFromSession.SelectedIndex == 0 && rblLeaveToSession.SelectedIndex == 1)
                        {
                            return leaveDays;
                        }
                        //check if different sesions are selected,and from leave session is on an afternoon and endsession is in the morning, the substract a day from the total counted days 
                        else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex && rblLeaveFromSession.SelectedIndex == 1 && rblLeaveToSession.SelectedIndex == 0)
                        {
                            return leaveDays - 1;
                        }
                    }
                }
            }
            //check if we are on a leave appliable by hours
            if (rblDayOrHourMode.Visible == true)
                ImageButtonLeaveToDate.Visible = false;
            else if (rblDayOrHourMode.Visible == false)
                ImageButtonLeaveToDate.Visible = true;
            return leaveDays;
        }
        //get next woking day if the leave from applied date or till applied date fall on a holiday or weekend 
        //depending on the employee's posting location
        private DateTime GetNextWorkingDate(DateTime _date, int _postingLocationID)
        {
            DateTime workingDay = _date;
            if (_date.DayOfWeek == DayOfWeek.Sunday)//check if the date selected is on a sunday
            {
                workingDay = workingDay.AddDays(1);//add a day to sunday
                return workingDay;
            }
            //check if date selected is a saturday
            else if (_date.DayOfWeek == DayOfWeek.Saturday && db.Holidays.Any(p => p.holiday_dtDate == _date && p.holiday_siApplicableToBranchID == _postingLocationID))
            {
                //get type of leave day (either 1 or 0.5)
                double _leaveDay = Convert.ToDouble(db.Holidays.Single(p => p.holiday_dtDate == _date && p.holiday_siApplicableToBranchID == _postingLocationID).holiday_fDuration);
                if (_leaveDay == 1)
                {
                    workingDay = workingDay.AddDays(2);//add two days non-working saturday
                    return workingDay;
                }
                else if (_leaveDay == 0.5)//return the saturday selected is a working saturday
                {
                    return workingDay;
                }
            }
            //check if the date selected is holiday and does not fall on a sunday or saturday
            else if ((_date.DayOfWeek != DayOfWeek.Saturday || _date.DayOfWeek != DayOfWeek.Sunday) && db.Holidays.Any(p => p.holiday_dtDate == _date && (p.holiday_siApplicableToBranchID == 1 || p.holiday_siApplicableToBranchID == _postingLocationID)))
            {
                workingDay = workingDay.AddDays(1);//add a day to the day selected
                //after adding a day, check if the day added fall on a wekend
                // GetNextWorkingDate(workingDay);//recurse or repeat the process inorder to get the corect working day
                if (workingDay.DayOfWeek == DayOfWeek.Sunday)//check if added day is on a sunday
                {
                    workingDay = workingDay.AddDays(1);//add a day to sunday
                    return workingDay;
                }
                //check if date added is a saturday
                else if (workingDay.DayOfWeek == DayOfWeek.Saturday && db.Holidays.Any(p => p.holiday_dtDate == workingDay && p.holiday_siApplicableToBranchID == _postingLocationID))
                {
                    //get type of leave day (either 1 or 0.5)
                    double _leaveDay = Convert.ToDouble(db.Holidays.Single(p => p.holiday_dtDate == workingDay && p.holiday_siApplicableToBranchID == _postingLocationID).holiday_fDuration);
                    if (_leaveDay == 1)
                    {
                        workingDay = workingDay.AddDays(2);//add two days non-working saturday
                        return workingDay;
                    }
                    else if (_leaveDay == 0.5)//return the saturday selected is a working saturday
                    {
                        return workingDay;
                    }
                }
                return workingDay;
            }
            return workingDay;
        }
        //generate leave serial number
        private string GenerateLeaveSerialNumber()
        {
            string leaveSerialNumber = "", firstPart = "LV", secondPart = "", lastLeaveSerialNumber = companyInitials + "-" + firstPart + "-00001";
            int lastUsedLeaveID = 0, _2ndPart = 0;//initialize with zero
            try
            {
                //get the last id for leaves where is is of used type (U)
                lastUsedLeaveID = Convert.ToInt32(db.Leaves.Where(p => p.leave_cTrType == 'U' && p.leave_vSerialNumber != null).Max(p => p.leave_iLeaveID));
            }
            catch { }
            if (lastUsedLeaveID == 0)//check if there is any used leave id record
            {
                leaveSerialNumber = lastLeaveSerialNumber;
            }
            else
            {
                lastLeaveSerialNumber = db.Leaves.Single(p => p.leave_iLeaveID == lastUsedLeaveID).leave_vSerialNumber;
                //lastLeaveSerialNumber = lastLeaveSerialNumber.Substring(3);//remove the first 3 letters
                lastLeaveSerialNumber = lastLeaveSerialNumber.Replace(companyInitials, "").Replace(firstPart, "").Replace("-", "");//remove company initials, firts part and (-) to get the numeric part of the number
                _2ndPart = Convert.ToInt32(lastLeaveSerialNumber) + 1;//by incrementing with 1 
                secondPart = _2ndPart.ToString();
                //generate leave serial number by checking the length of the second part
                leaveSerialNumber = companyInitials + '-' + firstPart + "-";//get the start defination of the leave number
                if (secondPart.Length == 1)
                {
                    leaveSerialNumber += "0000" + secondPart;
                }
                else if (secondPart.Length == 2)
                {
                    leaveSerialNumber += "000" + secondPart;
                }
                else if (secondPart.Length == 3)
                {
                    leaveSerialNumber += "00" + secondPart;
                }
                else if (secondPart.Length == 4)
                {
                    leaveSerialNumber += "0" + secondPart;
                }
                else//length exceeds 4
                {
                    leaveSerialNumber += secondPart;
                }
            }
            return leaveSerialNumber;
        }

        //submit leave application
        protected void lnkBtnSubmit_Click(object sender, EventArgs e)
        {
            SubmitLeaveApplication(); return;
        }
        //update leave application
        protected void lnkBtnUpdateLeave_Click(object sender, EventArgs e)
        {
            UpdateLeaveApplication(Convert.ToInt32(HiddenFieldLeaveID.Value)); return;
        }
        //clear leave application controls
        protected void lnkBtnClear_Click(object sender, EventArgs e)
        {
            ClearLeaveApplicationControls(); return;
        }
        // display effective from date when leave from date is selected
        protected void txtLeaveFromDate_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    if (ddlLeaveType.SelectedIndex == 0)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Select leave type!", this, PanelLeaves);
                        txtLeaveFromDate.Text = "";
                        return;
                    }
                    else
                    {
                        //get leave applicant posting location
                        int leaveApplicantPostingLocationID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);
                        int leaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                        //get leave type
                        LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeId);
                        //check if leave is continous
                        if ((bool)getLeaveType.leavetype_bIsContinuous == true)
                        {
                            int leaveDays = (int)getLeaveType.leavetype_fMaxDays;//get maximamun leave days for the leave type
                            //get from when leave is due to start
                            DateTime fromDate = Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), toDate, effectiveToDate;
                            //get till date 
                            toDate = fromDate.AddDays(leaveDays - 1);
                            txtLeaveToDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(toDate.ToString()));
                            effectiveToDate = GetNextWorkingDate(toDate, leaveApplicantPostingLocationID);//get effective till working date
                            txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(effectiveToDate.ToString()));
                            txtLeaveDays.Text = leaveDays.ToString();
                            ImageButtonLeaveToDate.Enabled = false;
                            rblLeaveFromSession.SelectedIndex = 0;
                            rblLeaveToSession.SelectedIndex = 1;
                            rblLeaveFromSession.Enabled = false;
                            rblLeaveToSession.Enabled = false;
                        }
                        else
                        {
                            ImageButtonLeaveToDate.Enabled = true;
                            rblLeaveFromSession.Enabled = true;
                            rblLeaveToSession.Enabled = true;
                        }
                        DateTime _effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text), leaveApplicantPostingLocationID);
                        txtEffectiveFromDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(_effectiveFromDate.ToString()));
                        //check if leave application being made is applicable on hourly basis
                        if (rblDayOrHourMode.Visible == true)
                        {
                            txtLeaveToDate.Text = txtLeaveFromDate.Text;
                            ImageButtonLeaveToDate.Enabled = false;
                            if (rblDayOrHourMode.SelectedIndex == 0)//check if it is fill day
                            {
                                rblLeaveFromSession.Enabled = false;
                                rblLeaveToSession.Enabled = false;
                            }
                            else if (rblDayOrHourMode.SelectedIndex == 1 || rblDayOrHourMode.SelectedIndex == 2)//check if it is half a day or hourly based
                            {
                                if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex == -1)//check if from session is selected but to session is not
                                    rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;
                                else if (rblLeaveFromSession.SelectedIndex == -1 && rblLeaveToSession.SelectedIndex != -1)//check if from session is not selected but to session is selected
                                    rblLeaveFromSession.SelectedIndex = rblLeaveToSession.SelectedIndex;
                                else if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex != -1 && rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)//check if both from and to session are selected but do not match
                                    rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;//set to session to the value of from sesstion selected
                            }
                        }
                        else
                        {
                            ImageButtonLeaveToDate.Enabled = true;
                        }
                        //check if all the fields are entered, and calaculate leave days
                        string _error = ValidateLeaveApplicationControls();//check for errors
                        if (_error == "")//check if all the fields are entered
                        {
                            //count leave days
                            double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                            txtLeaveDays.Text = countLeaveDays.ToString();
                            return;
                        }
                    }
                    return;
                }
                catch (Exception ex)
                {
                    _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                    return;
                }
            }
        }

        // display effective to date when leave till date is selected
        protected void txtLeaveToDate_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    if (txtLeaveFromDate.Text.Trim() == "")
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Select the date when the leave you are are applying for is expected to begin by clickng on the calendar button adjacent to field!", this, PanelLeaves);
                        ImageButtonLeaveFromDate.Focus();
                        txtLeaveFromDate.Text = "";
                        return;
                    }
                    else
                    {
                        //get leave applicant posting location id
                        int leaveApplicantPostingLocationID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);
                        //get the next working date
                        DateTime _effectiveTillDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), leaveApplicantPostingLocationID);
                        txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(_effectiveTillDate.ToString()));

                        //check if leave application being made is applicable on hourly basis
                        if (rblDayOrHourMode.Visible == true)
                        {
                            txtLeaveToDate.Text = txtLeaveFromDate.Text;
                            ImageButtonLeaveToDate.Enabled = false;
                            if (rblDayOrHourMode.SelectedIndex == 0)//check if it is fill day
                            {
                                rblLeaveFromSession.Enabled = false;
                                rblLeaveToSession.Enabled = false;
                            }
                            else if (rblDayOrHourMode.SelectedIndex == 1 || rblDayOrHourMode.SelectedIndex == 2)//check if it is half a day or hourly based
                            {
                                if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex == -1)//check if from session is selected but to session is not
                                    rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;
                                else if (rblLeaveFromSession.SelectedIndex == -1 && rblLeaveToSession.SelectedIndex != -1)//check if from session is not selected but to session is selected
                                    rblLeaveFromSession.SelectedIndex = rblLeaveToSession.SelectedIndex;
                                else if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex != -1 && rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)//check if both from and to session are selected but do not match
                                    rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;//set to session to the value of from sesstion selected
                            }
                        }
                        else
                        {
                            ImageButtonLeaveToDate.Enabled = true;
                        }
                        //check if all the fields are entered, and calaculate leave days
                        string _error = ValidateLeaveApplicationControls();//check for errors
                        if (_error == "")//check if all the fields are entered
                        {  //count lrave days
                            double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                            txtLeaveDays.Text = countLeaveDays.ToString();
                            return;
                        }
                        return;
                    }
                }
                catch (Exception ex)
                {
                    _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                    return;
                }
            }
        }
        //count the leave days when the index is changed
        protected void rblLeaveToSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            //check if the leave being applied for is applicable on hourly basis
            if (rblDayOrHourMode.Visible == true)
            {
                ImageButtonLeaveToDate.Enabled = false;
                if (txtLeaveFromDate.Text.Trim() != "" && txtLeaveToDate.Text.Trim() == "")
                    txtLeaveToDate.Text = txtLeaveFromDate.Text;
                else if (txtLeaveToDate.Text.Trim() != "" && txtLeaveFromDate.Text.Trim() == "")
                    txtLeaveFromDate.Text = txtLeaveToDate.Text;

                if (rblDayOrHourMode.SelectedIndex == 0)//check if it is fill day
                {
                    rblLeaveFromSession.Enabled = false;
                    rblLeaveToSession.Enabled = false;
                }
                else if (rblDayOrHourMode.SelectedIndex == 1 || rblDayOrHourMode.SelectedIndex == 2)//check if it is half a day or hourly based
                {
                    if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex == -1)//check if from session is selected but to session is not
                        rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;
                    else if (rblLeaveFromSession.SelectedIndex == -1 && rblLeaveToSession.SelectedIndex != -1)//check if from session is not selected but to session is selected
                        rblLeaveFromSession.SelectedIndex = rblLeaveToSession.SelectedIndex;
                    else if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex != -1 && rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)//check if both from and to session are selected but do not match
                        rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;//set to session to the value of from sesstion selected
                }
            }
            else
            {
                ImageButtonLeaveToDate.Enabled = true; ;
            }
            string _error = ValidateLeaveApplicationControls();//check for errors
            if (_error != "")
            {
                _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                rblLeaveToSession.SelectedIndex = -1;
                return;
            }
            else
            {
                //count leave days
                double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                txtLeaveDays.Text = countLeaveDays.ToString();
                return;
            }

        }
        //count leave days when from radio button is selected
        protected void rblLeaveFromSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            //check if the leave being applied for is applicable on hourly basis
            if (rblDayOrHourMode.Visible == true)
            {
                ImageButtonLeaveToDate.Enabled = false;
                if (txtLeaveFromDate.Text.Trim() != "" && txtLeaveToDate.Text.Trim() == "")
                    txtLeaveToDate.Text = txtLeaveFromDate.Text;
                else if (txtLeaveToDate.Text.Trim() != "" && txtLeaveFromDate.Text.Trim() == "")
                    txtLeaveFromDate.Text = txtLeaveToDate.Text;

                if (rblDayOrHourMode.SelectedIndex == 0)//check if it is fill day
                {
                    rblLeaveFromSession.Enabled = false;
                    rblLeaveToSession.Enabled = false;
                }
                else if (rblDayOrHourMode.SelectedIndex == 1 || rblDayOrHourMode.SelectedIndex == 2)//check if it is half a day or hourly based
                {
                    if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex == -1)//check if from session is selected but to session is not
                        rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;
                    else if (rblLeaveFromSession.SelectedIndex == -1 && rblLeaveToSession.SelectedIndex != -1)//check if from session is not selected but to session is selected
                        rblLeaveFromSession.SelectedIndex = rblLeaveToSession.SelectedIndex;
                    else if (rblLeaveFromSession.SelectedIndex != -1 && rblLeaveToSession.SelectedIndex != -1 && rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)//check if both from and to session are selected but do not match
                        rblLeaveToSession.SelectedIndex = rblLeaveFromSession.SelectedIndex;//set to session to the value of from sesstion selected
                }
            }
            else
            {
                ImageButtonLeaveToDate.Enabled = true; ;
            }
            string _error = ValidateLeaveApplicationControls();//check for errors
            if (_error == "")//check if all the fields are entered
            {
                //count leave days
                double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                txtLeaveDays.Text = countLeaveDays.ToString();
                return;
            }
        }
        //count leave days when from leave type is selected
        protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                rblSingleOrMultipleDays.SelectedIndex = -1;
                lbDayOrHourMode.Visible = false;
                lbDayOrHourModeError.Visible = false;
                rblDayOrHourMode.Visible = false;
                rblDayOrHourMode.SelectedIndex = -1;

                lbLeaveDays.Visible = true;
                txtLeaveDays.Visible = true;

                lbLeaveHours.Visible = false;
                lbLeaveHoursError.Visible = false;
                txtLeaveHours.Visible = false;
                txtLeaveHours.Text = "";
                if (ddlLeaveType.SelectedIndex == 0)
                {
                    //return;

                }
                else
                {

                    int leaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    //get leave type
                    LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeId);
                    //check if leave is continous
                    if ((bool)getLeaveType.leavetype_bIsContinuous == true)
                    {
                        ImageButtonLeaveToDate.Enabled = false;
                        rblLeaveFromSession.Enabled = false;
                        rblLeaveToSession.Enabled = false;
                    }
                    else
                    {
                        ImageButtonLeaveToDate.Enabled = true;
                        rblLeaveFromSession.Enabled = true;
                        rblLeaveToSession.Enabled = true;
                    }
                    //check if the leave is applicable on hourly basis
                    if ((bool)getLeaveType.leavetype_bIsApplicableInHours == true)
                    {
                        lbSingleOrMultipleDays.Visible = true;
                        lbSingleOrMultipleDaysError.Visible = true;
                        rblSingleOrMultipleDays.Visible = true;
                        HiddenFieldIsLeaveApplicableInHours.Value = "yes";
                    }
                    else
                    {
                        lbSingleOrMultipleDays.Visible = false;
                        lbSingleOrMultipleDaysError.Visible = false;
                        rblSingleOrMultipleDays.Visible = false;
                        HiddenFieldIsLeaveApplicableInHours.Value = "no";
                    }
                    string _error = ValidateLeaveApplicationControls();//check for errors
                    if (_error == "")//check if all the fields are entered
                    {
                        //count leave days
                        double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                        txtLeaveDays.Text = countLeaveDays.ToString();
                        return;
                    }
                }
            }
            catch { }
        }
        //single / multiple days  index is changed
        protected void rblSingleOrMultipleDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblSingleOrMultipleDays.SelectedIndex == 0)//check if it on a single day
                {
                    lbDayOrHourMode.Visible = true;
                    lbDayOrHourModeError.Visible = true;
                    rblDayOrHourMode.Visible = true;
                    ImageButtonLeaveFromDate.Enabled = true;
                    ImageButtonLeaveToDate.Enabled = false;
                }
                else//check if it is a multiple days application
                {
                    lbDayOrHourMode.Visible = false;
                    lbDayOrHourModeError.Visible = false;
                    rblDayOrHourMode.Visible = false;
                    ImageButtonLeaveToDate.Enabled = true;
                    ImageButtonLeaveFromDate.Enabled = true;
                }
                //check if all the fields are entered, and calaculate leave days
                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error == "")//check if all the fields are entered
                {
                    //count leave days
                    double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                    txtLeaveDays.Text = countLeaveDays.ToString();
                    return;
                }
                else txtLeaveDays.Text = "";
            }
            catch { }
        }
        //day/hour mode index is changed
        protected void rblDayOrHourMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (rblDayOrHourMode.SelectedIndex == 0)//check if it is full day
                {
                    lbLeaveDays.Visible = true;
                    txtLeaveDays.Visible = true;

                    lbLeaveHours.Visible = false;
                    lbLeaveHoursError.Visible = false;
                    txtLeaveHours.Visible = false;

                    rblLeaveFromSession.SelectedIndex = 0;
                    rblLeaveFromSession.Enabled = false;

                    rblLeaveToSession.SelectedIndex = 1;
                    rblLeaveToSession.Enabled = false;

                }
                else if (rblDayOrHourMode.SelectedIndex == 1)//check if it is half day application
                {
                    lbLeaveDays.Visible = true;
                    txtLeaveDays.Visible = true;

                    lbLeaveHours.Visible = false;
                    lbLeaveHoursError.Visible = false;
                    txtLeaveHours.Visible = false;

                    rblLeaveFromSession.SelectedIndex = -1;
                    rblLeaveFromSession.Enabled = true;

                    rblLeaveToSession.SelectedIndex = -1;
                    rblLeaveToSession.Enabled = true;
                }
                else//check if it is hourly based
                {
                    lbLeaveDays.Visible = false;
                    txtLeaveDays.Visible = false;

                    lbLeaveHours.Visible = true;
                    lbLeaveHoursError.Visible = true;
                    txtLeaveHours.Visible = true;

                    rblLeaveFromSession.SelectedIndex = -1;
                    rblLeaveFromSession.Enabled = true;

                    rblLeaveToSession.SelectedIndex = -1;
                    rblLeaveToSession.Enabled = true;
                }

                txtLeaveToDate.Text = txtLeaveFromDate.Text;
                //check if all the fields are entered, and calaculate leave days
                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error == "")//check if all the fields are entered
                {
                    //count leave days
                    double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                    txtLeaveDays.Text = countLeaveDays.ToString();
                    return;
                }
                else txtLeaveDays.Text = "";
                ImageButtonLeaveToDate.Enabled = false;
            }
            catch { }
        }
        //get leave due for approval
        private void GetLeavesDueForApproval(Guid staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_LeaveDueForApprovalPerStaff(staffID);
                gvLeavesDueForApproval.DataSourceID = null;
                gvLeavesDueForApproval.DataSource = _display;
                gvLeavesDueForApproval.DataBind();
                return;
            }
            catch { }
        }

        //load staff leave details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvLeavesDueForApproval_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditLeaveApplication") == 0 || e.CommandName.CompareTo("DeleteLeaveApplication") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFLeaveID = (HiddenField)gvLeavesDueForApproval.Rows[ID].FindControl("HiddenField1");
                    int _leaveID = Convert.ToInt32(_HFLeaveID.Value);
                    if (e.CommandName.CompareTo("EditLeaveApplication") == 0)//check if we are editing the leave
                    {
                        //load edit leave aqpplication details
                        LoadLeaveDetailsForEdit(_leaveID);
                        return;
                    }
                    else if (e.CommandName.CompareTo("DeleteLeaveApplication") == 0)//check if are deleting the leave
                    {
                        Session["DeleteLeaveID"] = _HFLeaveID.Value;
                        LoadLeaveApplicationDetailsToBeDeleted(_leaveID);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }
        //load edit leave application controls
        private void LoadLeaveDetailsForEdit(int LeaveID)
        {
            PROC_GetLeaveApplicationForEditResult getLeave = db.PROC_GetLeaveApplicationForEdit(LeaveID).FirstOrDefault();
            HiddenFieldLeaveID.Value = LeaveID.ToString();
            ddlLeaveType.SelectedValue = getLeave.leave_siLeaveTypeID.ToString();
            txtLeaveFromDate.Text = getLeave.leave_vFromDate;
            txtLeaveToDate.Text = getLeave.leave_vToDate;
            rblLeaveFromSession.SelectedValue = getLeave.leave_cEffFromSess.ToString();
            rblLeaveToSession.SelectedValue = getLeave.leave_cEffTillSess.ToString();
            txtLeaveDays.Text = getLeave.leave_fNoDaysAlloc.ToString();
            txtRemarks.Text = getLeave.leave_vRemarks;

            //create a session for leave application before edit
            DataTable dt = GetLeaveDetailsBeforeEditDataTable(LeaveID, ddlLeaveType.SelectedItem.ToString(), txtLeaveFromDate.Text, rblLeaveFromSession.SelectedItem.ToString(), txtLeaveToDate.Text, rblLeaveToSession.SelectedItem.ToString(), txtLeaveDays.Text, txtRemarks.Text);
            Session["GetLeaveDetailsBeforeEdit"] = dt;

            TabContainerLeaves.ActiveTabIndex = 0;
            lnkBtnSubmit.Visible = false;
            lnkBtnUpdateLeave.Visible = true;
            lnkBtnUpdateLeave.Enabled = true;
        }
        //get leave details before edit
        private DataTable GetLeaveDetailsBeforeEditDataTable(int _leaveID, string _leaveType, string _fromDate, string _fromSession, string _tillDate, string _tillSession, string _leaveDays, string _reasons)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("Leave Type", typeof(string)));
            dt.Columns.Add(new DataColumn("From Date", typeof(string)));
            dt.Columns.Add(new DataColumn("From Session", typeof(string)));
            dt.Columns.Add(new DataColumn("Till Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Till Session", typeof(string)));
            dt.Columns.Add(new DataColumn("Days", typeof(string)));
            dt.Columns.Add(new DataColumn("Reasons", typeof(string)));

            DataRow dr;
            dr = dt.NewRow();
            dr[0] = _leaveID;
            dr[1] = _leaveType;
            dr[2] = _fromDate;
            dr[3] = _fromSession;
            dr[4] = _tillDate;
            dr[5] = _tillSession;
            dr[6] = _leaveDays;
            dr[7] = _reasons;
            dt.Rows.Add(dr);
            return dt;
        }
        //method for checking if leave details have been changed when updating a leave
        //compares the details in the session and thethe ones in the formeditor
        private bool HasLeaveDetailsBeenChanged()
        {
            DataTable dt = (DataTable)Session["GetLeaveDetailsBeforeEdit"];//get the seseion data for the leave details before an edit
            DataRow dr = dt.Rows[0];//get data row
            if (ddlLeaveType.SelectedItem.ToString().Trim() != dr[1].ToString())//check if the asset type has been changed
                return true;
            else if (Convert.ToDateTime(txtLeaveFromDate.Text) != Convert.ToDateTime(dr[2].ToString()))//check if from date has changed
                return true;
            else if (rblLeaveFromSession.SelectedItem.ToString() != dr[3].ToString())//check if from session has changed
                return true;
            else if (Convert.ToDateTime(txtLeaveToDate.Text) != Convert.ToDateTime(dr[4].ToString()))//check if till date has chencged
                return true;
            else if (rblLeaveToSession.SelectedItem.ToString() != dr[5].ToString())//check if session till has changed
                return true;
            else if (txtLeaveDays.Text.Trim() != dr[6].ToString())//chech if allocated days have changed
                return true;
            else if (txtRemarks.Text.Trim() != dr[7].ToString())//check if remarks have changed
                return true;

            else
                return false;
        }
        //loads leave application details to be deleted
        private void LoadLeaveApplicationDetailsToBeDeleted(int _leaveID)
        {
            PROC_GetLeaveApplicationForEditResult getLeave = db.PROC_GetLeaveApplicationForEdit(_leaveID).FirstOrDefault();
            //string employeeNames = getLeave.staffmst_vStaffName;
            string leaveType = getLeave.leavetype_vName;
            lbDeleteLeaveApplicationHeader.Text = ("Delete " + leaveType + " Application").ToUpper();
            txtDeleteLeaveApplicationReasons.Text = "";
            ModalPopupExtenderDeleteLeaveApplication.Show();
            return;
        }
        //method for deleting/removing a leave application(not completely)
        private void DeleteALeaveApplication()
        {
            ModalPopupExtenderDeleteLeaveApplication.Show();
            try
            {
                if (txtDeleteLeaveApplicationReasons.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter reasons why the leave application is being deleted!", this, PanelLeaves);
                    return;
                }
                else
                {
                    string removalReasons = txtDeleteLeaveApplicationReasons.Text.Trim();
                    //delete the leave application
                    int leaveID = Convert.ToInt32(Session["DeleteLeaveID"]);
                    HRManagerClasses.Leave deleteLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
                    deleteLeave.leave_bIsRemoved = true;
                    deleteLeave.leave_uiRemovedByStaffID = _loggedStaffID;//logged in employee
                    deleteLeave.leave_dtDateRemoved = DateTime.Now;
                    deleteLeave.leave_vRemovalReasons = removalReasons;
                    db.SubmitChanges();
                    ClearDeleteALeaveApplicationControls();
                    ////update leaves in the escalation transactions once the leave is deleted
                    //UpdateLeavesInEscalationTransactionTable(deleteLeave.leave_vSerialNumber, "Deleted", _loggedStaffID);

                    _hrClass.LoadHRManagerMessageBox(2, "Leave application has been deleted from leave applications!", this, PanelLeaves);
                    //get leaves
                    //GetLeaveStatus(_loggedStaffMasterID);//refresh leave status for the logged in employee
                    GetLeavesDueForApproval(_loggedStaffID);//refresh leaves due for approval for the logged in employee

                    //create a mail sending transaction record
                    //case 6 for sending an alert to the approving manager when the leave applicant deletes a leave application
                    _hrMailWebService.CreateMailSendingTransaction(6, leaveID, _loggedStaffID, null);

                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed." + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }
        //clear delete a leave application controls
        private void ClearDeleteALeaveApplicationControls()
        {
            Session["DeleteLeaveID"] = null;
            txtDeleteLeaveApplicationReasons.Text = "";
            lbDeleteLeaveApplicationHeader.Text = "DELETE LEAVE APPLICATION";
            ModalPopupExtenderDeleteLeaveApplication.Hide();
        }
        //confirm if we are deleting the leave application
        protected void lnkBtnDeleteLeaveApplication_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderDeleteLeaveApplication.Show();
            _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, "Are you sure you want to delete the leave application?");
            return;
        }
        //delete records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            if (TabContainerLeaves.ActiveTabIndex == 2)//check if we are on leave due for approval
                DeleteALeaveApplication();//delete leave application
            return;
        }
        //get leave status
        private void GetLeaveStatus(Guid _staffID)
        {
            try
            {
                //get header for earned upto
                string earnedUptoHeader = "Earned Un Approved";
                int currentMonth = DateTime.Now.Month, currentYear = DateTime.Now.Year;
                try
                {
                    string lastMonthName = dateInfo.MonthNames[currentMonth - 2];//get last month name from the index of the current month by going two values back
                    earnedUptoHeader = "Earned Upto " + lastMonthName.Substring(0, 3) + ". " + currentYear;
                }
                catch { }
                //get employee gender
                string employeeGender = db.StaffMasters.Single(p => p.StaffID == _staffID).Gender.ToString().TrimEnd();
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));//get leave types
                dt.Columns.Add(new DataColumn("ApprovedEarned", typeof(string)));
                dt.Columns.Add(new DataColumn("ApprovedConsumed", typeof(string)));
                dt.Columns.Add(new DataColumn("UnApprovedConsumed", typeof(string)));
                dt.Columns.Add(new DataColumn("Bal", typeof(string)));
                dt.Columns.Add(new DataColumn("CarryFwd", typeof(string)));
                //dt.Columns.Add(new DataColumn("UnApprovedEarned", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //get leave status depending on the gender of the employee and leave type is applicable to all
                foreach (LeaveType _leaveType in db.LeaveTypes.Where(p => p.leavetype_vApplicationTo == "All" || p.leavetype_vApplicationTo == employeeGender).OrderBy(p => p.leavetype_vName))
                {
                    //get leave status totals
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, _leaveType.leavetype_siLeaveTypeID).Single();
                    //get leave days balance
                    string _balanceDays = "N/A";
                    //check if the leave balance for the leave type is to be calculated
                    if (_leaveType.leavetype_bCalculateLeaveBalance == true)
                    {
                        _balanceDays = string.Format("{0:F2}", (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed)));
                    }
                    //check if leave days can be carried or not
                    string _carryFwd = "No";
                    if (_leaveType.leavetype_bIsCarriedFwd == true)
                    {
                        _carryFwd = "Yes";
                    }
                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = _leaveType.leavetype_vName;
                    dr[2] = string.Format("{0:F2}", _sumLeaveDays.approved_earned);
                    dr[3] = string.Format("{0:F2}", _sumLeaveDays.approved_consumed);
                    dr[4] = string.Format("{0:F2}", _sumLeaveDays.unapproved_consumed);
                    dr[5] = _balanceDays;
                    dr[6] = _carryFwd;
                    ////check if the leave type is annual leave an the the unapproved leaved days
                    //string unApprovedEarnedDays = "N/A";
                    //if (_leaveType.leavetype_vName.ToLower().Contains("annual"))//check if the leave type is annual
                    //{
                    //    try
                    //    {
                    //        //get unapproved annual leave days for the seleted employee
                    //        unApprovedEarnedDays = db.ViewSummedUnApprovedLeaveDays.Single(p => p.staffmst_iStaffMstID == staffMasterID).UnApprovedLeaveDays.ToString();
                    //    }
                    //    catch { }
                    //}
                    //dr[7] = unApprovedEarnedDays;

                    dt.Rows.Add(dr);
                    countRows++;
                }
                gvLeaveStatus.DataSource = dt;
                //gvLeaveStatus.Columns[7].HeaderText = earnedUptoHeader;
                gvLeaveStatus.DataBind();
            }
            catch { }

        }

        //get leaves awaiting for approval
        private void GetLeavesToApproveFromEscalationTransactions(Guid staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_LeavesInEscalationTransactionsForApproval(staffID);
                gvLeavesToApprove.DataSourceID = null;
                gvLeavesToApprove.DataSource = _display;
                gvLeavesToApprove.DataBind();
                return;
            }
            catch { }
        }
        //get the leave that is to be updated on gvLeavesToApprove or cancel a leave
        protected void gvLeavesToApprove_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("ApproveLeave") == 0)//check if we are approving a leave
                {
                    WebControl wc = e.CommandSource as WebControl;
                    GridViewRow row = wc.NamingContainer as GridViewRow;
                    int DataItemIndex = row.DataItemIndex;
                    HiddenField _HFLeaveID = (HiddenField)gvLeavesToApprove.Rows[DataItemIndex].FindControl("HiddenField1");
                    Session["ApproveLeaveID"] = _HFLeaveID.Value;
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmLeaveApproval, "Are you sure you want to approve the leave application?");
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //approve a leave application and use the esclation master to keep  employees in it to be informed the employeed
        private void ApproveLeaveApplication()
        {
            try
            {

                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());//get the employee logged to be the approving manager
                int _leaveID = Convert.ToInt32(Session["ApproveLeaveID"]);//get leave being approved
                //check if the leave has not yet been approved, to avoid multiple email 
                if ((db.Leaves.Single(p => p.leave_iLeaveID == _leaveID).leave_bLeaveStatus == false) || (db.PROC_LeavesInEscalationTransactionsForApproval(_staffID).Any(p => p.leave_iLeaveID == _leaveID)))
                {
                    HRManagerClasses.Leave approveLeave = db.Leaves.Single(p => p.leave_iLeaveID == _leaveID);
                    approveLeave.leave_uiAuthByStaffID = _staffID;//approving manager
                    approveLeave.leave_dtDateApproved = DateTime.Now;
                    approveLeave.leave_bLeaveStatus = true;
                    db.SubmitChanges();
                    //update leaves in the escalation transaction table where the serial number is for the leave being approved
                    UpdateLeavesInEscalationTransactionTable(approveLeave.leave_vSerialNumber, "Approved", _staffID);
                    _hrClass.LoadHRManagerMessageBox(2, "Leave application has been successfully approved.", this, PanelLeaves);

                    //refresh leaves to approve where the logged in employee is ana aproving level required
                    GetLeavesToApproveFromEscalationTransactions(_staffID);

                    //create a maill sending transaction 
                    //case 2 for sending a leave approval notification mail
                    _hrMailWebService.CreateMailSendingTransaction(2, _leaveID, _staffID, null);
                    return;
                }
                else
                {
                    _hrClass.LoadHRManagerMessageBox(2, "Leave application has been successfully approved.", this, PanelLeaves);
                    GetLeavesToApproveFromEscalationTransactions(_staffID);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //method for updating a leave in the escalation  transaction table once an update,a delete or a leave cancelation is made
        private void UpdateLeavesInEscalationTransactionTable(string leaveSerialNumber, string closeType, Guid closedByStaffID)
        {
            foreach (EscalationTransaction updateLeavesEscalated in db.EscalationTransactions.Where(p => p.escalationtransaction_vTransactionNumber == leaveSerialNumber))
            {
                updateLeavesEscalated.escalationtransaction_bIsClosed = true;
                updateLeavesEscalated.escalationtransaction_vCloseType = closeType;
                updateLeavesEscalated.escalationtransaction_uiClosedByStaffID = closedByStaffID;
                db.SubmitChanges();
            }
        }
        //approve leave
        protected void lnkBtnYesApproveLeave_Click(object sender, EventArgs e)
        {
            ApproveLeaveApplication();
        }

        //get approved leave history
        private void GetApprovedLeavesHistory(Guid staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_ApprovedLeavePerStaff(staffID);
                gvApprovedLeavesHistory.DataSourceID = null;
                gvApprovedLeavesHistory.DataSource = _display;
                gvApprovedLeavesHistory.DataBind();
                Session["gvApprovedLeavesHistoryData"] = _display;
                gvApprovedLeavesHistory.AllowPaging = true;
            }
            catch { }
        }
        //enable page index changing on gvApprovedLeavesHistory
        protected void gvApprovedLeavesHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvApprovedLeavesHistory.PageIndex = e.NewPageIndex;
            gvApprovedLeavesHistory.DataSource = (object)Session["gvApprovedLeavesHistoryData"];
            gvApprovedLeavesHistory.DataBind();
            return;
        }
        //method for performing a task depending on the dropdown value selected on the drop down in the leaves to approve grid view
        protected void ddlGridViewLeavesToApproveMore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlSelected = (DropDownList)sender;
                string _selectedValue = ddlSelected.SelectedValue;
                //get the row involved
                GridViewRow row = (GridViewRow)ddlSelected.Parent.Parent;
                int selectedRowID = row.RowIndex;
                //get leave id depending on the row index selected
                HiddenField _HFLeaveID = (HiddenField)gvLeavesToApprove.Rows[selectedRowID].FindControl("HiddenField1");
                int _applicantLeaveID = Convert.ToInt32(_HFLeaveID.Value);
                //select More... after getting the value 
                ddlSelected.SelectedIndex = 0;
                // LoadSymphonyMessageBox(1, 3, selectedRowID.ToString() + " " + _selectedValue + " " + _HFLeaveID.Value + " ");
                //check if the manager is editing the leave application
                if (_selectedValue == "Edit")
                {
                    HttpContext.Current.Session["EditLeaveCase"] = 2;//when a leave is being edited by a manager
                    LoadLeaveDetailsForEdit(_applicantLeaveID);
                    return;
                }
                else if (_selectedValue == "Disapprove")  //check if the leave is being cancelled(disapproved)
                {
                    Session["CancelLeaveID"] = _applicantLeaveID.ToString();
                    ModalPopupExtenderCancelLeaveApplication.Show();
                    txtLeaveCancellationReason.Text = "";
                    txtLeaveCancellationReason.Focus();
                    return;
                }
                //else if (_selectedValue == "Keep On Hold")//check if the leave is being kept on hold
                //{
                //    //LoadSymphonyMessageBox(1, 3, "This is still under development");
                //    Session["KeepLeaveOnHoldID"] = _applicantLeaveID.ToString();
                //    ModalPopupExtenderKeepLeaveOnHold.Show();
                //    return;
                //}
                //else if (_selectedValue == "Leave Status")  //check if itz viewing leave applicant's leave status
                //{
                //    Session["ApplicantsLeaveID"] = _applicantLeaveID.ToString();
                //    GetLeaveStatusforLeaveApplicants(_applicantLeaveID);
                //    return;
                //}

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured ! " + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }
        //confirm if we are cancelling the leave application
        protected void lnkBtnCancelLeaveApplication_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderCancelLeaveApplication.Show();
            _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmCancelLeaveApplication, "Are you sure you want to cancel the leave application?");
            return;
        }
        //method for canceling leave by the employee's manager(by the logged in approving manager)
        private void CancelLeaveApplication()
        {
            ModalPopupExtenderCancelLeaveApplication.Show();
            try
            {
                //get leave id to be cancelled
                int leaveID = Convert.ToInt32(Session["CancelLeaveID"]);
                if (txtLeaveCancellationReason.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter leave cancellation reason!", this, PanelLeaves);
                    txtLeaveCancellationReason.Focus();
                    return;
                }
                else
                {
                    HRManagerClasses.Leave cancelLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
                    Guid leaveApplicantID = (Guid)cancelLeave.leave_uiStaffID;//get staff master id for the person who had applied for the leave
                    //cancel leave
                    cancelLeave.leave_bIsCancelled = true;
                    cancelLeave.leave_vCancellationReason = txtLeaveCancellationReason.Text.Trim();
                    cancelLeave.leave_uiCancelledByStaffID = _loggedStaffID;
                    cancelLeave.leave_dtDateCancelled = DateTime.Now;
                    db.SubmitChanges();

                    //update leave details in the escalation transaction table with the leaves serail number being cancelled
                    UpdateLeavesInEscalationTransactionTable(cancelLeave.leave_vSerialNumber, "Cancelled", _loggedStaffID);

                    //display leaves to approve where the logged in employee is the required approving level id
                    GetLeavesToApproveFromEscalationTransactions(_loggedStaffID);
                    _hrClass.LoadHRManagerMessageBox(2, "Leave selected has been successfully cancelled!", this, PanelLeaves);
                    ModalPopupExtenderCancelLeaveApplication.Hide();

                    //create a send mail transaction record for the cancled leave
                    //case 3 for sending a mail alert when a leave application has been cancelled
                    _hrMailWebService.CreateMailSendingTransaction(3, leaveID, _loggedStaffID, null);

                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Leave cancellation failed." + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }
        //cancel leave application
        protected void lnkBtnConfirmCancelLeaveApplication_Click(object sender, EventArgs e)
        {
            CancelLeaveApplication(); return;
        }
        //get rejected leave history
        private void GetRejectedLeavesHistory(Guid staffID)
        {
            try
            {
                //if (db.PROC_RejectedLeavePerStaff(staffID).Any())
                //{
                object _display;
                _display = db.PROC_RejectedLeavePerStaff(staffID);
                gvRejectedLeaveHistory.DataSourceID = null;
                gvRejectedLeaveHistory.DataSource = _display;
                gvRejectedLeaveHistory.DataBind();

                Session["gvRejectedLeaveHistoryData"] = _display;
                TabPanelRejectedLeaves.Visible = true;
                //}
                //else
                //{
                //    //TabPanelRejectedLeaves.Visible = false;
                //}
            }
            catch { }
        }
        //enable page index changing on gvRejectedLeaveHistory
        protected void gvRejectedLeaveHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRejectedLeaveHistory.PageIndex = e.NewPageIndex;
            gvRejectedLeaveHistory.DataSource = (object)Session["gvRejectedLeaveHistoryData"];
            gvRejectedLeaveHistory.DataBind();
            return;
        }


        //load test leaves
        private void LoadTestLeaves()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("LeaveID", typeof(int)));
            dt.Columns.Add(new DataColumn("FromDate", typeof(string)));
            dt.Columns.Add(new DataColumn("ToDate", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfDays", typeof(string)));
            DataRow dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "14/Sep/2014";
            dr[2] = "17/Sep/2014";
            dr[3] = "4";
            dt.Rows.Add(dr);

            DataRow dr2 = dt.NewRow();
            dr2[0] = 2;
            dr2[1] = "12/Jun/2014";
            dr2[2] = "19/Jun/2014";
            dr2[3] = "7";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3[0] = 3;
            dr3[1] = "14/Mar/2014";
            dr3[2] = "20/Mar/2014";
            dr3[3] = "6";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4[0] = 4;
            dr4[1] = "03/Jan/2014";
            dr4[2] = "05/Jan/2014";
            dr4[3] = "3";
            dt.Rows.Add(dr4);
            gvLeavesDueForApproval.DataSource = dt;
            gvLeavesDueForApproval.DataBind();
        }
    }
}