﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class Education_Background : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayEducationBackground();//load logged user education backgrpound details
            }
        }
        //display staff education background details
        private void DisplayEducationBackground()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                _display = db.PROC_StaffEducationBackground(_staffID);
                gvEducationBackground.DataSourceID = null;
                gvEducationBackground.DataSource = _display;
                gvEducationBackground.DataBind();
                return;
            }
            catch { }
        }
    }
}