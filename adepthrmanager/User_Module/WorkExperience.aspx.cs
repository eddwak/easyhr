﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.User_Module
{
    public partial class WorkExperience : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //LoadEmploymentHistory();
                DisplayWorkExperience();//display staff work experience details
            }
        }
        //display staff work expreience details
        private void DisplayWorkExperience()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                _display = db.PROC_StaffWorkExperience(_staffID);
                gvEmploymentHistory.DataSourceID = null;
                gvEmploymentHistory.DataSource = _display;
                gvEmploymentHistory.DataBind();
                return;
            }
            catch { }
        }

        //load employment history
        private void LoadEmploymentHistory()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("EmploymentHistoryID", typeof(int)));
            dt.Columns.Add(new DataColumn("StartDate", typeof(string)));
            dt.Columns.Add(new DataColumn("EndDate", typeof(string)));
            dt.Columns.Add(new DataColumn("EmployerName", typeof(string)));
            dt.Columns.Add(new DataColumn("Position", typeof(string)));
            DataRow dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "01/Jan/2014";
            dr[2] = "Current";
            dr[3] = "Coca Cola EA";
            dr[4] = "Product Mananger";
            dt.Rows.Add(dr);

            DataRow dr2 = dt.NewRow();
            dr2[0] = 2;
            dr2[1] = "01/Aug/2011";
            dr2[2] = "20/Dec/2013";
            dr2[3] = "Glaxosmith";
            dr2[4] = "Senior Accountant";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3[0] = 3;
            dr3[1] = "05/Apr/2005";
            dr3[2] = "31/Jul/2011";
            dr3[3] = "Glaxosmith";
            dr3[4] = "Accountant";
            dt.Rows.Add(dr3);

            gvEmploymentHistory.DataSource = dt;
            gvEmploymentHistory.DataBind();
        }
    }
}