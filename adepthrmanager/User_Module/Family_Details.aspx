﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Family_Details.aspx.cs" Inherits="AdeptHRManager.User_Module.Family_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelFamilyDetails" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelFamilyDetails" runat="server">
                <div class="panel-details">
                    <asp:Panel ID="PanelSpouseDetails" runat="server">
                        <fieldset>
                            <legend>Spouse Details </legend>
                            <asp:GridView ID="gvSpouseDetails" runat="server" AllowPaging="True" Width="100%"
                                AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="5"
                                EmptyDataText="No spouse details available!">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1+"." %>
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("spouse_iSpouseID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="4px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="spouse_vFirstName" HeaderText="First Name" />
                                    <asp:BoundField DataField="spouse_vSurname" HeaderText="Surname" />
                                     <asp:BoundField DataField="spouse_vCountry" HeaderText="Country" />
                                      <asp:BoundField DataField="spouse_vNationality" HeaderText="Nationality" />
                                    <asp:BoundField DataField="spouse_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                    <asp:BoundField DataField="spouse_vEmployedAt" HeaderText="Employed At" />
                                    <asp:BoundField DataField="spouse_vEmploymentDetails" HeaderText="Employment Details" />
                                    <asp:BoundField DataField="spouse_vPhoneNumber" HeaderText="Phone No" />
                                    <asp:BoundField DataField="spouse_vEmailID" HeaderText="Email" />
                                    <asp:BoundField DataField="spouse_vPostalAddress" HeaderText="Postal Address" />
                                     <asp:BoundField DataField="spouse_vPassNo" HeaderText="Pass No" />
                                      <asp:BoundField DataField="spouse_vPassExpiryDate" HeaderText="Pass Expiry Date" />
                                      <asp:BoundField DataField="spouse_vMariageCerticateProvided" HeaderText="Marriage Certificate Provided?" />
                                </Columns>
                                <FooterStyle CssClass="PagerStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </fieldset>
                    </asp:Panel>
                    <asp:Panel ID="PanelChildrenDetails" runat="server">
                        <fieldset>
                            <legend>Children Details </legend>
                            <asp:GridView ID="gvChildDetails" runat="server" AllowPaging="True" Width="100%"
                                AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                                EmptyDataText="No child details available!">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1+"." %>
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("child_iChildID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="4px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="child_vFirstName" HeaderText="First Name" />
                                     <asp:BoundField DataField="child_vSurname" HeaderText="Surname" />
                                      <asp:BoundField DataField="child_vGender" HeaderText="Gender" />
                                    <asp:BoundField DataField="child_dtDOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date of Birth" />
                                    <asp:BoundField DataField="child_vAge" HeaderText="Age" />
                                    <asp:BoundField DataField="child_vCountryOfResidence" HeaderText="Country Of Residence" />
                                    <asp:BoundField DataField="child_vCountry" HeaderText="Country" />
                                    <asp:BoundField DataField="child_vNationality" HeaderText="Nationality" />
                                    <asp:BoundField DataField="child_vPassNo" HeaderText="Pass No" />
                                    <asp:BoundField DataField="child_vPassExpiryDate" HeaderText="Pass Expiry Date" />
                                    <asp:BoundField DataField="child_vBirthCertificateProvided" HeaderText="Birth / Adoption Certificate Provided?" />
                                </Columns>
                                <FooterStyle CssClass="PagerStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </fieldset>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
