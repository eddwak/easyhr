﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true" CodeBehind="Alerts.aspx.cs" Inherits="AdeptHRManager.User_Module.Alerts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:Panel ID="PanelAlerts" runat="server">
  <div class="panel-details">
                    <fieldset>
                        <legend>Anniversary Reminders</legend>
                        <asp:Label ID="lblContractEnd" runat="server" Text="ContractEnd" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblProbationEnd" runat="server" Text="Probation End" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblRetirementEnd" runat="server" Text="Retirement End" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblTenancyAgreement" runat="server" Text="Tenancy Agreement" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblCarAgreement" runat="server" Text="Car Agreement" CssClass="alerts" Visible="false"></asp:Label>
                        <asp:Label ID="lblLeaveApplication" runat="server" Text="Leave Application" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblWorkPermitEnd" runat="server" Text="Work Permit End" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblVisaRenewal" runat="server" Text="Visa Renewal" CssClass="alerts" Visible="false"></asp:Label>
                         <asp:Label ID="lblDependentPassExpiry" runat="server" Text="Dependent Pass End" CssClass="alerts" Visible="false"></asp:Label>
                         </fieldset>

                  
                  </div>
   </asp:Panel>
</asp:Content>
