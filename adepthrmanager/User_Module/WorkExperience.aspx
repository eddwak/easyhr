﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="WorkExperience.aspx.cs" Inherits="AdeptHRManager.User_Module.WorkExperience" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel-details">
        <asp:GridView ID="gvEmploymentHistory" runat="server" AllowPaging="True" Width="100%"
            AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
            EmptyDataText="No employement history available!">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1+"." %>
                    </ItemTemplate>
                    <ItemStyle Width="4px" />
                </asp:TemplateField>
                <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                <asp:BoundField DataField="EmployerName" HeaderText="Employer's Name" />
                <asp:BoundField DataField="PositionHeld" HeaderText="Position/Title Held" />
            </Columns>
            <FooterStyle CssClass="PagerStyle" />
            <AlternatingRowStyle CssClass="AltRowStyle" />
        </asp:GridView>
    </div>
</asp:Content>
