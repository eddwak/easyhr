﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;
using System.Data;

namespace AdeptHRManager.User_Module
{
    public partial class Performance_Review_old: System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropDowns();//populate dropdowns
                LoadStaffPerformanceReview();//display staff performance review details
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //load staff perfomance reive
        private void LoadStaffPerformanceReview()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                DisplayStaffQuarterlyReviewDocuments(_staffID);//display quartely review documents
                DisplayPerformancePlanObjective(_staffID, DateTime.Now.Year.ToString());//populate perforfomance objectives plan for the current year
                DisplayStaffToAppraise(_staffID);//display staff to appraise
            }
            catch { }
        }
        //populate drop downs
        private void PopulateDropDowns()
        {
            _hrClass.GenerateYears(ddlAnnualPerformancePlanYear);//populate annual years
            ddlAnnualPerformancePlanYear.SelectedValue = DateTime.Now.Year.ToString();//select current year
            _hrClass.GenerateYears(ddlQuarterlyReviewYear);//display Quarterly review years
            _hrClass.GenerateYears(ddlAnnualAppraisalYear);//display annual appraisal years
            GetAppraisalTemplates();//get appraisal templates
            //_hrClass.GetStaffNames(ddlQuarter1AppraisedBy, "Appraised By");//populate  quarter 1 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter2AppraisedBy, "Appraised By");//populate  quarter 2 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter3AppraisedBy, "Appraised By");//populate  quarter 3 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter4AppraisedBy, "Appraised By");//populate  quarter 4 appraised by dropdown
        }
        //populate documents dropdown
        private void GetAppraisalTemplates()
        {
            try
            {
                ddlAppraisalTemplates.Items.Clear();
                object _getStaff;
                _getStaff = db.AppraisalTemplates.Select(p => p);
                ddlAppraisalTemplates.DataSource = _getStaff;
                ddlAppraisalTemplates.Items.Insert(0, new ListItem("--"));
                ddlAppraisalTemplates.DataTextField = "appraisaltemplate_vTemplateTitle";
                ddlAppraisalTemplates.DataValueField = "appraisaltemplate_iAppraisalTemplateID";
                ddlAppraisalTemplates.AppendDataBoundItems = true;
                ddlAppraisalTemplates.DataBind();
                return;
            }
            catch { }
        }
        //load staff performance details for the selected year
        protected void ddlAnnualPerformancePlanYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "An error occured. There is no performance year selected. Select a performance year!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")
                    {
                        _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                        string _staffName = _hrClass.getStaffNames(_staffID);
                        lbStaffToAppraiseHeader.Text = _staffName + " Performance Objective Details for the Year " + _performanceYear;
                    }
                    DisplayPerformancePlanObjective(_staffID, _performanceYear);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        //download the appraisal template selected
        protected void ddlAppraisalTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _appraisalTemplateID = 0;

                if (ddlAppraisalTemplates.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No template selected. Select a template document that you want to downlod!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    _appraisalTemplateID = Convert.ToInt32(ddlAppraisalTemplates.SelectedValue);
                    //load the document that has been uploaded
                    string strPageUrl = "user_document_viewer.aspx?doc_ref=" + _appraisalTemplateID.ToString();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    return;
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        #region PERFORMANCE PLAN
        //add perfomance objective details
        protected void LinkButtonNewPerformanceObjective_Click(object sender, EventArgs e)
        {
            PanelPerformanceObjectiveListing.Visible = false;
            PanelAddPerformanceObjective.Visible = true;
            _hrClass.ClearEntryControls(PanelAddPerformanceObjective);
            ddlAnnualPerformancePlanYear.SelectedValue = DateTime.Now.Year.ToString();//select current year
            lnkBtnSavePerformancePlan.Text = "Save Performance Details";
            HiddenFieldPerformancePlanID.Value = "";

            PanelAnnualPerformanceObjectiveDetails.Enabled = true;
            //enable performance details panels
            PanelQuarter1PerformanceDetails.Enabled = true;
            PanelQuarter2PerformanceDetails.Enabled = true;
            PanelQuarter3PerformanceDetails.Enabled = true;
            PanelQuarter4PerformanceDetails.Enabled = true;
            //disable appraiser's panels details
            //PanelQuarter1AppraiserDetails.Enabled = false;
            //PanelQuarter2AppraiserDetails.Enabled = false;
            //PanelQuarter3AppraiserDetails.Enabled = false;
            //PanelQuarter4AppraiserDetails.Enabled = false;
            ////disable apraiser's date buttons
            //ImageButtonQuarter1AppraisalDate.Visible = false;
            //ImageButtonQuarter2AppraisalDate.Visible = false;
            //ImageButtonQuarter3AppraisalDate.Visible = false;
            //ImageButtonQuarter4AppraisalDate.Visible = false;
            return;
        }
        //validate controls
        private string ValidatePerformancePlanControls()
        {
            if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
            {
                ddlAnnualPerformancePlanYear.Focus();
                return "Select annual perfomance plan year!";
            }
            else if (txtPerformanceObjective.Text.Trim() == "")
            {
                txtPerformanceObjective.Focus();
                return "Enter performance objective!";
            }
            else if (txtKeyPerformanceIndicator.Text.Trim() == "")
            {
                txtKeyPerformanceIndicator.Focus();
                return "Enter key performance indicator!";
            }
            else if (txtAnnualPercentageWeight.Text.Trim() == "")
            {
                txtAnnualPercentageWeight.Focus();
                return "Enter percentage weight for the perfomance objective!";
            }
            else if (_hrClass.isNumberValid(txtAnnualPercentageWeight.Text.Trim()) == false)
            {
                txtAnnualPercentageWeight.Focus();
                return "Invalid percentage weight entered. Enter a numeric value for the percentage weight!";
            }
            else if (txtAnnualPercentageScore.Text.Trim() != "" && _hrClass.isNumberValid(txtAnnualPercentageScore.Text.Trim()) == false)
            {
                txtAnnualPercentageScore.Focus();
                return "Invalid percentage score entered. Enter a numeric value for the percentage score!";
            }
            //else if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter1AppraisalDate.Text.Trim()) == false)
            //{
            //    txtQuarter1AppraisalDate.Focus();
            //    return "Invalid quarter 1 appraisal date entered!";
            //}
            //else if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter2AppraisalDate.Text.Trim()) == false)
            //{
            //    txtQuarter2AppraisalDate.Focus();
            //    return "Invalid quarter 2 appraisal date entered!";
            //}
            //else if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter3AppraisalDate.Text.Trim()) == false)
            //{
            //    txtQuarter3AppraisalDate.Focus();
            //    return "Invalid quarter 3 appraisal date entered!";
            //}
            //else if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter4AppraisalDate.Text.Trim()) == false)
            //{
            //    txtQuarter4AppraisalDate.Focus();
            //    return "Invalid quarter 4 appraisal date entered!";
            //}
            else
                return "";
        }

        //save annual performance objective plan details]

        private void SaveAnnualPerfomanceObjectivePlan()
        {
            try
            {
                string _error = ValidatePerformancePlanControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelPerformanceReview); return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    string _performanceYear = "", _performanceObjective, _keyPerformanceIndicator = "";
                    _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                    _performanceObjective = txtPerformanceObjective.Text.Trim();
                    _keyPerformanceIndicator = txtKeyPerformanceIndicator.Text.Trim();
                    Double? _annualWeight = null, _annualScore = null;
                    _annualWeight = Convert.ToDouble(txtAnnualPercentageWeight.Text.Trim());
                    if (txtAnnualPercentageScore.Text.Trim() != "")
                        _annualScore = Convert.ToDouble(txtAnnualPercentageScore.Text.Trim());
                    //save the performance objective
                    if (lnkBtnSavePerformancePlan.Text.ToLower().Contains("save"))//check if were are saving a new perfomance objective
                    {
                        //save new performance objective plan details
                        SaveNewPerformanceObjectivePlan(_staffID, _performanceYear, _performanceObjective, _keyPerformanceIndicator, _annualWeight, _annualScore);
                        _hrClass.LoadHRManagerMessageBox(2, "Performance objective details have been successfully saved.", this, PanelPerformanceReview);

                    }
                    else
                    {
                        //update existing performance objective plan details
                        int _performancePlanID = Convert.ToInt32(HiddenFieldPerformancePlanID.Value);
                        UpdatePerformanceObjectivePlan(_performancePlanID, _performanceYear, _performanceObjective, _keyPerformanceIndicator, _annualWeight, _annualScore);
                        _hrClass.LoadHRManagerMessageBox(2, "Performance objective details have been successfully updated.", this, PanelPerformanceReview);

                    }
                    //populate the perfomance objectives
                    if (Convert.ToString(HiddenFieldStaffToAppraiseID.Value) != "")//check of were are appraising a staff
                        _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                    DisplayPerformancePlanObjective(_staffID, _performanceYear);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }

        }
        //save new performance objective
        private void SaveNewPerformanceObjectivePlan(Guid _staffID, string _year, string _performanceObjective, string _keyPerformanceIndicator, Double? _annualWeight, Double? _annualScore)
        {
            PerformancePlan newPerformancePlan = new PerformancePlan();
            newPerformancePlan.StaffID = _staffID;
            newPerformancePlan.PerformanceYear = _year;
            newPerformancePlan.PerformanceObjective = _performanceObjective;
            newPerformancePlan.KeyPerformanceIndicators = _keyPerformanceIndicator;
            newPerformancePlan.PercentageWeight = _annualWeight;
            newPerformancePlan.AnnualPercentageScore = _annualScore;

            //quarter one details
            newPerformancePlan.Quarter1Target = txtQuarter1Target.Text.Trim();
            newPerformancePlan.Quarter1Achieved = txtQuarter1Achieved.Text.Trim();
            newPerformancePlan.Quarter1Rating = txtQuarter1Rating.Text.Trim();
            newPerformancePlan.Quarter1AverageRating = txtQuarter1AverageRating.Text.Trim();
            newPerformancePlan.Quarter1Comments = txtQuarter1Comments.Text.Trim();
            newPerformancePlan.Quarter1NextQuarterActionPlan = txtQuarter1NextActionPlan.Text.Trim();
            //if (ddlQuarter1AppraisedBy.SelectedIndex != 0)
            //    newPerformancePlan.Quarter1AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter1AppraisedBy.SelectedValue);
            //else newPerformancePlan.Quarter1AppraisedByStaffID = null;
            //if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____")
            //    newPerformancePlan.Quarter1AppraisalDate = Convert.ToDateTime(txtQuarter1AppraisalDate.Text.Trim()).Date;
            //else newPerformancePlan.Quarter1AppraisalDate = null;
            //newPerformancePlan.Quarter1AppraiserComments = txtQuarter1AppraiserComments.Text.Trim();
            //newPerformancePlan.Quarter1ReviewedBySupervisor = false;
            //newPerformancePlan.Quarter1ReviewedByHR = false;

            //quarter two details
            newPerformancePlan.Quarter2Target = txtQuarter2Target.Text.Trim();
            newPerformancePlan.Quarter2Achieved = txtQuarter2Achieved.Text.Trim();
            newPerformancePlan.Quarter2Rating = txtQuarter2Rating.Text.Trim();
            newPerformancePlan.Quarter2AverageRating = txtQuarter2AverageRating.Text.Trim();
            newPerformancePlan.Quarter2Comments = txtQuarter2Comments.Text.Trim();
            newPerformancePlan.Quarter2NextQuarterActionPlan = txtQuarter2NextActionPlan.Text.Trim();
            //if (ddlQuarter2AppraisedBy.SelectedIndex != 0)
            //    newPerformancePlan.Quarter2AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter2AppraisedBy.SelectedValue);
            //else newPerformancePlan.Quarter2AppraisedByStaffID = null;
            //if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____")
            //    newPerformancePlan.Quarter2AppraisalDate = Convert.ToDateTime(txtQuarter2AppraisalDate.Text.Trim()).Date;
            //else newPerformancePlan.Quarter2AppraisalDate = null;
            //newPerformancePlan.Quarter2AppraiserComments = txtQuarter2AppraiserComments.Text.Trim();
            //newPerformancePlan.Quarter2ReviewedBySupervisor = false;
            //newPerformancePlan.Quarter2ReviewedByHR = false;

            //quarter three details
            newPerformancePlan.Quarter3Target = txtQuarter3Target.Text.Trim();
            newPerformancePlan.Quarter3Achieved = txtQuarter3Achieved.Text.Trim();
            newPerformancePlan.Quarter3Rating = txtQuarter3Rating.Text.Trim();
            newPerformancePlan.Quarter3AverageRating = txtQuarter3AverageRating.Text.Trim();
            newPerformancePlan.Quarter3Comments = txtQuarter3Comments.Text.Trim();
            newPerformancePlan.Quarter3NextQuarterActionPlan = txtQuarter3NextActionPlan.Text.Trim();
            //if (ddlQuarter3AppraisedBy.SelectedIndex != 0)
            //    newPerformancePlan.Quarter3AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter3AppraisedBy.SelectedValue);
            //else newPerformancePlan.Quarter3AppraisedByStaffID = null;
            //if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____")
            //    newPerformancePlan.Quarter3AppraisalDate = Convert.ToDateTime(txtQuarter3AppraisalDate.Text.Trim()).Date;
            //else newPerformancePlan.Quarter3AppraisalDate = null;
            //newPerformancePlan.Quarter3AppraiserComments = txtQuarter3AppraiserComments.Text.Trim();
            //newPerformancePlan.Quarter3ReviewedBySupervisor = false;
            //newPerformancePlan.Quarter3ReviewedByHR = false;

            //quarter four details
            newPerformancePlan.Quarter4Target = txtQuarter4Target.Text.Trim();
            newPerformancePlan.Quarter4Achieved = txtQuarter4Achieved.Text.Trim();
            newPerformancePlan.Quarter4Rating = txtQuarter4Rating.Text.Trim();
            newPerformancePlan.Quarter4AverageRating = txtQuarter4AverageRating.Text.Trim();
            newPerformancePlan.Quarter4Comments = txtQuarter4Comments.Text.Trim();
            newPerformancePlan.Quarter4NextQuarterActionPlan = txtQuarter4NextActionPlan.Text.Trim();
            //if (ddlQuarter4AppraisedBy.SelectedIndex != 0)
            //    newPerformancePlan.Quarter4AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter4AppraisedBy.SelectedValue);
            //else newPerformancePlan.Quarter4AppraisedByStaffID = null;
            //if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____")
            //    newPerformancePlan.Quarter4AppraisalDate = Convert.ToDateTime(txtQuarter4AppraisalDate.Text.Trim()).Date;
            //else newPerformancePlan.Quarter4AppraisalDate = null;
            //newPerformancePlan.Quarter4AppraiserComments = txtQuarter4AppraiserComments.Text.Trim();
            //newPerformancePlan.Quarter4ReviewedBySupervisor = false;
            //newPerformancePlan.Quarter4ReviewedByHR = false;


            db.PerformancePlans.InsertOnSubmit(newPerformancePlan);
            db.SubmitChanges();
            HiddenFieldPerformancePlanID.Value = newPerformancePlan.PerformancePlanID.ToString();

        }
        //update performance objective
        private void UpdatePerformanceObjectivePlan(int _performancePlanID, string _year, string _performanceObjective, string _keyPerformanceIndicator, Double? _annualWeight, Double? _annualScore)
        {
            PerformancePlan updatePerformancePlan = db.PerformancePlans.Single(p => p.PerformancePlanID == _performancePlanID);
            updatePerformancePlan.PerformanceYear = _year;
            updatePerformancePlan.PerformanceObjective = _performanceObjective;
            updatePerformancePlan.KeyPerformanceIndicators = _keyPerformanceIndicator;
            updatePerformancePlan.PercentageWeight = _annualWeight;
            updatePerformancePlan.AnnualPercentageScore = _annualScore;

            //quarter one details
            updatePerformancePlan.Quarter1Target = txtQuarter1Target.Text.Trim();
            updatePerformancePlan.Quarter1Achieved = txtQuarter1Achieved.Text.Trim();
            updatePerformancePlan.Quarter1Rating = txtQuarter1Rating.Text.Trim();
            updatePerformancePlan.Quarter1AverageRating = txtQuarter1AverageRating.Text.Trim();
            updatePerformancePlan.Quarter1Comments = txtQuarter1Comments.Text.Trim();
            updatePerformancePlan.Quarter1NextQuarterActionPlan = txtQuarter1NextActionPlan.Text.Trim();
            //if (ddlQuarter1AppraisedBy.SelectedIndex != 0)
            //    updatePerformancePlan.Quarter1AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter1AppraisedBy.SelectedValue);
            //else updatePerformancePlan.Quarter1AppraisedByStaffID = null;
            //if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____")
            //    updatePerformancePlan.Quarter1AppraisalDate = Convert.ToDateTime(txtQuarter1AppraisalDate.Text.Trim()).Date;
            //else updatePerformancePlan.Quarter1AppraisalDate = null;
            //updatePerformancePlan.Quarter1AppraiserComments = txtQuarter1AppraiserComments.Text.Trim();
            //Boolean _quarter1ReviewedBySupervisor = false, _quarter1ReviewedByHR = false;
            //if (rblHasQuarter1BeenReviewedBySupervisor.SelectedIndex == 0)
            //    _quarter1ReviewedBySupervisor = true;
            //if (rblHasQuarter1BeenReviewedByHR.SelectedIndex == 0)
            //    _quarter1ReviewedByHR = true;
            //updatePerformancePlan.Quarter1ReviewedBySupervisor = _quarter1ReviewedBySupervisor;
            //updatePerformancePlan.Quarter1ReviewedByHR = _quarter1ReviewedByHR;

            //quarter two details
            updatePerformancePlan.Quarter2Target = txtQuarter2Target.Text.Trim();
            updatePerformancePlan.Quarter2Achieved = txtQuarter2Achieved.Text.Trim();
            updatePerformancePlan.Quarter2Rating = txtQuarter2Rating.Text.Trim();
            updatePerformancePlan.Quarter2AverageRating = txtQuarter2AverageRating.Text.Trim();
            updatePerformancePlan.Quarter2Comments = txtQuarter2Comments.Text.Trim();
            updatePerformancePlan.Quarter2NextQuarterActionPlan = txtQuarter2NextActionPlan.Text.Trim();
            //if (ddlQuarter2AppraisedBy.SelectedIndex != 0)
            //    updatePerformancePlan.Quarter2AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter2AppraisedBy.SelectedValue);
            //else updatePerformancePlan.Quarter2AppraisedByStaffID = null;
            //if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____")
            //    updatePerformancePlan.Quarter2AppraisalDate = Convert.ToDateTime(txtQuarter2AppraisalDate.Text.Trim()).Date;
            //else updatePerformancePlan.Quarter2AppraisalDate = null;
            //updatePerformancePlan.Quarter2AppraiserComments = txtQuarter2AppraiserComments.Text.Trim();
            //Boolean _quarter2ReviewedBySupervisor = false, _quarter2ReviewedByHR = false;
            //if (rblHasQuarter2BeenReviewedBySupervisor.SelectedIndex == 0)
            //    _quarter2ReviewedBySupervisor = true;
            //if (rblHasQuarter2BeenReviewedByHR.SelectedIndex == 0)
            //    _quarter2ReviewedByHR = true;
            //updatePerformancePlan.Quarter2ReviewedBySupervisor = _quarter2ReviewedBySupervisor;
            //updatePerformancePlan.Quarter2ReviewedByHR = _quarter2ReviewedByHR;

            //quarter three details
            updatePerformancePlan.Quarter3Target = txtQuarter3Target.Text.Trim();
            updatePerformancePlan.Quarter3Achieved = txtQuarter3Achieved.Text.Trim();
            updatePerformancePlan.Quarter3Rating = txtQuarter3Rating.Text.Trim();
            updatePerformancePlan.Quarter3AverageRating = txtQuarter3AverageRating.Text.Trim();
            updatePerformancePlan.Quarter3Comments = txtQuarter3Comments.Text.Trim();
            updatePerformancePlan.Quarter3NextQuarterActionPlan = txtQuarter3NextActionPlan.Text.Trim();
            //if (ddlQuarter3AppraisedBy.SelectedIndex != 0)
            //    updatePerformancePlan.Quarter3AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter3AppraisedBy.SelectedValue);
            //else updatePerformancePlan.Quarter3AppraisedByStaffID = null;
            //if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____")
            //    updatePerformancePlan.Quarter3AppraisalDate = Convert.ToDateTime(txtQuarter3AppraisalDate.Text.Trim()).Date;
            //else updatePerformancePlan.Quarter3AppraisalDate = null;
            //updatePerformancePlan.Quarter3AppraiserComments = txtQuarter3AppraiserComments.Text.Trim();
            //Boolean _quarter3ReviewedBySupervisor = false, _quarter3ReviewedByHR = false;
            //if (rblHasQuarter3BeenReviewedBySupervisor.SelectedIndex == 0)
            //    _quarter3ReviewedBySupervisor = true;
            //if (rblHasQuarter3BeenReviewedByHR.SelectedIndex == 0)
            //    _quarter3ReviewedByHR = true;
            //updatePerformancePlan.Quarter3ReviewedBySupervisor = _quarter3ReviewedBySupervisor;
            //updatePerformancePlan.Quarter3ReviewedByHR = _quarter3ReviewedByHR;

            //quarter four details
            updatePerformancePlan.Quarter4Target = txtQuarter4Target.Text.Trim();
            updatePerformancePlan.Quarter4Achieved = txtQuarter4Achieved.Text.Trim();
            updatePerformancePlan.Quarter4Rating = txtQuarter4Rating.Text.Trim();
            updatePerformancePlan.Quarter4AverageRating = txtQuarter4AverageRating.Text.Trim();
            updatePerformancePlan.Quarter4Comments = txtQuarter4Comments.Text.Trim();
            updatePerformancePlan.Quarter4NextQuarterActionPlan = txtQuarter4NextActionPlan.Text.Trim();
            //if (ddlQuarter4AppraisedBy.SelectedIndex != 0)
            //    updatePerformancePlan.Quarter4AppraisedByStaffID = _hrClass.ReturnGuid(ddlQuarter4AppraisedBy.SelectedValue);
            //else updatePerformancePlan.Quarter4AppraisedByStaffID = null;
            //if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____")
            //    updatePerformancePlan.Quarter4AppraisalDate = Convert.ToDateTime(txtQuarter4AppraisalDate.Text.Trim()).Date;
            //else updatePerformancePlan.Quarter4AppraisalDate = null;
            //updatePerformancePlan.Quarter4AppraiserComments = txtQuarter4AppraiserComments.Text.Trim();
            //Boolean _quarter4ReviewedBySupervisor = false, _quarter4ReviewedByHR = false;
            //if (rblHasQuarter4BeenReviewedBySupervisor.SelectedIndex == 0)
            //    _quarter4ReviewedBySupervisor = true;
            //if (rblHasQuarter4BeenReviewedByHR.SelectedIndex == 0)
            //    _quarter4ReviewedByHR = true;
            //updatePerformancePlan.Quarter4ReviewedBySupervisor = _quarter4ReviewedBySupervisor;
            //updatePerformancePlan.Quarter4ReviewedByHR = _quarter4ReviewedByHR;

            db.SubmitChanges();

        }
        //save perfomance objective details
        protected void lnkBtnSavePerformancePlan_Click(object sender, EventArgs e)
        {
            SaveAnnualPerfomanceObjectivePlan();//save performance details
        }
        //close perfomance objective details
        protected void lnkBtnClosePerformancePlan_Click(object sender, EventArgs e)
        {
            PanelPerformanceObjectiveListing.Visible = true;
            PanelAddPerformanceObjective.Visible = false;
            _hrClass.ClearEntryControls(PanelAddPerformanceObjective);
            return;
        }
        //display staff performance plan objective details
        private void DisplayPerformancePlanObjective(Guid _staffID, string _year)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffPerfomancePlanObjective(_staffID, _year);
                gvPerformancePlanObjectives.DataSourceID = null;
                gvPerformancePlanObjectives.DataSource = _display;
                Session["gvPerformancePlanObjectivesData"] = _display;
                gvPerformancePlanObjectives.DataBind();
                return;
            }
            catch { }
        }
        //load perfomance details 
        protected void gvPerformancePlanObjectives_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("QuarterlyReview") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFPerformancePlanID = (HiddenField)gvPerformancePlanObjectives.Rows[ID].FindControl("HiddenField1");
                    int _performancePlanID = Convert.ToInt32(_HFPerformancePlanID.Value);
                    LoadPerformanceObjectivePlanControlsForEdit(_performancePlanID);
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        //load performance objective plan for edit
        private void LoadPerformanceObjectivePlanControlsForEdit(int _performancePlanID)
        {
            PerformancePlan getPerformancePlan = db.PerformancePlans.Single(p => p.PerformancePlanID == _performancePlanID);
            HiddenFieldPerformancePlanID.Value = _performancePlanID.ToString();
            ddlAnnualPerformancePlanYear.SelectedValue = getPerformancePlan.PerformanceYear;
            txtPerformanceObjective.Text = getPerformancePlan.PerformanceObjective;
            txtKeyPerformanceIndicator.Text = getPerformancePlan.KeyPerformanceIndicators;
            txtAnnualPercentageWeight.Text = getPerformancePlan.PercentageWeight.ToString();
            txtAnnualPercentageScore.Text = getPerformancePlan.AnnualPercentageScore.ToString();

            //quarter one details
            txtQuarter1Target.Text = getPerformancePlan.Quarter1Target;
            txtQuarter1Achieved.Text = getPerformancePlan.Quarter1Achieved;
            txtQuarter1Rating.Text = getPerformancePlan.Quarter1Rating;
            txtQuarter1AverageRating.Text = getPerformancePlan.Quarter1AverageRating;
            txtQuarter1Comments.Text = getPerformancePlan.Quarter1Comments;
            txtQuarter1NextActionPlan.Text = getPerformancePlan.Quarter1NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter1AppraisedByStaffID != null)
            //    ddlQuarter1AppraisedBy.SelectedValue = getPerformancePlan.Quarter1AppraisedByStaffID.ToString();
            //else ddlQuarter1AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter1AppraisalDate != null)
            //    txtQuarter1AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter1AppraisalDate.ToString());
            //else txtQuarter1AppraisalDate.Text = "";
            //txtQuarter1AppraiserComments.Text = getPerformancePlan.Quarter1AppraiserComments;

            //if (getPerformancePlan.Quarter1ReviewedBySupervisor == true)
            //    rblHasQuarter1BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter1BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter1ReviewedByHR == true)
            //    rblHasQuarter1BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter1BeenReviewedByHR.SelectedIndex = 1;

            //quarter two details
            txtQuarter2Target.Text = getPerformancePlan.Quarter2Target;
            txtQuarter2Achieved.Text = getPerformancePlan.Quarter2Achieved;
            txtQuarter2Rating.Text = getPerformancePlan.Quarter2Rating;
            txtQuarter2AverageRating.Text = getPerformancePlan.Quarter2AverageRating;
            txtQuarter2Comments.Text = getPerformancePlan.Quarter2Comments;
            txtQuarter2NextActionPlan.Text = getPerformancePlan.Quarter2NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter2AppraisedByStaffID != null)
            //    ddlQuarter2AppraisedBy.SelectedValue = getPerformancePlan.Quarter2AppraisedByStaffID.ToString();
            //else ddlQuarter2AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter2AppraisalDate != null)
            //    txtQuarter2AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter2AppraisalDate.ToString());
            //else txtQuarter2AppraisalDate.Text = ""; ;
            //txtQuarter2AppraiserComments.Text = getPerformancePlan.Quarter2AppraiserComments;

            //if (getPerformancePlan.Quarter2ReviewedBySupervisor == true)
            //    rblHasQuarter2BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter2BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter2ReviewedByHR == true)
            //    rblHasQuarter2BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter2BeenReviewedByHR.SelectedIndex = 1;


            //quarter three details
            txtQuarter3Target.Text = getPerformancePlan.Quarter3Target;
            txtQuarter3Achieved.Text = getPerformancePlan.Quarter3Achieved;
            txtQuarter3Rating.Text = getPerformancePlan.Quarter3Rating;
            txtQuarter3AverageRating.Text = getPerformancePlan.Quarter3AverageRating;
            txtQuarter3Comments.Text = getPerformancePlan.Quarter3Comments;
            txtQuarter3NextActionPlan.Text = getPerformancePlan.Quarter3NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter3AppraisedByStaffID != null)
            //    ddlQuarter3AppraisedBy.SelectedValue = getPerformancePlan.Quarter3AppraisedByStaffID.ToString();
            //else ddlQuarter3AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter3AppraisalDate != null)
            //    txtQuarter3AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter3AppraisalDate.ToString());
            //else txtQuarter3AppraisalDate.Text = ""; ;
            //txtQuarter3AppraiserComments.Text = getPerformancePlan.Quarter3AppraiserComments;

            //if (getPerformancePlan.Quarter3ReviewedBySupervisor == true)
            //    rblHasQuarter3BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter3BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter3ReviewedByHR == true)
            //    rblHasQuarter3BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter3BeenReviewedByHR.SelectedIndex = 1;

            //quarter four details
            txtQuarter4Target.Text = getPerformancePlan.Quarter4Target;
            txtQuarter4Achieved.Text = getPerformancePlan.Quarter4Achieved;
            txtQuarter4Rating.Text = getPerformancePlan.Quarter4Rating;
            txtQuarter4AverageRating.Text = getPerformancePlan.Quarter4AverageRating;
            txtQuarter4Comments.Text = getPerformancePlan.Quarter4Comments;
            txtQuarter4NextActionPlan.Text = getPerformancePlan.Quarter4NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter4AppraisedByStaffID != null)
            //    ddlQuarter4AppraisedBy.SelectedValue = getPerformancePlan.Quarter4AppraisedByStaffID.ToString();
            //else ddlQuarter4AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter4AppraisalDate != null)
            //    txtQuarter4AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter4AppraisalDate.ToString());
            //else txtQuarter4AppraisalDate.Text = ""; ;
            //txtQuarter4AppraiserComments.Text = getPerformancePlan.Quarter4AppraiserComments;

            //if (getPerformancePlan.Quarter4ReviewedBySupervisor == true)
            //    rblHasQuarter4BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter4BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter4ReviewedByHR == true)
            //    rblHasQuarter4BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter4BeenReviewedByHR.SelectedIndex = 1;

            lbAddPerformanceObjectiveHeader.Text = "Performance Objective for the Year";
            lnkBtnSavePerformancePlan.Text = "Update Performance Details";
            PanelAddPerformanceObjective.Visible = true;
            PanelPerformanceObjectiveListing.Visible = false;
            return;
        }
        //check if any performance objective is selected before editing
        protected void LinkButtonEditPerformanceObjective_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvPerformancePlanObjectives))//check if a performance objective is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no performance objective selected!. Select the performance objective that you want to edit!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvPerformancePlanObjectives.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one performance objective!. Select only one performance objective to edit at a time!", this, PanelPerformanceReview);
                        return;
                    }
                    foreach (GridViewRow _grv in gvPerformancePlanObjectives.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFPerformancePlanID = (HiddenField)_grv.FindControl("HiddenField1");
                            int _performancePlanID = Convert.ToInt32(_HFPerformancePlanID.Value);
                            LoadPerformanceObjectivePlanControlsForEdit(_performancePlanID);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading performance details. " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //load delete performance objective plan details
        protected void LinkButtonDeletePerformanceObjective_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvPerformancePlanObjectives))//check if there is any record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the objective that you want to delete!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvPerformancePlanObjectives.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected performance objective?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected performance objectives?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //method for deleting an qperformance plan objective
        private void DeletePerformancePlanObjective()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvPerformancePlanObjectives.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;

                    if (_cb.Checked)
                    {
                        HiddenField _HFPerformancePlanID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _performancePlanID = Convert.ToInt32(_HFPerformancePlanID.Value);

                        PerformancePlan deletePerformancePlan = db.PerformancePlans.Single(p => p.PerformancePlanID == _performancePlanID);
                        db.PerformancePlans.DeleteOnSubmit(deletePerformancePlan);
                        db.SubmitChanges();
                    }
                }
                //refresh performance objectives after delete
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                DisplayPerformancePlanObjective(_staffID, DateTime.Now.Year.ToString());
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected performance objective has been deleted.", this, PanelPerformanceReview);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected performance objectives have been deleted.", this, PanelPerformanceReview);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed . " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        #endregion

        #region  QUARTERLY REVIEW

        //upload quarterly review  document
        private void UploadQuarterlyReviewDocument()
        {
            try
            {
                if (ddlQuarterlyReviewYear.SelectedIndex == 0)
                {
                    ddlQuarterlyReviewYear.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Select review year!", this, PanelPerformanceReview);
                    return;
                }
                else if (ddlQuarterlyReviewQuarter.SelectedIndex == 0)
                {
                    ddlQuarterlyReviewQuarter.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Select quarter being reviewed!", this, PanelPerformanceReview);
                    return;
                }
                else if (FileUploadQuarterlyReviewForm.HasFile)
                {
                    int _fileLength = FileUploadQuarterlyReviewForm.PostedFile.ContentLength;
                    //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
                    if (_hrClass.IsUploadedFileBig(_fileLength) == true)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "File uploaded exceeds the allowed limit of 3mb.", this, PanelPerformanceReview);
                        return;
                    }
                    else
                    {
                        string _uploadedDocFileName = FileUploadQuarterlyReviewForm.FileName;//get name of the uploaded file name
                        string _fileExtension = Path.GetExtension(_uploadedDocFileName);

                        _fileExtension = _fileExtension.ToLower();
                        string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".xls", ".xlsx", ".rtf" };

                        bool _isFileAccepted = false;
                        foreach (string _acceptedFileExtension in _acceptedFileTypes)
                        {
                            if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
                                _isFileAccepted = true;
                        }
                        if (_isFileAccepted == false)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "The file you are trying to upload is not a permitted file type! The file should be pdf, word or excel document.", this, PanelPerformanceReview);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                            string _year = ddlQuarterlyReviewYear.SelectedValue;
                            char _quarter = Convert.ToChar(ddlQuarterlyReviewQuarter.SelectedValue);

                            //save uploaded template document
                            SaveQuarterlyReviewDocument(_staffID, FileUploadQuarterlyReviewForm, _year, _quarter, _uploadedDocFileName, _fileExtension);
                            return;
                        }
                    }
                }
                else
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Browse for the appraisal template document to upload!", this, PanelPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReview); return;
            }
        }

        //save quarterly review document
        private void SaveQuarterlyReviewDocument(Guid _staffID, FileUpload _fileUpload, string _year, char _quarter, string _uploadedDocFileName, string _fileExtension)
        {

            if (db.QuarterlyReviews.Any(p => p.StaffID == _staffID && p.Year == _year && p.Quarter == _quarter))
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Another quarterly review document for the same quarter and year selected already exists!", this, PanelPerformanceReview);
                return;
            }
            else
            {
                //save the document in the temp document
                _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
                string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
                //save the quartery review document into the database

                FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                //get document info
                FileInfo documentInfo = new FileInfo(_fileLocation);
                int fileSize = (int)documentInfo.Length;//get file size
                byte[] fileDocument = new byte[fileSize];
                fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

                //add the new document
                QuarterlyReview newQuarterlyReview = new QuarterlyReview();
                newQuarterlyReview.StaffID = _staffID;
                newQuarterlyReview.Year = _year;
                newQuarterlyReview.Quarter = _quarter;
                newQuarterlyReview.StaffDocumentName = _uploadedDocFileName;
                newQuarterlyReview.StaffUploadedDocument = fileDocument;
                newQuarterlyReview.DateStaffUploaded = DateTime.Now;
                db.QuarterlyReviews.InsertOnSubmit(newQuarterlyReview);
                db.SubmitChanges();
                //dispaly staff quartelry review documents
                DisplayStaffQuarterlyReviewDocuments(_staffID);
                _hrClass.LoadHRManagerMessageBox(2, "Quarterly review document has been successfully uploaded!", this, PanelPerformanceReview);
                return;
            }
        }
        //upload the filled quarterly review document
        protected void lnkBtnUploadQuarterlyReviewForm_OnClick(object sender, EventArgs e)
        {
            UploadQuarterlyReviewDocument(); return;
        }
        //display staff quarterly review documents
        private void DisplayStaffQuarterlyReviewDocuments(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffQuarterlyReview(_staffID);
                gvQuarterlyReviewDocuments.DataSourceID = null;
                gvQuarterlyReviewDocuments.DataSource = _display;
                gvQuarterlyReviewDocuments.DataBind();
                return;
            }
            catch { }
        }
        //load quartely review document details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvQuarterlyReviewDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFQuarterlyReviewID = (HiddenField)gvQuarterlyReviewDocuments.Rows[ID].FindControl("HiddenField1");
                    int _quarterlyReviewID = Convert.ToInt32(_HFQuarterlyReviewID.Value);
                    if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
                    {
                        HiddenFieldQuarterlyReviewFormID.Value = _quarterlyReviewID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the quarterly review document?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    }
                    else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
                    {
                        string strPageUrl = "user_document_Viewer.aspx?quarterlyreview=" + _HFQuarterlyReviewID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //method for deleting an quarterly review document
        private void DeleteQuarterlyReviewDocument()
        {
            try
            {
                int _quarterlyReviewID = Convert.ToInt32(HiddenFieldQuarterlyReviewFormID.Value);
                QuarterlyReview deleteQuarterlyReview = db.QuarterlyReviews.Single(p => p.QuarterlyReviewID == _quarterlyReviewID);
                db.QuarterlyReviews.DeleteOnSubmit(deleteQuarterlyReview);
                db.SubmitChanges();
                DisplayStaffQuarterlyReviewDocuments((Guid)deleteQuarterlyReview.StaffID);//reload quarterly review documents after deletion
                _hrClass.LoadHRManagerMessageBox(2, "Quarterly review document selected has been successfully deleted.", this, PanelPerformanceReview);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelPerformanceReview);
                return;
            }
        }
        //delete records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            if (TabContainerPerformanceReview.ActiveTabIndex == 0)//performance plan tab
                DeletePerformancePlanObjective();
            else if (TabContainerPerformanceReview.ActiveTabIndex == 1)//Quarterly review tab
                DeleteQuarterlyReviewDocument();
        }
        #endregion
        #region STAFF TO APPRAISE
        //display staff staff to appraise
        private void DisplayStaffToAppraise(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffUnderSupervisor(_staffID);
                gvStaffToAppraise.DataSourceID = null;
                gvStaffToAppraise.DataSource = _display;
                Session["gvStaffToAppraiseData"] = _display;
                gvStaffToAppraise.DataBind();
                return;
            }
            catch { }
        }
        //load staff to review performance details 
        protected void gvStaffToAppraise_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("ReviewPerformance") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFStaffID = (HiddenField)gvStaffToAppraise.Rows[ID].FindControl("HiddenField1");
                    Guid _staffID = _hrClass.ReturnGuid(_HFStaffID.Value);
                    LoadStaffToAppraiseDetails(_staffID);//load staff to appraise details
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //load staff to appraise details
        private void LoadStaffToAppraiseDetails(Guid _staffID)
        {
            HiddenFieldStaffToAppraiseID.Value = _staffID.ToString();
            string _staffName = _hrClass.getStaffNames(_staffID);
            string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
            lbStaffToAppraiseHeader.Text = _staffName + " Performance Objective Details for the Year " + _performanceYear + ".";
            //load performance details for staff year
            DisplayPerformancePlanObjective(_staffID, _performanceYear);
            PanelStaffToAppraiseHeader.Visible = true;
            PanelPerformanceObjectiveMenu.Visible = false;
            PanelAddPerformanceObjective.Visible = false;
            PanelPerformanceObjectiveListing.Visible = true;
            TabContainerPerformanceReview.ActiveTabIndex = 0;//go to tab one

            PanelAnnualPerformanceObjectiveDetails.Enabled = false;
            PanelStaffPerformancePlanQuarteryReview.Visible = false;
            ddlStaffQuarterToReview.SelectedIndex = 0;
            //disable performance details panels
            PanelQuarter1PerformanceDetails.Enabled = false;
            PanelQuarter2PerformanceDetails.Enabled = false;
            PanelQuarter3PerformanceDetails.Enabled = false;
            PanelQuarter4PerformanceDetails.Enabled = false;
            //enable appraiser's panels details
            //PanelQuarter1AppraiserDetails.Enabled = true;
            //PanelQuarter2AppraiserDetails.Enabled = true;
            //PanelQuarter3AppraiserDetails.Enabled = true;
            //PanelQuarter4AppraiserDetails.Enabled = true;
            ////enable appraiser's date buttons
            //ImageButtonQuarter1AppraisalDate.Visible = true;
            //ImageButtonQuarter2AppraisalDate.Visible = true;
            //ImageButtonQuarter3AppraisalDate.Visible = true;
            //ImageButtonQuarter4AppraisalDate.Visible = true;
            return;
        }
        //close staff to appraise details
        protected void ImageButtonCloseStaffToAppraiseDetails_Click(object sender, EventArgs e)
        {
            PanelStaffToAppraiseHeader.Visible = false;
            PanelPerformanceObjectiveMenu.Visible = true;
            PanelAddPerformanceObjective.Visible = false;
            PanelPerformanceObjectiveListing.Visible = true;
            Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            string _performanceYear = DateTime.Now.Year.ToString();
            DisplayPerformancePlanObjective(_staffID, _performanceYear);//display currently logged in staff performance details
            HiddenFieldStaffToAppraiseID.Value = "";

            PanelAnnualPerformanceObjectiveDetails.Enabled = true;
            //enable performance details panels
            PanelQuarter1PerformanceDetails.Enabled = true;
            PanelQuarter2PerformanceDetails.Enabled = true;
            PanelQuarter3PerformanceDetails.Enabled = true;
            PanelQuarter4PerformanceDetails.Enabled = true;
            //disable appraiser's panels details
            //PanelQuarter1AppraiserDetails.Enabled = false;
            //PanelQuarter2AppraiserDetails.Enabled = false;
            //PanelQuarter3AppraiserDetails.Enabled = false;
            //PanelQuarter4AppraiserDetails.Enabled = false;
            ////disable apraiser's date buttons
            //ImageButtonQuarter1AppraisalDate.Visible = false;
            //ImageButtonQuarter2AppraisalDate.Visible = false;
            //ImageButtonQuarter3AppraisalDate.Visible = false;
            //ImageButtonQuarter4AppraisalDate.Visible = false;
            return;
        }
        //download the review a staff quarter
        protected void ddlStaffQuarterToReview_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _quarter = 0;

                if (ddlStaffQuarterToReview.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No quarter selected. Select the quarter that you want to review!", this, PanelPerformanceReview);
                    return;
                }
                else
                {
                    _quarter = Convert.ToInt32(ddlStaffQuarterToReview.SelectedValue);
                    //load details for the quarter being reviewd

                    LoadStaffQuarterlyPlannedPerformanceForReview(_quarter);
                    return;
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceReview);
                return;
            }
        }
        //display staff quarter details for the selected quarter
        private void LoadStaffQuarterlyPlannedPerformanceForReview(int _quarter)
        {
            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
            string _staffName = _hrClass.getStaffNames(_staffID);
            string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
            string _reviewQuarterHeader = "Review " + _staffName + " Quarter " + _quarter + " Performance for the Year " + _performanceYear;
            lbReviewStaffQuarterHeader.Text = _reviewQuarterHeader;
            HiddenFieldQuarter.Value = _quarter.ToString();

            double _overallAverageRating = 0, _percentageWeight = 0, _averageRating = 0;//for calculating average rating
            //create a data table
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("PerformancePlanID", typeof(int)));
            dt.Columns.Add(new DataColumn("PerformanceObjective", typeof(string)));
            dt.Columns.Add(new DataColumn("KeyPerformanceIndicators", typeof(string)));
            dt.Columns.Add(new DataColumn("PercentageWeight", typeof(float)));
            dt.Columns.Add(new DataColumn("QuarterTarget", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterAchieved", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterKPIRating", typeof(string)));
            dt.Columns.Add(new DataColumn("AverageObjectiveRating", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterComment", typeof(string)));
            dt.Columns.Add(new DataColumn("NextQuarterActionPlan", typeof(string)));
            DataRow dr;
            foreach (PerformancePlan getPerformancePlan in db.PerformancePlans.Where(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear).OrderBy(p => p.PerformanceObjective))
            {

                dr = dt.NewRow();
                dr[0] = getPerformancePlan.PerformancePlanID;
                dr[1] = getPerformancePlan.PerformanceObjective;
                dr[2] = getPerformancePlan.KeyPerformanceIndicators;
                dr[3] = getPerformancePlan.PercentageWeight;
                if (_hrClass.isNumberValid(getPerformancePlan.PercentageWeight.ToString()) == true)//check if the value entered is numeric
                    _percentageWeight = Convert.ToDouble(getPerformancePlan.PercentageWeight.ToString());

                string _quarterTarget = "", _quarterAchieved = "", _quarterKPIRating = "N/A", _averageObjectiveRating = "N/A", _quarterComment = "", _nextQuarterActionPlan = "";
                if (_quarter == 1)//check if its quarter 1
                {
                    _quarterTarget = getPerformancePlan.Quarter1Target;
                    _quarterAchieved = getPerformancePlan.Quarter1Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter1Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter1AverageRating;
                    _quarterComment = getPerformancePlan.Quarter1Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter1NextQuarterActionPlan;
                }
                else if (_quarter == 2)//check if its quarter 2
                {
                    _quarterTarget = getPerformancePlan.Quarter2Target;
                    _quarterAchieved = getPerformancePlan.Quarter2Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter2Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter2AverageRating;
                    _quarterComment = getPerformancePlan.Quarter2Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter2NextQuarterActionPlan;
                }
                else if (_quarter == 3)//check if its quarter 3
                {
                    _quarterTarget = getPerformancePlan.Quarter3Target;
                    _quarterAchieved = getPerformancePlan.Quarter3Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter3Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter3AverageRating;
                    _quarterComment = getPerformancePlan.Quarter3Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter3NextQuarterActionPlan;
                }
                else if (_quarter == 4)//check if its quarter 4
                {
                    _quarterTarget = getPerformancePlan.Quarter4Target;
                    _quarterAchieved = getPerformancePlan.Quarter4Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter4Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter4AverageRating;
                    _quarterComment = getPerformancePlan.Quarter4Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter4NextQuarterActionPlan;
                }
                dr[4] = "<pre>" + _quarterTarget + "</pre>";
                dr[5] = "<pre>" + _quarterAchieved + "</pre>";
                dr[6] = "<pre>" + _quarterKPIRating + "</pre>";
                dr[7] = _averageObjectiveRating;
                if (_hrClass.isNumberValid(_averageObjectiveRating) == true)//check if the value entered is numeric
                    _averageRating = Convert.ToDouble(_averageObjectiveRating);
                dr[8] = _quarterComment;
                dr[9] = _nextQuarterActionPlan;
                dt.Rows.Add(dr);
                //calculate overall average rating
                _overallAverageRating += (_percentageWeight * _averageRating);
            }
            gvStaffQuarterlyPerformanceReview.DataSourceID = null;
            gvStaffQuarterlyPerformanceReview.DataSource = dt;
            gvStaffQuarterlyPerformanceReview.DataBind();
            _overallAverageRating = (_overallAverageRating / 100);

            txtQuarterOverallAverageRating.Text = "";
            txtQuarterAppraisalDate.Text = "";
            txtQuarterAppraiserComments.Text = "";
            //
            txtQuarterOverallAverageRating.Text = _hrClass.ConvertToCurrencyValue((decimal)_overallAverageRating);
            //chek if the quarter has already been appraised
            if (db.QuarterlyPerformancePlanReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter))
            {
                QuarterlyPerformancePlanReview getQuarterlyReview = db.QuarterlyPerformancePlanReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter);
                txtQuarterOverallAverageRating.Text = getQuarterlyReview.OverallAverageRating;
                txtQuarterAppraisalDate.Text = _hrClass.ShortDateDayStart(getQuarterlyReview.AppraisalDate.Value.Date);
                txtQuarterAppraiserComments.Text = getQuarterlyReview.AppraiserComments;
            }

            PanelStaffPerformancePlanQuarteryReview.Visible = true;
            PanelPerformanceObjectiveListing.Visible = false;
            return;
        }

        //close review  staff quarter details
        protected void ImageButtonCloseReviewStaffQuarter_Click(object sender, EventArgs e)
        {
            PanelStaffPerformancePlanQuarteryReview.Visible = false;
            PanelPerformanceObjectiveListing.Visible = true;
            ddlStaffQuarterToReview.SelectedIndex = 0;
            return;
        }
        //save quarte appraisal details
        private void SaveQuartelyPerfomanceReviewAppraisal()
        {
            try
            {
            if (txtQuarterOverallAverageRating.Text.Trim() == "")
            {

                _hrClass.LoadHRManagerMessageBox(1, "Enter overall average rating!", this, PanelPerformanceReview);
                txtQuarterOverallAverageRating.Focus();
                return;
            }
            if (txtQuarterAppraisalDate.Text.Trim() == "__/___/____")
            {
                _hrClass.LoadHRManagerMessageBox(1, "Enter appraisal date!", this, PanelPerformanceReview);
                txtQuarterAppraisalDate.Focus();
                return;
            }
            else if (txtQuarterAppraisalDate.Text.Trim() == "__/___/____" && _hrClass.isDateValid(txtQuarterAppraisalDate.Text.Trim()) == false)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Invalid appraisal date entered!", this, PanelPerformanceReview);
                txtQuarterAppraisalDate.Focus();
                return;
            }
            else
            {
                //save the appraisal details
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffToAppraiseID.Value);
                Guid _appraiserID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                Int16 _quarter = Convert.ToInt16(HiddenFieldQuarter.Value);
                string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                if (db.QuarterlyPerformancePlanReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter))
                {
                    //update existing quarter review
                    //add new quarter review
                    QuarterlyPerformancePlanReview updateQuarterlyReview = db.QuarterlyPerformancePlanReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter);
                    updateQuarterlyReview.StaffID = _staffID;
                    updateQuarterlyReview.PerformanceYear = _performanceYear;
                    updateQuarterlyReview.PerformanceQuarter = _quarter;
                    updateQuarterlyReview.OverallAverageRating = txtQuarterOverallAverageRating.Text.Trim();
                    updateQuarterlyReview.AppraisedByStaffID = _appraiserID;
                    updateQuarterlyReview.AppraisalDate = Convert.ToDateTime(txtQuarterAppraisalDate.Text.Trim()).Date;
                    updateQuarterlyReview.AppraiserComments = txtQuarterAppraiserComments.Text.Trim();
                    db.SubmitChanges();
                }
                else
                {
                    //add new quarter review
                    QuarterlyPerformancePlanReview newQuarterlyReview = new QuarterlyPerformancePlanReview();
                    newQuarterlyReview.StaffID = _staffID;
                    newQuarterlyReview.PerformanceYear = _performanceYear;
                    newQuarterlyReview.PerformanceQuarter = _quarter;
                    newQuarterlyReview.OverallAverageRating = txtQuarterOverallAverageRating.Text.Trim();
                    newQuarterlyReview.AppraisedByStaffID = _appraiserID;
                    newQuarterlyReview.AppraisalDate = Convert.ToDateTime(txtQuarterAppraisalDate.Text.Trim()).Date;
                    newQuarterlyReview.AppraiserComments = txtQuarterAppraiserComments.Text.Trim();
                    db.QuarterlyPerformancePlanReviews.InsertOnSubmit(newQuarterlyReview);
                    db.SubmitChanges();
                }

                _hrClass.LoadHRManagerMessageBox(2, "Appraisal details entered. ", this, PanelPerformanceReview);
                return;
            }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelPerformanceReview);
                return;
            }
        }
        //save quarter appraisal details
        protected void lnkBtnSaveQuarterAppraisalDetails_Click(object sender, EventArgs e)
        {
            SaveQuartelyPerfomanceReviewAppraisal();
            return;
        }
        #endregion
    }
}