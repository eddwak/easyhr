﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using AdeptHRManager.HRManagerClasses;
using System.Net;

namespace AdeptHRManager
{
    public partial class Login : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtStaffUserName.Text = "kirinya";//user name for test purpose
            txtStaffPassword.Text = "Kirinya1!";//for test purpose

            txtAdminUserName.Text = "admin";//for test afmin
            txtAdminPassword.Text = "Admin1!";//for test
        }
        //perform submit user login
        private void SubmitStaffUserLogin()
        {
            try
            {
                string _password = txtStaffPassword.Text.Trim();
                string _userName = txtStaffUserName.Text.Trim().ToLower();
                lbStaffLoginAttempts.Text = "";
                if (_password != "")
                    txtStaffPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                else txtStaffPassword.Text = "";
                if (txtStaffUserName.Text.Trim() == "")
                {
                    txtStaffUserName.Focus();
                    lbStaffError.Text = "Enter user name !";
                    return;
                }
                else if (txtStaffPassword.Text.Trim() == "")
                {
                    txtStaffPassword.Focus();
                    lbStaffError.Text = "Enter  password !";
                    return;
                }
                else if (_userName == "admin")//check if its the admin 
                {
                    lbStaffError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else if (db.Users.Any(p => p.UserName.ToLower() == _userName) == false)
                {
                    txtStaffUserName.Focus();
                    lbStaffError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else
                {
                    //get the user with the username entered
                    Users_View getUser = db.Users_Views.Single(p => p.UserName.Trim().ToLower() == _userName);
                    string _getPassword = getUser.Password;
                    byte _accountStatus = Convert.ToByte(getUser.IsActive);//get user account status
                    if (_accountStatus == 0)//
                    {
                        txtStaffUserName.Focus();
                        lbStaffError.Text = "Login failed. The user account is no longer active. Contact the administrator for help !";
                        return;
                    }
                    else
                    {
                        //get the user login attempts
                        Int16 _loginAttempts = (short)getUser.LoginAttempts;
                        if (_loginAttempts == 3)
                        {
                            txtStaffUserName.Focus();
                            lbStaffError.Text = "Your account has been de-activated beacuse you have exhausted the defined login attempts.<br>Contact the administrator for help!";
                            return;
                        }
                        else
                        {
                            //validate user entry details
                            if (_hrClass.ValidateHashAndEncrytPwd(_password, _getPassword) == true && _hrClass.ValidatePassword(_password, _getPassword) == true)
                            {
                                FormsAuthentication.RedirectFromLoginPage(txtStaffUserName.Text, cbStaffRememberMe.Checked);
                                //Get the requested page from the url
                                bool rememberUserName = cbStaffRememberMe.Checked;
                                HttpContext.Current.Session["LoggedUserID"] = getUser.UserID;//pass user id
                                HttpContext.Current.Session["LoggedStaffID"] = getUser.StaffID;//pass staff id
                                HttpContext.Current.Session["LoggedStaffName"] = getUser.StaffName;//pass staff name
                                HttpContext.Current.Session["LoggedStaffPostedAtID"] = getUser.PostedAtID;//pass staff posted at ID
                                //update user account details after login
                                getUser.LoginAttempts = 0;
                                db.SubmitChanges();
                                //redirect the user to the default page
                                //Response.Redirect("~/User_Module/PersonalInformation.aspx");
                                string _defaultPage = "~/User_Module/PersonalInformation.aspx";

                                Guid _staffID = (Guid)getUser.StaffID;
                                //login user
                                LogInUser(_staffID, _defaultPage);
                                return;
                            }
                            else
                            {
                                //update login attempts made
                                _loginAttempts++;//increase login attempts by one
                                getUser.LoginAttempts = _loginAttempts;
                                db.SubmitChanges();
                                int _attemptsLeft = (Convert.ToInt16(3) - Convert.ToInt16(getUser.LoginAttempts));

                                //count the  number of login
                                string _attemptsMsg = "";
                                if (_attemptsLeft > 1)
                                    _attemptsMsg = " login attempts ";
                                else
                                    _attemptsMsg = " login attempt ";
                                lbStaffLoginAttempts.Text = "<br><br>You are left with " + _attemptsLeft + _attemptsMsg + "out of 3 attempts!";

                                lbStaffError.Text = "Invalid username and/or password entered. Please try again!";
                                txtStaffUserName.Focus();
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbStaffError.Text = "Login failed. " + ex.Message.ToString() + " Please try again !";
            }
        }
        //login staff user
        protected void lnkBtnStaffLogin_Click(object sender, EventArgs e)
        {
            SubmitStaffUserLogin();
            return;
        }

        //perform submit admin user login
        private void SubmitAdminUserLogin()
        {
            try
            {
                string _password = txtAdminPassword.Text.Trim();
                string _userName = txtAdminUserName.Text.Trim().ToLower();
                if (_password != "")
                    txtAdminPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                else txtAdminPassword.Text = "";
                if (txtAdminUserName.Text.Trim() == "")
                {
                    txtAdminUserName.Focus();
                    lbAdminError.Text = "Enter user name !";
                    return;
                }
                else if (txtAdminPassword.Text.Trim() == "")
                {
                    txtAdminPassword.Focus();
                    lbAdminError.Text = "Enter  password !";
                    return;
                }
                else if (db.Users.Any(p => p.UserName.ToLower() == _userName) == false)
                {
                    txtAdminUserName.Focus();
                    lbAdminError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else
                {
                    //get the user with the username entered
                    Users_View getUser = db.Users_Views.Single(p => p.UserName.Trim().ToLower() == _userName);
                    string _getPassword = getUser.Password;
                    byte _accountStatus = Convert.ToByte(getUser.IsActive);//get user account status
                    if (_accountStatus == 0)//
                    {
                        txtAdminUserName.Focus();
                        lbAdminError.Text = "Login failed. The user account is no longer active. Contact the administrator for help !";
                        return;
                    }
                    else
                    {

                        //get the user login attempts
                        Int16 _loginAttempts = (short)getUser.LoginAttempts;
                        if (_loginAttempts == 3)
                        {
                            txtAdminUserName.Focus();
                            lbAdminError.Text = "Your account has been de-activated beacuse you have exhausted the defined login attempts.<br>Contact the administrator for help!";
                            return;
                        }
                        else
                        {
                            //validate user entry details
                            if (_hrClass.ValidateHashAndEncrytPwd(_password, _getPassword) == true && _hrClass.ValidatePassword(_password, _getPassword) == true)
                            {
                                //get role associated with the user
                                string _userRole = db.Roles_Views.Single(p => p.RoleID == getUser.UserRoleID).RoleName;
                                if (_userRole != "Administrator")
                                {
                                    lbAdminError.Text = "Login failed. Your account does not have access to this module!";
                                    return;
                                }
                                else
                                {
                                    FormsAuthentication.RedirectFromLoginPage(txtAdminUserName.Text, cbAdminRememberMe.Checked);
                                    //Get the requested page from the url
                                    bool rememberUserName = cbAdminRememberMe.Checked;
                                    HttpContext.Current.Session["LoggedUserID"] = getUser.UserID;//pass user id
                                    HttpContext.Current.Session["LoggedStaffID"] = getUser.StaffID;//pass staff id
                                    HttpContext.Current.Session["LoggedStaffName"] = getUser.StaffName;//pass staff name
                                    HttpContext.Current.Session["LoggedStaffPostedAtID"] = getUser.PostedAtID;//pass staff posted at ID
                                    //update user account details after login
                                    getUser.LoginAttempts = 0;
                                    db.SubmitChanges();
                                    //redirect the user to the default page
                                    //Response.Redirect("~/Default.aspx");
                                    //Response.Redirect("~/HR_Module/Staff_Master.aspx");
                                    string _defaultPage = "~/default.aspx";
                                    Guid _staffID = (Guid)getUser.StaffID;
                                    //login user
                                    LogInUser(_staffID, _defaultPage);
                                    return;
                                }
                            }
                            else
                            {
                                //update login attempts made
                                _loginAttempts++;//increase login attempts by one
                                getUser.LoginAttempts = _loginAttempts;
                                db.SubmitChanges();
                                int _attemptsLeft = (Convert.ToInt16(3) - Convert.ToInt16(getUser.LoginAttempts));

                                //count the  number of login
                                string _attemptsMsg = "";
                                if (_attemptsLeft > 1)
                                    _attemptsMsg = " login attempts ";
                                else
                                    _attemptsMsg = " login attempt ";
                                lbAdminLoginAttempts.Text = "<br><br>You are left with " + _attemptsLeft + _attemptsMsg + "out of 3 attempts!";

                                lbAdminError.Text = "Invalid username and/or password entered. Please try again!";
                                txtAdminUserName.Focus();
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbAdminError.Text = "Login failed. " + ex.Message.ToString() + " Please try again !";
            }
        }
        //login user
        protected void lnkBtnAdminLogin_Click(object sender, EventArgs e)
        {
            SubmitAdminUserLogin();
            return;
        }
        //create new user log record
        private void CreateUserLog(Guid _staffID)
        {
            UserLog newUserLog = new UserLog();
            newUserLog.userlog_uiStaffID = _staffID;
            newUserLog.userlog_dtLoginTime = DateTime.Now;
            newUserLog.userlog_vMachineIP = HttpContext.Current.Request.UserHostName;//get ip address
            newUserLog.userlog_vMachineName = Dns.GetHostName();//WindowsIdentity.GetCurrent().Name.ToString();//Environment.MachineName;//get machine name
            newUserLog.userlog_vMachineUser = HttpContext.Current.Request.LogonUserIdentity.Name;//get the logged machene user
            //newUserLog.userlog_vMachineUser = Environment.UserName;
            //newUserLog.userlog_vMachineUser = HttpContext.Current.User.Identity.Name.ToString();
            //newUserLog.userlog_siModulesID = Convert.ToInt16(ddlModule.SelectedValue);
            //newUserLog.userlog_dtLogoutTime
            //newUserLog.userlog_siLastAppPagesID
            db.UserLogs.InsertOnSubmit(newUserLog);
            db.SubmitChanges();
            Session["LoggedUserLogID"] = newUserLog.userlog_iUserLogID;//create as session for user logid to be used when logging out
        }
        //log user

        private void LogInUser(Guid _staffID, string _defaultPage)
        {
            string pageRedirect = _defaultPage;
            int lastUserLogID = 0;
            //get lastUserLogID for the employee for the module selected
            try
            {
                lastUserLogID = Convert.ToInt16(db.UserLogs.Where(p => p.userlog_uiStaffID == _staffID).Max(p => p.userlog_iUserLogID));
            }
            catch { }
            if (lastUserLogID == 0)//
            {
                CreateUserLog(_staffID);//create a new user log
                Response.Redirect(pageRedirect);
                return;
            }
            else
            {
                //get last application page id accessed
                int lastAppPageID = Convert.ToInt32(db.UserLogs.Single(p => p.userlog_iUserLogID == lastUserLogID).userlog_siLastAppPagesID);
                if (lastAppPageID == 0)//check if the last accessed page was saved
                {
                    CreateUserLog(_staffID);
                    Response.Redirect(pageRedirect);
                    return;
                }
                else
                {
                    //GET LAST PAGE URL
                    string _lastPageUrl = db.AppPages.Single(p => p.apppages_PageID == lastAppPageID).apppages_Url;
                    CreateUserLog(_staffID);
                    Response.Redirect(_lastPageUrl);
                    return;
                }

            }
        }
     
    }
}