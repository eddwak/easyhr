﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Assets : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
                _hrClass.GenerateYears(ddlVehicleYearOfManufacture);// populate vehicle year of manufacture
                _hrClass.GenerateYears(ddlHouseYearOfManufacture);// populate house year of manufacture
                _hrClass.GenerateYears(ddlTelephoneYearOfManufacture);// populate telephone year of manufacture
                _hrClass.GenerateYears(ddlLaptopYearOfManufacture);// populate laptop year of manufacture
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffAssets(_staffID);//load staff training attended details
                    return;
                }
            }
            catch { }
        }
        //display staff assets by selected staff
        private void DisplayStaffAssets(Guid _staffID)
        {
            try
            {
                LoadStaffVehicleDetails(_staffID);//populate vehicle details
                LoadStaffHouseDetails(_staffID);//populate house details
                LoadStaffTelephoneDetails(_staffID);//populate telephone details
                LoadStaffLaptopDetails(_staffID);//populate laptop details
                LoadStaffAssetFinancedDetails(_staffID);//populate asset financed details
                LoadStaffLoanInformationDetails(_staffID);//populate loan information details
                return;
            }
            catch { }
        }
        //search for   by staff reference number
        private void SearchForStaffAssetsByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAssets(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAssets(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelStaffAssets);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffAssets);
                return;
            }
        }
        //search staff assets by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffAssetsByStaffReference();
            return;
        }
        //search for staff assets by staff namae
        private void SearchForStaffAssetsByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAssets(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAssets(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffAssets);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffAssets);
                return;
            }
        }
        //search staff assets by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffAssetsByStaffName();
            return;
        }
        //search for staff by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAssets(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAssets(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffAssets);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffAssets);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAssets(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAssets(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffAssets);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffAssets);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }

        #region CAR DETAILS
        //enable the panel for adding vehicle details
        protected void rblVehicleAssigned_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblVehicleAssigned.SelectedIndex == 0)
            {
                PanelAddCarDetails.Enabled = true;
                foreach (Control _control in PanelAddCarDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddCarDetails.Enabled = false;
                foreach (Control _control in PanelAddCarDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add car details
        private string ValidateAddCarDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the car details against!";
            else if (txtTypeOfVehicle.Text.Trim() == "")
                return "Enter the type of the vehicle!";
            else if (txtVehicleMake.Text.Trim() == "")
                return "Enter vehicle make!";
            else if (txtVehicleColor.Text.Trim() == "")
                return "Enter vehicle color!";
            else if (txtVehicleRegistrationNumber.Text.Trim() == "")
                return "Enter vehicle registration number!";
            else if (rblVehicleDrivingDetails.SelectedIndex == -1)
                return "Select vehicle driving details!";
            else if (txtVehicleAgreementEndDate.Text.Trim() == "")
                return "Enter vehicle agreement end date!";
            else if (_hrClass.isDateValid(txtVehicleAgreementEndDate.Text.Trim()) == false)          
                return "Invalid vehicle agreement end date entered";         
            else if (txtDrivingPermitExpiryDate.Text.Trim() == "")
                return "Enter driving permit expiry date!";
            else if (_hrClass.isDateValid(txtDrivingPermitExpiryDate.Text.Trim()) == false)
                return "Invalid driving permit expiry date entered"; 
            else if (ddlVehicleYearOfManufacture.SelectedIndex == 0)
                return "Select vehicle's year of manufacture!";
            else return "";
        }
        //method for saving car details
        private void SaveCarDetails()
        {
            try
            {
                if (rblVehicleAssigned.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the employee has been given a vehicle!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _isAssignedVehicle = false;
                    if (rblVehicleAssigned.SelectedIndex == 0)
                        _isAssignedVehicle = true;
                    //check if we are adding some details
                    if (_isAssignedVehicle == true)
                    {
                        string _error = ValidateAddCarDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save car details
                            //check  if the staff has a vehicle record
                            if (db.StaffVehicles.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffVehicle(_staffID, _isAssignedVehicle, txtTypeOfVehicle.Text.Trim(), txtVehicleColor.Text.Trim(), txtVehicleMake.Text.Trim(), txtVehicleRegistrationNumber.Text.Trim(), Convert.ToBoolean(rblVehicleDrivingDetails.SelectedValue), txtVehicleAssignedDriver.Text.Trim(), ddlVehicleYearOfManufacture.SelectedValue, txtVehicleComments.Text.Trim(), Convert.ToDateTime(txtVehicleAgreementEndDate.Text.Trim()).Date, Convert.ToDateTime(txtDrivingPermitExpiryDate.Text.Trim()).Date);
                            }
                            else
                            {
                                SaveNewVehicle(_staffID, _isAssignedVehicle, txtTypeOfVehicle.Text.Trim(), txtVehicleColor.Text.Trim(), txtVehicleMake.Text.Trim(), txtVehicleRegistrationNumber.Text.Trim(), Convert.ToBoolean(rblVehicleDrivingDetails.SelectedValue), txtVehicleAssignedDriver.Text.Trim(), ddlVehicleYearOfManufacture.SelectedValue, txtVehicleComments.Text.Trim(), Convert.ToDateTime(txtVehicleAgreementEndDate.Text.Trim()).Date, Convert.ToDateTime(txtDrivingPermitExpiryDate.Text.Trim()).Date);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Car details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the car details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check  if the staff has a vehicle record
                            if (db.StaffVehicles.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffVehicle(_staffID, _isAssignedVehicle, null, null, null, null, false, null, null, null, null, null);
                            }
                            else
                            {
                                SaveNewVehicle(_staffID, _isAssignedVehicle, null, null, null, null, false, null, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Car details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }
        }
        //saave car details
        protected void lnkBtnSaveVehicle_Click(object sender, EventArgs e)
        {
            SaveCarDetails();//save car details
            return;
        }
        //save new vehicle 
        private void SaveNewVehicle(Guid _staffID, Boolean _IsAssignedVehicle, string _typeOfVehicle, string _vehicleColor, string _vehicleMake, string _registrationNumber, bool? _driverDetails, string _assignedDriver, string _yearOfManufacture, string _comments, DateTime? _VehicleAgreementEndDate, DateTime? _DrivingPermitExpiryDate)
        {
            StaffVehicle newVehicle = new StaffVehicle();
            newVehicle.StaffID = _staffID;
            newVehicle.IsStaffAssignedVehicle = _IsAssignedVehicle;
            newVehicle.TypeOfVehicle = _typeOfVehicle;
            newVehicle.VehicleColor = _vehicleColor;
            newVehicle.VehicleMake = _vehicleMake;
            newVehicle.RegistrationNumber = _registrationNumber;
            newVehicle.DriverDetails = _driverDetails;
            newVehicle.AssignedDriver = _assignedDriver;
            newVehicle.YearOfManaufacture = _yearOfManufacture;
            newVehicle.Comments = _comments;
            newVehicle.VehicleAgreementEndDate = _VehicleAgreementEndDate;
            newVehicle.DrivingPermitExpiryDate = _DrivingPermitExpiryDate;
            db.StaffVehicles.InsertOnSubmit(newVehicle);
            db.SubmitChanges();
        }
        //update staff vehicle details
        private void UpdateStaffVehicle(Guid _staffID, Boolean _IsAssignedVehicle, string _typeOfVehicle, string _vehicleColor, string _vehicleMake, string _registrationNumber, Boolean _driverDetails, string _assignedDriver, string _yearOfManufacture, string _comments, DateTime? _VehicleAgreementEndDate, DateTime? _DrivingPermitExpiryDate)
        {
            StaffVehicle updateVehicle = db.StaffVehicles.Single(p => p.StaffID == _staffID);
            updateVehicle.IsStaffAssignedVehicle = _IsAssignedVehicle;
            updateVehicle.TypeOfVehicle = _typeOfVehicle;
            updateVehicle.VehicleColor = _vehicleColor;
            updateVehicle.VehicleMake = _vehicleMake;
            updateVehicle.RegistrationNumber = _registrationNumber;
            updateVehicle.DriverDetails = _driverDetails;
            updateVehicle.AssignedDriver = _assignedDriver;
            updateVehicle.YearOfManaufacture = _yearOfManufacture;
            updateVehicle.Comments = _comments;
            updateVehicle.VehicleAgreementEndDate = _VehicleAgreementEndDate;
            updateVehicle.DrivingPermitExpiryDate = _DrivingPermitExpiryDate;
            db.SubmitChanges();
        }
        //load staff vehicle details
        private void LoadStaffVehicleDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffVehicles.Any(p => p.StaffID == _staffID))
                {
                    StaffVehicle getStaffVehicle = db.StaffVehicles.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffVehicle.IsStaffAssignedVehicle == true)
                        rblVehicleAssigned.SelectedIndex = 0;
                    else rblVehicleAssigned.SelectedIndex = 1;
                    txtTypeOfVehicle.Text = getStaffVehicle.TypeOfVehicle;
                    txtVehicleColor.Text = getStaffVehicle.VehicleColor;
                    txtVehicleMake.Text = getStaffVehicle.VehicleMake;
                    txtVehicleRegistrationNumber.Text = getStaffVehicle.RegistrationNumber;
                    if (getStaffVehicle.DriverDetails == false)
                        rblVehicleDrivingDetails.SelectedIndex = 0;
                    else rblVehicleDrivingDetails.SelectedIndex = 1;                   
                    txtVehicleAssignedDriver.Text = getStaffVehicle.AssignedDriver;
                    ddlVehicleYearOfManufacture.SelectedValue = getStaffVehicle.YearOfManaufacture;
                    txtVehicleComments.Text = getStaffVehicle.Comments;
                    txtVehicleAgreementEndDate.Text = _hrClass.ShortDateDayStart(getStaffVehicle.VehicleAgreementEndDate.ToString());
                    txtDrivingPermitExpiryDate.Text = _hrClass.ShortDateDayStart(getStaffVehicle.DrivingPermitExpiryDate.ToString());
                    PanelAddCarDetails.Enabled = true;
                    // if (Convert.ToBoolean(getStaffVehicle.IsStaffAssignedVehicle) == true)
                    if ((bool)getStaffVehicle.IsStaffAssignedVehicle == true)
                    {
                        PanelAddCarDetails.Enabled = true;
                        foreach (Control _control in PanelAddCarDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    //else if(Convert.ToBoolean(getStaffVehicle.IsStaffAssignedVehicle) ==false)
                    else
                    {
                        PanelAddCarDetails.Enabled = false;
                        foreach (Control _control in PanelAddCarDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddCarDetails);
                    }
                }
                else
                {
                    PanelAddCarDetails.Enabled = false;
                    foreach (Control _control in PanelAddCarDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddCarDetails);
                    rblVehicleAssigned.SelectedIndex = -1;
                }

            }
            catch { }
        }
        #endregion
        #region HOUSE DETAILS
        //enable the panel for adding house details
        protected void rblHouseAllocated_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblHouseAllocated.SelectedIndex == 0)
            {
                PanelAddHouseDetails.Enabled = true;
                foreach (Control _control in PanelAddHouseDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddHouseDetails.Enabled = false;
                foreach (Control _control in PanelAddHouseDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add house details
        private string ValidateAddHouseDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the house details against!";
            else if (txtHouseBlockNumber.Text.Trim() == "")
                return "Enter house block number!";
            else if (txtHousePlotNumber.Text.Trim() == "")
                return "Enter plot number!";
            else if (txtHouseRoad.Text.Trim() == "")
                return "Enter house road!";
            else if (txtHouseRentAmount.Text.Trim() == "")
                return "Enter house rent amount!";
            else if (ddlHouseYearOfManufacture.SelectedIndex == 0)
                return "Select house year of manufacture!";
            else if (txtHouseTenure.Text.Trim() == "")
                return "Enter house tenure  details!";
            else if (rblHouseTenancyAgreement.SelectedIndex == -1)
                return "Select whether the tenancy agreement has been recieved and signed!";
            else if (txtTenancyAgreementEndDate.Text.Trim() == "")
                return "Enter tenancy agreement end date!";
            else if (_hrClass.isDateValid(txtTenancyAgreementEndDate.Text.Trim()) == false)
                return "Invalid tenancy agreement end date entered"; 
            else return "";
        }
        //method for saving car details
        private void SaveHouseDetails()
        {
            try
            {

                if (rblHouseAllocated.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the emloyee has been allocated a house or not!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _isAllocatedHouse = false;
                    if (rblHouseAllocated.SelectedIndex == 0)
                        _isAllocatedHouse = true;
                    //check if we are adding some details
                    if (_isAllocatedHouse == true)
                    {
                        string _error = ValidateAddHouseDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save house details
                            //check if the staff has a house record
                            if (db.StaffHouses.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffHouse(_staffID, _isAllocatedHouse, txtHouseBlockNumber.Text.Trim(), txtHousePlotNumber.Text.Trim(), txtHouseRoad.Text.Trim(), Convert.ToDecimal(txtHouseRentAmount.Text.Trim()), ddlHouseYearOfManufacture.SelectedValue, txtHouseTenure.Text.Trim(), Convert.ToBoolean(rblHouseTenancyAgreement.SelectedValue), txtHouseComments.Text.Trim(), Convert.ToDateTime(txtTenancyAgreementEndDate.Text.Trim()).Date);
                            }
                            else
                            {
                                SaveNewHouse(_staffID, _isAllocatedHouse, txtHouseBlockNumber.Text.Trim(), txtHousePlotNumber.Text.Trim(), txtHouseRoad.Text.Trim(), Convert.ToDecimal(txtHouseRentAmount.Text.Trim()), ddlHouseYearOfManufacture.SelectedValue, txtHouseTenure.Text.Trim(), Convert.ToBoolean(rblHouseTenancyAgreement.SelectedValue), txtHouseComments.Text.Trim(), Convert.ToDateTime(txtTenancyAgreementEndDate.Text.Trim()).Date);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "House details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the house details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check  if the staff has a vehicle record
                            if (db.StaffHouses.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffHouse(_staffID, _isAllocatedHouse, null, null, null, null, null, null, null, null,null);
                            }
                            else
                            {
                                SaveNewHouse(_staffID, _isAllocatedHouse, null, null, null, null, null, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "House details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }
        }
        //save house details
        protected void lnkBtnSaveHouse_Click(object sender, EventArgs e)
        {
            SaveHouseDetails();//save house details
            return;
        }
        //save new house 
        private void SaveNewHouse(Guid _staffID, Boolean _IsAllocatedHouse, string _blockNumber, string _plotNumber, string _houseRoad, decimal? _rentAmount, string _yearOfManufacture, string _tenure, Boolean? _tenancyAgreementSigned, string _comments, DateTime? _TenancyAgreementEndDate)
        {
            StaffHouse newHouse = new StaffHouse();
            newHouse.StaffID = _staffID;
            newHouse.IsStaffAssigned = _IsAllocatedHouse;
            newHouse.BlockNumber = _blockNumber;
            newHouse.PlotNumber = _plotNumber;
            newHouse.HouseRoad = _houseRoad;
            newHouse.RentAmount = _rentAmount;
            newHouse.YearOfManufacture = _yearOfManufacture;
            newHouse.Tenure = _tenure;
            newHouse.TenancyAgreementSigned = _tenancyAgreementSigned;
            newHouse.Comments = _comments;
            newHouse.TenancyAgreementEndDate = _TenancyAgreementEndDate;
            db.StaffHouses.InsertOnSubmit(newHouse);
            db.SubmitChanges();
        }
        //update staff house details
        private void UpdateStaffHouse(Guid _staffID, Boolean _IsAllocatedHouse, string _blockNumber, string _plotNumber, string _houseRoad, decimal? _rentAmount, string _yearOfManufacture, string _tenure, Boolean? _tenancyAgreementSigned, string _comments, DateTime? _TenancyAgreementEndDate)
        {
            StaffHouse updateHouse = db.StaffHouses.Single(p => p.StaffID == _staffID);
            updateHouse.IsStaffAssigned = _IsAllocatedHouse;
            updateHouse.BlockNumber = _blockNumber;
            updateHouse.PlotNumber = _plotNumber;
            updateHouse.HouseRoad = _houseRoad;
            updateHouse.RentAmount = _rentAmount;
            updateHouse.YearOfManufacture = _yearOfManufacture;
            updateHouse.Tenure = _tenure;
            updateHouse.TenancyAgreementSigned = _tenancyAgreementSigned;
            updateHouse.Comments = _comments;
            updateHouse.TenancyAgreementEndDate = _TenancyAgreementEndDate;
            db.SubmitChanges();
        }
        //load staff house details
        private void LoadStaffHouseDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffHouses.Any(p => p.StaffID == _staffID))
                {
                    StaffHouse getStaffHouse = db.StaffHouses.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffHouse.IsStaffAssigned == true)
                        rblHouseAllocated.SelectedIndex = 0;
                    else rblHouseAllocated.SelectedIndex = 0;
                    txtHouseBlockNumber.Text = getStaffHouse.BlockNumber;
                    txtHousePlotNumber.Text = getStaffHouse.PlotNumber;
                    txtHouseRoad.Text = getStaffHouse.HouseRoad;
                    txtHouseRentAmount.Text = getStaffHouse.RentAmount.ToString();
                    ddlHouseYearOfManufacture.SelectedValue = getStaffHouse.YearOfManufacture;
                    txtHouseTenure.Text = getStaffHouse.Tenure;
                    rblHouseTenancyAgreement.SelectedValue = Convert.ToString(getStaffHouse.TenancyAgreementSigned);
                    txtHouseComments.Text = getStaffHouse.Comments;
                    txtTenancyAgreementEndDate.Text = _hrClass.ShortDateDayStart(getStaffHouse.TenancyAgreementEndDate.ToString());
                    PanelAddHouseDetails.Enabled = true;
                    // if (Convert.ToBoolean(getStaffHouse.IsStaffAllocatedHouse) == true)
                    if ((bool)getStaffHouse.IsStaffAssigned == true)
                    {

                        PanelAddHouseDetails.Enabled = true;
                        foreach (Control _control in PanelAddHouseDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    //else if(Convert.ToBoolean(getStaffHouse.IsStaffAllocatedHouse) ==false)
                    else
                    {
                        PanelAddHouseDetails.Enabled = false;
                        foreach (Control _control in PanelAddHouseDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddHouseDetails);
                    }
                }
                else
                {
                    PanelAddHouseDetails.Enabled = false;
                    foreach (Control _control in PanelAddHouseDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddHouseDetails);
                    rblHouseAllocated.SelectedIndex = -1;
                }

            }
            catch { }
        }
        #endregion
        #region TELEPHONE DETAILS
        //enable the panel for adding telephone details
        protected void rblTelephoneAssigned_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblTelephoneAssigned.SelectedIndex == 0)
            {
                PanelAddTelephoneDetails.Enabled = true;
                foreach (Control _control in PanelAddTelephoneDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddTelephoneDetails.Enabled = false;
                foreach (Control _control in PanelAddTelephoneDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add telephone details
        private string ValidateAddTelephoneDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the telephone details against!";
            else if (txtTelephoneType.Text.Trim() == "")
                return "Enter telephone type!";
            else if (txtTelephoneMake.Text.Trim() == "")
                return "Enter telephone make!";
            else if (txtTelephoneSerialNumber.Text.Trim() == "")
                return "Enter telephone serial number!";
            else if (ddlTelephoneYearOfManufacture.SelectedIndex == 0)
                return "Select telephone year of manufacture!";
            else return "";
        }
        //method for saving telephone details
        private void SaveTelephoneDetails()
        {
            try
            {
                if (rblTelephoneAssigned.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the employee has been assigned a telephone or not!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _isAssignedTelephone = false;
                    if (rblTelephoneAssigned.SelectedIndex == 0)
                        _isAssignedTelephone = true;
                    //check if we are adding some details
                    if (_isAssignedTelephone == true)
                    {
                        string _error = ValidateAddTelephoneDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save telephone details
                            //check if the staff has a telephone record
                            if (db.StaffTelephones.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffTelephone(_staffID, _isAssignedTelephone, txtTelephoneType.Text.Trim(), txtTelephoneMake.Text.Trim(), txtTelephoneSerialNumber.Text.Trim(), txtTelephoneOtherAccessories.Text.Trim(), ddlTelephoneYearOfManufacture.SelectedValue, txtTelephoneComments.Text.Trim());
                            }
                            else
                            {
                                SaveNewTelephone(_staffID, _isAssignedTelephone, txtTelephoneType.Text.Trim(), txtTelephoneMake.Text.Trim(), txtTelephoneSerialNumber.Text.Trim(), txtTelephoneOtherAccessories.Text.Trim(), ddlTelephoneYearOfManufacture.SelectedValue, txtTelephoneComments.Text.Trim());
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Telephone details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the telephone details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check  if the staff has a telephone record
                            if (db.StaffTelephones.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffTelephone(_staffID, _isAssignedTelephone, null, null, null, null, null, null);
                            }
                            else
                            {
                                SaveNewTelephone(_staffID, _isAssignedTelephone, null, null, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Telephone details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }
        }
        //save telephone details
        protected void lnkBtnSaveTelephone_Click(object sender, EventArgs e)
        {
            SaveTelephoneDetails();//save telephone details
            return;
        }
        //save new telephone 
        private void SaveNewTelephone(Guid _staffID, Boolean _IsAssignedTelephone, string _typeOfTelephone, string _phoneMake, string _serialNumber, string _otherAccessories, string _yearOfManufacture, string _comments)
        {
            StaffTelephone newTelephone = new StaffTelephone();
            newTelephone.StaffID = _staffID;
            newTelephone.IsStaffAssigned = _IsAssignedTelephone;
            newTelephone.TypeOfPhone = _typeOfTelephone;
            newTelephone.PhoneMake = _phoneMake;
            newTelephone.SerialNumber = _serialNumber;
            newTelephone.OtherAccessories = _otherAccessories;
            newTelephone.YearOfManufacture = _yearOfManufacture;
            newTelephone.Comments = _comments;
            db.StaffTelephones.InsertOnSubmit(newTelephone);
            db.SubmitChanges();
        }
        //update staff house details
        private void UpdateStaffTelephone(Guid _staffID, Boolean _IsAssignedTelephone, string _typeOfTelephone, string _phoneMake, string _serialNumber, string _otherAccessories, string _yearOfManufacture, string _comments)
        {
            StaffTelephone updateTelephone = db.StaffTelephones.Single(p => p.StaffID == _staffID);
            updateTelephone.IsStaffAssigned = _IsAssignedTelephone;
            updateTelephone.TypeOfPhone = _typeOfTelephone;
            updateTelephone.PhoneMake = _phoneMake;
            updateTelephone.SerialNumber = _serialNumber;
            updateTelephone.OtherAccessories = _otherAccessories;
            updateTelephone.YearOfManufacture = _yearOfManufacture;
            updateTelephone.Comments = _comments;
            db.SubmitChanges();
        }
        //load staff telephone details
        private void LoadStaffTelephoneDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffTelephones.Any(p => p.StaffID == _staffID))
                {
                    StaffTelephone getStaffTelephone = db.StaffTelephones.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffTelephone.IsStaffAssigned == true)
                        rblTelephoneAssigned.SelectedIndex = 0;
                    else
                        rblTelephoneAssigned.SelectedIndex = 1;
                    txtTelephoneType.Text = getStaffTelephone.TypeOfPhone;
                    txtTelephoneMake.Text = getStaffTelephone.PhoneMake;
                    txtTelephoneSerialNumber.Text = getStaffTelephone.SerialNumber;
                    ddlTelephoneYearOfManufacture.SelectedValue = getStaffTelephone.YearOfManufacture;
                    txtTelephoneOtherAccessories.Text = getStaffTelephone.OtherAccessories;
                    txtTelephoneComments.Text = getStaffTelephone.Comments;
                    PanelAddTelephoneDetails.Enabled = true;
                    if ((bool)getStaffTelephone.IsStaffAssigned == true)
                    {
                        PanelAddTelephoneDetails.Enabled = true;
                        foreach (Control _control in PanelAddTelephoneDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    else
                    {
                        PanelAddTelephoneDetails.Enabled = false;
                        foreach (Control _control in PanelAddTelephoneDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddTelephoneDetails);
                    }
                }
                else
                {
                    PanelAddTelephoneDetails.Enabled = false;
                    foreach (Control _control in PanelAddTelephoneDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddTelephoneDetails);
                    rblTelephoneAssigned.SelectedIndex = -1;
                }

            }
            catch { }
        }
        #endregion
        #region LAPTOP DETAILS
        //enable the panel for adding laptop details
        protected void rblLaptopAssigned_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblLaptopAssigned.SelectedIndex == 0)
            {
                PanelAddLaptopDetails.Enabled = true;
                foreach (Control _control in PanelAddLaptopDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddLaptopDetails.Enabled = false;
                foreach (Control _control in PanelAddLaptopDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add laptop details
        private string ValidateAddLaptopDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the laptop details against!";
            else if (txtLaptopMake.Text.Trim() == "")
                return "Enter laptop make!";
            else if (txtLaptopSerialNumber.Text.Trim() == "")
                return "Enter laptop serial number!";
            else if (ddlLaptopYearOfManufacture.SelectedIndex == 0)
                return "Select laptop year of manufacture!";
            else return "";
        }
        //method for saving laptop details
        private void SaveLaptopDetails()
        {
            try
            {
                if (rblLaptopAssigned.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the employee has been assigned a laptop or not!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _isAssignedLaptop = false;
                    if (rblLaptopAssigned.SelectedIndex == 0)
                        _isAssignedLaptop = true;
                    //check if we are adding some details
                    if (_isAssignedLaptop == true)
                    {
                        string _error = ValidateAddLaptopDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save laptop details
                            //check if the staff has a laptop record
                            if (db.StaffLaptops.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffLaptop(_staffID, _isAssignedLaptop, txtLaptopMake.Text.Trim(), txtLaptopSerialNumber.Text.Trim(), ddlLaptopYearOfManufacture.SelectedValue, txtLaptopOtherAccessories.Text.Trim(), txtLaptopComments.Text.Trim());
                            }
                            else
                            {
                                SaveNewLaptop(_staffID, _isAssignedLaptop, txtLaptopMake.Text.Trim(), txtLaptopSerialNumber.Text.Trim(), ddlLaptopYearOfManufacture.SelectedValue, txtLaptopOtherAccessories.Text.Trim(), txtLaptopComments.Text.Trim());
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Laptop details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the laptop details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check if the staff has a laptop record
                            if (db.StaffLaptops.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffLaptop(_staffID, _isAssignedLaptop, null, null, null, null, null);
                            }
                            else
                            {
                                SaveNewLaptop(_staffID, _isAssignedLaptop, null, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Laptop details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }
        }
        //save laptop details
        protected void lnkBtnSaveLaptop_Click(object sender, EventArgs e)
        {
            SaveLaptopDetails();//save laptop details
            return;
        }
        //save new laptop 
        private void SaveNewLaptop(Guid _staffID, Boolean _IsAssignedLaptop, string _laptopMake, string _serialNumber, string _yearOfManufacture, string _otherAccessories, string _comments)
        {
            StaffLaptop newLaptop = new StaffLaptop();
            newLaptop.StaffID = _staffID;
            newLaptop.IsStaffAssigned = _IsAssignedLaptop;
            newLaptop.LaptopMake = _laptopMake;
            newLaptop.SerialNumber = _serialNumber;
            newLaptop.YearOfManufacture = _yearOfManufacture;
            newLaptop.OtherAccessories = _otherAccessories;
            newLaptop.Comments = _comments;
            db.StaffLaptops.InsertOnSubmit(newLaptop);
            db.SubmitChanges();
        }
        //update staff house details
        private void UpdateStaffLaptop(Guid _staffID, Boolean _IsAssignedLaptop, string _laptopMake, string _serialNumber, string _yearOfManufacture, string _otherAccessories, string _comments)
        {
            StaffLaptop updateLaptop = db.StaffLaptops.Single(p => p.StaffID == _staffID);
            updateLaptop.IsStaffAssigned = _IsAssignedLaptop;
            updateLaptop.LaptopMake = _laptopMake;
            updateLaptop.SerialNumber = _serialNumber;
            updateLaptop.YearOfManufacture = _yearOfManufacture;
            updateLaptop.OtherAccessories = _otherAccessories;
            updateLaptop.Comments = _comments;
            db.SubmitChanges();
        }
        //load staff telephone details
        private void LoadStaffLaptopDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffLaptops.Any(p => p.StaffID == _staffID))
                {
                    StaffLaptop getStaffLaptop = db.StaffLaptops.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffLaptop.IsStaffAssigned == true)
                        rblLaptopAssigned.SelectedIndex = 0;
                    else rblLaptopAssigned.SelectedIndex = 1;
                    txtLaptopMake.Text = getStaffLaptop.LaptopMake;
                    txtLaptopSerialNumber.Text = getStaffLaptop.SerialNumber;
                    ddlLaptopYearOfManufacture.SelectedValue = getStaffLaptop.YearOfManufacture;
                    txtLaptopOtherAccessories.Text = getStaffLaptop.OtherAccessories;
                    txtLaptopComments.Text = getStaffLaptop.Comments;
                    PanelAddLaptopDetails.Enabled = true;
                    if ((bool)getStaffLaptop.IsStaffAssigned == true)
                    {
                        PanelAddLaptopDetails.Enabled = true;
                        foreach (Control _control in PanelAddLaptopDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    else
                    {
                        PanelAddLaptopDetails.Enabled = false;
                        foreach (Control _control in PanelAddLaptopDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddLaptopDetails);
                    }
                }
                else
                {
                    PanelAddLaptopDetails.Enabled = false;
                    foreach (Control _control in PanelAddLaptopDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddLaptopDetails);
                    rblLaptopAssigned.SelectedIndex = -1;
                }
            }
            catch { }
        }
        #endregion

        #region ASSET FINANCED DETAILS
        //enable the panel for adding asset financed details
        protected void rblAssetFinanced_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblAssetFinanced.SelectedIndex == 0)
            {
                PanelAddAssetFinancedDetails.Enabled = true;
                foreach (Control _control in PanelAddAssetFinancedDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddAssetFinancedDetails.Enabled = false;
                foreach (Control _control in PanelAddAssetFinancedDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add asset financed details
        private string ValidateAddAssetFinancedDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record asset financed details against!";
            else if (txtAssetFinanced.Text.Trim() == "")
                return "Enter asset financed!";
            else if (txtAssetFinancedDate.Text.Trim() == "__/___/____")
                return "Enter asset financed date!";
            else if (_hrClass.isDateValid(txtAssetFinancedDate.Text.Trim()) == false)
                return "Invalid asset financed date!";
            else if (txtAssetFinancedAmount.Text.Trim() == "")
                return "Enter amount financed!";
            else if (_hrClass.IsCurrencyValid(txtAssetFinancedAmount.Text.Trim()) == false)
                return "Enter a valid numeric value for amount financed!";
            else if (txtAssetFinancedDuration.Text.Trim() == "")
                return "Enter asset finance duration!";
            else if (txtAssetFinancedInterestCharged.Text.Trim() == "")
                return "Enter interest charged!";
            else if (_hrClass.IsCurrencyValid(txtAssetFinancedInterestCharged.Text.Trim()) == false)
                return "Enter a valid numeric value for the interest charged!";
            else return "";
        }
        //method for saving asset financed details
        private void SaveAssetFinancedDetails()
        {
            try
            {
                if (rblAssetFinanced.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the employee has a financed asset or not!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _isAssetFinanced = false;
                    if (rblAssetFinanced.SelectedIndex == 0)
                        _isAssetFinanced = true;
                    //check if we are adding some details
                    if (_isAssetFinanced == true)
                    {
                        string _error = ValidateAddAssetFinancedDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save asset financed details
                            //check if the staff has a asset financed record
                            if (db.StaffAssetFinanceds.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffAssetFinanced(_staffID, _isAssetFinanced, txtAssetFinanced.Text.Trim(), Convert.ToDateTime(txtAssetFinancedDate.Text.Trim()), Convert.ToDecimal(txtAssetFinancedAmount.Text.Trim()), txtAssetFinancedDuration.Text.Trim(), Convert.ToDecimal(txtAssetFinancedInterestCharged.Text.Trim()), txtAssetFinancedComments.Text.Trim());
                            }
                            else
                            {
                                SaveNewAssetFinanced(_staffID, _isAssetFinanced, txtAssetFinanced.Text.Trim(), Convert.ToDateTime(txtAssetFinancedDate.Text.Trim()), Convert.ToDecimal(txtAssetFinancedAmount.Text.Trim()), txtAssetFinancedDuration.Text.Trim(), Convert.ToDecimal(txtAssetFinancedInterestCharged.Text.Trim()), txtAssetFinancedComments.Text.Trim());
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Asset financed details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the asset financed details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check  if the staff has a asset financed record
                            if (db.StaffAssetFinanceds.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffAssetFinanced(_staffID, _isAssetFinanced, null, null, null, null, null, null);
                            }
                            else
                            {
                                SaveNewAssetFinanced(_staffID, _isAssetFinanced, null, null, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Asset financed details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }
        }
        //save asset financed details
        protected void lnkBtnSaveAssetFinanced_Click(object sender, EventArgs e)
        {
            SaveAssetFinancedDetails();//save asset financed details
            return;
        }
        //save new asset financed 
        private void SaveNewAssetFinanced(Guid _staffID, Boolean _IsAssetFinanced, string _assetFinanced, DateTime? _dateFinanced, decimal? _amountFinanced, string _duration, decimal? _interestCharged, string _comments)
        {
            StaffAssetFinanced newAssetFinanced = new StaffAssetFinanced();
            newAssetFinanced.StaffID = _staffID;
            newAssetFinanced.IsStaffFinanced = _IsAssetFinanced;
            newAssetFinanced.AssetFinanced = _assetFinanced;
            newAssetFinanced.Date = _dateFinanced;
            newAssetFinanced.AmountFinanced = _amountFinanced;
            newAssetFinanced.Duration = _duration;
            newAssetFinanced.InterestCharged = _interestCharged;
            newAssetFinanced.Comments = _comments;
            db.StaffAssetFinanceds.InsertOnSubmit(newAssetFinanced);
            db.SubmitChanges();
        }
        //update staff house details
        private void UpdateStaffAssetFinanced(Guid _staffID, Boolean _IsAssetFinanced, string _assetFinanced, DateTime? _dateFinanced, decimal? _amountFinanced, string _duration, decimal? _interestCharged, string _comments)
        {
            StaffAssetFinanced updateAssetFinanced = db.StaffAssetFinanceds.Single(p => p.StaffID == _staffID);
            updateAssetFinanced.IsStaffFinanced = _IsAssetFinanced;
            updateAssetFinanced.AssetFinanced = _assetFinanced;
            updateAssetFinanced.Date = _dateFinanced;
            updateAssetFinanced.AmountFinanced = _amountFinanced;
            updateAssetFinanced.Duration = _duration;
            updateAssetFinanced.InterestCharged = _interestCharged;
            updateAssetFinanced.Comments = _comments;
            db.SubmitChanges();
        }
        //load staff asset financed details
        private void LoadStaffAssetFinancedDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffAssetFinanceds.Any(p => p.StaffID == _staffID && p.IsStaffFinanced == true))
                {
                    StaffAssetFinanced getStaffAssetFinanced = db.StaffAssetFinanceds.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffAssetFinanced.IsStaffFinanced == true)
                        rblAssetFinanced.SelectedIndex = 0;
                    else rblAssetFinanced.SelectedIndex = 1;
                    txtAssetFinanced.Text = getStaffAssetFinanced.AssetFinanced;
                    txtAssetFinancedDate.Text = _hrClass.ShortDateDayStart(getStaffAssetFinanced.Date.ToString());
                    txtAssetFinancedAmount.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaffAssetFinanced.AmountFinanced);
                    txtAssetFinancedDuration.Text = getStaffAssetFinanced.Duration;
                    txtAssetFinancedInterestCharged.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaffAssetFinanced.InterestCharged).Replace(",", ""); ;
                    txtAssetFinancedComments.Text = getStaffAssetFinanced.Comments;
                    PanelAddAssetFinancedDetails.Enabled = true;

                    if ((bool)getStaffAssetFinanced.IsStaffFinanced == true)
                    {
                        PanelAddAssetFinancedDetails.Enabled = true;
                        foreach (Control _control in PanelAddAssetFinancedDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    else
                    {
                        PanelAddAssetFinancedDetails.Enabled = false;
                        foreach (Control _control in PanelAddAssetFinancedDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddAssetFinancedDetails);
                    }
                }
                else
                {
                    PanelAddAssetFinancedDetails.Enabled = false;
                    foreach (Control _control in PanelAddAssetFinancedDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddAssetFinancedDetails);
                    rblAssetFinanced.SelectedIndex = -1;
                }

            }
            catch { }
        }

        #endregion

        #region LOAN INFORMATION DETAILS
        //enable the panel for adding loan information details
        protected void rblDoesStaffHaveALoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblDoesStaffHaveALoan.SelectedIndex == 0)
            {
                PanelAddLoanInformationDetails.Enabled = true;
                foreach (Control _control in PanelAddLoanInformationDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = false;
                    }
                }
            }
            else
            {
                PanelAddLoanInformationDetails.Enabled = false;
                foreach (Control _control in PanelAddLoanInformationDetails.Controls)
                {
                    if (_control.GetType() == typeof(TextBox))
                    {
                        ((TextBox)(_control)).ReadOnly = true;
                    }
                }
            }
        }
        //validate add loan information details
        private string ValidateAddLoanInformationDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record loan information details against!";
            else if (txtLoanAmount.Text.Trim() == "")
                return "Enter loan amount!";
            else if (_hrClass.IsCurrencyValid(txtLoanAmount.Text.Trim()) == false)
                return "Enter a valid numeric value for the loan amount!";
            else if (txtLoanDate.Text.Trim() == "__/___/____")
                return "Enter loan date!";
            else if (_hrClass.isDateValid(txtLoanDate.Text.Trim()) == false)
                return "Invalid loan date!";
            else if (txtLoanDuration.Text.Trim() == "")
                return "Enter loan duration!";
            else return "";
        }
        //method for saving loan information details
        private void SaveLoanInformationDetails()
        {
            try
            {

                if (rblDoesStaffHaveALoan.SelectedIndex == -1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select whether the employee has a loan or not!", this, PanelStaffAssets);
                    return;
                }
                else
                {
                    Boolean _doesStaffHasALoan = false;
                    if (rblDoesStaffHaveALoan.SelectedIndex == 0)
                        _doesStaffHasALoan = true;
                    //check if we are adding some details
                    if (_doesStaffHasALoan == true)
                    {
                        string _error = ValidateAddLoanInformationDetailsControls();//check for errors
                        if (_error != "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //save loan information details
                            //check if the staff has a loan information record
                            if (db.StaffLoans.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffLoan(_staffID, _doesStaffHasALoan, Convert.ToDecimal(txtLoanAmount.Text.Trim()), Convert.ToDateTime(txtLoanDate.Text.Trim()), txtLoanDuration.Text.Trim(), txtLoanComments.Text.Trim());
                            }
                            else
                            {
                                SaveNewLoan(_staffID, _doesStaffHasALoan, Convert.ToDecimal(txtLoanAmount.Text.Trim()), Convert.ToDateTime(txtLoanDate.Text.Trim()), txtLoanDuration.Text.Trim(), txtLoanComments.Text.Trim());
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Loan information details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                    else//adding no details
                    {
                        if (HiddenFieldStaffID.Value == "")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Save failed. Search for the employee that you want to record the loan information details against!", this, PanelStaffAssets);
                            return;
                        }
                        else
                        {
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            //check  if the staff has a loan information record
                            if (db.StaffLoans.Any(p => p.StaffID == _staffID))
                            {
                                UpdateStaffLoan(_staffID, _doesStaffHasALoan, null, null, null, null);
                            }
                            else
                            {
                                SaveNewLoan(_staffID, _doesStaffHasALoan, null, null, null, null);
                            }
                            _hrClass.LoadHRManagerMessageBox(2, "Loan information details have been saved!", this, PanelStaffAssets);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured. " + ex.Message.ToString() + "!", this, PanelStaffAssets);
                return;
            }

        }
        //save loan information details
        protected void lnkBtnSaveLoanDetails_Click(object sender, EventArgs e)
        {
            SaveLoanInformationDetails();//save loan information details
            return;
        }
        //save new loan information 
        private void SaveNewLoan(Guid _staffID, Boolean _doesStaffHasLoan, decimal? _loanAmount, DateTime? _loanDate, string _loanDuration, string _comments)
        {
            StaffLoan newLoan = new StaffLoan();
            newLoan.StaffID = _staffID;
            newLoan.StaffHasALoan = _doesStaffHasLoan;
            newLoan.LoanAmount = _loanAmount;
            newLoan.LoanDate = _loanDate;
            newLoan.LoanDuration = _loanDuration;
            newLoan.Comments = _comments;
            db.StaffLoans.InsertOnSubmit(newLoan);
            db.SubmitChanges();
        }
        //update staff house details
        private void UpdateStaffLoan(Guid _staffID, Boolean _doesStaffHasLoan, decimal? _loanAmount, DateTime? _loanDate, string _loanDuration, string _comments)
        {
            StaffLoan updateLoan = db.StaffLoans.Single(p => p.StaffID == _staffID);
            updateLoan.StaffHasALoan = _doesStaffHasLoan;
            updateLoan.LoanAmount = _loanAmount;
            updateLoan.LoanDate = _loanDate;
            updateLoan.LoanDuration = _loanDuration;
            updateLoan.Comments = _comments;
            db.SubmitChanges();
        }
        //load staff loan information details
        private void LoadStaffLoanInformationDetails(Guid _staffID)
        {
            try
            {
                if (db.StaffLoans.Any(p => p.StaffID == _staffID && p.StaffHasALoan == true))
                {
                    StaffLoan getStaffLoan = db.StaffLoans.Single(p => p.StaffID == _staffID);
                    if ((bool)getStaffLoan.StaffHasALoan == true)
                        rblDoesStaffHaveALoan.SelectedIndex = 0;
                    else rblDoesStaffHaveALoan.SelectedIndex = 1;
                    txtLoanAmount.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaffLoan.LoanAmount);
                    txtLoanDate.Text = _hrClass.ShortDateDayStart(getStaffLoan.LoanDate.ToString());
                    txtLoanDuration.Text = getStaffLoan.LoanDuration;
                    txtLoanComments.Text = getStaffLoan.Comments;
                    PanelAddLoanInformationDetails.Enabled = true;
                    if ((bool)getStaffLoan.StaffHasALoan == true)
                    {
                        PanelAddLoanInformationDetails.Enabled = true;
                        foreach (Control _control in PanelAddLoanInformationDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = false;
                            }
                        }
                    }
                    else
                    {
                        PanelAddLoanInformationDetails.Enabled = false;
                        foreach (Control _control in PanelAddLoanInformationDetails.Controls)
                        {
                            if (_control.GetType() == typeof(TextBox))
                            {
                                ((TextBox)(_control)).ReadOnly = true;
                            }
                        }
                        _hrClass.ClearEntryControls(PanelAddLoanInformationDetails);
                    }
                }
                else
                {
                    PanelAddLoanInformationDetails.Enabled = false;
                    foreach (Control _control in PanelAddLoanInformationDetails.Controls)
                    {
                        if (_control.GetType() == typeof(TextBox))
                        {
                            ((TextBox)(_control)).ReadOnly = true;
                        }
                    }
                    _hrClass.ClearEntryControls(PanelAddLoanInformationDetails);
                    rblDoesStaffHaveALoan.SelectedIndex = -1;
                }

            }
            catch { }
        }
        #endregion
    }
}