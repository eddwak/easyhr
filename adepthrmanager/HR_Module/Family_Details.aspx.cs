﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;

namespace AdeptHRManager.HR_Module
{
    public partial class Family_Details : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
                _hrClass.GetListingItems(ddlChildCountryOfResidence, 1000, "Country of Residence");//display child country of residence(1000) available
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffFamilyDetails(_staffID);//load family details
                    return;
                }
            }
            catch { }
        }
        //load family details
        private void DisplayStaffFamilyDetails(Guid _staffID)
        {
            DisplayStaffSpouse();//display spouse details
            ClearAddSpouseDetailsControls();
            DisplayStaffChildren();//display chid details
            ClearAddChildDetailsControls();
            DisplayStaffOtherDependants();//display other dependants
            ClearAddOtherDependantDetailsControls();
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffFamilyDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffFamilyDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelFamilyDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelFamilyDetails);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffFamilyDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffFamilyDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelFamilyDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelFamilyDetails);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffFamilyDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffFamilyDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelFamilyDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelFamilyDetails);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffFamilyDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffFamilyDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelFamilyDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelFamilyDetails);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //load add new detail
        protected void LinkButtonNew_OnClick(object sender, EventArgs e)
        {
            if (TabContainerFamilyDetails.ActiveTabIndex == 0)//spouse details tab
            {
                ClearAddSpouseDetailsControls();
                ModalPopupExtenderAddSpouseDetails.Show();
            }
            else if (TabContainerFamilyDetails.ActiveTabIndex == 1)//child details
            {
                ClearAddChildDetailsControls();
                ModalPopupExtenderAddChildDetails.Show();
            }
            else if (TabContainerFamilyDetails.ActiveTabIndex == 2)//other depandat relationship
                ModalPopupExtenderAddOtherDependantDetails.Show();
            return;
        }
        //load edit family details
        protected void LinkButtonEdit_OnClick(object sender, EventArgs e)
        {
            if (TabContainerFamilyDetails.ActiveTabIndex == 0)//spouse details tab
                PopulateEditSpouseControls();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 1)//child details
                PopulateEditChildControls();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 2)//other dependant relationship
                PopulateEditOtherDependantControls();
            return;
        }
        //load delete family details
        protected void LinkButtonDelete_OnClick(object sender, EventArgs e)
        {
            if (TabContainerFamilyDetails.ActiveTabIndex == 0)//spouse details tab
                LoadDeleteStaffSpouse();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 1)//child details
                LoadDeleteStaffChild();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 2)//other dependant relationship
                LoadDeleteStaffOtherDependant();
            return;
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            if (TabContainerFamilyDetails.ActiveTabIndex == 0)//spouse details tab
                DeleteStaffSpouse();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 1)//child details
                DeleteStaffChild();
            else if (TabContainerFamilyDetails.ActiveTabIndex == 2)//other dependant relationship
                DeleteStaffOtherDependant();
            return;
        }
        //validate add spouse cnotrols
        private string ValidateAddSpouseControls()
        {
            if (HiddenFieldStaffID.Value.ToString() == "")
                return "Spouse save failed. Enter employee's basic information first before adding employee's spouse details!";
            else if (txtFirstName.Text.Trim() == "")
                return "Enter  spouse's first name!";
            else if (txtSurname.Text.Trim() == "")
                return "Enter spouse's surname name!";
            else if (txtCountry.Text.Trim() == "")
                return "Enter spouse's country!";
            else if (txtNationality.Text.Trim() == "")
                return "Enter spouse's nationality!";
            else if (txtPassNo.Text.Trim() == "")
                return "Enter spouse's pass no!";
            else if (txtPassExpiryDate.Text == "__/___/____")
                return "Enter spouse's expiry date!";
            else if (txtSpouseDateOfBirth.Text == "__/___/____")
                return "Enter date of birth!";
            else if (txtSpouseDateOfBirth.Text != "__/___/____" && (Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim()).Year >= DateTime.Now.Year || Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim()).Year > DateTime.Now.Year - 18))
                return "Invalid employee's spouse Date of Birth selected. Spouse date of birth cannot be current date or in future or less than 18 years. Select a valid date of birth for the spouse!";
            else if (txtSpouseTelephoneNumber.Text.Trim() == "")
                return "Enter spouse's telephone number!";
            else if (txtSpouseEmailID.Text.Trim() != "" && _hrClass.IsValidEmail(txtSpouseEmailID.Text.Trim()) == false)
                return "Invalid spouse email address entered! Check the email address entered!";
            else if (rblSpouseMarriageCertificateProvided.SelectedIndex == -1)
                return "Select whether the marriage certificate has been provided or not!";
            return "";
        }
        //save employee's spouse details
        private void SaveSpouseDetails()
        {
            ModalPopupExtenderAddSpouseDetails.Show();
            try
            {
                string _error = ValidateAddSpouseControls();//check if there are any errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
               
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    //save spouse
                    Spouse newSpouse = new Spouse();
                    newSpouse.spouse_uiStaffID = _staffID;
                    newSpouse.spouse_vFirstName = txtFirstName.Text.Trim();
                    newSpouse.spouse_vSurname = txtSurname.Text.Trim();
                    newSpouse.spouse_vCountry = txtSurname.Text.Trim();
                    newSpouse.spouse_vNationality = txtNationality.Text.Trim();
                    newSpouse.spouse_vPassNo = txtPassNo.Text.Trim();
                    newSpouse.spouse_vPassExpiryDate = txtPassExpiryDate.Text.Trim();
                    newSpouse.spouse_dtDOB = Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim());
                    newSpouse.spouse_vEmployedAt = txtSpouseEmployedAt.Text.Trim();
                    newSpouse.spouse_vEmploymentDetails = txtSpouseEmploymentDetails.Text.Trim();
                    newSpouse.spouse_vPhoneNumber = txtSpouseTelephoneNumber.Text.Trim();
                    newSpouse.spouse_vEmailID = txtSpouseEmailID.Text.Trim();
                    newSpouse.spouse_bIsMedicalCovered = false;//set false for medical coverage
                    newSpouse.spouse_vPostalAddress = txtSpousePostalAddress.Text.Trim();
                    newSpouse.spouse_bMariageCerticateProvided = Convert.ToBoolean(rblSpouseMarriageCertificateProvided.SelectedValue);
                    newSpouse.spouse_vComments = txtSpouseComments.Text.Trim();
                    db.Spouses.InsertOnSubmit(newSpouse);
                    db.SubmitChanges();
                    HiddenFieldSpouseID.Value = newSpouse.spouse_iSpouseID.ToString();
                    _hrClass.LoadHRManagerMessageBox(2, "Employee's spouse details have been successfully saved!", this, PanelFamilyDetails);
                    lnkBtnUpdateSpouse.Enabled = true;
                    ModalPopupExtenderAddSpouseDetails.Hide();
                    //display employee's spouses
                    DisplayStaffSpouse();
                    //display spouses to see who wil benefit from the medical scheme
                    //DisplaySpouseForMedicalBenefit();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //update employee's spouse details
        private void UpdateSpouseDetails(short SpouseID)
        {
            ModalPopupExtenderAddSpouseDetails.Show();
            try
            {
                string _error = ValidateAddSpouseControls();//check if there are any errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
               
                else
                {
                    //update spouse details
                    Spouse updateSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == SpouseID);
                    updateSpouse.spouse_vFirstName = txtFirstName.Text.Trim();
                    updateSpouse.spouse_vSurname = txtSurname.Text.Trim();
                    updateSpouse.spouse_vCountry = txtCountry.Text.Trim();
                    updateSpouse.spouse_vNationality = txtNationality.Text.Trim();
                    updateSpouse.spouse_vPassNo = txtPassNo.Text.Trim();
                    updateSpouse.spouse_vPassExpiryDate = txtPassExpiryDate.Text.Trim();
                    updateSpouse.spouse_dtDOB = Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim());
                    updateSpouse.spouse_vEmployedAt = txtSpouseEmployedAt.Text.Trim();
                    updateSpouse.spouse_vEmploymentDetails = txtSpouseEmploymentDetails.Text.Trim();
                    updateSpouse.spouse_vPhoneNumber = txtSpouseTelephoneNumber.Text.Trim();
                    updateSpouse.spouse_vEmailID = txtSpouseEmailID.Text.Trim();
                    updateSpouse.spouse_vPostalAddress = txtSpousePostalAddress.Text.Trim();
                    updateSpouse.spouse_bMariageCerticateProvided = Convert.ToBoolean(rblSpouseMarriageCertificateProvided.SelectedValue);
                    updateSpouse.spouse_vComments = txtSpouseComments.Text.Trim();
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "Spouse details have been successfully updated!", this, PanelFamilyDetails);
                    ModalPopupExtenderAddSpouseDetails.Hide();
                    DisplayStaffSpouse();//display employee's spouse details
                    //display spouses to see who wil benefit from the medical scheme
                    //DisplaySpouseForMedicalBenefit();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //save spouse details
        protected void btnSaveSpouse_OnClick(object sender, EventArgs e)
        {
            SaveSpouseDetails();
            return;
        }
        //update spouse details
        protected void btnUpdateSpouse_OnClick(object sender, EventArgs e)
        {
            UpdateSpouseDetails(Convert.ToInt16(HiddenFieldSpouseID.Value));
            return;
        }
        protected void lnkBtnClearSpouse_OnClick(object sender, EventArgs e)
        {
            ClearAddSpouseDetailsControls();
            ModalPopupExtenderAddSpouseDetails.Show();
            return;
        }
        //display staff spouse details
        private void DisplayStaffSpouse()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                _display = db.PROC_StaffSpouse(_staffID);
                gvSpouseDetails.DataSourceID = null;
                gvSpouseDetails.DataSource = _display;
                gvSpouseDetails.DataBind();
                return;
            }
            catch { }
        }
        //load spouse details for edit when the edit button is clicked fron the grid view, or delete when the delete button is clicked
        protected void gvSpouseDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditSpouse") == 0 || e.CommandName.CompareTo("DeleteSpouse") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    HiddenField _HFSpouseID = (HiddenField)gvSpouseDetails.Rows[ID].FindControl("HiddenField1");
                    int _spouseID = Convert.ToInt32(_HFSpouseID.Value);
                    if (e.CommandName.CompareTo("EditSpouse") == 0)//check if we are editing
                    {
                        LoadSpouseDetailsForEdit(_spouseID);
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteSpouse") == 0)//check if we are deleting
                    {
                        Session["DeleteSpouseID"] = _spouseID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the spouse?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                        Session["FamilyDeleteCase"] = 1;//set family delete case to 1 for deleting a spouse
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        private void LoadSpouseDetailsForEdit(int _spouseID)
        {
            Spouse getSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _spouseID);
            HiddenFieldSpouseID.Value = _spouseID.ToString();
            txtFirstName.Text = getSpouse.spouse_vFirstName;
            txtSurname.Text = getSpouse.spouse_vSurname;
            txtCountry.Text = getSpouse.spouse_vCountry;
            txtNationality.Text = getSpouse.spouse_vNationality;
            txtPassNo.Text = getSpouse.spouse_vPassNo;
            txtPassExpiryDate.Text = getSpouse.spouse_vPassExpiryDate.ToString();            

            if (getSpouse.spouse_dtDOB != null)
            {
                txtSpouseDateOfBirth.Text = _hrClass.ShortDateDayStart(getSpouse.spouse_dtDOB.ToString());
            }
            txtSpouseEmployedAt.Text = getSpouse.spouse_vEmployedAt;
            txtSpouseEmploymentDetails.Text = getSpouse.spouse_vEmploymentDetails;
            txtSpouseTelephoneNumber.Text = getSpouse.spouse_vPhoneNumber;
            txtSpouseEmailID.Text = getSpouse.spouse_vEmailID;
            txtSpousePostalAddress.Text = Convert.ToString(getSpouse.spouse_vPostalAddress);
            if (getSpouse.spouse_bMariageCerticateProvided == true)//check if the marriage certifcate has been provided
                rblSpouseMarriageCertificateProvided.SelectedIndex = 0;
            else rblSpouseMarriageCertificateProvided.SelectedIndex = 1;
            txtSpouseComments.Text = Convert.ToString(getSpouse.spouse_vComments);
            lnkBtnUpdateSpouse.Enabled = true;
            lnkBtnUpdateSpouse.Enabled = true;
            lnkBtnSaveSpouse.Visible = false;
            lnkBtnClearSpouse.Visible = true;
            lnkBtnUpdateSpouse.Enabled = true;
            lbAddSpouseDetailsHeader.Text = "Edit Spouse Details";
            ImageAddSpouseDetailsHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            return;
        }
        //populate edit spouse details
        private void PopulateEditSpouseControls()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvSpouseDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a spouse record to edit!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvSpouseDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one spouse record. Select only one recoed to edit at a time!", this, PanelFamilyDetails);
                        return;
                    }
                    foreach (GridViewRow _grv in gvSpouseDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFSpouseID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit spouse details
                            LoadSpouseDetailsForEdit(Convert.ToInt32(_HFSpouseID.Value));
                            ModalPopupExtenderAddSpouseDetails.Show();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading spouse details to edit! " + ex.Message.ToString(), this, PanelFamilyDetails);
            }
        }
        //load delete staff spouse
        private void LoadDeleteStaffSpouse()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvSpouseDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a spouse record to delete!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvSpouseDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected spouse record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected spouse records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //delete staff spouse
        private void DeleteStaffSpouse()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvSpouseDetails.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFSpouseID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _spouseID = Convert.ToInt32(_HFSpouseID.Value);
                        Spouse deleteSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _spouseID);
                        db.Spouses.DeleteOnSubmit(deleteSpouse);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayStaffSpouse();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected spouse record has been deleted.", this, PanelFamilyDetails);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected spouse records have been deleted.", this, PanelFamilyDetails);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed !" + ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        ////delete employee's spouse
        //private void DeleteSpouseDetails()
        //{
        //    try
        //    {
        //        int _spouseID = Convert.ToInt32(Session["DeleteSpouseID"]);//get spouse id
        //        //delete spouse
        //        Spouse deleteSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _spouseID);
        //        db.Spouses.DeleteOnSubmit(deleteSpouse);
        //        db.SubmitChanges();
        //        //refresh spouses
        //        DisplayStaffSpouse();
        //        _hrClass.LoadHRManagerMessageBox(2, "Employee's spouse has been deleted!", this, PanelFamilyDetails);
        //        //clear add spouse details if spouse details in the add controls are details being deleted
        //        if (HiddenFieldSpouseID.Value != "" && Convert.ToInt32(HiddenFieldSpouseID.Value) == _spouseID)
        //        {
        //            ClearAddSpouseDetailsControls();
        //        }
        //        //display spouses to see who wil benefit from the medical scheme
        //        //DisplaySpouseForMedicalBenefit();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
        //        return;
        //    }
        //}
        //clear add spouse details controls
        private void ClearAddSpouseDetailsControls()
        {
            txtFirstName.Text = "";
            txtSurname.Text = "";
            txtCountry.Text = "";
            txtNationality.Text =  "";
            txtPassNo.Text = "";
            txtPassExpiryDate.Text = "";          
            txtSpouseDateOfBirth.Text = "";
            txtSpouseEmployedAt.Text = "";
            txtSpouseEmploymentDetails.Text = "";
            txtSpouseTelephoneNumber.Text = "";
            txtSpouseEmailID.Text = "";
            txtSpousePostalAddress.Text = "";
            rblSpouseMarriageCertificateProvided.SelectedIndex = -1;
            txtSpouseComments.Text = "";
            HiddenFieldSpouseID.Value = "";
            HiddenFieldSpouseID.Value = "";
            lnkBtnSaveSpouse.Visible = true;
            lnkBtnUpdateSpouse.Enabled = false;
            lnkBtnUpdateSpouse.Visible = true;
            lnkBtnClearSpouse.Visible = true;
            lbAddSpouseDetailsHeader.Text = "Add Spouse Details";
            ImageAddSpouseDetailsHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            return;
        }

        //validate add staff's child details
        private string ValidateAddStaffChildDetails()
        {
            if (HiddenFieldStaffID.Value.ToString() == "")
                return "Child save failed. Enter staff's basic information first before adding his /her children!";          
            else if (txtChildDOB.Text.Trim() == "")
                return "Select child's date of birth by clicking the date button adjacent to the field!";
            else if (txtChildFirstName.Text.Trim() == "")
                return "Enter child's First name!";
            else if (txtChildSurname.Text.Trim() == "")
                return "Enter child's surname!";
            else if (txtChildPassNo.Text.Trim() == "")
                return "Enter child's pass no!";
            else if (txtChildNationality.Text.Trim() == "")
                return "Enter child's nationality!";
            else if (txtChildPassExpiryDate.Text.Trim() == "")
                return "Enter child's pass expiry date!";
            else if (txtChildDOB.Text.Trim() != "" && Convert.ToDateTime(txtChildDOB.Text.Trim()) >= DateTime.Now)
                return "Invalid child date of birth selected!.<br>Child's date of birth cannot be current date or a future date. Select a valid date of birth for the child!";
            else if (rblChildGender.SelectedIndex == -1)
                return "Select the child's gender!";
            else if (ddlChildCountryOfResidence.SelectedIndex == 0)
                return "Select child's country of residence!";
            else if (rblChildBirthCertificateProvided.SelectedIndex == -1)
                return "Select whether the child certifcate has been provided or not?";
            return "";
        }
        //save employee's child details
        private void SaveChildDetails()
        {
            ModalPopupExtenderAddChildDetails.Show();
            try
            {
                string _error = ValidateAddStaffChildDetails();//check if any errors are found
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
             
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);//get staff master id
                    //save child details
                    Child newChild = new Child();
                    newChild.child_uiStaffID = _staffID;
                     if (txtChildDOB.Text.Trim() != "")
                    {
                        newChild.child_dtDOB = Convert.ToDateTime(txtChildDOB.Text.Trim());
                    }

                     if (txtChildPassExpiryDate.Text.Trim() != "")
                     {
                         newChild.child_vPassExpiryDate = txtChildPassExpiryDate.Text.Trim();
                     }
                    newChild.child_vGender = rblChildGender.SelectedValue.ToString();
                    newChild.child_vFirstName = txtChildFirstName.Text.Trim();
                    newChild.child_vSurname = txtChildSurname.Text.Trim();
                    newChild.child_vPassNo = txtChildSurname.Text.Trim();
                    newChild.child_vNationality = txtChildNationality.Text.Trim();
                    newChild.child_iCountryOfResidenceID = Convert.ToInt32(ddlChildCountryOfResidence.SelectedValue);
                    newChild.child_bIsMedicalCovered = false;//set medical coverage to false
                    newChild.child_bBirthCertificateProvided = Convert.ToBoolean(rblChildBirthCertificateProvided.SelectedValue);
                    db.Childs.InsertOnSubmit(newChild);
                    db.SubmitChanges();
                    HiddenFieldChildID.Value = newChild.child_iChildID.ToString();
                    lnkBtnUpdateChild.Enabled = true;
                    //display employee's children
                    ModalPopupExtenderAddChildDetails.Hide();
                    DisplayStaffChildren();
                    _hrClass.LoadHRManagerMessageBox(2, "Staff's child details has been successfully saved!", this, PanelFamilyDetails);
                    //display employee's  children to have medical benefit
                    //DisplayChildForMedicalBenefit();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //update staff's child details
        private void UpdateChildDetails(int _childID)
        {
            ModalPopupExtenderAddChildDetails.Show();
            try
            {
                string _error = ValidateAddStaffChildDetails();//check if any errors are found
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
              
                else
                {
                    //update  child details
                    Child updateChild = db.Childs.Single(p => p.child_iChildID == _childID);
                    if (txtChildDOB.Text.Trim() != "")
                    {
                        updateChild.child_dtDOB = Convert.ToDateTime(txtChildDOB.Text.Trim());
                    }
                    if (txtChildPassExpiryDate.Text.Trim() != "")
                    {
                        updateChild.child_vPassExpiryDate = txtChildPassExpiryDate.Text.Trim();
                    }

                    updateChild.child_vFirstName = txtChildFirstName.Text.Trim();
                    updateChild.child_vSurname = txtChildSurname.Text.Trim();
                    updateChild.child_vPassNo= txtChildPassNo.Text.Trim();
                    updateChild.child_vGender = rblChildGender.SelectedValue.ToString();
                    updateChild.child_vNationality = txtChildNationality.Text.Trim();
                    updateChild.child_iCountryOfResidenceID = Convert.ToInt32(ddlChildCountryOfResidence.SelectedValue);
                    updateChild.child_bBirthCertificateProvided = Convert.ToBoolean(rblChildBirthCertificateProvided.SelectedValue);
                    db.SubmitChanges();
                    //display employee's children
                    DisplayStaffChildren();
                    _hrClass.LoadHRManagerMessageBox(2, "Staff's child details has been updated!", this, PanelFamilyDetails);
                    //display employee's  children to have medical benefit
                    //DisplayChildForMedicalBenefit();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        protected void btnSaveChild_OnClick(object sender, EventArgs e)
        {
            SaveChildDetails();
            return;
        }
        protected void btnUpdateChild_OnClick(object sender, EventArgs e)
        {
            UpdateChildDetails(Convert.ToInt16(HiddenFieldChildID.Value));
            return;
        }
        protected void lnkBtnClearChild_OnClick(object sender, EventArgs e)
        {
            ClearAddChildDetailsControls();
            ModalPopupExtenderAddChildDetails.Show();
            return;
        }
        //display employee's child details
        public void DisplayStaffChildren()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                object _display;
                _display = db.PROC_StaffChildren(_staffID);
                gvChildDetails.DataSourceID = null;
                gvChildDetails.DataSource = _display;
                gvChildDetails.DataBind();
                return;
            }
            catch { }
        }

        private void LoadChildDetailsForEdit(int _childID)
        {
            Child getChild = db.Childs.Single(p => p.child_iChildID == _childID);
            HiddenFieldChildID.Value = _childID.ToString();
            if (getChild.child_dtDOB != null)
            {
                txtChildDOB.Text = _hrClass.ShortDateDayStart(getChild.child_dtDOB.ToString());
            }
           
          txtChildPassExpiryDate.Text = getChild.child_vPassExpiryDate.ToString();            
         txtChildFirstName.Text = getChild.child_vFirstName.ToString();
         txtChildSurname.Text = getChild.child_vSurname.ToString();
         txtChildNationality.Text = getChild.child_vNationality.ToString();
         txtChildPassNo.Text = getChild.child_vPassNo.ToString();
       
            if (getChild.child_vGender != null)
                rblChildGender.SelectedValue = getChild.child_vGender.ToString();
            else rblChildGender.SelectedIndex = -1;
            ddlChildCountryOfResidence.SelectedValue = getChild.child_iCountryOfResidenceID.ToString();
            if (getChild.child_bBirthCertificateProvided == true)//check if the birth certificate has been received or not
                rblChildBirthCertificateProvided.SelectedIndex = 0;
            else rblChildBirthCertificateProvided.SelectedIndex = 1;
            lnkBtnUpdateChild.Enabled = true;
            lnkBtnUpdateChild.Enabled = true;
            lnkBtnSaveChild.Visible = false;
            lnkBtnClearChild.Visible = true;
            lnkBtnUpdateChild.Enabled = true;
            lbAddChildDetailsHeader.Text = "Edit Child Details";
            ImageAddChildDetailsHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            return;
        }
        //populate edit child details
        private void PopulateEditChildControls()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvChildDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a child record to edit!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvChildDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one child record. Select only one record to edit at a time!", this, PanelFamilyDetails);
                        return;
                    }
                    foreach (GridViewRow _grv in gvChildDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit child details
                            LoadChildDetailsForEdit(Convert.ToInt32(_HFID.Value));
                            ModalPopupExtenderAddChildDetails.Show();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading child details for edit! " + ex.Message.ToString(), this, PanelFamilyDetails);
            }
        }
        //load delete staff child
        private void LoadDeleteStaffChild()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvChildDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvChildDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected child record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected children records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //delete staff child
        private void DeleteStaffChild()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvChildDetails.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFChildID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _childID = Convert.ToInt32(_HFChildID.Value);
                        Child deleteChild = db.Childs.Single(p => p.child_iChildID == _childID);
                        db.Childs.DeleteOnSubmit(deleteChild);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayStaffChildren();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected child record has been deleted.", this, PanelFamilyDetails);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected children records have been deleted.", this, PanelFamilyDetails);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed !" + ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        ////delete employee's child
        //private void DeleteChildDetails()
        //{
        //    try
        //    {

        //        int _childID = Convert.ToInt32(Session["DeleteChildID"]);//get child id
        //        //delete child
        //        Child deleteChild = db.Childs.Single(p => p.child_iChildID == _childID);
        //        db.Childs.DeleteOnSubmit(deleteChild);
        //        db.SubmitChanges();
        //        //refresh children
        //        DisplayStaffChildren();
        //        _hrClass.LoadHRManagerMessageBox(2, "Staff's child has been deleted!", this, PanelFamilyDetails);
        //        //clear add child details if child details in the add controls are details being deleted
        //        if (HiddenFieldChildID.Value != "" && Convert.ToInt32(HiddenFieldChildID.Value) == _childID)
        //        {
        //            ClearAddChildDetailsControls();
        //        }
        //        //refresh children medical benefit details
        //        //DisplayChildForMedicalBenefit();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
        //        return;
        //    }
        //}
        //clear add child details controls
        private void ClearAddChildDetailsControls()
        {
            txtChildDOB.Text = "";
            txtChildFirstName.Text = "";
            txtChildSurname.Text = "";
            txtChildPassNo.Text = "";
            txtChildPassExpiryDate.Text = "";
            txtChildNationality.Text = "";
            rblChildGender.SelectedIndex = -1;
            HiddenFieldChildID.Value = "";
            lnkBtnSaveChild.Visible = true;
            lnkBtnUpdateChild.Enabled = false;
            lnkBtnUpdateChild.Visible = true;
            lnkBtnClearChild.Visible = true;
            lbAddChildDetailsHeader.Text = "Add Child Details";
            ImageAddChildDetailsHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            return;
        }

        #region OTHER DEPENDANTS
        //validate add staff's other dependants details
        private string ValidateAddStaffOtherDependantDetails()
        {
            if (HiddenFieldStaffID.Value.ToString() == "")
                return "Dependant save failed. Enter staff's basic information first or search for an employee before adding his /her children!";
            else if (txtOtherDependantName.Text.Trim() == "")
                return "Enter employee's dependant name!";
            else if (txtOtherDependantDOB.Text.Trim() == "")
                return "Enter date of birth!";
            else if (txtOtherDependantDOB.Text.Trim() != "" && Convert.ToDateTime(txtOtherDependantDOB.Text.Trim()) >= DateTime.Now)
                return "Invalid dependant date of birth selected!. Dependant's date of birth cannot be current date or a future date. Select a valid date of birth for the dependant!";
            else if (rblOtherDependantGender.SelectedIndex == -1)
                return "Select dependant's gender!";
            return "";
        }
        //save employee's dependant details
        private void SaveOtherDependantDetails()
        {
            ModalPopupExtenderAddOtherDependantDetails.Show();
            try
            {
                string _error = ValidateAddStaffOtherDependantDetails();//check if any errors are found
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    if (db.OtherDependants.Any(p => p.StaffID == _staffID && p.DependantName.ToLower() == txtOtherDependantName.Text.Trim().ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Staff's dependant entered already exists!", this, PanelFamilyDetails);
                        return;
                    }
                    else
                    {
                        //save OtherDependant details
                        OtherDependant newOtherDependant = new OtherDependant();
                        newOtherDependant.StaffID = _staffID;
                        newOtherDependant.DependantName = txtOtherDependantName.Text.Trim();
                        if (txtOtherDependantDOB.Text.Trim() != "")
                        {
                            newOtherDependant.DependantDOB = Convert.ToDateTime(txtOtherDependantDOB.Text.Trim());
                        }
                        newOtherDependant.Gender = rblOtherDependantGender.SelectedValue;
                        newOtherDependant.Relationship = txtOtherDependantRelationship.Text.Trim();
                        newOtherDependant.IsMedicalCovered = false;//set medical coverage to false 
                        db.OtherDependants.InsertOnSubmit(newOtherDependant);
                        db.SubmitChanges();
                        HiddenFieldOtherDependantID.Value = newOtherDependant.OtherDependantID.ToString();
                        lnkBtnUpdateOtherDependant.Enabled = true;
                        //display employee's OtherDependantren
                        DisplayStaffOtherDependants();
                        _hrClass.LoadHRManagerMessageBox(2, "Dependant details has been successfully saved!", this, PanelFamilyDetails);
                        //display employee's  OtherDependantren to have medical benefit
                        //DisplayOtherDependantForMedicalBenefit();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //update staff's OtherDependant details
        private void UpdateOtherDependantDetails(int _OtherDependantID)
        {
            ModalPopupExtenderAddOtherDependantDetails.Show();
            try
            {
                string _error = ValidateAddStaffOtherDependantDetails();//check if any errors are found
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    if (db.OtherDependants.Any(p => p.StaffID == _staffID && p.DependantName.ToLower() == txtOtherDependantName.Text.Trim().ToLower() && p.OtherDependantID != _OtherDependantID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "The dependant name you are trying to update with already exists!", this, PanelFamilyDetails);
                        txtOtherDependantName.Focus();
                        return;
                    }
                    else
                    {
                        //update  OtherDependant details
                        OtherDependant updateOtherDependant = db.OtherDependants.Single(p => p.OtherDependantID == _OtherDependantID);
                        updateOtherDependant.DependantName = txtOtherDependantName.Text.Trim();
                        if (txtOtherDependantDOB.Text.Trim() != "")
                        {
                            updateOtherDependant.DependantDOB = Convert.ToDateTime(txtOtherDependantDOB.Text.Trim());
                        }
                        else updateOtherDependant.DependantDOB = null;
                        updateOtherDependant.Gender = rblOtherDependantGender.SelectedValue;
                        updateOtherDependant.Relationship = txtOtherDependantRelationship.Text.Trim();
                        db.SubmitChanges();
                        //display employee's OtherDependantren
                        DisplayStaffOtherDependants();
                        _hrClass.LoadHRManagerMessageBox(2, "Staff's dependant details has been updated!", this, PanelFamilyDetails);
                        //display employee's  OtherDependantren to have medical benefit
                        //DisplayOtherDependantForMedicalBenefit();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        protected void lnkBtnSaveOtherDependant_OnClick(object sender, EventArgs e)
        {
            SaveOtherDependantDetails();
            return;
        }
        protected void lnkBtnUpdateOtherDependant_OnClick(object sender, EventArgs e)
        {
            UpdateOtherDependantDetails(Convert.ToInt32(HiddenFieldOtherDependantID.Value));
            return;
        }
        protected void lnkBtnClearOtherDependant_OnClick(object sender, EventArgs e)
        {
            ClearAddOtherDependantDetailsControls();
            ModalPopupExtenderAddOtherDependantDetails.Show();
            return;
        }
        //display employee's other dependants details
        public void DisplayStaffOtherDependants()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                object _display;
                _display = db.PROC_StaffOtherDependants(_staffID);
                gvOtherDependantDetails.DataSourceID = null;
                gvOtherDependantDetails.DataSource = _display;
                gvOtherDependantDetails.DataBind();
                return;
            }
            catch { }
        }
        //
        protected void gvOtherDependantDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditOtherDependant") == 0 || e.CommandName.CompareTo("DeleteOtherDependant") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFOtherDependantID = (HiddenField)gvOtherDependantDetails.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditOtherDependant") == 0)//check if we are editing
                    {
                        LoadOtherDependantDetailsForEdit(Convert.ToInt32(_HFOtherDependantID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteOtherDependant") == 0)//check if we are deleting
                    {
                        Session["DeleteOtherDependantID"] = _HFOtherDependantID.Value;

                        string _deleteMessage = "Are you sure you what to delete the OtherDependant?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                        Session["FamilyDeleteCase"] = 2;//set family delete case to 1 for deleting a OtherDependant
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured." + ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }

        }
        private void LoadOtherDependantDetailsForEdit(int _OtherDependantID)
        {
            OtherDependant getOtherDependant = db.OtherDependants.Single(p => p.OtherDependantID == _OtherDependantID);
            HiddenFieldOtherDependantID.Value = _OtherDependantID.ToString();
            txtOtherDependantName.Text = getOtherDependant.DependantName;
            if (getOtherDependant.DependantDOB != null)
            {
                txtOtherDependantDOB.Text = _hrClass.ShortDateDayStart(getOtherDependant.DependantDOB.ToString());
            }
            else txtOtherDependantDOB.Text = "";
            rblOtherDependantGender.SelectedValue = getOtherDependant.Gender;
            txtOtherDependantRelationship.Text = getOtherDependant.Relationship;
            lnkBtnUpdateOtherDependant.Enabled = true;
            lnkBtnSaveOtherDependant.Visible = false;
            lnkBtnClearOtherDependant.Visible = true;
            lnkBtnUpdateOtherDependant.Enabled = true;
            lbAddOtherDependantDetailsHeader.Text = "Edit Dependant";
            ImageAddOtherDependantDetailsHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            return;
        }
        //populate edit OtherDependant details
        private void PopulateEditOtherDependantControls()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvOtherDependantDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a dependant record to edit!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvOtherDependantDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one dependant record. Select only one recoed to edit at a time!", this, PanelFamilyDetails);
                        return;
                    }
                    foreach (GridViewRow _grv in gvOtherDependantDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit OtherDependant details
                            LoadOtherDependantDetailsForEdit(Convert.ToInt32(_HFID.Value));
                            ModalPopupExtenderAddOtherDependantDetails.Show();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading dependant details for edit! " + ex.Message.ToString(), this, PanelFamilyDetails);
            }
        }
        //load delete staff other dependant
        private void LoadDeleteStaffOtherDependant()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvOtherDependantDetails))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a dependant record to delete!", this, PanelFamilyDetails);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvOtherDependantDetails.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected dependant record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected dependants records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        //delete staff other dependant
        private void DeleteStaffOtherDependant()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvOtherDependantDetails.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFOtherDependantID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _otherDependantID = Convert.ToInt32(_HFOtherDependantID.Value);
                        OtherDependant deleteOtherDependant = db.OtherDependants.Single(p => p.OtherDependantID == _otherDependantID);
                        db.OtherDependants.DeleteOnSubmit(deleteOtherDependant);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayStaffOtherDependants();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected dependant record has been deleted.", this, PanelFamilyDetails);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected dependants records have been deleted.", this, PanelFamilyDetails);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed !" + ex.Message.ToString(), this, PanelFamilyDetails);
                return;
            }
        }
        ////delete employee's OtherDependant
        //private void DeleteStaffOtherDependantDetails()
        //{
        //    try
        //    {

        //        int _OtherDependantID = Convert.ToInt32(Session["DeleteOtherDependantID"]);//get OtherDependant id
        //        //delete OtherDependant
        //        OtherDependant deleteOtherDependant = db.OtherDependants.Single(p => p.OtherDependantID == _OtherDependantID);
        //        db.OtherDependants.DeleteOnSubmit(deleteOtherDependant);
        //        db.SubmitChanges();
        //        //refresh Other Dependants
        //        DisplayStaffOtherDependants();
        //        _hrClass.LoadHRManagerMessageBox(2, "Staff's dependant has been deleted!", this, PanelFamilyDetails);
        //        //clear add OtherDependant details if OtherDependant details in the add controls are details being deleted
        //        if (HiddenFieldOtherDependantID.Value != "" && Convert.ToInt32(HiddenFieldOtherDependantID.Value) == _OtherDependantID)
        //        {
        //            ClearAddOtherDependantDetailsControls();
        //        }
        //        //refresh OtherDependantren medical benefit details
        //        //DisplayOtherDependantForMedicalBenefit();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelFamilyDetails);
        //        return;
        //    }
        //}
        //clear add OtherDependant details controls
        private void ClearAddOtherDependantDetailsControls()
        {
            _hrClass.ClearEntryControls(PanelAddOtherDependantDetails);
            txtOtherDependantDOB.Text = "";
            HiddenFieldOtherDependantID.Value = "";
            lnkBtnSaveOtherDependant.Visible = true;
            lnkBtnUpdateOtherDependant.Enabled = false;
            lnkBtnUpdateOtherDependant.Visible = true;
            lnkBtnClearOtherDependant.Visible = true;
            lbAddOtherDependantDetailsHeader.Text = "Add Dependant";
            ImageAddOtherDependantDetailsHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        }
        #endregion
    }
}