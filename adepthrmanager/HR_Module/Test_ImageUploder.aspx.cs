﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.HR_Module
{
    public partial class Test_ImageUploder : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderUploadStaffImage.Show();
            // Upload Original Image Here
            string uploadFileName = "";
            string uploadFilePath = "";
            if (FU1.HasFile)
            {
                string ext = Path.GetExtension(FU1.FileName).ToLower();
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".png")
                {
                    uploadFileName = Guid.NewGuid().ToString() + ext;
                    uploadFilePath = Path.Combine(Server.MapPath("~/UploadImages"), uploadFileName);
                    try
                    {
                        FU1.SaveAs(uploadFilePath);

                        string _fileLocation = Server.MapPath("~/UploadImages/" + uploadFileName);
                        FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                        //get image info
                        FileInfo documentInfo = new FileInfo(_fileLocation);
                        int fileSize = (int)documentInfo.Length;//get file size
                        byte[] binaryImage = new byte[fileSize];
                        fs.Read(binaryImage, 0, fileSize);//read the document from the file stream
                        fs.Close();
                        HandleImageUpload(binaryImage, uploadFileName);

                        imgUpload.ImageUrl = "~/UploadImages/" + uploadFileName;
                        //panCrop.Visible = true;
                        btnCrop.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Text = "Error! Please try again.";
                    }
                }
                else
                {
                    lblMsg.Text = "Selected file type not allowed!";
                }
            }
            else
            {
                lblMsg.Text = "Please select file first!";
            }
        }
        protected void btnCrop_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderUploadStaffImage.Show();
            // Crop Image Here & Save
            string fileName = Path.GetFileName(imgUpload.ImageUrl);
            string filePath = Path.Combine(Server.MapPath("~/UploadImages"), fileName);
            string cropFileName = "";
            string cropFilePath = "";
            if (File.Exists(filePath))
            {
                if (hfH.Value == "" && hfW.Value == "" && hfX.Value == "" && hfY.Value == "")
                {
                    lblMsg.Text = "Crop an image sectopn to save has the staff photo!";
                    return;
                }
                else
                {
                    System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                    Rectangle CropArea = new Rectangle(
                        Convert.ToInt32(hfX.Value),
                        Convert.ToInt32(hfY.Value),
                        Convert.ToInt32(hfW.Value),
                        Convert.ToInt32(hfH.Value));

                    try
                    {
                        Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                        using (Graphics g = Graphics.FromImage(bitMap))
                        {
                            g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                        }
                        cropFileName = "crop_" + fileName;
                        cropFilePath = Path.Combine(Server.MapPath("~/UploadImages"), cropFileName);
                        bitMap.Save(cropFilePath);

                        //get file streem of the croped image
                        FileStream fs = new FileStream(cropFilePath, FileMode.Open);//
                        //get document info
                        FileInfo documentInfo = new FileInfo(cropFilePath);
                        int fileSize = (int)documentInfo.Length;//get file size
                        byte[] fileDocument = new byte[fileSize];
                        fs.Read(fileDocument, 0, fileSize);//read the document from the file stream
                        fs.Close();

                        //save the photo in the staff master table
                        Guid _staffID = _hrClass.ReturnGuid("25af7b04-076f-e411-8c43-68a3c4aec686");
                        StaffMaster saveStaffPhoto = db.StaffMasters.Single(p => p.StaffID == _staffID);
                        saveStaffPhoto.StaffPhoto = fileDocument;
                        db.SubmitChanges();
                        ModalPopupExtenderUploadStaffImage.Hide();
                        _hrClass.LoadHRManagerMessageBox(2, "Staff photo has been successfully saved!", this, PanelImageUploader);
                        //load staff photo
                        return;
                        //Response.Redirect("~/UploadImages/" + cropFileName, false);
                    }

                    catch (Exception ex)
                    {
                        //throw;
                        lblMsg.Text = "Crop failed." + ex.Message.ToString();
                        return;
                    }
                }
            }
        }
        private Image RezizeImage(Image img, int maxWidth, int maxHeight)
        {
            if (img.Height < maxHeight && img.Width < maxWidth) return img;
            using (img)
            {
                //Double xRatio = (double)img.Width / maxWidth;
                //Double yRatio = (double)img.Height / maxHeight;
                //Double ratio = Math.Max(xRatio, yRatio);

                //int nnx = (int)Math.Floor(img.Width / ratio);
                //int nny = (int)Math.Floor(img.Height / ratio);
                int nnx = maxWidth;
                int nny = maxHeight;
                Bitmap cpy = new Bitmap(nnx, nny, PixelFormat.Format32bppArgb);
                using (Graphics gr = Graphics.FromImage(cpy))
                {
                    gr.Clear(Color.Transparent);

                    // This is said to give best quality when resizing images
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    gr.DrawImage(img,
                        new Rectangle(0, 0, nnx, nny),
                        new Rectangle(0, 0, img.Width, img.Height),
                        GraphicsUnit.Pixel);
                }
                return cpy;
            }

        }

        private MemoryStream BytearrayToStream(byte[] arr)
        {
            return new MemoryStream(arr, 0, arr.Length);
        }

        private void HandleImageUpload(byte[] binaryImage, string _fileName)
        {
            using (Image img = RezizeImage(Image.FromStream(BytearrayToStream(binaryImage)), 305, 372))
            {
                img.Save(Server.MapPath("~/UploadImages/" + _fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
            }

        }
    }
}