﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Employment_Terms.aspx.cs" Inherits="AdeptHRManager.HR_Module.Employment_Terms" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
    <script type="text/javascript" language="javascript">

        //calculate consolidated pay
        function CalculateConsolidatedPay() {
            var _basicPay = document.getElementById('<%= txtEmploymentTermsBasicPay.ClientID %>').value;
            if (isNaN(_basicPay)) {
                alert('Invalid basic pay entered!');
            }
            else {
                var _housingBenefit = _basicPay * 0.2; //0.2 as the 20% of basic pay as housing allowance
                var _consolidatedPay = parseFloat(_basicPay) + parseFloat(_housingBenefit); //total basic and housing benefit
                document.getElementById('<%= txtHousingBenefit.ClientID %>').value = _housingBenefit.toFixed(2);
                document.getElementById('<%= txtConsolidatedPay.ClientID %>').value = _consolidatedPay.toFixed(2);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelEmploymentTerms" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelEmploymentTerms" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="panel-details">
                    <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Salary & Benefits Details</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    Basic Pay:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsBasicPay" Style="text-align: right;" onkeyup="CalculateConsolidatedPay()"
                                        runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Basic Pay Currency:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlBasicPayCurrency" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Housing Benefit:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHousingBenefit" Style="text-align: right;" ReadOnly="true" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Consolidated Pay:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtConsolidatedPay" Style="text-align: right;" ReadOnly="true" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Retirement Date</td>
                                <td>
                                    <asp:TextBox ID="txtRetirementDate" runat="server" 
                                        Style="text-align: right;"></asp:TextBox>
                                         <asp:ImageButton ID="ImageButtonRetirementDate" ToolTip="Pick Retirement Date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtRetirementDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtRetirementDate" PopupButtonID="ImageButtonRetirementDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtRetirementDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtRetirementDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtRetirementDate_MaskedEditValidator" runat="server" ControlExtender="txtRetirementDate_MaskedEditExtender"
                                        ControlToValidate="txtRetirementDate" CssClass="errorMessage" ErrorMessage="txtRetirementDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid Retirement Date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>

                                </td>
                                <td>
                                    Retirement Benefits</td>
                                <td>
                                    <asp:TextBox ID="txtRetirementBenefits" runat="server" 
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Insurance:<span class="errorMessage">*</span>
                                </td>
                                <td colspan="2">
                                    <%--  <asp:DropDownList ID="ddlEmploymentTermsMedicalScheme" CssClass="drpDown" runat="server">
                                        <asp:ListItem Value="0">Select Medical Scheme</asp:ListItem>
                                    </asp:DropDownList>--%>
                                    <asp:CheckBoxList ID="cblEmploymentTermsMedicalScheme" RepeatDirection="Vertical"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </td>
                                <td>
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsID" runat="server" />
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsPerksDetailID" runat="server" />
                                    <asp:Button ID="btnSaveEmployementTerms" OnClick="btnSaveEmployementTerms_Click"
                                        runat="server" Text="Save Details" ToolTip="Save salary & benefits" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Allowances</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    Allowance:
                                   
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEmploymentTermsPerkName" Width="200px" runat="server">
                                        <asp:ListItem Value="0">Select Perk</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Allowance Amount:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsPerkAmount" Width="190px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Allowance Currency:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAllowanceCurrency" Width="190px" runat="server">
                                        <asp:ListItem Value="0">-Select Currency-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnEmploymentTermsAddPerk" OnClick="btnEmploymentTermsAddPerk_Click"
                                        runat="server" Text="Save Allowance" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                    <asp:GridView ID="gvEmploymentTermsPerksDetails" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" OnRowCommand="gvEmploymentTermsPerksDetails_RowCommand"
                                        AllowSorting="True" PageSize="10" EmptyDataText="No allowance details!">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("emptermsperkdetail_iEmpTermsPerkDetailID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="emptermsperkdetail_vPerk" HeaderText="Allowance Name" />
                                            <asp:BoundField DataField="emptermsperkdetail_mPerkAmount" HeaderText="Amount" DataFormatString="{0:n2}"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="emptermsperkdetail_vCurrency" HeaderText="Allowance Currency" />
                                            <%-- <asp:BoundField DataField="emptermsperkdetail_dtPerkDateFrom" HeaderText="Effective From"
                                                DataFormatString="{0:dd/MMM/yyyy}" />
                                            <asp:BoundField DataField="emptermsperkdetail_dtPerkDateUntil" HeaderText="Effective Until"
                                                DataFormatString="{0:dd/MMM/yyyy}" />--%>
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditPerkDetail" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeletePerkDetail" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <asp:Panel ID="PanelStaffDependantToBenefit" Visible="false" runat="server">
                        <fieldset>
                            <legend>Dependant to benefit from the medical cover</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvStaffDependants" runat="server" CssClass="GridViewStyle" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="SNo" ItemStyle-Width="4px" />
                                                <asp:BoundField DataField="spouse_vFirstName" HeaderText="First Name" />
                                                <asp:BoundField DataField="spouse_vSurname" HeaderText="Surname" />
                                                <asp:BoundField DataField="DependantDOB" HeaderText="Date Of Birth" />
                                                <asp:BoundField DataField="Relationship" HeaderText="Relationship" />
                                                <asp:TemplateField HeaderText="Is a Benefeciary?" ItemStyle-Width="4px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Checked") %>' />
                                                        <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                                        <asp:HiddenField ID="HiddenFieldIsSpouse" runat="server" EnableViewState="False"
                                                            Value='<%# Bind("IsSpouse") %>' />
                                                        <asp:HiddenField ID="HiddenFieldIsChild" runat="server" EnableViewState="False" Value='<%# Bind("IsChild") %>' />
                                                        <asp:HiddenField ID="HiddenFieldIsOtherDependant" runat="server" EnableViewState="False"
                                                            Value='<%# Bind("IsOtherDependant") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="IsSpouse" HeaderText="Is Spouse" />
                                                <asp:BoundField DataField="IsChild" HeaderText="Is Child" />
                                                <asp:BoundField DataField="IsOtherDependant" HeaderText="Is Other Dependant" />--%>
                                            </Columns>
                                            <FooterStyle CssClass="PagerStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                  <legend>
                                   NB:To edit details of the dependants, please go to the &quot;Family&quot; tab
                                    </legend>
                                  </td>
                                    <td align="right">
                                    
                                       <asp:Button ID="btnSaveStaffDependantToBenefit" OnClick="btnSaveStaffDependantToBenefit_OnClick"
                                            runat="server" Text="Save Dependant To Benefit" />
                                           
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
