﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;

namespace AdeptHRManager.HR_Module
{
    public partial class Leave_Tracking : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        DateTimeFormatInfo dateInfo = new DateTimeFormatInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayLeaveStatus(_staffID);//load leave tracking details
                    return;
                }
            }
            catch { }
        }
        //display staff leave status by selected staff id
        private void DisplayLeaveStatus(Guid _staffID)
        {
            try
            {
                //load leave status
                GetLeaveStatus(_staffID);
                //load approved leaves
                GetApprovedLeavesHistory(_staffID);
                return;
            }
            catch { }
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayLeaveStatus(_StaffID);
                    return;
                }
                else
                {
                    DisplayLeaveStatus(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelLeaveTracking);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaveTracking);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayLeaveStatus(_StaffID);
                    return;
                }
                else
                {
                    DisplayLeaveStatus(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelLeaveTracking);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaveTracking);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayLeaveStatus(_StaffID);
                    return;
                }
                else
                {
                    DisplayLeaveStatus(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelLeaveTracking);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaveTracking);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayLeaveStatus(_StaffID);
                    return;
                }
                else
                {
                    DisplayLeaveStatus(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelLeaveTracking);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaveTracking);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }

        private void GetLeaveStatus(Guid _staffID)
        {
            try
            {
                //get header for earned upto
                string earnedUptoHeader = "Earned Un Approved";
                int currentMonth = DateTime.Now.Month, currentYear = DateTime.Now.Year;
                try
                {
                    string lastMonthName = dateInfo.MonthNames[currentMonth - 2];//get last month name from the index of the current month by going two values back
                    earnedUptoHeader = "Earned Upto " + lastMonthName.Substring(0, 3) + ". " + currentYear;
                }
                catch { }
                //get employee gender
                string employeeGender = db.StaffMasters.Single(p => p.StaffID == _staffID).Gender.ToString().TrimEnd();
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));//get leave types
                dt.Columns.Add(new DataColumn("ApprovedEarned", typeof(string)));
                dt.Columns.Add(new DataColumn("ApprovedConsumed", typeof(string)));
                dt.Columns.Add(new DataColumn("UnApprovedConsumed", typeof(string)));
                dt.Columns.Add(new DataColumn("Bal", typeof(string)));
                dt.Columns.Add(new DataColumn("CarryFwd", typeof(string)));
                //dt.Columns.Add(new DataColumn("UnApprovedEarned", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //get leave status depending on the gender of the employee and leave type is applicable to all
                foreach (LeaveType _leaveType in db.LeaveTypes.Where(p => p.leavetype_vApplicationTo == "All" || p.leavetype_vApplicationTo == employeeGender).OrderBy(p => p.leavetype_vName))
                {
                    //get leave status totals
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, _leaveType.leavetype_siLeaveTypeID).Single();
                    //get leave days balance
                    string _balanceDays = "N/A";
                    //check if the leave balance for the leave type is to be calculated
                    if (_leaveType.leavetype_bCalculateLeaveBalance == true)
                    {
                        _balanceDays = string.Format("{0:F2}", (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed)));
                    }
                    //check if leave days can be carried or not
                    string _carryFwd = "No";
                    if (_leaveType.leavetype_bIsCarriedFwd == true)
                    {
                        _carryFwd = "Yes";
                    }
                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = _leaveType.leavetype_vName;
                    dr[2] = string.Format("{0:F2}", _sumLeaveDays.approved_earned);
                    dr[3] = string.Format("{0:F2}", _sumLeaveDays.approved_consumed);
                    dr[4] = string.Format("{0:F2}", _sumLeaveDays.unapproved_consumed);
                    dr[5] = _balanceDays;
                    dr[6] = _carryFwd;
                    ////check if the leave type is annual leave an the the unapproved leaved days
                    //string unApprovedEarnedDays = "N/A";
                    //if (_leaveType.leavetype_vName.ToLower().Contains("annual"))//check if the leave type is annual
                    //{
                    //    try
                    //    {
                    //        //get unapproved annual leave days for the seleted employee
                    //        unApprovedEarnedDays = db.ViewSummedUnApprovedLeaveDays.Single(p => p.staffmst_iStaffMstID == staffMasterID).UnApprovedLeaveDays.ToString();
                    //    }
                    //    catch { }
                    //}
                    //dr[7] = unApprovedEarnedDays;

                    dt.Rows.Add(dr);
                    countRows++;
                }
                gvLeaveStatus.DataSource = dt;
                //gvLeaveStatus.Columns[7].HeaderText = earnedUptoHeader;
                gvLeaveStatus.DataBind();
            }
            catch { }

        }
        //get approved leave history
        private void GetApprovedLeavesHistory(Guid staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_ApprovedLeavePerStaff(staffID);
                gvApprovedLeavesHistory.DataSourceID = null;
                gvApprovedLeavesHistory.DataSource = _display;
                gvApprovedLeavesHistory.DataBind();
                Session["gvApprovedLeavesHistoryData"] = _display;
                gvApprovedLeavesHistory.AllowPaging = true;
            }
            catch { }
        }
        //enable page index changing on gvApprovedLeavesHistory
        protected void gvApprovedLeavesHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvApprovedLeavesHistory.PageIndex = e.NewPageIndex;
            gvApprovedLeavesHistory.DataSource = (object)Session["gvApprovedLeavesHistoryData"];
            gvApprovedLeavesHistory.DataBind();
            return;
        }
       
    }
}