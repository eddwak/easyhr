﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Employment_History.aspx.cs" Inherits="AdeptHRManager.HR_Module.Employment_History" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelEmploymentHistory" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelEmploymentHistory" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNewWorkExperience" OnClick="LinkButtonNewWorkExperience_Click"
                        ToolTip="Add new work experience record" CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNewWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEditWorkExperience" OnClick="LinkButtonEditWorkExperience_Click"
                        ToolTip="Edit work experience details" CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEditWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEditWorkExperience" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDeleteWorkExperience" OnClick="LinkButtonDeleteWorkExperience_Click"
                        CausesValidation="false" ToolTip="Delete work experience" runat="server">
                        <asp:Image ID="ImageDeleteWorkExperience" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                    <asp:Label ID="LabelDeleteWorkExperience" runat="server" Text="|"></asp:Label>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvEmploymentHistory" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                        OnLoad="gvEmploymentHistory_OnLoad" OnPageIndexChanging="gvEmploymentHistory_PageIndexChanging"
                        EmptyDataText="No employement history available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffWorkExperienceID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PositionHeld" HeaderText="Position/Title Held" />
                            <asp:BoundField DataField="EmployerName" HeaderText="Employer's Name" />
                            <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                            <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                            <asp:BoundField DataField="Supervisor" HeaderText="Supervisor" />
                            <asp:BoundField DataField="IsEmployerVerified" HeaderText="Employer Verified?" />
                             <asp:BoundField DataField="TerminationReason" HeaderText="Reason for Termination" />                            
                           <%-- <asp:BoundField DataField="WasEmployerEADB" HeaderText="Was Employer EADB?" />
                            <asp:BoundField DataField="EADBContractRef" HeaderText="EADB Contract Reference" />
                            <asp:BoundField DataField="EADBContractDate" HeaderText="EADB Contract Date" DataFormatString="{0:dd/MMM/yyyy}" />--%>
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddWorkExperience" Style="display: none; width: 620px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddWorkExperience" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddWorkExperienceHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddWorkExperienceHeader" runat="server" Text="Add Employment History"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddWorkExperience" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Employment History Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Position/Title Held:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtWorkExperiencePositionHeld" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Employer Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtWorkExperienceEmployerName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Start Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkExperienceStartDateMonth" AppendDataBoundItems="true"
                                            Width="95px" runat="server">
                                            <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlWorkExperienceStartDateYear" Width="80px" runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        End Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkExperienceEndDateMonth" Width="95px" runat="server">
                                            <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlWorkExperienceEndDateYear" Width="80px" runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Supervisor:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSupervisor" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                  <tr>
                                    <td>
                                        Reason for Termination:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTerminationReason" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbEmployerVerified" Text="Has the employer been verified?" TextAlign="Left"
                                            runat="server" />
                                    </td>
                                   <%-- <td>
                                        <asp:CheckBox ID="cbWasEmployerEADB" Text="Was the employer EADB?" TextAlign="Left"
                                            runat="server" />
                                    </td>--%>
                                </tr>
                          <%--      <tr>
                                    <td>
                                        EADB Contact Reference:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEADBContractReference" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        EADB Contract Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEADBContractDate" Width="200px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonEADBContractDate" ToolTip="Pick probation end date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtEADBContractDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtEADBContractDate" PopupButtonID="ImageButtonEADBContractDate"
                                            PopupPosition="TopRight"></asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtEADBContractDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEADBContractDate"
                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtEADBContractDate_MaskedEditValidator" runat="server"
                                            ControlExtender="txtEADBContractDate_MaskedEditExtender" ControlToValidate="txtEADBContractDate"
                                            CssClass="errorMessage" ErrorMessage="txtEADBContractDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid EADB contract date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label1" CssClass="errorMessage" runat="server" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbWorkExperienceError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbWorkExperienceInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldWorkExperienceID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddWorkExperiencePopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveWorkExperience" OnClick="lnkBtnSaveWorkExperience_Click"
                                ToolTip="Save work experience" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateWorkExperience" OnClick="lnkBtnUpdateWorkExperience_Click"
                                CausesValidation="false" ToolTip="Update work experience" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearWorkExperience" OnClick="lnkBtnClearWorkExperience_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddWorkExperience" RepositionMode="RepositionOnWindowResize"
                    TargetControlID="HiddenFieldAddWorkExperiencePopup" PopupControlID="PanelAddWorkExperience"
                    CancelControlID="ImageButtonCloseAddWorkExperience" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <%--<asp:DragPanelExtender ID="DragPanelExtenderAddWorkExperience" TargetControlID="PanelAddWorkExperience"
                    DragHandleID="PanelDragAddWorkExperience" runat="server"></asp:DragPanelExtender>--%>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
