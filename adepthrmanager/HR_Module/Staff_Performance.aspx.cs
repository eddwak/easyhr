﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;
using AjaxControlToolkit;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Performance : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
                PopulateDropDowns();//populate drop downs
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffPerformanceDetails(_staffID);//load staff appraisal details

                    return;
                }
            }
            catch { }
        }
        //load staff  perfoemance details
        private void DisplayStaffPerformanceDetails(Guid _staffID)
        {
            //load staff appraisal details here

            //LoadStaffAnnualPerformance(_staffID, DateTime.Now.Year.ToString());//load staff performance details for the current year
            DisplayAnnualPerformance(_staffID, DateTime.Now.Year.ToString());//load staff performance details for the current year
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelStaffPerformance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffPerformance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffPerformance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffPerformance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //populate drop downs
        private void PopulateDropDowns()
        {
            _hrClass.GenerateYears(ddlPerformanceYear);//display performance year drop down
            ddlPerformanceYear.SelectedValue = DateTime.Now.Year.ToString();//select current year
            _hrClass.GetStaffNames(ddlQuarter1AppraisedBy, "Appraised By");//populate  quarter 1 appraised by dropdown
            _hrClass.GetStaffNames(ddlQuarter2AppraisedBy, "Appraised By");//populate  quarter 2 appraised by dropdown
            _hrClass.GetStaffNames(ddlQuarter3AppraisedBy, "Appraised By");//populate  quarter 3 appraised by dropdown
            _hrClass.GetStaffNames(ddlQuarter4AppraisedBy, "Appraised By");//populate  quarter 4 appraised by dropdown
        }
        //load add new performance objective
        protected void LinkButtonNew_OnClick(object sender, EventArgs e)
        {
            ModalPopupExtenderAddAnnualPerformance.Show();
        }
        //validate staff annual Performance
        private string ValidateAnnualPerformanceControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the performance details against!";
            else if (ddlPerformanceYear.SelectedIndex == 0)
            {
                ddlPerformanceYear.Focus();
                return "Select performance year!";
            }
            else if (txtPerformanceObjective.Text.Trim() == "")
            {
                txtPerformanceObjective.Focus();
                return "Enter performance objective!";
            }
            else if (txtAnnualTarget.Text.Trim() == "")
            {
                txtAnnualTarget.Focus();
                return "Enter annual target!";
            }
            else if (txtAnnualAchieved.Text.Trim() == "")
            {
                txtAnnualAchieved.Focus();
                return "Enter actual achieved at the end of the year!";
            }
            else if (txtAnnualRating.Text.Trim() == "")
            {
                txtAnnualRating.Focus();
                return "Enter rating!";
            }
            else if (_hrClass.isNumberValid(txtAnnualRating.Text.Trim()) == false)
            {
                txtAnnualRating.Focus();
                return "Invalid rating value entered. Please enter a numeric value!";
            }
            else if (txtAnnualWeight.Text.Trim() == "")
            {
                txtAnnualWeight.Focus();
                return "Enter weight achieved!";
            }
            else if (_hrClass.isNumberValid(txtAnnualWeight.Text.Trim()) == false)
            {
                txtAnnualWeight.Focus();
                return "Invalid weight value entered. Please enter a numeric value!";
            }
            else if (txtAnnualScore.Text.Trim() == "")
            {
                txtAnnualScore.Focus();
                return "Enter score!";
            }
            else if (_hrClass.isNumberValid(txtAnnualScore.Text.Trim()) == false)
            {
                txtAnnualScore.Focus();
                return "Invalid score value entered. Please enter a numeric value!";
            }
            else return "";
        }
        //save annual performance
        private void SaveAnnualPerformance()
        {
            ModalPopupExtenderAddAnnualPerformance.Show();
            try
            {
                string _error = ValidateAnnualPerformanceControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffPerformance);
                    return;
                }
                else
                {
                    string _performanceYear = ddlPerformanceYear.SelectedValue.Trim();
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _performanceObjective = "", _annualTarget = "", _annualAchieved = "", _comments = "";
                    Double _rating = 0, _weight = 0, _score = 0;
                    _performanceObjective = txtPerformanceObjective.Text.Trim();
                    _annualTarget = txtAnnualTarget.Text.Trim();
                    _annualAchieved = txtAnnualAchieved.Text.Trim();
                    _comments = txtAnnualComments.Text.Trim();
                    _rating = Convert.ToDouble(txtAnnualRating.Text.Trim());
                    _weight = Convert.ToDouble(txtAnnualWeight.Text.Trim());
                    _score = Convert.ToDouble(txtAnnualScore.Text.Trim());
                    //check if the staff as an existing record for the year
                    //if (db.AnnualPerformances.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear))
                    //if (db.AnnualPerformances.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear))
                    //{
                    //    //update the existing details
                    //    UpdateAnnualPerformance(_staffID, _performanceYear, _performanceObjective, _annualTarget, _annualAchieved, _rating, _weight, _score, _comments);
                    //    _hrClass.LoadHRManagerMessageBox(2, "Staff annual performance details have been successfully updated.", this, PanelStaffPerformance);
                    //    return;
                    //}
                    //else
                    //{
                    //save a new record
                    SaveNewAnnualPerformance(_staffID, _performanceYear, _performanceObjective, _annualTarget, _annualAchieved, _rating, _weight, _score, _comments);
                    DisplayAnnualPerformance(_staffID, _performanceYear);//refresh performance objectives
                    _hrClass.LoadHRManagerMessageBox(2, "Staff annual performance details have been successfully saved.", this, PanelStaffPerformance);
                    return;
                    //}
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(2, "Save failed. " + ex.Message.ToString() + " Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //save a new annual performance record
        private void SaveNewAnnualPerformance(Guid _staffID, string _performanceYear, string _performanceObjective, string _annualTarget, string _annualAchieved, Double _rating, Double _weight, Double _score, string _comments)
        {
            AnnualPerformance newAnnualPerformance = new AnnualPerformance();
            newAnnualPerformance.StaffID = _staffID;
            newAnnualPerformance.PerformanceYear = _performanceYear;
            newAnnualPerformance.PerformanceObjective = _performanceObjective;
            newAnnualPerformance.Target = _annualTarget;
            newAnnualPerformance.Achieved = _annualAchieved;
            newAnnualPerformance.Rating = _rating;
            newAnnualPerformance.Weight = _weight;
            newAnnualPerformance.Score = _score;
            newAnnualPerformance.Comments = _comments;
            db.AnnualPerformances.InsertOnSubmit(newAnnualPerformance);
            db.SubmitChanges();
        }
        //update an existing annnual performance record
        private void UpdateAnnualPerformance(Guid _staffID, string _performanceYear, string _performanceObjective, string _annualTarget, string _annualAchieved, Double _rating, Double _weight, Double _score, string _comments)
        {
            AnnualPerformance updateAnnualPerformance = db.AnnualPerformances.Single(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear);
            updateAnnualPerformance.StaffID = _staffID;
            updateAnnualPerformance.PerformanceYear = _performanceYear;
            updateAnnualPerformance.PerformanceObjective = _performanceObjective;
            updateAnnualPerformance.Target = _annualTarget;
            updateAnnualPerformance.Achieved = _annualAchieved;
            updateAnnualPerformance.Rating = _rating;
            updateAnnualPerformance.Weight = _weight;
            updateAnnualPerformance.Score = _score;
            updateAnnualPerformance.Comments = _comments;
            db.SubmitChanges();
        }
        //save staff annnual performance details
        protected void btnSaveAnnualPerformance_Click(object sender, EventArgs e)
        {
            SaveAnnualPerformance();//save staff annual performance
            return;
        }
        //load staff annual performance details
        private void LoadStaffAnnualPerformance(Guid _staffID, string _performanceYear)
        {
            if (db.PROC_StaffAnnualPerformance(_staffID, _performanceYear).Any())
            {
                PROC_StaffAnnualPerformanceResult getAnnualPerformance = db.PROC_StaffAnnualPerformance(_staffID, _performanceYear).FirstOrDefault();
                ddlPerformanceYear.SelectedValue = _performanceYear;
                txtPerformanceObjective.Text = getAnnualPerformance.PerformanceObjective;
                txtAnnualTarget.Text = getAnnualPerformance.Target;
                txtAnnualAchieved.Text = getAnnualPerformance.Achieved;
                txtAnnualRating.Text = getAnnualPerformance.Rating.ToString();
                txtAnnualWeight.Text = getAnnualPerformance.Weight.ToString();
                txtAnnualScore.Text = getAnnualPerformance.Score.ToString();
                return;
            }
            else
            {
                _hrClass.ClearEntryControls(PanelAddAnnualPerformance);
                ddlPerformanceYear.SelectedValue = _performanceYear;
                return;
            }
        }
        //validate staff quarterly performance details 
        private string ValidateStaffQuarterlyPerformanceControls()
        {

            if (HiddenFieldStaffID.Value == "")
                return "Save failed. Search for the employee that you want to record the quarterly performance details against!";
            else if (ddlPerformanceYear.SelectedIndex == 0)
            {
                ddlPerformanceYear.Focus();
                return "Select performance year!";
            }
            else if (txtKeyPerformanceIndicator.Text.Trim() == "")
            {
                txtKeyPerformanceIndicator.Focus();
                return "Enter key performance indicator!";
            }
            else if (txtQuarter1.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter1.Text.Trim()) == false)
            {
                txtQuarter1.Focus();
                return "Invalid quarter one value entered!";
            }
            else if (txtQuarter2.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter2.Text.Trim()) == false)
            {
                txtQuarter2.Focus();
                return "Invalid quarter two value entered!";
            }
            else if (txtQuarter3.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter3.Text.Trim()) == false)
            {
                txtQuarter3.Focus();
                return "Invalid quarter three value entered!";
            }
            else if (txtQuarter4.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter4.Text.Trim()) == false)
            {
                txtQuarter4.Focus();
                return "Invalid quarter four value entered!";
            }
            else if (txtQuarter1.Text.Trim() == "" && txtQuarter2.Text.Trim() == "" && txtQuarter3.Text.Trim() == "" && txtQuarter4.Text.Trim() == "")
            {
                txtQuarter1.Focus();
                return "There are no achieved quarter details entered. Please entere atlest one quarter's achieved value!";
            }
            else if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter1AppraisalDate.Text.Trim()) == false)
            {
                txtQuarter1AppraisalDate.Focus();
                return "Invalid quarter one appraisal date value entered!";
            }
            else if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter2AppraisalDate.Text.Trim()) == false)
            {
                txtQuarter2AppraisalDate.Focus();
                return "Invalid quarter two appraisal date value entered!";
            }
            else if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter3AppraisalDate.Text.Trim()) == false)
            {
                txtQuarter3AppraisalDate.Focus();
                return "Invalid quarter three appraisal date value entered!";
            }
            else if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtQuarter4AppraisalDate.Text.Trim()) == false)
            {
                txtQuarter4AppraisalDate.Focus();
                return "Invalid quarter four appraisal date value entered!";
            }
            else return "";
        }

        //save quarterly key performance indicator
        private void SaveQuarterlyKeyPerformanceIndicator()
        {
            ModalPopupExtenderAddKeyPerformanceIndicator.Show();
            try
            {
                string _error = ValidateStaffQuarterlyPerformanceControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffPerformance);
                    return;
                }
                else
                {
                    string _performanceYear = ddlPerformanceYear.SelectedValue.Trim();
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _keyPerformanceIndicator = txtKeyPerformanceIndicator.Text.Trim();
                    if (db.QuarterlyPerformances.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.KeyIndicator.ToLower() == _keyPerformanceIndicator.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed. The key performance indicator entered is already set against the employee!", this, PanelStaffPerformance);
                        return;
                    }
                    else
                    {
                        //save new key performance indicator
                        QuarterlyPerformance newQuarterlyPerformance = new QuarterlyPerformance();
                        newQuarterlyPerformance.AnnualPerformanceID = Convert.ToInt32(HiddenFieldAnnualPerformanceID.Value);
                        newQuarterlyPerformance.StaffID = _staffID;
                        newQuarterlyPerformance.KeyIndicator = _keyPerformanceIndicator;
                        newQuarterlyPerformance.PerformanceYear = _performanceYear;
                        newQuarterlyPerformance.Quarter1Achieved = txtQuarter1.Text.Trim();
                        newQuarterlyPerformance.Quarter2Achieved = txtQuarter2.Text.Trim();
                        newQuarterlyPerformance.Quarter3Achieved = txtQuarter3.Text.Trim();
                        newQuarterlyPerformance.Quarter4Achieved = txtQuarter4.Text.Trim();
                        //appraisal dates details
                        if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____")
                            newQuarterlyPerformance.Quarter1AppraisalDate = Convert.ToDateTime(txtQuarter1AppraisalDate.Text.Trim()).Date;
                        else
                            newQuarterlyPerformance.Quarter1AppraisalDate = null;
                        if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____")
                            newQuarterlyPerformance.Quarter2AppraisalDate = Convert.ToDateTime(txtQuarter2AppraisalDate.Text.Trim()).Date;
                        else
                            newQuarterlyPerformance.Quarter2AppraisalDate = null;
                        if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____")
                            newQuarterlyPerformance.Quarter3AppraisalDate = Convert.ToDateTime(txtQuarter3AppraisalDate.Text.Trim()).Date;
                        else
                            newQuarterlyPerformance.Quarter3AppraisalDate = null;
                        if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____")
                            newQuarterlyPerformance.Quarter4AppraisalDate = Convert.ToDateTime(txtQuarter4AppraisalDate.Text.Trim()).Date;
                        else
                            newQuarterlyPerformance.Quarter4AppraisalDate = null;
                        //appraised by details
                        if (ddlQuarter1AppraisedBy.SelectedIndex != 0)
                            newQuarterlyPerformance.Quarter1AppraisedByID = _hrClass.ReturnGuid(ddlQuarter1AppraisedBy.SelectedValue);
                        else
                            newQuarterlyPerformance.Quarter1AppraisedByID = null;
                        if (ddlQuarter2AppraisedBy.SelectedIndex != 0)
                            newQuarterlyPerformance.Quarter2AppraisedByID = _hrClass.ReturnGuid(ddlQuarter2AppraisedBy.SelectedValue);
                        else
                            newQuarterlyPerformance.Quarter2AppraisedByID = null;

                        if (ddlQuarter3AppraisedBy.SelectedIndex != 0)
                            newQuarterlyPerformance.Quarter3AppraisedByID = _hrClass.ReturnGuid(ddlQuarter3AppraisedBy.SelectedValue);
                        else
                            newQuarterlyPerformance.Quarter3AppraisedByID = null;
                        if (ddlQuarter4AppraisedBy.SelectedIndex != 0)
                            newQuarterlyPerformance.Quarter4AppraisedByID = _hrClass.ReturnGuid(ddlQuarter4AppraisedBy.SelectedValue);
                        else
                            newQuarterlyPerformance.Quarter4AppraisedByID = null;
                        //comments details
                        newQuarterlyPerformance.Quarter1Comments = txtQuarter1Comments.Text.Trim();
                        newQuarterlyPerformance.Quarter2Comments = txtQuarter2Comments.Text.Trim();
                        newQuarterlyPerformance.Quarter3Comments = txtQuarter3Comments.Text.Trim();
                        newQuarterlyPerformance.Quarter4Comments = txtQuarter4Comments.Text.Trim();
                        newQuarterlyPerformance.DateRecorded = DateTime.Now;
                        db.QuarterlyPerformances.InsertOnSubmit(newQuarterlyPerformance);
                        db.SubmitChanges();
                        DisplayAnnualPerformance(_staffID, _performanceYear);//refresh the grid view
                        ExpandACollapsiblePanelExtenderAccessed();
                        _hrClass.LoadHRManagerMessageBox(2, "Quarterly key performance indicator details have been saved!", this, PanelStaffPerformance);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + " Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //update quarterly key performance indicator
        private void UpdateQuarterlyKeyPerformanceIndicator()
        {
            ModalPopupExtenderAddKeyPerformanceIndicator.Show();
            try
            {
                string _error = ValidateStaffQuarterlyPerformanceControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffPerformance);
                    return;
                }
                else
                {
                    string _performanceYear = ddlPerformanceYear.SelectedValue.Trim();
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _keyPerformanceIndicator = txtKeyPerformanceIndicator.Text.Trim();
                    int _quarterlyPerformanceID = Convert.ToInt32(HiddenFieldQuarterlyPerformanceID.Value);
                    if (db.QuarterlyPerformances.Any(p => p.QuarterlyPerformanceID != _quarterlyPerformanceID && p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.KeyIndicator.ToLower() == _keyPerformanceIndicator.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed. The key performance indicator entered is already set against the employee!", this, PanelStaffPerformance);
                        return;
                    }
                    else
                    {
                        //update new key performance indicator
                        QuarterlyPerformance updateQuarterlyPerformance = db.QuarterlyPerformances.Single(p => p.QuarterlyPerformanceID == _quarterlyPerformanceID);
                        updateQuarterlyPerformance.KeyIndicator = _keyPerformanceIndicator;
                        updateQuarterlyPerformance.PerformanceYear = _performanceYear;
                        updateQuarterlyPerformance.Quarter1Achieved = txtQuarter1.Text.Trim();
                        updateQuarterlyPerformance.Quarter2Achieved = txtQuarter1.Text.Trim();
                        updateQuarterlyPerformance.Quarter3Achieved = txtQuarter1.Text.Trim();
                        updateQuarterlyPerformance.Quarter4Achieved = txtQuarter1.Text.Trim();
                        //appraisal dates details
                        if (txtQuarter1AppraisalDate.Text.Trim() != "__/___/____")
                            updateQuarterlyPerformance.Quarter1AppraisalDate = Convert.ToDateTime(txtQuarter1AppraisalDate.Text.Trim()).Date;
                        else
                            updateQuarterlyPerformance.Quarter1AppraisalDate = null;
                        if (txtQuarter2AppraisalDate.Text.Trim() != "__/___/____")
                            updateQuarterlyPerformance.Quarter2AppraisalDate = Convert.ToDateTime(txtQuarter2AppraisalDate.Text.Trim()).Date;
                        else
                            updateQuarterlyPerformance.Quarter2AppraisalDate = null;
                        if (txtQuarter3AppraisalDate.Text.Trim() != "__/___/____")
                            updateQuarterlyPerformance.Quarter3AppraisalDate = Convert.ToDateTime(txtQuarter3AppraisalDate.Text.Trim()).Date;
                        else
                            updateQuarterlyPerformance.Quarter3AppraisalDate = null;
                        if (txtQuarter4AppraisalDate.Text.Trim() != "__/___/____")
                            updateQuarterlyPerformance.Quarter4AppraisalDate = Convert.ToDateTime(txtQuarter4AppraisalDate.Text.Trim()).Date;
                        else
                            updateQuarterlyPerformance.Quarter4AppraisalDate = null;
                        //appraised by details
                        if (ddlQuarter1AppraisedBy.SelectedIndex != 0)
                            updateQuarterlyPerformance.Quarter1AppraisedByID = _hrClass.ReturnGuid(ddlQuarter1AppraisedBy.SelectedValue);
                        else
                            updateQuarterlyPerformance.Quarter1AppraisedByID = null;
                        if (ddlQuarter2AppraisedBy.SelectedIndex != 0)
                            updateQuarterlyPerformance.Quarter2AppraisedByID = _hrClass.ReturnGuid(ddlQuarter2AppraisedBy.SelectedValue);
                        else
                            updateQuarterlyPerformance.Quarter2AppraisedByID = null;

                        if (ddlQuarter3AppraisedBy.SelectedIndex != 0)
                            updateQuarterlyPerformance.Quarter3AppraisedByID = _hrClass.ReturnGuid(ddlQuarter3AppraisedBy.SelectedValue);
                        else
                            updateQuarterlyPerformance.Quarter3AppraisedByID = null;
                        if (ddlQuarter4AppraisedBy.SelectedIndex != 0)
                            updateQuarterlyPerformance.Quarter4AppraisedByID = _hrClass.ReturnGuid(ddlQuarter4AppraisedBy.SelectedValue);
                        else
                            updateQuarterlyPerformance.Quarter4AppraisedByID = null;
                        //comments details
                        updateQuarterlyPerformance.Quarter1Comments = txtQuarter1Comments.Text.Trim();
                        updateQuarterlyPerformance.Quarter2Comments = txtQuarter2Comments.Text.Trim();
                        updateQuarterlyPerformance.Quarter3Comments = txtQuarter3Comments.Text.Trim();
                        updateQuarterlyPerformance.Quarter4Comments = txtQuarter4Comments.Text.Trim();
                        updateQuarterlyPerformance.DateRecorded = DateTime.Now;
                        db.SubmitChanges();
                        DisplayAnnualPerformance(_staffID, _performanceYear);//refresh the grid view
                        ExpandACollapsiblePanelExtenderAccessed();
                        _hrClass.LoadHRManagerMessageBox(2, "Quarterly key performance indicator details have been updated!", this, PanelStaffPerformance);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString() + " Please try again.", this, PanelStaffPerformance);
                return;
            }
        }
        //save quarterly details
        protected void lnkBtnSaveKeyPerformanceIndicator_Click(object sender, EventArgs e)
        {
            SaveQuarterlyKeyPerformanceIndicator();
            return;
        }
        //update quarterly details
        protected void lnkBtnUpdateKeyPerformanceIndicator_Click(object sender, EventArgs e)
        {
            UpdateQuarterlyKeyPerformanceIndicator();
            return;
        }
        //clear quarterly details
        protected void lnkBtnClearKeyPerformanceIndicator_Click(object sender, EventArgs e)
        {
            //if (HiddenFieldAnnualPerformanceID.Value != null)
            //{
            LoadAddNewKeyPerformanceIndicator(Convert.ToInt32(HiddenFieldAnnualPerformanceID.Value));
            return;
            //}
            //else
            //{

            //}
        }
        //display annual performance
        private void DisplayAnnualPerformance(Guid _staffID, string _year)
        {
            try
            {
                object _display;
                //_display = db.AnnualPerformances.Select(p => p);
                _display = db.PROC_StaffAnnualPerformance(_staffID, _year);
                gvAnnualPerformance.DataSourceID = null;
                gvAnnualPerformance.DataSource = _display;
                gvAnnualPerformance.DataBind();
                return;
            }
            catch { }
        }
        //detailed annual performance row data bound
        protected void gvAnnualPerformance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    int _annualPerformanceID = Convert.ToInt32(gvAnnualPerformance.DataKeys[e.Row.RowIndex].Value.ToString());
                    //check if the performance objective has key performance indicators
                    if (db.QuarterlyPerformances.Any(p => p.AnnualPerformanceID == Convert.ToInt32(_annualPerformanceID)))
                    {
                        GridView _gvQuarterlyPerformance = e.Row.FindControl("gvQuarterlyPerformance") as GridView;
                        DisplayQuarterlyPerformanceGridView(_annualPerformanceID, _gvQuarterlyPerformance);//load the key performance indicator griview data
                        return;
                    }
                    else
                    {
                        //disable the edit and delete budget item buttons
                        Button _btnEditKeyPerformanceIndicator = (Button)e.Row.FindControl("btnEditKeyPerformanceIndicator");
                        _btnEditKeyPerformanceIndicator.Visible = false;// hide the edit item button
                        Button _btnDeleteKeyPerformanceIndicator = (Button)e.Row.FindControl("btnDeleteKeyPerformanceIndicator");
                        _btnDeleteKeyPerformanceIndicator.Visible = false;//hide delete key performance 

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Failed in loading applicant sheduled for interview !" + ex.Message.ToString(), this, PanelStaffPerformance);
                return;
            }
        }
        //load key performance indicators
        private void DisplayQuarterlyPerformanceGridView(int _annualPerformanceID, GridView _gvQuarterlyPerformance)
        {
            _gvQuarterlyPerformance.DataSourceID = null;
            //object _getKeyPerformanceIndicators = db.QuarterlyPerformances.Where(p => p.AnnualPerformanceID == Convert.ToInt32(_annualPerformanceID)).OrderBy(p => p.KeyIndicator);//
            object _getKeyPerformanceIndicators = db.PROC_StaffQuarterlyPerformance(_annualPerformanceID);//
            _gvQuarterlyPerformance.ToolTip = _annualPerformanceID.ToString();
            _gvQuarterlyPerformance.DataSource = _getKeyPerformanceIndicators;
            _gvQuarterlyPerformance.DataBind();
        }
        //row command for annual performance 
        protected void gvAnnualPerformance_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("ViewKeyPerformanceIndicator") == 0)
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvAnnualPerformanceRowIndex.Value = _selectedRow.RowIndex.ToString();

                    GridView _gvQuarterlyPerformance = gvAnnualPerformance.Rows[_rowIndex].FindControl("gvQuarterlyPerformance") as GridView;
                    HiddenField _HFQuarterlyPerformanceID = (HiddenField)_gvQuarterlyPerformance.FindControl("HiddenFieldQuarterlyPerformanceID");

                    //load key performance indicator being edited
                    LoadQuarterlyPerformanceControlsForEdit(Convert.ToInt32(_HFQuarterlyPerformanceID.Value));
                    return;
                }
                else if (e.CommandName.CompareTo("AddKeyPerformanceIndicator") == 0)//add new key performance indicator
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvAnnualPerformanceRowIndex.Value = _selectedRow.RowIndex.ToString();
                    HiddenField _hfAnnualPerformanceID = gvAnnualPerformance.Rows[_rowIndex].FindControl("HiddenFieldAnnualPerformanceID") as HiddenField;
                    string _annualPerformanceID = _hfAnnualPerformanceID.Value;
                    // ModalPopupExtenderAddKeyPerformanceIndicator.Show();
                    //display controls for adding a new interview appointment
                    LoadAddNewKeyPerformanceIndicator(Convert.ToInt32(_annualPerformanceID));
                    return;
                }
                else if (e.CommandName.CompareTo("EditKeyPerformanceIndicator") == 0)//edit  keyvperformance indicator
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvAnnualPerformanceRowIndex.Value = _selectedRow.RowIndex.ToString();
                    //get child grid view(i.e. interview appointment applicant grid view)
                    GridView _gvQuarterlyPerformance = gvAnnualPerformance.Rows[_rowIndex].FindControl("gvQuarterlyPerformance") as GridView;
                    //load edit details for the selected quarterly performance
                    LoadEditKeyPerformanceIndicatorControls(_gvQuarterlyPerformance);
                    return;
                }
                else if (e.CommandName.CompareTo("DeleteKeyPerformanceIndicator") == 0)//delete key performance indicator
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvAnnualPerformanceRowIndex.Value = _selectedRow.RowIndex.ToString();
                    //get child grid view(i.e. interview appointment applicant grid view)
                    GridView _gvQuarterlyPerformance = gvAnnualPerformance.Rows[_rowIndex].FindControl("gvQuarterlyPerformance") as GridView;
                    DeleteKeyPerformanceIndicator(_gvQuarterlyPerformance);
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error ocurred ! " + ex.Message.ToString(), this, PanelStaffPerformance);
                return;
            }

        }

        //load add performance key indicator
        private void LoadAddNewKeyPerformanceIndicator(int _annualPerfomanceID)
        {
            _hrClass.ClearEntryControls(PanelAddKeyPerformanceIndicator);
            AnnualPerformance getAnnualPerformance = db.AnnualPerformances.Single(p => p.AnnualPerformanceID == _annualPerfomanceID);
            lbAddKeyPerformanceIndicatorHeader.Text = "Add Key Performance Indicator againist " + getAnnualPerformance.PerformanceObjective + " performance objective";
            HiddenFieldAnnualPerformanceID.Value = _annualPerfomanceID.ToString();
            lnkBtnSaveKeyPerformanceIndicator.Visible = true;
            lnkBtnUpdateKeyPerformanceIndicator.Enabled = false;
            lnkBtnUpdateKeyPerformanceIndicator.Visible = true;
            lnkBtnClearKeyPerformanceIndicator.Visible = true;
            ImageAddKeyPerformanceIndicatorHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            ModalPopupExtenderAddKeyPerformanceIndicator.Show();
            return;
        }
        //load interface for editing an key performance indicator
        private void LoadEditKeyPerformanceIndicatorControls(GridView _gvKeyPerformanceIndicator)
        {
            if (!_hrClass.isGridviewItemSelected(_gvKeyPerformanceIndicator))
            {
                _hrClass.LoadHRManagerMessageBox(1, "There is no key performance indicator selected. Select the key performance indicator thet you want to edit!", this, PanelStaffPerformance);
                return;
            }
            else
            {
                int x = 0;
                foreach (GridViewRow _grvRow in _gvKeyPerformanceIndicator.Rows)
                {
                    CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                    if (_cb.Checked) ++x;
                }
                if (x > 1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one key performance indicator. Select only one key performance indicator to edit at a time!", this, PanelStaffPerformance);
                    return;
                }
                else
                {
                    foreach (GridViewRow _grvRow in _gvKeyPerformanceIndicator.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                        if (_cb.Checked)
                        {
                            HiddenField _HFQuarterlyPerformanceID = (HiddenField)_grvRow.FindControl("HiddenFieldQuarterlyPerformanceID");

                            //load key performance indicator being edited
                            LoadQuarterlyPerformanceControlsForEdit(Convert.ToInt32(_HFQuarterlyPerformanceID.Value));
                            return;
                        }
                    }
                }
            }
        }
        //load quarterly performance controls for edit purpose
        private void LoadQuarterlyPerformanceControlsForEdit(int _quarterlyPerformanceID)
        {
            QuarterlyPerformance getKeyIndicator = db.QuarterlyPerformances.Single(p => p.QuarterlyPerformanceID == _quarterlyPerformanceID);
            HiddenFieldQuarterlyPerformanceID.Value = _quarterlyPerformanceID.ToString();
            HiddenFieldAnnualPerformanceID.Value = getKeyIndicator.AnnualPerformanceID.ToString();
            txtKeyPerformanceIndicator.Text = getKeyIndicator.KeyIndicator;

            txtQuarter1.Text = getKeyIndicator.Quarter1Achieved;
            txtQuarter2.Text = getKeyIndicator.Quarter2Achieved;
            txtQuarter3.Text = getKeyIndicator.Quarter3Achieved;
            txtQuarter4.Text = getKeyIndicator.Quarter4Achieved;
            //appraisal dates details
            if (getKeyIndicator.Quarter1AppraisalDate != null)
                txtQuarter1AppraisalDate.Text = _hrClass.ShortDateDayStart(getKeyIndicator.Quarter1AppraisalDate.ToString());
            else
                txtQuarter1AppraisalDate.Text = "";

            if (getKeyIndicator.Quarter2AppraisalDate != null)
                txtQuarter2AppraisalDate.Text = _hrClass.ShortDateDayStart(getKeyIndicator.Quarter2AppraisalDate.ToString());
            else
                txtQuarter2AppraisalDate.Text = "";

            if (getKeyIndicator.Quarter3AppraisalDate != null)
                txtQuarter3AppraisalDate.Text = _hrClass.ShortDateDayStart(getKeyIndicator.Quarter3AppraisalDate.ToString());
            else
                txtQuarter3AppraisalDate.Text = "";
            if (getKeyIndicator.Quarter4AppraisalDate != null)
                txtQuarter4AppraisalDate.Text = _hrClass.ShortDateDayStart(getKeyIndicator.Quarter4AppraisalDate.ToString());
            else
                txtQuarter4AppraisalDate.Text = "";
            //appraised by details
            if (getKeyIndicator.Quarter1AppraisedByID != null)
                ddlQuarter1AppraisedBy.SelectedValue = getKeyIndicator.Quarter1AppraisedByID.ToString();
            else
                ddlQuarter1AppraisedBy.SelectedIndex = 0;

            if (getKeyIndicator.Quarter2AppraisedByID != null)
                ddlQuarter2AppraisedBy.SelectedValue = getKeyIndicator.Quarter2AppraisedByID.ToString();
            else
                ddlQuarter2AppraisedBy.SelectedIndex = 0;

            if (getKeyIndicator.Quarter3AppraisedByID != null)
                ddlQuarter3AppraisedBy.SelectedValue = getKeyIndicator.Quarter3AppraisedByID.ToString();
            else
                ddlQuarter3AppraisedBy.SelectedIndex = 0;
            if (getKeyIndicator.Quarter4AppraisedByID != null)
                ddlQuarter4AppraisedBy.SelectedValue = getKeyIndicator.Quarter4AppraisedByID.ToString();
            else
                ddlQuarter4AppraisedBy.SelectedIndex = 0;

            //comments details
            txtQuarter1Comments.Text = getKeyIndicator.Quarter1Comments;
            txtQuarter2Comments.Text = getKeyIndicator.Quarter2Comments;
            txtQuarter3Comments.Text = getKeyIndicator.Quarter3Comments;
            txtQuarter4Comments.Text = getKeyIndicator.Quarter4Comments;

            lnkBtnSaveKeyPerformanceIndicator.Visible = false;
            lnkBtnUpdateKeyPerformanceIndicator.Enabled = true;
            lbAddKeyPerformanceIndicatorHeader.Text = "Edit Key Performance Indicator";
            ImageAddKeyPerformanceIndicatorHeader.ImageUrl = "~/Images/icons/Medium/Modify.png";
            ModalPopupExtenderAddKeyPerformanceIndicator.Show();
            return;
        }
        //delete key performance indicator applicant details
        private void DeleteKeyPerformanceIndicator(GridView _gvKeyPerformanceIndicator)
        {
            if (!_hrClass.isGridviewItemSelected(_gvKeyPerformanceIndicator))
            {
                _hrClass.LoadHRManagerMessageBox(1, "There is no key performance indicator selected. Select the key performance indicator thet you want to delete!", this, PanelStaffPerformance);
                return;
            }
            else
            {
                foreach (GridViewRow _grvRow in _gvKeyPerformanceIndicator.Rows)
                {
                    CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                    if (_cb.Checked)
                    {
                        HiddenField _HFQuarterlyPerformanceID = (HiddenField)_grvRow.FindControl("HiddenFieldQuarterlyPerformanceID");

                        int _quarterlyPerformanceID = Convert.ToInt32(_HFQuarterlyPerformanceID.Value);

                        //delete a key performance indicator
                        QuarterlyPerformance deleteQuarterlyPerformance = db.QuarterlyPerformances.Single(p => p.QuarterlyPerformanceID == _quarterlyPerformanceID);
                        db.QuarterlyPerformances.DeleteOnSubmit(deleteQuarterlyPerformance);
                        db.SubmitChanges();
                    }
                }
                _hrClass.LoadHRManagerMessageBox(1, "Key performance indicator has been deleted.", this, PanelStaffPerformance);
                //refresg
               // gvAnnualPerformance.DataBind();
                ExpandACollapsiblePanelExtenderAccessed();
                return;
            }
        }
        //expand a performance obnectibe collapsible panel extender
        private void ExpandACollapsiblePanelExtenderAccessed()
        {
            //get the collapsible panel that was accessed and expand it
            int _gvRowIndex = Convert.ToInt32(HiddenFieldAccessedgvAnnualPerformanceRowIndex.Value);
            CollapsiblePanelExtender _collapsiblePanel = gvAnnualPerformance.Rows[_gvRowIndex].FindControl("CollapsiblePanelExtenderExpandCollapseDetails") as CollapsiblePanelExtender;
            _collapsiblePanel.Collapsed = false;
            _collapsiblePanel.ClientState = "false";
            return;
        }
    }
}