﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;
namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Master : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            //ContentPlaceHolder _mainContent = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
            //Panel _staffModuleHeaderPanel = (Panel)_mainContent.FindControl("PanelStaffModuleHeader");
            //_staffModuleHeaderPanel.Visible = false;
            if (!IsPostBack)
            {
                DisplayStaffRecords();//load staff records
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //load staff records
        private void DisplayStaffRecords()
        {
            object _display;
            _display = db.StaffMaster_Views.Select(p => p).OrderBy(p => p.StaffName);
            gvStaffMaster.DataSourceID = null;
            gvStaffMaster.DataSource = _display;
            Session["gvStaffMasterData"] = _display;
            gvStaffMaster.DataBind();
            return;
        }
        //check if a staff is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvStaffMaster))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit !", this, PanelStaffMaster);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvStaffMaster.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one recoed to edit at a time !", this, PanelStaffMaster);
                        return;
                    }
                    foreach (GridViewRow _grv in gvStaffMaster.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFStaffID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit staff details
                            Response.Redirect(string.Format("~/HR_Module/Staff_Record.aspx?ref={0}", _HFStaffID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading staff details for edit! " + ex.Message.ToString(), this, PanelStaffMaster);
            }
        }
        //method for searching for staff record by use of reference number, name, id number or phone number
        private void SearchForStaffRecord()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.StaffMaster_Views.Where(p => (p.StaffRef.ToLower() + p.StaffName.ToLower() + p.IDNumber.ToLower() + p.PhoneNumber.ToLower()).Contains(_searchText)).OrderBy(p => p.StaffName);
                gvStaffMaster.DataSourceID = null;
                gvStaffMaster.DataSource = _searchDetails;
                Session["StaffMasterData"] = _searchDetails;
                gvStaffMaster.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffMaster);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffRecord();
            return;
        }
        protected void gvStaffMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvStaffMaster.PageIndex = e.NewPageIndex;
            Session["gvStaffMasterPageIndex"] = e.NewPageIndex;//get page index
            gvStaffMaster.DataSource = (object)Session["gvStaffMasterData"];
            gvStaffMaster.DataBind();
            return;
        }
        protected void gvStaffMaster_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvStaffMasterPageIndex"] == null)//check if page index is empty
                {
                    gvStaffMaster.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvStaffMasterPageIndex"]);
                    gvStaffMaster.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //load delete staff record
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvStaffMaster))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelStaffMaster);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvStaffMaster.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffMaster);
                return;
            }
        }
        //delete selected staff record
        private void DeleteStaffRecord()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvStaffMaster.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFStaffID = (HiddenField)_grv.FindControl("HiddenField1");
                        Guid _staffID = _hrClass.ReturnGuid(_HFStaffID.Value);

                        StaffMaster deleteStaff = db.StaffMasters.Single(p => p.StaffID == _staffID);


                        //check existing relationships
                        //check if there is any work experience records available
                        if (db.StaffWorkExperiences.Any(p => p.StaffID == _staffID))
                        {
                            foreach (StaffWorkExperience deleteWorkExperience in db.StaffWorkExperiences.Where(p => p.StaffID == _staffID))
                            {
                                db.StaffWorkExperiences.DeleteOnSubmit(deleteWorkExperience);
                            }
                        }
                        //check if there any  education background records
                        if (db.StaffEducationBackgrounds.Any(p => p.StaffID == _staffID))
                        {
                            foreach (StaffEducationBackground deleteEducationBackground in db.StaffEducationBackgrounds.Where(p => p.StaffID == _staffID))
                            {
                                db.StaffEducationBackgrounds.DeleteOnSubmit(deleteEducationBackground);
                            }
                        }
                        //check if there are any staff docuuments saved against the staff
                        if (db.StaffDocs.Any(p => p.staffdoc_uiStaffID == _staffID))
                        {
                            foreach (StaffDoc deleteStaffDoc in db.StaffDocs.Where(p => p.staffdoc_uiStaffID == _staffID))
                            {
                                db.StaffDocs.DeleteOnSubmit(deleteStaffDoc);
                            }
                        }
                        //other relationships to be ignored and through the error

                        //delete the staff record if there is no conflict
                        if (db.StaffMasterAdditionals.Any(p => p.StaffID == _staffID))
                        {
                            StaffMasterAdditional deleteStaffMasterAdditional = db.StaffMasterAdditionals.Single(p => p.StaffID == _staffID);
                            db.StaffMasterAdditionals.DeleteOnSubmit(deleteStaffMasterAdditional);
                        }
                        db.StaffMasters.DeleteOnSubmit(deleteStaff);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayStaffRecords();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected record has been deleted.", this, PanelStaffMaster);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected records have been deleted.", this, PanelStaffMaster);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelStaffMaster);
                return;
            }
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteStaffRecord();//delete selected records(s)
            return;
        }
        //load employee  profile/details when his/her name is clicked
        protected void gvStaffMaster_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("ViewStaffDetails") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFStaffID = (HiddenField)gvStaffMaster.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("ViewStaffDetails") == 0)//view staff details
                    {
                        Response.Redirect(string.Format("~/HR_Module/Staff_Record.aspx?ref={0}", _HFStaffID.Value));
                        //get session page  index fo gvstatffmaster
                        Session["gvStaffMasterPageIndex"] = gvStaffMaster.PageIndex;
                        return;
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }

    }
}