﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.HR_Module
{
    public partial class Attendance : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFromDate.Attributes.Add("readonly", "readonly");//make attendance from date texbox readony 
                txtToDate.Attributes.Add("readonly", "readonly");//make attendance to date text box readonly
                DisplayCurrentWeekStartAndEndDate();//load current week start and end dates
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffAttendance(_staffID);//load staff attendance details
                    return;
                }
            }
            catch { }
        }
        //display staff attendance by selected staff id
        private void DisplayStaffAttendance(Guid _staffID)
        {
            try
            {
                ////load staff image photo 
                //_hrClass.LoadStaffPhoto(_staffID, ImageStaffPhoto);
                //display attendance for the weekek
                DisplayEmployeeAttendanceForCurrentWeek(_staffID);
                //acctivate/disable buttons
                if (_staffID == Guid.Empty)
                {
                    //dissable the buttons
                    ImageButtonFromDate.Enabled = false;
                    ImageButtonToDate.Enabled = false;
                    btnViewAllAttendance.Enabled = false;
                }
                else
                {
                    //enable the buttons
                    ImageButtonFromDate.Enabled = true;
                    ImageButtonToDate.Enabled = true;
                    btnViewAllAttendance.Enabled = true;
                }
                return;
            }
            catch { }
        }
        //search for attendance by staff reference number
        private void SearchForAttendanceByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber,ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAttendance(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAttendance(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelAttendance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAttendance);
                return;
            }
        }
        //search attendence by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForAttendanceByStaffReference();
            return;
        }
        //search for attendance by staff namae
        private void SearchForAttendanceByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber,ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAttendance(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAttendance(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelAttendance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAttendance);
                return;
            }
        }
        //search attendace by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForAttendanceByStaffName();
            return;
        }
        //search for attendance by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber,ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAttendance(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAttendance(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelAttendance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAttendance);
                return;
            }
        }
        //search for attendance by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber,ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffAttendance(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffAttendance(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelAttendance);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAttendance);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //load date with time span for the current week
        private void DisplayCurrentWeekStartAndEndDate()
        {
            try
            {
                DayOfWeek day = DateTime.Now.DayOfWeek;//get current day
                int days = day - DayOfWeek.Monday;//get day wher monday of the current week falls
                DateTime weekStartDate = DateTime.Now.AddDays(-days);//get date for monday of the current week
                DateTime weekEndDate = weekStartDate.AddDays(6);//get date for sunday of the current week by adding s 6 days to week start date
                txtFromDate.Text = _hrClass.ShortDateDayStart(weekStartDate.ToString());
                txtToDate.Text = _hrClass.ShortDateDayStart(weekEndDate.ToString());

            }
            catch { }
        }

        //display employee attendance for the currnt week
        private void DisplayEmployeeAttendanceForCurrentWeek(Guid _staffID)
        {
            DayOfWeek day = DateTime.Now.DayOfWeek;//get current day
            int days = day - DayOfWeek.Monday;//get day wher monday of the current week falls
            DateTime weekStartDate = DateTime.Now.AddDays(-days);//get date for monday of the current week
            DateTime weekEndDate = weekStartDate.AddDays(6);//get date for sunday of the current week by adding s 6 days to week start date
            txtFromDate.Text = _hrClass.ShortDateDayStart(weekStartDate.ToString());
            txtToDate.Text = _hrClass.ShortDateDayStart(weekEndDate.ToString());
            //display attendance details for the employee during the current week
            LoadEmployeeAttendancePerDuration(_staffID, weekStartDate, weekEndDate);
            HiddenFieldStaffID.Value = _staffID.ToString();
            return;
        }

        //load employee attendance per duration 
        private void LoadEmployeeAttendancePerDuration(Guid _staffID, DateTime fromDate, DateTime toDate)
        {
            try
            {
                //get staff refference number asssoicated with the staff id
                string _staffRef = _hrClass.GetStaffReferenceByStaffID(_staffID);
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("Date", typeof(string)));//get leave types
                dt.Columns.Add(new DataColumn("CheckType", typeof(string)));
                dt.Columns.Add(new DataColumn("Time", typeof(string)));
                dt.Columns.Add(new DataColumn("CheckPlace", typeof(string)));
                dt.Columns.Add(new DataColumn("MinutesLate", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //}
                //get time and attendance for the logged employee
                foreach (PROC_SingleEmployeeCheckInCheckOutPerDurationResult _attendance in db.PROC_SingleEmployeeCheckInCheckOutPerDuration(_staffRef, fromDate.Date, toDate.Date))
                {
                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = _attendance.DATE___TIME.Value.ToLongDateString();
                    dr[2] = _attendance.CHECK_TYPE;
                    string _time = _attendance.DATE___TIME.Value.TimeOfDay.ToString();
                    if (_time == "00:00:00")
                        _time = "N/A";
                    dr[3] = _time;
                    string _checkPlace = "N/A";
                    if (_attendance.CHECK_PLACE != null)
                        _checkPlace = _hrClass.ConvertFirstStringLettersToUpper(_attendance.CHECK_PLACE);
                    dr[4] = _checkPlace;
                    dr[5] = _attendance.MINUTES_LATE.ToString();
                    dt.Rows.Add(dr);
                    countRows++;
                }
                Session["gvAttendanceData"] = dt;
                gvAttendance.DataSource = dt;
                gvAttendance.DataBind();
                return;
            }
            catch { }
        }
        //page index changing
        protected void gvAttendance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAttendance.PageIndex = e.NewPageIndex;
                Session["gvAttendancePageIndex"] = e.NewPageIndex;
                gvAttendance.DataSource = (object)Session["gvAttendanceData"];
                gvAttendance.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }
        protected void gvEmployeeAttendanceDetails_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvAttendancePageIndex"] == null)//check if page index is empty
                {
                    gvAttendance.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvAttendancePageIndex"]);
                    gvAttendance.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //load all attendance details for the employee 
        private void LoadAllAttendanceDetailsForTheEmployee(Guid _staffID)
        {
            //get logged employee's number
            //get staff refference number asssoicated with the staff id
            string _staffRef = _hrClass.GetStaffReferenceByStaffID(_staffID);
            //check if there are any attendance details for the selected staff
            if (db.CheckInCheckOuts.Any(p => p.checkincheckout_vEmployeeNo == _staffRef))
            {
                //get current date
                DateTime currentDate = DateTime.Now.Date;
                txtToDate.Text = _hrClass.ShortDateDayStart(currentDate.ToString());
                //get minimum date when the first ever check in or check out record was saved
                DateTime minimumDate = Convert.ToDateTime(db.CheckInCheckOuts.Where(p => p.checkincheckout_vEmployeeNo == _staffRef).Min(p => Convert.ToDateTime(p.checkincheckout_vCheckDate)));
                txtFromDate.Text = _hrClass.ShortDateDayStart(minimumDate.ToString());
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("Date", typeof(string)));//get leave types
                dt.Columns.Add(new DataColumn("CheckType", typeof(string)));
                dt.Columns.Add(new DataColumn("Time", typeof(string)));
                dt.Columns.Add(new DataColumn("CheckPlace", typeof(string)));
                dt.Columns.Add(new DataColumn("MinutesLate", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //get time and attendance for the logged employee
                foreach (PROC_SingleEmployeeCheckInCheckOutPerDurationResult _attendance in db.PROC_SingleEmployeeCheckInCheckOutPerDuration(_staffRef, minimumDate.Date, currentDate.Date))
                {
                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = _attendance.DATE___TIME.Value.ToLongDateString();
                    dr[2] = _attendance.CHECK_TYPE;
                    string _time = _attendance.DATE___TIME.Value.TimeOfDay.ToString();
                    if (_time == "00:00:00")
                        _time = "N/A";
                    dr[3] = _time;
                    string _checkPlace = "N/A";
                    if (_attendance.CHECK_PLACE != null)
                        _checkPlace = _hrClass.ConvertFirstStringLettersToUpper(_attendance.CHECK_PLACE);
                    dr[4] = _checkPlace;
                    dr[5] = _attendance.MINUTES_LATE.ToString();
                    dt.Rows.Add(dr);
                    countRows++;
                }
                Session["gvAttendanceData"] = dt;
                gvAttendance.DataSource = dt;
                gvAttendance.DataBind();
            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, "There are no attendance details associoated with the employee!", this, PanelAttendance); return;
            }

        }
        protected void btnViewAllAttendance_Click(object sender, EventArgs e)
        {
            try
            {
                LoadAllAttendanceDetailsForTheEmployee(_hrClass.ReturnGuid(HiddenFieldStaffID.Value));
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }
        //display employee attendnce when the from date text is changed
        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    DateTime fromDate = Convert.ToDateTime(txtFromDate.Text).Date;
                    DateTime toDate = Convert.ToDateTime(txtToDate.Text).Date;
                    LoadEmployeeAttendancePerDuration(_hrClass.ReturnGuid(HiddenFieldStaffID.Value), fromDate, toDate);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAttendance); return;
            }
        }



        //load test attendance
        private void LoadTestAttendance()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("LeaveID", typeof(int)));
            dt.Columns.Add(new DataColumn("FromDate", typeof(string)));
            dt.Columns.Add(new DataColumn("ToDate", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfDays", typeof(string)));
            dt.Columns.Add(new DataColumn("NumberOfHours", typeof(string)));
            dt.Columns.Add(new DataColumn("Reason", typeof(string)));
            DataRow dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "01/Oct/2014";
            dr[2] = "03/Oct/2014";
            dr[3] = "3";
            dr[4] = "4";
            dr[5] = "Sick";
            dt.Rows.Add(dr);

            DataRow dr2 = dt.NewRow();
            dr2[0] = 2;
            dr2[1] = "11/Aug/2014";
            dr2[2] = "17/Aug/2014";
            dr2[3] = "5";
            dr2[4] = "3";
            dr2[5] = "No Reason";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3[0] = 3;
            dr3[1] = "22/May/2014";
            dr3[2] = "29/May/2014";
            dr3[3] = "8";
            dr3[4] = "0";
            dr3[5] = "Attending Funeral";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4[0] = 4;
            dr4[1] = "25/Feb/2014";
            dr4[2] = "25/Feb/2014";
            dr4[3] = "1";
            dr4[4] = "0";
            dr4[5] = "No Reason";
            dt.Rows.Add(dr4);
            gvAttendance.DataSource = dt;
            gvAttendance.DataBind();
        }
    }
}