﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.HR_Module
{
    public partial class Training_Attended : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayTrainingAttended(_staffID);//load staff training attended details
                    return;
                }
            }
            catch { }
        }
        //display training attended by selected staff
        private void DisplayTrainingAttended(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffTrainingAttended(_staffID);
                gvTrainingAttended.DataSourceID = null;
                gvTrainingAttended.DataSource = _display;
                gvTrainingAttended.DataBind();

                //load staff image photo 
                //_hrClass.LoadStaffPhoto(_staffID, ImageStaffPhoto);
                return;
            }
            catch { }
        }
        //search for training attended by staff reference number
        private void SearchForTrainingAttendedByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayTrainingAttended(_StaffID);
                    return;
                }
                else
                {
                    DisplayTrainingAttended(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelTrainingAttended);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingAttended);
                return;
            }
        }


        //search training attended by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForTrainingAttendedByStaffReference();
            return;
        }
        //search for training attended by staff namae
        private void SearchForTrainingAttendedByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayTrainingAttended(_StaffID);
                    return;
                }
                else
                {
                    DisplayTrainingAttended(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelTrainingAttended);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingAttended);
                return;
            }
        }
        //search training attended by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForTrainingAttendedByStaffName();
            return;
        }
        //search for staff by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayTrainingAttended(_StaffID);
                    return;
                }
                else
                {
                    DisplayTrainingAttended(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelTrainingAttended);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingAttended);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayTrainingAttended(_StaffID);
                    return;
                }
                else
                {
                    DisplayTrainingAttended(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelTrainingAttended);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingAttended);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
      
    }

}