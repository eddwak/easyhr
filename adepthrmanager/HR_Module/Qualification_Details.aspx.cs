﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;

namespace AdeptHRManager.HR_Module
{
    public partial class Qualification_Details : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItemsWithTableOrder(ddlEducationBackgroundLevelAttained, 8000, "Attained Level");//display attained level(8000) available
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
            //add delete record event to delete a qualification record
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
           ucConfirmComputer.lnkBtnYesConfirmationClick+=new EventHandler(ucConfirmComputer_lnkBtnYesConfirmationClick);
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffQualification(_staffID);//load qualification details
                    DisplayComputerProficiency(_staffID);//load computer profeciency details

                    return;
                }
            }
            catch { }
        }
        //display staff qualification details
        private void DisplayStaffQualification(Guid _staffID)
        {
            DisplayEducationBackground();//display education
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffQualification(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffQualification(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelQualificationDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelQualificationDetails);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffQualification(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffQualification(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelQualificationDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelQualificationDetails);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffQualification(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffQualification(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelQualificationDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelQualificationDetails);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffQualification(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffQualification(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelQualificationDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelQualificationDetails);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //clear add education background controls
        private void ClearAddEducationBackgroundControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddEducationBackground);
            HiddenFieldEducationBackgroundID.Value = "";
            lnkBtnSaveEducationBackground.Visible = true;
            lnkBtnUpdateEducationBackground.Enabled = false;
            lnkBtnUpdateEducationBackground.Visible = true;
            lnkBtnClearEducationBackground.Visible = true;
            lbAddEducationBackgroundHeader.Text = "Add Qualification";
            ImageAddEducationBackgroundHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbEducationBackgroundError.Text = "";
            lbEducationBackgroundInfo.Text = "";
        }
        //load add education background
        protected void LinkButtonNewEducationBackground_Click(object sender, EventArgs e)
        {
            //_hrClass.GenerateMonths(ddlEducationBackgroundCompletionDateMonth);//start date months
           
            ClearAddEducationBackgroundControls();
            ModalPopupExtenderAddEducationBackground.Show();
            return;
        }
        //validate add education backgrofund record
        private string ValidateAddEducationBackgroundControls()
        {
            if (HiddenFieldStaffID.Value.Trim() == "")
                return "Search for the employee that you want to record the education details against!";
            if (rblEducationBackgroundQualificationType.SelectedIndex == -1)
            {
                rblEducationBackgroundQualificationType.Focus();
                return "Select  qualification type!";
            }
            else if (txtEducationBackgroundInstitutionName.Text.Trim() == "")
            {
                txtEducationBackgroundInstitutionName.Focus();
                return "Enter institution name!";
            }
            else if (ddlEducationBackgroundLevelAttained.SelectedIndex == 0)
            {
                ddlEducationBackgroundLevelAttained.Focus();
                return "Select level attained!";
            }
            else if (txtAward.Text.Trim() == "")
            {
                txtAward.Focus();
                return "Enter award attained";
            }
            else if (rblEducationBackgroundStatus.SelectedIndex == -1)
            {
                rblEducationBackgroundStatus.Focus();
                return "Select status";
            }
           
            else return "";
        }
        //method for saving education background 
        private void SaveEducationBackground()
        {
            ModalPopupExtenderAddEducationBackground.Show();
            try
            {
                lbEducationBackgroundInfo.Text = "";
                string _error = ValidateAddEducationBackgroundControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelQualificationDetails);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _qualificationType = rblEducationBackgroundQualificationType.SelectedValue;
                    string _status = rblEducationBackgroundStatus.SelectedValue;
                    string _institutionName = txtEducationBackgroundInstitutionName.Text.Trim();
                    int _attainedLevelID = Convert.ToInt32(ddlEducationBackgroundLevelAttained.SelectedValue);
                    string _award = txtAward.Text.Trim();
                    string _PeriodFrom = txtPeriodFrom.Text.Trim();
                    string _PeriodTo = txtPeriodTo.Text.Trim();
                    string _ProfessionalMembership = txtProfessionalMembership.Text.Trim();
                    //string _completionMonth = ddlEducationBackgroundCompletionDateMonth.SelectedValue;
                    string _gradeAttained = txtEducationBackgroundGrade.Text.Trim();
                    Boolean _isHighestQualification = cbIsItHighestQualification.Checked;
                    //check if the record being saved already exists
                    //if (db.StaffEducationBackgrounds.Any(p => p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.ProgramCourse == _programmeCourse && p.CompletionMonth == _completionMonth && p.CompletionYear == _completionYear))
                    if (db.StaffEducationBackgrounds.Any(p => p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.Award == _award))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "The education record that you are trying to save is already saved.", this, PanelQualificationDetails);
                        return;
                    }

                    else
                    {
                        //check if there is another highest qualification saved against the staff
                        if (_isHighestQualification == true && (db.StaffEducationBackgrounds.Any(p => p.StaffID == _staffID && p.IsHighestQualification == true)) == true)
                        {
                            //get the records with the highest qualifiaction and change them
                            foreach (StaffEducationBackground updateQualification in db.StaffEducationBackgrounds.Where(p => p.StaffID == _staffID))
                            {
                                updateQualification.IsHighestQualification = false;
                            }
                        }
                        //save the new education background record
                        StaffEducationBackground newEducationBackground = new StaffEducationBackground();
                        newEducationBackground.StaffID = _staffID;
                        newEducationBackground.EducationQualificationType = _qualificationType;
                        newEducationBackground.InstitutionName = _institutionName;
                        newEducationBackground.EducationalLevelID = _attainedLevelID;
                        newEducationBackground.Award = _award;
                        newEducationBackground.QualificationStatus = _status;
                        //newEducationBackground.CompletionMonth = _completionMonth;
                        newEducationBackground.PeriodFrom = _PeriodFrom;
                        newEducationBackground.PeriodTo = _PeriodTo;
                        newEducationBackground.ProfessionalMembership = _ProfessionalMembership;
                        newEducationBackground.GradeAttained = _gradeAttained;
                        newEducationBackground.ProfessionalMembership = txtProfessionalMembership.Text.Trim();
                        newEducationBackground.IsCertificateSubmitted = cbCerificateSubmitted.Checked;
                        newEducationBackground.IsTranscriptSubmitted = cbTranscriptSubmitted.Checked;
                        newEducationBackground.IsVerificationDone = cbVerificationDone.Checked;
                        newEducationBackground.IsAJobRequirement = cbIsItAJobRequirement.Checked;
                        newEducationBackground.IsHighestQualification = _isHighestQualification;
                        db.StaffEducationBackgrounds.InsertOnSubmit(newEducationBackground);
                        db.SubmitChanges();
                        DisplayEducationBackground();//display education list
                        lnkBtnUpdateEducationBackground.Enabled = true;
                        lbEducationBackgroundError.Text = "";
                        HiddenFieldEducationBackgroundID.Value = newEducationBackground.StaffEducationBackgroundID.ToString();
                        _hrClass.LoadHRManagerMessageBox(2, "Qualification record has been saved successfully.", this, PanelQualificationDetails);
                        return;
                    }
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Please try again!", this, PanelQualificationDetails);
                return;
            }
        }
        //method for updating an education background record
        private void UpdateEducationBackground()
        {
            ModalPopupExtenderAddEducationBackground.Show();
            try
            {
                lbEducationBackgroundInfo.Text = "";
                string _error = ValidateAddEducationBackgroundControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelQualificationDetails);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    int _educationID = Convert.ToInt32(HiddenFieldEducationBackgroundID.Value);
                    string _qualificationType = rblEducationBackgroundQualificationType.SelectedValue;
                    string _status = rblEducationBackgroundStatus.SelectedValue;
                    string _institutionName = txtEducationBackgroundInstitutionName.Text.Trim();
                    int _attainedLevelID = Convert.ToInt32(ddlEducationBackgroundLevelAttained.SelectedValue);
                    string _award = txtAward.Text.Trim();
                    //string _completionMonth = ddlEducationBackgroundCompletionDateMonth.SelectedValue;
                    string _PeriodFrom = txtPeriodFrom.Text.Trim();
                    string _PeriodTo = txtPeriodTo.Text.Trim();
                    string _ProfessionalMembership = txtProfessionalMembership.Text.Trim();
                    string _gradeAttained = txtEducationBackgroundGrade.Text.Trim();
                    Boolean _isHighestQualification = cbIsItHighestQualification.Checked;
                    //check if the record being updated with already exists
                    //if (db.StaffEducationBackgrounds.Any(p => p.StaffEducationBackgroundID != _educationID && p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.ProgramCourse == _programmeCourse && p.CompletionMonth == _completionMonth && p.CompletionYear == _completionYear))
                    if (db.StaffEducationBackgrounds.Any(p => p.StaffEducationBackgroundID != _educationID && p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.Award == _award ))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "The qualification record that you are trying to update is already saved.", this, PanelQualificationDetails);
                        return;
                    }
                    else
                    {
                        //check if there is another highest qualification saved against the staff
                        if (_isHighestQualification == true && (db.StaffEducationBackgrounds.Any(p => p.StaffID == _staffID && p.IsHighestQualification == true && p.StaffEducationBackgroundID != _educationID)) == true)
                        {
                            //get the records with the highest qualification and change them
                            foreach (StaffEducationBackground updateQualification in db.StaffEducationBackgrounds.Where(p => p.StaffID == _staffID && p.StaffEducationBackgroundID != _educationID))
                            {
                                updateQualification.IsHighestQualification = false;
                            }
                        }
                        //update the education background record
                        StaffEducationBackground updateEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _educationID);
                        updateEducationBackground.EducationQualificationType = _qualificationType;
                        updateEducationBackground.QualificationStatus = _status;
                      updateEducationBackground.InstitutionName = _institutionName;
                      updateEducationBackground.EducationalLevelID = _attainedLevelID;
                      //updateEducationBackground.CompletionMonth = _completionMonth;
                       updateEducationBackground.PeriodFrom = _PeriodFrom;
                        updateEducationBackground.PeriodTo = _PeriodTo;
                        updateEducationBackground.ProfessionalMembership = _ProfessionalMembership;
                        updateEducationBackground.GradeAttained = _gradeAttained;
                        updateEducationBackground.ProfessionalMembership = txtProfessionalMembership.Text.Trim();
                        updateEducationBackground.IsCertificateSubmitted = cbCerificateSubmitted.Checked;
                        updateEducationBackground.IsTranscriptSubmitted = cbTranscriptSubmitted.Checked;
                        updateEducationBackground.IsVerificationDone = cbVerificationDone.Checked;
                        updateEducationBackground.IsAJobRequirement = cbIsItAJobRequirement.Checked;
                        updateEducationBackground.IsHighestQualification = cbIsItHighestQualification.Checked;
                        db.SubmitChanges();
                        DisplayEducationBackground();//display education list
                        lnkBtnUpdateEducationBackground.Enabled = true;
                        lbEducationBackgroundError.Text = "";
                        _hrClass.LoadHRManagerMessageBox(2, "Qualification record has been updated successfully.", this, PanelQualificationDetails);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed." + ex.Message.ToString() + " Please try again.", this, PanelQualificationDetails);
                return;
            }
        }
        //save the education background details
        protected void lnkBtnSaveEducationBackground_Click(object sender, EventArgs e)
        {
            SaveEducationBackground(); return;
        }
        //update the education background details
        protected void lnkBtnUpdateEducationBackground_Click(object sender, EventArgs e)
        {
            UpdateEducationBackground(); return;
        }
        protected void lnkBtnClearEducationBackground_Click(object sender, EventArgs e)
        {
            ClearAddEducationBackgroundControls();
            ModalPopupExtenderAddEducationBackground.Show();
            return;
        }
        //check if a staff is selected before edit details controls are loaded
        protected void LinkButtonEditEducationBackground_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvEducationBackground))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelQualificationDetails);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvEducationBackground.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelQualificationDetails);
                        return;
                    }
                    foreach (GridViewRow _grv in gvEducationBackground.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFEducationBackgroundID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit education background details
                            LoadEditStaffEducationBackgroundControls(Convert.ToInt32(_HFEducationBackgroundID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading staff qualification details for edit! " + ex.Message.ToString(), this, PanelQualificationDetails);
            }
        }
        //load education background record for edit
        private void LoadEditStaffEducationBackgroundControls(int _staffEducationBackgroundID)
        {
            StaffEducationBackground getEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _staffEducationBackgroundID);
            //_hrClass.GenerateMonths(ddlEducationBackgroundCompletionDateMonth);//start date months
                    HiddenFieldEducationBackgroundID.Value = _staffEducationBackgroundID.ToString();
            rblEducationBackgroundQualificationType.SelectedValue = getEducationBackground.EducationQualificationType;
            txtEducationBackgroundInstitutionName.Text = getEducationBackground.InstitutionName;
            ddlEducationBackgroundLevelAttained.SelectedValue = getEducationBackground.EducationalLevelID.ToString();
            txtAward.Text = getEducationBackground.Award;
            rblEducationBackgroundStatus.SelectedValue = getEducationBackground.QualificationStatus;
            //ddlEducationBackgroundCompletionDateMonth.SelectedValue = getEducationBackground.CompletionMonth;
            txtProfessionalMembership.Text = getEducationBackground.ProfessionalMembership;
            txtPeriodFrom.Text = getEducationBackground.PeriodFrom;
            txtPeriodTo.Text = getEducationBackground.PeriodTo;
            txtEducationBackgroundGrade.Text = getEducationBackground.GradeAttained;
            txtProfessionalMembership.Text = getEducationBackground.ProfessionalMembership;
            cbCerificateSubmitted.Checked = (bool)getEducationBackground.IsCertificateSubmitted;
            cbTranscriptSubmitted.Checked = (bool)getEducationBackground.IsTranscriptSubmitted;
            cbVerificationDone.Checked = (bool)getEducationBackground.IsVerificationDone;
            cbIsItAJobRequirement.Checked = (bool)getEducationBackground.IsAJobRequirement;
            cbIsItHighestQualification.Checked = (bool)getEducationBackground.IsHighestQualification;
            lnkBtnSaveEducationBackground.Visible = false;
            lnkBtnClearEducationBackground.Visible = true;
            lnkBtnUpdateEducationBackground.Enabled = true;
            lbAddEducationBackgroundHeader.Text = "Edit Qualification";
            ImageAddEducationBackgroundHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbEducationBackgroundError.Text = "";
            lbEducationBackgroundInfo.Text = "";
            ModalPopupExtenderAddEducationBackground.Show();
            return;
        }
        //display staff education background details
        private void DisplayEducationBackground()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                _display = db.PROC_StaffEducationBackground(_staffID);
                gvEducationBackground.DataSourceID = null;
                gvEducationBackground.DataSource = _display;
                Session["gvEducationBackgroundData"] = _display;
                gvEducationBackground.DataBind();
                return;
            }
            catch { }
        }
        protected void gvEducationBackground_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvEducationBackground.PageIndex = e.NewPageIndex;
            Session["gvEducationBackgroundPageIndex"] = e.NewPageIndex;//get page index
            gvEducationBackground.DataSource = (object)Session["gvEducationBackgroundData"];
            gvEducationBackground.DataBind();
            return;
        }
        protected void gvEducationBackground_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvEducationBackgroundPageIndex"] == null)//check if page index is empty
                {
                    gvEducationBackground.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvEducationBackgroundPageIndex"]);
                    gvEducationBackground.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //load delete education background details
        protected void LinkButtonDeleteEducationBackground_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvEducationBackground))//check if there is any education background record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select record(s) to delete!", this, PanelQualificationDetails);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvEducationBackground.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected qualification record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected qualification records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelQualificationDetails);
                return;
            }
        }
        //delete education background record
        private void DeleteEducationBackground()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvEducationBackground.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFEducationBackgroundID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _educationBackgroundID = Convert.ToInt32(_HFEducationBackgroundID.Value);
                        StaffEducationBackground deleteEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _educationBackgroundID);
                        db.StaffEducationBackgrounds.DeleteOnSubmit(deleteEducationBackground);
                        db.SubmitChanges();
                    }
                }
                //refresh education background records after the delete is done
                DisplayEducationBackground();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected qualification record has been deleted.", this, PanelQualificationDetails);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected qualification records have been deleted.", this, PanelQualificationDetails);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelQualificationDetails);
                return;
            }
        }
        //delete staff qualification
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteEducationBackground();
            return;
        }

        #region "COMPUTER PROFICIENCY"
        private void DisplayComputerProficiency(Guid _staffID)//Display Staff computer proficiency
        {
            try
            {
                 object _display;
                _display = db.PROC_StaffComputerProficiency(_staffID);
                gvComputerProficiency.DataSourceID = null;
                gvComputerProficiency.DataSource = _display;
                gvComputerProficiency.DataBind();
                return;
            }
            catch { }
        }

        //validate add computer proficiency details
        private string ValidateComputerProficiency()
        {
            if (HiddenFieldStaffID.Value == "")
                return "computer proficiency save failed. Enter staff's personal information or search for the staff before adding his/her computer proficiency details!";
            else if (txtComputerPackage.Text.Trim() == "")
                return "Enter the computer package!";
            else if (txtProficiencyExtent.Text.Trim() == "")
                return "Enter the proficiency extent!";
            else return "";
        }
        protected void btnAddComputerProficiency_Click(object sender, EventArgs e)
        {
            SaveComputerProficiency(); return;//
        }

        //method for saving allergies
        private void SaveComputerProficiency()
        {
            try
            {
                string _error = ValidateComputerProficiency();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelQualificationDetails);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _ComputerProficiency = txtComputerPackage.Text.Trim();
                    if (btnAddComputerProficiency.Text.ToLower() == "save computer proficiency details")
                    {
                        //check if the selected  computer proficiency detail is selected against the staff
                        if (db.StaffComputerProficiencies.Any(p => p.ComputerPackage == _ComputerProficiency))
                        {
                            _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected computer package has already been saved!", this, PanelQualificationDetails);
                            return;
                        }
                        else
                        {
                            //save computer proficiency
                            AddComputerProficiency(_staffID);
                            _hrClass.LoadHRManagerMessageBox(2, "Computer proficiency details have been successfully saved!", this, PanelQualificationDetails);
                            return;
                        }
                    }
                    else//update computer proficiency
                    {
                        int _ComputerProficiencyID = Convert.ToInt32(HiddenFieldComputerProficiencyID.Value);

                        //update the computer proficiency details
                        UpdateStaffComputerProficiency(_ComputerProficiencyID);//
                        _hrClass.LoadHRManagerMessageBox(2, "Computer proficiency details have been successfully updated!", this, PanelQualificationDetails);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelQualificationDetails);
                return;
            }
        }
        //method for adding ComputerProficiency
        private void AddComputerProficiency(Guid _staffID)
        {
            //add ComputerProficiency
           
            StaffComputerProficiency newStaffComputerProficiency = new StaffComputerProficiency();
            newStaffComputerProficiency.StaffID = _staffID;
            newStaffComputerProficiency.ComputerPackage = txtComputerPackage.Text.Trim();
            newStaffComputerProficiency.ProficiencyExtent = txtProficiencyExtent.Text.Trim();
            db.StaffComputerProficiencies.InsertOnSubmit(newStaffComputerProficiency);
           
            db.SubmitChanges();
            HiddenFieldComputerProficiencyID.Value = newStaffComputerProficiency.StaffComputerProficiencyID.ToString();
            //display computer proficiency details associated with the staff
            DisplayComputerProficiency(_staffID);
            //clear the entry controls
            ClearStaffComputerProficiencyControls();
        }
        //method for updating computer proficiency details
        private void UpdateStaffComputerProficiency(int _ComputerProficiencyID)
        {
            //alter computer proficiency details
            StaffComputerProficiency updateStaffComputerProficiency = db.StaffComputerProficiencies.Single(p => p.StaffComputerProficiencyID == _ComputerProficiencyID);
            updateStaffComputerProficiency.ComputerPackage = txtComputerPackage.Text.Trim();
            updateStaffComputerProficiency.ProficiencyExtent = txtProficiencyExtent.Text.Trim();
            db.SubmitChanges();
            //refresh computer proficiency details
            DisplayComputerProficiency((Guid)updateStaffComputerProficiency.StaffID);

            //clear the entry controls
            ClearStaffComputerProficiencyControls();
            return;
        }
        //save computer proficiency details

        private void ClearStaffComputerProficiencyControls()
        {
            txtComputerPackage.Text = "";
            txtProficiencyExtent.Text = "";
            btnAddComputerProficiency.Text = "Save Computer Proficiency Details";
            HiddenFieldComputerProficiencyID.Value = "";
            return;
        }

        //load allergy for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvComputerProficiency_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditComputerProficiency") == 0 || e.CommandName.CompareTo("DeleteComputerProficiency") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFComputerProficiencyID = (HiddenField)gvComputerProficiency.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditComputerProficiency") == 0)//check if we are editing
                    {
                        LoadComputerProficiencyForEdit(Convert.ToInt32(_HFComputerProficiencyID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteComputerProficiency") == 0)//check if we are deleting
                    {
                        HiddenFieldComputerProficiencyID.Value = _HFComputerProficiencyID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmComputer, "Are you sure you what to delete the selected proficiency details?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelQualificationDetails);
                return;
            }
        }

        //load computer proficiency for edit
        private void LoadComputerProficiencyForEdit(int _ComputerProficiencyID)
        {
            HiddenFieldComputerProficiencyID.Value = _ComputerProficiencyID.ToString();
            StaffComputerProficiency getStaffComputerProficiency = db.StaffComputerProficiencies.Single(p => p.StaffComputerProficiencyID == _ComputerProficiencyID);
            txtComputerPackage.Text = getStaffComputerProficiency.ComputerPackage.ToString();
            txtProficiencyExtent.Text = getStaffComputerProficiency.ProficiencyExtent.ToString();

            btnAddComputerProficiency.Text = "Update Computer Proficiency Details";
            return;
        }
        //method for deleting a Computer Proficiency detail
        private void DeleteComputerProficiency()
        {
            try
            {
                int _ComputerProficiencyID = Convert.ToInt32(HiddenFieldComputerProficiencyID.Value);
                StaffComputerProficiency deleteStaffComputerProficiency = db.StaffComputerProficiencies.Single(p => p.StaffComputerProficiencyID == _ComputerProficiencyID);
                db.StaffComputerProficiencies.DeleteOnSubmit(deleteStaffComputerProficiency);
                db.SubmitChanges();
                //refresh Allergy after delete is done
                DisplayComputerProficiency((Guid)deleteStaffComputerProficiency.StaffID);

                _hrClass.LoadHRManagerMessageBox(2, "Computer proficiency details have been deleted!", this, PanelQualificationDetails);
                ClearStaffComputerProficiencyControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelQualificationDetails);
                return;
            }
        }
        //delete the ComputerProficiency details
        protected void ucConfirmComputer_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
            DeleteComputerProficiency();
            return;
        }
        #endregion



    }
}