﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;


namespace AdeptHRManager.HR_Module
{
    public partial class Job_Details : System.Web.UI.Page
    {

        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlPostedAt, 2000, "Duty Station /Office");//display branches(2000) available
                _hrClass.GetListingItems(ddlDepartment, 3000, "Department");//display departments(3000) available
                _hrClass.GetListingItems(ddlJobTitle, 7000, "Job Title");//display job titles(7000) available
                _hrClass.GetListingItemsWithTableOrder(ddlJobGrade, 20000, "Job Grade");//display job grades(20000) available
                _hrClass.GetListingItems(ddlLocationPosted, 12000, "Location");//display loaction posted(12000) available
                _hrClass.GetListingItems(ddlContractType, 21000, "Contract Type");//display contract type(21000) available
                _hrClass.GetListingItems(ddlTypeOfWork, 22000, "Type of Work");//display (22000) available
                _hrClass.GetStaffNames(ddlReportsTo, "Reports To");//populate reports to dropdown
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff              
            }
            ucConfirmContractDate.lnkBtnYesConfirmationClick += new EventHandler(ucConfirmContractDate_lnkBtnYesConfirmationClick);
            ucConfirmWorkPermit.lnkBtnYesConfirmationClick += new EventHandler(ucConfirmWorkPermit_lnkBtnYesConfirmationClick);
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffJobDetails(_staffID);//load job details
                    return;
                }
            }
            catch { }
        }
        //load staff job details
        private void DisplayStaffJobDetails(Guid _staffID)
        {
            //load staff job details here
            //get job details associated with the staff 
            if (_staffID != Guid.Empty)
            {
                if (db.StaffJobDetails.Any(p => p.StaffID == _staffID))
                {
                    //load job details assoicated with staff
                    LoadJobDetailsForEdit(_staffID);
                }
                else
                {
                    //clear entry contols
                    ClearAddJobDetailsControls();
                }
            }
            else
            {
                //clear entry controls
                ClearAddJobDetailsControls();
            }
        }
        //search for job details by staff reference number
        private void SearchForJobDetailsByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffJobDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffJobDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelJobDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelJobDetails);
                return;
            }
        }
        //search job details by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForJobDetailsByStaffReference();
            return;
        }
        //search for job details by staff namae
        private void SearchForJobDetailsByStaffName()
        {
            try
            {
            string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
            string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
            if (_staffID != "")
            {
                Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                DisplayStaffJobDetails(_StaffID);
                return;
            }
            else
            {
                DisplayStaffJobDetails(Guid.Empty);
                _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelJobDetails);
                return;
            }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelJobDetails);
                return;
            }
        }
        //search job details by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForJobDetailsByStaffName();
            return;
        }
        //search for job details by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffJobDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffJobDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelJobDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelJobDetails);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffJobDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffJobDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelJobDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelJobDetails);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //validate add job details controls
        private string ValidateAddJobDetailsControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Search for the employee that you want to record the job details against!";
            else if (txtJoinDate.Text.Trim() == "")
            {
                txtJoinDate.Focus();
                return "Enter join date!";
            }
            else if (txtJobRole.Text.Trim() == "")
            {
                txtJobRole.Focus();
                return "Enter job details!";
            }
            else if (_hrClass.isDateValid(txtJoinDate.Text.Trim()) == false)
            {
                txtJoinDate.Focus();
                return "Invalid join date entered!";
            }
            else if (ddlJobTitle.SelectedIndex == 0)
            {
                ddlJobTitle.Focus();
                return "Select job title !";
            }
            else if (ddlJobGrade.SelectedIndex == 0)
            {
                ddlJobGrade.Focus();
                return "Select job grade !";
            }
            else if (ddlDepartment.SelectedIndex == 0)
            {
                ddlDepartment.Focus();
                return "Select department !";
            }
            else if (ddlLocationPosted.SelectedIndex == 0)
            {
                ddlLocationPosted.Focus();
                return "Select location posted !";
            }
            else if (ddlPostedAt.SelectedIndex == 0)
            {
                ddlPostedAt.Focus();
                return "Select duty station/ office for the staff !";
            }
            else if (ddlContractType.SelectedIndex == 0)
            {
                ddlContractType.Focus();
                return "Select contract type!";
            }
            else if (rblProfessional.SelectedIndex == -1)
                return "Select the employee category i.e. general support or proffesional";
            /* else if (txtContractStartDate.Text.Trim() == "__/___/____")
             {
                 txtContractStartDate.Focus();
                 return "Enter contract start date!";
             }
             else if (_hrClass.isDateValid(txtContractStartDate.Text.Trim()) == false)
             {
                 txtContractStartDate.Focus();
                 return "Invalid contract start date entered!";
             }*/
            else if (ddlJobTitle.SelectedIndex == 0)
            {
                ddlJobTitle.Focus();
                return "Select job title !";
            }
            else if (ddlReportsTo.SelectedIndex == 0)
            {
                return "Select the supervisor!";
            }
            else if (rblStaffUnderProbation.SelectedIndex == -1)
            {
                return "Select whether the employee is under probation or not!";
            }
            else if (rblInternationalOrNational.SelectedIndex == -1)
            {
                return "Select whether the employee is a national or international!";
            }
            else return "";
        }

        //save job details
        private void SaveStaffJobDetails()
        {
            try
            {
                string _error = ValidateAddJobDetailsControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelJobDetails);
                    return;
                }
                else
                {
                    //save the details
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    //check if the staff has got any job detailsthat exist
                    if (db.StaffJobDetails.Any(p => p.StaffID == _staffID))
                    {
                        //update the job details
                        UpdateJobDetail(_staffID);
                    }
                    else
                    {
                        //save the new job details
                        SaveNewJobDetail(_staffID);
                    }
                    _hrClass.LoadHRManagerMessageBox(2, "Job details have been successfully saved!", this, PanelJobDetails);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString(), this, PanelJobDetails);
                return;
            }
        }
        //save staff job details
        protected void lnkBtnSaveJobDetails_Click(object sender, EventArgs e)
        {
            SaveStaffJobDetails();
            return;
        }
        //method for saving a new job detail record
        private void SaveNewJobDetail(Guid _staffID)
        {
            StaffJobDetail newJobDetail = new StaffJobDetail();
            newJobDetail.StaffID = _staffID;
            newJobDetail.JoinDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
            newJobDetail.JobTitleID = Convert.ToInt32(ddlJobTitle.SelectedValue);
            newJobDetail.JobGradeID = Convert.ToInt32(ddlJobGrade.SelectedValue);
            newJobDetail.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
            newJobDetail.JobLocationID = Convert.ToInt32(ddlLocationPosted.SelectedValue);
            newJobDetail.PostedBranchID = Convert.ToInt32(ddlPostedAt.SelectedValue);
            newJobDetail.JobRole = txtJobRole.Text.Trim();

            newJobDetail.ContractTypeID = Convert.ToInt32(ddlContractType.SelectedValue);

            /*newJobDetail.ContractStartDate = Convert.ToDateTime(txtContractStartDate.Text.Trim()).Date;
            if (txtContractEndDate.Text.Trim() != "__/___/____")
                newJobDetail.ContractEndDate = Convert.ToDateTime(txtContractEndDate.Text.Trim()).Date;
            else
                newJobDetail.ContractEndDate = null;*/
            if (txtVisaExpiryDate.Text.Trim() != "__/___/____")
                newJobDetail.VisaExpiryDate = Convert.ToDateTime(txtVisaExpiryDate.Text.Trim()).Date;
            else
                newJobDetail.VisaExpiryDate = null;
            if (ddlTypeOfWork.SelectedIndex != 0)
                newJobDetail.TypeOfWorkID = Convert.ToInt32(ddlTypeOfWork.SelectedValue);
            else newJobDetail.TypeOfWorkID = 0;
            newJobDetail.IsProfessional = Convert.ToBoolean(rblProfessional.SelectedValue);
            newJobDetail.ReportToStaffID = _hrClass.ReturnGuid(ddlReportsTo.SelectedValue);
            newJobDetail.IsUnderProbation = Convert.ToBoolean(rblStaffUnderProbation.SelectedValue);
            if (txtProbationEndDate.Text.Trim() != "__/___/____")
                newJobDetail.ProbationEndDate = Convert.ToDateTime(txtProbationEndDate.Text.Trim()).Date;
            else
                newJobDetail.ProbationEndDate = null;
            newJobDetail.BadgeNumber = txtBadgeNumber.Text.Trim();
            newJobDetail.IsNational = Convert.ToBoolean(rblInternationalOrNational.SelectedValue);
            db.StaffJobDetails.InsertOnSubmit(newJobDetail);
            db.SubmitChanges();
        }
        //method for uodating an existing job detail
        private void UpdateJobDetail(Guid _staffID)
        {
            int _jobDetailID = Convert.ToInt32(db.StaffJobDetails.Single(p => p.StaffID == _staffID).JobDetailID);
            StaffJobDetail updateJobDetail = db.StaffJobDetails.Single(p => p.JobDetailID == _jobDetailID);
            updateJobDetail.JoinDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
            updateJobDetail.JobTitleID = Convert.ToInt32(ddlJobTitle.SelectedValue);
            updateJobDetail.JobGradeID = Convert.ToInt32(ddlJobGrade.SelectedValue);
            updateJobDetail.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
            updateJobDetail.JobLocationID = Convert.ToInt32(ddlLocationPosted.SelectedValue);
            updateJobDetail.PostedBranchID = Convert.ToInt32(ddlPostedAt.SelectedValue);
            updateJobDetail.ContractTypeID = Convert.ToInt32(ddlContractType.SelectedValue);
            updateJobDetail.JobRole = txtJobRole.Text.Trim();
           /* updateJobDetail.ContractStartDate = Convert.ToDateTime(txtContractStartDate.Text.Trim()).Date;
            if (txtContractEndDate.Text.Trim() != "__/___/____")
                updateJobDetail.ContractEndDate = Convert.ToDateTime(txtContractEndDate.Text.Trim()).Date;
            else
                updateJobDetail.ContractEndDate = null;*/
            if (txtVisaExpiryDate.Text.Trim() != "__/___/____")
                updateJobDetail.VisaExpiryDate = Convert.ToDateTime(txtVisaExpiryDate.Text.Trim()).Date;
            else
                updateJobDetail.VisaExpiryDate = null;
            if (ddlTypeOfWork.SelectedIndex != 0)
                updateJobDetail.TypeOfWorkID = Convert.ToInt32(ddlTypeOfWork.SelectedValue);
            else updateJobDetail.TypeOfWorkID = 0;
            updateJobDetail.IsProfessional = Convert.ToBoolean(rblProfessional.SelectedValue);
            updateJobDetail.ReportToStaffID = _hrClass.ReturnGuid(ddlReportsTo.SelectedValue);
            updateJobDetail.IsUnderProbation = Convert.ToBoolean(rblStaffUnderProbation.SelectedValue);
            if (txtProbationEndDate.Text.Trim() != "__/___/____")
                updateJobDetail.ProbationEndDate = Convert.ToDateTime(txtProbationEndDate.Text.Trim()).Date;
            else
                updateJobDetail.ProbationEndDate = null;
            updateJobDetail.BadgeNumber = txtBadgeNumber.Text.Trim();
            updateJobDetail.IsNational = Convert.ToBoolean(rblInternationalOrNational.SelectedValue);
            db.SubmitChanges();
        }
        //clear add hob details controls
        private void ClearAddJobDetailsControls()
        {
            _hrClass.ClearEntryControls(PanelAddJobDetails);
            return;
        }
        //load staff job details for edit
        private void LoadJobDetailsForEdit(Guid _staffID)
        {
            try
            {
            StaffJobDetail getJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);
            int _JobDetailID = Convert.ToInt32(getJobDetail.JobDetailID.ToString());
            HttpContext.Current.Session["JobDetailID"] = _JobDetailID;
            DisplayContractDates(_JobDetailID);
            DisplayWorkPermitNumber(_JobDetailID);
            if (getJobDetail.JoinDate != null)
                txtJoinDate.Text = _hrClass.ShortDateDayStart(getJobDetail.JoinDate.ToString());
            else txtJoinDate.Text = "";
            if (getJobDetail.VisaExpiryDate != null)
                txtVisaExpiryDate.Text = _hrClass.ShortDateDayStart(getJobDetail.VisaExpiryDate.ToString());
            else txtVisaExpiryDate.Text = "";
            if (getJobDetail.JobTitleID != null)
                ddlJobTitle.SelectedValue = getJobDetail.JobTitleID.ToString();
            else ddlJobTitle.SelectedIndex = 0;
            if (getJobDetail.JobGradeID != null)
                ddlJobGrade.SelectedValue = getJobDetail.JobGradeID.ToString();
            else ddlJobGrade.SelectedIndex = 0;
            ddlDepartment.SelectedValue = getJobDetail.DepartmentID.ToString();
            if (getJobDetail.JobLocationID != null)
                ddlLocationPosted.SelectedValue = getJobDetail.JobLocationID.ToString();
            else ddlLocationPosted.SelectedIndex = 0;

            if (getJobDetail.PostedBranchID != null)
                ddlPostedAt.SelectedValue = getJobDetail.PostedBranchID.ToString();
            else
                ddlPostedAt.SelectedIndex = 0;
            if (getJobDetail.ContractTypeID != null)
                ddlContractType.SelectedValue = getJobDetail.ContractTypeID.ToString();
            else ddlContractType.SelectedIndex = 0;
            /*if (getJobDetail.ContractStartDate != null)
                txtContractStartDate.Text = _hrClass.ShortDateDayStart(getJobDetail.ContractStartDate.ToString());
            else txtContractStartDate.Text = "";
            if (getJobDetail.ContractEndDate != null)
                txtContractEndDate.Text = _hrClass.ShortDateDayStart(getJobDetail.ContractEndDate.ToString());
            else
                txtContractEndDate.Text = "";*/

            if (getJobDetail.TypeOfWorkID != null)
                ddlTypeOfWork.SelectedValue = getJobDetail.TypeOfWorkID.ToString();
            else ddlTypeOfWork.SelectedIndex = 0;

            rblProfessional.SelectedValue = getJobDetail.IsProfessional.ToString();
            if (getJobDetail.ReportToStaffID != null)
                ddlReportsTo.SelectedValue = getJobDetail.ReportToStaffID.ToString();
            else ddlReportsTo.SelectedIndex = 0;
            if (getJobDetail.IsUnderProbation != null)
                rblStaffUnderProbation.SelectedValue = getJobDetail.IsUnderProbation.ToString();
            else rblStaffUnderProbation.SelectedIndex = 0;
            if (getJobDetail.ProbationEndDate != null)
                txtProbationEndDate.Text = _hrClass.ShortDateDayStart(getJobDetail.ProbationEndDate.ToString());
            else
                txtProbationEndDate.Text = "";
            txtBadgeNumber.Text = Convert.ToString(getJobDetail.BadgeNumber);
            if (getJobDetail.IsNational != null)
                rblInternationalOrNational.SelectedValue = getJobDetail.IsNational.ToString();
            else
                rblInternationalOrNational.SelectedIndex = -1;
            txtJobRole.Text = getJobDetail.JobRole.ToString();
            }
                
            catch { }
        }
        #region "CONTRACT DATES"
        private void DisplayContractDates(int _JobDetailID)//Display Job contract date
        {
            try
            {
                object _display;
                _display = db.PROC_JobContractDates(_JobDetailID);               
                gvContractDates.DataSourceID = null;
                gvContractDates.DataSource = _display;
                gvContractDates.DataBind();
                return;
            }
            catch { }
        }

        //validate add contract dates
        private string ValidateJobContractDates()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Job details save failed. Enter staff's personal information or search for the staff before adding his/her contract dates!";
            else if (txtStartDate.Text.Trim() == "")
                return "Enter the start date!";
            else if (txtEndDate.Text.Trim() == "")
                return "Enter the end date!";
            else if (_hrClass.isDateValid(txtStartDate.Text.Trim()) == false)
            {     
                return "Invalid start date entered!";
            }
            else if (_hrClass.isDateValid(txtEndDate.Text.Trim()) == false)
            {
                return "Invalid end date entered!";
            }
            else return "";
        }
        protected void btnAddContractDates_Click(object sender, EventArgs e)
        {
            SaveJobContractDate(); return;//
        }

        //method for saving job contract dates
        private void SaveJobContractDate()
        {
            try
            {
                string _error = ValidateJobContractDates();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelJobDetails);
                    return;
                }
                else
                {
                  
                    if (btnAddContractDates.Text.ToLower() == "save contract dates")
                    {
                        
                            //save contract dates
                            AddContractDates();
                            _hrClass.LoadHRManagerMessageBox(2, "contract dates have been successfully saved!", this, PanelJobDetails);
                            return;
                      
                    }
                    else//update contract dates
                    {
                        int JobContractDateID = Convert.ToInt32(HiddenFieldContractDatesID.Value);

                        //update the contract dates
                        UpdateJobContractDate(JobContractDateID);//
                        _hrClass.LoadHRManagerMessageBox(2, "Contract dates have been successfully updated!", this, PanelJobDetails);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelJobDetails);
                return;
            }
        }

        //method for adding Job contract dates
        private void AddContractDates()
        {
            JobContractDate newJobContractDate = new JobContractDate();  
            //newJobContractDate.JobDetailsID=job
            int _JobDetailID=Convert.ToInt32(HttpContext.Current.Session["JobDetailID"]) ;
            newJobContractDate.JobDetailsID = _JobDetailID;
            if (txtStartDate.Text.Trim() != "__/___/____")
                newJobContractDate.StartContractDate = Convert.ToDateTime(txtStartDate.Text.Trim()).Date;
            else
                newJobContractDate.StartContractDate = null;
            if (txtEndDate.Text.Trim() != "__/___/____")
                newJobContractDate.EndContractDate = Convert.ToDateTime(txtEndDate.Text.Trim()).Date;
            else
                newJobContractDate.StartContractDate = null;
         
            db.JobContractDates.InsertOnSubmit(newJobContractDate);

            db.SubmitChanges();
            HiddenFieldContractDatesID.Value = newJobContractDate.JobContractDateID.ToString();
            //display contract dates associated with the staff
            DisplayContractDates(_JobDetailID);
       
            //clear the entry controls
            ClearJobContractDateControls();
        }
        //method for updating contract dates
        private void UpdateJobContractDate(int JobContractDateID)
        {
            //alter contract dates
            JobContractDate updateJobContractDate = db.JobContractDates.Single(p => p.JobContractDateID == JobContractDateID);
            if (txtStartDate.Text.Trim() != "__/___/____")
                updateJobContractDate.StartContractDate = Convert.ToDateTime(txtStartDate.Text.Trim()).Date;
            else
                updateJobContractDate.StartContractDate = null;
            if (txtEndDate.Text.Trim() != "__/___/____")
                updateJobContractDate.EndContractDate = Convert.ToDateTime(txtEndDate.Text.Trim()).Date;
            else
                updateJobContractDate.EndContractDate = null;

            updateJobContractDate.StartContractDate = Convert.ToDateTime(txtStartDate.Text.Trim()).Date;
            db.SubmitChanges();
            //refresh contract dates
            
            DisplayContractDates((int)updateJobContractDate.JobDetailsID);

            //clear the entry controls
            ClearJobContractDateControls();
            return;
        }
        //save contract dates

        private void ClearJobContractDateControls()
        {
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            btnAddContractDates.Text = "Save contract dates";
            HiddenFieldContractDatesID.Value = "";
            return;
        }

        //load allergy for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvContractDates_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditContractDates") == 0 || e.CommandName.CompareTo("DeleteContractDates") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFContractDatesID = (HiddenField)gvContractDates.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditContractDates") == 0)//check if we are editing
                    {
                        LoadContractDatesForEdit(Convert.ToInt32(_HFContractDatesID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteContractDates") == 0)//check if we are deleting
                    {
                        HiddenFieldContractDatesID.Value = _HFContractDatesID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmContractDate, "Are you sure you what to delete the selected contract dates?");
                        return;
                    }
                }
           }
             catch (Exception ex)
             {
                 _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelJobDetails);
                 return;
             }
        }

        //load contract date details for edit
        private void LoadContractDatesForEdit(int JobContractDateID)
        {
            HiddenFieldContractDatesID.Value = JobContractDateID.ToString();
            JobContractDate getJobContractDate = db.JobContractDates.Single(p => p.JobContractDateID == JobContractDateID);
            if (getJobContractDate.StartContractDate != null)
            {
                txtStartDate.Text = _hrClass.ShortDateDayStart(getJobContractDate.StartContractDate.ToString());

            }
            if (getJobContractDate.EndContractDate != null)
            {
                txtEndDate.Text = _hrClass.ShortDateDayStart(getJobContractDate.EndContractDate.ToString());
            }
            
            btnAddContractDates.Text = "Update Contract Dates";
            return;
        }
        //method for deleting a contract date
        private void DeleteContractDates()
        {
            try
            {
                int JobContractDateID = Convert.ToInt32(HiddenFieldContractDatesID.Value);
                JobContractDate deleteJobContractDate = db.JobContractDates.Single(p => p.JobContractDateID == JobContractDateID);
                db.JobContractDates.DeleteOnSubmit(deleteJobContractDate);
                db.SubmitChanges();
                //refresh contract date after delete is done
                DisplayContractDates((int)deleteJobContractDate.JobDetailsID);
        
                _hrClass.LoadHRManagerMessageBox(2, "contract dates have been deleted!", this, PanelJobDetails);
                ClearJobContractDateControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelJobDetails);
                return;

            }
        }
       // delete the ComputerProficiency details
        protected void ucConfirmContractDate_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
            DeleteContractDates();
            return;
        }
        #endregion

        #region "WORK PERMIT"
        private void DisplayWorkPermitNumber(int _JobDetailID)//Display Job contract date
        {
            try
            {
                object _display;
                _display = db.PROC_JobWorkPermit(_JobDetailID);
                gvWorkPermitNumber.DataSourceID = null;
                gvWorkPermitNumber.DataSource = _display;
                gvWorkPermitNumber.DataBind();
                return;
            }
            catch { }
        }

        //validate add contract dates
        private string ValidateWorkPermitNumber()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Job details save failed. Enter staff's personal information or search for the staff before adding his/her work permit!";
            else if (txtWorkPermitNumber.Text.Trim() == "")
                return "Enter the Work Permit Number!";
            else if (txtExpiryDate.Text.Trim() == "")
                return "Enter the Expiry Date!";

            else if (_hrClass.isDateValid(txtExpiryDate.Text.Trim()) == false)
            {
                return "Invalid Expiry Date entered!";
            }
            else return "";
        }
        protected void btnAddWorkPermitNumber_Click(object sender, EventArgs e)
        {
            SaveWorkPermit(); return;//
        }

        //method for saving job work permit
        private void SaveWorkPermit()
        {
            try
            {
                string _error = ValidateWorkPermitNumber();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelJobDetails);
                    return;
                }
                else
                {

                    if (btnAddWorkPermitNumber.Text.ToLower() == "save work permit number")
                    {

                        //save work permit
                        AddWorkPermitDetails();
                        _hrClass.LoadHRManagerMessageBox(2, "work permit details have been successfully saved!", this, PanelJobDetails);
                        return;

                    }
                    else//update work permit
                    {
                        int JobWorkPermitID = Convert.ToInt32(HiddenFieldWorkPermitNumberID.Value);

                        //update the work permit
                        UpdateWorkPermitNumber(JobWorkPermitID);//
                        _hrClass.LoadHRManagerMessageBox(2, "work permit  details have been successfully updated!", this, PanelJobDetails);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelJobDetails);
                return;
            }
        }

        //method for adding Job work permit
        private void AddWorkPermitDetails()
        {
            JobWorkPermit newJobWorkPermit = new JobWorkPermit();            
            int _JobDetailID = Convert.ToInt32(HttpContext.Current.Session["JobDetailID"]);
            newJobWorkPermit.JobDetailsID = _JobDetailID;
      
                newJobWorkPermit.JobWorkPermitNumber = txtWorkPermitNumber.Text.Trim();

                if (txtExpiryDate.Text.Trim() != "__/___/____")

                    newJobWorkPermit.JobWorkPermitExpiryDate = Convert.ToDateTime(txtExpiryDate.Text.Trim()).Date;
                else
                    newJobWorkPermit.JobWorkPermitExpiryDate = null;                

                db.JobWorkPermits.InsertOnSubmit(newJobWorkPermit);

            db.SubmitChanges();
            HiddenFieldWorkPermitNumberID.Value = newJobWorkPermit.JobWorkPermitID.ToString();
            //display work permit associated with the staff
            DisplayWorkPermitNumber(_JobDetailID);

            //clear the entry controls
            ClearJobWorkPermitControls();
        }
        //method for updating work permit
        private void UpdateWorkPermitNumber(int JobWorkPermitID)
        {
            //alter work permit
            JobWorkPermit UpdateWorkPermitNumber = db.JobWorkPermits.Single(p => p.JobWorkPermitID == JobWorkPermitID);

            UpdateWorkPermitNumber.JobWorkPermitNumber = txtWorkPermitNumber.Text.Trim();

            if (txtExpiryDate.Text.Trim() != "__/___/____")
                UpdateWorkPermitNumber.JobWorkPermitExpiryDate = Convert.ToDateTime(txtExpiryDate.Text.Trim()).Date;
            else
                UpdateWorkPermitNumber.JobWorkPermitExpiryDate = null;
            db.SubmitChanges();
            //refresh work permit
            DisplayWorkPermitNumber((int)UpdateWorkPermitNumber.JobDetailsID);

            //clear the entry controls
            ClearJobWorkPermitControls();
            return;
        }
        //save work permit

        private void ClearJobWorkPermitControls()
        {
            txtWorkPermitNumber.Text = "";
            txtExpiryDate.Text = "";
            btnAddWorkPermitNumber.Text = "Save work permit number";
            HiddenFieldWorkPermitNumberID.Value = "";
            return;
        }

        //load allergy for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvWorkPermitNumber_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditWorkPermitNumber") == 0 || e.CommandName.CompareTo("DeleteWorkPermitNumber") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFWorkPermitID = (HiddenField)gvWorkPermitNumber.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditWorkPermitNumber") == 0)//check if we are editing
                    {
                        LoadWorkPermitDetailsForEdit(Convert.ToInt32(_HFWorkPermitID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteWorkPermitNumber") == 0)//check if we are deleting
                    {
                        HiddenFieldWorkPermitNumberID.Value = _HFWorkPermitID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmWorkPermit, "Are you sure you what to delete the selected work permit?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelJobDetails);
                return;
            }
        }

        //load Allergy details for edit
        private void LoadWorkPermitDetailsForEdit(int JobWorkPermitID)
        {
            HiddenFieldWorkPermitNumberID.Value = JobWorkPermitID.ToString();
            JobWorkPermit getJobWorkPermit = db.JobWorkPermits.Single(p => p.JobWorkPermitID == JobWorkPermitID);

            txtWorkPermitNumber.Text = getJobWorkPermit.JobWorkPermitNumber.ToString();


            if (getJobWorkPermit.JobWorkPermitExpiryDate != null)
            {
                txtExpiryDate.Text = _hrClass.ShortDateDayStart(getJobWorkPermit.JobWorkPermitExpiryDate.ToString());
            }

            btnAddWorkPermitNumber.Text = "Update work permit number";
            return;
        }
        //method for deleting a contract date
        private void DeleteWorkPermitNumber()
        {
            try
            {
                int JobWorkPermitID = Convert.ToInt32(HiddenFieldWorkPermitNumberID.Value);
                JobWorkPermit deleteJobWorkPermit = db.JobWorkPermits.Single(p => p.JobWorkPermitID == JobWorkPermitID);
                db.JobWorkPermits.DeleteOnSubmit(deleteJobWorkPermit);
                db.SubmitChanges();
                //refresh contract date after delete is done
                DisplayWorkPermitNumber((int)deleteJobWorkPermit.JobDetailsID);
                _hrClass.LoadHRManagerMessageBox(2, "work permit have been deleted!", this, PanelJobDetails);
               
                ClearJobWorkPermitControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelJobDetails);
                return;

            }
        }
        // delete the ComputerProficiency details
        protected void ucConfirmWorkPermit_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
            DeleteWorkPermitNumber();
            return;
        }
        #endregion


    }
}