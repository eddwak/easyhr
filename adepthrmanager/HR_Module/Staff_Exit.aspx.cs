﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Exit : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetStaffNames(ddlInterviewBy, "Interviewed By");//populate interviewed to dropdown
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
                _hrClass.GetListingItems(ddlModeOfExit, 23000, "Mode of Exit");//display exit mode (23000) available
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    LoadStaffExitControls(_staffID);//load staff exit details
                    return;
                }
            }
            catch { }
        }
        //search for exit interview by staff reference number
        private void SearchForExitInterviewByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffExitControls(_StaffID);//load staff exit interview
                    return;
                }
                else
                {
                    LoadStaffExitControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelStaffExit);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
                return;
            }
        }
        //search exit interview by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForExitInterviewByStaffReference();
            return;
        }
        //search for exit interview by staff namae
        private void SearchForExitInterviewByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffExitControls(_StaffID);//dicpaly staff exit details
                    return;
                }
                else
                {
                    LoadStaffExitControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffExit);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
                return;
            }
        }
        //search exit interview by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForExitInterviewByStaffName();
            return;
        }
        //search for staff by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffExitControls(_StaffID);
                    return;
                }
                else
                {
                    LoadStaffExitControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffExit);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffExitControls(_StaffID);
                    return;
                }
                else
                {
                    LoadStaffExitControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffExit);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //load selected staff exit details
        private void LoadStaffExitControls(Guid _staffID)
        {

            //get etails associated with thestaff if
            if (_staffID != Guid.Empty)
            {
                if (db.StaffExits.Any(p => p.StaffID == _staffID))
                {
                    //load staff exit details assoicated with staff
                    DisplayStaffExitDetails(_staffID);
                }
                else
                {
                    //clear entry contols
                    ClearAddStaffExitControls();
                }
            }
            else
            {
                //clear entry controls
                ClearAddStaffExitControls();
            }
            //populate staff assets controls
            DisplayStaffAllocatedToStaffControls(_staffID);
            //load staff image 
            // _hrClass.LoadStaffPhoto(_staffID, ImageStaffPhoto);
            return;
        }
        //cleear add staff exit controls
        private void ClearAddStaffExitControls()
        {
            _hrClass.ClearEntryControls(PanelAddStaffExit);
            HiddenFieldStaffExitID.Value = "";
            lnkBtnSaveStaffExit.Text = "Save Staff Exit";
            //lnkBtnUploadedDocumentLink.Text = "No uploaded document";
            //lnkBtnUploadedDocumentLink.ToolTip = "No uploaded document";
            return;
        }
        //display staff exit details
        private void DisplayStaffExitDetails(Guid _staffID)
        {
            StaffExit getStaffExit = db.StaffExits.Single(p => p.StaffID == _staffID);
            HiddenFieldStaffExitID.Value = getStaffExit.StaffExitID.ToString();
            ddlModeOfExit.SelectedValue = getStaffExit.ModeOfExitID.ToString();
            txtExitDate.Text = _hrClass.ShortDateDayStart(getStaffExit.ExitDate.ToString());
            txtInternalExitReason.Text = getStaffExit.InternalExitReason.ToString();
            txtExternalExitReason.Text = getStaffExit.ExternalExitReason.ToString();
            txtBankAdvice.Text = getStaffExit.BankAdvice.ToString();
            txtExitInterviewComments.Text = getStaffExit.ExitComment;
            txtClearance.Text = getStaffExit.ExitClearance;
            if (getStaffExit.WillingnessToReturn == true)
                rblWillingnessToReturn.SelectedIndex = 0;
            else rblWillingnessToReturn.SelectedIndex = 1;
            ddlInterviewBy.SelectedValue = getStaffExit.InterviewedByStaffID.ToString();
            if (getStaffExit.ExitInterviewSubmitted == true)
                rblExitInterviewSubmitted.SelectedIndex = 0;
            else rblExitInterviewSubmitted.SelectedIndex = 1;
            if (getStaffExit.ClearanceFormSignedOff == true)
                rblClearanceFormSigned.SelectedIndex = 0;
            else rblClearanceFormSigned.SelectedIndex = 1;
            if (getStaffExit.HandoverReportSubmitted == true)
                rblHandOverReportSubmitted.SelectedIndex = 0;
            else rblHandOverReportSubmitted.SelectedIndex = 1;

            //populate car details
            if (getStaffExit.CarReturned == null)
            {
                rblCarReturned.SelectedIndex = -1;
                txtCarReturnDate.Text = "";
                txtCarReturnedComments.Text = "";
            }
            else
            {
                if (getStaffExit.CarReturned == true)
                    rblCarReturned.SelectedIndex = 0;
                else rblCarReturned.SelectedIndex = 1;
                if (getStaffExit.CarReturnDate != null)
                    txtCarReturnDate.Text = _hrClass.ShortDateDayStart(getStaffExit.CarReturnDate.ToString());
                else txtCarReturnDate.Text = "";
                txtCarReturnedComments.Text = Convert.ToString(getStaffExit.CarReturnComments);
            }
            //populate House details
            if (getStaffExit.HouseReturned == null)
            {
                rblHouseReturned.SelectedIndex = -1;
                txtHouseReturnDate.Text = "";
                txtHouseReturnedComments.Text = "";
            }
            else
            {
                if (getStaffExit.HouseReturned == true)
                    rblHouseReturned.SelectedIndex = 0;
                else rblHouseReturned.SelectedIndex = 1;
                if (getStaffExit.HouseReturnDate != null)
                    txtHouseReturnDate.Text = _hrClass.ShortDateDayStart(getStaffExit.HouseReturnDate.ToString());
                else txtHouseReturnDate.Text = "";
                txtHouseReturnedComments.Text = Convert.ToString(getStaffExit.HouseReturnComments);
            }
            //populate Telephone details
            if (getStaffExit.TelephoneReturned == null)
            {
                rblTelephoneReturned.SelectedIndex = -1;
                txtTelephoneReturnDate.Text = "";
                txtTelephoneReturnedComments.Text = "";
            }
            else
            {
                if (getStaffExit.TelephoneReturned == true)
                    rblTelephoneReturned.SelectedIndex = 0;
                else rblTelephoneReturned.SelectedIndex = 1;
                if (getStaffExit.TelephoneReturnDate != null)
                    txtTelephoneReturnDate.Text = _hrClass.ShortDateDayStart(getStaffExit.TelephoneReturnDate.ToString());
                else txtTelephoneReturnDate.Text = "";
                txtTelephoneReturnedComments.Text = Convert.ToString(getStaffExit.TelephoneReturnComments);
            }
            //populate Laptop details
            if (getStaffExit.LaptopReturned == null)
            {
                rblLaptopReturned.SelectedIndex = -1;
                txtLaptopReturnDate.Text = "";
                txtLaptopReturnedComments.Text = "";
            }
            else
            {
                if (getStaffExit.LaptopReturned == true)
                    rblLaptopReturned.SelectedIndex = 0;
                else rblLaptopReturned.SelectedIndex = 1;
                if (getStaffExit.LaptopReturnDate != null)
                    txtLaptopReturnDate.Text = _hrClass.ShortDateDayStart(getStaffExit.LaptopReturnDate.ToString());
                else txtLaptopReturnDate.Text = "";
                txtLaptopReturnedComments.Text = Convert.ToString(getStaffExit.LaptopReturnComments);
            }
            //populate LoanGuranteed details
            if (getStaffExit.LoanReturned == null)
            {
                rblLoanGuranteedReturned.SelectedIndex = -1;
                txtLoanGuranteedReturnDate.Text = "";
                txtLoanGuranteedReturnedComments.Text = "";
            }
            else
            {
                if (getStaffExit.LoanReturned == true)
                    rblLoanGuranteedReturned.SelectedIndex = 0;
                else rblLoanGuranteedReturned.SelectedIndex = 1;
                if (getStaffExit.LoanReturnDate != null)
                    txtLoanGuranteedReturnDate.Text = _hrClass.ShortDateDayStart(getStaffExit.LoanReturnDate.ToString());
                else txtLoanGuranteedReturnDate.Text = "";
                txtLoanGuranteedReturnedComments.Text = Convert.ToString(getStaffExit.LoanReturnComments);
            }
            //90% terminal benefits
            if (getStaffExit.NinetyPercentPaid == true)
                rblHas90TerminalBenefitsBeenPaid.SelectedIndex = 0;
            else rblHas90TerminalBenefitsBeenPaid.SelectedIndex = 1;
            if (getStaffExit.NinetyPercentPaidDate != null)
                txtDate90PercentPaid.Text = _hrClass.ShortDateDayStart(getStaffExit.NinetyPercentPaidDate.ToString());
            else txtDate90PercentPaid.Text = "";
            txt90PercentComments.Text = Convert.ToString(getStaffExit.NinetyPercentPaidComments);
            //10% terminal benefits
            if (getStaffExit.TenPercentPaid == true)
                rblHas10TerminalBenefitsBeenPaid.SelectedIndex = 0;
            else rblHas10TerminalBenefitsBeenPaid.SelectedIndex = 1;
            if (getStaffExit.TenPercentPaidDate != null)
                txtDate10PercentPaid.Text = _hrClass.ShortDateDayStart(getStaffExit.TenPercentPaidDate.ToString());
            else txtDate10PercentPaid.Text = "";
            txt10PercentComments.Text = Convert.ToString(getStaffExit.TenPercentPaidComments);
            //CertificateOfServiceIssued teminal benefits
            if (getStaffExit.CertificateOfServiceIssued == true)
                rblCertificateOfServiceIssued.SelectedIndex = 0;
            else rblCertificateOfServiceIssued.SelectedIndex = 1;
            if (getStaffExit.CertificateOfServiceIssuedDate != null)
                txtDateCertificateOfServiceIssued.Text = _hrClass.ShortDateDayStart(getStaffExit.CertificateOfServiceIssuedDate.ToString());
            else txtDateCertificateOfServiceIssued.Text = "";
            txtCertificateOfServiceIssuedComments.Text = Convert.ToString(getStaffExit.CertificateOfServiceIssuedComments);

            //string _uploadedDocName = "No uploaded document", _uploadedDocToolTip = "No uploaded document";
            //if (getStaffExit.ExitDocumentName != null)
            //{
            //    _uploadedDocName = getStaffExit.ExitDocumentName;
            //    _uploadedDocToolTip = "Click to view the uploaded document";
            //}
            //lnkBtnUploadedDocumentLink.Text = _uploadedDocName;
            //lnkBtnUploadedDocumentLink.ToolTip = _uploadedDocToolTip;
            lnkBtnSaveStaffExit.Text = "Update Staff Exit";
        }
        //Display assets asoiciated with a staff
        private void DisplayStaffAllocatedToStaffControls(Guid _staffID)
        {
            //populate car controls
            if (db.StaffVehicles.Any(p => p.StaffID == _staffID && p.IsStaffAssignedVehicle == true))
            {
                lbCarReturned.Visible = true;
                rblCarReturned.Visible = true;
                lbCarReturnDate.Visible = true;
                txtCarReturnDate.Visible = true;
                ImageButtonCarReturnDate.Visible = true;
                lbCarReturnedComments.Visible = true;
                txtCarReturnedComments.Visible = true;
            }
            else
            {
                lbCarReturned.Visible = false;
                rblCarReturned.Visible = false;
                lbCarReturnDate.Visible = false;
                txtCarReturnDate.Visible = false;
                ImageButtonCarReturnDate.Visible = false;
                lbCarReturnedComments.Visible = false;
                txtCarReturnedComments.Visible = false;
            }
            //populate house contols
            if (db.StaffHouses.Any(p => p.StaffID == _staffID && p.IsStaffAssigned == true))
            {
                lbHouseReturned.Visible = true;
                rblHouseReturned.Visible = true;
                lbHouseReturnDate.Visible = true;
                txtHouseReturnDate.Visible = true;
                ImageButtonHouseReturnDate.Visible = true;
                lbHouseReturnedComments.Visible = true;
                txtHouseReturnedComments.Visible = true;
            }
            else
            {
                lbHouseReturned.Visible = false;
                rblHouseReturned.Visible = false;
                lbHouseReturnDate.Visible = false;
                txtHouseReturnDate.Visible = false;
                ImageButtonHouseReturnDate.Visible = false;
                lbHouseReturnedComments.Visible = false;
                txtHouseReturnedComments.Visible = false;
            }
            //populate telephone controls
            if (db.StaffTelephones.Any(p => p.StaffID == _staffID && p.IsStaffAssigned == true))
            {
                lbTelephoneReturned.Visible = true;
                rblTelephoneReturned.Visible = true;
                lbTelephoneReturnDate.Visible = true;
                txtTelephoneReturnDate.Visible = true;
                ImageButtonTelephoneReturnDate.Visible = true;
                lbTelephoneReturnedComments.Visible = true;
                txtTelephoneReturnedComments.Visible = true;
            }
            else
            {
                lbTelephoneReturned.Visible = false;
                rblTelephoneReturned.Visible = false;
                lbTelephoneReturnDate.Visible = false;
                txtTelephoneReturnDate.Visible = false;
                ImageButtonTelephoneReturnDate.Visible = false;
                lbTelephoneReturnedComments.Visible = false;
                txtTelephoneReturnedComments.Visible = false;
            }
            //populate laptop controls
            if (db.StaffLaptops.Any(p => p.StaffID == _staffID && p.IsStaffAssigned == true))
            {
                lbLaptopReturned.Visible = true;
                rblLaptopReturned.Visible = true;
                lbLaptopReturnDate.Visible = true;
                txtLaptopReturnDate.Visible = true;
                ImageButtonLaptopReturnDate.Visible = true;
                lbLaptopReturnedComments.Visible = true;
                txtLaptopReturnedComments.Visible = true;
            }
            else
            {
                lbLaptopReturned.Visible = false;
                rblLaptopReturned.Visible = false;
                lbLaptopReturnDate.Visible = false;
                txtLaptopReturnDate.Visible = false;
                ImageButtonLaptopReturnDate.Visible = false;
                lbLaptopReturnedComments.Visible = false;
                txtLaptopReturnedComments.Visible = false;
            }
            //populate loan controls
            if (db.StaffLoans.Any(p => p.StaffID == _staffID && p.StaffHasALoan == true))
            {
                lbLoanGuranteedReturned.Visible = true;
                rblLoanGuranteedReturned.Visible = true;
                lbLoanGuranteedReturnDate.Visible = true;
                txtLoanGuranteedReturnDate.Visible = true;
                ImageButtonLoanGuranteedReturnDate.Visible = true;
                lbLoanGuranteedReturnedComments.Visible = true;
                txtLoanGuranteedReturnedComments.Visible = true;
            }
            else
            {
                lbLoanGuranteedReturned.Visible = false;
                rblLoanGuranteedReturned.Visible = false;
                lbLoanGuranteedReturnDate.Visible = false;
                txtLoanGuranteedReturnDate.Visible = false;
                ImageButtonLoanGuranteedReturnDate.Visible = false;
                lbLoanGuranteedReturnedComments.Visible = false;
                txtLoanGuranteedReturnedComments.Visible = false;
            }
        }
        //validate controls for saving the exit interview
        private string ValidateExitInterviewControls()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Search for staff to record the exit details against!";
            else if (ddlModeOfExit.SelectedIndex == 0)
            {
                ddlModeOfExit.Focus();
                return "Select mode of exit!";
            }
            else if (_hrClass.isDateValid(txtExitDate.Text.Trim()) == false)
            {
                txtExitDate.Focus();
                return "Invalid exit date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtExitDate) == false)
            {
                return "Invalid exit date entered!";
            }
            else if (ddlInterviewBy.SelectedIndex == 0)
            {
                ddlInterviewBy.Focus();
                return "Select who interviewed the staff!";
            }
            else if (rblExitInterviewSubmitted.SelectedIndex == -1)
            {
                rblExitInterviewSubmitted.Focus();
                return "Indicate whether the exit interview has been submitted or not!";
            }
            else if (rblWillingnessToReturn.SelectedIndex == -1)
            {
                rblWillingnessToReturn.Focus();
                return "Indicate whether staff is willing to return or not!";
            }

            else if (rblClearanceFormSigned.SelectedIndex == -1)
            {
                rblClearanceFormSigned.Focus();
                return "Indicate whether the clearance form has been submitted or not!";
            }
            else if (rblHandOverReportSubmitted.SelectedIndex == -1)
            {
                rblHandOverReportSubmitted.Focus();
                return "Indicate whether the handover report has been submitted or not!";
            }

            else if (lbCarReturned.Visible == true && rblCarReturned.SelectedIndex == -1)
            {
                rblCarReturned.Focus();
                return "Indicate whether the car has been returned or not!";
            }
            else if (lbCarReturned.Visible == true && rblCarReturned.SelectedIndex == 0 && txtCarReturnDate.Text.Trim() == "__/___/____")
            {
                txtCarReturnDate.Focus();
                return "Enter date when the car was returned!";
            }

            else if (lbHouseReturned.Visible == true && rblHouseReturned.SelectedIndex == -1)
            {
                rblHouseReturned.Focus();
                return "Indicate whether the house has been returned or not!";
            }
            else if (lbHouseReturned.Visible == true && rblHouseReturned.SelectedIndex == 0 && txtHouseReturnDate.Text.Trim() == "__/___/____")
            {
                txtHouseReturnDate.Focus();
                return "Enter date when the house was returned!";
            }
            else if (lbTelephoneReturned.Visible == true && rblTelephoneReturned.SelectedIndex == -1)
            {
                rblTelephoneReturned.Focus();
                return "Indicate whether the telephone has been returned or not!";
            }
            else if (lbTelephoneReturned.Visible == true && rblTelephoneReturned.SelectedIndex == 0 && txtTelephoneReturnDate.Text.Trim() == "__/___/____")
            {
                txtTelephoneReturnDate.Focus();
                return "Enter date when the telephone was returned!";
            }
            else if (lbLaptopReturned.Visible == true && rblLaptopReturned.SelectedIndex == -1)
            {
                rblLaptopReturned.Focus();
                return "Indicate whether the laptop has been returned or not!";
            }
            else if (lbLaptopReturned.Visible == true && rblLaptopReturned.SelectedIndex == 0 && txtLaptopReturnDate.Text.Trim() == "__/___/____")
            {
                txtLaptopReturnDate.Focus();
                return "Enter date when the laptop was returned!";
            }
            else if (lbLoanGuranteedReturned.Visible == true && rblLoanGuranteedReturned.SelectedIndex == -1)
            {
                rblLoanGuranteedReturned.Focus();
                return "Indicate whether the loan guranteed has been cleared or not!";
            }
            else if (lbLoanGuranteedReturned.Visible == true && rblLoanGuranteedReturned.SelectedIndex == 0 && txtLoanGuranteedReturnDate.Text.Trim() == "__/___/____")
            {
                txtLoanGuranteedReturnDate.Focus();
                return "Enter date when the loan guranteed was cleared!";
            }
            else if (rblHas90TerminalBenefitsBeenPaid.SelectedIndex == -1)
            {
                rblHas90TerminalBenefitsBeenPaid.Focus();
                return "Indicate whether 90% terminal benefits have been paid or not!";
            }
            else if (rblHas90TerminalBenefitsBeenPaid.SelectedIndex == 0 && txtDate90PercentPaid.Text.Trim() == "__/___/____")
            {
                txtDate90PercentPaid.Focus();
                return "Enter date when the 90% terminal benefits was paid!";
            }
            else if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == -1)
            {
                rblHas10TerminalBenefitsBeenPaid.Focus();
                return "Indicate whether 10% terminal benefits have been paid or not!";
            }
            else if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == 0 && txtDate10PercentPaid.Text.Trim() == "__/___/____")
            {
                txtDate10PercentPaid.Focus();
                return "Enter date when the 10% terminal benefits was paid!";
            }
            else if (rblCertificateOfServiceIssued.SelectedIndex == -1)
            {
                rblCertificateOfServiceIssued.Focus();
                return "Indicate whether certificate of service has been issued or not!";
            }
            else if (rblCertificateOfServiceIssued.SelectedIndex == 0 && txtDateCertificateOfServiceIssued.Text.Trim() == "__/___/____")
            {
                txtDateCertificateOfServiceIssued.Focus();
                return "Enter date when certificate of service was issued!";
            }
            else return "";
        }
        //save exit interview with a document
        //private void SaveStaffExitInterviewWithADocument()
        //{
        //    try
        //    {
        //        string _error = ValidateExitInterviewControls();//check for errors
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffExit);
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            DateTime _exitDate = Convert.ToDateTime(txtExitDate.Text.Trim());
        //            string _exitReason = txtExitReason.Text.Trim();
        //            string _exitComments = txtExitInterviewComments.Text.Trim(); ;
        //            Guid _interviewedByStaffID = _hrClass.ReturnGuid(ddlInterviewBy.SelectedValue);

        //            Guid _savedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
        //            //chech if the file uploader has a document
        //            if (FileUploadDocument.HasFile)
        //            {
        //                //save the exit interview with document
        //                int _fileLength = FileUploadDocument.PostedFile.ContentLength;
        //                //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
        //                if (_hrClass.IsUploadedFileBig(_fileLength) == true)
        //                {
        //                    _hrClass.LoadHRManagerMessageBox(1, "The file uploaded exceeds the allowed limit of 3mb.!", this, PanelStaffExit);
        //                    return;
        //                }
        //                else
        //                {
        //                    string _documentName = FileUploadDocument.FileName;//get name of the uploaded file name
        //                    string _fileExtension = Path.GetExtension(_documentName);

        //                    _fileExtension = _fileExtension.ToLower();
        //                    string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".rtf" };

        //                    bool _isFileAccepted = false;
        //                    foreach (string _acceptedFileExtension in _acceptedFileTypes)
        //                    {
        //                        if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
        //                            _isFileAccepted = true;
        //                    }
        //                    if (_isFileAccepted == false)
        //                    {
        //                        _hrClass.LoadHRManagerMessageBox(1, "The file you are trying to upload is not a permitted file type!", this, PanelStaffExit);
        //                        return;
        //                    }
        //                    else
        //                    {
        //                        //check if the staff has an existing exit record
        //                        if (db.ExitInterviews.Any(p => p.StaffID == _staffID))
        //                        {
        //                            //update the record with an attachement
        //                            int _exitInterviewID = Convert.ToInt32(HiddenFieldExitInterviewID.Value);
        //                            UpdateExitInteview(_exitInterviewID, _staffID, _exitDate, _exitReason, _exitComments, _interviewedByStaffID, _savedByStaffID, FileUploadDocument, _documentName, _fileExtension);
        //                            _hrClass.LoadHRManagerMessageBox(2, "Staff exit interview details have been updated!", this, PanelStaffExit);
        //                            return;
        //                        }
        //                        else
        //                        {
        //                            //save the new exit interview with an attached document
        //                            SaveNewExitInteview(_staffID, _exitDate, _exitReason, _exitComments, _interviewedByStaffID, _savedByStaffID, FileUploadDocument, _documentName, _fileExtension);
        //                            _hrClass.LoadHRManagerMessageBox(2, "The exit interview has been saved againist the employee!", this, PanelStaffExit);
        //                            return;
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                //save exit interview with no document
        //                //check if the selected staff has a record that is already saved
        //                if (db.ExitInterviews.Any(p => p.StaffID == _staffID))
        //                {
        //                    //update the existing exit interview record
        //                    int _exitInterviewID = Convert.ToInt32(HiddenFieldExitInterviewID.Value);
        //                    UpdateExitInteview(_exitInterviewID, _staffID, _exitDate, _exitReason, _exitComments, _interviewedByStaffID, _savedByStaffID);
        //                    _hrClass.LoadHRManagerMessageBox(2, "Staff exit interview details have been updated!", this, PanelStaffExit);
        //                    return;
        //                }
        //                else
        //                {
        //                    //save the new exit interview without a document
        //                    SaveNewExitInteview(_staffID, _exitDate, _exitReason, _exitComments, _interviewedByStaffID, _savedByStaffID);
        //                    _hrClass.LoadHRManagerMessageBox(2, "The exit interview has been saved againist the employee!", this, PanelStaffExit);
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
        //        return;
        //    }
        //}
        //save exit interview with no document
        private void SaveStaffExitWithNoDocument()
        {
            try
            {
                string _error = ValidateExitInterviewControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffExit);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    DateTime _exitDate = Convert.ToDateTime(txtExitDate.Text.Trim());
                    string _ExternalExitReason = txtExternalExitReason.Text.Trim();
                    string _InternalExitReason = txtInternalExitReason.Text.Trim();
                    string _BankAdvice = txtBankAdvice.Text.Trim();
                    string _exitComments = txtExitInterviewComments.Text.Trim(); ;
                    Guid _interviewedByStaffID = _hrClass.ReturnGuid(ddlInterviewBy.SelectedValue);
                    Guid _savedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());

                    //save exit interview with no document
                    //check if the selected staff has a record that is already saved
                    if (db.StaffExits.Any(p => p.StaffID == _staffID))
                    {
                        //update the existing staff exit record
                        int _staffExitID = Convert.ToInt32(HiddenFieldStaffExitID.Value);
                        UpdateStaffExit(_staffExitID, _staffID, _exitDate, _ExternalExitReason,_InternalExitReason,_BankAdvice, _exitComments, _interviewedByStaffID, _savedByStaffID);
                        _hrClass.LoadHRManagerMessageBox(2, "Staff exit details have been updated!", this, PanelStaffExit);
                        return;
                    }
                    else
                    {
                        //save the new staff exit without a document
                        SaveNewStaffExit(_staffID, _exitDate, _ExternalExitReason, _InternalExitReason, _BankAdvice, _exitComments, _interviewedByStaffID, _savedByStaffID);
                        _hrClass.LoadHRManagerMessageBox(2, "The staff exit has been saved againist the employee!", this, PanelStaffExit);
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffExit);
                return;
            }
        }
        //method for saving a new staff exit
        private void SaveNewStaffExit(Guid _staffID, DateTime _exitDate, string _ExternalExitReason, string _InternalExitReason, string _BankAdvice, string _exitComments, Guid _interviewedByStaffID, Guid _savedByStaffID)
        {
            StaffExit newStaffExit = new StaffExit();
            newStaffExit.StaffID = _staffID;
            newStaffExit.ModeOfExitID = Convert.ToInt32(ddlModeOfExit.SelectedValue);
            newStaffExit.ExitDate = _exitDate.Date;
            newStaffExit.ExternalExitReason=_ExternalExitReason;
            newStaffExit.InternalExitReason=_InternalExitReason;
            newStaffExit.BankAdvice = _BankAdvice;
            newStaffExit.ExitComment = _exitComments;
            newStaffExit.ExitClearance = txtClearance.Text.Trim();
            newStaffExit.InterviewedByStaffID = _interviewedByStaffID;
            if (rblWillingnessToReturn.SelectedIndex == 0)
                newStaffExit.WillingnessToReturn = true;
            else newStaffExit.WillingnessToReturn = false;
            if (rblExitInterviewSubmitted.SelectedIndex == 0)
                newStaffExit.ExitInterviewSubmitted = true;
            else newStaffExit.ExitInterviewSubmitted = false;
            if (rblClearanceFormSigned.SelectedIndex == 0)
                newStaffExit.ClearanceFormSignedOff = true;
            else newStaffExit.ClearanceFormSignedOff = false;
            if (rblHandOverReportSubmitted.SelectedIndex == 0)
                newStaffExit.HandoverReportSubmitted = true;
            else newStaffExit.HandoverReportSubmitted = false;
            //car return details
            if (lbCarReturned.Visible == true)
            {
                if (rblCarReturned.SelectedIndex == 0)
                {
                    newStaffExit.CarReturned = true;
                    newStaffExit.CarReturnDate = Convert.ToDateTime(txtCarReturnDate.Text.Trim()).Date;
                    newStaffExit.CarReturnComments = txtCarReturnedComments.Text.Trim();
                }
                else
                {
                    newStaffExit.CarReturned = false;
                    newStaffExit.CarReturnDate = null;
                    newStaffExit.CarReturnComments = null;
                }
            }
            else
            {
                newStaffExit.CarReturned = null;
                newStaffExit.CarReturnDate = null;
                newStaffExit.CarReturnComments = null;
            }
            //House return details
            if (lbHouseReturned.Visible == true)
            {
                if (rblHouseReturned.SelectedIndex == 0)
                {
                    newStaffExit.HouseReturned = true;
                    newStaffExit.HouseReturnDate = Convert.ToDateTime(txtHouseReturnDate.Text.Trim()).Date;
                    newStaffExit.HouseReturnComments = txtHouseReturnedComments.Text.Trim();
                }
                else
                {
                    newStaffExit.HouseReturned = false;
                    newStaffExit.HouseReturnDate = null;
                    newStaffExit.HouseReturnComments = null;
                }
            }
            else
            {
                newStaffExit.HouseReturned = null;
                newStaffExit.HouseReturnDate = null;
                newStaffExit.HouseReturnComments = null;
            }
            //Telephone return details
            if (lbTelephoneReturned.Visible == true)
            {
                if (rblTelephoneReturned.SelectedIndex == 0)
                {
                    newStaffExit.TelephoneReturned = true;
                    newStaffExit.TelephoneReturnDate = Convert.ToDateTime(txtTelephoneReturnDate.Text.Trim()).Date;
                    newStaffExit.TelephoneReturnComments = txtTelephoneReturnedComments.Text.Trim();
                }
                else
                {
                    newStaffExit.TelephoneReturned = false;
                    newStaffExit.TelephoneReturnDate = null;
                    newStaffExit.TelephoneReturnComments = null;
                }
            }
            else
            {
                newStaffExit.TelephoneReturned = null;
                newStaffExit.TelephoneReturnDate = null;
                newStaffExit.TelephoneReturnComments = null;
            }
            //Laptop return details
            if (lbLaptopReturned.Visible == true)
            {
                if (rblLaptopReturned.SelectedIndex == 0)
                {
                    newStaffExit.LaptopReturned = true;
                    newStaffExit.LaptopReturnDate = Convert.ToDateTime(txtLaptopReturnDate.Text.Trim()).Date;
                    newStaffExit.LaptopReturnComments = txtLaptopReturnedComments.Text.Trim();
                }
                else
                {
                    newStaffExit.LaptopReturned = false;
                    newStaffExit.LaptopReturnDate = null;
                    newStaffExit.LaptopReturnComments = null;
                }
            }
            else
            {
                newStaffExit.LaptopReturned = null;
                newStaffExit.LaptopReturnDate = null;
                newStaffExit.LaptopReturnComments = null;
            }
            //Loan return details
            if (lbLoanGuranteedReturned.Visible == true)
            {
                if (rblLoanGuranteedReturned.SelectedIndex == 0)
                {
                    newStaffExit.LoanReturned = true;
                    newStaffExit.LoanReturnDate = Convert.ToDateTime(txtLoanGuranteedReturnDate.Text.Trim()).Date;
                    newStaffExit.LoanReturnComments = txtLoanGuranteedReturnedComments.Text.Trim();
                }
                else
                {
                    newStaffExit.LoanReturned = false;
                    newStaffExit.LoanReturnDate = null;
                    newStaffExit.LoanReturnComments = null;
                }
            }
            else
            {
                newStaffExit.LoanReturned = null;
                newStaffExit.LoanReturnDate = null;
                newStaffExit.LoanReturnComments = null;
            }
            //90% terminal benefits
            if (rblHas90TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                newStaffExit.NinetyPercentPaid = true;
                newStaffExit.NinetyPercentPaidDate = Convert.ToDateTime(txtDate90PercentPaid.Text.Trim()).Date;
                newStaffExit.NinetyPercentPaidComments = txt90PercentComments.Text.Trim();
            }
            else
            {
                newStaffExit.NinetyPercentPaid = false;
                newStaffExit.NinetyPercentPaidDate = null;
                newStaffExit.NinetyPercentPaidComments = null;
            }

            //10% terminal benefits
            if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                newStaffExit.TenPercentPaid = true;
                newStaffExit.TenPercentPaidDate = Convert.ToDateTime(txtDate10PercentPaid.Text.Trim()).Date;
                newStaffExit.TenPercentPaidComments = txt10PercentComments.Text.Trim();
            }
            else
            {
                newStaffExit.TenPercentPaid = false;
                newStaffExit.TenPercentPaidDate = null;
                newStaffExit.TenPercentPaidComments = null;
            }
            //10% terminal benefits
            if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                newStaffExit.CertificateOfServiceIssued = true;
                newStaffExit.CertificateOfServiceIssuedDate = Convert.ToDateTime(txtDateCertificateOfServiceIssued.Text.Trim()).Date;
                newStaffExit.CertificateOfServiceIssuedComments = txtCertificateOfServiceIssuedComments.Text.Trim();
            }
            else
            {
                newStaffExit.CertificateOfServiceIssued = false;
                newStaffExit.CertificateOfServiceIssuedDate = null;
                newStaffExit.CertificateOfServiceIssuedComments = null;
            }


            //newStaffExit.ExitDocumentName = _documentName;
            //newStaffExit.ExitDocumentExtension = _documentExtension;
            //newStaffExit.ExitDocument = _exitDocument;
            newStaffExit.SavedByStaffID = _savedByStaffID;
            newStaffExit.DateSaved = DateTime.Now;
            db.StaffExits.InsertOnSubmit(newStaffExit);
            db.SubmitChanges();
            HiddenFieldStaffExitID.Value = newStaffExit.StaffExitID.ToString();
        }
        //method for saving a new staff exit with an attached dcoument
        private void SaveNewStaffExit(Guid _staffID, DateTime _exitDate, string _ExternalExitReason, string _InternalExitReason, string _BankAdvice, string _exitComments, Guid _interviewedByStaffID, Guid _savedByStaffID, FileUpload _fileUpload, string _documentName, string _docExtension)
        {
            //save the document in the temp documents folder
            _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _documentName));
            string _fileLocation = Server.MapPath("~/TempDocs/" + _documentName);
            //save exit interview  document into the database
            FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
            //get document info
            FileInfo documentInfo = new FileInfo(_fileLocation);
            int fileSize = (int)documentInfo.Length;//get file size
            byte[] fileDocument = new byte[fileSize];
            fs.Read(fileDocument, 0, fileSize);//read the document from the file stream
            //save the new document
            StaffExit newStaffExit = new StaffExit();
            newStaffExit.StaffID = _staffID;
            newStaffExit.ExitDate = _exitDate.Date;
            newStaffExit.ExternalExitReason=_ExternalExitReason;
            newStaffExit.InternalExitReason=_InternalExitReason;
            newStaffExit.BankAdvice = _BankAdvice;
            newStaffExit.ExitComment = _exitComments;
            newStaffExit.InterviewedByStaffID = _interviewedByStaffID;
            newStaffExit.ExitDocumentName = _documentName;
            newStaffExit.ExitDocumentExtension = _docExtension;
            newStaffExit.ExitDocument = fileDocument;
            newStaffExit.SavedByStaffID = _savedByStaffID;
            newStaffExit.DateSaved = DateTime.Now;
            db.StaffExits.InsertOnSubmit(newStaffExit);
            db.SubmitChanges();
            HiddenFieldStaffExitID.Value = newStaffExit.StaffExitID.ToString();
            //lnkBtnUploadedDocumentLink.Text = _documentName;
            //lnkBtnUploadedDocumentLink.ToolTip = "Click to view the uploaded document";
        }
        //method for updating an exitsting staff exit without an attachment
        private void UpdateStaffExit(int _StaffExitID, Guid _staffID, DateTime _exitDate, string _ExternalExitReason, string _InternalExitReason, string _BankAdvice, string _exitComments, Guid _interviewedByStaffID, Guid _savedByStaffID)
        {
            StaffExit updateStaffExit = db.StaffExits.Single(p => p.StaffExitID == _StaffExitID); ;
            updateStaffExit.StaffID = _staffID;
            updateStaffExit.ModeOfExitID = Convert.ToInt32(ddlModeOfExit.SelectedValue);
            updateStaffExit.ExitDate = _exitDate.Date;
            updateStaffExit.ExternalExitReason = _ExternalExitReason;
            updateStaffExit.InternalExitReason = _InternalExitReason;
            updateStaffExit.BankAdvice = _BankAdvice;
            updateStaffExit.ExitComment = _exitComments;
            updateStaffExit.ExitClearance = txtClearance.Text.Trim();
            updateStaffExit.InterviewedByStaffID = _interviewedByStaffID;
            if (rblWillingnessToReturn.SelectedIndex == 0)
                updateStaffExit.WillingnessToReturn = true;
            else updateStaffExit.WillingnessToReturn = false;
            if (rblExitInterviewSubmitted.SelectedIndex == 0)
                updateStaffExit.ExitInterviewSubmitted = true;
            else updateStaffExit.ExitInterviewSubmitted = false;
            if (rblClearanceFormSigned.SelectedIndex == 0)
                updateStaffExit.ClearanceFormSignedOff = true;
            else updateStaffExit.ClearanceFormSignedOff = false;
            if (rblHandOverReportSubmitted.SelectedIndex == 0)
                updateStaffExit.HandoverReportSubmitted = true;
            else updateStaffExit.HandoverReportSubmitted = false;
            //car return details
            if (lbCarReturned.Visible == true)
            {
                if (rblCarReturned.SelectedIndex == 0)
                {
                    updateStaffExit.CarReturned = true;
                    updateStaffExit.CarReturnDate = Convert.ToDateTime(txtCarReturnDate.Text.Trim()).Date;
                    updateStaffExit.CarReturnComments = txtCarReturnedComments.Text.Trim();
                }
                else
                {
                    updateStaffExit.CarReturned = false;
                    updateStaffExit.CarReturnDate = null;
                    updateStaffExit.CarReturnComments = null;
                }
            }
            else
            {
                updateStaffExit.CarReturned = null;
                updateStaffExit.CarReturnDate = null;
                updateStaffExit.CarReturnComments = null;
            }
            //House return details
            if (lbHouseReturned.Visible == true)
            {
                if (rblHouseReturned.SelectedIndex == 0)
                {
                    updateStaffExit.HouseReturned = true;
                    updateStaffExit.HouseReturnDate = Convert.ToDateTime(txtHouseReturnDate.Text.Trim()).Date;
                    updateStaffExit.HouseReturnComments = txtHouseReturnedComments.Text.Trim();
                }
                else
                {
                    updateStaffExit.HouseReturned = false;
                    updateStaffExit.HouseReturnDate = null;
                    updateStaffExit.HouseReturnComments = null;
                }
            }
            else
            {
                updateStaffExit.HouseReturned = null;
                updateStaffExit.HouseReturnDate = null;
                updateStaffExit.HouseReturnComments = null;
            }
            //Telephone return details
            if (lbTelephoneReturned.Visible == true)
            {
                if (rblTelephoneReturned.SelectedIndex == 0)
                {
                    updateStaffExit.TelephoneReturned = true;
                    updateStaffExit.TelephoneReturnDate = Convert.ToDateTime(txtTelephoneReturnDate.Text.Trim()).Date;
                    updateStaffExit.TelephoneReturnComments = txtTelephoneReturnedComments.Text.Trim();
                }
                else
                {
                    updateStaffExit.TelephoneReturned = false;
                    updateStaffExit.TelephoneReturnDate = null;
                    updateStaffExit.TelephoneReturnComments = null;
                }
            }
            else
            {
                updateStaffExit.TelephoneReturned = null;
                updateStaffExit.TelephoneReturnDate = null;
                updateStaffExit.TelephoneReturnComments = null;
            }
            //Laptop return details
            if (lbLaptopReturned.Visible == true)
            {
                if (rblLaptopReturned.SelectedIndex == 0)
                {
                    updateStaffExit.LaptopReturned = true;
                    updateStaffExit.LaptopReturnDate = Convert.ToDateTime(txtLaptopReturnDate.Text.Trim()).Date;
                    updateStaffExit.LaptopReturnComments = txtLaptopReturnedComments.Text.Trim();
                }
                else
                {
                    updateStaffExit.LaptopReturned = false;
                    updateStaffExit.LaptopReturnDate = null;
                    updateStaffExit.LaptopReturnComments = null;
                }
            }
            else
            {
                updateStaffExit.LaptopReturned = null;
                updateStaffExit.LaptopReturnDate = null;
                updateStaffExit.LaptopReturnComments = null;
            }
            //Loan return details
            if (lbLoanGuranteedReturned.Visible == true)
            {
                if (rblLoanGuranteedReturned.SelectedIndex == 0)
                {
                    updateStaffExit.LoanReturned = true;
                    updateStaffExit.LoanReturnDate = Convert.ToDateTime(txtLoanGuranteedReturnDate.Text.Trim()).Date;
                    updateStaffExit.LoanReturnComments = txtLoanGuranteedReturnedComments.Text.Trim();
                }
                else
                {
                    updateStaffExit.LoanReturned = false;
                    updateStaffExit.LoanReturnDate = null;
                    updateStaffExit.LoanReturnComments = null;
                }
            }
            else
            {
                updateStaffExit.LoanReturned = null;
                updateStaffExit.LoanReturnDate = null;
                updateStaffExit.LoanReturnComments = null;
            }
            //90% terminal benefits
            if (rblHas90TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                updateStaffExit.NinetyPercentPaid = true;
                updateStaffExit.NinetyPercentPaidDate = Convert.ToDateTime(txtDate90PercentPaid.Text.Trim()).Date;
                updateStaffExit.NinetyPercentPaidComments = txt90PercentComments.Text.Trim();
            }
            else
            {
                updateStaffExit.NinetyPercentPaid = false;
                updateStaffExit.NinetyPercentPaidDate = null;
                updateStaffExit.NinetyPercentPaidComments = null;
            }

            //10% terminal benefits
            if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                updateStaffExit.TenPercentPaid = true;
                updateStaffExit.TenPercentPaidDate = Convert.ToDateTime(txtDate10PercentPaid.Text.Trim()).Date;
                updateStaffExit.TenPercentPaidComments = txt10PercentComments.Text.Trim();
            }
            else
            {
                updateStaffExit.TenPercentPaid = false;
                updateStaffExit.TenPercentPaidDate = null;
                updateStaffExit.TenPercentPaidComments = null;
            }
            //10% terminal benefits
            if (rblHas10TerminalBenefitsBeenPaid.SelectedIndex == 0)
            {
                updateStaffExit.CertificateOfServiceIssued = true;
                updateStaffExit.CertificateOfServiceIssuedDate = Convert.ToDateTime(txtDateCertificateOfServiceIssued.Text.Trim()).Date;
                updateStaffExit.CertificateOfServiceIssuedComments = txtCertificateOfServiceIssuedComments.Text.Trim();
            }
            else
            {
                updateStaffExit.CertificateOfServiceIssued = false;
                updateStaffExit.CertificateOfServiceIssuedDate = null;
                updateStaffExit.CertificateOfServiceIssuedComments = null;
            }
            //updateStaffExit.ExitDocumentName = _documentName;
            //updateStaffExit.ExitDocumentExtension = _documentExtension;
            //updateStaffExit.ExitDocument = _exitDocument;
            updateStaffExit.SavedByStaffID = _savedByStaffID;
            updateStaffExit.DateSaved = DateTime.Now;
            db.SubmitChanges();
        }
        //method for updating an exitsting exit interview with an attachment
        private void UpdateStaffExit(int _StaffExitID, Guid _staffID, DateTime _exitDate, string _ExternalExitReason, string _InternalExitReason, string _BankAdvice, string _exitComments, Guid _interviewedByStaffID, Guid _savedByStaffID, FileUpload _fileUpload, string _documentName, string _docExtension)
        {
            //save the document in the temp documents folder
            _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _documentName));
            string _fileLocation = Server.MapPath("~/TempDocs/" + _documentName);
            //save exit interview  document into the database
            FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
            //get document info
            FileInfo documentInfo = new FileInfo(_fileLocation);
            int fileSize = (int)documentInfo.Length;//get file size
            byte[] fileDocument = new byte[fileSize];
            fs.Read(fileDocument, 0, fileSize);//read the document from the file stream
            //update existing document
            StaffExit updateStaffExit = db.StaffExits.Single(p => p.StaffExitID == _StaffExitID); ;
            updateStaffExit.StaffID = _staffID;
            updateStaffExit.ExitDate = _exitDate;
            updateStaffExit.ExternalExitReason=_ExternalExitReason;
            updateStaffExit.InternalExitReason=_InternalExitReason;
            updateStaffExit.BankAdvice =_BankAdvice;
            updateStaffExit.ExitComment = _exitComments;
            updateStaffExit.InterviewedByStaffID = _interviewedByStaffID;
            updateStaffExit.ExitDocumentName = _documentName;
            updateStaffExit.ExitDocumentExtension = _docExtension;
            updateStaffExit.ExitDocument = fileDocument;
            updateStaffExit.SavedByStaffID = _savedByStaffID;
            updateStaffExit.DateSaved = DateTime.Now;
            db.SubmitChanges();
            //lnkBtnUploadedDocumentLink.Text = _documentName;
            //lnkBtnUploadedDocumentLink.ToolTip = "Click to view the uploaded document";
        }
        //save staff exit
        protected void lnkBtnSaveStaffExit_Click(object sender, EventArgs e)
        {
            SaveStaffExitWithNoDocument(); return;
        }
        //view exit interview document that has been uploaded
        private void ViewExitInterviewDocument()
        {
            //try
            //{
            //    if (lnkBtnUploadedDocumentLink.Text == "No uploaded document")
            //    {
            //        //do nothing
            //    }
            //    else
            //    {
            //        //load the document that has been uploaded
            //        Session["HRDocumentViewCase"] = 2;//case 2 for viewing exit interview document
            //        string strPageUrl = "HR_Document_Viewer.aspx?ref=" + HiddenFieldStaffExitID.Value;
            //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window                 
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Document load failed . " + ex.Message.ToString() + ". Please try again!", this, PanelStaffExit); return;
            //}
        }
        protected void lnkBtnUploadedDocumentLink_OnClick(object sender, EventArgs e)
        {
            //ViewExitInterviewDocument(); return;
        }
    }

}