﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.HR_Module
{
    public partial class HR_Document_Viewer : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    string _docID = Request.QueryString["ref"].ToString();//get document id passed

                    if (_docID != null)
                    {
                        // view the document
                        LoadHRDocuments(Convert.ToInt32(_docID));
                        // LoadStaffDocument(Convert.ToInt32(_docID));
                    }
                }
                catch { }
            }
        }
        private void LoadStaffDocument(int DocumentID)
        {
            if (db.StaffDocs.Any(p => p.staffdoc_iStaffDocID == DocumentID))
            {
                StaffDoc getStaffDocument = db.StaffDocs.Single(p => p.staffdoc_iStaffDocID == DocumentID);
                if (getStaffDocument.staffdoc_vbDocument != null)
                {
                    string _docExtension = getStaffDocument.staffdoc_vDocExtension;
                    string _documentName = getStaffDocument.staffdoc_vDocName;
                    //lbDocumentViewer.Text = "View Document " + _documentName;
                    //get document content type as per the document's extension
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getStaffDocument.staffdoc_vbDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
        }
        //load exit interview document
        private void LoadExitInterviewDocument(int _staffExitID)
        {
            if (db.StaffExits.Any(p => p.StaffExitID == _staffExitID))
            {
                StaffExit getStaffExit =db.StaffExits.Single(p => p.StaffExitID == _staffExitID);
                if (getStaffExit.ToString() != null)
                {
                    string _docExtension = getStaffExit.ExitDocumentExtension;
                    string _documentName = getStaffExit.ExitDocumentName;
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getStaffExit.ExitDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
        }
        //load document
        private void LoadHRDocuments(int _docID)
        {
            int _docCase = Convert.ToInt32(Session["HRDocumentViewCase"]);
            switch (_docCase)
            {
                case 1://staff document
                    LoadStaffDocument(_docID);
                    break;
                case 2://exit interview document
                    LoadExitInterviewDocument(_docID);
                    break;
            }
        }

    }
}