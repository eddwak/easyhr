﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Performance_Review : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                PopulateDropDowns();//populate drop downs
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffPerformanceDetails(_staffID);//load staff appraisal details

                    return;
                }
            }
            catch { }
        }
        //load staff  performance details
        private void DisplayStaffPerformanceDetails(Guid _staffID)
        {
            //load staff performance objective 
            LoadStaffToAppraiseDetails(_staffID);
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelStaffPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformanceReview);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformanceReview);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformanceReview);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffPerformanceDetails(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffPerformanceDetails(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffPerformanceReview);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffPerformanceReview);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //populate drop downs
        private void PopulateDropDowns()
        {
            _hrClass.GenerateYears(ddlAnnualPerformancePlanYear);//display performance year drop down
            ddlAnnualPerformancePlanYear.SelectedValue = DateTime.Now.Year.ToString();//select current year
            //_hrClass.GetStaffNames(ddlQuarter1AppraisedBy, "Appraised By");//populate  quarter 1 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter2AppraisedBy, "Appraised By");//populate  quarter 2 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter3AppraisedBy, "Appraised By");//populate  quarter 3 appraised by dropdown
            //_hrClass.GetStaffNames(ddlQuarter4AppraisedBy, "Appraised By");//populate  quarter 4 appraised by dropdown
        }
        //load staff performance details for the selected year
        protected void ddlAnnualPerformancePlanYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlAnnualPerformancePlanYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "An error occured. There is no performance year selected. Select a performance year!", this, PanelStaffPerformanceReview);
                    return;
                }
                else
                {
                    string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _staffName = _hrClass.getStaffNames(_staffID);
                    lbStaffToAppraiseHeader.Text = _staffName + " Performance Objective Details for the Year " + _performanceYear;
                    DisplayPerformancePlanObjective(_staffID, _performanceYear);
                    PanelStaffPerformancePlanQuarteryReview.Visible = false;
                    PanelPerformanceObjectiveListing.Visible = true;
                    PanelAddPerformanceObjective.Visible = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffPerformanceReview);
                return;
            }
        }

        //load staff to appraise details
        private void LoadStaffToAppraiseDetails(Guid _staffID)
        {
            string _staffName = _hrClass.getStaffNames(_staffID);
            string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue.ToString();
            lbStaffToAppraiseHeader.Text = _staffName + " Performance Objective Details for the Year " + _performanceYear + ".";
            //load performance details for staff year
            DisplayPerformancePlanObjective(_staffID, _performanceYear);
            PanelStaffToAppraiseHeader.Visible = true;
            //PanelPerformanceObjectiveMenu.Visible = false;
            PanelAddPerformanceObjective.Visible = false;
            PanelPerformanceObjectiveListing.Visible = true;
            //TabContainerPerformanceReview.ActiveTabIndex = 0;//go to tab one

            PanelAnnualPerformanceObjectiveDetails.Enabled = false;
            PanelStaffPerformancePlanQuarteryReview.Visible = false;
            ddlStaffQuarterToReview.SelectedIndex = 0;
            //disable performance details panels
            PanelQuarter1PerformanceDetails.Enabled = false;
            PanelQuarter2PerformanceDetails.Enabled = false;
            PanelQuarter3PerformanceDetails.Enabled = false;
            PanelQuarter4PerformanceDetails.Enabled = false;
            //enable appraiser's panels details
            //PanelQuarter1AppraiserDetails.Enabled = true;
            //PanelQuarter2AppraiserDetails.Enabled = true;
            //PanelQuarter3AppraiserDetails.Enabled = true;
            //PanelQuarter4AppraiserDetails.Enabled = true;
            ////enable appraiser's date buttons
            //ImageButtonQuarter1AppraisalDate.Visible = true;
            //ImageButtonQuarter2AppraisalDate.Visible = true;
            //ImageButtonQuarter3AppraisalDate.Visible = true;
            //ImageButtonQuarter4AppraisalDate.Visible = true;
            return;
        }
        //close perfomance objective details
        protected void lnkBtnClosePerformancePlan_Click(object sender, EventArgs e)
        {
            PanelPerformanceObjectiveListing.Visible = true;
            PanelAddPerformanceObjective.Visible = false;
            _hrClass.ClearEntryControls(PanelAddPerformanceObjective);
            return;
        }
        //display staff performance plan objective details
        private void DisplayPerformancePlanObjective(Guid _staffID, string _year)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffPerfomancePlanObjective(_staffID, _year);
                gvPerformancePlanObjectives.DataSourceID = null;
                gvPerformancePlanObjectives.DataSource = _display;
                Session["gvPerformancePlanObjectivesData"] = _display;
                gvPerformancePlanObjectives.DataBind();
                return;
            }
            catch { }
        }
        //load perfomance details 
        protected void gvPerformancePlanObjectives_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("QuarterlyReview") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFPerformancePlanID = (HiddenField)gvPerformancePlanObjectives.Rows[ID].FindControl("HiddenField1");
                    int _performancePlanID = Convert.ToInt32(_HFPerformancePlanID.Value);
                    LoadPerformanceObjectivePlanControlsForEdit(_performancePlanID);
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        //load performance objective plan for edit
        private void LoadPerformanceObjectivePlanControlsForEdit(int _performancePlanID)
        {
            PerformancePlan getPerformancePlan = db.PerformancePlans.Single(p => p.PerformancePlanID == _performancePlanID);
            HiddenFieldPerformancePlanID.Value = _performancePlanID.ToString();
            ddlAnnualPerformancePlanYear.SelectedValue = getPerformancePlan.PerformanceYear;
            txtPerformanceObjective.Text = getPerformancePlan.PerformanceObjective;
            txtKeyPerformanceIndicator.Text = getPerformancePlan.KeyPerformanceIndicators;
            txtAnnualPercentageWeight.Text = getPerformancePlan.PercentageWeight.ToString();
            txtAnnualPercentageScore.Text = getPerformancePlan.AnnualPercentageScore.ToString();

            //quarter one details
            txtQuarter1Target.Text = getPerformancePlan.Quarter1Target;
            txtQuarter1Achieved.Text = getPerformancePlan.Quarter1Achieved;
            txtQuarter1Rating.Text = getPerformancePlan.Quarter1Rating;
            txtQuarter1AverageRating.Text = getPerformancePlan.Quarter1AverageRating;
            txtQuarter1Comments.Text = getPerformancePlan.Quarter1Comments;
            txtQuarter1NextActionPlan.Text = getPerformancePlan.Quarter1NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter1AppraisedByStaffID != null)
            //    ddlQuarter1AppraisedBy.SelectedValue = getPerformancePlan.Quarter1AppraisedByStaffID.ToString();
            //else ddlQuarter1AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter1AppraisalDate != null)
            //    txtQuarter1AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter1AppraisalDate.ToString());
            //else txtQuarter1AppraisalDate.Text = "";
            //txtQuarter1AppraiserComments.Text = getPerformancePlan.Quarter1AppraiserComments;

            //if (getPerformancePlan.Quarter1ReviewedBySupervisor == true)
            //    rblHasQuarter1BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter1BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter1ReviewedByHR == true)
            //    rblHasQuarter1BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter1BeenReviewedByHR.SelectedIndex = 1;

            //quarter two details
            txtQuarter2Target.Text = getPerformancePlan.Quarter2Target;
            txtQuarter2Achieved.Text = getPerformancePlan.Quarter2Achieved;
            txtQuarter2Rating.Text = getPerformancePlan.Quarter2Rating;
            txtQuarter2AverageRating.Text = getPerformancePlan.Quarter2AverageRating;
            txtQuarter2Comments.Text = getPerformancePlan.Quarter2Comments;
            txtQuarter2NextActionPlan.Text = getPerformancePlan.Quarter2NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter2AppraisedByStaffID != null)
            //    ddlQuarter2AppraisedBy.SelectedValue = getPerformancePlan.Quarter2AppraisedByStaffID.ToString();
            //else ddlQuarter2AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter2AppraisalDate != null)
            //    txtQuarter2AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter2AppraisalDate.ToString());
            //else txtQuarter2AppraisalDate.Text = ""; ;
            //txtQuarter2AppraiserComments.Text = getPerformancePlan.Quarter2AppraiserComments;

            //if (getPerformancePlan.Quarter2ReviewedBySupervisor == true)
            //    rblHasQuarter2BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter2BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter2ReviewedByHR == true)
            //    rblHasQuarter2BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter2BeenReviewedByHR.SelectedIndex = 1;


            //quarter three details
            txtQuarter3Target.Text = getPerformancePlan.Quarter3Target;
            txtQuarter3Achieved.Text = getPerformancePlan.Quarter3Achieved;
            txtQuarter3Rating.Text = getPerformancePlan.Quarter3Rating;
            txtQuarter3AverageRating.Text = getPerformancePlan.Quarter3AverageRating;
            txtQuarter3Comments.Text = getPerformancePlan.Quarter3Comments;
            txtQuarter3NextActionPlan.Text = getPerformancePlan.Quarter3NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter3AppraisedByStaffID != null)
            //    ddlQuarter3AppraisedBy.SelectedValue = getPerformancePlan.Quarter3AppraisedByStaffID.ToString();
            //else ddlQuarter3AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter3AppraisalDate != null)
            //    txtQuarter3AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter3AppraisalDate.ToString());
            //else txtQuarter3AppraisalDate.Text = ""; ;
            //txtQuarter3AppraiserComments.Text = getPerformancePlan.Quarter3AppraiserComments;

            //if (getPerformancePlan.Quarter3ReviewedBySupervisor == true)
            //    rblHasQuarter3BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter3BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter3ReviewedByHR == true)
            //    rblHasQuarter3BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter3BeenReviewedByHR.SelectedIndex = 1;

            //quarter four details
            txtQuarter4Target.Text = getPerformancePlan.Quarter4Target;
            txtQuarter4Achieved.Text = getPerformancePlan.Quarter4Achieved;
            txtQuarter4Rating.Text = getPerformancePlan.Quarter4Rating;
            txtQuarter4AverageRating.Text = getPerformancePlan.Quarter4AverageRating;
            txtQuarter4Comments.Text = getPerformancePlan.Quarter4Comments;
            txtQuarter4NextActionPlan.Text = getPerformancePlan.Quarter4NextQuarterActionPlan;
            //if (getPerformancePlan.Quarter4AppraisedByStaffID != null)
            //    ddlQuarter4AppraisedBy.SelectedValue = getPerformancePlan.Quarter4AppraisedByStaffID.ToString();
            //else ddlQuarter4AppraisedBy.SelectedIndex = 0;
            //if (getPerformancePlan.Quarter4AppraisalDate != null)
            //    txtQuarter4AppraisalDate.Text = _hrClass.ShortDateDayStart(getPerformancePlan.Quarter4AppraisalDate.ToString());
            //else txtQuarter4AppraisalDate.Text = ""; ;
            //txtQuarter4AppraiserComments.Text = getPerformancePlan.Quarter4AppraiserComments;

            //if (getPerformancePlan.Quarter4ReviewedBySupervisor == true)
            //    rblHasQuarter4BeenReviewedBySupervisor.SelectedIndex = 0;
            //else rblHasQuarter4BeenReviewedBySupervisor.SelectedIndex = 1;

            //if (getPerformancePlan.Quarter4ReviewedByHR == true)
            //    rblHasQuarter4BeenReviewedByHR.SelectedIndex = 0;
            //else rblHasQuarter4BeenReviewedByHR.SelectedIndex = 1;

            lbAddPerformanceObjectiveHeader.Text = "Performance Objective for the Year";
            //lnkBtnSavePerformancePlan.Text = "Update Performance Details";
            PanelAddPerformanceObjective.Visible = true;
            PanelPerformanceObjectiveListing.Visible = false;
            return;
        }
        //download the review a staff quarter
        protected void ddlStaffQuarterToReview_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _quarter = 0;

                if (ddlStaffQuarterToReview.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No quarter selected. Select the quarter that you want to review!", this, PanelStaffPerformanceReview);
                    return;
                }
                else
                {
                    _quarter = Convert.ToInt32(ddlStaffQuarterToReview.SelectedValue);
                    //load details for the quarter being reviewd

                    LoadStaffQuarterlyPlannedPerformanceForReview(_quarter);
                    return;
                }
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelStaffPerformanceReview);
                return;
            }
        }
        //display staff quarter details for the selected quarter
        private void LoadStaffQuarterlyPlannedPerformanceForReview(int _quarter)
        {
            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
            string _staffName = _hrClass.getStaffNames(_staffID);
            string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
            string _reviewQuarterHeader = "Review " + _staffName + " Quarter " + _quarter + " Performance for the Year " + _performanceYear;
            lbReviewStaffQuarterHeader.Text = _reviewQuarterHeader;
            HiddenFieldQuarter.Value = _quarter.ToString();

            double _overallAverageRating = 0, _percentageWeight = 0, _averageRating = 0;//for calculating average rating
            //create a data table
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("PerformancePlanID", typeof(int)));
            dt.Columns.Add(new DataColumn("PerformanceObjective", typeof(string)));
            dt.Columns.Add(new DataColumn("KeyPerformanceIndicators", typeof(string)));
            dt.Columns.Add(new DataColumn("PercentageWeight", typeof(float)));
            dt.Columns.Add(new DataColumn("QuarterTarget", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterAchieved", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterKPIRating", typeof(string)));
            dt.Columns.Add(new DataColumn("AverageObjectiveRating", typeof(string)));
            dt.Columns.Add(new DataColumn("QuarterComment", typeof(string)));
            dt.Columns.Add(new DataColumn("NextQuarterActionPlan", typeof(string)));
            DataRow dr;
            foreach (PerformancePlan getPerformancePlan in db.PerformancePlans.Where(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear).OrderBy(p => p.PerformanceObjective))
            {

                dr = dt.NewRow();
                dr[0] = getPerformancePlan.PerformancePlanID;
                dr[1] = getPerformancePlan.PerformanceObjective;
                dr[2] = getPerformancePlan.KeyPerformanceIndicators;
                dr[3] = getPerformancePlan.PercentageWeight;
                if (_hrClass.isNumberValid(getPerformancePlan.PercentageWeight.ToString()) == true)//check if the value entered is numeric
                    _percentageWeight = Convert.ToDouble(getPerformancePlan.PercentageWeight.ToString());

                string _quarterTarget = "", _quarterAchieved = "", _quarterKPIRating = "N/A", _averageObjectiveRating = "N/A", _quarterComment = "", _nextQuarterActionPlan = "";
                if (_quarter == 1)//check if its quarter 1
                {
                    _quarterTarget = getPerformancePlan.Quarter1Target;
                    _quarterAchieved = getPerformancePlan.Quarter1Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter1Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter1AverageRating;
                    _quarterComment = getPerformancePlan.Quarter1Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter1NextQuarterActionPlan;
                }
                else if (_quarter == 2)//check if its quarter 2
                {
                    _quarterTarget = getPerformancePlan.Quarter2Target;
                    _quarterAchieved = getPerformancePlan.Quarter2Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter2Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter2AverageRating;
                    _quarterComment = getPerformancePlan.Quarter2Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter2NextQuarterActionPlan;
                }
                else if (_quarter == 3)//check if its quarter 3
                {
                    _quarterTarget = getPerformancePlan.Quarter3Target;
                    _quarterAchieved = getPerformancePlan.Quarter3Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter3Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter3AverageRating;
                    _quarterComment = getPerformancePlan.Quarter3Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter3NextQuarterActionPlan;
                }
                else if (_quarter == 4)//check if its quarter 4
                {
                    _quarterTarget = getPerformancePlan.Quarter4Target;
                    _quarterAchieved = getPerformancePlan.Quarter4Achieved;
                    _quarterKPIRating = getPerformancePlan.Quarter4Rating;
                    _averageObjectiveRating = getPerformancePlan.Quarter4AverageRating;
                    _quarterComment = getPerformancePlan.Quarter4Comments;
                    _nextQuarterActionPlan = getPerformancePlan.Quarter4NextQuarterActionPlan;
                }
                dr[4] = "<pre>" + _quarterTarget + "</pre>";
                dr[5] = "<pre>" + _quarterAchieved + "</pre>";
                dr[6] = "<pre>" + _quarterKPIRating + "</pre>";
                dr[7] = _averageObjectiveRating;
                if (_hrClass.isNumberValid(_averageObjectiveRating) == true)//check if the value entered is numeric
                    _averageRating = Convert.ToDouble(_averageObjectiveRating);
                dr[8] = _quarterComment;
                dr[9] = _nextQuarterActionPlan;
                dt.Rows.Add(dr);
                //calculate overall average rating
                _overallAverageRating += (_percentageWeight * _averageRating);
            }
            gvStaffQuarterlyPerformanceReview.DataSourceID = null;
            gvStaffQuarterlyPerformanceReview.DataSource = dt;
            gvStaffQuarterlyPerformanceReview.DataBind();
            _overallAverageRating = (_overallAverageRating / 100);

            txtQuarterOverallAverageRating.Text = "";
            txtQuarterAppraisalDate.Text = "";
            txtQuarterAppraiserComments.Text = "";
            //
            txtQuarterOverallAverageRating.Text = _hrClass.ConvertToCurrencyValue((decimal)_overallAverageRating);
            //chek if the quarter has already been appraised
            if (db.QuarterlyPerformancePlanReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter))
            {
                QuarterlyPerformancePlanReview getQuarterlyReview = db.QuarterlyPerformancePlanReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter);
                txtQuarterOverallAverageRating.Text = getQuarterlyReview.OverallAverageRating;
                txtQuarterAppraisalDate.Text = _hrClass.ShortDateDayStart(getQuarterlyReview.AppraisalDate.Value.Date);
                txtQuarterAppraiserComments.Text = getQuarterlyReview.AppraiserComments;
                string _supervisorName = _hrClass.getStaffNames((Guid)getQuarterlyReview.AppraisedByStaffID);
                txtSupervisorName.Text = _supervisorName;
            }

            PanelStaffPerformancePlanQuarteryReview.Visible = true;
            PanelPerformanceObjectiveListing.Visible = false;
            return;
        }

        //close review  staff quarter details
        protected void ImageButtonCloseReviewStaffQuarter_Click(object sender, EventArgs e)
        {
            PanelStaffPerformancePlanQuarteryReview.Visible = false;
            PanelPerformanceObjectiveListing.Visible = true;
            ddlStaffQuarterToReview.SelectedIndex = 0;
            return;
        }
        //save quarte appraisal details
        private void SaveQuartelyPerfomanceReviewAppraisal()
        {
            try
            {
                if (txtQuarterOverallAverageRating.Text.Trim() == "")
                {

                    _hrClass.LoadHRManagerMessageBox(1, "Enter overall average rating!", this, PanelStaffPerformanceReview);
                    txtQuarterOverallAverageRating.Focus();
                    return;
                }
                if (txtHRQuarterAppraisalDate.Text.Trim() == "__/___/____")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter appraisal date!", this, PanelStaffPerformanceReview);
                    txtHRQuarterAppraisalDate.Focus();
                    return;
                }
                else if (txtHRQuarterAppraisalDate.Text.Trim() == "__/___/____" && _hrClass.isDateValid(txtHRQuarterAppraisalDate.Text.Trim()) == false)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Invalid appraisal date entered!", this, PanelStaffPerformanceReview);
                    txtHRQuarterAppraisalDate.Focus();
                    return;
                }
                else
                {
                    //save the appraisal details
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    Guid _appraiserID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    Int16 _quarter = Convert.ToInt16(HiddenFieldQuarter.Value);
                    string _performanceYear = ddlAnnualPerformancePlanYear.SelectedValue;
                    if (db.QuarterlyPerformancePlanReviews.Any(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter))
                    {
                        //update existing quarter review
                        //add new quarter review
                        QuarterlyPerformancePlanReview updateQuarterlyReview = db.QuarterlyPerformancePlanReviews.Single(p => p.StaffID == _staffID && p.PerformanceYear == _performanceYear && p.PerformanceQuarter == _quarter);
                        updateQuarterlyReview.StaffID = _staffID;
                        updateQuarterlyReview.PerformanceYear = _performanceYear;
                        updateQuarterlyReview.PerformanceQuarter = _quarter;
                        updateQuarterlyReview.OverallAverageRating = txtQuarterOverallAverageRating.Text.Trim();
                        //updateQuarterlyReview.AppraisedByStaffID = _appraiserID;
                        //updateQuarterlyReview.AppraisalDate = Convert.ToDateTime(txtQuarterAppraisalDate.Text.Trim()).Date;
                       //updateQuarterlyReview.AppraiserComments = txtQuarterAppraiserComments.Text.Trim();
                        updateQuarterlyReview.HRAppraiserStaffID = _appraiserID;
                        updateQuarterlyReview.HRAppraisalDate= Convert.ToDateTime(txtHRQuarterAppraisalDate.Text.Trim()).Date;
                        updateQuarterlyReview.HRComments = txtHRQuarterAppraiserComments.Text.Trim();
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "Staff appraisal details have been saved! ", this, PanelStaffPerformanceReview);
                        return;
                    }
                    else
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Appraisal details could not be saved because the supervisor has not reviewd the performance! ", this, PanelStaffPerformanceReview);
                        return;
                    }

                   
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffPerformanceReview);
                return;
            }
        }
        //save quarter appraisal details
        protected void lnkBtnSaveQuarterAppraisalDetails_Click(object sender, EventArgs e)
        {
            SaveQuartelyPerfomanceReviewAppraisal();
            return;
        }
    }
}