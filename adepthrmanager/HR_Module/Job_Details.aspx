﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Job_Details.aspx.cs" Inherits="AdeptHRManager.HR_Module.Job_Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelJobDetails" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelJobDetails" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="panel-details">
                    <asp:Panel ID="PanelAddJobDetails" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    Joining Date:<span class="errorMessage">*</span>
                                </td>
                                <td style="vertical-align: bottom;">
                                    <asp:TextBox ID="txtJoinDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonJoinDate" ToolTip="Pick join date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtJoinDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtJoinDate" PopupButtonID="ImageButtonJoinDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtJoinDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtJoinDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtJoinDate_MaskedEditValidator" runat="server" ControlExtender="txtJoinDate_MaskedEditExtender"
                                        ControlToValidate="txtJoinDate" CssClass="errorMessage" ErrorMessage="txtJoinDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                    <br />
                                    <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2015"></asp:Label>
                                </td>
                                <td>
                                    Job Title:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobTitle" runat="server">
                                        <asp:ListItem Value="0">-Select Job TItle-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Grade: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobGrade" runat="server">
                                        <asp:ListItem Value="0">-Select Job Grade-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Department: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDepartment" runat="server">
                                        <asp:ListItem Value="0">-Select Department-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Geographical Location:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLocationPosted" runat="server">
                                        <asp:ListItem Value="0">-Select Location Posted-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Duty Station:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPostedAt" runat="server">
                                        <asp:ListItem Value="0">-Select Posted At-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contract Type: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlContractType" runat="server">
                                        <asp:ListItem Value="0">-Select Contract Type-</asp:ListItem>
                                        <asp:ListItem Value="1">Full-Time</asp:ListItem>
                                        <asp:ListItem Value="2">Contract</asp:ListItem>
                                        <asp:ListItem Value="3">Temporary</asp:ListItem>
                                        <asp:ListItem Value="">Intern</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                     Type of Work: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTypeOfWork" runat="server">
                                        <asp:ListItem Value="0">-Select Type of Work-</asp:ListItem>
                                        <asp:ListItem Value="1">Operational</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>                          
                            <tr>
                                <td>
                                    Job Category: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <%--  <asp:DropDownList ID="ddlJobCategory" runat="server">
                                    <asp:ListItem Value="0">-Select Job Category-</asp:ListItem>
                                    <asp:ListItem Value="1">General</asp:ListItem>
                                    <asp:ListItem Value="2">Professional</asp:ListItem>
                                </asp:DropDownList>--%>
                                    <asp:RadioButtonList ID="rblProfessional" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Value="False">General Support</asp:ListItem>
                                        <asp:ListItem Value="True">Professional</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Supervisor: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlReportsTo" runat="server">
                                        <asp:ListItem Value="0">-Select Reports To-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Under Probation?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblStaffUnderProbation" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Value="True">Yes</asp:ListItem>
                                        <asp:ListItem Value="False">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Probation End Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProbationEndDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonProbationEndDate" ToolTip="Pick probation end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtProbationEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtProbationEndDate" PopupButtonID="ImageButtonProbationEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtProbationEndDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtProbationEndDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtProbationEndDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtProbationEndDate_MaskedEditExtender" ControlToValidate="txtProbationEndDate"
                                        CssClass="errorMessage" ErrorMessage="txtProbationEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid probation end date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr>
							<td>
                                    National / International?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblInternationalOrNational" RepeatDirection="Horizontal"
                                        runat="server">
                                        <asp:ListItem Value="True">National</asp:ListItem>
                                        <asp:ListItem Value="False">International</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Visa Expiry Date</td>
                                <td>
                                    <asp:TextBox ID="txtVisaExpiryDate" runat="server" Width="200px"></asp:TextBox>
                                     <asp:ImageButton ID="ImageButtonVisaExpiryDate" ToolTip="Pick probation end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtVisaExpiryDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtVisaExpiryDate" PopupButtonID="ImageButtonVisaExpiryDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtVisaExpiryDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtVisaExpiryDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtVisaExpiryDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtVisaExpiryDate_MaskedEditExtender" ControlToValidate="txtVisaExpiryDate"
                                        CssClass="errorMessage" ErrorMessage="txtVisaExpiryDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid probation end date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Job Role (Summary)<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtJobRole" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lbRequired" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                     <asp:TextBox ID="txtBadgeNumber" runat="server" visible="false" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="linkBtn" style="text-align: right;">
                                        <asp:LinkButton ID="lnkBtnSaveJobDetails" OnClick="lnkBtnSaveJobDetails_Click" ToolTip="Save job details"
                                            runat="server">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Job Details</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="fixed-table">
                        <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter contract dates</legend>
                           <table width="100%">                          
                            <tr>
							<td>
                                    Start Date
                                </td>
                                <td>
                                    <asp:TextBox ID="txtStartDate" runat="server" Width="190px"></asp:TextBox>
                                                                        <asp:ImageButton ID="ImageButtonStartDate" ToolTip="Pick start end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtStartDate" PopupButtonID="ImageButtonStartDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtStartDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtStartDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtStartDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtStartDate_MaskedEditExtender" ControlToValidate="txtStartDate"
                                        CssClass="errorMessage" ErrorMessage="txtStartDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid start date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                <td>
                                    End Date:
                                </td>
								
                                <td>
                                    <asp:TextBox ID="txtEndDate" runat="server" Width="190px"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonEndDate" ToolTip="Pick end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtEndDate" PopupButtonID="ImageButtonEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtEndDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEndDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtEndDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtEndDate_MaskedEditExtender" ControlToValidate="txtEndDate"
                                        CssClass="errorMessage" ErrorMessage="txtEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid end date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>								
                                <td align="right">
                                    <asp:Button ID="btnAddContractDates" OnClick="btnAddContractDates_Click" runat="server" Text="Save Contract Dates" />
                                     <asp:HiddenField ID="HiddenFieldContractDatesID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvContractDates" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10" OnRowCommand="gvContractDates_RowCommand" EmptyDataText="No Bank Details!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobContractDateID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="StartContractDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Start Date" />
                                          <asp:BoundField DataField="EndContractDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="End Date" />                                   
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditContractDates" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteContractDates" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                         </div>
                        <div class="fixed-table">
                         <fieldset class="fieldsetDetails">
                        <legend class="legendDetails">Enter work permit number</legend>
                           <table width="100%">
                           
                            <tr>
							<td>
                                   Work Permit Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWorkPermitNumber" runat="server" Width="190px"></asp:TextBox>
                                </td>
                                <td>
                                    Expiry Date:
                                </td>
								
                                <td>
                                    <asp:TextBox ID="txtExpiryDate" runat="server" Width="190px"></asp:TextBox>                                    
                                    <asp:ImageButton ID="ImageButtonExpiryDate" ToolTip="Pick expiry date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtExpiryDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtExpiryDate" PopupButtonID="ImageButtonExpiryDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtExpiryDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtExpiryDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtExpiryDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtExpiryDate_MaskedEditExtender" ControlToValidate="txtExpiryDate"
                                        CssClass="errorMessage" ErrorMessage="txtExpiryDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid expiry date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>								
                                <td align="right" valign="top">
                                    <asp:Button ID="btnAddWorkPermitNumber" OnClick="btnAddWorkPermitNumber_Click" runat="server" Text="Save Work Permit Number" />
                                    <asp:HiddenField ID="HiddenFieldWorkPermitNumberID" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:GridView ID="gvWorkPermitNumber" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" 
                                        AllowSorting="True" PageSize="10" OnRowCommand="gvWorkPermitNumber_RowCommand" EmptyDataText="No Work Permit Number!">
                                        <Columns>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                   <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobWorkPermitID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="JobWorkPermitNumber" HeaderText="Work Permit Number" />
                                          <asp:BoundField DataField="JobWorkPermitExpiryDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Expiry Date" />                                   
                                            
                                            <asp:ButtonField HeaderText="Edit" CommandName="EditWorkPermitNumber" ItemStyle-Width="4px"
                                                Text="Edit" />
                                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteWorkPermitNumber" ItemStyle-Width="4px"
                                                Text="Delete" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                        </div>                                                 

                    </asp:Panel>
                </div>
                <uc:ConfirmMessageBox ID="ucConfirmContractDate" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmWorkPermit" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
