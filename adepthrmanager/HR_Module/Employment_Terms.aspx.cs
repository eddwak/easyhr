﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;
using System.Data;

namespace AdeptHRManager.HR_Module
{
    public partial class Employment_Terms : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //_hrClass.GetListingItems(ddlEmploymentTermsTypeOfEmployment, 17000, "Type of Employment");//display employment types(17000) available
                GetMedicalBenefits();//populate mediical scheme dropdown
                _hrClass.GetListingItems(ddlBasicPayCurrency, 14000, "Currency");//display curiencies(14000) available
                //_hrClass.GetListingItems(ddlHousingBenefitCurrency, 14000, "Currency");//display curiencies(14000) available
                _hrClass.GetListingItems(ddlAllowanceCurrency, 14000, "Currency");//display curiencies(14000) available
                _hrClass.GetListingItems(ddlEmploymentTermsPerkName, 18000, "Allowance");//display perkd/allowances(18000) available
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
            //add delete record event to delete an allowance
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffEmploymentTerms(_staffID);//load family details
                    return;
                }
            }
            catch { }
        }
        //load employment terms details
        private void DisplayStaffEmploymentTerms(Guid _staffID)
        {
            LoadStaffEmploymentTerms(_staffID);//load the staff's employment term details
            //DisplaySpouseForMedicalBenefit();//load staff's spouse medical scheme beneficiaries
            //DisplayChildForMedicalBenefit();//load staff;s children medical scheme beneficiaries
            DisplayEmploymentPerkDetails(_staffID);//display employment term perk/allowances associated with the staff
            ClearEmploymentTermsPerkDetailsEntryControls();
            DisplayStaffDependantsForMedicalBenefit(_staffID);//display staff dependants
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffEmploymentTerms(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffEmploymentTerms(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelEmploymentTerms);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentTerms);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffEmploymentTerms(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffEmploymentTerms(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelEmploymentTerms);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentTerms);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffEmploymentTerms(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffEmploymentTerms(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelEmploymentTerms);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentTerms);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffEmploymentTerms(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffEmploymentTerms(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelEmploymentTerms);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentTerms);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //populate medical schemes dropdown
        public void GetMedicalBenefits()
        {
            try
            {
                //ddlEmploymentTermsMedicalScheme.Items.Clear();
                //object _getMedicalSchemes;
                //_getMedicalSchemes = db.MedSchemes.Select(p => p).OrderBy(p => p.medscheme_vName);
                //ddlEmploymentTermsMedicalScheme.DataSource = _getMedicalSchemes;
                //ddlEmploymentTermsMedicalScheme.Items.Insert(0, new ListItem("-Select Medical Scheme-"));
                //ddlEmploymentTermsMedicalScheme.DataTextField = "medscheme_vName";
                //ddlEmploymentTermsMedicalScheme.DataValueField = "medscheme_siMedSchemeID";
                //ddlEmploymentTermsMedicalScheme.AppendDataBoundItems = true;
                //ddlEmploymentTermsMedicalScheme.DataBind();

                cblEmploymentTermsMedicalScheme.Items.Clear();
                object _getMedicalSchemes;
                _getMedicalSchemes = db.MedSchemes.Select(p => p).OrderBy(p => p.medscheme_vName);
                cblEmploymentTermsMedicalScheme.DataSource = _getMedicalSchemes;
                //cblEmploymentTermsMedicalScheme.Items.Insert(0, new ListItem("-Select Medical Scheme-"));
                cblEmploymentTermsMedicalScheme.DataTextField = "medscheme_vName";
                cblEmploymentTermsMedicalScheme.DataValueField = "medscheme_siMedSchemeID";
                cblEmploymentTermsMedicalScheme.AppendDataBoundItems = true;
                cblEmploymentTermsMedicalScheme.DataBind();
                return;
            }
            catch { }
        }
        //validate add employment terms controls
        private string ValidateAddEmploymentTermsControls()
        {

            if (HiddenFieldStaffID.Value == "")
                return "Employment terms save failed. Enter staff's personal information or search for the staff before adding his/her employment terms!";
            else if (txtEmploymentTermsBasicPay.Text.Trim() == "")
                return "Enter employee's basic pay!";
            else if (txtEmploymentTermsBasicPay.Text.Trim() != "" && _hrClass.IsCurrencyValid(txtEmploymentTermsBasicPay.Text.Trim()) == false)
                return "Enter a valid numeric basic pay value!";
            else if (ddlBasicPayCurrency.SelectedIndex == 0)
                return "Select basic pay currency!";          

            else if (_hrClass.isDateValid(txtRetirementDate.Text.Trim()) == false)
            {
               return "Invalid retirement date entered!";
            }

            //else if (txtEmploymentTermsLiquidatedDamages.Text != "" && _hrClass.IsCurrencyValid(txtEmploymentTermsLiquidatedDamages.Text.Trim()) == false)
            //    return "Enter a valid numeric liquidated damage value!";
            else return "";
        }
        //method for saving staff employment terms
        private void SaveEmploymentTerms()
        {
            try
            {
                string _error = ValidateAddEmploymentTermsControls();//
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelEmploymentTerms);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    //save employment terms
                    if (HiddenFieldEmploymentTermsID.Value == "")
                    {
                        EmpTerm newEmploymentTerms = new EmpTerm();
                        newEmploymentTerms.empterms_uiStaffID = _staffID;
                        if (txtEmploymentTermsBasicPay.Text.Trim() != "")
                        {
                            newEmploymentTerms.empterms_mBasicPay = Convert.ToDecimal(txtEmploymentTermsBasicPay.Text.Trim());
                        }
                        newEmploymentTerms.empterms_iBasicPayCurrencyID = Convert.ToInt32(ddlBasicPayCurrency.SelectedValue);
                        newEmploymentTerms.empterms_vMedicalSchemeID = GetSelectedMedicalSchemes();
                        newEmploymentTerms.empterms_RetirementBenefits = txtRetirementBenefits.Text.Trim();
                        newEmploymentTerms.empterms_RetirementDate = Convert.ToDateTime(txtRetirementDate.Text.Trim()).Date;
                        db.EmpTerms.InsertOnSubmit(newEmploymentTerms);
                        db.SubmitChanges();
                        HiddenFieldEmploymentTermsID.Value = newEmploymentTerms.empterms_iEmpTermsID.ToString();
                        _hrClass.LoadHRManagerMessageBox(2, "Staff employment terms have been successfully saved!", this, PanelEmploymentTerms);
                        return;
                    }
                    else
                    {
                        int _employmentTermID = Convert.ToInt32(HiddenFieldEmploymentTermsID.Value);
                        //update employment term details
                        EmpTerm updateEmploymentTerms = db.EmpTerms.Single(p => p.empterms_iEmpTermsID == _employmentTermID);

                        if (txtEmploymentTermsBasicPay.Text.Trim() != "")
                            updateEmploymentTerms.empterms_mBasicPay = Convert.ToDecimal(txtEmploymentTermsBasicPay.Text.Trim());
                        else
                            updateEmploymentTerms.empterms_mBasicPay = null;
                        updateEmploymentTerms.empterms_iBasicPayCurrencyID = Convert.ToInt32(ddlBasicPayCurrency.SelectedValue);
                        updateEmploymentTerms.empterms_vMedicalSchemeID = GetSelectedMedicalSchemes();
                        updateEmploymentTerms.empterms_RetirementBenefits = txtRetirementBenefits.Text.Trim();
                        updateEmploymentTerms.empterms_RetirementDate = Convert.ToDateTime(txtRetirementDate.Text.Trim()).Date;
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "Staff employment terms have been successfully updated!", this, PanelEmploymentTerms);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelEmploymentTerms);
                return;
            }
        }
        //get selected isnsurance cover in the check box list
        private string GetSelectedMedicalSchemes()
        {
            string _selectedMedicalSchemeIDs = "";
            foreach (ListItem _module in cblEmploymentTermsMedicalScheme.Items)
            {
                if (_module.Selected)
                    _selectedMedicalSchemeIDs += _module.Value.ToString() + ",";
            }
            return _selectedMedicalSchemeIDs;
        }
        //save/update staff's employment terms
        protected void btnSaveEmployementTerms_Click(object sender, EventArgs e)
        {
            SaveEmploymentTerms(); return;
        }
        //load staff employment terms
        private void LoadStaffEmploymentTerms(Guid _staffID)
        {
            try
            {
                //check if the staff has got any employment terms details that are saved
                if (db.EmpTerms.Any(p => p.empterms_uiStaffID == _staffID))
                {
                    Decimal _basicPay = 0, _housingBenefit = 0, _consolidatedPay = 0;
                    //get the staff
                    EmpTerm getEmploymentTerms = db.EmpTerms.FirstOrDefault(p => p.empterms_uiStaffID == _staffID);
                    HiddenFieldEmploymentTermsID.Value = getEmploymentTerms.empterms_iEmpTermsID.ToString();
                    if (getEmploymentTerms.empterms_uiStaffID != null)
                    {
                        txtRetirementBenefits.Text = getEmploymentTerms.empterms_RetirementBenefits;
                        if (getEmploymentTerms.empterms_RetirementDate!= null)
                            txtRetirementDate.Text = _hrClass.ShortDateDayStart(getEmploymentTerms.empterms_RetirementDate.ToString());
                        else txtRetirementDate.Text = "";
                        
                        _basicPay = (decimal)getEmploymentTerms.empterms_mBasicPay;
                        _housingBenefit = _basicPay * (decimal)0.2;//housing bnefit is 20% of basic pay
                        _consolidatedPay = _basicPay + _housingBenefit;


                        txtEmploymentTermsBasicPay.Text = _hrClass.ConvertToCurrencyValue(_basicPay).Replace(",", "");
                        txtHousingBenefit.Text = _hrClass.ConvertToCurrencyValue(_housingBenefit).Replace(",", "");
                        txtConsolidatedPay.Text = _hrClass.ConvertToCurrencyValue(_consolidatedPay).Replace(",", "");
                        
                    }
                    else
                    {
                        txtEmploymentTermsBasicPay.Text = "";
                        txtHousingBenefit.Text = "";
                        txtConsolidatedPay.Text = "";
                        txtRetirementBenefits.Text = "";
                        txtRetirementDate.Text = "";
                    }
                    if (getEmploymentTerms.empterms_iBasicPayCurrencyID != null)
                        ddlBasicPayCurrency.SelectedValue = getEmploymentTerms.empterms_iBasicPayCurrencyID.ToString();
                    else ddlBasicPayCurrency.SelectedIndex = 0;

                    string _medicalSchemeIDs = getEmploymentTerms.empterms_vMedicalSchemeID;
                    var _listMedicalSchemeID = _medicalSchemeIDs.Split(',').Select(p => p).ToArray();//get array for the assigned modules
                    foreach (ListItem _module in cblEmploymentTermsMedicalScheme.Items)
                    {
                        foreach (string _assignedModule in _listMedicalSchemeID)
                        {
                            if (_module.Value == _assignedModule)//check if the select medical 
                                _module.Selected = true;
                        }
                    }
                    return;
                }
                else
                {
                    txtEmploymentTermsBasicPay.Text = "";
                    txtHousingBenefit.Text = "";
                    ddlBasicPayCurrency.SelectedIndex = 0;
                    txtConsolidatedPay.Text = "";
                    cblEmploymentTermsMedicalScheme.SelectedIndex = -1;
                    txtRetirementBenefits.Text = "";
                    txtRetirementDate.Text = "";
                    return;
                }
            }
            catch { }
        }
        //validate add employment allowance controls
        private string ValidateAddEmploymentAllowanceControls()
        {
              if (HiddenFieldStaffID.Value == "")
                return "Employment allowance save failed. Enter staff's personal information or search for the staff before adding his/her employment terms!";
            else if (ddlEmploymentTermsPerkName.SelectedIndex == 0)
                return "Select allowance!";
            else if (txtEmploymentTermsPerkAmount.Text.Trim() == "")
                return "Enter allowance amount!";
            else if (_hrClass.IsCurrencyValid(txtEmploymentTermsPerkAmount.Text.Trim()) == false)
                return "Enter a valid numeric allowance amount!";
            //else if (txtEmploymentTermsEffectiveFrom.Text.Trim() == "")
            //    return "Select perk effective from date!";
            //else if (_hrClass.isDateValid(txtEmploymentTermsEffectiveFrom.Text.Trim()) == false)
            //    return "Invalid perk effective date entered!";
            //else if (Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text.Trim()).Date < Convert.ToDateTime(txtEmploymentTermsDateOfJoin.Text.Trim()))
            //    return "Date when  perk is effective from cannot be before employee's join date!";
            //else if (ddlEmploymentTermsPerkName.SelectedIndex != 0 && txtEmploymentTermsEffectiveUntil.Text.Trim() == "")
            //{
            //    _error = "Select perk effective until date!";
            //    btnEmploymentTermsEffectiveUntil.Focus();
            //}
            //else if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtEmploymentTermsEffectiveUntil.Text.Trim()) == false)
            //    return "Invalid perk effective until date entered!";
            //else if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____" && Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text.Trim()).Date < Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text.Trim()).Date)
            //    return "Date when perk is effective until cannot be before date when perk is effective from!";
            else if (txtEmploymentTermsPerkAmount.Text.Trim() != "" && ddlEmploymentTermsPerkName.SelectedIndex == 0)
                return "Select allowance for the amount entered!";
            else if (ddlAllowanceCurrency.SelectedIndex == 0)
                return "Select allowance currency!";
            else return "";
        }
        //method for saving employment perk
        private void SaveEmploymentAllowance()
        {
            try
            {
                string _error = ValidateAddEmploymentAllowanceControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelEmploymentTerms);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    int _allowanceID = Convert.ToInt32(ddlEmploymentTermsPerkName.SelectedValue);
                    int _currenyID = Convert.ToInt32(ddlAllowanceCurrency.SelectedValue);
                    //Check if there is a different currency from the one the employeee gets paid on
                    string newCurrency = ddlAllowanceCurrency.SelectedValue;
                    EmpTermsPerkDetail getEmpTermsPerkDetail = db.EmpTermsPerkDetails.FirstOrDefault(p => p.emptermsperkdetail_uiStaffID == _staffID);
                    string OldCurrency = Convert.ToString(getEmpTermsPerkDetail.emptermsperkdetail_iCurrencyID);
                
                    if (OldCurrency != newCurrency)
                    {
                        _hrClass.LoadHRManagerMessageBox(2, "You cannot enter a different currency from the one already used to pay the employee's allowances!", this, PanelEmploymentTerms);
                        return;
                    }
                    else
                    {                                                             

                    if (btnEmploymentTermsAddPerk.Text.ToLower() == "save allowance")
                    {
                        //check if the selected perk is selected against the staff
                        if (db.EmpTermsPerkDetails.Any(p => p.emptermsperkdetail_uiStaffID == _staffID && p.emptermsperkdetail_iPerkID == _allowanceID))
                        {

                            _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected allowance is already saved against the staff!", this, PanelEmploymentTerms);
                            return;
                        }
                        else
                        {
                            //save perk details
                            AddEmployeePerkDetail(_staffID, _allowanceID, _currenyID);
                            _hrClass.LoadHRManagerMessageBox(2, "Employment allowance details have been successfully saved!", this, PanelEmploymentTerms);
                            return;
                        }
                    }
                    else//update perk details
                    {
                        int _perkDetailID = Convert.ToInt32(HiddenFieldEmploymentTermsPerksDetailID.Value);

                        //check if the selected perk is selected against the staff
                        if (db.EmpTermsPerkDetails.Any(p => p.emptermsperkdetail_uiStaffID == _staffID && p.emptermsperkdetail_iPerkID == _allowanceID && p.emptermsperkdetail_iEmpTermsPerkDetailID != _perkDetailID))
                        {

                            _hrClass.LoadHRManagerMessageBox(2, "Update failed. The selected allowance is already saved against the staff!", this, PanelEmploymentTerms);
                            return;
                        }
                        else
                        {
                            //update the staff perk details
                            UpdateEmployeePerkDetail(_perkDetailID, _allowanceID, _currenyID);//
                            _hrClass.LoadHRManagerMessageBox(2, "Employment term allowance details have been successfully updated!", this, PanelEmploymentTerms);
                            return;
                        }
                    }
                }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelEmploymentTerms);
                return;
            }
        }
        //method for adding perk/allowance detail
        private void AddEmployeePerkDetail(Guid _staffID, int _allowanceID, int _currencyID)
        {
            //create perk detail
            EmpTermsPerkDetail newEmployeeTemsPerkDetail = new EmpTermsPerkDetail();
            newEmployeeTemsPerkDetail.emptermsperkdetail_uiStaffID = _staffID;
            newEmployeeTemsPerkDetail.emptermsperkdetail_iPerkID = _allowanceID;
            newEmployeeTemsPerkDetail.emptermsperkdetail_mPerkAmount = Convert.ToDecimal(txtEmploymentTermsPerkAmount.Text.Trim());
            newEmployeeTemsPerkDetail.emptermsperkdetail_iCurrencyID = _currencyID;
            //newEmployeeTemsPerkDetail.emptermsperkdetail_dtPerkDateFrom = Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text);
            //if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____")
            //{
            //    newEmployeeTemsPerkDetail.emptermsperkdetail_dtPerkDateUntil = Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text);
            //}
            db.EmpTermsPerkDetails.InsertOnSubmit(newEmployeeTemsPerkDetail);
            db.SubmitChanges();
            HiddenFieldEmploymentTermsPerksDetailID.Value = newEmployeeTemsPerkDetail.emptermsperkdetail_iEmpTermsPerkDetailID.ToString();
            //display employment terms perk details associated with the staff
            DisplayEmploymentPerkDetails(_staffID);
        }
        //methhod for updating a perk/allowance detail
        private void UpdateEmployeePerkDetail(int _perkDetailID, int _allowanceID, int _currencyID)
        {
            //alter perk detail
            EmpTermsPerkDetail updatePerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _perkDetailID);
            updatePerkDetail.emptermsperkdetail_iPerkID = _allowanceID;
            updatePerkDetail.emptermsperkdetail_mPerkAmount = Convert.ToDecimal(txtEmploymentTermsPerkAmount.Text.Trim());
            updatePerkDetail.emptermsperkdetail_iCurrencyID = _currencyID;
            //updatePerkDetail.emptermsperkdetail_dtPerkDateFrom = Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text).Date;
            //if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____")
            //    updatePerkDetail.emptermsperkdetail_dtPerkDateUntil = Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text);
            //else updatePerkDetail.emptermsperkdetail_dtPerkDateUntil = null;
            db.SubmitChanges();
            //refresh employment perk details]
            DisplayEmploymentPerkDetails((Guid)updatePerkDetail.emptermsperkdetail_uiStaffID);
            //clear the netry controls
            ClearEmploymentTermsPerkDetailsEntryControls();
            return;
        }
        //save employment allowance details
        protected void btnEmploymentTermsAddPerk_Click(object sender, EventArgs e)
        {
            SaveEmploymentAllowance(); return;//
        }
        //display employment perk details
        private void DisplayEmploymentPerkDetails(Guid _staffID)
        {
            try
            {
                object _display;
                _display = db.PROC_StaffPerks(_staffID);
                gvEmploymentTermsPerksDetails.DataSourceID = null;
                gvEmploymentTermsPerksDetails.DataSource = _display;
                gvEmploymentTermsPerksDetails.DataBind();
                return;
            }
            catch { }
        }
        //load employee's employment terms perks  details for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvEmploymentTermsPerksDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditPerkDetail") == 0 || e.CommandName.CompareTo("DeletePerkDetail") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFEmploymentTermPerkDetailID = (HiddenField)gvEmploymentTermsPerksDetails.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditPerkDetail") == 0)//check if we are editing
                    {
                        LoadEmploymentTermPerkDetailsControlsForEdit(Convert.ToInt32(_HFEmploymentTermPerkDetailID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeletePerkDetail") == 0)//check if we are deleting
                    {
                        HiddenFieldEmploymentTermsPerksDetailID.Value = _HFEmploymentTermPerkDetailID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, "Are you sure you what to delete the employee's allowance detail?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelEmploymentTerms);
                return;
            }
        }
        //clear employment terms perk details entry controls()
        private void ClearEmploymentTermsPerkDetailsEntryControls()
        {
            ddlEmploymentTermsPerkName.SelectedIndex = 0;
            txtEmploymentTermsPerkAmount.Text = "";
            ddlAllowanceCurrency.SelectedIndex = 0;
            //txtEmploymentTermsEffectiveFrom.Text = "";
            //txtEmploymentTermsEffectiveUntil.Text = "";
            btnEmploymentTermsAddPerk.Text = "Save Allowance";
            HiddenFieldEmploymentTermsPerksDetailID.Value = "";
            return;
        }
        //load employment term perk details for edit
        private void LoadEmploymentTermPerkDetailsControlsForEdit(int _employmentTermPerkDetailID)
        {
            HiddenFieldEmploymentTermsPerksDetailID.Value = _employmentTermPerkDetailID.ToString();
            EmpTermsPerkDetail getPerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _employmentTermPerkDetailID);
            ddlEmploymentTermsPerkName.SelectedValue = getPerkDetail.emptermsperkdetail_iPerkID.ToString();
            txtEmploymentTermsPerkAmount.Text = _hrClass.ConvertToCurrencyValue((decimal)getPerkDetail.emptermsperkdetail_mPerkAmount);
            ddlAllowanceCurrency.SelectedValue = getPerkDetail.emptermsperkdetail_iCurrencyID.ToString();
            //txtEmploymentTermsEffectiveFrom.Text = _hrClass.ShortDateDayStart(getPerkDetail.emptermsperkdetail_dtPerkDateFrom.ToString());
            //if (getPerkDetail.emptermsperkdetail_dtPerkDateUntil != null)
            //{
            //    txtEmploymentTermsEffectiveUntil.Text = _hrClass.ShortDateDayStart(getPerkDetail.emptermsperkdetail_dtPerkDateUntil.ToString());
            //}
            //else txtEmploymentTermsEffectiveUntil.Text = "";
            btnEmploymentTermsAddPerk.Text = "Update Allowance";
            return;
        }
        //method for deleting an employment term perk detail
        private void DeleteEmploymentTermPerkDetails()
        {
            try
            {
                int _perkDetailID = Convert.ToInt32(HiddenFieldEmploymentTermsPerksDetailID.Value);//get employment term perk detail id
                //delete emploment term perk detail
                EmpTermsPerkDetail deletePerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _perkDetailID);
                db.EmpTermsPerkDetails.DeleteOnSubmit(deletePerkDetail);
                db.SubmitChanges();
                //refresh employee's perk details after delete is done
                DisplayEmploymentPerkDetails((Guid)deletePerkDetail.emptermsperkdetail_uiStaffID);
                _hrClass.LoadHRManagerMessageBox(2, "Employee allowance detail has been deleted!", this, PanelEmploymentTerms);
                ClearEmploymentTermsPerkDetailsEntryControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelEmploymentTerms);
                return;
            }
        }
        //delete the allowance details
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteEmploymentTermPerkDetails();
            return;
        }
        ////display employee's spouse to benefit from the employee's medical scheme
        //private void DisplaySpouseForMedicalBenefit()
        //{
        //    try
        //    {
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        //check if the staf has got any spouse details
        //        if (db.PROC_StaffSpouse(_staffID).Any())
        //        {
        //            PanelSpouseToBenefit.Visible = true;

        //            DataTable dt = new DataTable();
        //            dt.Columns.Add(new DataColumn("SNO", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ID", typeof(int)));
        //            dt.Columns.Add(new DataColumn("SpouseName", typeof(string)));
        //            dt.Columns.Add(new DataColumn("SpouseDOB", typeof(string)));
        //            dt.Columns.Add(new DataColumn("PhoneNumber", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Checked", typeof(bool)));

        //            DataRow dr;
        //            int countRows = 1;//count number of rows
        //            foreach (PROC_StaffSpouseResult getSpouse in db.PROC_StaffSpouse(_staffID))
        //            {
        //                dr = dt.NewRow();
        //                dr[0] = countRows;
        //                dr[1] = getSpouse.spouse_iSpouseID;
        //                dr[2] = getSpouse.spouse_vSpouseName;
        //                if (getSpouse.spouse_dtDOB != null || getSpouse.spouse_dtDOB.ToString() != "")
        //                {
        //                    dr[3] = _hrClass.ShortDateDayStart(getSpouse.spouse_dtDOB.ToString());
        //                }
        //                dr[4] = getSpouse.spouse_vPhoneNumber;
        //                dr[5] = getSpouse.spouse_bIsMedicalCovered;

        //                dt.Rows.Add(dr);
        //                countRows++;
        //            }
        //            gvSpouseToBenefit.DataSource = dt;
        //            gvSpouseToBenefit.DataBind();
        //            return;
        //        }
        //        else
        //        {
        //            PanelSpouseToBenefit.Visible = false;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        //save spouse to benefit from medical schemebenefits
        //private void SaveSpouseToBenefitFromMedicalSchemeBenefits()
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvSpouseToBenefit))//check if there is  any spouse selected to be assigned medical benefits
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Save Failed. There is no spouse selected. Select employee's spouse to be enlisted as a medical benefits beneficiary!", this, PanelEmploymentTerms);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvSpouseToBenefit.Rows)
        //            {

        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            //if (x > 1)
        //            //{
        //            //    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one employee spouse!.<br>Select only one spouse to assign to the medical scheme benefits!", this, PanelEmploymentTerms);
        //            //    return;
        //            //}
        //            foreach (GridViewRow _grv in gvSpouseToBenefit.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;

        //                if (_cb.Checked)
        //                {
        //                    HiddenField _HFSpouseID = (HiddenField)_grv.FindControl("HFID");
        //                    int SpouseID = Convert.ToInt32(_HFSpouseID.Value);
        //                    Session["SpouseToBenefitID"] = SpouseID.ToString();

        //                    if (db.Spouses.Where(p => p.spouse_iSpouseID == SpouseID && p.spouse_bIsMedicalCovered == true).Count() == 0)//check if the spouse selected is assigned to the medical scheme
        //                    {
        //                        //assign medical scheme benefit to the spouse selected
        //                        Spouse assignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == SpouseID);
        //                        assignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = true;
        //                    }
        //                }
        //                else
        //                {
        //                    int SpouseID = Convert.ToInt32(Session["SpouseToBenefitID"]);
        //                    foreach (Spouse deAssignSpouse in db.Spouses.Where(p => p.spouse_iSpouseID != SpouseID))//deasign medical scheme for the unselected employee's spouse
        //                    {
        //                        Spouse deAssignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == deAssignSpouse.spouse_iSpouseID);
        //                        deAssignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = false;
        //                    }
        //                }
        //            }
        //            db.SubmitChanges();
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected spouse has been enlisted as medical beneficiary!", this, PanelEmploymentTerms);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelEmploymentTerms);
        //        return;
        //    }
        //}

        //display employee's dependant to benefit from the employee's medical scheme
        private void DisplayStaffDependantsForMedicalBenefit(Guid _staffID)
        {
            try
            {
                //check if the staf has got any spouse details
                if (db.PROC_StaffAllDependants(_staffID).Any())
                {
                    PanelStaffDependantToBenefit.Visible = true;
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("SNO", typeof(string)));
                    dt.Columns.Add(new DataColumn("ID", typeof(int)));
                    dt.Columns.Add(new DataColumn("spouse_vFirstName", typeof(string)));
                    dt.Columns.Add(new DataColumn("spouse_vSurname", typeof(string)));
                    dt.Columns.Add(new DataColumn("DependantDOB", typeof(string)));
                    dt.Columns.Add(new DataColumn("Relationship", typeof(string)));
                    dt.Columns.Add(new DataColumn("Checked", typeof(bool)));
                    dt.Columns.Add(new DataColumn("IsSpouse", typeof(int)));
                    dt.Columns.Add(new DataColumn("IsChild", typeof(int)));
                    dt.Columns.Add(new DataColumn("IsOtherDependant", typeof(int)));

                    DataRow dr;
                    int countRows = 1;//count number of rows
                    foreach (PROC_StaffAllDependantsResult getDependant in db.PROC_StaffAllDependants(_staffID).OrderBy(p => p.DependantName))
                    {
                        dr = dt.NewRow();
                        dr[0] = countRows + ".";
                        dr[1] = getDependant.DependantID;
                        dr[2] = getDependant.spouse_vFirstName;
                        dr[3] = getDependant.spouse_vSurname;
                        if (getDependant.DependantDOB != null || getDependant.DependantDOB.ToString() != "")
                        {
                            dr[4] = _hrClass.ShortDateDayStart(getDependant.DependantDOB.ToString());
                        }
                        dr[5] = getDependant.Relationship;
                        dr[6] = getDependant.IsMedicalCovered;
                        dr[7] = getDependant.IsSpouse;
                        dr[8] = getDependant.IsChild;
                        dr[9] = getDependant.IsOtherDependant;
                        dt.Rows.Add(dr);
                        countRows++;
                    }
                    gvStaffDependants.DataSource = dt;
                    gvStaffDependants.DataBind();
                    return;
                }
                else
                {
                    PanelStaffDependantToBenefit.Visible = false;
                    return;
                }
            }
            catch { }
        }
        //save dependant to benefi
        private void SaveDependantToBenefitFromMedicalSchemeBenefits()
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvStaffDependants))//check if there is  any dependant selected to be assigned medical benefits
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save failed. There is no dependant selected. Select employee's dependant to be enlisted as a medical benefits beneficiary!", this, PanelEmploymentTerms);
                    return;
                }
                else
                {
                    int _countSpouse = 0, _countChildAndDependants = 0;
                    foreach (GridViewRow _grv in gvStaffDependants.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked)
                        {
                            HiddenField _HFIsSpouse = (HiddenField)_grv.FindControl("HiddenFieldIsSpouse");
                            if (_HFIsSpouse.Value == "1")//1 for its a spouse record
                                _countSpouse++;
                            else _countChildAndDependants++;
                        }
                    }
                    if (_countSpouse > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one employee's spouse. Select only one spouse to assign as a medical insurance beneficiary!", this, PanelEmploymentTerms);
                        return;
                    }
                    else if (_countChildAndDependants > 4)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than four employee's child and other dependant. Select up to four children to assign as medical insurance beneficiaries!", this, PanelEmploymentTerms);
                        return;
                    }
                    else
                    {
                        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                        foreach (GridViewRow _grv in gvStaffDependants.Rows)
                        {
                            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                            if (_cb.Checked)
                            {

                                HiddenField _HFDependantID = (HiddenField)_grv.FindControl("HFID");
                                HiddenField _HFIsSpouse = (HiddenField)_grv.FindControl("HiddenFieldIsSpouse");
                                HiddenField _HFIsChild = (HiddenField)_grv.FindControl("HiddenFieldIsChild");
                                HiddenField _HFIsOtherDependant = (HiddenField)_grv.FindControl("HiddenFieldIsOtherDependant");
                                int _dependantID = Convert.ToInt32(_HFDependantID.Value);
                                // Session["DependantToBenefitID"] = _dependantID.ToString();
                                //check if the dependant is a spouse
                                if (_HFIsSpouse.Value == "1")//1 for its a spouse record
                                {
                                    Spouse assignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _dependantID);
                                    assignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = true;
                                }
                                //check if it is a child
                                if (_HFIsChild.Value == "1")//1 for its a child record
                                {
                                    Child assignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == _dependantID);
                                    //check if the child is over 24 years
                                    if (assignMedicalSchemeToChild.child_dtDOB.Value.Date.Year < DateTime.Now.Date.Year - 24)
                                    {
                                        _hrClass.LoadHRManagerMessageBox(1, "Selected child  " + assignMedicalSchemeToChild.child_vName + " is over 24 years!", this, PanelEmploymentTerms);
                                        return;
                                    }
                                    else
                                        assignMedicalSchemeToChild.child_bIsMedicalCovered = true;
                                }
                                //check if it is other dependant 
                                if (_HFIsOtherDependant.Value == "1")//1 for its other dependant record
                                {
                                    OtherDependant assignMedicalSchemeToDependant = db.OtherDependants.Single(p => p.OtherDependantID == _dependantID);
                                    //check if the child is over 24 years
                                    if (assignMedicalSchemeToDependant.DependantDOB.Value.Date.Year < DateTime.Now.Date.Year - 24)
                                    {
                                        _hrClass.LoadHRManagerMessageBox(1, "Selected dependant " + assignMedicalSchemeToDependant.DependantName + " is over 24 years!", this, PanelEmploymentTerms);
                                        return;
                                    }
                                    else
                                        assignMedicalSchemeToDependant.IsMedicalCovered = true;

                                }
                            }

                            else
                            {
                                HiddenField _HFDependantID = (HiddenField)_grv.FindControl("HFID");
                                int _dependantID = Convert.ToInt32(_HFDependantID.Value);
                                HiddenField _HFIsSpouse = (HiddenField)_grv.FindControl("HiddenFieldIsSpouse");
                                HiddenField _HFIsChild = (HiddenField)_grv.FindControl("HiddenFieldIsChild");
                                HiddenField _HFIsOtherDependant = (HiddenField)_grv.FindControl("HiddenFieldIsOtherDependant");
                                //check if it is a spouse record
                                if (_HFIsSpouse.Value == "1")//1 for its a spouse record
                                {
                                    Spouse deAssignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _dependantID);
                                    deAssignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = false;
                                }
                                //check if it is a child
                                if (_HFIsChild.Value == "1")//1 for its a child record
                                {
                                    Child deAssignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == _dependantID);
                                    deAssignMedicalSchemeToChild.child_bIsMedicalCovered = false;
                                }
                                //check if it is other dependant 
                                if (_HFIsOtherDependant.Value == "1")//1 for its other dependant record
                                {
                                    OtherDependant deAssignMedicalSchemeToDependant = db.OtherDependants.Single(p => p.OtherDependantID == _dependantID);
                                    deAssignMedicalSchemeToDependant.IsMedicalCovered = false;
                                }
                            }
                        }
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "The selected dependant(s) has been en-listed as medical beneficiary!", this, PanelEmploymentTerms);
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelEmploymentTerms);
                return;
            }
        }
        protected void btnSaveStaffDependantToBenefit_OnClick(object sender, EventArgs e)
        {
            SaveDependantToBenefitFromMedicalSchemeBenefits();
            return;
        }
        ////display employee's  children to have medical benefit
        //private void DisplayChildForMedicalBenefit()
        //{
        //    try
        //    {
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        //check if the staff has got any children
        //        if (db.PROC_StaffChildren(_staffID).Any())
        //        {
        //            PanelChildToBenefit.Visible = true;

        //            DataTable dt = new DataTable();
        //            dt.Columns.Add(new DataColumn("SNO", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ID", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ChildName", typeof(string)));
        //            dt.Columns.Add(new DataColumn("ChildDOB", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Checked", typeof(bool)));

        //            DataRow dr;
        //            int countRows = 1;//count number of rows
        //            foreach (PROC_StaffChildrenResult getChild in db.PROC_StaffChildren(_staffID))
        //            {
        //                dr = dt.NewRow();
        //                dr[0] = countRows;
        //                dr[1] = getChild.child_iChildID;
        //                dr[2] = getChild.child_vName;
        //                if (getChild.child_dtDOB != null || getChild.child_dtDOB.ToString() != "")
        //                {
        //                    dr[3] = _hrClass.ShortDateDayStart(getChild.child_dtDOB.ToString());
        //                }
        //                dr[4] = getChild.child_bIsMedicalCovered;
        //                dt.Rows.Add(dr);
        //                countRows++;
        //            }
        //            gvChildToBenefit.DataSource = dt;
        //            gvChildToBenefit.DataBind();
        //            return;
        //        }
        //        else
        //        {
        //            PanelChildToBenefit.Visible = false;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////save children to benefit from medical scheme benefits
        //private void SaveChildrenToBenefitFromMedicalSchemeBenefits()
        //{
        //    try
        //    {
        //        //get employee's staff id
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        if (!_hrClass.isGridviewItemSelected(gvChildToBenefit))//check if there is  any child selected to be assigned medical benefits
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Save Failed!. There is no employee's child selected!.<br>Select employee's children to be alocated the medical scheme benefits!", this, PanelEmploymentTerms);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvChildToBenefit.Rows)
        //            {

        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            if (x > 2)
        //            {
        //                _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one employee children!.<br>Only two children can be allocated to medical scheme benefits!", this, PanelEmploymentTerms);
        //                return;
        //            }
        //            foreach (GridViewRow _grv in gvChildToBenefit.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                HiddenField _HFChildID = (HiddenField)_grv.FindControl("HFID");
        //                int ChildID = Convert.ToInt32(_HFChildID.Value);
        //                if (_cb.Checked) ++x;
        //                if (_cb.Checked)//if child is selected th assign medical benefits
        //                {
        //                    Child assignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == ChildID);
        //                    //check if the child is over 18 years
        //                    if (assignMedicalSchemeToChild.child_dtDOB.Value.Date.Year < DateTime.Now.Date.Year - 18)
        //                    {
        //                        _hrClass.LoadHRManagerMessageBox(1, "The Child selected, " + assignMedicalSchemeToChild.child_vName + " is over 18 years!", this, PanelEmploymentTerms);
        //                        return;
        //                    }
        //                    else
        //                        assignMedicalSchemeToChild.child_bIsMedicalCovered = true;
        //                }
        //                //deasign the employee's children who are not selected
        //                ///in future loop to check if the children selected have sur-pursses 18 years or 
        //                ///orr if the de-selected children have claimed any amount
        //                else//if child is not selected then deassign medical benefits
        //                {
        //                    Child deAssignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == ChildID);
        //                    deAssignMedicalSchemeToChild.child_bIsMedicalCovered = false;
        //                }
        //            }
        //            db.SubmitChanges();
        //            if (x > 1)
        //                _hrClass.LoadHRManagerMessageBox(2, "The selected child has been enlisted as a beneficairy!", this, PanelEmploymentTerms);
        //            else
        //                _hrClass.LoadHRManagerMessageBox(2, "The selected children has been enlisted as beneficiaries!", this, PanelEmploymentTerms);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelEmploymentTerms);
        //        return;
        //    }
        //}
        //protected void btnSaveChildToBenefit_OnClick(object sender, EventArgs e)
        //{
        //    SaveChildrenToBenefitFromMedicalSchemeBenefits();
        //}
    }
}