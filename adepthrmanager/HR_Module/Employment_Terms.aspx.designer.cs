﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdeptHRManager.HR_Module {
    
    
    public partial class Employment_Terms {
        
        /// <summary>
        /// UpdatePanelEmploymentTerms control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanelEmploymentTerms;
        
        /// <summary>
        /// PanelEmploymentTerms control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelEmploymentTerms;
        
        /// <summary>
        /// PanelStaffModuleHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelStaffModuleHeader;
        
        /// <summary>
        /// ImageStaffPhoto control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageStaffPhoto;
        
        /// <summary>
        /// HiddenFieldStaffID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField HiddenFieldStaffID;
        
        /// <summary>
        /// txtSearchByStaffName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByStaffName;
        
        /// <summary>
        /// txtSearchByStaffName_TextBoxWatermarkExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TextBoxWatermarkExtender txtSearchByStaffName_TextBoxWatermarkExtender;
        
        /// <summary>
        /// txtSearchByStaffName_AutoCompleteExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender txtSearchByStaffName_AutoCompleteExtender;
        
        /// <summary>
        /// txtSearchByIDNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByIDNumber;
        
        /// <summary>
        /// txtSrachByIDNumber_TextBoxWatermarkExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TextBoxWatermarkExtender txtSrachByIDNumber_TextBoxWatermarkExtender;
        
        /// <summary>
        /// txtSearchByIDNumber_AutoCompleteExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender txtSearchByIDNumber_AutoCompleteExtender;
        
        /// <summary>
        /// txtSearchByStaffReference control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByStaffReference;
        
        /// <summary>
        /// txtSearchByStaffReference_TextBoxWatermarkExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TextBoxWatermarkExtender txtSearchByStaffReference_TextBoxWatermarkExtender;
        
        /// <summary>
        /// txtSearchByStaffReference_AutoCompleteExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender txtSearchByStaffReference_AutoCompleteExtender;
        
        /// <summary>
        /// txtSearchByMobileNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByMobileNumber;
        
        /// <summary>
        /// txtSearchByMobileNumber_TextBoxWatermarkExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TextBoxWatermarkExtender txtSearchByMobileNumber_TextBoxWatermarkExtender;
        
        /// <summary>
        /// txtSearchByMobileNumber_AutoCompleteExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender txtSearchByMobileNumber_AutoCompleteExtender;
        
        /// <summary>
        /// txtEmploymentTermsBasicPay control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtEmploymentTermsBasicPay;
        
        /// <summary>
        /// ddlBasicPayCurrency control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlBasicPayCurrency;
        
        /// <summary>
        /// txtHousingBenefit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtHousingBenefit;
        
        /// <summary>
        /// txtConsolidatedPay control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtConsolidatedPay;
        
        /// <summary>
        /// txtRetirementDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtRetirementDate;
        
        /// <summary>
        /// ImageButtonRetirementDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ImageButtonRetirementDate;
        
        /// <summary>
        /// txtRetirementDate_CalendarExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.CalendarExtender txtRetirementDate_CalendarExtender;
        
        /// <summary>
        /// txtRetirementDate_MaskedEditExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditExtender txtRetirementDate_MaskedEditExtender;
        
        /// <summary>
        /// txtRetirementDate_MaskedEditValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.MaskedEditValidator txtRetirementDate_MaskedEditValidator;
        
        /// <summary>
        /// txtRetirementBenefits control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtRetirementBenefits;
        
        /// <summary>
        /// cblEmploymentTermsMedicalScheme control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList cblEmploymentTermsMedicalScheme;
        
        /// <summary>
        /// HiddenFieldEmploymentTermsID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField HiddenFieldEmploymentTermsID;
        
        /// <summary>
        /// HiddenFieldEmploymentTermsPerksDetailID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField HiddenFieldEmploymentTermsPerksDetailID;
        
        /// <summary>
        /// btnSaveEmployementTerms control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSaveEmployementTerms;
        
        /// <summary>
        /// ddlEmploymentTermsPerkName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlEmploymentTermsPerkName;
        
        /// <summary>
        /// txtEmploymentTermsPerkAmount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtEmploymentTermsPerkAmount;
        
        /// <summary>
        /// ddlAllowanceCurrency control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlAllowanceCurrency;
        
        /// <summary>
        /// btnEmploymentTermsAddPerk control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEmploymentTermsAddPerk;
        
        /// <summary>
        /// gvEmploymentTermsPerksDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gvEmploymentTermsPerksDetails;
        
        /// <summary>
        /// PanelStaffDependantToBenefit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelStaffDependantToBenefit;
        
        /// <summary>
        /// gvStaffDependants control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gvStaffDependants;
        
        /// <summary>
        /// btnSaveStaffDependantToBenefit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSaveStaffDependantToBenefit;
        
        /// <summary>
        /// ucConfirm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AdeptHRManager.HRManagerClasses.HRManagerUserControls.HRManager_Confirmation_Message_Box ucConfirm;
    }
}
