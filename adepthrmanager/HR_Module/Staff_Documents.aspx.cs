﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;
using System.IO;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Documents : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffDocuments(_staffID);//load qualification details
                    return;
                }
            }
            catch { }
        }
        //display staff documents
        private void DisplayStaffDocuments(Guid _staffID)
        {
            DisplayStaffDocuments();//dispaly staff records
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffDocuments(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffDocuments(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelStaffDocuments);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffDocuments);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffDocuments(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffDocuments(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffDocuments);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffDocuments);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffDocuments(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffDocuments(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffDocuments);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffDocuments);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffDocuments(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffDocuments(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffDocuments);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffDocuments);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //save staff document
        private void UploadStaffDocument()
        {
            //try
            //{
            if (txtStaffDocumentTitle.Text.Trim() == "")
            {
                lbDocumentsError.Text = "Enter document title!";
                txtStaffDocumentTitle.Focus(); return;
            }
            else if (FileUploadDocument.HasFile)
            {
                int _fileLength = FileUploadDocument.PostedFile.ContentLength;
                //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
                if (_hrClass.IsUploadedFileBig(_fileLength) == true)
                {
                    lbDocumentsError.Text = "File uploaded exceeds the allowed limit of 3mb.";
                    return;
                }
                else
                {
                    string _uploadedDocFileName = FileUploadDocument.FileName;//get name of the uploaded file name
                    string _fileExtension = Path.GetExtension(_uploadedDocFileName);

                    _fileExtension = _fileExtension.ToLower();
                    string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".rtf" };

                    bool _isFileAccepted = false;
                    foreach (string _acceptedFileExtension in _acceptedFileTypes)
                    {
                        if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
                            _isFileAccepted = true;
                    }
                    if (_isFileAccepted == false)
                    {
                        lbDocumentsError.Visible = true;
                        lbDocumentsError.Text = "The file you are trying to upload is not a permitted file type!";
                        return;
                    }
                    else
                    {
                        lbDocumentsError.Text = "";
                        string _documentTitle = txtStaffDocumentTitle.Text.Trim();
                        string _documentType = ddlDocumentType.SelectedValue.Trim();
                        //save uploaded staff document
                        SaveStaffDocument(FileUploadDocument, _documentType, _documentTitle, _uploadedDocFileName, _fileExtension);
                        return;
                    }
                }
            } 
            else
            {
                lbDocumentsError.Text = "Browse for the staff document to upload!";
                return;
            }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelStaffDocuments); return;
            //}
        }

        //save  staff document
        private void SaveStaffDocument(FileUpload _fileUpload, string _documentType, string _documentTitle, string _uploadedDocFileName, string _fileExtension)
        {

            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
            if (db.StaffDocs.Any(p => p.staffdoc_uiStaffID == _staffID && p.staffdoc_vDocTitle.ToLower() == _documentTitle.ToLower()))
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Another document with the same title already exists!", this, PanelStaffDocuments);
                return;
            }
            else
            {
                //save the document in the temp document
                _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
                string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
                lbDocumentsError.Text = "";
                Guid _loggedStaffMasterID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());

                //save employee document into the database
                FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                //get document info
                FileInfo documentInfo = new FileInfo(_fileLocation);
                int fileSize = (int)documentInfo.Length;//get file size
                byte[] fileDocument = new byte[fileSize];
                fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

                //add the new document
                StaffDoc newStaffDocument = new StaffDoc();
                newStaffDocument.staffdoc_uiStaffID = _staffID;
                newStaffDocument.staffdoc_vDocType = _documentType;
                newStaffDocument.staffdoc_vDocTitle = _documentTitle;
                newStaffDocument.staffdoc_vDocName = _uploadedDocFileName;
                newStaffDocument.staffdoc_vDocExtension = _fileExtension;
                newStaffDocument.staffdoc_vbDocument = fileDocument;
                newStaffDocument.staffdoc_dtDateStored = DateTime.Now;
                newStaffDocument.staffdoc_uiStoredByID = _loggedStaffMasterID;
                db.StaffDocs.InsertOnSubmit(newStaffDocument);
                db.SubmitChanges();
                //dispaly staff documents
                DisplayStaffDocuments();
                _hrClass.LoadHRManagerMessageBox(2, "Staff document has been successfully uploaded!", this, PanelStaffDocuments);


                //Guid _applicantID = Guid.Parse(Session["LoggedApplicantID"].ToString());
                //Applicant_Document newApplicantDocument = new Applicant_Document();
                //newApplicantDocument.ApplicantID = _applicantID;
                //newApplicantDocument.DocumentName = _newfileName;
                //newApplicantDocument.DocumentType = _fileType;//1 is for a CV document type,2 for cover leter
                //applicant_db.Applicant_Documents.InsertOnSubmit(newApplicantDocument);
                //applicant_db.SubmitChanges();
                ////update the document with the file name generated
                //Applicant_Document updateDocument = applicant_db.Applicant_Documents.Single(p => p.Applicant_DocumentID == newApplicantDocument.Applicant_DocumentID);
                ////create filename by using the name entered and the docunemt unique identifier
                //string _fileName = _newfileName + "_" + newApplicantDocument.Applicant_DocumentID.ToString() + _fileExtension;
                //string _documentPath = "../Applicant_Documents/" + _fileName;
                //updateDocument.DocumentPath = _documentPath;
                //applicant_db.SubmitChanges();

                ////check the document were are saving
                //if (_fileType == 1)//CV
                //{
                //    HiddenFieldCVID.Value = newApplicantDocument.Applicant_DocumentID.ToString();
                //    btnUploadCV.Text = "Upload New CV";
                //    HiddenFieldCVFilePath.Value = _documentPath;
                //    lnkBtnUploadedCV.Text = _newfileName + _fileExtension;
                //}
                //else if (_fileType == 2)//CV//cover letter
                //{
                //    HiddenFieldCoverLetterID.Value = newApplicantDocument.Applicant_DocumentID.ToString();
                //    btnUploadCoverLetter.Text = "Upload New Cover Letter";
                //    HiddenFieldCoverLetterPath.Value = _documentPath;
                //    lnkBtnUploadedCoverLetter.Text = _newfileName + _fileExtension;
                //}
                return;
            }
        }
        protected void btnUploadDocument_OnClick(object sender, EventArgs e)
        {
            UploadStaffDocument(); return;
        }
        //display staff document details
        private void DisplayStaffDocuments()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                _display = db.PROC_StaffDocuments(_staffID);
                gvStaffDocuments.DataSourceID = null;
                gvStaffDocuments.DataSource = _display;
                gvStaffDocuments.DataBind();
                return;
            }
            catch { }
        }
        //load staff document details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvStaffDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditDocument") == 0 || e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFStaffDocID = (HiddenField)gvStaffDocuments.Rows[ID].FindControl("HiddenField1");
                    int _staffDocID = Convert.ToInt32(_HFStaffDocID.Value);
                    if (e.CommandName.CompareTo("EditDocument") == 0)//check if we are editing the document
                    {
                        //load edit staff document details
                        //LoadEditStaffDocumentDetails(_staffDocID);
                        return;
                    }
                    else if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
                    {
                        HiddenFieldDocumentID.Value = _staffDocID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the document?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    }
                    else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
                    {
                        Session["HRDocumentViewCase"] = 1;//case 1 for viewing staff documents
                        string strPageUrl = "HR_Document_Viewer.aspx?ref=" + _HFStaffDocID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //method for deleting a staff document
        private void DeleteStaffDocument()
        {
            try
            {
                int _staffDocumentID = Convert.ToInt32(HiddenFieldDocumentID.Value);
                StaffDoc deleteStaffDoc = db.StaffDocs.Single(p => p.staffdoc_iStaffDocID == _staffDocumentID);
                db.StaffDocs.DeleteOnSubmit(deleteStaffDoc);
                db.SubmitChanges();
                DisplayStaffDocuments();//reload staff documnets after deletion
                _hrClass.LoadHRManagerMessageBox(2, "Document selected has been successfully deleted.", this, PanelStaffDocuments);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelStaffDocuments);
                return;
            }
        }
    }
}