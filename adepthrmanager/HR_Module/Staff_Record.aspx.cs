﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using AdeptHRManager.HRManagerClasses;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace AdeptHRManager.HR_Module
{
    public partial class Staff_Record : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRModule _hrModule = new HRModule();
        protected void Page_Load(object sender, EventArgs e)
        {
            //display staff additional controls
            LoadStaffAdditionalControls();
            if (!IsPostBack)
            {
                // LoadEmploymentHistory();//load employment history
                //_hrClass.GetListingItems(ddlPostedAt, 2000, "Posted At");//display branches(2000) available
                //_hrClass.GetListingItems(ddlDepartment, 3000, "Department");//display departments(3000) available
                //_hrClass.GetListingItems(ddlJobTitle, 7000, "Job Title");//display job titles(7000) available
                _hrClass.GetListingItemsValue1(ddlNationality, 1000, "Nationality");//display nationality(1000) available
                _hrClass.GetListingItems(ddlMaritalStatus, 19000, "Marital Status");//display marital status(19000) available
                _hrClass.GetListingItems(ddlPrefix, 15000, "Salutation");//display salutation(15000) available
                //_hrClass.GetStaffNames(ddlReportsTo, "Reports To");//populate reports to dropdown
                //LoadEditStaffRecordControls();//load edit staff details
                GetSearchedStaffOnPageLoad();//populate already searched staff details
                //DisplayWorkExperience();//display work experience records
                //_hrClass.GetListingItems(ddlEducationBackgroundLevelAttained, 8000, "Attained Level");//display attained level(8000) available
                //DisplayEducationBackground();//display education background
                //DisplayStaffSpouse();//display spouse details
                //DisplayStaffChildren();//display staff's children details
                //DisplayStaffDocuments();//dispaly staff records
                //DisplaySpouseForMedicalBenefit();//display staff's spouse to have medical benefits
                //DisplayChildForMedicalBenefit();//display staff's children to have medical benefits
                //_hrClass.GetListingItems(ddlEmploymentTermsTypeOfEmployment, 17000, "Type of Employment");//display employment types(17000) available
                //GetMedicalBenefits();//populate mediical scheme dropdown

                //_hrClass.GetListingItems(ddlEmploymentTermsPerkName, 18000, "Allowance");//display perkd/allowances(18000) available
            }
            ////add confirm record event 
           ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            ucConfirmDeleteBank.lnkBtnYesConfirmationClick+=new EventHandler(ucConfirmDeleteBank_lnkBtnYesConfirmationClick);
            ucConfirmDeleteAllergy.lnkBtnYesConfirmationClick+=new EventHandler(ucConfirmDeleteAllergy_lnkBtnYesConfirmationClick);
            ucConfirmDeleteChronicConditions.lnkBtnYesConfirmationClick+=new EventHandler(ucConfirmDeleteChronicConditions_lnkBtnYesConfirmationClick);
                
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                string _staffID = Request["ref"];
                if (_staffID != "" && _staffID != null)
                {
                    LoadEditStaffRecordControls();
                    HttpContext.Current.Session["SearchedStaffID"] = _staffID;
                    return;
                }
                else
                {
                    var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                    if (_searchedStaffID != null)
                    {
                        _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                        Guid _staffSearchedID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                        LoadAllStaffRecordCointrols(_staffSearchedID);//load staff personal information

                        return;
                    }
                }
            }
            catch { }
        }
        //display staff additional controls allowed
        private void LoadStaffAdditionalControls()
        {
            XDocument xmlDoc = XDocument.Load(Server.MapPath("~/xml_Files/Staff_AdditionalInformation.xml"));//read the document
            var staffAdditionalControls = from r in xmlDoc.Descendants("control")//get the required details
                                          select new
                                          {
                                              ControlID = r.Element("controlid").Value.Trim(),
                                              ControlName = r.Element("controlname").Value.Trim(),
                                              IsNumeric = r.Element("isnumeric").Value.Trim(),
                                              Active = r.Element("active").Value.Trim(),
                                          };
            foreach (var _control in staffAdditionalControls)
            {
                if (_control.Active == "Yes")
                {
                    //get text box associated with the conteol
                    TextBox _textbox = (TextBox)PanelStaffAdditionalFields.FindControl(_control.ControlID);
                    _textbox.Visible = true;
                    //get label associated with the control
                    Label _label = (Label)PanelStaffAdditionalFields.FindControl(_control.ControlID.Replace("txt", "lb"));
                    _label.Text = _control.ControlName + ":";
                    _label.Visible = true;
                    //get hiddenfield associated with the control to set id it is numeric or not
                    HiddenField _hiddenField = (HiddenField)PanelStaffAdditionalFields.FindControl(_control.ControlID.Replace("txt", "HiddenField"));
                    _hiddenField.Value = _control.IsNumeric;
                }
            }
            return;
        }

        #region "STAFF PERSONAL INFORMATION"
        //clear add staff record controls
        private void ClearAddStaffControls()
        {
            //clear entry controls
            //_hrClass.ClearEntryControls(PanelStaffModuleHeader);
            //_hrClass.ClearEntryControls(TabPanelPersonalInformation);
            _hrClass.ClearEntryControls(PanelStaffRecord);
            HiddenFieldStaffID.Value = "";
            lnkBtnSaveStaffRecord.Visible = true;
            lnkBtnUpdateStaffRecord.Enabled = false;
            lnkBtnUpdateStaffRecord.Visible = true;
            lnkBtnClearStaffRecord.Visible = true;
            lbError.Text = "";
            lbInfo.Text = "";
            ImageStaffPhoto.ImageUrl = "~/images/background/person.png";
            HttpContext.Current.Session["SearchedStaffID"] = null;
            return;
        }
        //validate staff record
        private string ValidateStaffRecords()
        {
            if (txtStaffReference.Text.Trim() == "")
            {
                txtStaffReference.Focus();
                return "Enter employee reference/number!";
            }
            //else if (txtStaffName.Text.Trim() == "")
            //{
            //    txtStaffName.Focus();
            //    return "Enter staff name!";
            //}
            else if (txtLastName.Text.Trim() == "")
            {
                txtLastName.Focus();
                return "Enter last name!";
            }
            else if (txtFirstName.Text.Trim() == "")
            {
                txtFirstName.Focus();
                return "Enter first name!";
            }
            else if (txtDOB.Text.Trim() == "")
            {
                txtDOB.Focus();
                return "Enter date of birth!";
            }
            else if (_hrClass.isDateValid(txtDOB.Text.Trim()) == false)
            {
                txtDOB.Focus();
                return "Invalid date of birth entered!";
            }
            else if (Convert.ToInt32(Convert.ToDateTime(txtDOB.Text.Trim()).Year) >= Convert.ToInt32(DateTime.Now.Year))
            {
                txtDOB.Focus();
                return "Invalid date of birth entered. Date of birth cannot be current date or a future date. Select a valid date of birth for the employee!";

            }
            else if (Convert.ToInt32(Convert.ToDateTime(txtDOB.Text.Trim()).Year) > (Convert.ToInt32(DateTime.Now.Year) - 18))
            {
                txtDOB.Focus();
                return "Invalid date of birth entered. Date of birth less than 18 years from the current date.";
            }
            else if (rblGender.SelectedIndex == -1)
            {
                rblGender.Focus();
                return "Select staff's gender!";
            }
            else if (ddlPrefix.SelectedIndex == 0)
            {
                ddlPrefix.Focus();
                return "Select salutation/prefix!";
            }
            else if (ddlMaritalStatus.SelectedIndex == 0)
            {
                ddlMaritalStatus.Focus();
                return "Select marital status!";
            }
            else if (ddlNationality.SelectedIndex == 0)
            {
                ddlNationality.Focus();
                return "Select nationality!";
            }
            else if (txtPassportExpiryDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtPassportExpiryDate.Text.Trim()) == false)
            {
                txtPassportExpiryDate.Focus();
                return "Invalid passport expiry date entered!";
            }

            //
            //else if (ddlDepartment.SelectedIndex == 0)
            //{
            //    ddlDepartment.Focus();
            //    return "Select department !";
            //}
            //else if (ddlJobTitle.SelectedIndex == 0)
            //{
            //    ddlJobTitle.Focus();
            //    return "Select job title !";
            //}
            //else if (ddlPostedAt.SelectedIndex == 0)
            //{
            //    ddlPostedAt.Focus();
            //    return "Select posted location for the staff !";
            //}
            //else if (txtJoinDate.Text.Trim() == "")
            //{
            //    txtJoinDate.Focus();
            //    return "Enter join date!";
            //}
            //else if (_hrClass.isDateValid(txtJoinDate.Text.Trim()) == false)
            //{
            //    txtJoinDate.Focus();
            //    return "Invalid join date entered!";
            //}
            //else if (txtExitDate.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtExitDate.Text.Trim()) == false)
            //{
            //    txtExitDate.Focus();
            //    return "Invalid exit date entered!";
            //}
            //else if (txtNationalID.Text.Trim() == "")
            //{
            //    txtNationalID.Focus();
            //    return "Enter national ID!";
            //}
            else if (txtNationalID.Text != "" && txtNationalID.Text.Trim().Length < 5)
            {
                txtNationalID.Focus();
                return "Invalid national ID entered!. National ID entered is too short!";
            }

            //else if (txtMobileNumber.Text.Trim() == "")
            //{
            //    txtMobileNumber.Focus();
            //    return "Enter mobile number";
            //}
            else if (txtMobileNumber.Text.Trim() != "" && txtMobileNumber.Text.Trim().Length < 7)
            {
                txtMobileNumber.Focus();
                return "Invalid mobile number entered!. The number entered is too short!";
            }
            else if (txtMobileNumber.Text.Trim() != "" && txtMobileNumber.Text.Trim().Length > 15)
            {
                txtMobileNumber.Focus();
                return "Invalid mobile number entered!. Mobile number cannot have more than 15 digits!";
            }
            //else if (txtEmailAddress.Text.Trim() == "")
            //{
            //    txtEmailAddress.Focus();
            //    return "Enter email address!";
            //}
            else if (txtEmailAddress.Text.Trim() != "" && _hrClass.IsValidEmail(txtEmailAddress.Text.Trim()) == false)
            {
                txtEmailAddress.Focus();
                return "Invalid email address entered!";
            }
            else if (txtAlternativeEmailAddress.Text.Trim() != "" && _hrClass.IsValidEmail(txtAlternativeEmailAddress.Text.Trim()) == false)
            {
                txtAlternativeEmailAddress.Focus();
                return "Invalid alternative email address entered!";
            }
            else return "";
        }
        //method for saving a staff record
        private void SaveStaffRecord()
        {
            try
            {
                string _staffRef = txtStaffReference.Text.Trim(), _staffName = "", _idNumber = txtNationalID.Text.Trim(), _mobileNumber = txtMobileNumber.Text.Trim();
                string _lastName = txtLastName.Text.Trim(), _firstName = txtFirstName.Text.Trim(), _middleName = txtMiddleName.Text.Trim();
                _staffName = (_lastName + " " + _firstName + " " + _middleName).Trim();
                //get employee reference
                //ContentPlaceHolder _mainContent = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                //Panel _staffModuleHeaderPanel = (Panel)_mainContent.FindControl("PanelStaffModuleHeader");
                //TextBox _txtStaffRef = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByStaffReference");//get staff reference textbox
                //TextBox _txtStaffName = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByEmployeeName");//get staff name textbox

                string _error = ValidateStaffRecords(); //check for errors
                //if (_txtStaffRef.Text.Trim() == "")
                //{
                //    _hrClass.LoadHRManagerMessageBox(1, "Enter employee reference number!", this, PanelStaffRecord);
                //    return;
                //}
                //else if (_txtStaffName.Text.Trim() == "")
                //{
                //    _hrClass.LoadHRManagerMessageBox(1, "Enter staff's name!", this, PanelStaffRecord);
                //    return;
                //}
                //else 
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    lbError.Text = _error;
                    return;
                }
                else if (db.StaffMasters.Any(p => p.StaffRef.ToLower() == _staffRef.ToLower()))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save failed. There already exists an employee with the same reference number entered!", this, PanelStaffRecord);
                    return;
                }

                else if (db.StaffMasters.Any(p => p.StaffName.ToLower() == _staffName.ToLower()))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save failed. There already exists an employee with the same name entered!", this, PanelStaffRecord);
                    return;
                }
                else if (db.StaffMasters.Any(p => p.IDNumber.ToLower() == _idNumber.ToLower() && _idNumber.Trim() != ""))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save failed. There already exists an employee with the national ID number entered!", this, PanelStaffRecord);
                    return;
                }
                else if (db.StaffMasters.Any(p => p.PhoneNumber.ToLower() == _mobileNumber.ToLower() && _mobileNumber.Trim() != ""))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save failed. There already exists an employee with the same mobile number entered!", this, PanelStaffRecord);
                    return;
                }
                else
                {
                    //save staff record
                    StaffMaster newStaff = new StaffMaster();
                    //newStaff.StaffRef = _txtStaffRef.Text.Trim();
                    //newStaff.StaffName = _txtStaffName.Text.Trim();
                    newStaff.StaffRef = txtStaffReference.Text.Trim();
                    //newStaff.StaffName = txtStaffName.Text.Trim();
                    newStaff.StaffName = _staffName;
                    newStaff.LastName = _lastName;
                    newStaff.FirstName = _firstName;
                    newStaff.MiddleName = _middleName;
                    //newStaff.DepartmentID = 3001;
                    newStaff.JobTitleID = 7001;
                    //newStaff.PostedAtID = Convert.ToInt32(ddlPostedAt.SelectedValue);
                    //if (ddlReportsTo.SelectedIndex != 0)
                    //    newStaff.ReportingToStaffID = _hrClass.ReturnGuid(ddlReportsTo.SelectedValue);
                    //newStaff.JoinDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    //if (txtExitDate.Text.Trim() != "" && txtExitDate.Text.Trim() != "__/___/____")
                    //    newStaff.ExitDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    //else newStaff.ExitDate = null;
                    newStaff.DateOfBirth = Convert.ToDateTime(txtDOB.Text.Trim()).Date;
                    newStaff.Gender = rblGender.SelectedValue;
                    newStaff.SalutationID = Convert.ToInt32(ddlPrefix.SelectedValue);
                    newStaff.MaritalStatusID = Convert.ToInt32(ddlMaritalStatus.SelectedValue);
                    newStaff.PhoneNumber = txtMobileNumber.Text.Trim();
                    newStaff.NationalityID = Convert.ToInt32(ddlNationality.SelectedValue);
                    newStaff.PassportNumber = txtPassportNumber.Text.Trim();
                    if (txtPassportExpiryDate.Text.Trim() != "__/___/____")
                        newStaff.PassportExpiryDate = Convert.ToDateTime(txtPassportExpiryDate.Text.Trim()).Date;
                    else newStaff.PassportExpiryDate = null;
                    newStaff.IDNumber = txtNationalID.Text.Trim();
                    newStaff.DiplomaticIDNumber = txtDiplomaticIDNumber.Text.Trim();
                    newStaff.DrivingPermitNumber = txtDrivingPermitNumber.Text.Trim();
                    newStaff.NSSFNumber = txtNSSFNumber.Text.Trim();
                    //newStaff.NHIFNumber = txtNHIFNumber.Text.Trim()
                    newStaff.PinNumber = txtPinNumber.Text.Trim();
                    newStaff.TINNumber = txtTINNumber.Text.Trim();
                    newStaff.PermanentAddress = txtPermanentAddress.Text.Trim();
                    newStaff.CurrentAddress = txtCurrentAddress.Text.Trim();
                    newStaff.TelephoneContact = txtTelephoneContact.Text.Trim();

                    //if (txtAlternativeMobileNumber.Text.Trim() != "")
                    //    newStaff.AlternativePhoneNumber = txtAlternativeMobileNumber.Text.Trim();
                    //else newStaff.AlternativePhoneNumber = null;
                    newStaff.EmailAddress = txtEmailAddress.Text.Trim();
                    if (txtAlternativeEmailAddress.Text.Trim() != "")
                        newStaff.AlternativeEmailAddress = txtAlternativeEmailAddress.Text.Trim();
                    else newStaff.AlternativeEmailAddress = null;
                    newStaff.PostalAddress = txtPostalAddress.Text.Trim();
                    newStaff.Languages = txtLanguages.Text.Trim();

                    //if (txtSalary.Text.Trim() != "")
                    //    newStaff.Salary = Convert.ToDecimal(txtSalary.Text.Trim());
                    //newStaff.EmergencyContactPerson = txtEmergencyContactPerson.Text.Trim();
                    //newStaff.EmergencyContactPhoneNo = txtEmergencyContactPersonPhoneNumber.Text.Trim();
                    newStaff.NextOfKinName = txtNextOfKinName.Text.Trim();
                    newStaff.NextOfKinPhone = txtNextOfKinPhoneNumber.Text.Trim();
                    newStaff.NextOfKinRelationship = txtNextOfKinRelationship.Text.Trim();
                    newStaff.NextOfKinAddress = txtNextOfKinAddress.Text.Trim();
                    newStaff.NextOfKin2Name = txtNextOfKin2Name.Text.Trim();
                    newStaff.NextOfKin2Phone = txtNextOfKin2PhoneNumber.Text.Trim();
                    newStaff.NextOfKin2Relationship = txtNextOfKin2Relationship.Text.Trim();
                    newStaff.NextOfKin2Address = txtNextOfKin2Address.Text.Trim();
                    newStaff.DisabilityDetails = txtDisabilityDetails.Text.Trim();
                    newStaff.Referee1Details = txtRefereeDetails1.Text.Trim();
                    newStaff.Referee2Details = txtRefereeDetails2.Text.Trim();
                    newStaff.Referee3Details = txtRefereeDetails3.Text.Trim();
                    newStaff.Remarks = txtStaffRecordRemarks.Text.Trim();
                    db.StaffMasters.InsertOnSubmit(newStaff);
                    db.SubmitChanges();
                    //save staff addional fields
                    SaveOrUpdateStaffRecordAdditionalFields(newStaff.StaffID);
                    HiddenFieldStaffID.Value = newStaff.StaffID.ToString();
                    lnkBtnUpdateStaffRecord.Enabled = true;
                    //start here
                    lbError.Text = "";
                    //load search controls
                    //txtSearchByStaffReference.Text = txtStaffReference.Text.Trim();
                    //txtSearchByStaffName.Text = txtStaffName.Text.Trim();
                    //txtSearchByIDNumber.Text = txtNationalID.Text.Trim();
                    //txtSearchByMobileNumber.Text = txtMobileNumber.Text.Trim();
                    //load search controls
                    _hrClass.LoadSearchStaffControls(txtSearchByStaffName, _staffName, txtSearchByStaffReference, _staffRef, txtSearchByIDNumber, _idNumber, txtSearchByMobileNumber, _mobileNumber);
                    _hrClass.LoadHRManagerMessageBox(2, "Staff details have been successfully saved.", this, PanelStaffRecord);
                    HttpContext.Current.Session["SearchedStaffID"] = newStaff.StaffID.ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //method for updating a staff record
        private void UpdateStaffRecord()
        {
            try
            {
                //get employee reference

                Guid _id = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                string _staffRef = txtStaffReference.Text.Trim(), _staffName = "", _idNumber = txtNationalID.Text.Trim(), _mobileNumber = txtMobileNumber.Text.Trim();
                string _lastName = txtLastName.Text.Trim(), _firstName = txtFirstName.Text.Trim(), _middleName = txtMiddleName.Text.Trim();
                _staffName = (_lastName + " " + _firstName + " " + _middleName).Trim();
                string _error = ValidateStaffRecords(); //check for errors              
                //ContentPlaceHolder _mainContent = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                //Panel _staffModuleHeaderPanel = (Panel)_mainContent.FindControl("PanelStaffModuleHeader");
                //TextBox _txtStaffRef = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByStaffReference");//get staff reference textbox
                //TextBox _txtStaffName = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByEmployeeName");//get staff name textbox
                //if (_txtStaffRef.Text.Trim() == "")
                //{
                //    _hrClass.LoadHRManagerMessageBox(1, "Enter employee reference number!", this, PanelStaffRecord);
                //    return;
                //}
                //else if (_txtStaffName.Text.Trim() == "")
                //{
                //    _hrClass.LoadHRManagerMessageBox(1, "Enter staff's name!", this, PanelStaffRecord);
                //    return;
                //}
                //else 
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    lbError.Text = _error;
                    return;
                }
                else if (db.StaffMasters.Any(p => p.StaffRef.ToLower() == txtStaffReference.Text.Trim().ToLower() && p.StaffID != _id))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update failed. There already exists an employee with the same reference number entered!", this, PanelStaffRecord);
                    return;
                }
                else if (db.StaffMasters.Any(p => p.StaffID != _id && p.StaffName.ToLower() == _staffName.ToLower()))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update failed. There already exists an employee with the same name entered!", this, PanelStaffRecord);
                    return;
                }
                else if (db.StaffMasters.Any(p => p.StaffID != _id && p.IDNumber.ToLower() == _idNumber.ToLower() && _idNumber != ""))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update failed. There already exists an employee with the national ID number entered!", this, PanelStaffRecord);
                    return;
                }
                else if (db.StaffMasters.Any(p => p.StaffID != _id && p.PhoneNumber.ToLower() == _mobileNumber.ToLower() && _mobileNumber != ""))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update failed. There already exists an employee with the same mobile number entered!", this, PanelStaffRecord);
                    return;
                }
                else
                {

                    //update staff record
                    StaffMaster updateStaff = db.StaffMasters.Single(p => p.StaffID == _id);
                    //updateStaff.StaffRef = _txtStaffRef.Text.Trim();
                    //updateStaff.StaffName = _txtStaffName.Text.Trim();

                    updateStaff.StaffRef = _staffRef;
                    updateStaff.StaffName = _staffName;
                    updateStaff.LastName = _lastName;
                    updateStaff.FirstName = _firstName;
                    updateStaff.MiddleName = _middleName;
                    //updateStaff.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    //updateStaff.JobTitleID = Convert.ToInt32(ddlJobTitle.SelectedValue);
                    //updateStaff.PostedAtID = Convert.ToInt32(ddlPostedAt.SelectedValue);
                    //if (ddlReportsTo.SelectedIndex != 0)
                    //    updateStaff.ReportingToStaffID = _hrClass.ReturnGuid(ddlReportsTo.SelectedValue);
                    //updateStaff.JoinDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    //if (txtExitDate.Text.Trim() != "" && txtExitDate.Text.Trim() != "__/___/____")
                    //    updateStaff.ExitDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    //else updateStaff.ExitDate = null;
                    updateStaff.DateOfBirth = Convert.ToDateTime(txtDOB.Text.Trim()).Date;
                    updateStaff.Gender = rblGender.SelectedValue;
                    updateStaff.SalutationID = Convert.ToInt32(ddlPrefix.SelectedValue);
                    updateStaff.MaritalStatusID = Convert.ToInt32(ddlMaritalStatus.SelectedValue);
                    updateStaff.PhoneNumber = txtMobileNumber.Text.Trim();
                    updateStaff.NationalityID = Convert.ToInt32(ddlNationality.SelectedValue);
                    updateStaff.PassportNumber = txtPassportNumber.Text.Trim();
                    if (txtPassportExpiryDate.Text.Trim() != "__/___/____")
                        updateStaff.PassportExpiryDate = Convert.ToDateTime(txtPassportExpiryDate.Text.Trim()).Date;
                    else updateStaff.PassportExpiryDate = null;
                    updateStaff.IDNumber = txtNationalID.Text.Trim();
                    updateStaff.DiplomaticIDNumber = txtDiplomaticIDNumber.Text.Trim();
                    updateStaff.DrivingPermitNumber = txtDrivingPermitNumber.Text.Trim();
                    updateStaff.NSSFNumber = txtNSSFNumber.Text.Trim();
                    //updateStaff.NHIFNumber = txtNHIFNumber.Text.Trim()
                    updateStaff.PinNumber = txtPinNumber.Text.Trim();
                    updateStaff.TINNumber = txtTINNumber.Text.Trim();
                    updateStaff.PermanentAddress = txtPermanentAddress.Text.Trim();
                    updateStaff.CurrentAddress = txtCurrentAddress.Text.Trim();
                    updateStaff.TelephoneContact = txtTelephoneContact.Text.Trim();

                    //if (txtAlternativeMobileNumber.Text.Trim() != "")
                    //    updateStaff.AlternativePhoneNumber = txtAlternativeMobileNumber.Text.Trim();
                    //else updateStaff.AlternativePhoneNumber = null;
                    updateStaff.EmailAddress = txtEmailAddress.Text.Trim();
                    if (txtAlternativeEmailAddress.Text.Trim() != "")
                        updateStaff.AlternativeEmailAddress = txtAlternativeEmailAddress.Text.Trim();
                    else updateStaff.AlternativeEmailAddress = null;
                    updateStaff.PostalAddress = txtPostalAddress.Text.Trim();
                    updateStaff.Languages = txtLanguages.Text.Trim();

                    //if (txtSalary.Text.Trim() != "")
                    //    updateStaff.Salary = Convert.ToDecimal(txtSalary.Text.Trim());
                    //updateStaff.EmergencyContactPerson = txtEmergencyContactPerson.Text.Trim();
                    //updateStaff.EmergencyContactPhoneNo = txtEmergencyContactPersonPhoneNumber.Text.Trim();
                    updateStaff.NextOfKinName = txtNextOfKinName.Text.Trim();
                    updateStaff.NextOfKinPhone = txtNextOfKinPhoneNumber.Text.Trim();
                    updateStaff.NextOfKinRelationship = txtNextOfKinRelationship.Text.Trim();
                    updateStaff.NextOfKinAddress = txtNextOfKinAddress.Text.Trim();
                    updateStaff.NextOfKin2Name = txtNextOfKin2Name.Text.Trim();
                    updateStaff.NextOfKin2Phone = txtNextOfKin2PhoneNumber.Text.Trim();
                    updateStaff.NextOfKin2Relationship = txtNextOfKin2Relationship.Text.Trim();
                    updateStaff.NextOfKin2Address = txtNextOfKin2Address.Text.Trim();
                    updateStaff.DisabilityDetails = txtDisabilityDetails.Text.Trim();
                    updateStaff.Referee1Details = txtRefereeDetails1.Text.Trim();
                    updateStaff.Referee2Details = txtRefereeDetails2.Text.Trim();
                    updateStaff.Referee3Details = txtRefereeDetails3.Text.Trim();
                    updateStaff.Remarks = txtStaffRecordRemarks.Text.Trim();
                    db.SubmitChanges();
                    //update staff additional fileds
                    SaveOrUpdateStaffRecordAdditionalFields(_id);
                    lbError.Text = "";
                    //txtSearchByStaffReference.Text = txtStaffReference.Text.Trim();
                    //txtSearchByStaffName.Text = txtStaffName.Text.Trim();
                    //load serach controls
                    _hrClass.LoadSearchStaffControls(txtSearchByStaffName, _staffName, txtSearchByStaffReference, _staffRef, txtSearchByIDNumber, _idNumber, txtSearchByMobileNumber, _mobileNumber);
                    _hrClass.LoadHRManagerMessageBox(2, "Staff details have been successfully updated.", this, PanelStaffRecord);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //save or update staff additonal fields
        private void SaveOrUpdateStaffRecordAdditionalFields(Guid _staffID)
        {
            if (db.StaffMasterAdditionals.Any(p => p.StaffID == _staffID))//check if the staff has any addional fields 
                //update additonal fields
                UpdateStaffRecordAdditionalFields(_staffID);
            else
                //save new  additional fields
                SaveStaffRecordAdditonalFileds(_staffID);
        }
        //save staff additional details
        private void SaveStaffRecordAdditonalFileds(Guid _staffID)
        {
            StaffMasterAdditional newStaffAdditions = new StaffMasterAdditional();
            newStaffAdditions.StaffID = _staffID;
            if (txtHRDescription01.Visible == true)
                newStaffAdditions.HRDescription01 = txtHRDescription01.Text.Trim();
            if (txtHRDescription02.Visible == true)
                newStaffAdditions.HRDescription02 = txtHRDescription02.Text.Trim();
            if (txtHRDescription03.Visible == true)
                newStaffAdditions.HRDescription03 = txtHRDescription03.Text.Trim();
            if (txtHRDescription04.Visible == true)
                newStaffAdditions.HRDescription04 = txtHRDescription04.Text.Trim();
            if (txtHRDescription05.Visible == true)
                newStaffAdditions.HRDescription05 = txtHRDescription06.Text.Trim();
            if (txtHRDescription06.Visible == true)
                newStaffAdditions.HRDescription06 = txtHRDescription06.Text.Trim();
            if (txtHRDescription07.Visible == true)
                newStaffAdditions.HRDescription07 = txtHRDescription07.Text.Trim();
            if (txtHRDescription08.Visible == true)
                newStaffAdditions.HRDescription08 = txtHRDescription08.Text.Trim();
            if (txtHRDescription09.Visible == true)
                newStaffAdditions.HRDescription09 = txtHRDescription09.Text.Trim();
            if (txtHRDescription10.Visible == true)
                newStaffAdditions.HRDescription10 = txtHRDescription10.Text.Trim();
            db.StaffMasterAdditionals.InsertOnSubmit(newStaffAdditions);
            db.SubmitChanges();
        }
        //update staff additonal fields
        private void UpdateStaffRecordAdditionalFields(Guid _staffID)
        {

            StaffMasterAdditional updateStaffAdditions = db.StaffMasterAdditionals.Single(p => p.StaffID == _staffID);
            if (txtHRDescription01.Visible == true)
                updateStaffAdditions.HRDescription01 = txtHRDescription01.Text.Trim();
            if (txtHRDescription02.Visible == true)
                updateStaffAdditions.HRDescription02 = txtHRDescription02.Text.Trim();
            if (txtHRDescription03.Visible == true)
                updateStaffAdditions.HRDescription03 = txtHRDescription03.Text.Trim();
            if (txtHRDescription04.Visible == true)
                updateStaffAdditions.HRDescription04 = txtHRDescription04.Text.Trim();
            if (txtHRDescription05.Visible == true)
                updateStaffAdditions.HRDescription05 = txtHRDescription06.Text.Trim();
            if (txtHRDescription06.Visible == true)
                updateStaffAdditions.HRDescription06 = txtHRDescription06.Text.Trim();
            if (txtHRDescription07.Visible == true)
                updateStaffAdditions.HRDescription07 = txtHRDescription07.Text.Trim();
            if (txtHRDescription08.Visible == true)
                updateStaffAdditions.HRDescription08 = txtHRDescription08.Text.Trim();
            if (txtHRDescription09.Visible == true)
                updateStaffAdditions.HRDescription09 = txtHRDescription09.Text.Trim();
            if (txtHRDescription10.Visible == true)
                updateStaffAdditions.HRDescription10 = txtHRDescription10.Text.Trim();
            db.SubmitChanges();
        }
        //save staff record
        protected void lnkBtnSaveStaffRecord_Click(object sender, EventArgs e)
        {
            SaveStaffRecord(); return;
        }
        //update staff record
        protected void lnkBtnUpdateStaffRecord_Click(object sender, EventArgs e)
        {
            UpdateStaffRecord(); return;
        }
        //clear
        protected void lnkBtnClearStaffRecord_Click(object sender, EventArgs e)
        {
            //ClearAddStaffControls(); return;
            ClearAllStaffRecordControls();
            _hrClass.ClearEntryControls(PanelStaffModuleHeader);
            return;
        }
        // populate the edit staff records controls
        private void LoadEditStaffRecordControls()
        {
            ClearAddStaffControls();
            //get the passed job id
            string _staffID = Request["ref"];
            if (_staffID != "" && _staffID != null)
            {
                //ContentPlaceHolder _mainContent = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                //Panel _staffModuleHeaderPanel = (Panel)_mainContent.FindControl("PanelStaffModuleHeader");
                //TextBox _txtStaffRef = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByStaffReference");//get staff reference textbox
                //TextBox _txtStaffName = (TextBox)_staffModuleHeaderPanel.FindControl("txtSearchByEmployeeName");//get staff name textbox

                Guid _id = _hrClass.ReturnGuid(_staffID);
                StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID == _id);
                HiddenFieldStaffID.Value = _staffID;
                //_txtStaffRef.Text = getStaff.StaffRef;
                //_txtStaffName.Text = getStaff.StaffName;//search fields
                txtSearchByStaffReference.Text = getStaff.StaffRef;
                txtSearchByStaffName.Text = getStaff.StaffName;//search fields
                txtSearchByIDNumber.Text = getStaff.IDNumber;//s
                txtSearchByMobileNumber.Text = getStaff.PhoneNumber;//search phone
                txtStaffReference.Text = getStaff.StaffRef;
                //txtStaffName.Text = getStaff.StaffName;
                txtLastName.Text = getStaff.LastName;
                txtFirstName.Text = getStaff.FirstName;
                txtMiddleName.Text = getStaff.MiddleName;
                //ddlDepartment.SelectedValue = getStaff.DepartmentID.ToString();
                //ddlJobTitle.SelectedValue = getStaff.JobTitleID.ToString();
                //ddlPostedAt.SelectedValue = getStaff.PostedAtID.ToString();
                //if (getStaff.ReportingToStaffID != null)
                //    ddlReportsTo.SelectedValue = getStaff.ReportingToStaffID.ToString();
                //txtJoinDate.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
                ////txtEmploymentTermsDateOfJoin.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
                //if (getStaff.ExitDate != null)
                //    txtExitDate.Text = _hrClass.ShortDateDayStart(getStaff.ExitDate.ToString());
                //else txtExitDate.Text = "";

                txtDOB.Text = _hrClass.ShortDateDayStart(getStaff.DateOfBirth.ToString());
                rblGender.SelectedValue = getStaff.Gender;
                if (getStaff.SalutationID != null)
                    ddlPrefix.SelectedValue = getStaff.SalutationID.ToString();
                else ddlPrefix.SelectedIndex = 0;
                if (getStaff.MaritalStatusID != null)
                    ddlMaritalStatus.SelectedValue = getStaff.MaritalStatusID.ToString();
                else ddlMaritalStatus.SelectedIndex = 0;
                txtMobileNumber.Text = getStaff.PhoneNumber;
                if (getStaff.NationalityID != null)
                    ddlNationality.SelectedValue = getStaff.NationalityID.ToString();
                else ddlNationality.SelectedIndex = 0;
                txtPassportNumber.Text = getStaff.PassportNumber;
                if (getStaff.PassportExpiryDate != null)
                    txtPassportExpiryDate.Text = _hrClass.ShortDateDayStart(getStaff.PassportExpiryDate.ToString());
                else txtPassportExpiryDate.Text = "";
                txtNationalID.Text = getStaff.IDNumber.ToString();
                txtDiplomaticIDNumber.Text = getStaff.DiplomaticIDNumber;
                txtDrivingPermitNumber.Text = getStaff.DrivingPermitNumber;
                txtNSSFNumber.Text = getStaff.NSSFNumber;
                txtPinNumber.Text = getStaff.PinNumber;
                txtTINNumber.Text = getStaff.TINNumber;
                txtPermanentAddress.Text = getStaff.PermanentAddress;
                txtCurrentAddress.Text = getStaff.CurrentAddress;
                txtTelephoneContact.Text = getStaff.TelephoneContact;
                //txtNHIFNumber.Text = getStaff.NHIFNumber;
                //if (getStaff.AlternativePhoneNumber != null)
                //    txtAlternativeMobileNumber.Text = getStaff.AlternativePhoneNumber;
                txtEmailAddress.Text = getStaff.EmailAddress;
                if (Convert.ToString(getStaff.AlternativeEmailAddress) != "")
                    txtAlternativeEmailAddress.Text = getStaff.AlternativeEmailAddress;
                else txtAlternativeEmailAddress.Text = "";
                txtPostalAddress.Text = getStaff.PostalAddress;
                txtLanguages.Text = getStaff.Languages;
                //if (getStaff.Salary != null)
                //    txtSalary.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaff.Salary);
                //txtEmergencyContactPerson.Text = getStaff.EmergencyContactPerson;
                //txtEmergencyContactPersonPhoneNumber.Text = getStaff.EmergencyContactPhoneNo;
                txtNextOfKinName.Text = getStaff.NextOfKinName;
                txtNextOfKinPhoneNumber.Text = getStaff.NextOfKinPhone;
                txtNextOfKinRelationship.Text = getStaff.NextOfKinRelationship;
                txtNextOfKinAddress.Text = getStaff.NextOfKinAddress;
                txtNextOfKin2Name.Text = getStaff.NextOfKin2Name;
                txtNextOfKin2PhoneNumber.Text = getStaff.NextOfKin2Phone;
                txtNextOfKin2Relationship.Text = getStaff.NextOfKin2Relationship;
                txtNextOfKin2Address.Text = getStaff.NextOfKin2Address;
                txtDisabilityDetails.Text = getStaff.DisabilityDetails;
                txtRefereeDetails1.Text = getStaff.Referee1Details;
                txtRefereeDetails2.Text = getStaff.Referee2Details;
                txtRefereeDetails3.Text = getStaff.Referee3Details;
                txtStaffRecordRemarks.Text = getStaff.Remarks;
                lnkBtnSaveStaffRecord.Visible = false;
                lnkBtnClearStaffRecord.Visible = true;
                lnkBtnUpdateStaffRecord.Enabled = true;
                //load staff additional fields
                LoadEditStaffAddionalFieldsControls(_id);
                lbError.Text = "";
                lbInfo.Text = "";
                LoadStaffPhoto(_id);//load staff photo
                //display employment term details
                //LoadStaffEmploymentTerms(_id);//load the staff's employment term details
                //DisplayEmploymentPerkDetails(_id);//display employment term perk/allowances associated with the staff

                return;
            }
        }
        //load edit staff addional fields for edit
        private void LoadEditStaffAddionalFieldsControls(Guid _staffID)
        {
            if (db.StaffMasterAdditionals.Any(p => p.StaffID == _staffID))
            {
                StaffMasterAdditional getStaffAdditionalFields = db.StaffMasterAdditionals.Single(p => p.StaffID == _staffID);
                if (txtHRDescription01.Visible == true)
                    txtHRDescription01.Text = Convert.ToString(getStaffAdditionalFields.HRDescription01);
                if (txtHRDescription02.Visible == true)
                    txtHRDescription02.Text = Convert.ToString(getStaffAdditionalFields.HRDescription02);
                if (txtHRDescription03.Visible == true)
                    txtHRDescription03.Text = Convert.ToString(getStaffAdditionalFields.HRDescription03);
                if (txtHRDescription04.Visible == true)
                    txtHRDescription04.Text = Convert.ToString(getStaffAdditionalFields.HRDescription04);
                if (txtHRDescription05.Visible == true)
                    txtHRDescription05.Text = Convert.ToString(getStaffAdditionalFields.HRDescription05);
                if (txtHRDescription06.Visible == true)
                    txtHRDescription06.Text = Convert.ToString(getStaffAdditionalFields.HRDescription06);
                if (txtHRDescription07.Visible == true)
                    txtHRDescription07.Text = Convert.ToString(getStaffAdditionalFields.HRDescription07);
                if (txtHRDescription08.Visible == true)
                    txtHRDescription08.Text = Convert.ToString(getStaffAdditionalFields.HRDescription08);
                if (txtHRDescription09.Visible == true)
                    txtHRDescription09.Text = Convert.ToString(getStaffAdditionalFields.HRDescription09);
                if (txtHRDescription10.Visible == true)
                    txtHRDescription10.Text = Convert.ToString(getStaffAdditionalFields.HRDescription10);
            }
        }
        #endregion
        #region "STAFF LANGUAGES"
        private void DisplayLanguages(Guid _staffID)//Display Staff languages
        {

            try
            {
                object _display;
             
                _display = db.PROC_StaffLanguage(_staffID);
                gvLanguage.DataSourceID = null;
                gvLanguage.DataSource = _display;
                gvLanguage.DataBind();
                return;
            }
            catch { }
        }

        //validate add employment language controls
        private string ValidateStaffLanguage()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Language save failed. Enter staff's personal information or search for the staff before adding his/her language details!";
            else if (txtLanguageName.Text.Trim() == "")
                return "Enter the language!";
            else if (_hrClass.isNumberValid(txtLanguageSpeaking.Text.Trim()) == false)
                return "Enter a valid figure to grade the employee speaking abilities";
            else if (_hrClass.isNumberValid(txtLanguageReading.Text.Trim()) == false)
                return "Enter a valid figure to grade the employee reading abilities";
            else if (_hrClass.isNumberValid(txtLanguageWriting.Text.Trim()) == false)
                return "Enter a valid figure to grade the employee writing abilities";
            else return "";
        }

        protected void btnAddLanguage_Click(object sender, EventArgs e)
        {
            SaveLanguages(); return;//
        }

        //method for saving languages
        private void SaveLanguages()
        {
            try
            {
                string _error = ValidateStaffLanguage();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
					string _LanguageName=txtLanguageName.Text.Trim();
                    if (btnAddLanguage.Text.ToLower() == "save language")
                    {
                        //check if the selected language is selected against the staff
                        if (db.StaffLanguages.Any(p=>p.LanguageName==_LanguageName))
                        {

                            _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected language has already been saved!", this, PanelStaffRecord);
                            return;
                        }
                        else
                        {
                            //save language details
                            AddStaffLanguage(_staffID);
                            _hrClass.LoadHRManagerMessageBox(2, "Employment language details have been successfully saved!", this, PanelStaffRecord);
                            return;
                        }
                    }
                    else//update language details
                    {
                        int _StaffLanguageID = Convert.ToInt32(HiddenFieldLanguageID.Value);

                        //update the staff language details
                        UpdateStaffLanguage(_StaffLanguageID);//
                            _hrClass.LoadHRManagerMessageBox(2, "Employment language details have been successfully updated!", this, PanelStaffRecord);
                            return;                       
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
                return;
            }
        }
        //method for adding staff language
        private void AddStaffLanguage(Guid _staffID)
        {
            //add language

            StaffLanguage newStaffLanguage = new StaffLanguage();
            newStaffLanguage.StaffID = _staffID;
            newStaffLanguage.LanguageName = txtLanguageName.Text.Trim();
            newStaffLanguage.LanguageReading = Convert.ToInt32(txtLanguageReading.Text.Trim());
            newStaffLanguage.LanguageSpeaking = Convert.ToInt32(txtLanguageSpeaking.Text.Trim());
            newStaffLanguage.LanguageWriting = Convert.ToInt32(txtLanguageWriting.Text.Trim());
            db.StaffLanguages.InsertOnSubmit(newStaffLanguage);
       
            db.SubmitChanges();
            HiddenFieldLanguageID.Value = newStaffLanguage.StaffLanguageID.ToString();
             //display language details associated with the staff
            DisplayLanguages(_staffID);
            //clear the entry controls
            ClearStaffLanguageControls();
        }
        //method for updating language details
        private void UpdateStaffLanguage(int _StaffLanguageID)
        {
            //alter language details
            StaffLanguage updateStaffLanguage=db.StaffLanguages.Single(p=>p.StaffLanguageID==_StaffLanguageID);
            updateStaffLanguage.LanguageName = txtLanguageName.Text.Trim();
            updateStaffLanguage.LanguageReading = Convert.ToInt32(txtLanguageReading.Text.Trim());
            updateStaffLanguage.LanguageSpeaking = Convert.ToInt32(txtLanguageSpeaking.Text.Trim());
            updateStaffLanguage.LanguageWriting = Convert.ToInt32(txtLanguageWriting.Text.Trim());
            db.SubmitChanges();
            //refresh language details
            DisplayLanguages((Guid)updateStaffLanguage.StaffID);
          
            //clear the entry controls
            ClearStaffLanguageControls();
            return;
        }
        //save employment allowance details

        private void ClearStaffLanguageControls()
        {
            txtLanguageName.Text = "";
            txtLanguageReading.Text = "";
            txtLanguageSpeaking.Text = "";
            txtLanguageWriting.Text = "";
            btnAddLanguage.Text = "Save Language";
            HiddenFieldLanguageID.Value = "";
            return;
        }

        //load employee's language details for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvLanguage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditLanguage") == 0 || e.CommandName.CompareTo("DeleteLanguage") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFLanguageID = (HiddenField)gvLanguage.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditLanguage") == 0)//check if we are editing
                    {
                        LoadLanguageControlsForEdit(Convert.ToInt32(_HFLanguageID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteLanguage") == 0)//check if we are deleting
                    {
                        HiddenFieldLanguageID.Value = _HFLanguageID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, "Are you sure you what to delete the selected language?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }

        //load language details for edit
        private void LoadLanguageControlsForEdit(int _StaffLanguageID)
        {
            HiddenFieldLanguageID.Value = _StaffLanguageID.ToString();
            StaffLanguage getStaffLanguage = db.StaffLanguages.Single(p => p.StaffLanguageID == _StaffLanguageID);
            txtLanguageName.Text = getStaffLanguage.LanguageName.ToString();
            txtLanguageReading.Text = getStaffLanguage.LanguageReading.ToString();
            txtLanguageSpeaking.Text = getStaffLanguage.LanguageSpeaking.ToString();
            txtLanguageWriting.Text = getStaffLanguage.LanguageWriting.ToString();

            btnAddLanguage.Text = "Update Language";
            return;
        }
        //method for deleting a language detail
        private void DeleteLanguages()
        {
            try
            {
               int _StaffLanguageID = Convert.ToInt32(HiddenFieldLanguageID.Value);
                StaffLanguage deleteStaffLanguage = db.StaffLanguages.Single(p => p.StaffLanguageID == _StaffLanguageID);
                db.StaffLanguages.DeleteOnSubmit(deleteStaffLanguage);        
                db.SubmitChanges();
                //refresh language after delete is done
                DisplayLanguages((Guid)deleteStaffLanguage.StaffID);
                _hrClass.LoadHRManagerMessageBox(2, "Employee language detail has been deleted!", this, PanelStaffRecord);
                ClearStaffLanguageControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //delete the language details
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteLanguages();
            return;
        }
        #endregion

        #region "STAFF ALLERGIES"
        private void DisplayAllergies(Guid _staffID)//Display Staff allergies
        {
            try
            {
                object _display;
                _display = db.PROC_StaffAllergies(_staffID);
                gvAllergy.DataSourceID = null;
                gvAllergy.DataSource = _display;
                gvAllergy.DataBind();
                return;
            }
            catch { }
        }

        //validate add allergy details
        private string ValidateStaffAllergy()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Allergy details save failed. Enter staff's personal information or search for the staff before adding his/her allergy details!";
            else if (txtAllergyName.Text.Trim() == "")
                return "Enter the allergy name!";
            else return "";
        }
        protected void btnAddAllergy_Click(object sender, EventArgs e)
        {
            SaveAllergies(); return;//
        }

        //method for saving allergies
        private void SaveAllergies()
        {
            try
            {
                string _error = ValidateStaffAllergy();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _Allergy = txtAllergyName.Text.Trim();
                    if (btnAddAllergy.Text.ToLower() == "save allergy")
                    {
                        //check if the selected allergy is selected against the staff
                        if (db.StaffAllergies.Any(p=>p.AllergyDetails==_Allergy))                        
                        {
                             _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected allergy has already been saved!", this, PanelStaffRecord);
                            return;
                        }
                        else
                        {
                            //save allergy details
                            AddStaffAllergy(_staffID);
                            _hrClass.LoadHRManagerMessageBox(2, "Allergy details have been successfully saved!", this, PanelStaffRecord);
                            return;
                        }
                    }
                    else//update allergy details
                    {
                        int _StaffAllergyID = Convert.ToInt32(HiddenFieldAllergyID.Value);

                        //update the staff allergy details
                        UpdateStaffAllergy(_StaffAllergyID);//
                        _hrClass.LoadHRManagerMessageBox(2, "Allergy details have been successfully updated!", this, PanelStaffRecord);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
                return;
            }
        }
        //method for adding staff allergy
        private void AddStaffAllergy(Guid _staffID)
        {
            //add allergy
            StaffAllergy newStaffAllergy = new StaffAllergy();
            newStaffAllergy.StaffID = _staffID;
            newStaffAllergy.AllergyDetails = txtAllergyName.Text.Trim();
            db.StaffAllergies.InsertOnSubmit(newStaffAllergy);

            db.SubmitChanges();
            HiddenFieldAllergyID.Value = newStaffAllergy.StaffAllergyID.ToString();
             //display allergy details associated with the staff
            DisplayAllergies(_staffID);
            //clear the entry controls
            ClearStaffAllergyControls();
        }
        //method for updating allergy details
        private void UpdateStaffAllergy(int _StaffAllergyID)
        {
            //alter allergy details
            StaffAllergy updateStaffAllergy = db.StaffAllergies.Single(p => p.StaffAllergyID == _StaffAllergyID);
            updateStaffAllergy.AllergyDetails = txtAllergyName.Text.Trim();         
            db.SubmitChanges();
            //refresh allergy details
            DisplayAllergies((Guid)updateStaffAllergy.StaffID);

            //clear the entry controls
            ClearStaffAllergyControls();
            return;
        }
        //save employment allowance details

        private void ClearStaffAllergyControls()
        {
            txtAllergyName.Text = "";
            btnAddAllergy.Text = "Save Allergy";
            HiddenFieldAllergyID.Value = "";
            return;
        }

        //load allergy for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvAllergy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditAllergy") == 0 || e.CommandName.CompareTo("DeleteAllergy") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFAllergyID = (HiddenField)gvAllergy.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditAllergy") == 0)//check if we are editing
                    {
                        LoadAllergyControlsForEdit(Convert.ToInt32(_HFAllergyID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteAllergy") == 0)//check if we are deleting
                    {
                        HiddenFieldAllergyID.Value = _HFAllergyID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmDeleteAllergy, "Are you sure you what to delete the selected allergy?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }

        //load Allergy details for edit
        private void LoadAllergyControlsForEdit(int _StaffAllergyID)
        {
            HiddenFieldAllergyID.Value = _StaffAllergyID.ToString();
            StaffAllergy getStaffAllergy = db.StaffAllergies.Single(p => p.StaffAllergyID == _StaffAllergyID);
            txtAllergyName.Text = getStaffAllergy.AllergyDetails.ToString();

            btnAddAllergy.Text = "Update Allergy";
            return;
        }
        //method for deleting an Allergy detail
        private void DeleteAllergies()
        {
            try
            {
                int _StaffAllergyID = Convert.ToInt32(HiddenFieldAllergyID.Value);
                StaffAllergy deleteStaffAllergy = db.StaffAllergies.Single(p => p.StaffAllergyID == _StaffAllergyID);
                db.StaffAllergies.DeleteOnSubmit(deleteStaffAllergy);
                db.SubmitChanges();
                //refresh Allergy after delete is done
                DisplayAllergies((Guid)deleteStaffAllergy.StaffID);
               
                _hrClass.LoadHRManagerMessageBox(2, "Employee allergy detail has been deleted!", this, PanelStaffRecord);
                ClearStaffAllergyControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //delete the Allergy details
        protected void ucConfirmDeleteAllergy_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
            DeleteAllergies();
            return;
        }
      #endregion
        #region "STAFF CHRONIC CONDITIONS"
        private void DisplayChronicConditions(Guid _staffID)//Display Staff chronic conditions
        {
            try
            {
                object _display;
                _display = db.PROC_StaffChronicConditions(_staffID);
                gvChronicCondition.DataSourceID = null;
                gvChronicCondition.DataSource = _display;
                gvChronicCondition.DataBind();
                return;
            }
            catch { }

             }
        //validate add chronic condition details
        private string ValidateStaffChronicCondition()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Chronic conditions save failed. Enter staff's personal information or search for the staff before adding his/her chronic condition details!";
            else if (txtChronicConditionName.Text.Trim() == "")
                return "Enter the chronic condition!";
            else return "";
        }

        protected void btnAddChronicCondition_Click(object sender, EventArgs e)
        {
            SaveChronicConditions(); return;//
        }
        //method for saving ChronicConditions
        private void SaveChronicConditions()
        {
            try
            {
                string _error = ValidateStaffChronicCondition();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    string _ChronicCondition = txtChronicConditionName.Text.Trim();
                    if (btnAddChronicCondition.Text.ToLower() == "save chronic condition")
                    {
                        //check if the selected ChronicCondition is selected against the staff
                        if (db.StaffChronicConditions.Any(p =>p.StaffChronicConditionDetails== _ChronicCondition))
                        {
                            _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected chronic condition has already been saved!", this, PanelStaffRecord);
                            return;
                        }
                        else
                        {
                            //save ChronicCondition details
                            AddStaffChronicCondition(_staffID);
                            _hrClass.LoadHRManagerMessageBox(2, "Chronic condition details have been successfully saved!", this, PanelStaffRecord);
                            return;
                        }
                    }
                    else//update ChronicCondition details
                    {
                        int _StaffChronicConditionID = Convert.ToInt32(HiddenFieldChronicConditionID.Value);

                        //update the staff ChronicCondition details
                        UpdateStaffChronicCondition(_StaffChronicConditionID);//
                        _hrClass.LoadHRManagerMessageBox(2, "Employee chronic condition details have been successfully updated!", this, PanelStaffRecord);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
                return;
            }
        }
        //method for adding staff ChronicCondition
        private void AddStaffChronicCondition(Guid _staffID)
        {
            //add ChronicCondition
            StaffChronicCondition newStaffChronicCondition = new StaffChronicCondition();
            newStaffChronicCondition.StaffID = _staffID;
            newStaffChronicCondition.StaffChronicConditionDetails = txtChronicConditionName.Text.Trim();
            db.StaffChronicConditions.InsertOnSubmit(newStaffChronicCondition);

            db.SubmitChanges();
            HiddenFieldChronicConditionID.Value = newStaffChronicCondition.StaffChronicConditionID.ToString();
            //display ChronicCondition details associated with the staff
            DisplayChronicConditions(_staffID);

            //clear the entry controls
            ClearStaffChronicConditionControls();
        }
        //method for updating ChronicCondition details
        private void UpdateStaffChronicCondition(int _StaffChronicConditionID)
        {
            //alter ChronicCondition details
            StaffChronicCondition updateStaffChronicCondition = db.StaffChronicConditions.Single(p => p.StaffChronicConditionID == _StaffChronicConditionID);
            updateStaffChronicCondition.StaffChronicConditionDetails = txtChronicConditionName.Text.Trim();
            db.SubmitChanges();
            //refresh ChronicCondition details
            DisplayChronicConditions((Guid)updateStaffChronicCondition.StaffID);

            //clear the entry controls
            ClearStaffChronicConditionControls();
            return;
        }
        //save employment allowance details

        private void ClearStaffChronicConditionControls()
        {
            txtChronicConditionName.Text = "";
            btnAddChronicCondition.Text = "Save Chronic Condition";
            HiddenFieldChronicConditionID.Value = "";
            return;
        }

        //load ChronicCondition for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvChronicCondition_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditChronicCondition") == 0 || e.CommandName.CompareTo("DeleteChronicCondition") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFChronicConditionID = (HiddenField)gvChronicCondition.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditChronicCondition") == 0)//check if we are editing
                    {
                        LoadChronicConditionControlsForEdit(Convert.ToInt32(_HFChronicConditionID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteChronicCondition") == 0)//check if we are deleting
                    {
                        HiddenFieldChronicConditionID.Value = _HFChronicConditionID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmDeleteChronicConditions, "Are you sure you what to delete the selected ChronicCondition?");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }

        //load ChronicCondition details for edit
        private void LoadChronicConditionControlsForEdit(int _StaffChronicConditionID)
        {
            HiddenFieldChronicConditionID.Value = _StaffChronicConditionID.ToString();
            StaffChronicCondition getStaffChronicCondition = db.StaffChronicConditions.Single(p => p.StaffChronicConditionID == _StaffChronicConditionID);
            txtChronicConditionName.Text = getStaffChronicCondition.StaffChronicConditionDetails.ToString();
            btnAddChronicCondition.Text = "Update Chronic Condition";
            return;
        }
        //method for deleting an ChronicCondition detail
        private void DeleteChronicConditions()
        {
            try
            {
                int _StaffChronicConditionID = Convert.ToInt32(HiddenFieldChronicConditionID.Value);
                StaffChronicCondition deleteStaffChronicCondition = db.StaffChronicConditions.Single(p => p.StaffChronicConditionID == _StaffChronicConditionID);
                db.StaffChronicConditions.DeleteOnSubmit(deleteStaffChronicCondition);
                db.SubmitChanges();
                //refresh ChronicCondition after delete is done
                DisplayChronicConditions((Guid)deleteStaffChronicCondition.StaffID);

                _hrClass.LoadHRManagerMessageBox(2, "Employee Chronic Condition detail has been deleted!", this, PanelStaffRecord);
                ClearStaffChronicConditionControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //delete the ChronicConditions details
        protected void ucConfirmDeleteChronicConditions_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
           DeleteChronicConditions();
            return;
        }
        #endregion
        #region "STAFF BANK DETAILS"
        private void DisplayBankDetails(Guid _staffID)//Display Staff Bank Details
        {
            try
            {
                object _display;
                _display = db.PROC_StaffBankDetails(_staffID);
                gvBankDetails.DataSourceID = null;
                gvBankDetails.DataSource = _display;
                gvBankDetails.DataBind();
                return;
            }
            catch { }
        }
        //validate add employment bank details
        private string ValidateStaffBankDetails()
        {
            if (HiddenFieldStaffID.Value == "")
                return "Bank details save failed. Enter staff's personal information or search for the staff before adding his/her bank details!";
            else if (txtBankName.Text.Trim() == "")
                return "Enter the bank name!";
            else if (txtBankBranch.Text.Trim() == "")
                return "Enter the bank branch!";
            else if (txtAccountName.Text.Trim() == "")
                return "Enter the account name!";
            else if (txtAccountNumber.Text.Trim() == "")
                return "Enter the account number.!";
            else if (txtCurrencyPercentage.Text.Trim() == "")
                return "Enter the currency percentage.!";
            else if (_hrClass.isNumberValid(txtCurrencyPercentage.Text.Trim()) == false)
                return "Enter a valid currency percentage";
            else return "";
        }

        protected void btnAddBankDetails_Click(object sender, EventArgs e)
        {
            SaveBankDetails(); return;//
        }
        //method for saving bank details
        private void SaveBankDetails()
        {
            try
            {
                string _error = ValidateStaffBankDetails();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);

                    if (btnAddBankDetails.Text.ToLower() == "save bank details")
                    {
                             //save bank details
                            AddBankDetails(_staffID);
                            _hrClass.LoadHRManagerMessageBox(2, "Bank details have been successfully saved!", this, PanelStaffRecord);
                            return;
                       
                    }
                    else//update bank details
                    {
                        int _StaffBankDetailsID = Convert.ToInt32(HiddenFieldBankDetailID.Value);
                       
                        //update the staff bank details
                        UpdateBankDetails(_StaffBankDetailsID);//
                        _hrClass.LoadHRManagerMessageBox(2, "bank details have been successfully updated!", this, PanelStaffRecord);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
                return;
            }
        }
        //method for adding bank details
        private void AddBankDetails(Guid _staffID)
        {
            //add bank details
            
            StaffBankDetails newStaffBankDetails = new StaffBankDetails();
            newStaffBankDetails.StaffID = _staffID;
            newStaffBankDetails.BankName = txtBankName.Text.Trim();
            newStaffBankDetails.BankBranch = txtBankBranch.Text.Trim();
            newStaffBankDetails.AccountName = txtAccountName.Text.Trim();
            newStaffBankDetails.AccountNumber = txtAccountNumber.Text.Trim();
            newStaffBankDetails.CurrencyPercentage = Convert.ToInt32(txtCurrencyPercentage.Text.Trim());
            db.StaffBankDetails.InsertOnSubmit(newStaffBankDetails);

            db.SubmitChanges();
            HiddenFieldBankDetailID.Value = newStaffBankDetails.StaffBankDetailsID.ToString();
        
            //display bank details associated with the staff
            DisplayBankDetails(_staffID);
            //clear the entry controls
            ClearStaffBankDetailControls();
         }
        //method for updating bank details
        private void UpdateBankDetails(int _StaffBankDetailsID)
        {
            //alter bank details
            StaffBankDetails updateStaffBankDetail = db.StaffBankDetails.Single(p => p.StaffBankDetailsID == _StaffBankDetailsID);
            updateStaffBankDetail.BankName = txtBankName.Text.Trim();
            updateStaffBankDetail.BankBranch = txtBankBranch.Text.Trim();
            updateStaffBankDetail.AccountName = txtAccountName.Text.Trim();
            updateStaffBankDetail.AccountNumber = txtAccountNumber.Text.Trim();
            updateStaffBankDetail.CurrencyPercentage = Convert.ToInt32(txtCurrencyPercentage.Text.Trim());
           
            db.SubmitChanges();
            //refresh bank details
            DisplayBankDetails((Guid)updateStaffBankDetail.StaffID);
          
            //clear the entry controls
            ClearStaffBankDetailControls();
            return;
        }
        //save employment allowance details

        private void ClearStaffBankDetailControls()
        {
            txtBankName.Text = "";
            txtBankBranch.Text = "";
            txtAccountName.Text = "";
            txtAccountNumber.Text = "";
            txtCurrencyPercentage.Text = "";
            btnAddBankDetails.Text = "Save Bank Details";
            HiddenFieldLanguageID.Value = "";
            return;
        }

        //load employee's language details for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        protected void gvBankDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditBankDetails") == 0 || e.CommandName.CompareTo("DeleteBankDetails") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFBankDetailsID = (HiddenField)gvLanguage.Rows[ID].FindControl("HiddenField1");
                    if (e.CommandName.CompareTo("EditBankDetails") == 0)//check if we are editing
                    {
                        LoadBankDetailsForEdit(Convert.ToInt32(_HFBankDetailsID.Value));
                        return;
                    }
                    if (e.CommandName.CompareTo("DeleteBankDetails") == 0)//check if we are deleting
                    {
                        HiddenFieldBankDetailID.Value = _HFBankDetailsID.Value;
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmDeleteBank, "Are you sure you what to delete the selected bank details?");
                        return;                        
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }

        //load language details for edit
        private void LoadBankDetailsForEdit(int _StaffBankDetailsID)
        {
            HiddenFieldBankDetailID.Value = _StaffBankDetailsID.ToString();
            StaffBankDetails getStaffBankDetail = db.StaffBankDetails.Single(p => p.StaffBankDetailsID == _StaffBankDetailsID);
            txtBankName.Text = getStaffBankDetail.BankName.ToString();
            txtBankBranch.Text = getStaffBankDetail.BankBranch.ToString();
            txtAccountName.Text = getStaffBankDetail.AccountName.ToString();
            txtAccountNumber.Text = getStaffBankDetail.AccountNumber.ToString();
            btnAddBankDetails.Text = "Update Bank Details";  
            return;
        }
        //method for deleting a language detail
        private void DeleteBankDetails()
        {
            try
            {
                int _StaffBankDetailsID = Convert.ToInt32(HiddenFieldBankDetailID.Value);
                StaffBankDetails deleteStaffBankDetail = db.StaffBankDetails.Single(p => p.StaffBankDetailsID == _StaffBankDetailsID);
                db.StaffBankDetails.DeleteOnSubmit(deleteStaffBankDetail);                
                db.SubmitChanges();
                //refresh bank detail after delete is done
                DisplayBankDetails((Guid)deleteStaffBankDetail.StaffID);
                
                _hrClass.LoadHRManagerMessageBox(2, "Employee bank detail record has been deleted!", this, PanelStaffRecord);
                ClearStaffBankDetailControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
                return;
            }
        }
        //delete the bank details
        protected void ucConfirmDeleteBank_lnkBtnYesConfirmationClick(object sender, EventArgs e)
        {
            DeleteBankDetails();
            return;
        }

        #endregion
        //#region "STAFF WORK EXPERIENCE"
        ////clear add work experience controls
        //private void ClearAddWorkExperienceControls()
        //{
        //    //clear entry controls
        //    _hrClass.ClearEntryControls(PanelAddWorkExperience);
        //    HiddenFieldWorkExperienceID.Value = "";
        //    lnkBtnSaveWorkExperience.Visible = true;
        //    lnkBtnUpdateWorkExperience.Enabled = false;
        //    lnkBtnUpdateWorkExperience.Visible = true;
        //    lnkBtnClearWorkExperience.Visible = true;
        //    lbAddWorkExperienceHeader.Text = "Add New Work Experience";
        //    ImageAddWorkExperienceHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        //    lbWorkExperienceError.Text = "";
        //    lbWorkExperienceInfo.Text = "";
        //}
        ////load ad new work experience record
        //protected void LinkButtonNewWorkExperience_Click(object sender, EventArgs e)
        //{
        //    _hrClass.GenerateMonths(ddlWorkExperienceStartDateMonth);//start date months
        //    _hrClass.GenerateYears(ddlWorkExperienceStartDateYear);//start date years
        //    _hrClass.GenerateMonths(ddlWorkExperienceEndDateMonth);//end date months
        //    _hrClass.GenerateYears(ddlWorkExperienceEndDateYear);//end date years
        //    ClearAddWorkExperienceControls();
        //    ModalPopupExtenderAddWorkExperience.Show();
        //    return;
        //}
        ////validate save satff record work experience
        //private string ValidateStaffWorkExperienceControls()
        //{
        //    if (HiddenFieldStaffID.Value == "")

        //        return "Save failed. Save staff personal information first for you to be able to add his/her work experience details!";
        //    if (txtWorkExperienceEmployerName.Text.Trim() == "")
        //    {
        //        txtWorkExperienceEmployerName.Focus();
        //        return "Enter employer's name!";
        //    }
        //    else if (txtWorkExperiencePositionHeld.Text.Trim() == "")
        //    {
        //        txtWorkExperiencePositionHeld.Focus();
        //        return "Enter posistion held !";
        //    }
        //    else if (ddlWorkExperienceStartDateMonth.SelectedIndex == 0)
        //    {
        //        ddlWorkExperienceStartDateMonth.Focus();
        //        return "Select start date month!";
        //    }
        //    else if (ddlWorkExperienceStartDateYear.SelectedIndex == 0)
        //    {
        //        ddlWorkExperienceStartDateYear.Focus();
        //        return "Select start date year!";
        //    }
        //    else if (ddlWorkExperienceEndDateMonth.SelectedIndex == 0)
        //    {
        //        ddlWorkExperienceEndDateMonth.Focus();
        //        return "Select end date month!";
        //    }
        //    else if (ddlWorkExperienceEndDateMonth.SelectedIndex == 0)
        //    {
        //        ddlWorkExperienceEndDateMonth.Focus();
        //        return "Select end date year!";
        //    }
        //    else if (db.PROC_ValidatesDates(ddlWorkExperienceStartDateMonth.SelectedValue, ddlWorkExperienceStartDateYear.SelectedValue, ddlWorkExperienceEndDateMonth.SelectedValue, ddlWorkExperienceEndDateYear.SelectedValue).FirstOrDefault().Column1 == 0)
        //    {
        //        return "Invalid Start and End dates selected.<br>End date should be greater than start date!";
        //    }
        //    else return "";

        //}
        ////save staff work experience details
        //private void SaveStaffWorkWorkExperience()
        //{
        //    ModalPopupExtenderAddWorkExperience.Show();
        //    try
        //    {
        //        //check for errors
        //        string _error = ValidateStaffWorkExperienceControls();
        //        if (_error != "")
        //        {
        //            lbWorkExperienceError.Text = _error;
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            StaffWorkExperience newWorkExperience = new StaffWorkExperience();
        //            newWorkExperience.StaffID = _staffID;
        //            newWorkExperience.EmployerName = txtWorkExperienceEmployerName.Text.Trim();
        //            newWorkExperience.PositionHeld = txtWorkExperiencePositionHeld.Text.Trim();
        //            newWorkExperience.StartDateMonth = ddlWorkExperienceStartDateMonth.SelectedValue;
        //            newWorkExperience.StartDateYear = ddlWorkExperienceStartDateYear.SelectedValue;
        //            newWorkExperience.EndDateMonth = ddlWorkExperienceEndDateMonth.SelectedValue;
        //            newWorkExperience.EndDateYear = ddlWorkExperienceEndDateYear.SelectedValue;
        //            db.StaffWorkExperiences.InsertOnSubmit(newWorkExperience);
        //            db.SubmitChanges();
        //            lbWorkExperienceError.Text = "";
        //            HiddenFieldWorkExperienceID.Value = newWorkExperience.StaffWorkExperienceID.ToString();
        //            lnkBtnUpdateWorkExperience.Enabled = true;
        //            lbWorkExperienceInfo.Text = "Staff work experience record has been successfully saved!";
        //            //display work experience
        //            DisplayWorkExperience();
        //            return;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        lbWorkExperienceError.Text = "Save failed . Please try again!";
        //        return;
        //    }
        //}
        ////update staff work experience details
        //private void UpdateStaffWorkWorkExperience()
        //{
        //    ModalPopupExtenderAddWorkExperience.Show();
        //    try
        //    {
        //        //check for errors
        //        string _error = ValidateStaffWorkExperienceControls();
        //        if (_error != "")
        //        {
        //            lbWorkExperienceError.Text = _error;
        //            return;
        //        }
        //        else
        //        {
        //            int _staffWorkExpereinceID = Convert.ToInt32(HiddenFieldWorkExperienceID.Value);
        //            StaffWorkExperience updateWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _staffWorkExpereinceID);
        //            updateWorkExperience.EmployerName = txtWorkExperienceEmployerName.Text.Trim();
        //            updateWorkExperience.PositionHeld = txtWorkExperiencePositionHeld.Text.Trim();
        //            updateWorkExperience.StartDateMonth = ddlWorkExperienceStartDateMonth.SelectedValue;
        //            updateWorkExperience.StartDateYear = ddlWorkExperienceStartDateYear.SelectedValue;
        //            updateWorkExperience.EndDateMonth = ddlWorkExperienceEndDateMonth.SelectedValue;
        //            updateWorkExperience.EndDateYear = ddlWorkExperienceEndDateYear.SelectedValue;
        //            db.SubmitChanges();
        //            lbWorkExperienceError.Text = "";
        //            //display work experience records
        //            DisplayWorkExperience();
        //            lbWorkExperienceInfo.Text = "Staff work experience record has been successfully updated!";

        //            return;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        lbWorkExperienceError.Text = "Save failed . Please try again!";
        //        return;
        //    }
        //}
        ////save staff work experience record
        //protected void lnkBtnSaveWorkExperience_Click(object sender, EventArgs e)
        //{
        //    SaveStaffWorkWorkExperience();
        //    return;
        //}
        ////update staff work experience record
        //protected void lnkBtnUpdateWorkExperience_Click(object sender, EventArgs e)
        //{
        //    UpdateStaffWorkWorkExperience();
        //    return;
        //}
        //protected void lnkBtnClearWorkExperience_Click(object sender, EventArgs e)
        //{
        //    ClearAddWorkExperienceControls();
        //    ModalPopupExtenderAddWorkExperience.Show();
        //}
        ////check if a staff is selected before edit details controls are loaded
        //protected void LinkButtonEditWorkExperience_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvEmploymentHistory))
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            if (x > 1)
        //            {
        //                _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one recoed to edit at a time!", this, PanelStaffRecord);
        //                return;
        //            }
        //            foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;

        //                if (_cb.Checked)
        //                {
        //                    HiddenField _HFWorkExperienceID = (HiddenField)_grv.FindControl("HiddenField1");
        //                    //load edit work experience details
        //                    LoadEditStaffWorkExperienceControls(Convert.ToInt32(_HFWorkExperienceID.Value));
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "An occured while loading staff work experience details for edit! " + ex.Message.ToString(), this, PanelStaffRecord);
        //    }
        //}
        ////load work experience record for edit
        //private void LoadEditStaffWorkExperienceControls(int _staffWorkExperienceID)
        //{
        //    StaffWorkExperience getWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _staffWorkExperienceID);
        //    HiddenFieldWorkExperienceID.Value = _staffWorkExperienceID.ToString();
        //    txtWorkExperienceEmployerName.Text = getWorkExperience.EmployerName;
        //    txtWorkExperiencePositionHeld.Text = getWorkExperience.PositionHeld;
        //    _hrClass.GenerateMonths(ddlWorkExperienceStartDateMonth);//start date months
        //    _hrClass.GenerateYears(ddlWorkExperienceStartDateYear);//start date years
        //    _hrClass.GenerateMonths(ddlWorkExperienceEndDateMonth);//end date months
        //    _hrClass.GenerateYears(ddlWorkExperienceEndDateYear);//end date years
        //    ddlWorkExperienceStartDateMonth.SelectedValue = getWorkExperience.StartDateMonth;
        //    ddlWorkExperienceStartDateYear.SelectedValue = getWorkExperience.StartDateYear;
        //    ddlWorkExperienceEndDateMonth.SelectedValue = getWorkExperience.EndDateMonth;
        //    ddlWorkExperienceEndDateYear.SelectedValue = getWorkExperience.EndDateYear;
        //    lnkBtnSaveWorkExperience.Visible = false;
        //    lnkBtnClearWorkExperience.Visible = true;
        //    lnkBtnUpdateWorkExperience.Enabled = true;
        //    lbAddWorkExperienceHeader.Text = "Edit Work Experience";
        //    ImageAddWorkExperienceHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
        //    lbWorkExperienceError.Text = "";
        //    lbWorkExperienceInfo.Text = "";
        //    ModalPopupExtenderAddWorkExperience.Show();
        //}
        ////display staff work expreience details
        //private void DisplayWorkExperience()
        //{
        //    try
        //    {
        //        object _display;
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        _display = db.PROC_StaffWorkExperience(_staffID);
        //        gvEmploymentHistory.DataSourceID = null;
        //        gvEmploymentHistory.DataSource = _display;
        //        Session["gvEmploymentHistoryData"] = _display;
        //        gvEmploymentHistory.DataBind();
        //        return;
        //    }
        //    catch { }
        //}

        //protected void gvEmploymentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{

        //    gvEmploymentHistory.PageIndex = e.NewPageIndex;
        //    Session["gvEmploymentHistoryPageIndex"] = e.NewPageIndex;//get page index
        //    gvEmploymentHistory.DataSource = (object)Session["gvEmploymentHistoryData"];
        //    gvEmploymentHistory.DataBind();
        //    return;
        //}
        //protected void gvEmploymentHistory_OnLoad(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Session["gvEmploymentHistoryPageIndex"] == null)//check if page index is empty
        //        {
        //            gvEmploymentHistory.PageIndex = 0;
        //            return;
        //        }
        //        else
        //        {
        //            //get grid view's page index from the session
        //            int pageIndex = Convert.ToInt32(Session["gvEmploymentHistoryPageIndex"]);
        //            gvEmploymentHistory.PageIndex = pageIndex;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////load delete work experience details
        //protected void LinkButtonDeleteWorkExperience_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvEmploymentHistory))//check if there is any work experience record that is selected before delete is done
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Please select record(s) to delete!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int _selectedRecordCount = 0;
        //            foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) _selectedRecordCount++;
        //            }
        //            string _deleteMessage = "";
        //            if (_selectedRecordCount == 1)
        //                _deleteMessage = "Are you sure that you want to delete the selected work experience record?";
        //            else if (_selectedRecordCount > 1)
        //                _deleteMessage = "Are you sure that you want to delete the selected work experience records?";
        //            _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////delete work experience record
        //private void DeleteWorkExperience()
        //{
        //    try
        //    {
        //        int _selectedRecordCount = 0;
        //        foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
        //        {
        //            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //            if (_cb.Checked) _selectedRecordCount++;
        //            if (_cb.Checked)
        //            {
        //                HiddenField _HFWorkExperienceID = (HiddenField)_grv.FindControl("HiddenField1");
        //                int _workExperienceID = Convert.ToInt32(_HFWorkExperienceID.Value);
        //                StaffWorkExperience deleteWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _workExperienceID);
        //                db.StaffWorkExperiences.DeleteOnSubmit(deleteWorkExperience);
        //                db.SubmitChanges();
        //            }
        //        }
        //        //refresh work experience records after the delete is done
        //        DisplayWorkExperience();
        //        if (_selectedRecordCount == 1)
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected work experience record has been deleted.", this, PanelStaffRecord);
        //        else if (_selectedRecordCount > 1)//
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected work experience records have been deleted.", this, PanelStaffRecord);
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////delete records
        //protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        //{
        //    if (TabContainerStaffRecord.ActiveTabIndex == 1)//check if we are on work experience tab
        //        DeleteWorkExperience();//delete selected work experience record(s)
        //    //else if (TabContainerStaffRecord.ActiveTabIndex == 2)//check if we are on education background tab
        //    //    DeleteEducationBackground();//delect education record selected
        //    //else if (TabContainerStaffRecord.ActiveTabIndex == 3)//check if we are on the family details tab
        //    //{
        //    //    //check the family delete case
        //    //    if (Session["FamilyDeleteCase"].ToString() == "1")//check if were deleting spouse details
        //    //        DeleteSpouseDetails();//delete spouse
        //    //    else if (Session["FamilyDeleteCase"].ToString() == "2")//check if we are deleting child details
        //    //        DeleteChildDetails();//delete child
        //    //}
        //    else if (TabContainerStaffRecord.ActiveTabIndex == 4)//check if we are on the documents tab
        //        DeleteStaffDocument();//delete staff document
        //    //else if (TabContainerStaffRecord.ActiveTabIndex == 5)//check if we are on the employment tab
        //    //    DeleteEmploymentTermPerkDetails();
        //    return;
        //}
        //#endregion
        //#region "STAFF EDUCATION BACKGROUND"
        ////clear add education background controls
        //private void ClearAddEducationBackgroundControls()
        //{
        //    //clear entry controls
        //    _hrClass.ClearEntryControls(PanelAddEducationBackground);
        //    HiddenFieldEducationBackgroundID.Value = "";
        //    lnkBtnSaveEducationBackground.Visible = true;
        //    lnkBtnUpdateEducationBackground.Enabled = false;
        //    lnkBtnUpdateEducationBackground.Visible = true;
        //    lnkBtnClearEducationBackground.Visible = true;
        //    lbAddEducationBackgroundHeader.Text = "Add Education Background";
        //    ImageAddEducationBackgroundHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        //    lbEducationBackgroundError.Text = "";
        //    lbEducationBackgroundInfo.Text = "";
        //}
        ////load add education background
        //protected void LinkButtonNewEducationBackground_Click(object sender, EventArgs e)
        //{
        //    _hrClass.GenerateMonths(ddlEducationBackgroundCompletionDateMonth);//start date months
        //    _hrClass.GenerateYearsWithExtraYears(ddlEducationBackgroundCompletionDateYear);//start date years
        //    ClearAddEducationBackgroundControls();
        //    ModalPopupExtenderAddEducationBackground.Show();
        //    return;
        //}
        ////validate add education backgrofund record
        //private string ValidateAddEducationBackgroundControls()
        //{
        //    if (rblEducationBackgroundQualificationType.SelectedIndex == -1)
        //    {
        //        rblEducationBackgroundQualificationType.Focus();
        //        return "Select  qualificatin type!";
        //    }
        //    else if (txtEducationBackgroundInstitutionName.Text.Trim() == "")
        //    {
        //        txtEducationBackgroundInstitutionName.Focus();
        //        return "Enter institution name!";
        //    }
        //    else if (ddlEducationBackgroundLevelAttained.SelectedIndex == 0)
        //    {
        //        ddlEducationBackgroundLevelAttained.Focus();
        //        return "Select level attained!";
        //    }
        //    else if (txtEducationBackgroundCourse.Text.Trim() == "")
        //    {
        //        txtEducationBackgroundCourse.Focus();
        //        return "Enter education programme/course!";
        //    }
        //    else if (rblEducationBackgroundStatus.SelectedIndex == -1)
        //    {
        //        rblEducationBackgroundStatus.Focus();
        //        return "Select status";
        //    }
        //    else if (ddlEducationBackgroundCompletionDateMonth.SelectedIndex == 0)
        //    {
        //        ddlEducationBackgroundCompletionDateMonth.Focus();
        //        return "Select completion month!";
        //    }
        //    else if (ddlEducationBackgroundCompletionDateYear.SelectedIndex == 0)
        //    {
        //        ddlEducationBackgroundCompletionDateYear.Focus();
        //        return "Select completion year!";
        //    }
        //    else return "";
        //}
        ////method for saving education background 
        //private void SaveEducationBackground()
        //{
        //    ModalPopupExtenderAddEducationBackground.Show();
        //    try
        //    {
        //        lbEducationBackgroundInfo.Text = "";
        //        string _error = ValidateAddEducationBackgroundControls();//check for errors
        //        if (_error != "")
        //        {
        //            lbEducationBackgroundError.Text = _error;
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            string _qualificationType = rblEducationBackgroundQualificationType.SelectedValue;
        //            string _status = rblEducationBackgroundStatus.SelectedValue;
        //            string _institutionName = txtEducationBackgroundInstitutionName.Text.Trim();
        //            int _attainedLevelID = Convert.ToInt32(ddlEducationBackgroundLevelAttained.SelectedValue);
        //            string _programmeCourse = txtEducationBackgroundCourse.Text.Trim();
        //            string _completionMonth = ddlEducationBackgroundCompletionDateMonth.SelectedValue;
        //            string _completionYear = ddlEducationBackgroundCompletionDateYear.SelectedValue;
        //            string _gradeAttained = txtEducationBackgroundGrade.Text.Trim();
        //            //check if the record being saved already exists
        //            if (db.StaffEducationBackgrounds.Any(p => p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.ProgramCourse == _programmeCourse && p.CompletionMonth == _completionMonth && p.CompletionYear == _completionYear))
        //            {
        //                lbEducationBackgroundError.Text = "The education record that you are trying to save is already saved.";
        //                return;
        //            }
        //            else
        //            {
        //                //save the new education background record
        //                StaffEducationBackground newEducationBackground = new StaffEducationBackground();
        //                newEducationBackground.StaffID = _staffID;
        //                newEducationBackground.EducationQualificationType = _qualificationType;
        //                newEducationBackground.QualificationStatus = _status;
        //                newEducationBackground.InstitutionName = _institutionName;
        //                newEducationBackground.EducationalLevelID = _attainedLevelID;
        //                newEducationBackground.ProgramCourse = _programmeCourse;
        //                newEducationBackground.CompletionMonth = _completionMonth;
        //                newEducationBackground.CompletionYear = _completionYear;
        //                newEducationBackground.GradeAttained = _gradeAttained;
        //                db.StaffEducationBackgrounds.InsertOnSubmit(newEducationBackground);
        //                db.SubmitChanges();
        //                DisplayEducationBackground();//display education list
        //                lnkBtnUpdateEducationBackground.Enabled = true;
        //                lbEducationBackgroundError.Text = "";
        //                HiddenFieldEducationBackgroundID.Value = newEducationBackground.StaffEducationBackgroundID.ToString();
        //                lbEducationBackgroundInfo.Text = "Education background record has been saved successfully.";
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        lbEducationBackgroundError.Text = "Savel failed. Please try again!";
        //        return;
        //    }
        //}
        ////method for updating an education background record
        //private void UpdateEducationBackground()
        //{
        //    ModalPopupExtenderAddEducationBackground.Show();
        //    try
        //    {
        //        lbEducationBackgroundInfo.Text = "";
        //        string _error = ValidateAddEducationBackgroundControls();//check for errors
        //        if (_error != "")
        //        {
        //            lbEducationBackgroundError.Text = _error;
        //            return;
        //        }
        //        else
        //        {
        //            int _educationID = Convert.ToInt32(HiddenFieldEducationBackgroundID.Value);
        //            string _qualificationType = rblEducationBackgroundQualificationType.SelectedValue;
        //            string _status = rblEducationBackgroundStatus.SelectedValue;
        //            string _institutionName = txtEducationBackgroundInstitutionName.Text.Trim();
        //            int _attainedLevelID = Convert.ToInt32(ddlEducationBackgroundLevelAttained.SelectedValue);
        //            string _programmeCourse = txtEducationBackgroundCourse.Text.Trim();
        //            string _completionMonth = ddlEducationBackgroundCompletionDateMonth.SelectedValue;
        //            string _completionYear = ddlEducationBackgroundCompletionDateYear.SelectedValue;
        //            string _gradeAttained = txtEducationBackgroundGrade.Text.Trim();
        //            //check if the record being updated with already exists
        //            if (db.StaffEducationBackgrounds.Any(p => p.StaffEducationBackgroundID != _educationID && p.EducationQualificationType == _qualificationType && p.QualificationStatus == _status && p.InstitutionName.ToLower() == _institutionName.ToLower() && p.EducationalLevelID == _attainedLevelID && p.ProgramCourse == _programmeCourse && p.CompletionMonth == _completionMonth && p.CompletionYear == _completionYear))
        //            {
        //                lbEducationBackgroundError.Text = "The education record that you are trying to update with already saved.";
        //                return;
        //            }
        //            else
        //            {
        //                //update the education background record
        //                StaffEducationBackground updateEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _educationID);
        //                updateEducationBackground.EducationQualificationType = _qualificationType;
        //                updateEducationBackground.QualificationStatus = _status;
        //                updateEducationBackground.InstitutionName = _institutionName;
        //                updateEducationBackground.EducationalLevelID = _attainedLevelID;
        //                updateEducationBackground.ProgramCourse = _programmeCourse;
        //                updateEducationBackground.CompletionMonth = _completionMonth;
        //                updateEducationBackground.CompletionYear = _completionYear;
        //                updateEducationBackground.GradeAttained = _gradeAttained;
        //                db.SubmitChanges();
        //                DisplayEducationBackground();//display education list
        //                lnkBtnUpdateEducationBackground.Enabled = true;
        //                lbEducationBackgroundError.Text = "";
        //                lbEducationBackgroundInfo.Text = "Education background record has been updated successfully.";
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lbEducationBackgroundError.Text = "Update failed." + ex.Message.ToString() + " Please try again.";
        //        return;
        //    }
        //}
        ////save the education background details
        //protected void lnkBtnSaveEducationBackground_Click(object sender, EventArgs e)
        //{
        //    SaveEducationBackground(); return;
        //}
        ////update the education background details
        //protected void lnkBtnUpdateEducationBackground_Click(object sender, EventArgs e)
        //{
        //    UpdateEducationBackground(); return;
        //}
        //protected void lnkBtnClearEducationBackground_Click(object sender, EventArgs e)
        //{
        //    ClearAddEducationBackgroundControls();
        //    ModalPopupExtenderAddEducationBackground.Show();
        //    return;
        //}
        ////check if a staff is selected before edit details controls are loaded
        //protected void LinkButtonEditEducationBackground_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvEducationBackground))
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvEducationBackground.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            if (x > 1)
        //            {
        //                _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one recoed to edit at a time!", this, PanelStaffRecord);
        //                return;
        //            }
        //            foreach (GridViewRow _grv in gvEducationBackground.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;

        //                if (_cb.Checked)
        //                {
        //                    HiddenField _HFEducationBackgroundID = (HiddenField)_grv.FindControl("HiddenField1");
        //                    //load edit education background details
        //                    LoadEditStaffEducationBackgroundControls(Convert.ToInt32(_HFEducationBackgroundID.Value));
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "An occured while loading staff education background details for edit! " + ex.Message.ToString(), this, PanelStaffRecord);
        //    }
        //}
        ////load education background record for edit
        //private void LoadEditStaffEducationBackgroundControls(int _staffEducationBackgroundID)
        //{
        //    StaffEducationBackground getEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _staffEducationBackgroundID);


        //    _hrClass.GenerateMonths(ddlEducationBackgroundCompletionDateMonth);//start date months
        //    _hrClass.GenerateYearsWithExtraYears(ddlEducationBackgroundCompletionDateYear);//start date years

        //    HiddenFieldEducationBackgroundID.Value = _staffEducationBackgroundID.ToString();
        //    rblEducationBackgroundQualificationType.SelectedValue = getEducationBackground.EducationQualificationType;
        //    txtEducationBackgroundInstitutionName.Text = getEducationBackground.InstitutionName;
        //    ddlEducationBackgroundLevelAttained.SelectedValue = getEducationBackground.EducationalLevelID.ToString();
        //    txtEducationBackgroundCourse.Text = getEducationBackground.ProgramCourse;
        //    rblEducationBackgroundStatus.SelectedValue = getEducationBackground.QualificationStatus;
        //    ddlEducationBackgroundCompletionDateMonth.SelectedValue = getEducationBackground.CompletionMonth;
        //    ddlEducationBackgroundCompletionDateYear.SelectedValue = getEducationBackground.CompletionYear;
        //    txtEducationBackgroundGrade.Text = getEducationBackground.GradeAttained;

        //    lnkBtnSaveEducationBackground.Visible = false;
        //    lnkBtnClearEducationBackground.Visible = true;
        //    lnkBtnUpdateEducationBackground.Enabled = true;
        //    lbAddEducationBackgroundHeader.Text = "Edit Education Background";
        //    ImageAddEducationBackgroundHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
        //    lbEducationBackgroundError.Text = "";
        //    lbEducationBackgroundInfo.Text = "";
        //    ModalPopupExtenderAddEducationBackground.Show();
        //}
        ////display staff education background details
        //private void DisplayEducationBackground()
        //{
        //    try
        //    {
        //        object _display;
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        _display = db.PROC_StaffEducationBackground(_staffID);
        //        gvEducationBackground.DataSourceID = null;
        //        gvEducationBackground.DataSource = _display;
        //        Session["gvEducationBackgroundData"] = _display;
        //        gvEducationBackground.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        //protected void gvEducationBackground_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{

        //    gvEducationBackground.PageIndex = e.NewPageIndex;
        //    Session["gvEducationBackgroundPageIndex"] = e.NewPageIndex;//get page index
        //    gvEducationBackground.DataSource = (object)Session["gvEducationBackgroundData"];
        //    gvEducationBackground.DataBind();
        //    return;
        //}
        //protected void gvEducationBackground_OnLoad(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Session["gvEducationBackgroundPageIndex"] == null)//check if page index is empty
        //        {
        //            gvEducationBackground.PageIndex = 0;
        //            return;
        //        }
        //        else
        //        {
        //            //get grid view's page index from the session
        //            int pageIndex = Convert.ToInt32(Session["gvEducationBackgroundPageIndex"]);
        //            gvEducationBackground.PageIndex = pageIndex;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////load delete education background details
        //protected void LinkButtonDeleteEducationBackground_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvEducationBackground))//check if there is any education background record that is selected before delete is done
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Please select record(s) to delete!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int _selectedRecordCount = 0;
        //            foreach (GridViewRow _grv in gvEducationBackground.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) _selectedRecordCount++;
        //            }
        //            string _deleteMessage = "";
        //            if (_selectedRecordCount == 1)
        //                _deleteMessage = "Are you sure that you want to delete the selected education background record?";
        //            else if (_selectedRecordCount > 1)
        //                _deleteMessage = "Are you sure that you want to delete the selected education background records?";
        //            _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////delete education background record
        //private void DeleteEducationBackground()
        //{
        //    try
        //    {
        //        int _selectedRecordCount = 0;
        //        foreach (GridViewRow _grv in gvEducationBackground.Rows)
        //        {
        //            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //            if (_cb.Checked) _selectedRecordCount++;
        //            if (_cb.Checked)
        //            {
        //                HiddenField _HFEducationBackgroundID = (HiddenField)_grv.FindControl("HiddenField1");
        //                int _educationBackgroundID = Convert.ToInt32(_HFEducationBackgroundID.Value);
        //                StaffEducationBackground deleteEducationBackground = db.StaffEducationBackgrounds.Single(p => p.StaffEducationBackgroundID == _educationBackgroundID);
        //                db.StaffEducationBackgrounds.DeleteOnSubmit(deleteEducationBackground);
        //                db.SubmitChanges();
        //            }
        //        }
        //        //refresh education background records after the delete is done
        //        DisplayEducationBackground();
        //        if (_selectedRecordCount == 1)
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected education background record has been deleted.", this, PanelStaffRecord);
        //        else if (_selectedRecordCount > 1)//
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected education background records have been deleted.", this, PanelStaffRecord);
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //#endregion
        //#region "STAFF FAMILY DETAILS"
        ////validate add spouse cnotrols
        //private string ValidateAddSpouseControls()
        //{
        //    if (HiddenFieldStaffID.Value.ToString() == "")
        //        return "Spouse save failed. Enter employee's basic information first before adding employee's spouse details!";
        //    else if (txtSpouseName.Text.Trim() == "")
        //        return "Enter employee's spouse name!";
        //    else if (txtSpouseDateOfBirth.Text != "" && (Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim()).Year >= DateTime.Now.Year || Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim()).Year > DateTime.Now.Year - 18))
        //        return "Invalid Employee's spouse Date of Birth selected!.Spouse date of birth cannot be current date or in future or less than 18 years. Select a valid date of birth for the spouse!";
        //    else if (txtSpouseEmailID.Text.Trim() != "" && _hrClass.IsValidEmail(txtSpouseEmailID.Text.Trim()) == false)
        //        return "Invalid spouse email ID entered! Check the email address entered!";
        //    return "";
        //}
        ////save employee's spouse details
        //private void SaveSpouseDetails()
        //{
        //    try
        //    {
        //        string _error = ValidateAddSpouseControls();//check if there are any errors
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else if (db.Spouses.Any(p => p.spouse_vSpouseName.ToLower() == txtSpouseName.Text.Trim().ToLower()))
        //        {
        //            txtSpouseName.Focus();
        //            _hrClass.LoadHRManagerMessageBox(1, "Employee's spouse you are trying to save already exists!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            //save spouse
        //            Spouse newSpouse = new Spouse();
        //            newSpouse.spouse_uiStaffID = _staffID;
        //            newSpouse.spouse_vSpouseName = txtSpouseName.Text.Trim();
        //            if (txtSpouseDateOfBirth.Text.Trim() != "")
        //            {
        //                newSpouse.spouse_dtDOB = Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim());
        //            }
        //            newSpouse.spouse_vEmployedAt = txtSpouseEmployedAt.Text.Trim();
        //            newSpouse.spouse_vEmploymentDetails = txtSpouseEmploymentDetails.Text.Trim();
        //            newSpouse.spouse_vPhoneNumber = txtSpouseTelephoneNumber.Text.Trim();
        //            newSpouse.spouse_vEmailID = txtSpouseEmailID.Text.Trim();
        //            newSpouse.spouse_bIsMedicalCovered = false;//set false for medical coverage
        //            db.Spouses.InsertOnSubmit(newSpouse);
        //            db.SubmitChanges();
        //            HiddenFieldSpouseID.Value = newSpouse.spouse_iSpouseID.ToString();
        //            _hrClass.LoadHRManagerMessageBox(2, "Employee's spouse details have been successfully saved!", this, PanelStaffRecord);
        //            btnUpdateSpouse.Enabled = true;
        //            //display employee's spouses
        //            DisplayStaffSpouse();
        //            //display spouses to see who wil benefit from the medical scheme
        //            DisplaySpouseForMedicalBenefit();
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////update employee's spouse details
        //private void UpdateSpouseDetails(short SpouseID)
        //{
        //    try
        //    {
        //        string _error = ValidateAddSpouseControls();//check if there are any errors
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else if (db.Spouses.Any(p => p.spouse_vSpouseName.ToLower() == txtSpouseName.Text.Trim().ToLower() && p.spouse_iSpouseID != SpouseID))
        //        {
        //            txtSpouseName.Focus();
        //            _hrClass.LoadHRManagerMessageBox(1, "Employee's spouse entered already exists!!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            //update spouse details
        //            Spouse updateSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == SpouseID);
        //            updateSpouse.spouse_vSpouseName = txtSpouseName.Text.Trim();
        //            if (txtSpouseDateOfBirth.Text.Trim() != "")
        //            {
        //                updateSpouse.spouse_dtDOB = Convert.ToDateTime(txtSpouseDateOfBirth.Text.Trim());
        //            }
        //            updateSpouse.spouse_vEmployedAt = txtSpouseEmployedAt.Text.Trim();
        //            updateSpouse.spouse_vEmploymentDetails = txtSpouseEmploymentDetails.Text.Trim();
        //            updateSpouse.spouse_vPhoneNumber = txtSpouseTelephoneNumber.Text.Trim();
        //            updateSpouse.spouse_vEmailID = txtSpouseEmailID.Text.Trim();
        //            db.SubmitChanges();
        //            _hrClass.LoadHRManagerMessageBox(2, "Spouse details have been successfully updated!", this, PanelStaffRecord);
        //            DisplayStaffSpouse();//display employee's spouse details
        //            //display spouses to see who wil benefit from the medical scheme
        //            DisplaySpouseForMedicalBenefit();
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////save spouse details
        //protected void btnSaveSpouse_OnClick(object sender, EventArgs e)
        //{
        //    SaveSpouseDetails();
        //    return;
        //}
        ////update spouse details
        //protected void btnUpdateSpouse_OnClick(object sender, EventArgs e)
        //{
        //    UpdateSpouseDetails(Convert.ToInt16(HiddenFieldSpouseID.Value));
        //    return;
        //}
        ////display staff spouse etails
        //private void DisplayStaffSpouse()
        //{
        //    try
        //    {
        //        object _display;
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        _display = db.PROC_StaffSpouse(_staffID);
        //        gvSpouseDetails.DataSourceID = null;
        //        gvSpouseDetails.DataSource = _display;
        //        gvSpouseDetails.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        ////load spouse details for edit when the edit button is clicked fron the grid view, or delete when the delete button is clicked
        //protected void gvSpouseDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName.CompareTo("EditSpouse") == 0 || e.CommandName.CompareTo("DeleteSpouse") == 0)
        //        {
        //            int ID = Convert.ToInt32(e.CommandArgument);
        //            HiddenField _HFSpouseID = (HiddenField)gvSpouseDetails.Rows[ID].FindControl("HiddenField1");
        //            int _spouseID = Convert.ToInt32(_HFSpouseID.Value);
        //            if (e.CommandName.CompareTo("EditSpouse") == 0)//check if we are editing
        //            {
        //                LoadSpouseDetailsForEdit(_spouseID);
        //                return;
        //            }
        //            if (e.CommandName.CompareTo("DeleteSpouse") == 0)//check if we are deleting
        //            {
        //                Session["DeleteSpouseID"] = _spouseID.ToString();
        //                string _deleteMessage = "Are you sure you want to delete the spouse?";
        //                _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
        //                Session["FamilyDeleteCase"] = 1;//set family delete case to 1 for deleting a spouse
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //private void LoadSpouseDetailsForEdit(int _spouseID)
        //{
        //    Spouse getSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _spouseID);
        //    HiddenFieldSpouseID.Value = _spouseID.ToString();
        //    txtSpouseName.Text = getSpouse.spouse_vSpouseName;
        //    if (getSpouse.spouse_dtDOB != null)
        //    {
        //        txtSpouseDateOfBirth.Text = _hrClass.ShortDateDayStart(getSpouse.spouse_dtDOB.ToString());
        //    }
        //    txtSpouseEmployedAt.Text = getSpouse.spouse_vEmployedAt;
        //    txtSpouseEmploymentDetails.Text = getSpouse.spouse_vEmploymentDetails;
        //    txtSpouseTelephoneNumber.Text = getSpouse.spouse_vPhoneNumber;
        //    txtSpouseEmailID.Text = getSpouse.spouse_vEmailID;
        //    btnUpdateSpouse.Enabled = true;
        //    return;
        //}
        ////delete employee's spouse
        //private void DeleteSpouseDetails()
        //{
        //    try
        //    {
        //        int _spouseID = Convert.ToInt32(Session["DeleteSpouseID"]);//get spouse id
        //        //delete spouse
        //        Spouse deleteSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == _spouseID);
        //        db.Spouses.DeleteOnSubmit(deleteSpouse);
        //        db.SubmitChanges();
        //        //refresh spouses
        //        DisplayStaffSpouse();
        //        _hrClass.LoadHRManagerMessageBox(2, "Employee's spouse has been deleted!", this, PanelStaffRecord);
        //        //clear add spouse details if spouse details in the add controls are details being deleted
        //        if (HiddenFieldSpouseID.Value != "" && Convert.ToInt32(HiddenFieldSpouseID.Value) == _spouseID)
        //        {
        //            ClearAddSpouseDetailsControls();
        //        }
        //        //display spouses to see who wil benefit from the medical scheme
        //        DisplaySpouseForMedicalBenefit();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////clear add spouse details controls
        //private void ClearAddSpouseDetailsControls()
        //{
        //    txtSpouseName.Text = "";
        //    txtSpouseDateOfBirth.Text = "";
        //    txtSpouseEmployedAt.Text = "";
        //    txtSpouseEmploymentDetails.Text = "";
        //    txtSpouseTelephoneNumber.Text = "";
        //    txtSpouseEmailID.Text = "";
        //    HiddenFieldSpouseID.Value = "";
        //}

        ////validate add staff's child details
        //private string ValidateAddStaffChildDetails()
        //{
        //    if (HiddenFieldStaffID.Value.ToString() == "")
        //        return "Child save failed. Enter staff's basic information first before adding his /her children!";
        //    else if (txtChildName.Text.Trim() == "")
        //        return "Enter employee's child name!";
        //    else if (txtChildDOB.Text.Trim() == "")
        //        return "Select child's date of birth by clicking the date button adjacent to the field!";
        //    else if (txtChildDOB.Text.Trim() != "" && Convert.ToDateTime(txtChildDOB.Text.Trim()) >= DateTime.Now)
        //        return "Invalid child date of birth selected!.<br>Child's date of birth cannot be current date or a future date. Select a valid date of birth for the child!";
        //    return "";
        //}
        ////save employee's child details
        //private void SaveChildDetails()
        //{
        //    try
        //    {
        //        string _error = ValidateAddStaffChildDetails();//check if any errors are found
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else if (db.Childs.Any(p => p.child_vName.ToLower() == txtChildName.Text.Trim().ToLower()))
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Staff's child entered already exists!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);//get staff master id
        //            //save child details
        //            Child newChild = new Child();
        //            newChild.child_uiStaffID = _staffID;
        //            newChild.child_vName = txtChildName.Text.Trim();
        //            if (txtChildDOB.Text.Trim() != "")
        //            {
        //                newChild.child_dtDOB = Convert.ToDateTime(txtChildDOB.Text.Trim());
        //            }
        //            newChild.child_bIsMedicalCovered = false;//set medical coverage to false 
        //            db.Childs.InsertOnSubmit(newChild);
        //            db.SubmitChanges();
        //            HiddenFieldChildID.Value = newChild.child_iChildID.ToString();
        //            btnUpdateChild.Enabled = true;
        //            //display employee's children
        //            DisplayStaffChildren();
        //            _hrClass.LoadHRManagerMessageBox(2, "Staff's child details has been successfully saved!", this, PanelStaffRecord);
        //            //display employee's  children to have medical benefit
        //            DisplayChildForMedicalBenefit();
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////update staff's child details
        //private void UpdateChildDetails(int _childID)
        //{
        //    try
        //    {
        //        string _error = ValidateAddStaffChildDetails();//check if any errors are found
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else if (db.Childs.Any(p => p.child_vName.ToLower() == txtChildName.Text.Trim().ToLower() && p.child_iChildID != _childID))
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "The child name you are trying to update with already exists!", this, PanelStaffRecord);
        //            txtChildName.Focus();
        //            return;
        //        }
        //        else
        //        {
        //            //update  child details
        //            Child updateChild = db.Childs.Single(p => p.child_iChildID == _childID);
        //            updateChild.child_vName = txtChildName.Text.Trim();
        //            if (txtChildDOB.Text.Trim() != "")
        //            {
        //                updateChild.child_dtDOB = Convert.ToDateTime(txtChildDOB.Text.Trim());
        //            }
        //            db.SubmitChanges();
        //            //display employee's children
        //            DisplayStaffChildren();
        //            _hrClass.LoadHRManagerMessageBox(2, "Staff's child details has been updated!", this, PanelStaffRecord);
        //            //display employee's  children to have medical benefit
        //            DisplayChildForMedicalBenefit();
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //protected void btnSaveChild_OnClick(object sender, EventArgs e)
        //{
        //    SaveChildDetails();
        //    return;
        //}
        //protected void btnUpdateChild_OnClick(object sender, EventArgs e)
        //{
        //    UpdateChildDetails(Convert.ToInt16(HiddenFieldChildID.Value));
        //    return;
        //}
        ////display employee's child details
        //public void DisplayStaffChildren()
        //{
        //    try
        //    {
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        object _display;
        //        _display = db.PROC_StaffChildren(_staffID);
        //        gvChildDetails.DataSourceID = null;
        //        gvChildDetails.DataSource = _display;
        //        gvChildDetails.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        ////
        //protected void gvChildDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName.CompareTo("EditChild") == 0 || e.CommandName.CompareTo("DeleteChild") == 0)
        //        {
        //            int ID = Convert.ToInt32(e.CommandArgument);
        //            GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
        //            GridView gdv = ((GridView)e.CommandSource);
        //            HiddenField _HFChildID = (HiddenField)gvChildDetails.Rows[ID].FindControl("HiddenField1");
        //            if (e.CommandName.CompareTo("EditChild") == 0)//check if we are editing
        //            {
        //                LoadChildDetailsForEdit(Convert.ToInt32(_HFChildID.Value));
        //                return;
        //            }
        //            if (e.CommandName.CompareTo("DeleteChild") == 0)//check if we are deleting
        //            {
        //                Session["DeleteChildID"] = _HFChildID.Value;

        //                string _deleteMessage = "Are you sure you what to delete the child?";
        //                _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
        //                Session["FamilyDeleteCase"] = 2;//set family delete case to 1 for deleting a child
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "An error occured." + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }

        //}
        //private void LoadChildDetailsForEdit(int _childID)
        //{
        //    Child getChild = db.Childs.Single(p => p.child_iChildID == _childID);
        //    HiddenFieldChildID.Value = _childID.ToString();
        //    txtChildName.Text = getChild.child_vName;
        //    if (getChild.child_dtDOB != null)
        //    {
        //        txtChildDOB.Text = _hrClass.ShortDateDayStart(getChild.child_dtDOB.ToString());
        //    }
        //    btnUpdateChild.Enabled = true;
        //}
        ////delete employee's child
        //private void DeleteChildDetails()
        //{
        //    try
        //    {

        //        int _childID = Convert.ToInt32(Session["DeleteChildID"]);//get child id
        //        //delete child
        //        Child deleteChild = db.Childs.Single(p => p.child_iChildID == _childID);
        //        db.Childs.DeleteOnSubmit(deleteChild);
        //        db.SubmitChanges();
        //        //refresh children
        //        DisplayStaffChildren();
        //        _hrClass.LoadHRManagerMessageBox(2, "Staff's child has been deleted!", this, PanelStaffRecord);
        //        //clear add child details if child details in the add controls are details being deleted
        //        if (HiddenFieldChildID.Value != "" && Convert.ToInt32(HiddenFieldChildID.Value) == _childID)
        //        {
        //            ClearAddChildDetailsControls();
        //        }
        //        //refresh children medical benefit details
        //        DisplayChildForMedicalBenefit();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////clear add child details controls
        //private void ClearAddChildDetailsControls()
        //{
        //    txtChildName.Text = "";
        //    txtChildDOB.Text = "";
        //    HiddenFieldChildID.Value = "";
        //}
        //#endregion
        //#region "STAFF DOCUMENTS"
        ////save staff document
        //private void UploadStaffDocument()
        //{
        //    //try
        //    //{
        //    if (txtStaffDocumentTitle.Text.Trim() == "")
        //    {
        //        lbDocumentsError.Text = "Enter document title!";
        //        txtStaffDocumentTitle.Focus(); return;
        //    }
        //    else if (FileUploadDocument.HasFile)
        //    {
        //        int _fileLength = FileUploadDocument.PostedFile.ContentLength;
        //        //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
        //        if (_hrClass.IsUploadedFileBig(_fileLength) == true)
        //        {
        //            lbDocumentsError.Text = "File uploaded exceeds the allowed limit of 3mb.";
        //            return;
        //        }
        //        else
        //        {
        //            string _uploadedDocFileName = FileUploadDocument.FileName;//get name of the uploaded file name
        //            string _fileExtension = Path.GetExtension(_uploadedDocFileName);

        //            _fileExtension = _fileExtension.ToLower();
        //            string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".rtf" };

        //            bool _isFileAccepted = false;
        //            foreach (string _acceptedFileExtension in _acceptedFileTypes)
        //            {
        //                if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
        //                    _isFileAccepted = true;
        //            }
        //            if (_isFileAccepted == false)
        //            {
        //                lbDocumentsError.Visible = true;
        //                lbDocumentsError.Text = "The file you are trying to upload is not a permitted file type!";
        //                return;
        //            }
        //            else
        //            {
        //                lbDocumentsError.Text = "";
        //                string _documentTitle = txtStaffDocumentTitle.Text.Trim();
        //                //save uploaded staff document
        //                SaveStaffDocument(FileUploadDocument, _documentTitle, _uploadedDocFileName, _fileExtension);
        //                return;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        lbDocumentsError.Text = "Browse for the staff document to upload!";
        //        return;
        //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord); return;
        //    //}
        //}

        ////save  staff document
        //private void SaveStaffDocument(FileUpload _fileUpload, string _documentTitle, string _uploadedDocFileName, string _fileExtension)
        //{

        //    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //    if (db.StaffDocs.Any(p => p.staffdoc_uiStaffID == _staffID && p.staffdoc_vDocTitle.ToLower() == _documentTitle.ToLower()))
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Save failed. Another document with the same title already exists!", this, PanelStaffRecord);
        //        return;
        //    }
        //    else
        //    {
        //        //save the document in the temp document
        //        _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
        //        string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
        //        lbDocumentsError.Text = "";
        //        Guid _loggedStaffMasterID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());

        //        //save employee document into the database
        //        FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
        //        //get document info
        //        FileInfo documentInfo = new FileInfo(_fileLocation);
        //        int fileSize = (int)documentInfo.Length;//get file size
        //        byte[] fileDocument = new byte[fileSize];
        //        fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

        //        //add the new document
        //        StaffDoc newStaffDocument = new StaffDoc();
        //        newStaffDocument.staffdoc_uiStaffID = _staffID;
        //        newStaffDocument.staffdoc_vDocTitle = _documentTitle;
        //        newStaffDocument.staffdoc_vDocName = _uploadedDocFileName;
        //        newStaffDocument.staffdoc_vDocExtension = _fileExtension;
        //        newStaffDocument.staffdoc_vbDocument = fileDocument;
        //        newStaffDocument.staffdoc_dtDateStored = DateTime.Now;
        //        newStaffDocument.staffdoc_uiStoredByID = _loggedStaffMasterID;
        //        db.StaffDocs.InsertOnSubmit(newStaffDocument);
        //        db.SubmitChanges();
        //        //dispaly staff documents
        //        DisplayStaffDocuments();
        //        _hrClass.LoadHRManagerMessageBox(2, "Staff document has been successfully uploaded!", this, PanelStaffRecord);


        //        //Guid _applicantID = Guid.Parse(Session["LoggedApplicantID"].ToString());
        //        //Applicant_Document newApplicantDocument = new Applicant_Document();
        //        //newApplicantDocument.ApplicantID = _applicantID;
        //        //newApplicantDocument.DocumentName = _newfileName;
        //        //newApplicantDocument.DocumentType = _fileType;//1 is for a CV document type,2 for cover leter
        //        //applicant_db.Applicant_Documents.InsertOnSubmit(newApplicantDocument);
        //        //applicant_db.SubmitChanges();
        //        ////update the document with the file name generated
        //        //Applicant_Document updateDocument = applicant_db.Applicant_Documents.Single(p => p.Applicant_DocumentID == newApplicantDocument.Applicant_DocumentID);
        //        ////create filename by using the name entered and the docunemt unique identifier
        //        //string _fileName = _newfileName + "_" + newApplicantDocument.Applicant_DocumentID.ToString() + _fileExtension;
        //        //string _documentPath = "../Applicant_Documents/" + _fileName;
        //        //updateDocument.DocumentPath = _documentPath;
        //        //applicant_db.SubmitChanges();

        //        ////check the document were are saving
        //        //if (_fileType == 1)//CV
        //        //{
        //        //    HiddenFieldCVID.Value = newApplicantDocument.Applicant_DocumentID.ToString();
        //        //    btnUploadCV.Text = "Upload New CV";
        //        //    HiddenFieldCVFilePath.Value = _documentPath;
        //        //    lnkBtnUploadedCV.Text = _newfileName + _fileExtension;
        //        //}
        //        //else if (_fileType == 2)//CV//cover letter
        //        //{
        //        //    HiddenFieldCoverLetterID.Value = newApplicantDocument.Applicant_DocumentID.ToString();
        //        //    btnUploadCoverLetter.Text = "Upload New Cover Letter";
        //        //    HiddenFieldCoverLetterPath.Value = _documentPath;
        //        //    lnkBtnUploadedCoverLetter.Text = _newfileName + _fileExtension;
        //        //}
        //        return;
        //    }
        //}
        //protected void btnUploadDocument_OnClick(object sender, EventArgs e)
        //{
        //    UploadStaffDocument(); return;
        //}
        ////display staff document details
        //private void DisplayStaffDocuments()
        //{
        //    try
        //    {
        //        object _display;
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        _display = db.PROC_StaffDocuments(_staffID);
        //        gvStaffDocuments.DataSourceID = null;
        //        gvStaffDocuments.DataSource = _display;
        //        gvStaffDocuments.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        ////load staff document details when the edit button is pressed or  delete when the delete button is clicked
        //protected void gvStaffDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName.CompareTo("EditDocument") == 0 || e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
        //        {
        //            int ID = Convert.ToInt32(e.CommandArgument);
        //            GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
        //            GridView gdv = ((GridView)e.CommandSource);

        //            HiddenField _HFStaffDocID = (HiddenField)gvStaffDocuments.Rows[ID].FindControl("HiddenField1");
        //            int _staffDocID = Convert.ToInt32(_HFStaffDocID.Value);
        //            if (e.CommandName.CompareTo("EditDocument") == 0)//check if we are editing the document
        //            {
        //                //load edit staff document details
        //                //LoadEditStaffDocumentDetails(_staffDocID);
        //                return;
        //            }
        //            else if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
        //            {
        //                HiddenFieldDocumentID.Value = _staffDocID.ToString();
        //                string _deleteMessage = "Are you sure you want to delete the document?";
        //                _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
        //            }
        //            else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
        //            {
        //                Session["HRDocumentViewCase"] = 1;//case 1 for viewing staff documents
        //                string strPageUrl = "HR_Document_Viewer.aspx?ref=" + _HFStaffDocID.Value;
        //                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return;
        //    }
        //}
        ////method for deleting a staff document
        //private void DeleteStaffDocument()
        //{
        //    try
        //    {
        //        int _staffDocumentID = Convert.ToInt32(HiddenFieldDocumentID.Value);
        //        StaffDoc deleteStaffDoc = db.StaffDocs.Single(p => p.staffdoc_iStaffDocID == _staffDocumentID);
        //        db.StaffDocs.DeleteOnSubmit(deleteStaffDoc);
        //        db.SubmitChanges();
        //        DisplayStaffDocuments();//reload staff documnets after deletion
        //        _hrClass.LoadHRManagerMessageBox(2, "Document selected has been successfully deleted.", this, PanelStaffRecord);
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //#endregion
        #region 'STAFF IMAGE UPLOADER'
        //load upload staff image controls
        private void LoadUploadStaffImageControls()
        {
            lblMsg.Text = "";
            imgUpload.ImageUrl = "";
            btnCrop.Visible = false;
            ModalPopupExtenderUploadStaffImage.Show();
            return;
        }
        protected void LinkButtonUploadStaffphoto_Click(object sender, EventArgs e)
        {
            LoadUploadStaffImageControls(); return;
        }
        //method for uploading staff image
        private void UploadStaffImage()
        {

            ModalPopupExtenderUploadStaffImage.Show();
            //try
            //{
            // Upload Original Image Here
            string uploadFileName = "";
            string uploadFilePath = "";
            if (FileUploadStaffPhoto.HasFile)
            {
                string ext = Path.GetExtension(FileUploadStaffPhoto.FileName).ToLower();
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".png")
                {
                    uploadFileName = Guid.NewGuid().ToString() + ext;
                    uploadFilePath = Path.Combine(Server.MapPath("~/UploadImages"), uploadFileName);
                    try
                    {
                        FileUploadStaffPhoto.SaveAs(uploadFilePath);

                        string _fileLocation = Server.MapPath("~/UploadImages/" + uploadFileName);
                        FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                        //get image info
                        FileInfo documentInfo = new FileInfo(_fileLocation);
                        int fileSize = (int)documentInfo.Length;//get file size
                        byte[] binaryImage = new byte[fileSize];
                        fs.Read(binaryImage, 0, fileSize);//read the document from the file stream
                        fs.Close();
                        HandleImageUpload(binaryImage, uploadFileName);

                        imgUpload.ImageUrl = "~/UploadImages/" + uploadFileName;
                        //panCrop.Visible = true;
                        btnCrop.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Text = "Error! Please try again.";
                    }
                }
                else
                {
                    lblMsg.Text = "Selected file type not allowed!";
                }
            }
            else
            {
                lblMsg.Text = "Please select file first!";
            }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Upload failed.<br>" + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
            //    return;
            //}
        }
        //upload the image
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            UploadStaffImage(); return;
        }
        //method for croping the image uploaded
        private void CropAndSaveUploadedStaffImage()
        {
            ModalPopupExtenderUploadStaffImage.Show();
            try
            {
                // Crop Image Here & Save
                string fileName = Path.GetFileName(imgUpload.ImageUrl);
                string filePath = Path.Combine(Server.MapPath("~/UploadImages"), fileName);
                string cropFileName = "";
                string cropFilePath = "";
                if (File.Exists(filePath))
                {
                    if (hfH.Value == "" && hfW.Value == "" && hfX.Value == "" && hfY.Value == "")
                    {
                        lblMsg.Text = "Crop an image sectopn to save has the staff photo!";
                        return;
                    }
                    else
                    {
                        System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                        Rectangle CropArea = new Rectangle(
                            Convert.ToInt32(hfX.Value),
                            Convert.ToInt32(hfY.Value),
                            Convert.ToInt32(hfW.Value),
                            Convert.ToInt32(hfH.Value));

                        try
                        {
                            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                            using (Graphics g = Graphics.FromImage(bitMap))
                            {
                                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                            }
                            cropFileName = "crop_" + fileName;
                            cropFilePath = Path.Combine(Server.MapPath("~/UploadImages"), cropFileName);
                            bitMap.Save(cropFilePath);

                            //get file streem of the croped image
                            FileStream fs = new FileStream(cropFilePath, FileMode.Open);//
                            //get document info
                            FileInfo documentInfo = new FileInfo(cropFilePath);
                            int fileSize = (int)documentInfo.Length;//get file size
                            byte[] fileDocument = new byte[fileSize];
                            fs.Read(fileDocument, 0, fileSize);//read the document from the file stream
                            fs.Close();

                            //save the photo in the staff master table
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            StaffMaster saveStaffPhoto = db.StaffMasters.Single(p => p.StaffID == _staffID);
                            saveStaffPhoto.StaffPhoto = fileDocument;
                            db.SubmitChanges();
                            ModalPopupExtenderUploadStaffImage.Hide();
                            _hrClass.LoadHRManagerMessageBox(2, "Staff photo has been successfully saved!", this, PanelStaffRecord);
                            //display staff photo uploaded
                            LoadStaffPhoto(_staffID);
                            return;
                            //Response.Redirect("~/UploadImages/" + cropFileName, false);
                        }

                        catch (Exception ex)
                        {
                            //throw;
                            lblMsg.Text = "Crop failed." + ex.Message.ToString();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while croping the uploaded image.<br>" + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
                return;
            }
        }
        //crop and save uploaded staff image
        protected void btnCrop_Click(object sender, EventArgs e)
        {
            CropAndSaveUploadedStaffImage(); return;
        }

        //resize the picrure uploaded to a fixed size
        public static System.Drawing.Image FixedSize(System.Drawing.Image image, int Width, int Height, bool needToFill)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;
            //check if the width is greater
            if (sourceWidth > Width && sourceHeight > Height)
            {
                nScaleW = ((double)Width / (double)sourceWidth);
                nScaleH = ((double)Height / (double)sourceHeight);
            }
            else//less photo width, set scale to 1
            {
                nScaleW = 1;
                nScaleH = 1;
            }
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);

            System.Drawing.Bitmap bmPhoto = null;
            try
            {
                bmPhoto = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, Width, Height), ex);
            }
            using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;

                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);

                return bmPhoto;
            }
        }

        private MemoryStream BytearrayToStream(byte[] arr)
        {
            return new MemoryStream(arr, 0, arr.Length);
        }

        private void HandleImageUpload(byte[] binaryImage, string _fileName)
        {
            //using (System.Drawing.Image img = RezizeImage(System.Drawing.Image.FromStream(BytearrayToStream(binaryImage)), 305, 372))
            using (System.Drawing.Image img = FixedSize(System.Drawing.Image.FromStream(BytearrayToStream(binaryImage)), 305, 372, true))
            {
                img.Save(Server.MapPath("~/UploadImages/" + _fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
            }

        }
        //load staff photo by checking if the employee has a photo
        private void LoadStaffPhoto(Guid _staffID)
        {
            PROC_GetStaffPhotoResult getStaffPhoto = db.PROC_GetStaffPhoto(_staffID).FirstOrDefault();
            if (getStaffPhoto.StaffPhoto != null)
            {
                ImageStaffPhoto.ImageUrl = "~/HRManagerClasses/HRManagerHandler.ashx?staffID=" + _staffID;
                LinkButtonUploadStaffphoto.Text = "Change Staff Photo";
            }
            else
            {
                LinkButtonUploadStaffphoto.Text = "Upload Staff Photo";
                ImageStaffPhoto.ImageUrl = "~/images/background/person.png";
            }
        }
        #endregion
        #region 'SEARCH FOR A STAFF'
        //SEARCH FOR A STAFF

        //search for staff by staff reference number
        private void SearchForStaffByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no staff found with the reference number entered.", this, PanelStaffRecord);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffRecord);
                return;
            }
        }
        //search for staff by staff reference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByStaffReference();
            return;
        }
        //search for staff by staff name
        private void SearchForStaffByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelStaffRecord);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffRecord);
                return;
            }
        }
        //search for staff by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByStaffName();
            return;
        }
        //search for staff by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelStaffRecord);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffRecord);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelStaffRecord);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelStaffRecord);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //load selected staff details
        private void LoadStaffRecordControls(Guid _staffID)
        {
            HiddenFieldStaffID.Value = _staffID.ToString();
            //get details associated with the staff id
            if (_staffID != Guid.Empty)
            {
                //load all staff record contols
                LoadAllStaffRecordCointrols(_staffID);
                return;
            }
            else
            {
                //clear entry controls
                ClearAllStaffRecordControls();
                return;
            }

        }
        //load all controls staff record 
        private void LoadAllStaffRecordCointrols(Guid _staffID)
        {
            //LoadStaffPhoto(_staffID);//load staff photo
            LoadStaffPersonalInformation(_staffID);//load staff personal information
            LoadEditStaffAddionalFieldsControls(_staffID);//load staff additional information
            DisplayAllergies(_staffID);//Load staff allergies
            DisplayLanguages(_staffID);//Load staff languages
            DisplayChronicConditions(_staffID);//Load staff chronic conditions
            DisplayBankDetails(_staffID);//Load staff Bank Details
            //DisplayWorkExperience();//display work experience 
            //DisplayEducationBackground();//display education
            //DisplayStaffSpouse();//display spouse details
            //ClearAddSpouseDetailsControls();
            //DisplayStaffChildren();//display chid details
            //ClearAddChildDetailsControls();
            //DisplayStaffDocuments();//display documents saved against the staff
            //LoadStaffEmploymentTerms(_staffID);//load the staff's employment term details
            //DisplaySpouseForMedicalBenefit();//load staff's spouse medical scheme beneficiaries
            //DisplayChildForMedicalBenefit();//load staff;s children medical scheme beneficiaries
            //DisplayEmploymentPerkDetails(_staffID);//display employment term perk/allowances associated with the staff
            return;
        }
        //clear all staff contraols
        private void ClearAllStaffRecordControls()
        {
            ClearAddStaffControls();//clear personal information
            //_hrClass.ClearGridView(gvEmploymentHistory);//clear work experience gridview
            //_hrClass.ClearGridView(gvEducationBackground);//clear education background gridview
            //_hrClass.ClearGridView(gvSpouseDetails);//clear spouse details
            //ClearAddSpouseDetailsControls();
            //_hrClass.ClearGridView(gvStaffDocuments);//clear staff documents  gridview
            return;
        }
        //load staff personal information
        private void LoadStaffPersonalInformation(Guid _staffID)
        {
            StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID == _staffID);
            txtStaffReference.Text = getStaff.StaffRef;
            //txtStaffName.Text = getStaff.StaffName;
            txtLastName.Text = getStaff.LastName;
            txtFirstName.Text = getStaff.FirstName;
            txtMiddleName.Text = getStaff.MiddleName;
            //ddlDepartment.SelectedValue = getStaff.DepartmentID.ToString();
            //ddlJobTitle.SelectedValue = getStaff.JobTitleID.ToString();
            //ddlPostedAt.SelectedValue = getStaff.PostedAtID.ToString();
            //if (getStaff.ReportingToStaffID != null)
            //    ddlReportsTo.SelectedValue = getStaff.ReportingToStaffID.ToString();
            //txtJoinDate.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
            ////txtEmploymentTermsDateOfJoin.Text = _hrClass.ShortDateDayStart(getStaff.JoinDate.ToString());
            //if (getStaff.ExitDate != null)
            //    txtExitDate.Text = _hrClass.ShortDateDayStart(getStaff.ExitDate.ToString());
            //else txtExitDate.Text = "";

            txtDOB.Text = _hrClass.ShortDateDayStart(getStaff.DateOfBirth.ToString());
            rblGender.SelectedValue = getStaff.Gender;
            if (getStaff.MaritalStatusID != null)
                ddlMaritalStatus.SelectedValue = getStaff.MaritalStatusID.ToString();
            else ddlMaritalStatus.SelectedIndex = 0;
            txtMobileNumber.Text = getStaff.PhoneNumber;
            if (getStaff.NationalityID != null)
                ddlNationality.SelectedValue = getStaff.NationalityID.ToString();
            else ddlNationality.SelectedIndex = 0;
            txtPassportNumber.Text = getStaff.PassportNumber;
            if (getStaff.PassportExpiryDate != null)
                txtPassportExpiryDate.Text = _hrClass.ShortDateDayStart(getStaff.PassportExpiryDate.ToString());
            else txtPassportExpiryDate.Text = "";
            txtNationalID.Text = getStaff.IDNumber.ToString();
            txtDiplomaticIDNumber.Text = getStaff.DiplomaticIDNumber;
            txtDrivingPermitNumber.Text = getStaff.DrivingPermitNumber;
            txtNSSFNumber.Text = getStaff.NSSFNumber;
            txtPinNumber.Text = getStaff.PinNumber;
            txtTINNumber.Text = getStaff.TINNumber;
            txtPermanentAddress.Text = getStaff.PermanentAddress;
            txtCurrentAddress.Text = getStaff.CurrentAddress;
            txtTelephoneContact.Text = getStaff.TelephoneContact;
            //txtNHIFNumber.Text = getStaff.NHIFNumber;
            //if (getStaff.AlternativePhoneNumber != null)
            //    txtAlternativeMobileNumber.Text = getStaff.AlternativePhoneNumber;
            txtEmailAddress.Text = getStaff.EmailAddress;
            if (Convert.ToString(getStaff.AlternativeEmailAddress) != "")
                txtAlternativeEmailAddress.Text = getStaff.AlternativeEmailAddress;
            else txtAlternativeEmailAddress.Text = "";
            txtPostalAddress.Text = getStaff.PostalAddress;
            txtLanguages.Text = getStaff.Languages;
            //if (getStaff.Salary != null)
            //    txtSalary.Text = _hrClass.ConvertToCurrencyValue((decimal)getStaff.Salary);
            //txtEmergencyContactPerson.Text = getStaff.EmergencyContactPerson;
            //txtEmergencyContactPersonPhoneNumber.Text = getStaff.EmergencyContactPhoneNo;
            txtNextOfKinName.Text = getStaff.NextOfKinName;
            txtNextOfKinPhoneNumber.Text = getStaff.NextOfKinPhone;
            txtNextOfKinRelationship.Text = getStaff.NextOfKinRelationship;
            txtNextOfKinAddress.Text = getStaff.NextOfKinAddress;
            txtNextOfKin2Name.Text = getStaff.NextOfKin2Name;
            txtNextOfKin2PhoneNumber.Text = getStaff.NextOfKin2Phone;
            txtNextOfKin2Relationship.Text = getStaff.NextOfKin2Relationship;
            txtNextOfKin2Address.Text = getStaff.NextOfKin2Address;
            txtDisabilityDetails.Text = getStaff.DisabilityDetails;
            txtRefereeDetails1.Text = getStaff.Referee1Details;
            txtRefereeDetails2.Text = getStaff.Referee2Details;
            txtRefereeDetails3.Text = getStaff.Referee3Details;
            txtStaffRecordRemarks.Text = getStaff.Remarks;
            lnkBtnSaveStaffRecord.Visible = false;
            lnkBtnClearStaffRecord.Visible = true;
            lnkBtnUpdateStaffRecord.Enabled = true;
        }

        #endregion
        //#region "STAFF EMPLOYMENT TERMS"
        ////populate medical schemes dropdown
        //public void GetMedicalBenefits()
        //{
        //    try
        //    {
        //        ddlEmploymentTermsMedicalScheme.Items.Clear();
        //        object _getMedicalSchemes;
        //        _getMedicalSchemes = db.MedSchemes.Select(p => p).OrderBy(p => p.medscheme_vName);
        //        ddlEmploymentTermsMedicalScheme.DataSource = _getMedicalSchemes;
        //        ddlEmploymentTermsMedicalScheme.Items.Insert(0, new ListItem("-Select Medical Scheme-"));
        //        ddlEmploymentTermsMedicalScheme.DataTextField = "medscheme_vName";
        //        ddlEmploymentTermsMedicalScheme.DataValueField = "medscheme_siMedSchemeID";
        //        ddlEmploymentTermsMedicalScheme.AppendDataBoundItems = true;
        //        ddlEmploymentTermsMedicalScheme.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        ////validate add employment terms controls
        //private string ValidateAddEmploymentTermsControls()
        //{
        //    if (HiddenFieldStaffID.Value == "")
        //        return "Employment terms save failed. Enter staff's basic information or search for the staff before adding his/her employment terms!";
        //    else if (ddlEmploymentTermsTypeOfEmployment.SelectedIndex == 0)
        //        return "Select employmet type for the staff!";
        //    else if (txtEmploymentTermsDateUntil.Text.Trim() != "__/___/____" && Convert.ToDateTime(txtEmploymentTermsDateUntil.Text).Date <= Convert.ToDateTime(txtEmploymentTermsDateOfJoin.Text).Date)
        //        return "Date until cannot be before or the same to date of join!";
        //    else if (txtEmploymentTermsBasicPay.Text.Trim() != "" && _hrClass.IsCurrencyValid(txtEmploymentTermsBasicPay.Text.Trim()) == false)
        //        return "Enter a valid numeric basic pay value!";
        //    else if (txtEmploymentTermsLiquidatedDamages.Text != "" && _hrClass.IsCurrencyValid(txtEmploymentTermsLiquidatedDamages.Text.Trim()) == false)
        //        return "Enter a valid numeric liquidated damage value!";
        //    else return "";
        //}
        ////method for saving staff employment terms
        //private void SaveEmploymentTerms()
        //{
        //    try
        //    {
        //        string _error = ValidateAddEmploymentTermsControls();//
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            //save employment terms
        //            if (HiddenFieldEmploymentTermsID.Value == "")
        //            {
        //                EmpTerm newEmploymentTerms = new EmpTerm();
        //                newEmploymentTerms.empterms_uiStaffID = _staffID;
        //                if (txtEmploymentTermsDateUntil.Text.Trim() != "__/___/____")
        //                {
        //                    newEmploymentTerms.empterms_dtDateUntil = Convert.ToDateTime(txtEmploymentTermsDateUntil.Text.Trim()).Date;
        //                }
        //                newEmploymentTerms.empterms_iEmpTypeID = Convert.ToInt32(ddlEmploymentTermsTypeOfEmployment.SelectedValue);
        //                if (txtEmploymentTermsBasicPay.Text.Trim() != "")
        //                {
        //                    newEmploymentTerms.empterms_mBasicPay = Convert.ToDecimal(txtEmploymentTermsBasicPay.Text.Trim());
        //                }
        //                newEmploymentTerms.empterms_vNoticePeriod = txtEmploymentTermsNoticePeriod.Text.Trim();
        //                if (txtEmploymentTermsLiquidatedDamages.Text != "")
        //                {
        //                    newEmploymentTerms.empterms_mLiquidatedDamage = Convert.ToDecimal(txtEmploymentTermsLiquidatedDamages.Text.Trim());
        //                }
        //                if (ddlEmploymentTermsMedicalScheme.SelectedIndex != 0)
        //                {
        //                    newEmploymentTerms.empterms_siMedicalSchemeID = Convert.ToInt16(ddlEmploymentTermsMedicalScheme.SelectedValue);
        //                }
        //                db.EmpTerms.InsertOnSubmit(newEmploymentTerms);
        //                db.SubmitChanges();
        //                HiddenFieldEmploymentTermsID.Value = newEmploymentTerms.empterms_iEmpTermsID.ToString();
        //                _hrClass.LoadHRManagerMessageBox(2, "Staff employment terms have been successfully saved!", this, PanelStaffRecord);
        //                return;
        //            }
        //            else
        //            {
        //                int _employmentTermID = Convert.ToInt32(HiddenFieldEmploymentTermsID.Value);
        //                //update employment term details
        //                EmpTerm updateEmploymentTerms = db.EmpTerms.Single(p => p.empterms_iEmpTermsID == _employmentTermID);
        //                if (txtEmploymentTermsDateUntil.Text.Trim() != "")
        //                    updateEmploymentTerms.empterms_dtDateUntil = Convert.ToDateTime(txtEmploymentTermsDateUntil.Text.Trim()).Date;
        //                else
        //                    updateEmploymentTerms.empterms_dtDateUntil = null;

        //                updateEmploymentTerms.empterms_iEmpTypeID = Convert.ToInt32(ddlEmploymentTermsTypeOfEmployment.SelectedValue);

        //                if (txtEmploymentTermsBasicPay.Text.Trim() != "")
        //                    updateEmploymentTerms.empterms_mBasicPay = Convert.ToDecimal(txtEmploymentTermsBasicPay.Text.Trim());
        //                else
        //                    updateEmploymentTerms.empterms_mBasicPay = null;

        //                updateEmploymentTerms.empterms_vNoticePeriod = txtEmploymentTermsNoticePeriod.Text.Trim();

        //                if (txtEmploymentTermsLiquidatedDamages.Text != "")
        //                    updateEmploymentTerms.empterms_mLiquidatedDamage = Convert.ToDecimal(txtEmploymentTermsLiquidatedDamages.Text.Trim());
        //                else
        //                    updateEmploymentTerms.empterms_mLiquidatedDamage = null;
        //                if (ddlEmploymentTermsMedicalScheme.SelectedIndex != 0)
        //                    updateEmploymentTerms.empterms_siMedicalSchemeID = Convert.ToInt16(ddlEmploymentTermsMedicalScheme.SelectedValue);
        //                else
        //                    updateEmploymentTerms.empterms_siMedicalSchemeID = null;
        //                db.SubmitChanges();
        //                _hrClass.LoadHRManagerMessageBox(2, "Staff employment terms have been successfully updated!", this, PanelStaffRecord);
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////save/update staff's employment terms
        //protected void btnSaveEmployementTerms_Click(object sender, EventArgs e)
        //{
        //    SaveEmploymentTerms(); return;
        //}
        ////load staff employment terms
        //private void LoadStaffEmploymentTerms(Guid _staffID)
        //{
        //    try
        //    {
        //        //check if the staff has goot any employment terfms details that are saved
        //        if (db.EmpTerms.Any(p => p.empterms_uiStaffID == _staffID))
        //        {
        //            //get the staff
        //            EmpTerm getEmploymentTerms = db.EmpTerms.Single(p => p.empterms_uiStaffID == _staffID);
        //            HiddenFieldEmploymentTermsID.Value = getEmploymentTerms.empterms_iEmpTermsID.ToString();
        //            if (getEmploymentTerms.empterms_dtDateUntil != null)
        //                txtEmploymentTermsDateUntil.Text = getEmploymentTerms.empterms_dtDateUntil.Value.Date.ToString("dd/MMM/yyyy");
        //            else txtEmploymentTermsDateUntil.Text = "";

        //            ddlEmploymentTermsTypeOfEmployment.SelectedValue = getEmploymentTerms.empterms_iEmpTypeID.ToString();

        //            txtEmploymentTermsNoticePeriod.Text = getEmploymentTerms.empterms_vNoticePeriod;
        //            if (getEmploymentTerms.empterms_mBasicPay != null)
        //                txtEmploymentTermsBasicPay.Text = _hrClass.ConvertToCurrencyValue((decimal)getEmploymentTerms.empterms_mBasicPay);
        //            else txtEmploymentTermsBasicPay.Text = "";
        //            if (getEmploymentTerms.empterms_mLiquidatedDamage != null)
        //                txtEmploymentTermsLiquidatedDamages.Text = _hrClass.ConvertToCurrencyValue((decimal)getEmploymentTerms.empterms_mLiquidatedDamage);
        //            else txtEmploymentTermsLiquidatedDamages.Text = "";
        //            if (getEmploymentTerms.empterms_siMedicalSchemeID != null)
        //                ddlEmploymentTermsMedicalScheme.SelectedValue = getEmploymentTerms.empterms_siMedicalSchemeID.ToString();
        //            else ddlEmploymentTermsMedicalScheme.SelectedIndex = 0;
        //            return;
        //        }
        //        else
        //        {
        //            txtEmploymentTermsDateUntil.Text = "";
        //            ddlEmploymentTermsTypeOfEmployment.SelectedIndex = 0;
        //            txtEmploymentTermsBasicPay.Text = "";
        //            txtEmploymentTermsNoticePeriod.Text = "";
        //            txtEmploymentTermsLiquidatedDamages.Text = "";
        //            ddlEmploymentTermsMedicalScheme.SelectedIndex = 0;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////validate add employment allowance controls
        //private string ValidateAddEmploymentAllowanceControls()
        //{
        //    if (HiddenFieldStaffID.Value == "")
        //        return "Employment allowance save failed. Enter staff's basic information or search for the staff before adding his/her employment terms!";
        //    else if (ddlEmploymentTermsPerkName.SelectedIndex == 0)
        //        return "Select an allowance!";
        //    else if (txtEmploymentTermsPerkAmount.Text.Trim() == "")
        //        return "Enter perk amount for the perk selected!";
        //    else if (_hrClass.IsCurrencyValid(txtEmploymentTermsPerkAmount.Text.Trim()) == false)
        //        return "Enter a valid numeric perk amount value!";
        //    else if (txtEmploymentTermsEffectiveFrom.Text.Trim() == "")
        //        return "Select perk effective from date!";
        //    else if (_hrClass.isDateValid(txtEmploymentTermsEffectiveFrom.Text.Trim()) == false)
        //        return "Invalid perk effective date entered!";
        //    else if (Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text.Trim()).Date < Convert.ToDateTime(txtEmploymentTermsDateOfJoin.Text.Trim()))
        //        return "Date when  perk is effective from cannot be before employee's join date!";
        //    //else if (ddlEmploymentTermsPerkName.SelectedIndex != 0 && txtEmploymentTermsEffectiveUntil.Text.Trim() == "")
        //    //{
        //    //    _error = "Select perk effective until date!";
        //    //    btnEmploymentTermsEffectiveUntil.Focus();
        //    //}
        //    else if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____" && _hrClass.isDateValid(txtEmploymentTermsEffectiveUntil.Text.Trim()) == false)
        //        return "Invalid perk effective until date entered!";
        //    else if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____" && Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text.Trim()).Date < Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text.Trim()).Date)
        //        return "Date when perk is effective until cannot be before date when perk is effective from!";
        //    else if (txtEmploymentTermsPerkAmount.Text.Trim() != "" && ddlEmploymentTermsPerkName.SelectedIndex == 0)
        //        return "Select perk for the perk amount entered!";
        //    else return "";
        //}
        ////method for saving employment perk
        //private void SaveEmploymentAllowance()
        //{
        //    try
        //    {
        //        string _error = ValidateAddEmploymentAllowanceControls();//check for errors
        //        if (_error != "")
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //            int _allowanceID = Convert.ToInt32(ddlEmploymentTermsPerkName.SelectedValue);
        //            if (btnEmploymentTermsAddPerk.Text.ToLower() == "add perk")
        //            {
        //                //check if the selected perk is selected against the staff
        //                if (db.EmpTermsPerkDetails.Any(p => p.emptermsperkdetail_uiStaffID == _staffID && p.emptermsperkdetail_iPerkID == _allowanceID))
        //                {

        //                    _hrClass.LoadHRManagerMessageBox(2, "Save failed. The selected perk is already saved against the staff!", this, PanelStaffRecord);
        //                    return;
        //                }
        //                else
        //                {
        //                    //save perk details
        //                    AddEmployeePerkDetail(_staffID, _allowanceID);
        //                    _hrClass.LoadHRManagerMessageBox(2, "Employment allowance details have been successfully saved!", this, PanelStaffRecord);
        //                    return;
        //                }
        //            }
        //            else//update perk details
        //            {
        //                int _perkDetailID = Convert.ToInt32(HiddenFieldEmploymentTermsPerksDetailID.Value);

        //                //check if the selected perk is selected against the staff
        //                if (db.EmpTermsPerkDetails.Any(p => p.emptermsperkdetail_uiStaffID == _staffID && p.emptermsperkdetail_iPerkID == _allowanceID && p.emptermsperkdetail_iEmpTermsPerkDetailID != _perkDetailID))
        //                {

        //                    _hrClass.LoadHRManagerMessageBox(2, "Update failed. The selected perk is already saved against the staff!", this, PanelStaffRecord);
        //                    return;
        //                }
        //                else
        //                {
        //                    //update the staff perk details
        //                    UpdateEmployeePerkDetail(_perkDetailID, _allowanceID);//
        //                    _hrClass.LoadHRManagerMessageBox(2, "Employment term allowance details have been successfully updated!", this, PanelStaffRecord);
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////method for adding perk/allowance detail
        //private void AddEmployeePerkDetail(Guid _staffID, int _allowanceID)
        //{
        //    //create perk detail
        //    EmpTermsPerkDetail newEmployeeTemsPerkDetail = new EmpTermsPerkDetail();
        //    newEmployeeTemsPerkDetail.emptermsperkdetail_uiStaffID = _staffID;
        //    newEmployeeTemsPerkDetail.emptermsperkdetail_iPerkID = _allowanceID;
        //    newEmployeeTemsPerkDetail.emptermsperkdetail_mPerkAmount = Convert.ToDecimal(txtEmploymentTermsPerkAmount.Text.Trim());
        //    newEmployeeTemsPerkDetail.emptermsperkdetail_dtPerkDateFrom = Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text);
        //    if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____")
        //    {
        //        newEmployeeTemsPerkDetail.emptermsperkdetail_dtPerkDateUntil = Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text);
        //    }
        //    db.EmpTermsPerkDetails.InsertOnSubmit(newEmployeeTemsPerkDetail);
        //    db.SubmitChanges();
        //    HiddenFieldEmploymentTermsPerksDetailID.Value = newEmployeeTemsPerkDetail.emptermsperkdetail_iEmpTermsPerkDetailID.ToString();
        //    //display employment terms perk details associated with the staff
        //    DisplayEmploymentPerkDetails(_staffID);
        //}
        ////methhod for updating a perk/allowance detail
        //private void UpdateEmployeePerkDetail(int _perkDetailID, int _allowanceID)
        //{
        //    //alter perk detail
        //    EmpTermsPerkDetail updatePerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _perkDetailID);
        //    updatePerkDetail.emptermsperkdetail_iPerkID = _allowanceID;
        //    updatePerkDetail.emptermsperkdetail_mPerkAmount = Convert.ToDecimal(txtEmploymentTermsPerkAmount.Text.Trim());
        //    updatePerkDetail.emptermsperkdetail_dtPerkDateFrom = Convert.ToDateTime(txtEmploymentTermsEffectiveFrom.Text).Date;
        //    if (txtEmploymentTermsEffectiveUntil.Text.Trim() != "__/___/____")
        //        updatePerkDetail.emptermsperkdetail_dtPerkDateUntil = Convert.ToDateTime(txtEmploymentTermsEffectiveUntil.Text);
        //    else updatePerkDetail.emptermsperkdetail_dtPerkDateUntil = null;
        //    db.SubmitChanges();
        //    //refresh employment perk details]
        //    DisplayEmploymentPerkDetails((Guid)updatePerkDetail.emptermsperkdetail_uiStaffID);
        //    //clear the netry controls
        //    ClearEmploymentTermsPerkDetailsEntryControls();
        //    return;
        //}
        ////save employment allowance details
        //protected void btnEmploymentTermsAddPerk_Click(object sender, EventArgs e)
        //{
        //    SaveEmploymentAllowance(); return;//
        //}
        ////display employment perk details
        //private void DisplayEmploymentPerkDetails(Guid _staffID)
        //{
        //    try
        //    {
        //        object _display;
        //        _display = db.PROC_StaffPerks(_staffID);
        //        gvEmploymentTermsPerksDetails.DataSourceID = null;
        //        gvEmploymentTermsPerksDetails.DataSource = _display;
        //        gvEmploymentTermsPerksDetails.DataBind();
        //        return;
        //    }
        //    catch { }
        //}
        ////load employee's employment terms perks  details for edit when the edit button is clicked from the grid view, or delete when the delete button is clicked
        //protected void gvEmploymentTermsPerksDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName.CompareTo("EditPerkDetail") == 0 || e.CommandName.CompareTo("DeletePerkDetail") == 0)
        //        {
        //            int ID = Convert.ToInt32(e.CommandArgument);
        //            GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
        //            GridView gdv = ((GridView)e.CommandSource);
        //            HiddenField _HFEmploymentTermPerkDetailID = (HiddenField)gvEmploymentTermsPerksDetails.Rows[ID].FindControl("HiddenField1");
        //            if (e.CommandName.CompareTo("EditPerkDetail") == 0)//check if we are editing
        //            {
        //                LoadEmploymentTermPerkDetailsControlsForEdit(Convert.ToInt32(_HFEmploymentTermPerkDetailID.Value));
        //                return;
        //            }
        //            if (e.CommandName.CompareTo("DeletePerkDetail") == 0)//check if we are deleting
        //            {
        //                HiddenFieldEmploymentTermsPerksDetailID.Value = _HFEmploymentTermPerkDetailID.Value;
        //                _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, "Are you sure you what to delete the employee's perk detail?");
        //                return;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////clear employment terms perk details entry controls()
        //private void ClearEmploymentTermsPerkDetailsEntryControls()
        //{
        //    ddlEmploymentTermsPerkName.SelectedIndex = 0;
        //    txtEmploymentTermsPerkAmount.Text = "";
        //    txtEmploymentTermsEffectiveFrom.Text = "";
        //    txtEmploymentTermsEffectiveUntil.Text = "";
        //    btnEmploymentTermsAddPerk.Text = "Add Perk";
        //    HiddenFieldEmploymentTermsPerksDetailID.Value = "";
        //    return;
        //}
        ////load employment term perk details for edit
        //private void LoadEmploymentTermPerkDetailsControlsForEdit(int _employmentTermPerkDetailID)
        //{
        //    HiddenFieldEmploymentTermsPerksDetailID.Value = _employmentTermPerkDetailID.ToString();
        //    EmpTermsPerkDetail getPerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _employmentTermPerkDetailID);
        //    ddlEmploymentTermsPerkName.SelectedValue = getPerkDetail.emptermsperkdetail_iPerkID.ToString();
        //    txtEmploymentTermsPerkAmount.Text = _hrClass.ConvertToCurrencyValue((decimal)getPerkDetail.emptermsperkdetail_mPerkAmount);
        //    txtEmploymentTermsEffectiveFrom.Text = _hrClass.ShortDateDayStart(getPerkDetail.emptermsperkdetail_dtPerkDateFrom.ToString());
        //    if (getPerkDetail.emptermsperkdetail_dtPerkDateUntil != null)
        //    {
        //        txtEmploymentTermsEffectiveUntil.Text = _hrClass.ShortDateDayStart(getPerkDetail.emptermsperkdetail_dtPerkDateUntil.ToString());
        //    }
        //    else txtEmploymentTermsEffectiveUntil.Text = "";
        //    btnEmploymentTermsAddPerk.Text = "Update Perk";
        //    return;
        //}
        ////method for deleting an employment term perk detail
        //private void DeleteEmploymentTermPerkDetails()
        //{
        //    try
        //    {
        //        int _perkDetailID = Convert.ToInt32(HiddenFieldEmploymentTermsPerksDetailID.Value);//get employment term perk detail id
        //        //delete emploment term perk detail
        //        EmpTermsPerkDetail deletePerkDetail = db.EmpTermsPerkDetails.Single(p => p.emptermsperkdetail_iEmpTermsPerkDetailID == _perkDetailID);
        //        db.EmpTermsPerkDetails.DeleteOnSubmit(deletePerkDetail);
        //        db.SubmitChanges();
        //        //refresh employee's perk details after delete is done
        //        DisplayEmploymentPerkDetails((Guid)deletePerkDetail.emptermsperkdetail_uiStaffID);
        //        _hrClass.LoadHRManagerMessageBox(1, "Employment perk detail has been deleted!", this, PanelStaffRecord);
        //        ClearEmploymentTermsPerkDetailsEntryControls();
        //        return;
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, "Delete failed. An error occured. " + ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        ////display employee's spouse to benefit from the employee's medical scheme
        //private void DisplaySpouseForMedicalBenefit()
        //{
        //    try
        //    {
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        //check if the staf has got any spouse details
        //        if (db.PROC_StaffSpouse(_staffID).Any())
        //        {
        //            PanelSpouseToBenefit.Visible = true;

        //            DataTable dt = new DataTable();
        //            dt.Columns.Add(new DataColumn("SNO", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ID", typeof(int)));
        //            dt.Columns.Add(new DataColumn("SpouseName", typeof(string)));
        //            dt.Columns.Add(new DataColumn("SpouseDOB", typeof(string)));
        //            dt.Columns.Add(new DataColumn("PhoneNumber", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Checked", typeof(bool)));

        //            DataRow dr;
        //            int countRows = 1;//count number of rows
        //            foreach (PROC_StaffSpouseResult getSpouse in db.PROC_StaffSpouse(_staffID))
        //            {
        //                dr = dt.NewRow();
        //                dr[0] = countRows;
        //                dr[1] = getSpouse.spouse_iSpouseID;
        //                dr[2] = getSpouse.spouse_vSpouseName;
        //                if (getSpouse.spouse_dtDOB != null || getSpouse.spouse_dtDOB.ToString() != "")
        //                {
        //                    dr[3] = _hrClass.ShortDateDayStart(getSpouse.spouse_dtDOB.ToString());
        //                }
        //                dr[4] = getSpouse.spouse_vPhoneNumber;
        //                dr[5] = getSpouse.spouse_bIsMedicalCovered;

        //                dt.Rows.Add(dr);
        //                countRows++;
        //            }
        //            gvSpouseToBenefit.DataSource = dt;
        //            gvSpouseToBenefit.DataBind();
        //            return;
        //        }
        //        else
        //        {
        //            PanelSpouseToBenefit.Visible = false;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////save spouse to benefit from medical schemebenefits
        //private void SaveSpouseToBenefitFromMedicalSchemeBenefits()
        //{
        //    try
        //    {
        //        if (!_hrClass.isGridviewItemSelected(gvSpouseToBenefit))//check if there is  any spouse selected to be assigned medical benefits
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Save Failed. There is no spouse selected. Select employee's spouse to be enlisted as a medical benefits beneficiary!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvSpouseToBenefit.Rows)
        //            {

        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            //if (x > 1)
        //            //{
        //            //    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one employee spouse!.<br>Select only one spouse to assign to the medical scheme benefits!", this, PanelStaffRecord);
        //            //    return;
        //            //}
        //            foreach (GridViewRow _grv in gvSpouseToBenefit.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;

        //                if (_cb.Checked)
        //                {
        //                    HiddenField _HFSpouseID = (HiddenField)_grv.FindControl("HFID");
        //                    int SpouseID = Convert.ToInt32(_HFSpouseID.Value);
        //                    Session["SpouseToBenefitID"] = SpouseID.ToString();

        //                    if (db.Spouses.Where(p => p.spouse_iSpouseID == SpouseID && p.spouse_bIsMedicalCovered == true).Count() == 0)//check if the spouse selected is assigned to the medical scheme
        //                    {
        //                        //assign medical scheme benefit to the spouse selected
        //                        Spouse assignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == SpouseID);
        //                        assignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = true;
        //                    }
        //                }
        //                else
        //                {
        //                    int SpouseID = Convert.ToInt32(Session["SpouseToBenefitID"]);
        //                    foreach (Spouse deAssignSpouse in db.Spouses.Where(p => p.spouse_iSpouseID != SpouseID))//deasign medical scheme for the unselected employee's spouse
        //                    {
        //                        Spouse deAssignMedicalSchemeToSpouse = db.Spouses.Single(p => p.spouse_iSpouseID == deAssignSpouse.spouse_iSpouseID);
        //                        deAssignMedicalSchemeToSpouse.spouse_bIsMedicalCovered = false;
        //                    }
        //                }
        //            }
        //            db.SubmitChanges();
        //            _hrClass.LoadHRManagerMessageBox(2, "The selected spouse has been enlisted as medical beneficiary!", this, PanelStaffRecord);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //protected void btnSaveSpouseToBenefit_OnClick(object sender, EventArgs e)
        //{
        //    SaveSpouseToBenefitFromMedicalSchemeBenefits();
        //}
        ////display employee's  children to have medical benefit
        //private void DisplayChildForMedicalBenefit()
        //{
        //    try
        //    {
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        //check if the staff has got any children
        //        if (db.PROC_StaffChildren(_staffID).Any())
        //        {
        //            PanelChildToBenefit.Visible = true;

        //            DataTable dt = new DataTable();
        //            dt.Columns.Add(new DataColumn("SNO", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ID", typeof(int)));
        //            dt.Columns.Add(new DataColumn("ChildName", typeof(string)));
        //            dt.Columns.Add(new DataColumn("ChildDOB", typeof(string)));
        //            dt.Columns.Add(new DataColumn("Checked", typeof(bool)));

        //            DataRow dr;
        //            int countRows = 1;//count number of rows
        //            foreach (PROC_StaffChildrenResult getChild in db.PROC_StaffChildren(_staffID))
        //            {
        //                dr = dt.NewRow();
        //                dr[0] = countRows;
        //                dr[1] = getChild.child_iChildID;
        //                dr[2] = getChild.child_vName;
        //                if (getChild.child_dtDOB != null || getChild.child_dtDOB.ToString() != "")
        //                {
        //                    dr[3] = _hrClass.ShortDateDayStart(getChild.child_dtDOB.ToString());
        //                }
        //                dr[4] = getChild.child_bIsMedicalCovered;
        //                dt.Rows.Add(dr);
        //                countRows++;
        //            }
        //            gvChildToBenefit.DataSource = dt;
        //            gvChildToBenefit.DataBind();
        //            return;
        //        }
        //        else
        //        {
        //            PanelChildToBenefit.Visible = false;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        ////save children to benefit from medical scheme benefits
        //private void SaveChildrenToBenefitFromMedicalSchemeBenefits()
        //{
        //    try
        //    {
        //        //get employee's staff id
        //        Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
        //        if (!_hrClass.isGridviewItemSelected(gvChildToBenefit))//check if there is  any child selected to be assigned medical benefits
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Save Failed!. There is no employee's child selected!.<br>Select employee's children to be alocated the medical scheme benefits!", this, PanelStaffRecord);
        //            return;
        //        }
        //        else
        //        {
        //            int x = 0;
        //            foreach (GridViewRow _grv in gvChildToBenefit.Rows)
        //            {

        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                if (_cb.Checked) ++x;
        //            }
        //            if (x > 2)
        //            {
        //                _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one employee children!.<br>Only two children can be allocated to medical scheme benefits!", this, PanelStaffRecord);
        //                return;
        //            }
        //            foreach (GridViewRow _grv in gvChildToBenefit.Rows)
        //            {
        //                CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
        //                HiddenField _HFChildID = (HiddenField)_grv.FindControl("HFID");
        //                int ChildID = Convert.ToInt32(_HFChildID.Value);
        //                if (_cb.Checked) ++x;
        //                if (_cb.Checked)//if child is selected th assign medical benefits
        //                {
        //                    Child assignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == ChildID);
        //                    //check if the child is over 18 years
        //                    if (assignMedicalSchemeToChild.child_dtDOB.Value.Date.Year < DateTime.Now.Date.Year - 18)
        //                    {
        //                        _hrClass.LoadHRManagerMessageBox(1, "The Child selected, " + assignMedicalSchemeToChild.child_vName + " is over 18 years!", this, PanelStaffRecord);
        //                        return;
        //                    }
        //                    else
        //                        assignMedicalSchemeToChild.child_bIsMedicalCovered = true;
        //                }
        //                //deasign the employee's children who are not selected
        //                ///in future loop to check if the children selected have sur-pursses 18 years or 
        //                ///orr if the de-selected children have claimed any amount
        //                else//if child is not selected then deassign medical benefits
        //                {
        //                    Child deAssignMedicalSchemeToChild = db.Childs.Single(p => p.child_iChildID == ChildID);
        //                    deAssignMedicalSchemeToChild.child_bIsMedicalCovered = false;
        //                }
        //            }
        //            db.SubmitChanges();
        //            if (x > 1)
        //                _hrClass.LoadHRManagerMessageBox(2, "The selected child has been enlisted as a beneficairy!", this, PanelStaffRecord);
        //            else
        //                _hrClass.LoadHRManagerMessageBox(2, "The selected children has been enlisted as beneficiaries!", this, PanelStaffRecord);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelStaffRecord);
        //        return;
        //    }
        //}
        //protected void btnSaveChildToBenefit_OnClick(object sender, EventArgs e)
        //{
        //    SaveChildrenToBenefitFromMedicalSchemeBenefits();
        //}
        //#endregion
    }

}