﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Globalization;

namespace AdeptHRManager.HR_Module
{
    public partial class Employment_History : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();//pouplate details for the already searched staff
            }

            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //get already searched staff details
        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                if (_searchedStaffID != null)
                {
                    Guid _staffID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                    _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                    DisplayStaffWorkExperience(_staffID);//load qualification details
                    return;
                }
            }
            catch { }
        }
        //display staff work experience
        private void DisplayStaffWorkExperience(Guid _staffID)
        {
            DisplayWorkExperience();//display work experience 
        }
        //search for leaves by staff reference number
        private void SearchForLeavesByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffWorkExperience(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffWorkExperience(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the reference number entered.", this, PanelEmploymentHistory);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentHistory);
                return;
            }
        }
        //search leaves by staff refference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffReference();
            return;
        }
        //search for leaves by staff namae
        private void SearchForLeavesByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffWorkExperience(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffWorkExperience(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelEmploymentHistory);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentHistory);
                return;
            }
        }
        //search leaves by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForLeavesByStaffName();
            return;
        }
        //search for leaves by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffWorkExperience(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffWorkExperience(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelEmploymentHistory);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentHistory);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    DisplayStaffWorkExperience(_StaffID);
                    return;
                }
                else
                {
                    DisplayStaffWorkExperience(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelEmploymentHistory);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelEmploymentHistory);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //clear add work experience controls
        private void ClearAddWorkExperienceControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddWorkExperience);
            HiddenFieldWorkExperienceID.Value = "";
            lnkBtnSaveWorkExperience.Visible = true;
            lnkBtnUpdateWorkExperience.Enabled = false;
            lnkBtnUpdateWorkExperience.Visible = true;
            lnkBtnClearWorkExperience.Visible = true;
            lbAddWorkExperienceHeader.Text = "Add Employment History";
            ImageAddWorkExperienceHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbWorkExperienceError.Text = "";
            lbWorkExperienceInfo.Text = "";
        }
        //load ad new work experience record
        protected void LinkButtonNewWorkExperience_Click(object sender, EventArgs e)
        {
            _hrClass.GenerateMonths(ddlWorkExperienceStartDateMonth);//start date months
            _hrClass.GenerateYears(ddlWorkExperienceStartDateYear);//start date years
            _hrClass.GenerateMonths(ddlWorkExperienceEndDateMonth);//end date months
            _hrClass.GenerateYears(ddlWorkExperienceEndDateYear);//end date years
            ClearAddWorkExperienceControls();
            ModalPopupExtenderAddWorkExperience.Show();
            return;
        }
        //validate save staff record work experience
        private string ValidateStaffWorkExperienceControls()
        {
            if (HiddenFieldStaffID.Value == "")

                return "Save failed. Search for an employee inorder for you to be able to add his/her employment history details!";
            else if (txtWorkExperiencePositionHeld.Text.Trim() == "")
            {
                txtWorkExperiencePositionHeld.Focus();
                return "Enter posistion held!";
            }
            else if (txtWorkExperienceEmployerName.Text.Trim() == "")
            {
                txtWorkExperienceEmployerName.Focus();
                return "Enter employer's name!";
            }
            else if (ddlWorkExperienceStartDateMonth.SelectedIndex == 0)
            {
                ddlWorkExperienceStartDateMonth.Focus();
                return "Select start date month!";
            }
            else if (ddlWorkExperienceStartDateYear.SelectedIndex == 0)
            {
                ddlWorkExperienceStartDateYear.Focus();
                return "Select start date year!";
            }
            else if (ddlWorkExperienceEndDateMonth.SelectedIndex == 0)
            {
                ddlWorkExperienceEndDateMonth.Focus();
                return "Select end date month!";
            }
            else if (ddlWorkExperienceEndDateMonth.SelectedIndex == 0)
            {
                ddlWorkExperienceEndDateMonth.Focus();
                return "Select end date year!";
            }
            else if (db.PROC_ValidatesDates(ddlWorkExperienceStartDateMonth.SelectedValue, ddlWorkExperienceStartDateYear.SelectedValue, ddlWorkExperienceEndDateMonth.SelectedValue, ddlWorkExperienceEndDateYear.SelectedValue).FirstOrDefault().Column1 == 0)
            {
                return "Invalid Start and End dates selected.<br>End date should be greater than start date!";
            }
            else if (txtSupervisor.Text.Trim() == "")
                return "Enter supervisor name!";
            //else if (cbWasEmployerEADB.Checked == true && txtEADBContractReference.Text.Trim() == "")
            //    return "Enter EADB contract reference!";
            //else if (cbWasEmployerEADB.Checked == true && txtEADBContractDate.Text.Trim() == "__/___/____")
            //    return "Enter EADB contract date!";
            //else if (cbWasEmployerEADB.Checked == true && _hrClass.isDateValid(txtEADBContractDate.Text.Trim()) == false)
            //    return "Invalid EADB contract date!";
            else return "";

        }
        //save staff work experience details
        private void SaveStaffWorkWorkExperience()
        {
            ModalPopupExtenderAddWorkExperience.Show();
            try
            {
                //check for errors
                string _error = ValidateStaffWorkExperienceControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelEmploymentHistory);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                    StaffWorkExperience newWorkExperience = new StaffWorkExperience();
                    newWorkExperience.StaffID = _staffID;
                    newWorkExperience.EmployerName = txtWorkExperienceEmployerName.Text.Trim();
                    newWorkExperience.PositionHeld = txtWorkExperiencePositionHeld.Text.Trim();
                    newWorkExperience.StartDateMonth = ddlWorkExperienceStartDateMonth.SelectedValue;
                    newWorkExperience.StartDateYear = ddlWorkExperienceStartDateYear.SelectedValue;
                    newWorkExperience.EndDateMonth = ddlWorkExperienceEndDateMonth.SelectedValue;
                    newWorkExperience.EndDateYear = ddlWorkExperienceEndDateYear.SelectedValue;
                    newWorkExperience.Supervisor = txtSupervisor.Text.Trim();
                    newWorkExperience.TerminationReason = txtTerminationReason.Text.Trim();
                    newWorkExperience.IsEmployerVerified = cbEmployerVerified.Checked;
                    //if (cbWasEmployerEADB.Checked == true)
                    //{
                    //    newWorkExperience.WasEmployerEADB = true;
                    //    newWorkExperience.EADBContractRef = txtEADBContractReference.Text.Trim();
                    //    newWorkExperience.EADBContractDate = Convert.ToDateTime(txtEADBContractDate.Text.Trim()).Date;
                    //}
                    //else
                    //{
                        newWorkExperience.WasEmployerEADB = false;
                        newWorkExperience.EADBContractRef = "";
                        newWorkExperience.EADBContractDate = null;
                    //}
                    db.StaffWorkExperiences.InsertOnSubmit(newWorkExperience);
                    db.SubmitChanges();
                    lbWorkExperienceError.Text = "";
                    HiddenFieldWorkExperienceID.Value = newWorkExperience.StaffWorkExperienceID.ToString();
                    lnkBtnUpdateWorkExperience.Enabled = true;
                    ModalPopupExtenderAddWorkExperience.Hide();
                    _hrClass.LoadHRManagerMessageBox(2, "Staff work experience record has been successfully saved!", this, PanelEmploymentHistory);
                    //display work experience
                    DisplayWorkExperience();
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . Please try again!", this, PanelEmploymentHistory);
                return;
            }
        }
        //update staff work experience details
        private void UpdateStaffWorkWorkExperience()
        {
            ModalPopupExtenderAddWorkExperience.Show();
            try
            {
                //check for errors
                string _error = ValidateStaffWorkExperienceControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelEmploymentHistory);
                    return;
                }
                else
                {
                    int _staffWorkExpereinceID = Convert.ToInt32(HiddenFieldWorkExperienceID.Value);
                    StaffWorkExperience updateWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _staffWorkExpereinceID);
                    updateWorkExperience.EmployerName = txtWorkExperienceEmployerName.Text.Trim();
                    updateWorkExperience.PositionHeld = txtWorkExperiencePositionHeld.Text.Trim();
                    updateWorkExperience.StartDateMonth = ddlWorkExperienceStartDateMonth.SelectedValue;
                    updateWorkExperience.StartDateYear = ddlWorkExperienceStartDateYear.SelectedValue;
                    updateWorkExperience.EndDateMonth = ddlWorkExperienceEndDateMonth.SelectedValue;
                    updateWorkExperience.EndDateYear = ddlWorkExperienceEndDateYear.SelectedValue;
                    updateWorkExperience.Supervisor = txtSupervisor.Text.Trim();
                    updateWorkExperience.TerminationReason = txtTerminationReason.Text.Trim();
                    updateWorkExperience.IsEmployerVerified = cbEmployerVerified.Checked;
                    //if (cbWasEmployerEADB.Checked == true)
                    //{
                    //    updateWorkExperience.WasEmployerEADB = true;
                    //    updateWorkExperience.EADBContractRef = txtEADBContractReference.Text.Trim();
                    //    updateWorkExperience.EADBContractDate = Convert.ToDateTime(txtEADBContractDate.Text.Trim()).Date;
                    //}
                    //else
                    //{
                    //    updateWorkExperience.WasEmployerEADB = false;
                    //    updateWorkExperience.EADBContractRef = "";
                    //    updateWorkExperience.EADBContractDate = null;
                    //}
                    db.SubmitChanges();
                    lbWorkExperienceError.Text = "";
                    //display work experience records
                    DisplayWorkExperience();
                    ModalPopupExtenderAddWorkExperience.Hide();
                    _hrClass.LoadHRManagerMessageBox(2, "Staff work experience record has been successfully updated!", this, PanelEmploymentHistory);
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed . Please try again!", this, PanelEmploymentHistory);
                return;
            }
        }
        //save staff work experience record
        protected void lnkBtnSaveWorkExperience_Click(object sender, EventArgs e)
        {
            SaveStaffWorkWorkExperience();
            return;
        }
        //update staff work experience record
        protected void lnkBtnUpdateWorkExperience_Click(object sender, EventArgs e)
        {
            UpdateStaffWorkWorkExperience();
            return;
        }
        protected void lnkBtnClearWorkExperience_Click(object sender, EventArgs e)
        {
            ClearAddWorkExperienceControls();
            ModalPopupExtenderAddWorkExperience.Show();
        }
        //check if a staff is selected before edit details controls are loaded
        protected void LinkButtonEditWorkExperience_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvEmploymentHistory))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelEmploymentHistory);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one recoed to edit at a time!", this, PanelEmploymentHistory);
                        return;
                    }
                    foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFWorkExperienceID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit work experience details
                            LoadEditStaffWorkExperienceControls(Convert.ToInt32(_HFWorkExperienceID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading staff work experience details for edit! " + ex.Message.ToString(), this, PanelEmploymentHistory);
            }
        }
        //load work experience record for edit
        private void LoadEditStaffWorkExperienceControls(int _staffWorkExperienceID)
        {
            StaffWorkExperience getWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _staffWorkExperienceID);
            HiddenFieldWorkExperienceID.Value = _staffWorkExperienceID.ToString();
            txtWorkExperienceEmployerName.Text = getWorkExperience.EmployerName;
            txtWorkExperiencePositionHeld.Text = getWorkExperience.PositionHeld;
            _hrClass.GenerateMonths(ddlWorkExperienceStartDateMonth);//start date months
            _hrClass.GenerateYears(ddlWorkExperienceStartDateYear);//start date years
            _hrClass.GenerateMonths(ddlWorkExperienceEndDateMonth);//end date months
            _hrClass.GenerateYears(ddlWorkExperienceEndDateYear);//end date years
            ddlWorkExperienceStartDateMonth.SelectedValue = getWorkExperience.StartDateMonth;
            ddlWorkExperienceStartDateYear.SelectedValue = getWorkExperience.StartDateYear;
            ddlWorkExperienceEndDateMonth.SelectedValue = getWorkExperience.EndDateMonth;
            ddlWorkExperienceEndDateYear.SelectedValue = getWorkExperience.EndDateYear;
            txtSupervisor.Text = getWorkExperience.Supervisor;
            txtTerminationReason.Text = getWorkExperience.TerminationReason;
            cbEmployerVerified.Checked = (bool)getWorkExperience.IsEmployerVerified;
            //if (getWorkExperience.WasEmployerEADB == true)
            //{
            //    cbWasEmployerEADB.Checked = true;
            //    txtEADBContractReference.Text = getWorkExperience.EADBContractRef;
            //    txtEADBContractDate.Text = _hrClass.ShortDateDayStart(getWorkExperience.EADBContractDate.ToString());
            //}
            //else
            //{
            //    cbWasEmployerEADB.Checked = false;
            //    txtEADBContractReference.Text = "";
            //    txtEADBContractDate.Text = "";
            //}
            lnkBtnSaveWorkExperience.Visible = false;
            lnkBtnClearWorkExperience.Visible = true;
            lnkBtnUpdateWorkExperience.Enabled = true;
            lbAddWorkExperienceHeader.Text = "Edit Employment History";
            ImageAddWorkExperienceHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbWorkExperienceError.Text = "";
            lbWorkExperienceInfo.Text = "";
            ModalPopupExtenderAddWorkExperience.Show();
            return;
        }
        //display staff work expreience details
        private void DisplayWorkExperience()
        {
            try
            {
                object _display;
                Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                _display = db.PROC_StaffWorkExperience(_staffID);
                gvEmploymentHistory.DataSourceID = null;
                gvEmploymentHistory.DataSource = _display;
                Session["gvEmploymentHistoryData"] = _display;
                gvEmploymentHistory.DataBind();
                return;
            }
            catch { }
        }

        protected void gvEmploymentHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvEmploymentHistory.PageIndex = e.NewPageIndex;
            Session["gvEmploymentHistoryPageIndex"] = e.NewPageIndex;//get page index
            gvEmploymentHistory.DataSource = (object)Session["gvEmploymentHistoryData"];
            gvEmploymentHistory.DataBind();
            return;
        }
        protected void gvEmploymentHistory_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvEmploymentHistoryPageIndex"] == null)//check if page index is empty
                {
                    gvEmploymentHistory.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvEmploymentHistoryPageIndex"]);
                    gvEmploymentHistory.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //load delete work experience details
        protected void LinkButtonDeleteWorkExperience_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvEmploymentHistory))//check if there is any work experience record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select record(s) to delete!", this, PanelEmploymentHistory);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected work experience record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected work experience records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelEmploymentHistory);
                return;
            }
        }
        //delete work experience record
        private void DeleteWorkExperience()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvEmploymentHistory.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFWorkExperienceID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _workExperienceID = Convert.ToInt32(_HFWorkExperienceID.Value);
                        StaffWorkExperience deleteWorkExperience = db.StaffWorkExperiences.Single(p => p.StaffWorkExperienceID == _workExperienceID);
                        db.StaffWorkExperiences.DeleteOnSubmit(deleteWorkExperience);
                        db.SubmitChanges();
                    }
                }
                //refresh work experience records after the delete is done
                DisplayWorkExperience();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected work experience record has been deleted.", this, PanelEmploymentHistory);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected work experience records have been deleted.", this, PanelEmploymentHistory);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelEmploymentHistory);
                return;
            }
        }
        //delete records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteWorkExperience();//delete selected work experience record(s)bIndex == 5)//check if we are on the employment tab
            return;
        }
    }
}