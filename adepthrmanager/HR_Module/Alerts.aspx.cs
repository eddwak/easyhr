﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using AdeptHRManager.HRManagerClasses;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;


namespace AdeptHRManager.HR_Module
{
    public partial class Alerts : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRModule _hrModule = new HRModule();
        DateTime _TodayDate = System.DateTime.Today;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSearchedStaffOnPageLoad();
            }

        }

        private void GetSearchedStaffOnPageLoad()
        {
            try
            {
                string _staffID = Request["ref"];
                if (_staffID != "" && _staffID != null)
                {
                   // LoadEditStaffRecordControls();
                    HttpContext.Current.Session["SearchedStaffID"] = _staffID;
                    return;
                }
                else
                {
                    var _searchedStaffID = HttpContext.Current.Session["SearchedStaffID"];
                    if (_searchedStaffID != null)
                    {
                        _hrClass.PopupateStaffModuleHeader(_searchedStaffID.ToString(), HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                        Guid _staffSearchedID = _hrClass.ReturnGuid(_searchedStaffID.ToString());
                        DisplayContractEnd(_staffSearchedID);
                        DisplayProbationEnd(_staffSearchedID);
                        DisplayRetirementEnd(_staffSearchedID);
                        DisplayTenancyEnd(_staffSearchedID);
                        DisplayCarEnd(_staffSearchedID);
                        DisplayWorkPermitEnd(_staffSearchedID);
                        DisplayDependentPassExpiry(_staffSearchedID);
                        DisplayVisaExpiry(_staffSearchedID);
                    }
                }
            }
            catch { }
        }

        #region 'SEARCH FOR A STAFF'
        //SEARCH FOR A STAFF

        //search for staff by staff reference number
        private void SearchForStaffByStaffReference()
        {
            try
            {
                string _searchText = txtSearchByStaffReference.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffReference(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no staff found with the reference number entered.", this, PanelAlerts);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAlerts);
                return;
            }
        }
        //search for staff by staff reference 
        protected void txtSearchByStaffReference_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByStaffReference();
            return;
        }
        //search for staff by staff name
        private void SearchForStaffByStaffName()
        {
            try
            {
                string _searchText = txtSearchByStaffName.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByStaffName(_searchText, HiddenFieldStaffID, txtSearchByStaffReference, txtSearchByIDNumber, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the name entered.", this, PanelAlerts);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAlerts);
                return;
            }
        }
        //search for staff by staff name
        protected void txtSearchByStaffName_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByStaffName();
            return;
        }
        //search for staff by satff national id number
        private void SearchForStaffByIDNumber()
        {
            try
            {
                string _searchText = txtSearchByIDNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByIDNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByMobileNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the ID number entered.", this, PanelAlerts);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAlerts);
                return;
            }
        }
        //search for staff by id number
        protected void txtSearchByIDNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByIDNumber();
            return;
        }
        //search for staff by satff mobile number
        private void SearchForStaffByMobileNumber()
        {
            try
            {
                string _searchText = txtSearchByMobileNumber.Text.Trim().ToLower();
                string _staffID = _hrClass.GetSearchStaffIDByMobileNumber(_searchText, HiddenFieldStaffID, txtSearchByStaffName, txtSearchByStaffReference, txtSearchByIDNumber, ImageStaffPhoto);
                if (_staffID != "")
                {
                    Guid _StaffID = _hrClass.ReturnGuidForSearchedStaffID(_staffID);
                    LoadStaffRecordControls(_StaffID);//load all staff record controls
                    return;
                }
                else
                {
                    LoadStaffRecordControls(Guid.Empty);
                    _hrClass.LoadHRManagerMessageBox(1, "There is no employee found with the mobile number entered.", this, PanelAlerts);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelAlerts);
                return;
            }
        }
        //search for staff by mobile number
        protected void txtSearchByMobileNumber_TextChanged(object sender, EventArgs e)
        {
            SearchForStaffByMobileNumber();
            return;
        }
        //load selected staff details
        private void LoadStaffRecordControls(Guid _staffID)
        {
            HiddenFieldStaffID.Value = _staffID.ToString();
            //get details associated with the staff id
            if (_staffID != Guid.Empty)
            {
                //load all staff record contols
                LoadAllStaffRecordCointrols(_staffID);
                return;
            }
            else
            {
                //clear entry controls
                ClearAllStaffRecordControls();
                return;
            }

        }
        //load all controls staff record 
        private void LoadAllStaffRecordCointrols(Guid _staffID)
        {
            //LoadStaffPhoto(_staffID);//load staff photo
            LoadStaffPersonalInformation(_staffID);//load staff personal information
                   
            return;
        }
        //clear all staff contraols
        private void ClearAllStaffRecordControls()
        {
            ClearAddStaffControls();//clear personal information
      
            return;
        }
        //load staff personal information
        private void LoadStaffPersonalInformation(Guid _staffID)
        {
            StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID == _staffID);
            
        }

        #endregion

        #region 'STAFF IMAGE UPLOADER'
        //load upload staff image controls
        private void LoadUploadStaffImageControls()
        {
            lblMsg.Text = "";
            imgUpload.ImageUrl = "";
            btnCrop.Visible = false;
            ModalPopupExtenderUploadStaffImage.Show();
            return;
        }
        protected void LinkButtonUploadStaffphoto_Click(object sender, EventArgs e)
        {
            LoadUploadStaffImageControls(); return;
        }
        //method for uploading staff image
        private void UploadStaffImage()
        {

            ModalPopupExtenderUploadStaffImage.Show();
            //try
            //{
            // Upload Original Image Here
            string uploadFileName = "";
            string uploadFilePath = "";
            if (FileUploadStaffPhoto.HasFile)
            {
                string ext = Path.GetExtension(FileUploadStaffPhoto.FileName).ToLower();
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".gif" || ext == ".png")
                {
                    uploadFileName = Guid.NewGuid().ToString() + ext;
                    uploadFilePath = Path.Combine(Server.MapPath("~/UploadImages"), uploadFileName);
                    try
                    {
                        FileUploadStaffPhoto.SaveAs(uploadFilePath);

                        string _fileLocation = Server.MapPath("~/UploadImages/" + uploadFileName);
                        FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                        //get image info
                        FileInfo documentInfo = new FileInfo(_fileLocation);
                        int fileSize = (int)documentInfo.Length;//get file size
                        byte[] binaryImage = new byte[fileSize];
                        fs.Read(binaryImage, 0, fileSize);//read the document from the file stream
                        fs.Close();
                        HandleImageUpload(binaryImage, uploadFileName);

                        imgUpload.ImageUrl = "~/UploadImages/" + uploadFileName;
                        //panCrop.Visible = true;
                        btnCrop.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        lblMsg.Text = "Error! Please try again.";
                    }
                }
                else
                {
                    lblMsg.Text = "Selected file type not allowed!";
                }
            }
            else
            {
                lblMsg.Text = "Please select file first!";
            }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Upload failed.<br>" + ex.Message.ToString() + ". Please try again!", this, PanelAlerts);
            //    return;
            //}
        }
        //upload the image
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            UploadStaffImage(); return;
        }
        //method for croping the image uploaded
        private void CropAndSaveUploadedStaffImage()
        {
            ModalPopupExtenderUploadStaffImage.Show();
            try
            {
                // Crop Image Here & Save
                string fileName = Path.GetFileName(imgUpload.ImageUrl);
                string filePath = Path.Combine(Server.MapPath("~/UploadImages"), fileName);
                string cropFileName = "";
                string cropFilePath = "";
                if (File.Exists(filePath))
                {
                    if (hfH.Value == "" && hfW.Value == "" && hfX.Value == "" && hfY.Value == "")
                    {
                        lblMsg.Text = "Crop an image sectopn to save has the staff photo!";
                        return;
                    }
                    else
                    {
                        System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                        Rectangle CropArea = new Rectangle(
                            Convert.ToInt32(hfX.Value),
                            Convert.ToInt32(hfY.Value),
                            Convert.ToInt32(hfW.Value),
                            Convert.ToInt32(hfH.Value));

                        try
                        {
                            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                            using (Graphics g = Graphics.FromImage(bitMap))
                            {
                                g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                            }
                            cropFileName = "crop_" + fileName;
                            cropFilePath = Path.Combine(Server.MapPath("~/UploadImages"), cropFileName);
                            bitMap.Save(cropFilePath);

                            //get file streem of the croped image
                            FileStream fs = new FileStream(cropFilePath, FileMode.Open);//
                            //get document info
                            FileInfo documentInfo = new FileInfo(cropFilePath);
                            int fileSize = (int)documentInfo.Length;//get file size
                            byte[] fileDocument = new byte[fileSize];
                            fs.Read(fileDocument, 0, fileSize);//read the document from the file stream
                            fs.Close();

                            //save the photo in the staff master table
                            Guid _staffID = _hrClass.ReturnGuid(HiddenFieldStaffID.Value);
                            StaffMaster saveStaffPhoto = db.StaffMasters.Single(p => p.StaffID == _staffID);
                            saveStaffPhoto.StaffPhoto = fileDocument;
                            db.SubmitChanges();
                            ModalPopupExtenderUploadStaffImage.Hide();
                            _hrClass.LoadHRManagerMessageBox(2, "Staff photo has been successfully saved!", this, PanelAlerts);
                            //display staff photo uploaded
                            LoadStaffPhoto(_staffID);
                            return;
                            //Response.Redirect("~/UploadImages/" + cropFileName, false);
                        }

                        catch (Exception ex)
                        {
                            //throw;
                            lblMsg.Text = "Crop failed." + ex.Message.ToString();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while croping the uploaded image.<br>" + ex.Message.ToString() + ". Please try again!", this, PanelAlerts);
                return;
            }
        }
        //crop and save uploaded staff image
        protected void btnCrop_Click(object sender, EventArgs e)
        {
            CropAndSaveUploadedStaffImage(); return;
        }

        //resize the picrure uploaded to a fixed size
        public static System.Drawing.Image FixedSize(System.Drawing.Image image, int Width, int Height, bool needToFill)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;
            //check if the width is greater
            if (sourceWidth > Width && sourceHeight > Height)
            {
                nScaleW = ((double)Width / (double)sourceWidth);
                nScaleH = ((double)Height / (double)sourceHeight);
            }
            else//less photo width, set scale to 1
            {
                nScaleW = 1;
                nScaleH = 1;
            }
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);

            System.Drawing.Bitmap bmPhoto = null;
            try
            {
                bmPhoto = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, Width, Height), ex);
            }
            using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;

                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);

                return bmPhoto;
            }
        }

        private MemoryStream BytearrayToStream(byte[] arr)
        {
            return new MemoryStream(arr, 0, arr.Length);
        }

        private void HandleImageUpload(byte[] binaryImage, string _fileName)
        {
            //using (System.Drawing.Image img = RezizeImage(System.Drawing.Image.FromStream(BytearrayToStream(binaryImage)), 305, 372))
            using (System.Drawing.Image img = FixedSize(System.Drawing.Image.FromStream(BytearrayToStream(binaryImage)), 305, 372, true))
            {
                img.Save(Server.MapPath("~/UploadImages/" + _fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
            }

        }
        //load staff photo by checking if the employee has a photo
        private void LoadStaffPhoto(Guid _staffID)
        {
            PROC_GetStaffPhotoResult getStaffPhoto = db.PROC_GetStaffPhoto(_staffID).FirstOrDefault();
            if (getStaffPhoto.StaffPhoto != null)
            {
                ImageStaffPhoto.ImageUrl = "~/HRManagerClasses/HRManagerHandler.ashx?staffID=" + _staffID;
                LinkButtonUploadStaffphoto.Text = "Change Staff Photo";
            }
            else
            {
                LinkButtonUploadStaffphoto.Text = "Upload Staff Photo";
                ImageStaffPhoto.ImageUrl = "~/images/background/person.png";
            }
        }
        #endregion

        #region 'LOAD ALERTS'

        private void ClearAddStaffControls()
        {
            //clear entry controls
            //_hrClass.ClearEntryControls(PanelStaffModuleHeader);
            //_hrClass.ClearEntryControls(TabPanelPersonalInformation);
          
            ImageStaffPhoto.ImageUrl = "~/images/background/person.png";
            HttpContext.Current.Session["SearchedStaffID"] = null;
            return;
        }

        private void DisplayAlerts(Guid _staffID)
        {


        }
        //Display Contract End Date
        private void DisplayContractEnd(Guid _staffID)
        {
            try
            {
                PROC_GetContractEndResult GetContractEnd = db.PROC_GetContractEnd(_staffID).FirstOrDefault();
                if (GetContractEnd.StaffID != null)
                { 
                    DateTime _ContractEnd=Convert.ToDateTime(GetContractEnd.EndContractDate);
                    int _ContractEndDifference = (_ContractEnd - _TodayDate).Days;
                   
                    if (_ContractEndDifference <=120)
                    {
                    
                    lblContractEnd.Text = "The contract end date is " + _hrClass.ShortDateDayStart(GetContractEnd.EndContractDate.ToString()); 
                    lblContractEnd.Visible = true;
                    }
                }
                else
                {
                    lblContractEnd.Visible = false;
                }
                return;
            }
            catch { }
        }
        //Probation End Date
        private void DisplayProbationEnd(Guid _staffID)
        {
            try
            {
                StaffJobDetail getStaffJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);
                

                if (getStaffJobDetail.StaffID != null)
                {
                    DateTime _ProbationEnd = Convert.ToDateTime(getStaffJobDetail.ProbationEndDate);
                    int _ProbationEndDifference = (_ProbationEnd - _TodayDate).Days;
                    if (_ProbationEndDifference <= 90)
                    {

                        lblProbationEnd.Text = "The probation end date is " + _hrClass.ShortDateDayStart(getStaffJobDetail.ProbationEndDate.ToString());
                        lblProbationEnd.Visible = true;
                    }
                }
                else
                {
                    lblProbationEnd.Visible = false;
                }
                return;
            }
            catch { }
        }
        //Retirement End Date
        private void DisplayRetirementEnd(Guid _staffID)
        {
            try
            {
                EmpTerm getEmpTerm = db.EmpTerms.FirstOrDefault(p => p.empterms_uiStaffID == _staffID);

                if (getEmpTerm.empterms_uiStaffID != null)
                {
                    DateTime _RetirementEnd = Convert.ToDateTime(getEmpTerm.empterms_RetirementDate);
                    int _RetirementEndDifference = (_RetirementEnd - _TodayDate).Days;
                    if (_RetirementEndDifference <= 210)
                    {

                        lblRetirementEnd.Text = "The retirement end date is " + _hrClass.ShortDateDayStart(getEmpTerm.empterms_RetirementDate.ToString());
                        lblRetirementEnd.Visible = true;
                    }
                }
                else
                {
                    lblRetirementEnd.Visible = false;
                }
                return;
            }
            catch { }
        }

        //Tenancy Agreement End Date
        private void DisplayTenancyEnd(Guid _staffID)
        {
            try
            {
                StaffHouse getStaffHouse = db.StaffHouses.Single(p => p.StaffID == _staffID);
                


                if (getStaffHouse.StaffID != null)
                {
                    DateTime _TenancyEnd = Convert.ToDateTime(getStaffHouse.TenancyAgreementEndDate);
                    int _TenancyEndDifference = (_TenancyEnd - _TodayDate).Days;
                    if (_TenancyEndDifference <= 60)
                    {

                        lblTenancyAgreement.Text = "The tenancy agreement end date is " + _hrClass.ShortDateDayStart(getStaffHouse.TenancyAgreementEndDate.ToString());
                        lblTenancyAgreement.Visible = true;
                    }
                    else
                    {
                        lblTenancyAgreement.Visible = false;
                    }
                }
               
                return;
            }
            catch { }
        }
        //Tenancy Agreement End Date
        private void DisplayCarEnd(Guid _staffID)
        {
            try
            {
                StaffVehicle getStaffVehicle = db.StaffVehicles.Single(p => p.StaffID == _staffID);

                if (getStaffVehicle.StaffID != null)
                {
                    DateTime _CarEnd = Convert.ToDateTime(getStaffVehicle.VehicleAgreementEndDate);
                    int _CarEndDifference = (_CarEnd - _TodayDate).Days;
                    if (_CarEndDifference <= 30)
                    {

                        lblCarAgreement.Text = "The vehicle agreement end date is " + _hrClass.ShortDateDayStart(getStaffVehicle.VehicleAgreementEndDate.ToString());
                        lblCarAgreement.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }
              
                return;
            }
            catch { }
        }
        //Display Work Permit End Date
        private void DisplayWorkPermitEnd(Guid _staffID)
        {
            try
            {
                PROC_GetContractWorkPermitExpiryResult GetWorkPermitEnd = db.PROC_GetContractWorkPermitExpiry(_staffID).FirstOrDefault();
                if (GetWorkPermitEnd.StaffID != null)
                {
                    DateTime _WorkPermitEnd = Convert.ToDateTime(GetWorkPermitEnd.JobWorkPermitExpiryDate);
                    int _WorkPermitEndDifference = (_WorkPermitEnd - _TodayDate).Days;
                    if (_WorkPermitEndDifference <= 60)
                    {
                        lblWorkPermitEnd.Text = "The work permit end date is " + _hrClass.ShortDateDayStart(GetWorkPermitEnd.JobWorkPermitExpiryDate.ToString());
                        lblWorkPermitEnd.Visible = true;
                    }
                    else
                    {
                        lblWorkPermitEnd.Visible = false;
                    }
                }
               
                return;
            }
            catch { }
        }

        //Display VISA expiry date 
        private void DisplayVisaExpiry(Guid _staffID)
        {
            try
            {
                StaffJobDetail getJobDetail = db.StaffJobDetails.Single(p => p.StaffID == _staffID);

                if (getJobDetail.StaffID != null)
                {
                    DateTime _VisaExpiry = Convert.ToDateTime(getJobDetail.VisaExpiryDate);
                    int _VisaExpiryDifference = (_VisaExpiry - _TodayDate).Days;
                    if (_VisaExpiryDifference <= 60)
                    {

                        lblVisaRenewal.Text = "The Visa expiry date is " + _hrClass.ShortDateDayStart(getJobDetail.VisaExpiryDate.ToString());
                        lblVisaRenewal.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }
               
                return;
            }
            catch { }
        }
            

        //Display Dependants Pass Expiry Date
        private void DisplayDependentPassExpiry(Guid _staffID)
        {
            try
            {
                Spouse getSpouse = db.Spouses.FirstOrDefault(p => p.spouse_uiStaffID == _staffID);

                if (getSpouse.spouse_uiStaffID != null)
                {
                    DateTime _DependentPassExpiry = Convert.ToDateTime(getSpouse.spouse_vPassExpiryDate).Date;
                    int _DependentPassExpiryDifference = (_DependentPassExpiry - _TodayDate).Days;
                    if (_DependentPassExpiryDifference <= 60)
                    {

                        lblDependentPassExpiry.Text = "The dependents' expiry date is " + getSpouse.spouse_vPassExpiryDate.ToString();
                        lblDependentPassExpiry.Visible = true;
                    }
                    else
                    {
                        lblCarAgreement.Visible = false;
                    }
                }
               
                return;
            }
            catch { }
        }
        #endregion




    }
}