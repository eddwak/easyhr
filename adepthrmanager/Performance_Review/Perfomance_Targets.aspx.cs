﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Performance_Review
{
    public partial class Perfomance_Targets : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlDepartment, 3000, "Department");//display departments(3000) available.
                txtYear.Text = DateTime.Now.Year.ToString();//populate the year with the current year
            }
        }
        //display staff names per the selected department
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string _targetYear = ddlYear.SelectedValue;
                int _departmentID = 0;
                if (ddlDepartment.SelectedIndex != 0)
                    _departmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                _hrClass.GetStaffNames(ddlStaffName, "Staff Name", _departmentID);//populate  staff name dropdown that are under the selected department
                //clear controls
                //_hrClass.ClearEntryControls(PanelAddStaffTargetAndAppraisalDetails);
                //HiddenFieldStaffTargetID.Value = string.Empty;
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceTargets);
                return;
            }
        }
        //display staff targets and appraisal details for the selected staff during for the year selected 
        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string _targetYear = ddlYear.SelectedValue;
                //int _departmentID = 0;
                //if (ddlDepartment.SelectedIndex != 0)
                //    _departmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                Guid _staffID = Guid.Empty;
                if (ddlStaffName.SelectedIndex != 0)
                    _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                //loda target/appraisal details
                //LoadSelectedStaffYearTargetAndAppraisal(_targetYear, _staffID);
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelPerformanceTargets);
                return;
            }
        }
    }
}