﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.Performance_Review
{

    public partial class Performance_Review_Reports : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerReports _hrReports = new HRManagerReports();
        ReportDataSource _reportSource = new ReportDataSource();
        DataTable dt = new DataTable();
        ReportDocument _performanceReviewReport = new ReportDocument();
        string reportPath = "";
        //page init method for viewing performance reports
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["ReportType"] != null)
                    LoadPerformanceReports();
            }
            catch { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropdowns();//populate reports drop downs
            }

        }
        //load dropdowns
        private void PopulateDropdowns()
        {
            _hrClass.GenerateYears(ddlOrganisationPerformancePerYear);//display years
            _hrClass.GetListingItems(ddlDepartmentalPerformanceReportDepartmentName, 3000, "Department");//display departments(3000) available
            _hrClass.GenerateYears(ddlDepartmentalPerformanceReportPerYear);//display years
            _hrClass.GetStaffNames(ddlSingleStaffPerformanceReportStaffName, "Staff Name");//populate staff name
            _hrClass.GenerateYears(ddlSingleStaffPerformanceReportPerYear);//display years
        }
        //check if we are viewing organisation's report for a particular year
        protected void cbOrganisationPerformancePerYear_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderOrganisationPerformanceReport.Show();
            ddlOrganisationPerformancePerYear.SelectedIndex = 0;
            if (cbOrganisationPerformancePerYear.Checked == true)
            {
                ddlOrganisationPerformancePerYear.Enabled = true;
            }
            else if (cbOrganisationPerformancePerYear.Checked == false)
            {
                ddlOrganisationPerformancePerYear.Enabled = false;
            }
        }
        //method for viewing an organization's performaance report
        private void ViewOrganisationPerformanceReport()
        {
            ModalPopupExtenderOrganisationPerformanceReport.Show();
            try
            {
                if (cbOrganisationPerformancePerYear.Checked == true && ddlOrganisationPerformancePerYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select the year!", this, PanelPerformanceReviewReports);
                    return;
                }
                else
                {
                    //check if we are viewing all organizatuion perfomance report
                    if (cbOrganisationPerformancePerYear.Checked == false)//for all years
                    {
                        Session["ReportType"] = "AllOrganizationPerformanceReport";
                        Session["ReportTitle"] = "Organisation Performance Report";
                    }
                    else if (cbOrganisationPerformancePerYear.Checked == true)//for a selected year
                    {
                        string _year = ddlOrganisationPerformancePerYear.SelectedValue.ToString();
                        Session["ReportOrganisationPerfomanceYear"] = _year;
                        Session["ReportType"] = "OrganisationPerformancePerYear";
                        Session["ReportTitle"] = "Organisation Performance Report for the Year " + _year;
                    }
                    ModalPopupExtenderOrganisationPerformanceReport.Hide();
                    LoadPerformanceReports();//view report
                    TabContainerPerformanceReviewReports.ActiveTabIndex = 1;//report viewer
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReviewReports);
                return;
            }
        }
        //view organisation's Performance report
        protected void lnkBtnViewOrganisationPerformanceReport_Click(object sender, EventArgs e)
        {
            ViewOrganisationPerformanceReport();
            return;
        }

        //check if we are viewing selected deaprtment's report for a particular year
        protected void cbDepartmentalReportPerYear_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderDepartmentPerformanceReport.Show();
            ddlDepartmentalPerformanceReportPerYear.SelectedIndex = 0;
            if (cbDepartmentalPerformanceReportPerYear.Checked == true)
            {
                ddlDepartmentalPerformanceReportPerYear.Enabled = true;
            }
            else if (cbDepartmentalPerformanceReportPerYear.Checked == false)
            {
                ddlDepartmentalPerformanceReportPerYear.Enabled = false;
            }
        }
        //method for viewing an performaance report per department
        private void ViewDepartmentalPerformanceReport()
        {
            ModalPopupExtenderDepartmentPerformanceReport.Show();
            try
            {
                if (ddlDepartmentalPerformanceReportDepartmentName.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No department selected. Select a department!", this, PanelPerformanceReviewReports);
                    return;
                }
                else if (cbDepartmentalPerformanceReportPerYear.Checked == true && ddlDepartmentalPerformanceReportPerYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select the year!", this, PanelPerformanceReviewReports);
                    return;
                }
                else
                {
                    string _departmentName = ddlDepartmentalPerformanceReportDepartmentName.SelectedItem.ToString();
                    Session["ReportDepartmentPerformanceName"] = _departmentName;
                    //check if we are viewing all perfomance report
                    if (cbDepartmentalPerformanceReportPerYear.Checked == false)//for all years
                    {
                        Session["ReportType"] = "DepartmentalPerformanceReport";
                        Session["ReportTitle"] = _departmentName + " Department Performance Report";
                    }
                    else if (cbDepartmentalPerformanceReportPerYear.Checked == true)//for a selected year
                    {
                        string _year = ddlDepartmentalPerformanceReportPerYear.SelectedValue.ToString();
                        Session["ReportDepartmentPerfomanceYear"] = _year;
                        Session["ReportType"] = "DepartmentalPerformancePerYear";
                        Session["ReportTitle"] = _departmentName + " Department Performance Report for the Year " + _year;
                    } ModalPopupExtenderDepartmentPerformanceReport.Hide();
                    LoadPerformanceReports();//view report
                    TabContainerPerformanceReviewReports.ActiveTabIndex = 1;//report viewer
                    //return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReviewReports);
                return;
            }
        }
        //view Performance report for a particular department
        protected void lnkBtnViewDepartmentPerformanceReport_Click(object sender, EventArgs e)
        {
            ViewDepartmentalPerformanceReport();
            return;
        }
        //check if we are viewing selected staff's report for a particular year
        protected void cbSingleStaffPerformanceReportPerYear_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderSingleStaffPerformanceReport.Show();
            ddlSingleStaffPerformanceReportPerYear.SelectedIndex = 0;
            if (cbSingleStaffPerformanceReportPerYear.Checked == true)
            {
                ddlSingleStaffPerformanceReportPerYear.Enabled = true;
            }
            else if (cbSingleStaffPerformanceReportPerYear.Checked == false)
            {
                ddlSingleStaffPerformanceReportPerYear.Enabled = false;
            }
        }
        //method for viewing an performaance report for a particular staff
        private void ViewSingleStaffPerformanceReport()
        {
            ModalPopupExtenderSingleStaffPerformanceReport.Show();
            try
            {
                if
                    (ddlSingleStaffPerformanceReportStaffName.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No staff selected. Select sfaff!", this, PanelPerformanceReviewReports);
                    return;
                }
                else if (cbSingleStaffPerformanceReportPerYear.Checked == true && ddlSingleStaffPerformanceReportPerYear.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select the year!", this, PanelPerformanceReviewReports);
                    return;
                }
                else
                {
                    string _staffID = ddlSingleStaffPerformanceReportStaffName.SelectedValue.ToString();
                    Session["SingleStaffPerformanceStaffID"] = _staffID.ToString();
                    //check if we are viewing all perfomance report for the selected staff
                    if (cbSingleStaffPerformanceReportPerYear.Checked == false)//for all years
                    {
                        Session["ReportType"] = "SingleStaffPerformanceReport";
                        Session["ReportTitle"] = "Single Staff Performance Report";
                    }
                    else if (cbSingleStaffPerformanceReportPerYear.Checked == true)//for a selected year
                    {
                        string _year = ddlSingleStaffPerformanceReportPerYear.SelectedValue.ToString();
                        Session["ReportSingleStaffPerformanceYear"] = _year;
                        Session["ReportType"] = "SingleStaffPerformancePerYear";
                        Session["ReportTitle"] = "Single Staff Performance Report for the Year " + _year;
                    }
                    ModalPopupExtenderSingleStaffPerformanceReport.Hide();
                    LoadPerformanceReports();//view report
                    TabContainerPerformanceReviewReports.ActiveTabIndex = 1;//report viewer
                    //return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelPerformanceReviewReports);
                return;
            }
        }
        //view Performance report for a particular staff
        protected void lnkBtnViewSingleStaffPerformanceReport_Click(object sender, EventArgs e)
        {
            ViewSingleStaffPerformanceReport();
            return;
        }

        //method for loading performance reports
        private void LoadPerformanceReports()
        {

            if (Session["ReportType"].ToString() == "AllOrganizationPerformanceReport")//ALL ORGANIZATION PERFORMANCE DETAILS
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_AllOrganisationPerformance.rpt");
                _performanceReviewReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_AllOrganisationPerformance();
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
            else if (Session["ReportType"].ToString() == "OrganisationPerformancePerYear")//ORGANIZATION PERFORMANCE REPORT FOR A PARTICLUAR YEAR
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_OrganisationPerformancePerYear.rpt");
                _performanceReviewReport.Load(reportPath);
                string _year = Session["ReportOrganisationPerfomanceYear"].ToString();
                DataSet _reportData = _hrReports.rpt_OrganisationPerformancePerYear(_year);
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
            else if (Session["ReportType"].ToString() == "DepartmentalPerformanceReport")//ALL PERFORMANCE DETAILS FOR A PARTICULAR DEPARTMENT
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_PerformancePerDepartment.rpt");
                _performanceReviewReport.Load(reportPath);
                string _departmentName = Session["ReportDepartmentPerformanceName"].ToString();
                DataSet _reportData = _hrReports.rpt_PerformancePerDepartment(_departmentName);
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
            else if (Session["ReportType"].ToString() == "DepartmentalPerformancePerYear")//PERFORMANCE DETAILS FOR A PARTICULAR DEPARTMENT FOR A GIVEN YEAR
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_PerformancePerDepartmentPerYear.rpt");
                _performanceReviewReport.Load(reportPath);
                string _departmentName = Session["ReportDepartmentPerformanceName"].ToString();
                string _year = Session["ReportDepartmentPerfomanceYear"].ToString();
                DataSet _reportData = _hrReports.rpt_PerformancePerDepartmentPerYear(_departmentName, _year);
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
            else if (Session["ReportType"].ToString() == "SingleStaffPerformanceReport")//ALL PERFORMANCE DETAILS FOR A PARTICULAR STAFF
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_SingleStaffPerformance.rpt");
                _performanceReviewReport.Load(reportPath);
                Guid _staffID = _hrClass.ReturnGuid(Session["SingleStaffPerformanceStaffID"].ToString());
                DataSet _reportData = _hrReports.rpt_PerformancePerStaff(_staffID);
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
            else if (Session["ReportType"].ToString() == "SingleStaffPerformancePerYear")//PERFORMANCE DETAILS FOR A PARTICULAR STAFF FOR A GIVEN YEAR
            {
                reportPath = Server.MapPath("~/Performance_Review/Performance_Review_Report/report_SingleStaffPerformancePerYear.rpt");
                _performanceReviewReport.Load(reportPath);
                Guid _staffID = _hrClass.ReturnGuid(Session["SingleStaffPerformanceStaffID"].ToString());
                string _year = Session["ReportSingleStaffPerformanceYear"].ToString();
                DataSet _reportData = _hrReports.rpt_PerformancePerStaff(_staffID, _year);
                _performanceReviewReport.SetDataSource(_reportData);
                _performanceReviewReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerPerformanceReviewReports.ReportSource = _performanceReviewReport;
            }
        }

        //unload report
        protected void Page_Unload(object sender, EventArgs e)
        {
            _performanceReviewReport.Close();
            _performanceReviewReport.Dispose();
            return;
        }
        protected void CrystalReportViewerPerformanceReviewReports_Unload(object sender, EventArgs e)
        {
            _performanceReviewReport.Close();
            _performanceReviewReport.Dispose();
            return;
        }


    }
}