﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.Performance_Review
{
    public partial class Staff_Appraisal : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropDowns();//populate dropdowns
                DisplayStaffQuarterlyReviewDocuments();//dispaly quarterly review
                //_hrClass.GetStaffNames(ddlQuarter1AppraisedBy, "Appraised By");//populate  quarter 1 appraised by dropdown
                //_hrClass.GetStaffNames(ddlQuarter2AppraisedBy, "Appraised By");//populate  quarter 2 appraised by dropdown
                //_hrClass.GetStaffNames(ddlQuarter3AppraisedBy, "Appraised By");//populate  quarter 3 appraised by dropdown
                //_hrClass.GetStaffNames(ddlQuarter4AppraisedBy, "Appraised By");//populate  quarter 4 appraised by dropdown
            }
        }
        //populate drop downs
        private void PopulateDropDowns()
        {
            _hrClass.GetListingItems(ddlDepartment, 3000, "Department");//display departments(3000) available
            _hrClass.GenerateYears(ddlQuarterlyReviewYear);//display Quarterly review years
            _hrClass.GetStaffNames(ddlQuarterlyReviewStaffName, "Staff Name");//populate quarterly review staff name dropdown
            _hrClass.GetStaffNames(ddlQuarterlyReviewAppraisedBy, "Appraised By");//populate quarterly review appraised by dropdown

            _hrClass.GenerateYears(ddlAnnualAppraisalYear);//display annual appraisal years
            _hrClass.GetStaffNames(ddlAnnualAppraisalStaffName, "Staff Name");//populate annual appraisal staff name dropdown
            _hrClass.GetStaffNames(ddlAnnualAppraisalAppraisedBy, "Appraised By");//populate annual appraisal appraised by dropdown

            _hrClass.GetStaffNames(ddlEndOfProbationStaffName, "Staff Name");//populate end of probation staff name dropdown
            _hrClass.GetStaffNames(ddlEndOfProbationAppraisedBy, "Appraised By");//populate end of probation appraised by dropdown

            _hrClass.GenerateYears(ddlEndOfContractYear);//populate end of contract year drop down
            _hrClass.GetStaffNames(ddlEndOfContractStaffName, "Staff Name");//populate end of contract staff name dropdown
            _hrClass.GetStaffNames(ddlEndOfContractAppraisedBy, "Appraised By");//populate end of contract appraised by dropdown

        }
        #region QUATERLY REVIEW
        //load add new Quarterly review
        protected void LinkButtonNewQuarterlyReview_OnClick(object sender, EventArgs e)
        {
            ModalPopupExtenderAddQuarterlyReview.Show();
        }
        //display staff quarterly review documents
        private void DisplayStaffQuarterlyReviewDocuments()
        {
            try
            {
                object _display;
                _display = db.PROC_AllStaffQuarterlyReview();
                gvQuarterlyReviewDocuments.DataSourceID = null;
                gvQuarterlyReviewDocuments.DataSource = _display;
                gvQuarterlyReviewDocuments.DataBind();
                return;
            }
            catch { }
        }
        //load quartely review document details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvQuarterlyReviewDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFQuarterlyReviewID = (HiddenField)gvQuarterlyReviewDocuments.Rows[ID].FindControl("HiddenField1");
                    int _quarterlyReviewID = Convert.ToInt32(_HFQuarterlyReviewID.Value);
                    //if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
                    //{
                    //    HiddenFieldQuarterlyReviewFormID.Value = _quarterlyReviewID.ToString();
                    //    string _deleteMessage = "Are you sure you want to delete the quarterly review document?";
                    //    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    //}
                    //else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
                    {
                        string strPageUrl = "performance_document_Viewer.aspx?quarterlyreview=" + _HFQuarterlyReviewID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        #endregion

        #region ANNUAL APPRAISAL
        //load add new annual appraisal
        protected void LinkButtonNewAnnualAppraisal_OnClick(object sender, EventArgs e)
        {
            ModalPopupExtenderAddAnnualAppraisal.Show();
        }
        #endregion

        #region END OF PROBATION
        //load add new end of probation
        protected void LinkButtonNewEndOfProbation_OnClick(object sender, EventArgs e)
        {
            ModalPopupExtenderAddEndOfProbation.Show();
        }
        #endregion

        #region END OF CONTRACT
        //load add new end of contract
        protected void LinkButtonNewEndOfContact_OnClick(object sender, EventArgs e)
        {
            ModalPopupExtenderAddEndOfContract.Show();
        }
        #endregion
        //validate staff target/appraisal entry controls
        private string ValidateStaffTargetAndAppraisalControls()
        {
            ////if (ddlYear.SelectedIndex == 0)
            ////    return "Select target year!";
            ////else
            //if (ddlDepartment.SelectedIndex == 0)
            //    return "Select a department!";
            //else if (ddlStaffName.SelectedIndex == 0)
            //    return "Select staff name";
            //else if (txtQuarter1Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter1Target.Text.Trim()) == false)
            //    return "Invalid quarter one target value entered!";
            //else if (txtQuarter2Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter2Target.Text.Trim()) == false)
            //    return "Invalid quarter two target value entered!";
            //else if (txtQuarter3Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter3Target.Text.Trim()) == false)
            //    return "Invalid quarter three target value entered!";
            //else if (txtQuarter4Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter4Target.Text.Trim()) == false)
            //    return "Invalid quarter four target value entered!";
            //else if (txtQuarter1Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter1Actual.Text.Trim()) == false)
            //    return "Invalid quarter one actual value entered!";
            //else if (txtQuarter2Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter2Actual.Text.Trim()) == false)
            //    return "Invalid quarter two actual value entered!";
            //else if (txtQuarter3Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter3Actual.Text.Trim()) == false)
            //    return "Invalid quarter three actual value entered!";
            //else if (txtQuarter4Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter4Actual.Text.Trim()) == false)
            //    return "Invalid quarter four actual value entered!";
            //else if (txtQuarter1Target.Text.Trim() == "" && txtQuarter2Target.Text.Trim() == "" && txtQuarter3Target.Text.Trim() == "" && txtQuarter4Target.Text.Trim() == "" && txtQuarter1Actual.Text.Trim() == "" && txtQuarter2Actual.Text.Trim() == "" && txtQuarter3Actual.Text.Trim() == "" && txtQuarter4Actual.Text.Trim() == "")
            //{
            //    return "Save failed. There are no details entered to save against year selected!";
            //}
            //else return "";
            return "";
        }
        //save staff target/appraisal details
        private void SaveStaffTargetAndAppraisalDetails()
        {
            //try
            //{
            //    string _error = ValidateStaffTargetAndAppraisalControls();//check for error
            //    if (_error != "")
            //    {
            //        _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelStaffTargetAndAppraisal);
            //        return;
            //    }
            //    else
            //    {
            //        string _targetYear = ddlYear.SelectedValue.Trim();
            //        int _departmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
            //        Guid _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
            //        decimal _quarter1Target = 0, _quarter2Target = 0, _quarter3Target = 0, _quarter4Target = 0, _quarter1Actual = 0, _quarter2Actual = 0, _quarter3Actual = 0, _quarter4Actual = 0;
            //        if (txtQuarter1Target.Text.Trim() != "")
            //            _quarter1Target = Convert.ToDecimal(txtQuarter1Target.Text.Trim());
            //        if (txtQuarter2Target.Text.Trim() != "")
            //            _quarter2Target = Convert.ToDecimal(txtQuarter2Target.Text.Trim());
            //        if (txtQuarter3Target.Text.Trim() != "")
            //            _quarter3Target = Convert.ToDecimal(txtQuarter3Target.Text.Trim());
            //        if (txtQuarter4Target.Text.Trim() != "")
            //            _quarter4Target = Convert.ToDecimal(txtQuarter4Target.Text.Trim());
            //        if (txtQuarter1Actual.Text.Trim() != "")
            //            _quarter1Actual = Convert.ToDecimal(txtQuarter1Actual.Text.Trim());
            //        if (txtQuarter2Actual.Text.Trim() != "")
            //            _quarter2Actual = Convert.ToDecimal(txtQuarter2Actual.Text.Trim());
            //        if (txtQuarter3Actual.Text.Trim() != "")
            //            _quarter3Actual = Convert.ToDecimal(txtQuarter3Actual.Text.Trim());
            //        if (txtQuarter4Actual.Text.Trim() != "")
            //            _quarter4Actual = Convert.ToDecimal(txtQuarter4Actual.Text.Trim());
            //        string _targetComments = "", _actualComments = "";
            //        _targetComments = txtTargetComments.Text.Trim();
            //        _actualComments = txtActualComments.Text.Trim();
            //        //get appraised by staff id
            //        Guid _Q1AppraisedBy = Guid.Empty, _Q2AppraisedBy = Guid.Empty, _Q3AppraisedBy = Guid.Empty, _Q4AppraisedBy = Guid.Empty;
            //        if (ddlQuarter1AppraisedBy.SelectedIndex != 0)
            //            _Q1AppraisedBy = _hrClass.ReturnGuid(ddlQuarter1AppraisedBy.SelectedValue);
            //        if (ddlQuarter2AppraisedBy.SelectedIndex != 0)
            //            _Q2AppraisedBy = _hrClass.ReturnGuid(ddlQuarter2AppraisedBy.SelectedValue);
            //        if (ddlQuarter3AppraisedBy.SelectedIndex != 0)
            //            _Q3AppraisedBy = _hrClass.ReturnGuid(ddlQuarter3AppraisedBy.SelectedValue);
            //        if (ddlQuarter4AppraisedBy.SelectedIndex != 0)
            //            _Q4AppraisedBy = _hrClass.ReturnGuid(ddlQuarter4AppraisedBy.SelectedValue);
            //        //get sta response details
            //        string _Q1StaffResponse = "", _Q2StaffResponse = "", _Q3StaffResponse = "", _Q4StaffResponse = "";
            //        _Q1StaffResponse = txtQuarter1StaffResponse.Text.Trim();
            //        _Q2StaffResponse = txtQuarter2StaffResponse.Text.Trim();
            //        _Q3StaffResponse = txtQuarter3StaffResponse.Text.Trim();
            //        _Q4StaffResponse = txtQuarter4StaffResponse.Text.Trim();

            //        //check if there is a record with the selected year and department
            //        if (db.StaffAppraisals.Any(p => p.TargetYear == _targetYear && p.DepartmentID == _departmentID && p.StaffID == _staffID))
            //        {
            //            //update the existing year's record
            //            UpdateStaffTargetAndAppraisal(_targetYear, _departmentID, _staffID, _quarter1Target, _quarter2Target, _quarter3Target, _quarter4Target, _quarter1Actual, _quarter2Actual, _quarter3Actual, _quarter4Actual, _targetComments, _actualComments, _Q1AppraisedBy, _Q2AppraisedBy, _Q3AppraisedBy, _Q4AppraisedBy, _Q1StaffResponse, _Q2StaffResponse, _Q3StaffResponse, _Q4StaffResponse);
            //        }
            //        else
            //        {
            //            //save a new record for the year
            //            SaveNewStaffTargetAndAppraisal(_targetYear, _departmentID, _staffID, _quarter1Target, _quarter2Target, _quarter3Target, _quarter4Target, _quarter1Actual, _quarter2Actual, _quarter3Actual, _quarter4Actual, _targetComments, _actualComments, _Q1AppraisedBy, _Q2AppraisedBy, _Q3AppraisedBy, _Q4AppraisedBy, _Q1StaffResponse, _Q2StaffResponse, _Q3StaffResponse, _Q4StaffResponse);
            //        }
            //        LoadSelectedStaffYearTargetAndAppraisal(_targetYear, _staffID);//load staff target/appraisal details entered and calculate totals
            //        _hrClass.LoadHRManagerMessageBox(2, "Staff target/appraisal details have been successfully saved!", this, PanelStaffTargetAndAppraisal);
            //        return;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffTargetAndAppraisal);
            //    return;
            //}
        }
        //save new staff target/appraisal
        private void SaveNewStaffTargetAndAppraisal(string _targetYear, int _departmentID, Guid _staffID, decimal _quarter1Target, decimal _quarter2Target, decimal _quarter3Target, decimal _quarter4Target, decimal _quarter1Actual, decimal _quarter2Actual, decimal _quarter3Actual, decimal _quarter4Actual, string _targetComments, string _actualComments, Guid _Q1AppraisedBy, Guid _Q2AppraisedBy, Guid _Q3AppraisedBy, Guid _Q4AppraisedBy, string _Q1StaffResponse, string _Q2StaffResponse, string _Q3StaffResponse, string _Q4StaffResponse)
        {
            StaffAppraisal newTargetAndAppraisal = new StaffAppraisal();
            newTargetAndAppraisal.TargetYear = _targetYear;
            newTargetAndAppraisal.DepartmentID = _departmentID;
            newTargetAndAppraisal.StaffID = _staffID;
            //target details
            newTargetAndAppraisal.Quarter1Target = _quarter1Target;
            newTargetAndAppraisal.Quarter2Target = _quarter2Target;
            newTargetAndAppraisal.Quarter3Target = _quarter3Target;
            newTargetAndAppraisal.Quarter4Target = _quarter4Target;
            //acctual details
            newTargetAndAppraisal.Quarter1Actual = _quarter1Actual;
            newTargetAndAppraisal.Quarter2Actual = _quarter2Actual;
            newTargetAndAppraisal.Quarter3Actual = _quarter3Actual;
            newTargetAndAppraisal.Quarter4Actual = _quarter4Actual;
            //comments
            newTargetAndAppraisal.TargetComments = _targetComments;
            newTargetAndAppraisal.ActualComments = _actualComments;
            //appraised by details
            newTargetAndAppraisal.Quarter1AppraisedBy = _Q1AppraisedBy;
            newTargetAndAppraisal.Quarter2AppraisedBy = _Q2AppraisedBy;
            newTargetAndAppraisal.Quarter3AppraisedBy = _Q3AppraisedBy;
            newTargetAndAppraisal.Quarter4AppraisedBy = _Q4AppraisedBy;
            //staff response details
            newTargetAndAppraisal.Quarter1StaffResponse = _Q1StaffResponse;
            newTargetAndAppraisal.Quarter2StaffResponse = _Q2StaffResponse;
            newTargetAndAppraisal.Quarter3StaffResponse = _Q3StaffResponse;
            newTargetAndAppraisal.Quarter4StaffResponse = _Q4StaffResponse;
            //appraisal documents detials

            db.StaffAppraisals.InsertOnSubmit(newTargetAndAppraisal);
            db.SubmitChanges();
        }
        //update staff target/appraisal
        private void UpdateStaffTargetAndAppraisal(string _targetYear, int _departmentID, Guid _staffID, decimal _quarter1Target, decimal _quarter2Target, decimal _quarter3Target, decimal _quarter4Target, decimal _quarter1Actual, decimal _quarter2Actual, decimal _quarter3Actual, decimal _quarter4Actual, string _targetComments, string _actualComments, Guid _Q1AppraisedBy, Guid _Q2AppraisedBy, Guid _Q3AppraisedBy, Guid _Q4AppraisedBy, string _Q1StaffResponse, string _Q2StaffResponse, string _Q3StaffResponse, string _Q4StaffResponse)
        {
            StaffAppraisal updateTargetAndAppraisal = db.StaffAppraisals.Single(p => p.TargetYear == _targetYear && p.DepartmentID == _departmentID && p.StaffID == _staffID);
            //target details
            updateTargetAndAppraisal.Quarter1Target = _quarter1Target;
            updateTargetAndAppraisal.Quarter2Target = _quarter2Target;
            updateTargetAndAppraisal.Quarter3Target = _quarter3Target;
            updateTargetAndAppraisal.Quarter4Target = _quarter4Target;
            //actual details
            updateTargetAndAppraisal.Quarter1Actual = _quarter1Actual;
            updateTargetAndAppraisal.Quarter2Actual = _quarter2Actual;
            updateTargetAndAppraisal.Quarter3Actual = _quarter3Actual;
            updateTargetAndAppraisal.Quarter4Actual = _quarter4Actual;
            //coments
            updateTargetAndAppraisal.TargetComments = _targetComments;
            updateTargetAndAppraisal.ActualComments = _actualComments;
            //update appraised by details
            updateTargetAndAppraisal.Quarter1AppraisedBy = _Q1AppraisedBy;
            updateTargetAndAppraisal.Quarter2AppraisedBy = _Q2AppraisedBy;
            updateTargetAndAppraisal.Quarter3AppraisedBy = _Q3AppraisedBy;
            updateTargetAndAppraisal.Quarter4AppraisedBy = _Q4AppraisedBy;
            //update staff reponse details
            updateTargetAndAppraisal.Quarter1StaffResponse = _Q1StaffResponse;
            updateTargetAndAppraisal.Quarter2StaffResponse = _Q2StaffResponse;
            updateTargetAndAppraisal.Quarter3StaffResponse = _Q3StaffResponse;
            updateTargetAndAppraisal.Quarter4StaffResponse = _Q4StaffResponse;
            //
            db.SubmitChanges();
        }
        //save staff target/appraisal details
        protected void lnkBtnSave_Click(object sender, EventArgs e)
        {
            SaveStaffTargetAndAppraisalDetails(); return;
        }
        ////load staff target/appraisal details agaisnt a selected year
        //private void LoadSelectedStaffYearTargetAndAppraisal(string _targetYear, Guid _staffID)
        //{
        //    if (db.StaffAppraisals.Any(p => p.TargetYear == _targetYear && p.StaffID == _staffID))
        //    {
        //        //get details
        //        StaffAppraisal_View getTargetAndAppraisal = db.StaffAppraisal_Views.Single(p => p.TargetYear == _targetYear && p.StaffID == _staffID);
        //        HiddenFieldStaffTargetID.Value = getTargetAndAppraisal.StaffTargetID.ToString();
        //        //target details
        //        txtQuarter1Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter1Target);
        //        txtQuarter2Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter2Target);
        //        txtQuarter3Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter3Target);
        //        txtQuarter4Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter4Target);
        //        txtStaffOverallTarget.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.OverallTarget);
        //        //actual details
        //        txtQuarter1Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter1Actual);
        //        txtQuarter2Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter2Actual);
        //        txtQuarter3Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter3Actual);
        //        txtQuarter4Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.Quarter4Actual);
        //        txtStaffOverallActual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTargetAndAppraisal.OverallActual);
        //        //coments
        //        txtTargetComments.Text = getTargetAndAppraisal.TargetComments;
        //        txtActualComments.Text = getTargetAndAppraisal.ActualComments;
        //        //appraised by details
        //        if (getTargetAndAppraisal.Quarter1AppraisedBy != Guid.Empty)
        //            ddlQuarter1AppraisedBy.SelectedValue = getTargetAndAppraisal.Quarter1AppraisedBy.ToString();
        //        else ddlQuarter1AppraisedBy.SelectedIndex = 0;
        //        if (getTargetAndAppraisal.Quarter2AppraisedBy != Guid.Empty)
        //            ddlQuarter2AppraisedBy.SelectedValue = getTargetAndAppraisal.Quarter2AppraisedBy.ToString();
        //        else ddlQuarter2AppraisedBy.SelectedIndex = 0;
        //        if (getTargetAndAppraisal.Quarter3AppraisedBy != Guid.Empty)
        //            ddlQuarter3AppraisedBy.SelectedValue = getTargetAndAppraisal.Quarter3AppraisedBy.ToString();
        //        else ddlQuarter3AppraisedBy.SelectedIndex = 0;
        //        if (getTargetAndAppraisal.Quarter4AppraisedBy != Guid.Empty)
        //            ddlQuarter4AppraisedBy.SelectedValue = getTargetAndAppraisal.Quarter4AppraisedBy.ToString();
        //        else ddlQuarter4AppraisedBy.SelectedIndex = 0;
        //        //staff response details
        //        txtQuarter1StaffResponse.Text = getTargetAndAppraisal.Quarter1StaffResponse;
        //        txtQuarter2StaffResponse.Text = getTargetAndAppraisal.Quarter2StaffResponse;
        //        txtQuarter3StaffResponse.Text = getTargetAndAppraisal.Quarter3StaffResponse;
        //        txtQuarter4StaffResponse.Text = getTargetAndAppraisal.Quarter4StaffResponse;
        //        //appraisal documents
        //        return;
        //    }
        //    else
        //    {
        //        //clear controls
        //        _hrClass.ClearEntryControls(PanelAddStaffTargetAndAppraisalDetails);
        //        HiddenFieldStaffTargetID.Value = string.Empty;
        //        return;
        //    }

        //}
        //display staff names per the selected department
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string _targetYear = ddlYear.SelectedValue;
                int _departmentID = 0;
                if (ddlDepartment.SelectedIndex != 0)
                    _departmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                _hrClass.GetStaffNames(ddlStaffName, "Staff Name", _departmentID);//populate  staff name dropdown that are under the selected department
                //clear controls
                //_hrClass.ClearEntryControls(PanelAddStaffTargetAndAppraisalDetails);
                //HiddenFieldStaffTargetID.Value = string.Empty;
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelStaffTargetAndAppraisal);
                return;
            }
        }
        //display staff targets and appraisal details for the selected staff during for the year selected 
        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string _targetYear = ddlYear.SelectedValue;
                //int _departmentID = 0;
                //if (ddlDepartment.SelectedIndex != 0)
                //    _departmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                Guid _staffID = Guid.Empty;
                if (ddlStaffName.SelectedIndex != 0)
                    _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                //loda target/appraisal details
                //LoadSelectedStaffYearTargetAndAppraisal(_targetYear, _staffID);
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelStaffTargetAndAppraisal);
                return;
            }
        }
        //display staff targets and appraisal details for the selected staff during for the year selected,when a year id selectd
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    string _targetYear = "";
            //    if (ddlYear.SelectedIndex != 0)
            //        _targetYear = ddlYear.SelectedValue;
            //    if (_targetYear != "" && ddlDepartment.SelectedIndex != 0 & ddlStaffName.SelectedIndex != 0)
            //    {
            //        Guid _staffID = Guid.Empty;
            //        if (ddlStaffName.SelectedIndex != 0)
            //            _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
            //        //load target/appraisal details for the staff for the selected year
            //        LoadSelectedStaffYearTargetAndAppraisal(_targetYear, _staffID);
            //        return;
            //    }
            //    else
            //    {
            //        //clear controls
            //        _hrClass.ClearEntryControls(PanelAddStaffTargetAndAppraisalDetails);
            //        return;
            //    }
            //}
            //catch (Exception)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelStaffTargetAndAppraisal);
            //    return;
            //}
        }
        //load form for addindg or viewing appraisal form
        private void LoadAppraisalForm(int _appraisalQuarter)
        {
            try
            {
                lbAppraisalFormHeader.Text = "Quarter " + _appraisalQuarter + " Appraisal Form";
                HiddenFieldAppraisalFormQuarter.Value = _appraisalQuarter.ToString();
                lbDocumentsError.Text = "";
                //get appraisal form already saved link
                DisplayUploadedAppraisalForm(_appraisalQuarter);
                ModalPopupExtenderAppraisalForm.Show();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelStaffTargetAndAppraisal);
                return;
            }
        }
        //load quarter one appraisal form
        protected void lnkBtnQuarter1FormLink_Click(object sender, EventArgs e)
        {
            LoadAppraisalForm(1);//quarter one
            return;
        }
        //load quarter two appraisal form
        protected void lnkBtnQuarter2FormLink_Click(object sender, EventArgs e)
        {
            LoadAppraisalForm(2);//quarter two
            return;
        }
        //load quarter three appraisal form
        protected void lnkBtnQuarter3FormLink_Click(object sender, EventArgs e)
        {
            LoadAppraisalForm(3);//quarter three
            return;
        }
        //load quarter four appraisal form
        protected void lnkBtnQuarter4FormLink_Click(object sender, EventArgs e)
        {
            LoadAppraisalForm(4);//quarter four
            return;
        }
        //save staff appraisal form 
        private void UploadStaffAppraisalForm()
        {
            //ModalPopupExtenderAppraisalForm.Show();
            //try
            //{
            //    //check if a record traget/appraisal record is saved before uploading the appraisal document
            //    if (HiddenFieldStaffTargetID.Value == string.Empty)
            //    {
            //        lbDocumentsError.Text = "There are no target/appraisal details saved to attached the appraisal form against. Save target/appraisal details before you can upload an appraisal form!";
            //        return;
            //    }
            //    else if (FileUploadDocument.HasFile)
            //    {
            //        int _fileLength = FileUploadDocument.PostedFile.ContentLength;
            //        //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
            //        if (_hrClass.IsUploadedFileBig(_fileLength) == true)
            //        {
            //            lbDocumentsError.Text = "File uploaded exceeds the allowed limit of 3mb.";
            //            return;
            //        }
            //        else
            //        {
            //            string _uploadedDocFileName = FileUploadDocument.FileName;//get name of the uploaded file name
            //            string _fileExtension = Path.GetExtension(_uploadedDocFileName);

            //            _fileExtension = _fileExtension.ToLower();
            //            string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".rtf" };

            //            bool _isFileAccepted = false;
            //            foreach (string _acceptedFileExtension in _acceptedFileTypes)
            //            {
            //                if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
            //                    _isFileAccepted = true;
            //            }
            //            if (_isFileAccepted == false)
            //            {
            //                lbDocumentsError.Visible = true;
            //                lbDocumentsError.Text = "The file you are trying to upload is not a permitted file type!";
            //                return;
            //            }
            //            else
            //            {
            //                lbDocumentsError.Text = "";
            //                //save uploaded staff appraisal form
            //                SaveStaffAppraisalForm(FileUploadDocument, _uploadedDocFileName, _fileExtension);
            //                return;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        lbDocumentsError.Text = "Browse for the staff appraisal form to upload!";
            //        return;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelStaffTargetAndAppraisal); return;
            //}
        }

        //save staff appraisal form
        private void SaveStaffAppraisalForm(FileUpload _fileUpload, string _uploadedDocFileName, string _fileExtension)
        {
            ////get the quarter for which the form is being saved against
            //int _appraisalQuarter = Convert.ToInt32(HiddenFieldAppraisalFormQuarter.Value);

            ////save the document in the temp document
            //_fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
            //string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
            //lbDocumentsError.Text = "";
            ////save staff appraisal document into the database
            //FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
            ////get document info
            //FileInfo documentInfo = new FileInfo(_fileLocation);
            //int fileSize = (int)documentInfo.Length;//get file size
            //byte[] fileDocument = new byte[fileSize];
            //fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

            //int _staffTargetID = Convert.ToInt32(HiddenFieldStaffTargetID.Value);
            //StaffAppraisal updateTargetAndAppraisal = db.StaffAppraisals.Single(p => p.StaffTargetID == _staffTargetID);
            ////check the uploaded document is for which quarter
            //switch (_appraisalQuarter)
            //{
            //    case 1://quarter one 
            //        updateTargetAndAppraisal.Quarter1DocName = _uploadedDocFileName;
            //        updateTargetAndAppraisal.Quarter1AppraisalForm = fileDocument;
            //        break;
            //    case 2://quarter two
            //        updateTargetAndAppraisal.Quarter2DocName = _uploadedDocFileName;
            //        updateTargetAndAppraisal.Quarter2AppraisalForm = fileDocument;
            //        break;
            //    case 3://quarter three
            //        updateTargetAndAppraisal.Quarter3DocName = _uploadedDocFileName;
            //        updateTargetAndAppraisal.Quarter3AppraisalForm = fileDocument;
            //        break;
            //    case 4://quarter four
            //        updateTargetAndAppraisal.Quarter4DocName = _uploadedDocFileName;
            //        updateTargetAndAppraisal.Quarter4AppraisalForm = fileDocument;
            //        break;
            //}
            //db.SubmitChanges();
            //////dispaly uploaded staff apppraisal form
            //DisplayUploadedAppraisalForm(_appraisalQuarter);
            //_hrClass.LoadHRManagerMessageBox(2, "Staff appraisal form has been successfully uploaded!", this, PanelStaffTargetAndAppraisal);
            //return;

        }
        //upload the form
        protected void lnkBtnUploadAppraisalForm_OnClick(object sender, EventArgs e)
        {
            UploadStaffAppraisalForm(); return;
        }
        //display uploaded appraisal form 
        private void DisplayUploadedAppraisalForm(int _appraisalQuarter)
        {
            //if (HiddenFieldStaffTargetID.Value != string.Empty)
            //{
            //    int _staffTargetID = Convert.ToInt32(HiddenFieldStaffTargetID.Value);
            //    StaffAppraisal getTargetAndAppraisal = db.StaffAppraisals.Single(p => p.StaffTargetID == _staffTargetID);
            //    //check the uploaded document is for which quarter
            //    switch (_appraisalQuarter)
            //    {
            //        case 1://quarter one 
            //            if (getTargetAndAppraisal.Quarter1DocName != null)
            //                lnkBtnUploadedDocumentLink.Text = getTargetAndAppraisal.Quarter1DocName;
            //            else
            //                lnkBtnUploadedDocumentLink.Text = "No uploaded document";
            //            break;
            //        case 2://quarter two
            //            if (getTargetAndAppraisal.Quarter2DocName != null)
            //                lnkBtnUploadedDocumentLink.Text = getTargetAndAppraisal.Quarter2DocName;
            //            else
            //                lnkBtnUploadedDocumentLink.Text = "No uploaded document";
            //            break;
            //        case 3://quarter three
            //            if (getTargetAndAppraisal.Quarter3DocName != null)
            //                lnkBtnUploadedDocumentLink.Text = getTargetAndAppraisal.Quarter3DocName;
            //            else
            //                lnkBtnUploadedDocumentLink.Text = "No uploaded document";
            //            break;
            //        case 4://quarter four
            //            if (getTargetAndAppraisal.Quarter4DocName != null)
            //                lnkBtnUploadedDocumentLink.Text = getTargetAndAppraisal.Quarter4DocName;
            //            else
            //                lnkBtnUploadedDocumentLink.Text = "No uploaded document";
            //            break;
            //    }
            //}
        }
        //view appraisal form that has been uploaded
        private void ViewStaffAppraisalForm()
        {
            //try
            //{
            //    ModalPopupExtenderAppraisalForm.Show();
            //    int _appraisalQuarter = Convert.ToInt32(HiddenFieldAppraisalFormQuarter.Value);
            //    if (lnkBtnUploadedDocumentLink.Text == "No uploaded document")
            //    {
            //        //do nothing
            //    }
            //    else
            //    {
            //        //load the document that has been uploaded
            //        string strPageUrl = "Performance_Document_Viewer.aspx?ref1=" + HiddenFieldStaffTargetID.Value + "&ref2=" + HiddenFieldAppraisalFormQuarter.Value;
            //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Document load failed . " + ex.Message.ToString() + ". Please try again!", this, PanelStaffTargetAndAppraisal); return;
            //}
        }
        protected void lnkBtnUploadedDocumentLink_OnClick(object sender, EventArgs e)
        {
            ViewStaffAppraisalForm(); return;
        }
    }
}