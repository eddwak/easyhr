﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.Performance_Review
{
    public partial class Appraisal_Template : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //LoadTemplateDocument();
                DisplayAppraisalTemplateDocuments();//load appraisal template documents
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //save appraisal template document
        private void UploadAppraisalTemplateDocument()
        {
            try
            {
                if (txtAppraisalTemplateName.Text.Trim() == "")
                {
                    lbDocumentsError.Text = "Enter the appraisal template document!";
                    txtAppraisalTemplateName.Focus(); return;
                }
                else if (FileUploadAppraisalTemplate.HasFile)
                {
                    int _fileLength = FileUploadAppraisalTemplate.PostedFile.ContentLength;
                    //check if the file is bigger than 3mb i.e 3 *1024 *1024 (3145728) bytes
                    if (_hrClass.IsUploadedFileBig(_fileLength) == true)
                    {
                        lbDocumentsError.Text = "File uploaded exceeds the allowed limit of 3mb.";
                        return;
                    }
                    else
                    {
                        string _uploadedDocFileName = FileUploadAppraisalTemplate.FileName;//get name of the uploaded file name
                        string _fileExtension = Path.GetExtension(_uploadedDocFileName);

                        _fileExtension = _fileExtension.ToLower();
                        string[] _acceptedFileTypes = new string[] { ".pdf", ".docx", ".doc", ".rtf" };

                        bool _isFileAccepted = false;
                        foreach (string _acceptedFileExtension in _acceptedFileTypes)
                        {
                            if (_fileExtension == _acceptedFileExtension)///check if the file extension is expected
                                _isFileAccepted = true;
                        }
                        if (_isFileAccepted == false)
                        {
                            lbDocumentsError.Visible = true;
                            lbDocumentsError.Text = "The file you are trying to upload is not a permitted file type!";
                            return;
                        }
                        else
                        {
                            lbDocumentsError.Text = "";
                            string _documentTitle = txtAppraisalTemplateName.Text.Trim();
                            //save uploaded template document
                            SaveAppraisalTemplateDocument(FileUploadAppraisalTemplate, _documentTitle, _uploadedDocFileName, _fileExtension);
                            return;
                        }
                    }
                }
                else
                {
                    lbDocumentsError.Text = "Browse for the appraisal template document to upload!";
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Document upload failed . " + ex.Message.ToString() + ". Please try again!", this, PanelAppraisalTemplate); return;
            }
        }

        //save appraisal template document
        private void SaveAppraisalTemplateDocument(FileUpload _fileUpload, string _documentTitle, string _uploadedDocFileName, string _fileExtension)
        {

            if (db.AppraisalTemplates.Any(p => p.appraisaltemplate_vTemplateTitle.ToLower() == _documentTitle.ToLower()))
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Another document with the same title already exists!", this, PanelAppraisalTemplate);
                return;
            }
            else
            {
                //save the document in the temp document
                _fileUpload.SaveAs(Server.MapPath("~/TempDocs/" + _uploadedDocFileName));
                string _fileLocation = Server.MapPath("~/TempDocs/" + _uploadedDocFileName);
                lbDocumentsError.Text = "";
                Guid _loggedStaffMasterID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                //save employee document into the database

                FileStream fs = new FileStream(_fileLocation, FileMode.Open);//
                //get document info
                FileInfo documentInfo = new FileInfo(_fileLocation);
                int fileSize = (int)documentInfo.Length;//get file size
                byte[] fileDocument = new byte[fileSize];
                fs.Read(fileDocument, 0, fileSize);//read the document from the file stream

                //add the new document
                AppraisalTemplate newAppraisalTemplate = new AppraisalTemplate();
                newAppraisalTemplate.appraisaltemplate_vTemplateTitle = _documentTitle;
                newAppraisalTemplate.appraisaltemplate_vDocName = _uploadedDocFileName;
                newAppraisalTemplate.appraisaltemplate_vDocExtension = _fileExtension;
                newAppraisalTemplate.appraisaltemplate_vbDocument = fileDocument;
                newAppraisalTemplate.appraisaltemplate_dtDateStored = DateTime.Now;
                newAppraisalTemplate.appraisaltemplate_uiStoredByID = _loggedStaffMasterID;
                db.AppraisalTemplates.InsertOnSubmit(newAppraisalTemplate);
                db.SubmitChanges();
                //dispaly appraisal template documents
                DisplayAppraisalTemplateDocuments();
                _hrClass.LoadHRManagerMessageBox(2, "Appraisal template document has been successfully uploaded!", this, PanelAppraisalTemplate);
                return;
            }
        }
        protected void lnkBtntnUploadDocument_OnClick(object sender, EventArgs e)
        {
            UploadAppraisalTemplateDocument(); return;
        }
        //display appraisal template document details
        private void DisplayAppraisalTemplateDocuments()
        {
            try
            {
                object _display;
                _display = db.AppraisalTemplate_Views.Select(P => P).OrderBy(p => p.appraisaltemplate_vTemplateTitle);
                gvAppraisalTemplates.DataSourceID = null;
                gvAppraisalTemplates.DataSource = _display;
                gvAppraisalTemplates.DataBind();
                return;
            }
            catch { }
        }
        //load appraisal template document details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvAppraisalTemplates_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditDocument") == 0 || e.CommandName.CompareTo("DeleteDocument") == 0 || e.CommandName.CompareTo("ViewDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFAppraisalTemplateID = (HiddenField)gvAppraisalTemplates.Rows[ID].FindControl("HiddenField1");
                    int _appraisalTemplateID = Convert.ToInt32(_HFAppraisalTemplateID.Value);
                    if (e.CommandName.CompareTo("EditDocument") == 0)//check if we are editing the document
                    {
                        //load edit appraisal template document details
                        //LoadEditAppraisalTemplate(_appraisalTemplateID);
                        return;
                    }
                    else if (e.CommandName.CompareTo("DeleteDocument") == 0)//check if are deleting the document
                    {
                        HiddenFieldAppraisalTemplateID.Value = _appraisalTemplateID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the appraisal template?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    }
                    else if (e.CommandName.CompareTo("ViewDocument") == 0)//check if we are viewing the document
                    {
                        string strPageUrl = "Performance_Document_Viewer.aspx?ref=" + _HFAppraisalTemplateID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window

                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        //method for deleting an appraisal template document
        private void DeleteAppraisalTemplateDocument()
        {
            try
            {
                int _appraisalTemplateID = Convert.ToInt32(HiddenFieldAppraisalTemplateID.Value);
                AppraisalTemplate deleteAppraisalTemplate = db.AppraisalTemplates.Single(p => p.appraisaltemplate_iAppraisalTemplateID == _appraisalTemplateID);
                db.AppraisalTemplates.DeleteOnSubmit(deleteAppraisalTemplate);
                db.SubmitChanges();
                DisplayAppraisalTemplateDocuments();//reload appraisal template documents after deletion
                _hrClass.LoadHRManagerMessageBox(2, "Appraisal template document selected has been successfully deleted.", this, PanelAppraisalTemplate);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelAppraisalTemplate);
                return;
            }
        }
          //delete records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteAppraisalTemplateDocument();
        }

        //load the template document
        private void LoadTemplateDocument()
        {
            Response.Redirect("~/Template_Documents/appraisal template.doc");
            return;
        }
        ////load template documents
        //private void DisplayTemplateDocuments()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("DocumentID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("DocumentName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("DocumentLink", typeof(string)));
        //    DataRow dr = dt.NewRow();
        //    dr[0] = 1;
        //    dr[1] = "Default Appraisal Template";
        //    dr[2] = "Appraisal Template";
        //    dt.Rows.Add(dr);

        //    DataRow dr2 = dt.NewRow();
        //    dr2[0] = 2;
        //    dr2[1] = "Appraisal Document 2";
        //    dr2[2] = "Link 2";
        //    dt.Rows.Add(dr2);

        //    gvAppraisalTemplates.DataSource = dt;
        //    gvAppraisalTemplates.DataBind();
        //}
    }
}