﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Performance_Review/PerformanceReview.master"
    AutoEventWireup="true" CodeBehind="Appraisal_Template.aspx.cs" Inherits="AdeptHRManager.Performance_Review.Appraisal_Template" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PerformanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelAppraisalTemplate" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelAppraisalTemplate" runat="server">
                <%--    <div class="panel-details-header">
                    Appraisal Templates
                </div>--%>
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                Appraisal Template Name:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAppraisalTemplateName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Select Document:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:FileUpload ID="FileUploadAppraisalTemplate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbDocumentsError" runat="server" CssClass="errorMessage" Text=""></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="HiddenFieldAppraisalTemplateID" runat="server" />
                            </td>
                            <td>
                                <div class="linkBtn">
                                    <asp:LinkButton ID="lnkBtnUploadDocument" OnClick="lnkBtntnUploadDocument_OnClick"
                                        ToolTip="Save Appraisal Template" runat="server">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Upload.png"
                                            ImageAlign="AbsMiddle" />
                                        Upload Appraisal Template</asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <asp:GridView ID="gvAppraisalTemplates" runat="server" OnRowCommand="gvAppraisalTemplates_RowCommand"
                        AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                        AllowSorting="True" PageSize="10" EmptyDataText="No appraisal templates available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("appraisaltemplate_iAppraisalTemplateID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="appraisaltemplate_vTemplateTitle" HeaderText="Appraisal Template Name" />
                            <asp:ButtonField DataTextField="appraisaltemplate_vDocName" HeaderText="Document Name"
                                CommandName="ViewDocument" />
                            <asp:ButtonField HeaderText="Delete" CommandName="DeleteDocument" ItemStyle-Width="4px"
                                Text="Delete" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkBtnUploadDocument" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
