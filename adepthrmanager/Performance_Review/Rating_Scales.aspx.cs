﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Performance_Review
{

    public partial class Rating_Scales : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayRatingScales();//display rating scales
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddRatingScaleControls();
            ModalPopupExtenderAddRatingScale.Show();
            return;
        }
        //clear add rating scale controls
        private void ClearAddRatingScaleControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddRatingScale);
            HiddenFieldRatingScaleID.Value = "";
            lnkBtnSaveRatingScale.Visible = true;
            lnkBtnUpdateRatingScale.Enabled = false;
            lnkBtnUpdateRatingScale.Visible = true;
            lnkBtnClearRatingScale.Visible = true;
            lbAddRatingScaleHeader.Text = "Add New Rating Scale";
            ImageAddRatingScaleHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add rating scale controls
        private string ValidateAddRatingScaleControls()
        {
            if (txtRate.Text.Trim() == "")
            {
                txtRate.Focus();
                return "Enter rating scale!";
            }
            else if (txtDescription.Text.Trim() == "")
            {
                txtDescription.Focus();
                return "Enter rating scale description!";
            }
            else if (txtPercentageOfAchievement.Text.Trim() == "")
            {
                txtPercentageOfAchievement.Focus();
                return "Enter percentage range!";
            }
            else return "";
        }
        //save new rating scale
        private void SaveNewRatingScale()
        {
            ModalPopupExtenderAddRatingScale.Show();
            try
            {
                string _error = ValidateAddRatingScaleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new rating scale details
                    string _rateName = "", _rateDescription = "", _ratePercentage = "";
                    _rateName = txtRate.Text.Trim();
                    _rateDescription = txtDescription.Text.Trim();
                    _ratePercentage = txtPercentageOfAchievement.Text.Trim();
                    if (db.RatingScales.Any(p => p.Rate.ToLower() == _rateName.ToLower()))//check if the rate exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the rate you are trying to add already exists!", this, PanelRatingScale);
                        return;
                    }
                    else
                    {
                        //save the new rating scale
                        RatingScale newRatingScale = new RatingScale();
                        newRatingScale.Rate = _rateName;
                        newRatingScale.Description = _rateDescription;
                        newRatingScale.Percentage = _ratePercentage;
                        db.RatingScales.InsertOnSubmit(newRatingScale);
                        db.SubmitChanges();
                        HiddenFieldRatingScaleID.Value = newRatingScale.RatingScaleID.ToString();
                        DisplayRatingScales();
                        _hrClass.LoadHRManagerMessageBox(2, "Rating scale has been successfully saved.", this, PanelRatingScale);
                        lnkBtnUpdateRatingScale.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again !", this, PanelRatingScale);

                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateRatingScale()
        {
            ModalPopupExtenderAddRatingScale.Show();
            try
            {
                string _error = ValidateAddRatingScaleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _ratingScaleID = Convert.ToInt32(HiddenFieldRatingScaleID.Value);
                    //save the new rating scale details
                    string _rateName = "", _rateDescription = "", _ratePercentage = "";
                    _rateName = txtRate.Text.Trim();
                    _rateDescription = txtDescription.Text.Trim();
                    _ratePercentage = txtPercentageOfAchievement.Text.Trim();
                    if (db.RatingScales.Any(p => p.Rate.ToLower() == _rateName.ToLower() && p.RatingScaleID != _ratingScaleID))//check if the rate exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the rate you are trying to add already exists!", this, PanelRatingScale);
                        return;
                    }
                    else
                    {
                        //save the update rating scale
                        RatingScale updateRatingScale = db.RatingScales.Single(p => p.RatingScaleID == _ratingScaleID);
                        updateRatingScale.Rate = _rateName;
                        updateRatingScale.Description = _rateDescription;
                        updateRatingScale.Percentage = _ratePercentage;
                        db.SubmitChanges();
                        DisplayRatingScales();
                        _hrClass.LoadHRManagerMessageBox(2, "Rating scale has been successfully updated.", this, PanelRatingScale);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString() + ". Please try again!", this, PanelRatingScale);
                return;
            }
        }
        protected void lnkBtnSaveRatingScale_Click(object sender, EventArgs e)
        {
            SaveNewRatingScale();
            return;
        }

        protected void lnkBtnUpdateRatingScale_Click(object sender, EventArgs e)
        {
            UpdateRatingScale();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearRatingScale_Click(object sender, EventArgs e)
        {
            ClearAddRatingScaleControls();
            ModalPopupExtenderAddRatingScale.Show();
            return;
        }
        //load rating scale
        private void DisplayRatingScales()
        {
            object _display;
            _display = db.RatingScales.Select(p => p).OrderBy(p => p.Rate);
            gvRatingScales.DataSourceID = null;
            gvRatingScales.DataSource = _display;
            gvRatingScales.DataBind();
            return;
        }
        // populate the edit rating scale controls
        private void LoadEditRatingScaleControls(int _ratingScaleID)
        {
            ClearAddRatingScaleControls();
            RatingScale getRatingScale = db.RatingScales.Single(p => p.RatingScaleID == _ratingScaleID);
            HiddenFieldRatingScaleID.Value = _ratingScaleID.ToString();
            txtRate.Text = getRatingScale.Rate;
            txtDescription.Text = getRatingScale.Description;
            txtPercentageOfAchievement.Text = getRatingScale.Percentage;
            lnkBtnSaveRatingScale.Visible = false;
            lnkBtnClearRatingScale.Visible = true;
            lnkBtnUpdateRatingScale.Enabled = true;
            lbAddRatingScaleHeader.Text = "Edit Rating Scale";
            ImageAddRatingScaleHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ModalPopupExtenderAddRatingScale.Show();
            return;
        }
        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvRatingScales))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelRatingScale);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvRatingScales.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelRatingScale);
                        return;
                    }
                    foreach (GridViewRow _grv in gvRatingScales.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFRatingScaleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit rating scale
                            LoadEditRatingScaleControls(Convert.ToInt32(_HFRatingScaleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelRatingScale);
            }
        }


        //load delete rating scale
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvRatingScales))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelRatingScale);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvRatingScales.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelRatingScale);
                return;
            }
        }
        //delete rating scale record
        private void DeleteRatingScales()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvRatingScales.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFRatingScaleID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _RatingScaleID = Convert.ToInt32(_HFRatingScaleID.Value);

                        RatingScale deleteRatingScale = db.RatingScales.Single(p => p.RatingScaleID == _RatingScaleID);
                        //check if there is any RatingScale used

                        //do not delete --- to be done

                        db.RatingScales.DeleteOnSubmit(deleteRatingScale);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayRatingScales();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected record has been deleted.", this, PanelRatingScale);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected records have been deleted.", this, PanelRatingScale);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelRatingScale);
                return;
            }
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteRatingScales();//delete selected records(s)
            return;
        }
    }
}