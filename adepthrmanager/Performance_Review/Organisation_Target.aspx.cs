﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Performance_Review
{
    public partial class Organisation_Target : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GenerateYears(ddlYear);//display years
            }
        }
        //validate organization target entry controls
        private string ValidateOrganizationTargetControls()
        {
            if (ddlYear.SelectedIndex == 0)
                return "Select target year!";
            else if (txtQuarter1Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter1Target.Text.Trim()) == false)
                return "Invalid quarter one target value entered!";
            else if (txtQuarter2Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter2Target.Text.Trim()) == false)
                return "Invalid quarter two target value entered!";
            else if (txtQuarter3Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter3Target.Text.Trim()) == false)
                return "Invalid quarter three target value entered!";
            else if (txtQuarter4Target.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter4Target.Text.Trim()) == false)
                return "Invalid quarter four target value entered!";
            else if (txtQuarter1Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter1Actual.Text.Trim()) == false)
                return "Invalid quarter one actual value entered!";
            else if (txtQuarter2Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter2Actual.Text.Trim()) == false)
                return "Invalid quarter two actual value entered!";
            else if (txtQuarter3Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter3Actual.Text.Trim()) == false)
                return "Invalid quarter three actual value entered!";
            else if (txtQuarter4Actual.Text.Trim() != "" && _hrClass.isNumberValid(txtQuarter4Actual.Text.Trim()) == false)
                return "Invalid quarter four actual value entered!";
            else if (txtQuarter1Target.Text.Trim() == "" && txtQuarter2Target.Text.Trim() == "" && txtQuarter3Target.Text.Trim() == "" && txtQuarter4Target.Text.Trim() == "" && txtQuarter1Actual.Text.Trim() == "" && txtQuarter2Actual.Text.Trim() == "" && txtQuarter3Actual.Text.Trim() == "" && txtQuarter4Actual.Text.Trim() == "")
            {
                return "Save failed. There are no details entered to save against year selected!";
            }
            else return "";
        }

        //save organization target details
        private void SaveOrganizationTargetDetails()
        {
            try
            {
                string _error = ValidateOrganizationTargetControls();//check error
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelOrganizationTarget);
                    return;
                }
                else
                {
                    string _targetYear = ddlYear.SelectedValue.Trim();
                    decimal _quarter1Target = 0, _quarter2Target = 0, _quarter3Target = 0, _quarter4Target = 0, _quarter1Actual = 0, _quarter2Actual = 0, _quarter3Actual = 0, _quarter4Actual = 0;
                    if (txtQuarter1Target.Text.Trim() != "")
                        _quarter1Target = Convert.ToDecimal(txtQuarter1Target.Text.Trim());
                    if (txtQuarter2Target.Text.Trim() != "")
                        _quarter2Target = Convert.ToDecimal(txtQuarter2Target.Text.Trim());
                    if (txtQuarter3Target.Text.Trim() != "")
                        _quarter3Target = Convert.ToDecimal(txtQuarter3Target.Text.Trim());
                    if (txtQuarter4Target.Text.Trim() != "")
                        _quarter4Target = Convert.ToDecimal(txtQuarter4Target.Text.Trim());
                    if (txtQuarter1Actual.Text.Trim() != "")
                        _quarter1Actual = Convert.ToDecimal(txtQuarter1Actual.Text.Trim());
                    if (txtQuarter2Actual.Text.Trim() != "")
                        _quarter2Actual = Convert.ToDecimal(txtQuarter2Actual.Text.Trim());
                    if (txtQuarter3Actual.Text.Trim() != "")
                        _quarter3Actual = Convert.ToDecimal(txtQuarter3Actual.Text.Trim());
                    if (txtQuarter4Actual.Text.Trim() != "")
                        _quarter4Actual = Convert.ToDecimal(txtQuarter4Actual.Text.Trim());
                    string _targetComments = "", _actualComments = "";
                    _targetComments = txtTargetComments.Text.Trim();
                    _actualComments = txtActualComments.Text.Trim();
                    //check if there is a record with the selected year
                    if (db.OrganizationTargets.Any(p => p.TargetYear == _targetYear))
                    {
                        //update the existing year's record
                        UpdateTarget(_targetYear, _quarter1Target, _quarter2Target, _quarter3Target, _quarter4Target, _quarter1Actual, _quarter2Actual, _quarter3Actual, _quarter4Actual, _targetComments, _actualComments);
                    }
                    else
                    {
                        //save a new record for the year
                        SaveNewTarget(_targetYear, _quarter1Target, _quarter2Target, _quarter3Target, _quarter4Target, _quarter1Actual, _quarter2Actual, _quarter3Actual, _quarter4Actual, _targetComments, _actualComments);
                    }
                    LoadSelectedYearTarget(_targetYear);//load target details entered and calculate totals
                    _hrClass.LoadHRManagerMessageBox(2, "Organization target details have been successfully saved!", this, PanelOrganizationTarget);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelOrganizationTarget);
                return;
            }
        }
        //save new target
        private void SaveNewTarget(string _targetYear, decimal _quarter1Target, decimal _quarter2Target, decimal _quarter3Target, decimal _quarter4Target, decimal _quarter1Actual, decimal _quarter2Actual, decimal _quarter3Actual, decimal _quarter4Actual, string _targetComments, string _actualComments)
        {
            OrganizationTarget newTarget = new OrganizationTarget();
            newTarget.TargetYear = _targetYear;
            newTarget.Quarter1Target = _quarter1Target;
            newTarget.Quarter2Target = _quarter2Target;
            newTarget.Quarter3Target = _quarter3Target;
            newTarget.Quarter4Target = _quarter4Target;
            newTarget.Quarter1Actual = _quarter1Actual;
            newTarget.Quarter2Actual = _quarter2Actual;
            newTarget.Quarter3Actual = _quarter3Actual;
            newTarget.Quarter4Actual = _quarter4Actual;
            newTarget.TargetComments = _targetComments;
            newTarget.ActualComments = _actualComments;
            db.OrganizationTargets.InsertOnSubmit(newTarget);
            db.SubmitChanges();
        }
        //update target
        private void UpdateTarget(string _targetYear, decimal _quarter1Target, decimal _quarter2Target, decimal _quarter3Target, decimal _quarter4Target, decimal _quarter1Actual, decimal _quarter2Actual, decimal _quarter3Actual, decimal _quarter4Actual, string _targetComments, string _actualComments)
        {
            OrganizationTarget updateTarget = db.OrganizationTargets.Single(p => p.TargetYear == _targetYear);
            updateTarget.Quarter1Target = _quarter1Target;
            updateTarget.Quarter2Target = _quarter2Target;
            updateTarget.Quarter3Target = _quarter3Target;
            updateTarget.Quarter4Target = _quarter4Target;
            updateTarget.Quarter1Actual = _quarter1Actual;
            updateTarget.Quarter2Actual = _quarter2Actual;
            updateTarget.Quarter3Actual = _quarter3Actual;
            updateTarget.Quarter4Actual = _quarter4Actual;
            updateTarget.TargetComments = _targetComments;
            updateTarget.ActualComments = _actualComments;
            db.SubmitChanges();
        }
        //save details
        protected void lnkBtnSave_Click(object sender, EventArgs e)
        {
            SaveOrganizationTargetDetails(); return;
        }
        //load target details agaisnt a selected year
        private void LoadSelectedYearTarget(string _targetYear)
        {
            if (db.OrganizationTargets.Any(p => p.TargetYear == _targetYear))
            {
                //get details
                OrganizationTarget_View getTarget = db.OrganizationTarget_Views.Single(p => p.TargetYear == _targetYear);
                txtQuarter1Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter1Target);
                txtQuarter2Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter2Target);
                txtQuarter3Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter3Target);
                txtQuarter4Target.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter4Target);
                txtCompanyTarget.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.OverallTarget);
                txtQuarter1Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter1Actual);
                txtQuarter2Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter2Actual);
                txtQuarter3Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter3Actual);
                txtQuarter4Actual.Text = _hrClass.ConvertToCurrencyValue((decimal)getTarget.Quarter4Actual);
                txtCompanyActual.Text=_hrClass.ConvertToCurrencyValue((decimal)getTarget.OverallActual);
                txtTargetComments.Text = getTarget.TargetComments;
                txtActualComments.Text = getTarget.ActualComments;
                return;
            }
            else
            {
                //clear controls
                _hrClass.ClearEntryControls(PanelAddTargetDetails);
                return;
            }

        }
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string _targetYear = ddlYear.SelectedValue;
                LoadSelectedYearTarget(_targetYear);
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelOrganizationTarget);
                return;
            }
        }
    }
}