﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Performance_Review/PerformanceReview.master"
    AutoEventWireup="true" CodeBehind="Perfomance_Targets.aspx.cs" Inherits="AdeptHRManager.Performance_Review.Perfomance_Targets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PerformanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelPerformanceTargets" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelPerformanceTargets" runat="server">
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                <b>Department:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                    AutoPostBack="true" Width="200px" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>Staff Name:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStaffName" OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>Year:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtYear" Width="70px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table width="100%">
                        <tr>
                            <td>
                                Performance Objective<span class="errorMessage">*</span>
                            </td>
                            <td>
                                Annual Target<span class="errorMessage">*</span>
                            </td>
                            <td>
                                Actual Achieved at Year End<span class="errorMessage">*</span>
                            </td>
                            <td>
                                Rating<span class="errorMessage">*</span>
                            </td>
                            <td>
                                % Weight<span class="errorMessage">*</span>
                            </td>
                            <td>
                                % Score<span class="errorMessage">*</span>
                            </td>
                            <td>
                                Sign Off
                            </td>
                            <td>
                                Comment
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPerformanceObjective" Width="200px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAnnualTarget" Width="200px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtActialAchievedAtYearEnd" Width="150px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRating" Width="70px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPercentageWeight" Width="70px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPercentageScore" Width="70px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="CBSignOff" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtComments" Width="200px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="btnSaveAnnualTargets" runat="server" Text="Save Annual Targets" />
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table>
                        <tr>
                            <td>
                                <b>Key Perfomance Indicator</b>
                            </td>
                            <td style="text-align: center;">
                                <b>Actual Achieved</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <asp:TextBox ID="txtKeyPerfomanceIndicator" Height="140px" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            Quarter 1
                                        </td>
                                        <td>
                                            Quarter 2
                                        </td>
                                        <td>
                                            Quarter 3
                                        </td>
                                        <td>
                                            Quarter 4
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter1" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter2" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter3" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter4" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appraisal Date:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter1AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter2AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter3AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter4AppraisalDate" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appraisal By:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter1AppraisedBy" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter2AppraisedBy" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter3AppraisedBy" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter4AppraisedBy" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comments:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter1Comments" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter2Comments" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter3Comments" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuarter4Comments" Width="150px" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="txtSaveQuarterlyDetails" runat="server" Text="Save Quarterly Details" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
