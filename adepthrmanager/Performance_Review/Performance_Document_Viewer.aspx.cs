﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.IO;

namespace AdeptHRManager.Performance_Review
{
    public partial class Performance_Document_Viewer : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                DownloadDocuments();//download documents
            }
        }

        private void DownloadDocuments()
        {
            try
            {
                string _staffTargetID = Request.QueryString["ref1"].ToString();//get staff target id passed
                string _appraisalQuarter = Request["ref2"].ToString();//get appraisal quarter id
                if (_staffTargetID != null && _appraisalQuarter != null)
                {
                    // view staff appraisal  form document
                    LoadStaffAppraisalDocument(Convert.ToInt32(_staffTargetID), Convert.ToInt32(_appraisalQuarter));
                }
            }
            catch { }
            try
            {

                string _appraisalTemplateID = Request["ref"].ToString();//
                if (_appraisalTemplateID != null)
                {
                    //view appraisal template documents
                    LoadAppraisalTemplateDocument(Convert.ToInt32(_appraisalTemplateID));
                }
            }
            catch { }
            try
            {
                string _quarterlyReviewID = Request["quarterlyreview"].ToString();//
                if (_quarterlyReviewID != null)
                {
                    //download quaterly review document
                    DownloadQuarteltyReviewDocument(Convert.ToInt32(_quarterlyReviewID));
                }
            }
            catch { }
        }
        private void LoadStaffAppraisalDocument(int _staffTargetID, int _appraisalQuarter)
        {
            StaffAppraisal getStaffAppraisalDocument = db.StaffAppraisals.Single(p => p.StaffTargetID == _staffTargetID);
            string _documentName = "", _docExtension = ".";
            byte[] _file = new byte[3145728];//3mb into bytes
            if (_appraisalQuarter == 1)//quarter one document
            {
                _documentName = getStaffAppraisalDocument.Quarter1DocName;
                _file = getStaffAppraisalDocument.Quarter1AppraisalForm.ToArray();

            }
            else if (_appraisalQuarter == 2)//quarter two document
            {
                _documentName = getStaffAppraisalDocument.Quarter2DocName;
                _file = getStaffAppraisalDocument.Quarter2AppraisalForm.ToArray();
            }
            else if (_appraisalQuarter == 3)//quarter three document
            {
                _documentName = getStaffAppraisalDocument.Quarter3DocName;
                _file = getStaffAppraisalDocument.Quarter3AppraisalForm.ToArray();
            }
            else if (_appraisalQuarter == 4)//quarter four document
            {
                _documentName = getStaffAppraisalDocument.Quarter4DocName;
                _file = getStaffAppraisalDocument.Quarter4AppraisalForm.ToArray();
            }
            _docExtension += _documentName.Split('.').LastOrDefault();
            //_hrClass.LoadHRManagerMessageBox(1, _docExtension, this, Panel1); return;
            if (_file != null)
            {
                _hrClass.GetDocumentContentType(_docExtension, _documentName);
                MemoryStream memoryStream = new MemoryStream();
                memoryStream.Write(_file, 0, _file.Length);
                HttpContext.Current.Response.BinaryWrite(_file);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Flush();
            }
            else { }
        }
        //load appraisal template document
        private void LoadAppraisalTemplateDocument(int _appraisalTemplateID)
        {
            if (db.AppraisalTemplates.Any(p => p.appraisaltemplate_iAppraisalTemplateID == _appraisalTemplateID))
            {
                AppraisalTemplate getAppraisalTemplate = db.AppraisalTemplates.Single(p => p.appraisaltemplate_iAppraisalTemplateID == _appraisalTemplateID);
                if (getAppraisalTemplate.appraisaltemplate_vbDocument != null)
                {
                    string _docExtension = getAppraisalTemplate.appraisaltemplate_vDocExtension;
                    string _documentName = getAppraisalTemplate.appraisaltemplate_vDocName;
                    //get document content type as per the document's extension
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getAppraisalTemplate.appraisaltemplate_vbDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
        }
        //download quaterly review document
        private void DownloadQuarteltyReviewDocument(int _quarterlyReviewID)
        {
            if (db.QuarterlyReviews.Any(p => p.QuarterlyReviewID == _quarterlyReviewID))
            {
                QuarterlyReview getQuarterlyReview = db.QuarterlyReviews.Single(p => p.QuarterlyReviewID == _quarterlyReviewID);
                if (getQuarterlyReview.StaffUploadedDocument != null)
                {
                    string _documentName = "", _docExtension = ".";
                    _documentName = getQuarterlyReview.StaffDocumentName;
                    _docExtension += _documentName.Split('.').LastOrDefault();
                    //get document content type as per the document's extension
                    _hrClass.GetDocumentContentType(_docExtension, _documentName);
                    byte[] file = getQuarterlyReview.StaffUploadedDocument.ToArray(); ;
                    MemoryStream memoryStream = new MemoryStream();
                    memoryStream.Write(file, 0, file.Length);
                    HttpContext.Current.Response.BinaryWrite(file);
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
                else { }
            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, "Download failed. There is no document available!", this, PanelPerformanceDocumentViewer);
                return;
            }
        }
    }
}