﻿<%@ Page Title="Leave Reports" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Leave_Reports.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Leave_Reports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaveReports" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeavesReports" runat="server">
                <asp:TabContainer ID="TabContainerLeaveReports" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelLeaveReports" runat="server" CssClass="ajax-tab" HeaderText="Leave Reports">
                        <HeaderTemplate>
                            Leave Reports
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table class="GridViewStyle">
                                    <tr class="AltRowStyle">
                                        <th>
                                        </th>
                                        <th>
                                            <b>Report Type</b>
                                        </th>
                                        <th>
                                            <b>Description</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="2%">
                                            1.
                                        </td>
                                        <td width="40%">
                                            <asp:LinkButton ID="lnkBtnStaffLeaveStatusReport" OnClick="lnkBtnStaffLeaveStatusReport_Click"
                                                runat="server">Staff Leave Status Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays staff leave status for all employees, or for employees within a particular
                                            branch or for employees from a certain department.
                                        </td>
                                    </tr>
                                    <tr class="AltRowStyle">
                                        <td>
                                            2.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnSingleStaffLeaveStatusReport" OnClick="lnkBtnSingleStaffLeaveStatusReport_Click"
                                                runat="server"> Single Staff Leave Status Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays leave status for a selected employee
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnLeaveApplicationsApprovedPerDurationReport" runat="server">Leave Applications Approved Per Duration Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays leave applications approved during a specified duration
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelReportViewer" runat="server" CssClass="ajax-tab" HeaderText="Report Viewer">
                        <HeaderTemplate>
                            Report Viewer
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divReportViewer" class="divReportViewerCSS">
                                <CR:CrystalReportViewer ID="CrystalReportViewerLeaveReports" OnUnload="CrystalReportViewerLeaveReports_Unload"
                                    runat="server" AutoDataBind="true" ToolPanelView="None" HasToggleGroupTreeButton="false"
                                    HasPageNavigationButtons="True" HasDrilldownTabs="False" SeparatePages="False"
                                    BestFitPage="False" GroupTreeStyle-ShowLines="False" PageZoomFactor="100" HasCrystalLogo="False"
                                    PrintMode="Pdf" Enabled="true" ToolbarStyle-CssClass="report-ToolbarStyle" BorderWidth="0px"
                                    ViewStateMode="Enabled" DisplayStatusbar="false" Width="100%" />
                                <CR:CrystalReportSource ID="CrystalReportSourceLeaveReports" runat="server">
                                </CR:CrystalReportSource>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelStaffLeaveStatusReport" Style="display: none; width: 600px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragStaffLeaveStatusReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageStaffLeaveStatusReportyHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbStaffLeaveStatusReportHeader" runat="server" Text="Staff Leave Status Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseStaffLeaveStatusReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Leave Status For?
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblStaffLeaveStatusReportsFor" OnSelectedIndexChanged="rblStaffLeaveStatusReportsFor_SelectedIndexChanged"
                                            RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                            <asp:ListItem Selected="True">All Employees</asp:ListItem>
                                            <asp:ListItem>Posting Location/Branch</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Posting Location/Branch:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStaffLeaveStatusReportPostingLocation" Enabled="false" AppendDataBoundItems="True"
                                            runat="server">
                                            <asp:ListItem Value="0">Select Company Branch</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbStaffLeaveStatusReportPerDepartment" OnCheckedChanged="cbStaffLeaveStatusReportPerDepartment_CheckedChanged"
                                            Text="View staff leave status report per department?" AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Department:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStaffLeaveStatusReportDepartment" Enabled="false" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress2" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewStaffLeaveStatusReport" OnClick="lnkBtnViewStaffLeaveStatusReport_Click"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderStaffLeaveStatusReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerLeaveReports$TabPanelLeaveReports$lnkBtnStaffLeaveStatusReport"
                    PopupControlID="PanelStaffLeaveStatusReport" CancelControlID="ImageButtonCloseStaffLeaveStatusReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderStaffLeaveStatusReport" TargetControlID="PanelStaffLeaveStatusReport"
                    DragHandleID="PanelDragStaffLeaveStatusReport" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelSingleStaffLeaveStatusReport" Style="display: none; width: 500px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragSingleStaffLeaveStatusReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageSingleStaffLeaveStatusReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbSingleStaffLeaveStatusReportHeader" runat="server" Text="Single Staff Leave Status Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseSingleStaffLeaveStatusReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Staff Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleStaffLeaveStatusReportStaffName" runat="server">
                                            <asp:ListItem Value="0">Select Staff Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress3" runat="server" />
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldSingleStaffLeaveStatusReportPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnViewSingleStaffLeaveStatusReport" OnClick="lnkBtnViewSingleStaffLeaveStatusReport_Click"
                                ToolTip="View single staff leave status report" runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderSingleStaffLeaveStatusReport" RepositionMode="None"
                    X="270" Y="80" TargetControlID="TabContainerLeaveReports$TabPanelLeaveReports$lnkBtnSingleStaffLeaveStatusReport"
                    PopupControlID="PanelSingleStaffLeaveStatusReport" CancelControlID="ImageButtonCloseSingleStaffLeaveStatusReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderSingleStaffLeaveStatusReport" TargetControlID="PanelSingleStaffLeaveStatusReport"
                    DragHandleID="PanelDragSingleStaffLeaveStatusReport" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelLeaveApplicationsApprovedPerDurationReport" Style="display: none;
                    width: 560px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragLeaveApplicationsApprovedPerDurationReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageLeaveApplicationsApprovedPerDurationReportHeader" runat="server"
                                            ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbLeaveApplicationsApprovedPerDurationReport" runat="server" Text="Leave Applications Approved Per Duration Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseLeaveApplicationsApprovedPerDurationReport"
                                            ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Between Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFromDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonFromDate" ToolTip="Pick from date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtFromDate" PopupButtonID="ImageButtonFromDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtFromDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtFromDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtFromDate_MaskedEditValidator" runat="server" ControlExtender="txtFromDate_MaskedEditExtender"
                                            ControlToValidate="txtFromDate" CssClass="errorMessage" ErrorMessage="txtFromDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        To Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtToDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonToDate" ToolTip="Pick to date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="ImageButtonToDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtToDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtToDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtToDate_MaskedEditValidator" runat="server" ControlExtender="txtToDate_MaskedEditExtender"
                                            ControlToValidate="txtToDate" CssClass="errorMessage" ErrorMessage="txtToDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid to date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewLeaveApplicationsApprovedPerDurationReport" OnClick="lnkBtnViewLeaveApplicationsApprovedPerDurationReport_Click"
                                ToolTip="View leave applications approved per duration report" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderLeaveApplicationsApprovedPerDurationReport"
                    RepositionMode="None" X="270" Y="80" TargetControlID="TabContainerLeaveReports$TabPanelLeaveReports$lnkBtnLeaveApplicationsApprovedPerDurationReport"
                    PopupControlID="PanelLeaveApplicationsApprovedPerDurationReport" CancelControlID="ImageButtonCloseLeaveApplicationsApprovedPerDurationReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderLeaveApplicationsApprovedPerDurationReport"
                    TargetControlID="PanelLeaveApplicationsApprovedPerDurationReport" DragHandleID="PanelDragLeaveApplicationsApprovedPerDurationReport"
                    runat="server">
                </asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
