﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Leave_Days_Allocations : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetLeaveTypes(ddlLeaveType, "Leave Type");
                GetEmployeesAllocated();//get employes to be allocated the leave days
                DisplayLeaveDaysAllocations();//dislay available leave days allocations
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddLeaveDayAllocationControls();
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            return;
        }
        //clear add leave day allocation controls
        private void ClearAddLeaveDayAllocationControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddLeaveDayAllocation);
            HiddenFieldBatchID.Value = "";
            lnkBtnSaveLeaveDayAllocation.Visible = true;
            lnkBtnUpdateLeaveDayAllocation.Enabled = false;
            lnkBtnUpdateLeaveDayAllocation.Visible = true;
            lnkBtnClearLeaveDayAllocation.Visible = true;
            ImageButtonFromDate.Visible = true;
            ImageButtonToDate.Visible = true;
            lbAddLeaveDayAllocationHeader.Text = "Add New Leave Day Allocation";
            ImageAddLeaveDayAllocationHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
            return;
        }
        //get employee being allocated the leave
        private void GetEmployeesAllocated()
        {
            try
            {
                ddlAllocatedTo.Items.Clear();
                object _getEmployees = db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686").OrderBy(p => p.StaffName);//remove super user id "c8f451c1-b573-e411-8a52-68a3c4aec686" and select ive active employees
                ddlAllocatedTo.DataSource = _getEmployees;
                ddlAllocatedTo.Items.Insert(0, new ListItem("Select Allocated To"));
                ddlAllocatedTo.Items.Insert(1, new ListItem("All Employees"));
                ddlAllocatedTo.Items.Insert(2, new ListItem("Females Only"));
                ddlAllocatedTo.Items.Insert(3, new ListItem("Males Only"));
                ddlAllocatedTo.DataTextField = "StaffName";
                ddlAllocatedTo.DataValueField = "StaffID";
                ddlAllocatedTo.AppendDataBoundItems = true;
                ddlAllocatedTo.DataBind();
                return;
            }
            catch { }
        }
        //validate leave days allocation controls'
        private string ValidateLeaveDaysAllocationControls()
        {
            if (ddlLeaveType.SelectedIndex == 0)
                return "Select leave type!";
            else if (_hrClass.isDateValid(txtFromDate.Text.Trim()) == false)
                return "Invalid 'From Which Date' entered!";
            else if (_hrClass.ValidateDateTextBox(txtFromDate) == false)
                return "Invalid 'From Which Date' entered!";
            else if (_hrClass.isDateValid(txtToDate.Text.Trim()) == false)
                return "Invalid 'To Which Date' entered!";
            else if (_hrClass.ValidateDateTextBox(txtToDate) == false)
                return "Invalid 'To Which Date' entered!";
            else if (Convert.ToDateTime(txtFromDate.Text.Trim()) > Convert.ToDateTime(txtToDate.Text.Trim()))
                return "Invalid duration selected. 'From Which Date' cannot be after 'To Which Date'!!";
            else if (txtDaysAllocated.Text.Trim() == "")
                return "Enter days allocated!";
            else if (_hrClass.isNumberValid(txtDaysAllocated.Text.Trim()) == false)
                return "Invalid value of number of days allocated entered! Enter a valid numeric value for days allocated!";
            else if (ddlAllocatedTo.SelectedIndex == 0)
                return "Select to  whom are the days being allocated to!";
            else return "";
        }
        //save leave allocation details
        private void SaveLeaveDaysAllocation()
        {
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            try
            {
                string _error = ValidateLeaveDaysAllocationControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveDaysAllocation);
                    return;
                }
                else
                {
                    string _isLeaveAllocationValid = IsLeaveAllocationValid();//check if the allocation being performed is valid
                    if (_isLeaveAllocationValid != "")//check if the leave days are being allocated to the relevant  group
                    {
                        _hrClass.LoadHRManagerMessageBox(1, _isLeaveAllocationValid, this, PanelLeaveDaysAllocation);
                        return;
                    }
                    else
                    {
                        //get last batch id
                        int _lastBatchID = 0;
                        string _noOfDays = txtDaysAllocated.Text.Trim();
                        string _allocatedTo = ddlAllocatedTo.SelectedItem.ToString();
                        _lastBatchID = Convert.ToInt32(db.Leaves.Max(p => p.leave_iBatch));// get last batch id
                        int _batchID = (_lastBatchID + 1);//increase last batch id by 1 to get a batch id for days allocation
                        //save leave days allocation depending on to who is selected

                        if (ddlAllocatedTo.SelectedIndex == 1)//all employees
                        {
                            //allocate leave days to all active employees
                            foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.StaffStatus == "Active" && p.StaffID != _hrClass.SuperAdminID()))//active employees minus super admin
                            {
                                //insert values
                                InsertLeaveDaysAllocation(getStaff.StaffID, _batchID, "A");// A for all employees
                            }
                        }
                        else if (ddlAllocatedTo.SelectedIndex == 2)//only females
                        {
                            //allocate leave days to all active female employees
                            foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.Gender == "Female" && p.StaffStatus == "Active"))//active female employees
                            {
                                //insert values
                                InsertLeaveDaysAllocation(getStaff.StaffID, _batchID, "F");// F for females
                            }
                        }
                        else if (ddlAllocatedTo.SelectedIndex == 3)//only males
                        {
                            //alocate leave days to all male employees who are active
                            foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.Gender == "Male" && p.StaffStatus == "Active"))//active male employees
                            {
                                //insert values
                                InsertLeaveDaysAllocation(getStaff.StaffID, _batchID, "M");// M for Males
                            }
                        }
                        else//if a single employee is seleced
                        {
                            Guid _staffID = _hrClass.ReturnGuid(ddlAllocatedTo.SelectedValue);
                            if (db.Leaves.Any(p => p.leave_cTrType == 'G' && p.leave_siLeaveTypeID == Convert.ToInt16(ddlLeaveType.SelectedValue) && p.leave_dtFrom.Value.Date == Convert.ToDateTime(txtFromDate.Text.Trim()).Date && p.leave_dtTill.Value.Date == Convert.ToDateTime(txtToDate.Text.Trim()).Date && p.leave_uiStaffID == _staffID))
                            {
                                _hrClass.LoadHRManagerMessageBox(1, "Save failed. Leave days allocation for the leave type selected for the duration specified already exists!", this, PanelLeaveDaysAllocation);
                                return;
                            }
                            else
                                InsertLeaveDaysAllocation(_staffID, _batchID, "i");// i for individual employee
                        }
                        HiddenFieldBatchID.Value = _batchID.ToString();
                        _hrClass.LoadHRManagerMessageBox(2, _noOfDays + " leave day(s) has been allocated to " + _allocatedTo + ".", this, PanelLeaveDaysAllocation);
                        lnkBtnUpdateLeaveDayAllocation.Enabled = true;
                        //refresh leave days allocations
                        DisplayLeaveDaysAllocations();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaveDaysAllocation);
                return;
            }
        }
        //insert leave alloaction days
        private void InsertLeaveDaysAllocation(Guid _staffID, int _batchID, string _allocatedTo)
        {
            Leave newLeaveAllocation = new Leave();
            newLeaveAllocation.leave_uiStaffID = _staffID;
            newLeaveAllocation.leave_siLeaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);
            newLeaveAllocation.leave_cTrType = 'G';//g for leave days gained
            newLeaveAllocation.leave_dtFrom = Convert.ToDateTime(txtFromDate.Text.Trim());
            newLeaveAllocation.leave_dtTill = Convert.ToDateTime(txtToDate.Text.Trim());
            newLeaveAllocation.leave_fNoDaysAlloc = Convert.ToDouble(txtDaysAllocated.Text.Trim());
            newLeaveAllocation.leave_vRemarks = txtReasons.Text.Trim();
            newLeaveAllocation.leave_iBatch = _batchID;
            newLeaveAllocation.leave_vAllocatedTo = _allocatedTo;
            newLeaveAllocation.leave_dtDateCreated = DateTime.Now;
            newLeaveAllocation.leave_bIsCancelled = false;
            db.Leaves.InsertOnSubmit(newLeaveAllocation);
            db.SubmitChanges();
        }
        //method for updating leave allocation details
        private void UpdateLeaveDaysAllocation(int _batchID)
        {
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            try
            {
                string _error = ValidateLeaveDaysAllocationControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveDaysAllocation);
                    return;
                }
                else
                {
                    string _isLeaveAllocationValid = IsLeaveAllocationValid();//check if the allocation being performed is valid
                    if (_isLeaveAllocationValid != "")//check if the leave days are being allocated to the relevant  group
                    {
                        _hrClass.LoadHRManagerMessageBox(1, _isLeaveAllocationValid, this, PanelLeaveDaysAllocation);
                        return;
                    }
                    else
                    {
                        //get last batch id
                        int _lastBatchID = 0;
                        _lastBatchID = Convert.ToInt32(db.Leaves.Max(p => p.leave_iBatch));// get last batch id
                        string _allocatedTo = ddlAllocatedTo.SelectedItem.ToString();
                        string _noOfDays = txtDaysAllocated.Text.Trim();
                        int _newBatchID = (_lastBatchID + 1);//increase last batch id by 1 to get a new batch id for days allocation
                        //get to whom were the leave days allocated to
                        string _previouslyAllocatedTo = "", _newlyAllocatedTo = "";
                        _newlyAllocatedTo = GenerateAllocatedCharacter();//to to whom is are the leave days being allocated to
                        foreach (Leave getBatchLeave in db.Leaves.Where(p => p.leave_iBatch == _batchID))
                        {
                            _previouslyAllocatedTo = getBatchLeave.leave_vAllocatedTo;//get only one value and break the loop
                            break;
                        }
                        //check if the leave days previously allocated to the group or individual selected,the do an update
                        if (_previouslyAllocatedTo == _newlyAllocatedTo)
                        {
                            EditLeaveDaysAllocation(_batchID, _newlyAllocatedTo);//
                        }
                        else
                        {
                            //delete all previous inserted leave days allocation details
                            DeleteLeaveDaysAllocationPerBatch(_batchID);
                            //create new leave days allocation records depending on to who are the days being allocated to
                            if (_newlyAllocatedTo == "A")//all employees
                            {
                                //allocate leave days to all active employees
                                foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.StaffStatus == "Active" && p.StaffID != _hrClass.SuperAdminID()))//active employees minus super admin
                                {
                                    //insert values
                                    InsertLeaveDaysAllocation(getStaff.StaffID, _newBatchID, _newlyAllocatedTo);
                                }
                            }
                            else if (_newlyAllocatedTo == "F")//only females
                            {
                                //allocate leave days to all active female employees
                                foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.Gender == "Female" && p.StaffStatus == "Active"))//active female employees
                                {
                                    //insert values
                                    InsertLeaveDaysAllocation(getStaff.StaffID, _newBatchID, _newlyAllocatedTo);
                                }
                            }
                            else if (_newlyAllocatedTo == "M")//only males
                            {
                                //alocate leave days to all male employees who are active
                                foreach (StaffMaster getStaff in db.StaffMasters.Where(p => p.Gender == "Male" && p.StaffStatus == "Active"))//active male employees
                                {
                                    //insert values
                                    InsertLeaveDaysAllocation(getStaff.StaffID, _newBatchID, _newlyAllocatedTo);
                                }
                            }
                            else if (_newlyAllocatedTo == "i")//an individual employee
                            {
                                InsertLeaveDaysAllocation(_hrClass.ReturnGuid(ddlAllocatedTo.SelectedValue), _newBatchID, _newlyAllocatedTo);
                            }

                        }
                        _hrClass.LoadHRManagerMessageBox(2, "Update saved. " + _noOfDays + " leave day(s) has been allocated to " + _allocatedTo + ".", this, PanelLeaveDaysAllocation);
                        //refresh leave days allocations
                        DisplayLeaveDaysAllocations();
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaveDaysAllocation);
                return;
            }
        }
        //edit leave days allocation
        private void EditLeaveDaysAllocation(int _batchID, string _allocatedTo)//depending on the batch id
        {
            foreach (Leave updateLeaveBatch in db.Leaves.Where(p => p.leave_iBatch == _batchID))
            {
                updateLeaveBatch.leave_siLeaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);
                updateLeaveBatch.leave_dtFrom = Convert.ToDateTime(txtFromDate.Text.Trim());
                updateLeaveBatch.leave_dtTill = Convert.ToDateTime(txtToDate.Text.Trim());
                updateLeaveBatch.leave_fNoDaysAlloc = Convert.ToDouble(txtDaysAllocated.Text.Trim());
                updateLeaveBatch.leave_vAllocatedTo = _allocatedTo;
                //check if the record is for a single employee
                if (_allocatedTo == "i")
                {
                    updateLeaveBatch.leave_uiStaffID = _hrClass.ReturnGuid(ddlAllocatedTo.SelectedValue);
                }
                updateLeaveBatch.leave_vRemarks = txtReasons.Text.Trim();
                db.SubmitChanges();
            }
        }
        //delete leave days allocation per batch
        private void DeleteLeaveDaysAllocationPerBatch(int _batchID)
        {
            foreach (Leave deleteLeaveDayAllocation in db.Leaves.Where(p => p.leave_iBatch == _batchID))
            {
                db.Leaves.DeleteOnSubmit(deleteLeaveDayAllocation);
                db.SubmitChanges();
            }
        }
        //get allocated to character
        private string GenerateAllocatedCharacter()
        {
            string allocatedToChar = "";
            if (ddlAllocatedTo.SelectedIndex == 1)//all aemployees
            {
                allocatedToChar = "A";
            }
            else if (ddlAllocatedTo.SelectedIndex == 2)//only females
            {
                allocatedToChar = "F";
            }
            else if (ddlAllocatedTo.SelectedIndex == 3)//only males
            {
                allocatedToChar = "M";
            }
            else
            {
                allocatedToChar = "i";
            }
            return allocatedToChar;
        }
        //check if the leave selected can be allocated to the group or employee selected
        private string IsLeaveAllocationValid()
        {
            string _applicableTo = "", _leaveType = "", _staffGender = "", _staffName;
            _leaveType = ddlLeaveType.SelectedItem.ToString();//get leave type
            int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);//get leave type id
            _applicableTo = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeID).leavetype_vApplicationTo;//get to who is the leave type applicable to
            Guid _staffID = Guid.Empty;
            if (ddlAllocatedTo.SelectedIndex == 1 || ddlAllocatedTo.SelectedIndex == 2 || ddlAllocatedTo.SelectedIndex == 3)//check if its all employees,Females only or Males only
            {
                //check if the leave type is applicable to females only and if leave days are being allocated to all employees
                if (_applicableTo == "Female" && ddlAllocatedTo.SelectedIndex == 1)//index 1 for all employees
                {
                    return "Leave days alloaction failed. " + _leaveType + " days cannot be allocated to all employees. " + _leaveType + " is applicable to female employees only!";
                }
                //check if the leave type is applicable to females and if leave days are being allocated to malae employees
                else if (_applicableTo == "Female" && ddlAllocatedTo.SelectedIndex == 3)//index 3 for only males
                {
                    return "Leave days allocation failed. " + _leaveType + " days cannot be allocated to male employees. " + _leaveType + " is applicable to female employees only!";
                }
                //check if the leave type is applicable to males and if leave days are being allocated to all employees
                else if (_applicableTo == "Male" && ddlAllocatedTo.SelectedIndex == 1)
                {
                    return "Leave days allocation failed. " + _leaveType + " days cannot be allocated to all employees. " + _leaveType + " is applicable to male employees only!";
                }
                //check if the leave type is applicable to males and if leave days are being allocated to female employees
                else if (_applicableTo == "Male" && ddlAllocatedTo.SelectedIndex == 2)
                {
                    return "Leave days allocation failed. " + _leaveType + " days cannot be allocated to female employees. " + _leaveType + " is applicable to male employees only!";
                }
                else return "";
            }
            else//check whether it is a single employee who is selected
            {
                //check when any individual employee is selected
                try
                {
                    _staffName = ddlAllocatedTo.SelectedItem.ToString();
                    _staffID = _hrClass.ReturnGuid(ddlAllocatedTo.SelectedValue);//get staff id
                    _staffGender = db.FUNC_GetStaffGender(_staffID);//get selected employee gender
                    //check if the leave type is applicable to females but leave days are being allocate to a male employee
                    if (_applicableTo == "Female" && _staffGender == "Male")
                    {
                        return "Leave days allocation failed. " + _leaveType + " is applicable to female employees only. The staff selected '" + _staffName + "' is a male employee!";

                    }
                    //check if the leave type is applicable to male employees and if the employee selected is a female employee
                    else if (_applicableTo == "Male" && _staffGender == "Female")
                    {
                        return "Leave days allocation failed! " + _leaveType + " is applicable to male employees only. The employee selected '" + _staffName + "' is a female employee!";
                    }
                    else return "";
                }
                catch (Exception ex)
                {
                    return "An error ocuured. " + ex.Message.ToString() + ". Please try again!";
                }
            }
        }

        //save leave days allocation
        protected void lnkBtnSaveLeaveDayAllocation_Click(object sender, EventArgs e)
        {
            SaveLeaveDaysAllocation();
            return;
        }
        //update leave days allocation details
        protected void lnkBtnUpdateLeaveDayAllocation_Click(object sender, EventArgs e)
        {
            UpdateLeaveDaysAllocation(Convert.ToInt32(HiddenFieldBatchID.Value));
            return;
        }
        //clear entry controls
        protected void lnkBtnClearLeaveDayAllocation_Click(object sender, EventArgs e)
        {
            ClearAddLeaveDayAllocationControls();
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            return;
        }
        //display leave days allocations
        private void DisplayLeaveDaysAllocations()
        {
            try
            {
                DataTable dt = new DataTable();
                int StaffMasterID = Convert.ToInt32(Session["StaffMasterID"]);
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("ID", typeof(int)));
                dt.Columns.Add(new DataColumn("Checked", typeof(bool)));
                dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));
                dt.Columns.Add(new DataColumn("From", typeof(string)));
                dt.Columns.Add(new DataColumn("Until", typeof(string)));
                dt.Columns.Add(new DataColumn("Days", typeof(string)));
                dt.Columns.Add(new DataColumn("Reasons", typeof(string)));
                dt.Columns.Add(new DataColumn("AllocatedTo", typeof(string)));
                DataRow dr;
                int countRows = 1;
                foreach (LeaveDaysAllocations_View leaveDaysAllocation in db.LeaveDaysAllocations_Views.OrderByDescending(p => p.leave_iBatch))//get all gained leaves
                {
                    //get to who are the leave days allocated to
                    string _allocatedToCType = leaveDaysAllocation.leave_vAllocatedTo, _allocatedTo = "";
                    if (_allocatedToCType == "A")//all employee
                    {
                        _allocatedTo = "All Employees";
                    }
                    else if (_allocatedToCType == "F")//females
                    {
                        _allocatedTo = "Only Females";
                    }
                    else if (_allocatedToCType == "M")//males
                    {
                        _allocatedTo = "Only Males";
                    }
                    else if (_allocatedToCType == "i")//individual
                    {
                        //get employee  name allocated the days
                        _allocatedTo = db.FUNC_GetStaffNameOnLeaveDaysAllocationByBatchID(leaveDaysAllocation.leave_iBatch);
                    }

                    dr = dt.NewRow();
                    dr[0] = countRows.ToString() + ".";
                    dr[1] = leaveDaysAllocation.leave_iBatch;
                    dr[2] = false;
                    dr[3] = leaveDaysAllocation.leavetype_vName;
                    dr[4] = _hrClass.ShortDateDayStart(leaveDaysAllocation.leave_dtFrom.ToString());
                    dr[5] = _hrClass.ShortDateDayStart(leaveDaysAllocation.leave_dtTill.ToString());
                    dr[6] = string.Format("{0:F2}", leaveDaysAllocation.leave_fNoDaysAlloc);
                    dr[7] = leaveDaysAllocation.leave_vRemarks;
                    dr[8] = _allocatedTo;
                    dt.Rows.Add(dr);
                    countRows++;
                }
                Session["gvLeaveDaysAllocationsData"] = dt;
                gvLeaveDaysAllocations.DataSource = dt;
                gvLeaveDaysAllocations.PageSize = 15;
                gvLeaveDaysAllocations.AllowPaging = true;
                gvLeaveDaysAllocations.DataBind();
            }
            catch { }
        }
        //check if any record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveDaysAllocations))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelLeaveDaysAllocation);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelLeaveDaysAllocation);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFBatchID = (HiddenField)_grv.FindControl("HFID");
                            //load edit leave days allocation details
                            LoadEditLeaveDaysAllocationControls(Convert.ToInt32(_HFBatchID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the selected record details for edit." + ex.Message.ToString() + ". Please try again!", this, PanelLeaveDaysAllocation);
            }
        }
        // populate the edit leave days allocation details
        private void LoadEditLeaveDaysAllocationControls(int _batchID)
        {
            //try
            //{
            ClearAddLeaveDayAllocationControls();
            LeaveDaysAllocations_View getLeaveDaysAllocation = db.LeaveDaysAllocations_Views.Single(p => p.leave_iBatch == _batchID);
            HiddenFieldBatchID.Value = _batchID.ToString();
            ddlLeaveType.SelectedValue = getLeaveDaysAllocation.leave_siLeaveTypeID.ToString();
            txtFromDate.Text = _hrClass.ShortDateDayStart(getLeaveDaysAllocation.leave_dtFrom.ToString());
            txtToDate.Text = _hrClass.ShortDateDayStart(getLeaveDaysAllocation.leave_dtTill.ToString());
            txtDaysAllocated.Text = getLeaveDaysAllocation.leave_fNoDaysAlloc.ToString();
            //get to who are the leave days allocated to
            string allocatedToCType = getLeaveDaysAllocation.leave_vAllocatedTo;
            if (allocatedToCType == "A")//all employee
            {
                ddlAllocatedTo.SelectedIndex = 1;
            }
            else if (allocatedToCType == "F")//females
            {
                ddlAllocatedTo.SelectedIndex = 2;
            }
            else if (allocatedToCType == "M")//males
            {
                ddlAllocatedTo.SelectedIndex = 3;
            }
            else if (allocatedToCType == "i")//individual
            {
                //get staff id for employee allocated the days
                Guid _staffID = (Guid)db.FUNC_GetStaffIDOnLeaveDaysAllocationByBatchID(getLeaveDaysAllocation.leave_iBatch);
                ddlAllocatedTo.SelectedValue = _staffID.ToString();
            }
            txtReasons.Text = getLeaveDaysAllocation.leave_vRemarks;
            lnkBtnSaveLeaveDayAllocation.Visible = false;
            lnkBtnUpdateLeaveDayAllocation.Enabled = true;
            lnkBtnClearLeaveDayAllocation.Visible = true;

            lbAddLeaveDayAllocationHeader.Text = "Edit Leave Days Allocation";
            ImageAddLeaveDayAllocationHeader.ImageUrl = "~/Images/icons/Medium/Modify.png";
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            //}
            //catch {
        }
        //check if any record is selected before view details controls are loaded
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveDaysAllocations))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to view its details!", this, PanelLeaveDaysAllocation);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to view its details at a time!", this, PanelLeaveDaysAllocation);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFBatchID = (HiddenField)_grv.FindControl("HFID");
                            //load view leave days allocation details
                            LoadViewLeaveDaysAllocationControls(Convert.ToInt32(_HFBatchID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the selected record details for viewing." + ex.Message.ToString() + ". Please try again!", this, PanelLeaveDaysAllocation);
            }
        }
        // populate view leave days allocation details
        private void LoadViewLeaveDaysAllocationControls(int _batchID)
        {
            LeaveDaysAllocations_View getLeaveDaysAllocation = db.LeaveDaysAllocations_Views.Single(p => p.leave_iBatch == _batchID);
            HiddenFieldBatchID.Value = _batchID.ToString();
            ddlLeaveType.SelectedValue = getLeaveDaysAllocation.leave_siLeaveTypeID.ToString();
            txtFromDate.Text = _hrClass.ShortDateDayStart(getLeaveDaysAllocation.leave_dtFrom.ToString());
            txtToDate.Text = _hrClass.ShortDateDayStart(getLeaveDaysAllocation.leave_dtTill.ToString());
            txtDaysAllocated.Text = getLeaveDaysAllocation.leave_fNoDaysAlloc.ToString();
            //get to who are the leave days allocated to
            string allocatedToCType = getLeaveDaysAllocation.leave_vAllocatedTo;
            if (allocatedToCType == "A")//all employee
            {
                ddlAllocatedTo.SelectedIndex = 1;
            }
            else if (allocatedToCType == "F")//females
            {
                ddlAllocatedTo.SelectedIndex = 2;
            }
            else if (allocatedToCType == "M")//males
            {
                ddlAllocatedTo.SelectedIndex = 3;
            }
            else if (allocatedToCType == "i")//individual
            {
                //get staff id for employee allocated the days
                Guid _staffID = (Guid)db.FUNC_GetStaffIDOnLeaveDaysAllocationByBatchID(getLeaveDaysAllocation.leave_iBatch);
                ddlAllocatedTo.SelectedValue = _staffID.ToString();
            }
            txtReasons.Text = getLeaveDaysAllocation.leave_vRemarks;

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddLeaveDayAllocation);
            lnkBtnSaveLeaveDayAllocation.Visible = false;
            lnkBtnUpdateLeaveDayAllocation.Visible = false;
            lnkBtnClearLeaveDayAllocation.Visible = false;
            ImageButtonFromDate.Visible = false;
            ImageButtonToDate.Visible = false;
            lbAddLeaveDayAllocationHeader.Text = "View Leave Days Allocation Details";
            ImageAddLeaveDayAllocationHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddLeaveDayAllocation.Show();
            return;
        }
        //load delete leave days allocations details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveDaysAllocations))//check if there is any record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelLeaveDaysAllocation);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected leave days allocation record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected leave days allocation records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaveDaysAllocation);
                return;
            }
        }
        //delete leave days allocation record
        private void DeleteLeaveDaysAllocations()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvLeaveDaysAllocations.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFBatchID = (HiddenField)_grv.FindControl("HFID");
                        int _batchID = Convert.ToInt32(_HFBatchID.Value);
                        //perform the delete
                        DeleteLeaveDaysAllocationPerBatch(_batchID);

                    }
                }
                //refresh leave days allocations after the delete is done
                DisplayLeaveDaysAllocations();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected leave days allocation record has been deleted.", this, PanelLeaveDaysAllocation);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected leave days allocation records have been deleted.", this, PanelLeaveDaysAllocation);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaveDaysAllocation);
                return;
            }
        }
        //delete the selected leave days allocation record
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteLeaveDaysAllocations();//delete selected  record
            return;
        }
    }
}