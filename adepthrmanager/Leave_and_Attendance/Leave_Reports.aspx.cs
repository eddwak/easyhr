﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Leave_Reports : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerReports _hrReports = new HRManagerReports();
        static short loggedStaffPostedAtID = 0;
        ReportDataSource _reportSource = new ReportDataSource();
        DataTable dt = new DataTable();
       // ReportDocument leaveReport = new ReportDocument();
        string reportPath = "";

        //page init method for viewing leave leave reports
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["ReportType"] != null)
                {

                   LeaveReportViewing();
                }
            }
            catch { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlStaffLeaveStatusReportPostingLocation, 2000, "Company Branch");//display company branches(2000) available
                _hrClass.GetListingItems(ddlStaffLeaveStatusReportDepartment, 3000, "Department");//display departments(3000) available
                _hrClass.GetStaffNames(ddlSingleStaffLeaveStatusReportStaffName, "Staff Name");//populate staff name dropdown
            }
            try
            {
                loggedStaffPostedAtID = Convert.ToInt16(Session["LoggedStaffPostedAtID"]);//get the location where the logged in employee is posed at
            }
            catch { }

        }

        //load view staff leave status report
        protected void lnkBtnStaffLeaveStatusReport_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderStaffLeaveStatusReport.Show(); return;
        }
        //check if which report are viewing if/itz for all the employee's or for all per a posting location
        protected void rblStaffLeaveStatusReportsFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderStaffLeaveStatusReport.Show();
            if (rblStaffLeaveStatusReportsFor.SelectedIndex == 0)//viewing all employees leave status
            {
                ddlStaffLeaveStatusReportPostingLocation.Enabled = false;
                ddlStaffLeaveStatusReportPostingLocation.SelectedIndex = 0;
                return;
            }
            else if (rblStaffLeaveStatusReportsFor.SelectedIndex == 1)//viewing leave status for a particular posting location
            {
                ddlStaffLeaveStatusReportPostingLocation.Enabled = true;
                //ddlStaffLeaveStatusReportPostingLocation.SelectedValue = loggedStaffPostedAtID.ToString();
                return;
            }
        }
        //check if we are viewing employees' leave status per employee
        protected void cbStaffLeaveStatusReportPerDepartment_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderStaffLeaveStatusReport.Show();
            ddlStaffLeaveStatusReportDepartment.SelectedIndex = 0;
            if (cbStaffLeaveStatusReportPerDepartment.Checked == true)
            {
                ddlStaffLeaveStatusReportDepartment.Enabled = true;
            }
            else if (cbStaffLeaveStatusReportPerDepartment.Checked == false)
            {
                ddlStaffLeaveStatusReportDepartment.Enabled = false;
            }
        }
        //method for viewing staff leave status reports
        private void ViewStaffLeaveStatusReport()
        {
            ModalPopupExtenderStaffLeaveStatusReport.Show();
            try
            {
                if (rblStaffLeaveStatusReportsFor.SelectedIndex == 1 && ddlStaffLeaveStatusReportPostingLocation.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select company branch!", this, PanelLeavesReports);
                    ddlStaffLeaveStatusReportPostingLocation.Focus();
                    return;
                }
                else if (cbStaffLeaveStatusReportPerDepartment.Checked == true && ddlStaffLeaveStatusReportDepartment.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Select department!", this, PanelLeavesReports);
                    ddlStaffLeaveStatusReportDepartment.Focus();
                    return;
                }
                else
                {
                    //check if the we are viewing leave status for all the emeployees 
                    if (rblStaffLeaveStatusReportsFor.SelectedIndex == 0)//leave ststus for all the employees
                    {
                        //check if we are viewing leave status report for all employees or per depart
                        if (cbStaffLeaveStatusReportPerDepartment.Checked == false)//for all company employees
                        {
                            Session["ReportType"] = "AllStaffLeaveStatus";
                            Session["ReportTitle"] = "All Employees' Leave Status";
                        }
                        else if (cbStaffLeaveStatusReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlStaffLeaveStatusReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffLeaveStatusPerDepartment";
                            Session["ReportTitle"] = "Employee Leave Status in " + _departmentName + " department";
                        }
                    }
                    else if (rblStaffLeaveStatusReportsFor.SelectedIndex == 1)//leave status for employees in a posting location
                    {
                        string _employeeLeaveStatusPostingLocation = ddlStaffLeaveStatusReportPostingLocation.SelectedItem.ToString();
                        HttpContext.Current.Session["StaffLeaveStatusPostingLocation"] = _employeeLeaveStatusPostingLocation;
                        if (cbStaffLeaveStatusReportPerDepartment.Checked == false)//for all employees in a posting location
                        {
                            Session["ReportType"] = "AllStaffLeaveStatusPerPostingLocation";
                            Session["ReportTitle"] = "Employee Leave Status For " + _employeeLeaveStatusPostingLocation;
                        }
                        else if (cbStaffLeaveStatusReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlStaffLeaveStatusReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffLeaveStatusPerDepartmentInAPostingLocation";
                            Session["ReportTitle"] = "Employee Leave Status in " + _employeeLeaveStatusPostingLocation + ", " + _departmentName + " department";
                        }
                    }
                    ModalPopupExtenderStaffLeaveStatusReport.Hide();
                    LeaveReportViewing();//view report
                    TabContainerLeaveReports.ActiveTabIndex = 1;//report viewer
                    //return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeavesReports);
                return;
            }
        }
        //view staff leave report
        protected void lnkBtnViewStaffLeaveStatusReport_Click(object sender, EventArgs e)
        {
            ViewStaffLeaveStatusReport();

            return;
        }

        //load view single staff leave status report popup
        protected void lnkBtnSingleStaffLeaveStatusReport_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderSingleStaffLeaveStatusReport.Show(); return;
        }

        //method for viewing a single staff leave status report
        private void ViewSingleStaffLeaveStatusReport()
        {

            ModalPopupExtenderSingleStaffLeaveStatusReport.Show();
            try 
            {
                if (ddlSingleStaffLeaveStatusReportStaffName.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No staff selected. Please select an employee!", this, PanelLeavesReports);
                    return;
                }
                else
                {
                    string _staffID = ddlSingleStaffLeaveStatusReportStaffName.SelectedValue.ToString();
                    Session["StaffID"] = _staffID.ToString();
                    Session["ReportType"] = "SingleStaffLeaveStatus";
                    Session["ReportTitle"] = "Single Staff Leave Status";

                    ModalPopupExtenderSingleStaffLeaveStatusReport.Hide();
                    LeaveReportViewing();//view the report
                    TabContainerLeaveReports.ActiveTabIndex = 1;//report viewer
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeavesReports);
                return;
            }
        }
        //view single statff leave status report
        protected void lnkBtnViewSingleStaffLeaveStatusReport_Click(object sender, EventArgs e)
        {
            ViewSingleStaffLeaveStatusReport(); return;
        }
        //validate view leave applicatins approved per duratio  controls
        private string ValidateViewLeaveApplicationsApprovedPerDurationReportControls()
        {
            if (_hrClass.isDateValid(txtFromDate.Text.Trim()) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtFromDate) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.isDateValid(txtToDate.Text.Trim()) == false)
            {
                return "Invalid end date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtToDate) == false)
            {
                return "Invalid end date entered!";
            }
            else if (Convert.ToDateTime(txtFromDate.Text.Trim()) > Convert.ToDateTime(txtToDate.Text.Trim()))
            {
                return "Start date cannot be greater than end date!";
            }
            else return "";
        }
        //method for viewing a leave appications approved during a specified durations report
        private void ViewLeaveApplicationsApprovedPerDurationReport()
        {

            ModalPopupExtenderLeaveApplicationsApprovedPerDurationReport.Show();
            try
            {
                string _error = ValidateViewLeaveApplicationsApprovedPerDurationReportControls();//check for error
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeavesReports);
                    return;
                }
                else
                {
                    Session["LeaveApprovalFromDate"] = txtFromDate.Text.Trim().ToString();
                    Session["LeaveApprovalToDate"] = txtToDate.Text.Trim().ToString();
                    Session["ReportType"] = "LeaveApplicationsApprovedPerDuration";
                    Session["ReportTitle"] = "Leave Applications Approved Per Duration";

                    ModalPopupExtenderLeaveApplicationsApprovedPerDurationReport.Hide();
                    LeaveReportViewing();//view the report
                    TabContainerLeaveReports.ActiveTabIndex = 1;//report viewer
                }

            }

            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeavesReports);
                return;
            }
        }
        //view leave applications approved during a specified duration
        protected void lnkBtnViewLeaveApplicationsApprovedPerDurationReport_Click(object sender, EventArgs e)
        {
            ViewLeaveApplicationsApprovedPerDurationReport(); return;
        }

        //method for viewing leave reports
        private void LeaveReportViewing()
        {

        /*    if (Session["ReportType"].ToString() == "AllStaffLeaveStatus")//ALL EMPLOYEES LEAVE STATUS REPORT
            {
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllEmployeesLeaveStatus.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_AllStaffLeaveStatusReport();
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                //leaveReport.SetParameterValue("EARNEDUPTO", earnedUptoHeader.ToUpper());
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }
            else if (Session["ReportType"].ToString() == "StaffLeaveStatusPerDepartment")//EMPLOYEES LEAVE STATUS PER DEPARTMENT REPORT
            {
                string departmentName = Session["ReportDepartmentName"].ToString();//GET DEPARTMENT TO VIEW VIEW LEAVE STATUS
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllEmployeesLeaveStatus.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffLeaveStatusReportPerDepartment(departmentName);
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                //leaveReport.SetParameterValue("EARNEDUPTO", earnedUptoHeader.ToUpper());
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }
            else if (Session["ReportType"].ToString() == "AllStaffLeaveStatusPerPostingLocation")//EMPLOYEES LEAVE STATUS REPORT IN A POSTING LOCATION
            { 
                string _postingLocation = HttpContext.Current.Session["StaffLeaveStatusPostingLocation"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllEmployeesLeaveStatus.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffLeaveStatusReportPerPostingLocation(_postingLocation);
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                //leaveReport.SetParameterValue("EARNEDUPTO", earnedUptoHeader.ToUpper());
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }
            else if (Session["ReportType"].ToString() == "StaffLeaveStatusPerDepartmentInAPostingLocation")//EMPLOYEES LEAVE STATUS PER DEPARTMENT REPORT IN A POSTING LOCATION
            {
                string _postingLocation = HttpContext.Current.Session["StaffLeaveStatusPostingLocation"].ToString();
                string departmentName = Session["ReportDepartmentName"].ToString();//GET DEPARTMENT TO VIEW VIEW LEAVE STATUS
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllEmployeesLeaveStatus.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffLeaveStatusReportPerPostingLocationPerDepartment(_postingLocation, departmentName);
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                //leaveReport.SetParameterValue("EARNEDUPTO", earnedUptoHeader.ToUpper());
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }
            else if (Session["ReportType"].ToString() == "SingleStaffLeaveStatus")//SINGLE STAFF LEAVE STATUS REPORT
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["StaffID"].ToString());//GET STATFF TO VIEW LEAVE STATUS
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_SingleEmployeeLeaveStatus.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_SingleStaffLeaveStatus(_staffID);
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                //leaveReport.SetParameterValue("EARNEDUPTO", earnedUptoHeader.ToUpper());
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }
            else if (Session["ReportType"].ToString() == "LeaveApplicationsApprovedPerDuration")//APPROVED LEAVES PER DAY REPORT
            {
                string _approvalFromDate = Session["LeaveApprovalFromDate"].ToString();
                string _approvalToDate = Session["LeaveApprovalToDate"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_ApprovedLeavesPerDuration.rpt");
                leaveReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_LeaveApplicationsApprovedPerDuration(_approvalFromDate, _approvalToDate);
                leaveReport.SetDataSource(_reportData);
                leaveReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                leaveReport.SetParameterValue("FromDate", _approvalFromDate);
                leaveReport.SetParameterValue("ToDate", _approvalToDate);
                CrystalReportViewerLeaveReports.ReportSource = leaveReport;
            }*/
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            //leaveReport.Close();
            //leaveReport.Dispose();
        }
        protected void CrystalReportViewerLeaveReports_Unload(object sender, EventArgs e)
        {
            //leaveReport.Close();
            //leaveReport.Dispose();
        }
        public void LoadLeaveReport()
        {
            //try
            //{
            //    ReportViewerLeaveReports.Reset();//clear data in the report viewer
            //    if (Session["ReportType"].ToString() == "AllStaffLeaveStatus")//ALL STAFF LEAVE STATUS REPORT
            //    {
            //        dt = _hrReports.rpt_AllStaffLeaveStatusReport();
            //    }
            //    else if (Session["ReportType"].ToString() == "StaffLeaveStatusPerDepartment")//LEAVE STATUS REPORT FOR ALL EMPLOYEES IN A PARTICULAR DEPARTMENT
            //    {
            //        string _departmentName = Session["ReportDepartmentName"].ToString();
            //        dt = _hrReports.rpt_StaffLeaveStatusReportPerDepartment(_departmentName);
            //    }
            //    else if (Session["ReportType"].ToString() == "AllStaffLeaveStatusPerPostingLocation")//LEAVE STATUS REPORT FOR ALL EMPLOYEES IN A POSTING LOCCATION/BRANCH
            //    {
            //        string _postingLocation = Session["StaffLeaveStatusPostingLocation"].ToString();
            //        dt = _hrReports.rpt_StaffLeaveStatusReportPerPostingLocation(_postingLocation);
            //    }
            //    else if (Session["ReportType"].ToString() == "StaffLeaveStatusPerDepartmentInAPostingLocation")
            //    {
            //        string _postingLocation = Session["StaffLeaveStatusPostingLocation"].ToString();
            //        string _departmentName = Session["ReportDepartmentName"].ToString();
            //        dt = _hrReports.rpt_StaffLeaveStatusReportPerPostingLocationPerDepartment(_postingLocation, _departmentName);
            //    }

            //    _reportSource = new ReportDataSource("DataSet_AllStaffLeaveStatusReport", dt);
            //    ReportViewerLeaveReports.LocalReport.ReportPath = "Leave_and_Attendance\\Leave_Reports\\report_AllStaffLeaveStatus.rdlc";
            //    //create parameter for the report title
            //    ReportParameter _reportTitle = new ReportParameter("ReportType", Session["ReportTitle"].ToString().ToUpper());
            //    ReportViewerLeaveReports.LocalReport.SetParameters(_reportTitle);
            //    ReportViewerLeaveReports.LocalReport.DataSources.Add(_reportSource);
            //    ReportViewerLeaveReports.ProcessingMode = ProcessingMode.Remote; //Bodge due Bug in reportViewer
            //    ReportViewerLeaveReports.ProcessingMode = ProcessingMode.Local;

            //    ReportViewerLeaveReports.LocalReport.Refresh();
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, "Failed in loading report " + ex.Message.ToString(), this, PanelLeavesReports); return;
            //}
        }
    }
}