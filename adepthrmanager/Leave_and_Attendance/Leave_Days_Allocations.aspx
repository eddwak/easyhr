﻿<%@ Page Title="Leave Days Allocations" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Leave_Days_Allocations.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Leave_Days_Allocations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaveDaysAllocation" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeaveDaysAllocation" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new leave days allocation"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick=" LinkButtonEdit_Click" ToolTip="Edit leave days allocation details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View leave days allocation details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete leave days allocation" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvLeaveDaysAllocations" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                        EmptyDataText="No leave days allocation available!">
                        <Columns>
                            <asp:BoundField DataField="SNo" HeaderText="" ItemStyle-Width="4px" />
                            <asp:TemplateField ItemStyle-Width="1px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Checked") %>' />
                                    <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" />
                            <asp:BoundField DataField="From" HeaderText="From" />
                            <asp:BoundField DataField="Until" HeaderText="Until" />
                            <asp:BoundField DataField="Days" HeaderText="Days Allocated" ItemStyle-Width="110px"
                                ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="Reasons" HeaderText="Reason / Remarks" />
                            <asp:BoundField DataField="AllocatedTo" HeaderText="Allocated To" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddLeaveDayAllocation" Style="display: none; width: 650px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddLeaveDayAllocation" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddLeaveDayAllocationHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddLeaveDayAllocationHeader" runat="server" Text="Add Leave Day Allocation"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddLeaveDayAllocation" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Leave Days Allocation Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Leave Type:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlLeaveType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        From Which Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFromDate" Width="150px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonFromDate" ToolTip="Pick from which date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtFromDate" PopupButtonID="ImageButtonFromDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtFromDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtFromDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtFormDate_MaskedEditValidator" runat="server" ControlExtender="txtFromDate_MaskedEditExtender"
                                            ControlToValidate="txtFromDate" CssClass="errorMessage" ErrorMessage="txtFromDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                        <br />
                                        <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2014"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        To Which Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtToDate" Width="150px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonToDate" ToolTip="Pick to which date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="ImageButtonToDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtToDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtToDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtToDate_MaskedEditValidator" runat="server" ControlExtender="txtToDate_MaskedEditExtender"
                                            ControlToValidate="txtToDate" CssClass="errorMessage" ErrorMessage="txtToDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid to which date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Day[s] Allocated:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDaysAllocated" Width="150px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Allocated To:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAllocatedTo" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description/Reason:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtReasons" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldBatchID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddLeaveDayAllocationPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveLeaveDayAllocation" OnClick="lnkBtnSaveLeaveDayAllocation_Click"
                                ToolTip="Save leave days allocation" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateLeaveDayAllocation" OnClick="lnkBtnUpdateLeaveDayAllocation_Click"
                                CausesValidation="false" ToolTip="Update leave days allocation" Enabled="false"
                                runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearLeaveDayAllocation" OnClick="lnkBtnClearLeaveDayAllocation_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddLeaveDayAllocation" RepositionMode="None"
                    X="270" Y="10" TargetControlID="HiddenFieldAddLeaveDayAllocationPopup" PopupControlID="PanelAddLeaveDayAllocation"
                    CancelControlID="ImageButtonCloseAddLeaveDayAllocation" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddLeaveDayAllocation" TargetControlID="PanelAddLeaveDayAllocation"
                    DragHandleID="PanelDragAddLeaveDayAllocation" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
