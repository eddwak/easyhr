﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Leave_Types : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayLeaveTypes();//load leave types
            }
            //add delete record event to delete a leave type
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddLeaveTypeControls();
            ModalPopupExtenderAddLeaveType.Show();
            return;
        }
        private void ClearAddLeaveTypeControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddLeaveType);
            HiddenFieldLeaveTypeID.Value = "";
            lnkBtnSaveLeaveType.Visible = true;
            lnkBtnUpdateLeaveType.Enabled = false;
            lnkBtnUpdateLeaveType.Visible = true;
            lnkBtnClearLeaveType.Visible = true;
            lbAddLeaveTypeHeader.Text = "Add Leave Type";
            ImageAddLeaveTypeHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        }
        private string ValidateAddLeaveTypeControls()
        {
            if (txtLeaveTypeName.Text.Trim() == "")
            {
                txtLeaveTypeName.Focus(); return "Enter leave type name!";
            }
            else if (rblApplicableTo.SelectedIndex == -1)
            {
                rblApplicableTo.Focus(); return "Select to who is the leave applicable to!";
            }
            else if (txtMaximumLeaveDays.Text.Trim() == "")
            {
                txtMaximumLeaveDays.Focus(); return "Enter maximum leave days!";
            }
            else if (_hrClass.isNumberValid(txtMaximumLeaveDays.Text.Trim()) == false)
            {
                txtMaximumLeaveDays.Focus(); return "Incorrect maximum leave days entered!. Enter valid maximum leave days containing numeric values only!";
            }
            else if (Convert.ToDecimal(txtMaximumLeaveDays.Text.Trim()) < 1)
            {
                txtMaximumLeaveDays.Focus(); return "Maximum leave days cannot be less than one day!";
            }
            else if (rblLeaveDaysType.SelectedIndex == -1)
            {
                rblLeaveDaysType.Focus(); return "Select if leave days calculation will be based on calendar or working days!";
            }
            else if (rblIsContinous.SelectedIndex == -1)
            {
                rblIsContinous.Focus(); return "Select if the leave days will be continous or not!";
            }
            else if (rblCarryForwardLeaveDays.SelectedIndex == -1)
            {
                rblCarryForwardLeaveDays.Focus(); return "Select if leave days can be carried forward or not!";
            }
            else if (rblCarryForwardLeaveDays.SelectedIndex == 0 && txtDaysLimit.Text.Trim() == "")
            {
                txtDaysLimit.Focus(); return "Enter days that can be carried forward!";
            }
            else if (rblCarryForwardLeaveDays.SelectedIndex == 0 && txtDaysLimit.Text.Trim() == "0")
            {
                txtDaysLimit.Focus(); return "Zero limit days cannot be carried forward! Enter valid limit days!.";
            }
            else if (rblCarryForwardLeaveDays.SelectedIndex == 0 && _hrClass.isNumberValid(txtDaysLimit.Text.Trim()) == false)
            {
                txtDaysLimit.Focus(); return "Incorrect limit days entered! Enter valid limit days containing numeric values only!.";
            }
            else if (rblCarryForwardLeaveDays.SelectedIndex == 0 && Convert.ToDecimal(txtDaysLimit.Text.Trim()) < 1)
            {
                txtDaysLimit.Focus(); return "Limit days entered cannot be less than 1";
            }
            else if (rblIsReapplicablePerYear.SelectedIndex == -1)
            {
                rblIsReapplicablePerYear.Focus(); return "Check if the leave type is re-applicable per year or not!";
            }

            else if (rblCalculateLeaveDaysBalance.SelectedIndex == -1)
            {
                rblCalculateLeaveDaysBalance.Focus(); return "Check if the leave type balance will be calculated or not !";
            }
            else if (rblRequiresHRorManagerApproval.SelectedIndex == -1)
            {
                rblRequiresHRorManagerApproval.Focus(); return "Select if the leave requires HR or manager's approval or not !";
            }
            else if (rblHasApplicationLimitTime.SelectedIndex == -1)
            {
                rblHasApplicationLimitTime.Focus(); return "Check if the leave type has an application limit time !";
            }
            else if (rblHasApplicationLimitTime.SelectedIndex == 0 && txtApplicationLimitHours.Text.Trim() == "")
            {
                txtApplicationLimitHours.Focus(); return "Enter application limit hours !";
            }
            else if (rblHasApplicationLimitTime.SelectedIndex == 0 && _hrClass.isNumberValid(txtApplicationLimitHours.Text.Trim()) == false)
            {
                txtApplicationLimitHours.Focus(); return "Invalid limit hours entered. Enter a valid numeric value for limit hours !";
            }
            else if (rblHasApplicationLimitTime.SelectedIndex == 0 && Convert.ToDecimal(txtApplicationLimitHours.Text.Trim()) < 1)
            {
                txtApplicationLimitHours.Focus(); return "Application limit hours entered cannot be less than 1 hour";

            }
            else if (rblIsLeaveApplicableInHours.SelectedIndex == -1)
            {
                rblIsLeaveApplicableInHours.Focus(); return "Check if the leave is applicable in hours!";
            }
            else
                return "";
        }
        //method for addin a new leave type
        private void SaveLeaveType()
        {
            ModalPopupExtenderAddLeaveType.Show();
            try
            {
                string _newLeaveTypeName = txtLeaveTypeName.Text.Trim();
                string _error = ValidateAddLeaveTypeControls();//validate entry controls
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveTypes); return;
                }
                else if (db.LeaveTypes.Any(p => p.leavetype_vName.ToLower() == _newLeaveTypeName.ToLower()))//check if there exists a leave type with the name entered
                {
                    txtLeaveTypeName.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Save Failed! The leave type you are trying to add already exists!", this, PanelLeaveTypes); return;
                }
                else
                {
                    //add a new leave type into the leave type table
                    LeaveType newLeaveType = new LeaveType();
                    newLeaveType.leavetype_vName = _newLeaveTypeName;
                    newLeaveType.leavetype_vApplicationTo = rblApplicableTo.SelectedValue;
                    newLeaveType.leavetype_fMaxDays = Convert.ToDouble(txtMaximumLeaveDays.Text.Trim());
                    newLeaveType.leavetype_vDaysType = rblLeaveDaysType.SelectedValue;
                    if (rblIsContinous.SelectedIndex == 0)//check if leave type is continous
                    {
                        newLeaveType.leavetype_bIsContinuous = true;
                    }
                    else newLeaveType.leavetype_bIsContinuous = false;
                    if (rblCarryForwardLeaveDays.SelectedIndex == 0)//check if leave days can be carried foward
                    {
                        newLeaveType.leavetype_bIsCarriedFwd = true;
                        newLeaveType.leavetype_fCarryFwdDays = Convert.ToDouble(txtDaysLimit.Text.Trim());//enter limit days for the leave type that can be carried forward
                    }
                    else
                    {
                        newLeaveType.leavetype_bIsCarriedFwd = false;
                        newLeaveType.leavetype_fCarryFwdDays = 0;// o limit days when no limit days are carried forward
                    }
                    if (rblIsReapplicablePerYear.SelectedIndex == 0)//check if the leave type is reapplicable or not
                    {
                        newLeaveType.leavetype_bIsReapplicablePerYear = true;
                    }
                    else newLeaveType.leavetype_bIsReapplicablePerYear = false;
                    newLeaveType.leavetype_vDescription = txtDescription.Text.Trim();
                    if (rblCalculateLeaveDaysBalance.SelectedIndex == 0)//check if leave balance for the leave type is to calcualted
                    {
                        newLeaveType.leavetype_bCalculateLeaveBalance = true;
                    }
                    else newLeaveType.leavetype_bCalculateLeaveBalance = false;
                    //check if the leave type has application limit time
                    if (rblHasApplicationLimitTime.SelectedIndex == 0)
                    {
                        newLeaveType.leavetype_bHasApplicationLimitTime = true;
                        newLeaveType.leavetype_fLimitHours = Convert.ToDouble(txtApplicationLimitHours.Text.Trim());
                    }
                    else
                    {
                        newLeaveType.leavetype_bHasApplicationLimitTime = false;
                        newLeaveType.leavetype_fLimitHours = 0;
                    }
                    //check if the leave requires approva; for the HR/manager
                    if (rblRequiresHRorManagerApproval.SelectedIndex == 0)
                        newLeaveType.leavetype_bRequiresApproval = true;
                    else newLeaveType.leavetype_bRequiresApproval = false;
                    //check if the leave is aplicable on hour basis
                    if (rblIsLeaveApplicableInHours.SelectedIndex == 0)
                        newLeaveType.leavetype_bIsApplicableInHours = true;
                    else newLeaveType.leavetype_bIsApplicableInHours = false;
                    db.LeaveTypes.InsertOnSubmit(newLeaveType);
                    db.SubmitChanges();
                    HiddenFieldLeaveTypeID.Value = newLeaveType.leavetype_siLeaveTypeID.ToString();
                    _hrClass.LoadHRManagerMessageBox(2, "New leave type <b>" + _newLeaveTypeName + "</b> has been successfully saved.", this, PanelLeaveTypes);
                    lnkBtnUpdateLeaveType.Enabled = true;
                    //refresh leave types
                    DisplayLeaveTypes();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! " + ex.Message.ToString(), this, PanelLeaveTypes);
                return;
            }
        }
        //method for updating a leave type
        private void UpdateLeaveType(int LeaveTypeID)
        {
            ModalPopupExtenderAddLeaveType.Show();
            try
            {
                string _leaveTypeName = txtLeaveTypeName.Text.Trim();
                string _error = ValidateAddLeaveTypeControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveTypes);
                    return;
                }
                else if (db.LeaveTypes.Any(p => p.leavetype_vName.ToLower() == _leaveTypeName.ToLower() && p.leavetype_siLeaveTypeID != LeaveTypeID))//check if there exists a leave type with the name entered
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update Failed! The leave type you are trying to add already exists!", this, PanelLeaveTypes);
                    txtLeaveTypeName.Focus();
                    return;
                }
                else
                {
                    //update a leaveType
                    LeaveType updateLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == LeaveTypeID);
                    updateLeaveType.leavetype_vName = txtLeaveTypeName.Text.Trim();
                    updateLeaveType.leavetype_vApplicationTo = rblApplicableTo.SelectedValue;
                    updateLeaveType.leavetype_fMaxDays = Convert.ToDouble(txtMaximumLeaveDays.Text.Trim());
                    updateLeaveType.leavetype_vDaysType = rblLeaveDaysType.SelectedValue;
                    if (rblIsContinous.SelectedIndex == 0)//check if leave type is continous
                    {
                        updateLeaveType.leavetype_bIsContinuous = true;
                    }
                    else updateLeaveType.leavetype_bIsContinuous = false;
                    if (rblCarryForwardLeaveDays.SelectedIndex == 0)//check if leave days can be carried forward
                    {
                        updateLeaveType.leavetype_bIsCarriedFwd = true;
                        updateLeaveType.leavetype_fCarryFwdDays = Convert.ToDouble(txtDaysLimit.Text.Trim());//enter limit days for the leave type that can be carried forward
                    }
                    else
                    {
                        updateLeaveType.leavetype_bIsCarriedFwd = false;
                        updateLeaveType.leavetype_fCarryFwdDays = 0;// o limit days when no limit days are carried forward
                    }
                    if (rblIsReapplicablePerYear.SelectedIndex == 0)//check if leave type is reaplicable per year or not
                    {
                        updateLeaveType.leavetype_bIsReapplicablePerYear = true;
                    }
                    else updateLeaveType.leavetype_bIsReapplicablePerYear = false;
                    updateLeaveType.leavetype_vDescription = txtDescription.Text.Trim();
                    if (rblCalculateLeaveDaysBalance.SelectedIndex == 0)//check if leave balance for the leave type will calcualted
                    {
                        updateLeaveType.leavetype_bCalculateLeaveBalance = true;
                    }
                    else updateLeaveType.leavetype_bCalculateLeaveBalance = false;
                    //check if the leave type has an application limit time
                    if (rblHasApplicationLimitTime.SelectedIndex == 0)
                    {
                        updateLeaveType.leavetype_bHasApplicationLimitTime = true;
                        updateLeaveType.leavetype_fLimitHours = Convert.ToDouble(txtApplicationLimitHours.Text.Trim());
                    }
                    else
                    {
                        updateLeaveType.leavetype_bHasApplicationLimitTime = false;
                        updateLeaveType.leavetype_fLimitHours = 0;
                    }
                    //check if the leave requires HR/manager approval
                    if (rblRequiresHRorManagerApproval.SelectedIndex == 0)
                        updateLeaveType.leavetype_bRequiresApproval = true;
                    else updateLeaveType.leavetype_bRequiresApproval = false;
                    //check if the leave is alicable on hour basis
                    if (rblIsLeaveApplicableInHours.SelectedIndex == 0)
                        updateLeaveType.leavetype_bIsApplicableInHours = true;
                    else updateLeaveType.leavetype_bIsApplicableInHours = false;
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "Update for leave type <b>" + _leaveTypeName + "</b> has been successfully saved.", this, PanelLeaveTypes);
                    //refresh leave types
                    DisplayLeaveTypes();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update Failed ! " + ex.Message.ToString(), this, PanelLeaveTypes);
                return;
            }
        }
        protected void lnkBtnSaveLeaveType_Click(object sender, EventArgs e)
        {
            SaveLeaveType(); return;
        }
        protected void lnkBtnUpdateLeaveType_Click(object sender, EventArgs e)
        {
            UpdateLeaveType(Convert.ToInt32(HiddenFieldLeaveTypeID.Value.ToString())); return;
        }
        protected void lnkBtnClearLeaveType_Click(object sender, EventArgs e)
        {
            ClearAddLeaveTypeControls();
            ModalPopupExtenderAddLeaveType.Show(); return;
        }
        //diaplay leave types
        private void DisplayLeaveTypes()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ID", typeof(int)));
                dt.Columns.Add(new DataColumn("SNo", typeof(string)));//count rows
                dt.Columns.Add(new DataColumn("Checked", typeof(bool)));
                dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));
                dt.Columns.Add(new DataColumn("ApplicableTo", typeof(string)));
                dt.Columns.Add(new DataColumn("MaxDays", typeof(string)));
                dt.Columns.Add(new DataColumn("DaysCalc", typeof(string)));
                dt.Columns.Add(new DataColumn("Continous", typeof(string)));
                dt.Columns.Add(new DataColumn("CarryFwd", typeof(string)));
                dt.Columns.Add(new DataColumn("CarriedDays", typeof(string)));
                dt.Columns.Add(new DataColumn("ReapplicablePerYear", typeof(string)));
                dt.Columns.Add(new DataColumn("Description", typeof(string)));
                dt.Columns.Add(new DataColumn("CalculateLeaveBalance", typeof(string)));
                dt.Columns.Add(new DataColumn("HasApplicationLimitTime", typeof(string)));
                dt.Columns.Add(new DataColumn("LimitHours", typeof(string)));
                dt.Columns.Add(new DataColumn("RequiresApproval", typeof(string)));
                dt.Columns.Add(new DataColumn("ApplicableInHours", typeof(string)));
                DataRow dr;
                int countRows = 1;
                //get all leave types
                foreach (LeaveType getLeaveTypes in db.LeaveTypes.OrderBy(p => p.leavetype_vName))
                {
                    string _carryFwd = "No", _isContinous = "No", _isReapplicablePerYear = "No", _calculateLeaveBalance = "No", _hasApplicationLimitTime = "No", _requiresApproval = "No", _isApplicableInHours = "No";
                    if (getLeaveTypes.leavetype_bIsCarriedFwd == true)//check if leave days can be carried forwrd
                    {
                        _carryFwd = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bIsContinuous == true)//check if leave type is continous
                    {
                        _isContinous = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bIsReapplicablePerYear == true)//check if the leave type is reaplicable per year
                    {
                        _isReapplicablePerYear = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bCalculateLeaveBalance == true)//check if leave balance is to be calculcualted for the leave type
                    {
                        _calculateLeaveBalance = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bHasApplicationLimitTime == true)//check if the leave type has an application limit time
                    {
                        _hasApplicationLimitTime = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bRequiresApproval == true)//check if the leave type requires HR/Manager's approval
                    {
                        _requiresApproval = "Yes";
                    }
                    if (getLeaveTypes.leavetype_bIsApplicableInHours == true)//cxheck if the leave type is applicable in hours\
                    {
                        _isApplicableInHours = "Yes";
                    }
                    dr = dt.NewRow();
                    dr[0] = getLeaveTypes.leavetype_siLeaveTypeID; ;
                    dr[1] = countRows.ToString() + ".";
                    dr[2] = false;//check box
                    dr[3] = getLeaveTypes.leavetype_vName;
                    dr[4] = getLeaveTypes.leavetype_vApplicationTo;
                    dr[5] = getLeaveTypes.leavetype_fMaxDays;
                    dr[6] = getLeaveTypes.leavetype_vDaysType;
                    dr[7] = _isContinous;
                    dr[8] = _carryFwd;
                    dr[9] = getLeaveTypes.leavetype_fCarryFwdDays;
                    dr[10] = _isReapplicablePerYear;
                    dr[11] = getLeaveTypes.leavetype_vDescription;
                    dr[12] = _calculateLeaveBalance;
                    dr[13] = _hasApplicationLimitTime;
                    dr[14] = getLeaveTypes.leavetype_fLimitHours;
                    dr[15] = _requiresApproval;
                    dr[16] = _isApplicableInHours;
                    dt.Rows.Add(dr);
                    countRows++;
                }
                Session["gvLeaveTypesData"] = dt;
                gvLeaveTypes.DataSource = dt;
                gvLeaveTypes.AllowPaging = true;
                gvLeaveTypes.PageSize = 10;
                gvLeaveTypes.DataBind();
            }
            catch { }
        }
        //check if any leave type is selected before editing
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveTypes))//check if a leave type right is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no leave type selected!. Select a leave type to edit!", this, PanelLeaveTypes);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one leave type!. Select only one leave type to edit at a time!", this, PanelLeaveTypes);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFLeaveTypeID = (HiddenField)_grv.FindControl("HFID");
                            LoadEditLeaveTypeControls(Convert.ToInt32(_HFLeaveTypeID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        //load values for the leave type
        private void LoadEditLeaveTypeControls(int LeaveTypeID)
        {
            ClearAddLeaveTypeControls();
            //get leave type to edit
            LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == LeaveTypeID);
            HiddenFieldLeaveTypeID.Value = LeaveTypeID.ToString();
            txtLeaveTypeName.Text = getLeaveType.leavetype_vName;
            rblApplicableTo.SelectedValue = getLeaveType.leavetype_vApplicationTo;
            txtMaximumLeaveDays.Text = getLeaveType.leavetype_fMaxDays.ToString();
            rblLeaveDaysType.SelectedValue = getLeaveType.leavetype_vDaysType;
            if (getLeaveType.leavetype_bIsContinuous == true)//check if leave is continous
            {
                rblIsContinous.SelectedIndex = 0;//yes
            }
            else rblIsContinous.SelectedIndex = 1;//no
            if (getLeaveType.leavetype_bIsCarriedFwd == true)//check leave days are carried forward
            {
                rblCarryForwardLeaveDays.SelectedIndex = 0;//yes
                txtDaysLimit.Text = getLeaveType.leavetype_fCarryFwdDays.ToString();
            }
            else//check if leave days are not carried forward
            {
                rblCarryForwardLeaveDays.SelectedIndex = 1;//no
                txtDaysLimit.Text = "0";
            }
            if (getLeaveType.leavetype_bIsReapplicablePerYear == true)//check if leave type is reapplicable
            {
                rblIsReapplicablePerYear.SelectedIndex = 0;//yes
            }
            else rblIsReapplicablePerYear.SelectedIndex = 1;//no
            txtDescription.Text = getLeaveType.leavetype_vDescription;
            if (getLeaveType.leavetype_bCalculateLeaveBalance == true)//check if the leave type balance is to be claculated
            {
                rblCalculateLeaveDaysBalance.SelectedIndex = 0;//yes
            }
            else rblCalculateLeaveDaysBalance.SelectedIndex = 1;//no
            //check if the leave type has an application limit time
            if (getLeaveType.leavetype_bHasApplicationLimitTime == true) //yes
            {
                rblHasApplicationLimitTime.SelectedIndex = 0;
                txtApplicationLimitHours.Text = getLeaveType.leavetype_fLimitHours.ToString();
            }
            else
            {
                rblHasApplicationLimitTime.SelectedIndex = 1;
                txtApplicationLimitHours.Text = "0";
            }
            //check if the leave requires HR/Manager's approval
            if (getLeaveType.leavetype_bRequiresApproval == true)
                rblRequiresHRorManagerApproval.SelectedIndex = 0;
            else rblRequiresHRorManagerApproval.SelectedIndex = 1;
            //check if the leave is applicable on hour basis or not
            if (getLeaveType.leavetype_bIsApplicableInHours == true)
                rblIsLeaveApplicableInHours.SelectedIndex = 0;
            else rblIsLeaveApplicableInHours.SelectedIndex = 1;
            lnkBtnSaveLeaveType.Visible = false;
            lnkBtnUpdateLeaveType.Enabled = true;
            lbAddLeaveTypeHeader.Text = "Edit Leave Type";
            ImageAddLeaveTypeHeader.ImageUrl = "~/Images/icons/Medium/Modify.png";
            ModalPopupExtenderAddLeaveType.Show();
            txtLeaveTypeName.Focus();
            return;
        }
        //check if a leave type is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveTypes))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a leave type to view !", this, PanelLeaveTypes);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one leave type !. Select only one leave type to view its details at a time !", this, PanelLeaveTypes);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFLeaveTypeID = (HiddenField)_grv.FindControl("HFID");
                            //load view leave type details
                            LoadViewLeaveTypeControls(Convert.ToInt32(_HFLeaveTypeID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading leave type details for view purpose ! " + ex.Message.ToString(), this, PanelLeaveTypes);
                return;
            }
        }
        // populate the view user controls
        private void LoadViewLeaveTypeControls(int _leaveTypeID)
        {

            LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == _leaveTypeID);
            txtLeaveTypeName.Text = getLeaveType.leavetype_vName;
            rblApplicableTo.SelectedValue = getLeaveType.leavetype_vApplicationTo;
            txtMaximumLeaveDays.Text = getLeaveType.leavetype_fMaxDays.ToString();
            rblLeaveDaysType.SelectedValue = getLeaveType.leavetype_vDaysType;
            if (getLeaveType.leavetype_bIsContinuous == true)//check if leave is continous
            {
                rblIsContinous.SelectedIndex = 0;//yes
            }
            else rblIsContinous.SelectedIndex = 1;//no
            if (getLeaveType.leavetype_bIsCarriedFwd == true)//check leave days are carried forward
            {
                rblCarryForwardLeaveDays.SelectedIndex = 0;//yes
                txtDaysLimit.Text = getLeaveType.leavetype_fCarryFwdDays.ToString();
            }
            else//check if leave days are not carried forward
            {
                rblCarryForwardLeaveDays.SelectedIndex = 1;//no
                txtDaysLimit.Text = "0";
            }
            if (getLeaveType.leavetype_bIsReapplicablePerYear == true)//check if leave type is reapplicable
            {
                rblIsReapplicablePerYear.SelectedIndex = 0;//yes
            }
            else rblIsReapplicablePerYear.SelectedIndex = 1;//no
            txtDescription.Text = getLeaveType.leavetype_vDescription;
            if (getLeaveType.leavetype_bCalculateLeaveBalance == true)//check if the leave type balance is to be claculated
            {
                rblCalculateLeaveDaysBalance.SelectedIndex = 0;//yes
            }
            else rblCalculateLeaveDaysBalance.SelectedIndex = 1;//no
            //check if the leave type has an application limit time
            if (getLeaveType.leavetype_bHasApplicationLimitTime == true) //yes
            {
                rblHasApplicationLimitTime.SelectedIndex = 0;
                txtApplicationLimitHours.Text = getLeaveType.leavetype_fLimitHours.ToString();
            }
            else
            {
                rblHasApplicationLimitTime.SelectedIndex = 1;
                txtApplicationLimitHours.Text = "0";
            }
            //check if the leave requires HR/Manager's approval
            if (getLeaveType.leavetype_bRequiresApproval == true)
                rblRequiresHRorManagerApproval.SelectedIndex = 0;
            else rblRequiresHRorManagerApproval.SelectedIndex = 1;
            //check if the leave is applicable on hour basis or not
            if (getLeaveType.leavetype_bIsApplicableInHours == true)
                rblIsLeaveApplicableInHours.SelectedIndex = 0;
            else rblIsLeaveApplicableInHours.SelectedIndex = 1;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddLeaveType);
            lnkBtnSaveLeaveType.Visible = false;
            lnkBtnClearLeaveType.Visible = false;
            lnkBtnUpdateLeaveType.Visible = false;
            lbAddLeaveTypeHeader.Text = "View Leave Type Details";
            ImageAddLeaveTypeHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddLeaveType.Show();
        }
        //load delete application user details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLeaveTypes))//check if there is any grid view that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a leave type to delete !", this, PanelLeaveTypes);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected leave type?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected leave types?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaveTypes);
                return;
            }
        }

        //delete selected leave types
        protected void DeleteLeaveType()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvLeaveTypes.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;

                    if (_cb.Checked)
                    {
                        HiddenField _HFLeaveTypeID = (HiddenField)_grv.FindControl("HFID");
                        int LeaveTypeID = Convert.ToInt32(_HFLeaveTypeID.Value);
                        //if (db.Leaves.Any(p => p.leaves_siLeaveTypID == LeaveTypeID))
                        //{
                        //    _hrClass.LoadHRManagerMessageBox(1, "Delete Failed!. Leave type selected has some employees already applied to it.",this,PanelLeaveTypes);
                        //    return;
                        //}

                        LeaveType deleteLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == LeaveTypeID);
                        db.LeaveTypes.DeleteOnSubmit(deleteLeaveType);
                        db.SubmitChanges();
                    }
                }
                //refresh leave types after delete is done
                DisplayLeaveTypes();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected leave type has been deleted.", this, PanelLeaveTypes);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected leave types have been deleted.", this, PanelLeaveTypes);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed . " + ex.Message.ToString(), this, PanelLeaveTypes);
                return;
            }
        }
        //delete the selected application records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteLeaveType();
            return;
        }
    }
}