﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Configuration;
using System.Data;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Leave_Applications : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerMailWebService _hrMailWebService = new HRManagerMailWebService();
        HRManagerMailClass _hrMail = new HRManagerMailClass();
        static string companyInitials = ConfigurationManager.AppSettings.Get("CompanyInitials").ToString();//get comapany's intials
        Guid _StaffMasterID = Guid.Empty;//get the logged in staff id
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _StaffMasterID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
            }
            catch { }
            if (!IsPostBack)
            {
                _hrClass.GetStaffNames(ddlStaffName, "Staff Name");//populate staff name dropdown
                _hrClass.GetLeaveTypes(ddlLeaveType, "Leave Type");//populate leave type dropdown
                _hrClass.GetLeaveApplicationYears(ddlSearchLeaveApplicationPerYear);//get leave application years
                _hrClass.GetStaffNames(ddlApprovedOrCanceledByManager, "Reports To");//populate reports to dropdown
                GetLeavesApplicationsMadePerYear(DateTime.Now.Year);//get leave applications made and approved
                GetLeavesDueForApproval();//load leave applications awaiting approval
                GetRejectedLeavesHistory();//load all rejected leave applications

                txtLeaveFromDate.Attributes.Add("readonly", "readonly");//make leave from date texbox readony 
                txtLeaveToDate.Attributes.Add("readonly", "readonly");//
                txtLeaveSerialNumber.Attributes.Add("readonly", "readonly");
                txtLeaveDays.Attributes.Add("readonly", "readonly");
            }
            //add confirm cancel leave application event
            ucConfirmEraseLeaveApplication.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnConfirmEraseLeaveApplication_Click);
            //add confirm leave approval or dissapproval
            ucConfirmLeaveApprovalOrDisapproval.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnConfirmLEaveApprovalOrDisapproval_Click);
        }
        //clear leave application details
        private void ClearLeaveApplicationControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelStaffLeaveApplication);
            HiddenFieldLeaveID.Value = "";
            lnkBtnSaveLeave.Visible = true;
            lnkBtnUpdateLeave.Enabled = false;
            lbleaveStatusInfo.Text = "";
            return;
        }
        //
        //validate leave application controls
        private string ValidateLeaveApplicationControls()
        {
            if (ddlStaffName.SelectedIndex == 0)
            {
                return "Select staff name!";
            }
            else if (ddlLeaveType.SelectedIndex == 0)
            {
                return "Select Leave type!";
            }
            else if (txtLeaveFromDate.Text.Trim() == "")
            {
                ImageButtonLeaveFromDate.Focus();
                return "Select from which date is the leave supposed to start on!";
            }
            //else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Date < DateTime.Now.Date)//check if from date is less than current date
            //{
            //    _errror = "Leave application from date cannot be before current date!";
            //    btnLeaveFromDate.Focus();
            //    return _errror;
            //}
            else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Year > DateTime.Now.Year)
            {
                ImageButtonLeaveFromDate.Focus();
                return "Date from year selected is not the current year. Leave can only be applied from the current year!";
            }
            else if (rblLeaveFromSession.SelectedIndex == -1)
            {
                rblLeaveFromSession.Focus();
                return "Please select the session when the leave is supposed to start from, i.e either in the morning or afternoon!";
            }
            else if (txtLeaveToDate.Text.Trim() == "")
            {
                ImageButtonLeaveToDate.Focus();
                return "Select date when the leave bieng applied for is expected to end!";
            }
            else if (Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) > Convert.ToDateTime(txtLeaveToDate.Text.Trim()))
            {
                ImageButtonLeaveFromDate.Focus();
                return "'Leave From Date' cannot be after 'Leave To Date'!";
            }
            else if (rblLeaveToSession.SelectedIndex == -1)
            {
                rblLeaveToSession.Focus();
                return "Please select the session when the leave being applied for is expected to end, I.e either in the morning or afternoon!";
            }
            return "";
        }
        //method for saving leave application
        private void SaveLeaveApplication()
        {
            try
            {
                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                    short _leaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);
                    if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == _leaveTypeID && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Leave application failed ! Leave application details entered already exits!", this, PanelLeaves);
                        ddlLeaveType.Focus();
                        return;
                    }
                    else
                    {
                        //get employee posting location
                        int employeePostingLocationID = _hrClass.GetStaffPostingLocationID(_staffID);
                        //get employee being applied for the leave
                        string employeeName = ddlStaffName.SelectedItem.ToString();
                        //count leave days
                        double leaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));//get leave days

                        DateTime effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), employeePostingLocationID);//get effective date when the leave begins
                        DateTime effectiveTilldate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), employeePostingLocationID);//get effective date when the leave ends
                        //get leave serial number
                        string leaveSerialNumber = GenerateLeaveSerialNumber();
                        //create new leave application

                        HRManagerClasses.Leave newLeave = new Leave();
                        newLeave.leave_uiStaffID = _staffID;
                        newLeave.leave_siLeaveTypeID = _leaveTypeID;
                        newLeave.leave_cTrType = 'U';//U for used
                        newLeave.leave_dtFrom = Convert.ToDateTime(txtLeaveFromDate.Text.Trim());
                        newLeave.leave_dtEffFrom = effectiveFromDate;
                        newLeave.leave_cEffFromSess = Convert.ToChar(rblLeaveFromSession.SelectedValue);
                        newLeave.leave_dtTill = Convert.ToDateTime(txtLeaveToDate.Text.Trim());
                        newLeave.leave_dtEffTill = effectiveTilldate;
                        newLeave.leave_cEffTillSess = Convert.ToChar(rblLeaveToSession.SelectedValue);
                        newLeave.leave_dtDateCreated = DateTime.Now;
                        newLeave.leave_uiAuthByStaffID = _StaffMasterID;//the authorizer is the currently logged in person
                        newLeave.leave_dtDateApproved = DateTime.Now;
                        newLeave.leave_fNoDaysAlloc = leaveDays;
                        newLeave.leave_bLeaveStatus = true;//leave status should be true
                        newLeave.leave_vRemarks = txtRemarks.Text.Trim();
                        newLeave.leave_bIsCancelled = false;
                        newLeave.leave_vSerialNumber = leaveSerialNumber;
                        newLeave.leave_bIsRemoved = false;
                        db.Leaves.InsertOnSubmit(newLeave);
                        db.SubmitChanges();
                        HiddenFieldLeaveID.Value = newLeave.leave_iLeaveID.ToString();
                        _hrClass.LoadHRManagerMessageBox(2, "Leave application details for " + employeeName + " have been saved.", this, PanelLeaves);

                        //calculate leave balance afeter the application is done
                        //check if the leave balance for the leave type is to be calculated
                        if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == _leaveTypeID).leavetype_bCalculateLeaveBalance == true)
                        {///show the leave balance for the leave selected
                            PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, _leaveTypeID).Single();
                            string leaveTypeBalance = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed)).ToString();
                            lbleaveStatusInfo.Text += ", Balance after leave application is : " + leaveTypeBalance;
                        }
                        else lbleaveStatusInfo.Text = "";
                        //display the leave serial number
                        txtLeaveSerialNumber.Text = leaveSerialNumber;
                        ////display leave applications made made on the selected date from
                        //GetLeavesApplicationsMadePerYear(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Year);

                        //create a maill sending transaction 
                        //case 8 for sending a leave application made by Hr on behalf of an employemail
                        _hrMailWebService.CreateMailSendingTransaction(8, newLeave.leave_iLeaveID, _StaffMasterID, null);

                        lnkBtnUpdateLeave.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //method for updating leave application made details
        private void UpdateLeaveApplication(int LeaveID)
        {
            try
            {
                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                    short _leaveTypeID = Convert.ToInt16(ddlLeaveType.SelectedValue);
                    if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_cTrType == 'U' && p.leave_siLeaveTypeID == _leaveTypeID && p.leave_dtFrom == Convert.ToDateTime(txtLeaveFromDate.Text.Trim()) && p.leave_dtTill == Convert.ToDateTime(txtLeaveToDate.Text.Trim()) && p.leave_iLeaveID != LeaveID && p.leave_bIsCancelled == false && p.leave_bIsRemoved == false))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Update failed. Leave details entered already exits!", this, PanelLeaves);
                        ddlLeaveType.Focus();
                        return;
                    }
                    else
                    {
                        //get employee posting locationid
                        int employeePostingLocationID = _hrClass.GetStaffPostingLocationID(_staffID);
                        //check if the employee had earlier applied for the leave type selected
                        string currentYear = DateTime.Now.Year.ToString();
                        int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);
                        //check if leaveType is reaplicable
                        LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeID);
                        bool isLeaveReaplicable = (bool)getLeaveType.leavetype_bIsReapplicablePerYear;
                        //check if the leave had been eralier applied for and consumed
                        if (db.Leaves.Any(p => p.leave_uiStaffID == _staffID && p.leave_siLeaveTypeID == leaveTypeID && p.leave_cTrType == 'U' && p.leave_bLeaveStatus == true && p.leave_iLeaveID != LeaveID) && isLeaveReaplicable == false)
                        {
                            //get the last leave id for theleave application thet was made for the leave
                            int lastLeaveID = Convert.ToInt32(db.Leaves.Where(p => p.leave_bLeaveStatus == true && p.leave_cTrType == 'U' && p.leave_uiStaffID == _staffID).Max(p => p.leave_iLeaveID));
                            //get the date when the leave was applied
                            DateTime getLastLeaveDate = Convert.ToDateTime(db.Leaves.Single(p => p.leave_iLeaveID == lastLeaveID).leave_dtEffFrom);
                            //get the year applied for
                            string yearApplied = getLastLeaveDate.Year.ToString();
                            if (currentYear == yearApplied)
                            {
                                _hrClass.LoadHRManagerMessageBox(1, "Leave application cannot be processed because the type of leave selected cannot be re-applicable in the same year because the selected employee has already consumed the leave!", this, PanelLeaves);
                                return;
                            }
                        }
                        double leaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));//get leave days

                        DateTime effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), employeePostingLocationID);//get effective date when the leave begins
                        DateTime effectiveTilldate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), employeePostingLocationID);//get effective date when the leave ends

                        //update the leave detail
                        Leave updateLeave = db.Leaves.Single(p => p.leave_iLeaveID == LeaveID);
                        updateLeave.leave_uiStaffID = _staffID;
                        updateLeave.leave_siLeaveTypeID = _leaveTypeID;
                        //updateLeave.leave_cTrType=
                        updateLeave.leave_dtFrom = Convert.ToDateTime(txtLeaveFromDate.Text.Trim());
                        updateLeave.leave_dtEffFrom = effectiveFromDate;
                        updateLeave.leave_cEffFromSess = Convert.ToChar(rblLeaveFromSession.SelectedValue);
                        updateLeave.leave_dtTill = Convert.ToDateTime(txtLeaveToDate.Text.Trim());
                        updateLeave.leave_dtEffTill = effectiveTilldate;
                        updateLeave.leave_cEffTillSess = Convert.ToChar(rblLeaveToSession.SelectedValue);

                        updateLeave.leave_uiAuthByStaffID = _StaffMasterID;//authorized by the logged in staff
                        updateLeave.leave_fNoDaysAlloc = leaveDays;
                        updateLeave.leave_vRemarks = txtRemarks.Text.Trim();
                        db.SubmitChanges();
                        _hrClass.LoadHRManagerMessageBox(2, "Leave application details have been updated.", this, PanelLeaves);


                        //check if the leave type balance is to be calculated
                        if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeID).leavetype_bCalculateLeaveBalance == true)
                        {
                            //calculate leave balance afeter the application is done
                            PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, leaveTypeID).Single();
                            string leaveTypeBalance = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed)).ToString();
                            lbleaveStatusInfo.Text += ", balance after leave application is : " + leaveTypeBalance;
                        }
                        else lbleaveStatusInfo.Text = "";
                        //display leave applications made on the date from selected year
                        GetLeavesApplicationsMadePerYear(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()).Year);
                        ////refresh leave applications due for approval
                        //GetLeavesDueForApproval();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //submit leave application
        protected void lnkBtnSaveLeave_Click(object sender, EventArgs e)
        {
            SaveLeaveApplication(); return;
        }
        //update leave application
        protected void lnkBtnUpdateLeave_Click(object sender, EventArgs e)
        {
            UpdateLeaveApplication(Convert.ToInt32(HiddenFieldLeaveID.Value)); return;
        }
        //clear leave application controls
        protected void lnkBtnClear_Click(object sender, EventArgs e)
        {
            ClearLeaveApplicationControls(); return;
        }

        //count leave days by substarticng the holidays defined
        private double CountLeaveDays(DateTime fromDate, DateTime toDate)
        {
            //get employee posting location
            Guid _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
            int employeePostingLocationID = _hrClass.GetStaffPostingLocationID(_staffID);

            //get employee department name for leave application being made for
            double leaveDays = 0;

            //get leave type id
            int leaveTypeID = Convert.ToInt32(ddlLeaveType.SelectedValue);
            //get the leave type selected
            LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeID);
            string leaveTypeName = getLeaveType.leavetype_vName.Trim();
            //get  leave type days(working or calendar)
            string _leaveDaysType = getLeaveType.leavetype_vDaysType.Trim();
            //check if leave type is continous or not
            if ((bool)getLeaveType.leavetype_bIsContinuous == true)// if it is continous, get total leave days for the leave type
            {

                leaveDays = (double)getLeaveType.leavetype_fMaxDays;
                //leaveDays=leaveDays;// 1;reduce leave days by one
                toDate = fromDate.AddDays(leaveDays - 1);//add the leave days to the fromDat to get the final leave date ingle date to get the end date
                txtLeaveToDate.Text = _hrClass.ShortDateDayStart(toDate.ToString());
                DateTime effectiveToDate = GetNextWorkingDate(toDate, employeePostingLocationID);//get effective till working date
                txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(effectiveToDate.ToString()));
                txtLeaveDays.Text = leaveDays.ToString();
            }
            else if ((bool)getLeaveType.leavetype_bIsContinuous == false)//else claculate the leavedays by checking if they are claendar or working days
            {
                //get all calendar day if leave type days are based on calendar days
                if (_leaveDaysType == "Calendar")
                {
                    TimeSpan _timeSpan = new TimeSpan();//for calculating date difference
                    _timeSpan = toDate.Subtract(fromDate);
                    leaveDays = _timeSpan.Days + 1;//plus add one day
                }
                //get workings days if leave type days calculation is based on working days
                else if (_leaveDaysType == "Working")
                {
                    //get total leave days by calling the proc_CountLeaveDays procedure result) to get the total leave days
                    PROC_CountLeaveDaysResult getLeaveDays = db.PROC_CountLeaveDays(employeePostingLocationID, fromDate, toDate).Single();
                    leaveDays = Convert.ToDouble(getLeaveDays.no_of_leaves_days);
                    DateTime dateIterator = fromDate;//start from the from date and loop till to date plus one day
                    while (dateIterator < toDate.AddDays(1))
                    {
                        //check if day of week is on a sunday, then reduce the total days by 1
                        if (dateIterator.DayOfWeek == DayOfWeek.Sunday)

                            leaveDays--;//reduce the leave days by one

                        dateIterator = dateIterator.AddDays(1);//add a day to the iterator after checking
                    }
                }
                //check if from date is the same with till date
                if (fromDate == toDate)
                {
                    //check if the dates are on a saturday or sunday or any other holiday
                    if (db.Holidays.Any(p => p.holiday_dtDate == fromDate))
                    {
                        //check if from session selected is morning  and session till is afternoon
                        if (rblLeaveFromSession.SelectedIndex == 0 && rblLeaveToSession.SelectedIndex == 1)
                        {
                            return leaveDays;//return the leave days
                        }
                        else
                        {
                            return 0.5;//return half day
                        }
                    }
                    //check if from date is exactly the same with to date and if sessions are the same, or the from session begins in the afternoon, then leave days is 0.5 days
                    else if (rblLeaveFromSession.SelectedIndex == rblLeaveToSession.SelectedIndex || rblLeaveFromSession.SelectedIndex == 1)
                    {
                        return leaveDays = 0.5;
                    }
                    //check if dates are the same but sessions are different, then its a full day
                    else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex)
                    {
                        return 1;
                    }
                }
                //check if number of leave days =1,and if enddate is saturday or sunday
                else if (leaveDays == 1)
                {
                    //check if fron session selected is morning or afternoon
                    if (rblLeaveFromSession.SelectedIndex == 0)
                    {
                        return leaveDays;
                    }
                    else if (rblLeaveFromSession.SelectedIndex == 1)
                    {
                        return 0.5;
                    }
                }

                else if (leaveDays >= 1)
                {
                    //   check if theleave days is greater than one and end date falls n working saturday(or  sunday- one day)

                    if ((toDate.DayOfWeek == DayOfWeek.Saturday || toDate.AddDays(-1).DayOfWeek == DayOfWeek.Saturday) && (db.Holidays.Any(p => p.holiday_dtDate == toDate || p.holiday_dtDate == toDate.AddDays(-1))))
                    {
                        if (rblLeaveFromSession.SelectedIndex == 0)//return the leave days as they are if the session selected is in the morning
                        {
                            return leaveDays;
                        }
                        else if (rblLeaveFromSession.SelectedIndex == 1)//reduce by 0.5 if the session is in the afternoon
                        {
                            return leaveDays - 0.5;
                        }
                    }
                    //check if leave days are greater than one and from date falls on a working saturday

                    else if (fromDate.DayOfWeek == DayOfWeek.Saturday)
                    {
                        if (rblLeaveToSession.SelectedIndex == 0)//if till session is in the morning reduce by 0.5 days
                        {
                            return leaveDays - 0.5;
                        }
                        else if (rblLeaveToSession.SelectedIndex == 1)//if till session is in the evening, return leave days as they are
                        {
                            return leaveDays;
                        }
                    }
                    //checking same sessions selected is morning and either from date session is on a morning or afternoon, then minus a half day from the total days counted
                    else if (rblLeaveFromSession.SelectedIndex == rblLeaveToSession.SelectedIndex && (rblLeaveFromSession.SelectedIndex == 0 || rblLeaveFromSession.SelectedIndex == 1))
                    {

                        return leaveDays - 0.5;
                    }
                    //check if different sesions are selected,and from leave session is in the morning and leave endsession is on an afternoon, then leave days are the total days counted 
                    else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex && rblLeaveFromSession.SelectedIndex == 0 && rblLeaveToSession.SelectedIndex == 1)
                    {
                        return leaveDays;
                    }
                    //check if different sesions are selected,and from leave session is on an afternoon and endsession is in the morning, the substract a day from the total counted days 
                    else if (rblLeaveFromSession.SelectedIndex != rblLeaveToSession.SelectedIndex && rblLeaveFromSession.SelectedIndex == 1 && rblLeaveToSession.SelectedIndex == 0)
                    {
                        return leaveDays - 1;
                    }
                }

            }
            return leaveDays;
        }
        //get next woking day if the leave from applied date or till applied date fall on a holiday or weekend
        //depending on the selected employee posting location
        private DateTime GetNextWorkingDate(DateTime _date, int _employeePostingLocationID)
        {
            DateTime workingDay = _date;
            if (_date.DayOfWeek == DayOfWeek.Sunday)//check if the date selected is on a sunday
            {
                workingDay = workingDay.AddDays(1);//add a day to sunday
                return workingDay;
            }
            //check if date selected is a saturday
            else if (_date.DayOfWeek == DayOfWeek.Saturday && db.Holidays.Any(p => p.holiday_dtDate == _date && p.holiday_siApplicableToBranchID == _employeePostingLocationID))
            {
                //get type of leave day (either 1 or 0.5)
                double _leaveDay = Convert.ToDouble(db.Holidays.Single(p => p.holiday_dtDate == _date && p.holiday_siApplicableToBranchID == _employeePostingLocationID).holiday_fDuration);
                if (_leaveDay == 1)
                {
                    workingDay = workingDay.AddDays(2);//add two days non-working saturday
                    return workingDay;
                }
                else if (_leaveDay == 0.5)//return the saturday selected is a working saturday
                {
                    return workingDay;
                }
            }
            //check if the date selected is holiday and does not fall on a sunday or saturday
            else if ((_date.DayOfWeek != DayOfWeek.Saturday || _date.DayOfWeek != DayOfWeek.Sunday) && db.Holidays.Any(p => p.holiday_dtDate == _date && (p.holiday_siApplicableToBranchID == 1 || p.holiday_siApplicableToBranchID == _employeePostingLocationID)))
            {
                workingDay = workingDay.AddDays(1);//add a day to the day selected
                //after adding a day, check if the day added fall on a wekend
                // GetNextWorkingDate(workingDay);//recurse or repeat the process inorder to get the corect working day
                if (workingDay.DayOfWeek == DayOfWeek.Sunday)//check if added day is on a sunday
                {
                    workingDay = workingDay.AddDays(1);//add a day to sunday
                    return workingDay;
                }
                //check if date added is a saturday
                else if (workingDay.DayOfWeek == DayOfWeek.Saturday && db.Holidays.Any(p => p.holiday_dtDate == workingDay && p.holiday_siApplicableToBranchID == _employeePostingLocationID))
                {
                    //get type of leave day (either 1 or 0.5)
                    double _leaveDay = Convert.ToDouble(db.Holidays.Single(p => p.holiday_dtDate == workingDay && p.holiday_siApplicableToBranchID == _employeePostingLocationID).holiday_fDuration);
                    if (_leaveDay == 1)
                    {
                        workingDay = workingDay.AddDays(2);//add two days non-working saturday
                        return workingDay;
                    }
                    else if (_leaveDay == 0.5)//return the saturday selected is a working saturday
                    {
                        return workingDay;
                    }
                }
                return workingDay;
            }
            return workingDay;
        }

        //generate leave serial number
        private string GenerateLeaveSerialNumber()
        {
            string leaveSerialNumber = "", firstPart = "LV", secondPart = "", lastLeaveSerialNumber = companyInitials + "-" + firstPart + "-00001";
            int lastUsedLeaveID = 0, _2ndPart = 0;//initialize with zero
            try
            {
                //get the last id for leaves where is is of used type (U)
                lastUsedLeaveID = Convert.ToInt32(db.Leaves.Where(p => p.leave_cTrType == 'U' && p.leave_vSerialNumber != null).Max(p => p.leave_iLeaveID));
            }
            catch { }
            if (lastUsedLeaveID == 0)//check if there is any used leave id record
            {
                leaveSerialNumber = lastLeaveSerialNumber;
            }
            else
            {
                lastLeaveSerialNumber = db.Leaves.Single(p => p.leave_iLeaveID == lastUsedLeaveID).leave_vSerialNumber;
                //lastLeaveSerialNumber = lastLeaveSerialNumber.Substring(3);//remove the first 3 letters
                lastLeaveSerialNumber = lastLeaveSerialNumber.Replace(companyInitials, "").Replace(firstPart, "").Replace("-", "");//remove company initials, firts part and (-) to get the numeric part of the number
                _2ndPart = Convert.ToInt32(lastLeaveSerialNumber) + 1;//by incrementing with 1 
                secondPart = _2ndPart.ToString();
                //generate leave serial number by checking the length of the second part
                leaveSerialNumber = companyInitials + '-' + firstPart + "-";//get the start defination of the leave number
                if (secondPart.Length == 1)
                {
                    leaveSerialNumber += "0000" + secondPart;
                }
                else if (secondPart.Length == 2)
                {
                    leaveSerialNumber += "000" + secondPart;
                }
                else if (secondPart.Length == 3)
                {
                    leaveSerialNumber += "00" + secondPart;
                }
                else if (secondPart.Length == 4)
                {
                    leaveSerialNumber += "0" + secondPart;
                }
                else//length exceeds 4
                {
                    leaveSerialNumber += secondPart;
                }
            }
            return leaveSerialNumber;
        }


        //method for calculating leave sttus for the leave being  applied for against thje selected employee
        private void CalculateLeaveStatusForLeaveBeingApplied(Guid _staffID, int _leaveTypeID)
        {
            //check if the leave type balnce in to be  calculated
            if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == _leaveTypeID).leavetype_bCalculateLeaveBalance == true)
            {//calculate leave balance for the leave selected
                PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, _leaveTypeID).FirstOrDefault();
                string leaveTypeBalance = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed)).ToString();
                lbleaveStatusInfo.Text = ddlLeaveType.SelectedItem.ToString() + " balance before leave application is " + leaveTypeBalance;
                return;
            }
            else lbleaveStatusInfo.Text = "";
        }
        //get the leave balance when a staff is selected
        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlStaffName.SelectedIndex != 0 && ddlLeaveType.SelectedIndex != 0)
                {
                    CalculateLeaveStatusForLeaveBeingApplied(_hrClass.ReturnGuid(ddlStaffName.SelectedValue), Convert.ToInt32(ddlLeaveType.SelectedValue));
                }
                string _error = ValidateLeaveApplicationControls();//check for errors
                if (_error == "")//check if all the fields are entered
                {
                    //count leave days
                    double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                    txtLeaveDays.Text = countLeaveDays.ToString();

                }
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error ocuured. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }

        // display effective from date when leave from date is selected
        protected void txtLeaveFromDate_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    if (ddlLeaveType.SelectedIndex == 0)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Select leave type!", this, PanelLeaves);
                        txtLeaveFromDate.Text = "";
                        return;
                    }
                    else
                    {
                        //get leave applicant posting location
                        int leaveApplicantPostingLocationID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);
                        int leaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                        //get leave type
                        LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeId);
                        //check if leave is continous
                        if ((bool)getLeaveType.leavetype_bIsContinuous == true)
                        {
                            int leaveDays = (int)getLeaveType.leavetype_fMaxDays;//get maximamun leave days for the leave type
                            //get from when leave is due to start
                            DateTime fromDate = Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), toDate, effectiveToDate;
                            //get till date 
                            toDate = fromDate.AddDays(leaveDays - 1);
                            txtLeaveToDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(toDate.ToString()));
                            effectiveToDate = GetNextWorkingDate(toDate, leaveApplicantPostingLocationID);//get effective till working date
                            txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(effectiveToDate.ToString()));
                            txtLeaveDays.Text = leaveDays.ToString();
                            ImageButtonLeaveToDate.Enabled = false;
                            rblLeaveFromSession.SelectedIndex = 0;
                            rblLeaveToSession.SelectedIndex = 1;
                            rblLeaveFromSession.Enabled = false;
                            rblLeaveToSession.Enabled = false;
                        }
                        else
                        {
                            ImageButtonLeaveToDate.Enabled = true;
                            rblLeaveFromSession.Enabled = true;
                            rblLeaveToSession.Enabled = true;
                        }
                        DateTime _effectiveFromDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveFromDate.Text), leaveApplicantPostingLocationID);
                        txtEffectiveFromDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(_effectiveFromDate.ToString()));
                        //check if all the fields are entered, and calaculate leave days
                        string _error = ValidateLeaveApplicationControls();//check for errors
                        if (_error == "")//check if all the fields are entered
                        {
                            //count leave days
                            double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                            txtLeaveDays.Text = countLeaveDays.ToString();
                            return;
                        }
                    }
                    return;
                }
                catch (Exception ex)
                {
                    _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                    return;
                }
            }
        }

        // display effective to date when leave till date is selected
        protected void txtLeaveToDate_TextChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    if (txtLeaveFromDate.Text.Trim() == "")
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Select the date when the leave you are are applying for is expected to begin by clickng on the calendar button adjacent to field!", this, PanelLeaves);
                        ImageButtonLeaveFromDate.Focus();
                        txtLeaveFromDate.Text = "";
                        return;
                    }
                    else
                    {
                        //get leave applicant posting location id
                        int leaveApplicantPostingLocationID = Convert.ToInt16(HttpContext.Current.Session["LoggedStaffPostedAtID"]);
                        //get the next working date
                        DateTime _effectiveTillDate = GetNextWorkingDate(Convert.ToDateTime(txtLeaveToDate.Text.Trim()), leaveApplicantPostingLocationID);
                        txtEffectiveTillDate.Text = Server.HtmlEncode(_hrClass.ShortDateDayStart(_effectiveTillDate.ToString()));
                        //check if all the fields are entered, and calaculate leave days
                        string _error = ValidateLeaveApplicationControls();//check for errors
                        if (_error == "")//check if all the fields are entered
                        {  //count lrave days
                            double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                            txtLeaveDays.Text = countLeaveDays.ToString();
                            return;
                        }
                        return;
                    }
                }
                catch (Exception ex)
                {
                    _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
                    return;
                }
            }
        }
        //count the leave days when the index is changed
        protected void rblLeaveToSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _error = ValidateLeaveApplicationControls();//check for errors
            if (_error != "")
            {
                _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                rblLeaveToSession.SelectedIndex = -1;
                return;
            }
            else
            {
                //count leave days
                double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                txtLeaveDays.Text = countLeaveDays.ToString();
                return;
            }

        }
        //count leave days when from radio button is selected
        protected void rblLeaveFromSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _error = ValidateLeaveApplicationControls();//check for errors
            if (_error == "")//check if all the fields are entered
            {
                //count lrave days
                double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                txtLeaveDays.Text = countLeaveDays.ToString();
                return;
            }
        }
        //count leave days when from leave tyopeis selected
        protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (ddlLeaveType.SelectedIndex == 0)
                {
                    //return;
                }
                else
                {
                    int leaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    //get leave type
                    LeaveType getLeaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == leaveTypeId);


                    //check if leave is continous
                    if ((bool)getLeaveType.leavetype_bIsContinuous == true)
                    {
                        ImageButtonLeaveToDate.Enabled = false;
                        rblLeaveFromSession.Enabled = false;
                        rblLeaveToSession.Enabled = false;
                    }
                    else
                    {
                        ImageButtonLeaveToDate.Enabled = true;
                        rblLeaveFromSession.Enabled = true;
                        rblLeaveToSession.Enabled = true;
                    }
                    string _error = ValidateLeaveApplicationControls();//check for errors
                    if (_error == "")//check if all the fields are entered
                    {
                        //count leave days
                        double countLeaveDays = CountLeaveDays(Convert.ToDateTime(txtLeaveFromDate.Text.Trim()), Convert.ToDateTime(txtLeaveToDate.Text.Trim()));
                        txtLeaveDays.Text = countLeaveDays.ToString();
                        //  return;
                    }
                }
                //calculate the leave balance for the leave being applied for
                if (ddlStaffName.SelectedIndex != 0 && ddlLeaveType.SelectedIndex != 0)
                {
                    CalculateLeaveStatusForLeaveBeingApplied(_hrClass.ReturnGuid(ddlStaffName.SelectedValue), Convert.ToInt32(ddlLeaveType.SelectedValue));
                }

            }
            catch { }
        }
        //get all leave applications made
        private void GetAllLeavesApplicationsMade()
        {
            try
            {
                object _display;
                _display = db.PROC_AllLeavesApprovedForAllStaff();
                gvLeavesApplicationsMade.DataSourceID = null;
                Session["gvLeavesApplicationsMadeData"] = _display;
                gvLeavesApplicationsMade.DataSource = _display;
                gvLeavesApplicationsMade.DataBind();
                lbLeaveApplicationsMade.Text = "All Leave Applications Made";
                return;
            }
            catch { }
        }
        //get leave applications made depending on the year selected
        private void GetLeavesApplicationsMadePerYear(int _selectedYear)
        {
            try
            {
                object _display;
                _display = db.PROC_LeavesApprovedForAllStaffPerYear(_selectedYear);
                gvLeavesApplicationsMade.DataSourceID = null;
                Session["gvLeavesApplicationsMadeData"] = _display;
                gvLeavesApplicationsMade.DataSource = _display;
                gvLeavesApplicationsMade.DataBind();
                lbLeaveApplicationsMade.Text = "Leave Applications Made in " + _selectedYear.ToString();
                return;
            }
            catch { }
        }
        //page indexing for leave applications made grid view
        protected void gvLeavesApplicationsMade_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvLeavesApplicationsMade.PageIndex = e.NewPageIndex;
                Session["gvLeavesApplicationsMadePageIndex"] = e.NewPageIndex;
                gvLeavesApplicationsMade.DataSource = (object)Session["gvLeavesApplicationsMadeData"];
                gvLeavesApplicationsMade.DataBind();
                return;
            }
            catch (Exception)
            {
                //Session["gvBusinessCardsPageIndex"] = null;//remove th page index id
                //return;
            }
        }
        protected void gvLeavesApplicationsMade_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvLeavesApplicationsMadePageIndex"] == null)//check if page index is empty
                {
                    gvLeavesApplicationsMade.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvLeavesApplicationsMadesPageIndex"]);
                    gvLeavesApplicationsMade.PageIndex = pageIndex;
                    return;
                }
            }
            catch
            {// Session[" gvBusinessCardsPageIndex"] = null;
            }
        }
        //method for searching for leave applicationd made and approved by use of reference number, name, id number or phone number
        private void SearchForApprovedLeaveApplications()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                //check if we are seraching against all leave application or leave applications within a certain year
                if (ddlSearchLeaveApplicationPerYear.SelectedIndex == 0)
                {
                    //search agaist all approved leave applications
                    _searchDetails = db.PROC_AllLeavesApprovedForAllStaff().Where(p => (p.StaffRef.ToLower() + p.StaffName.ToLower() + p.IDNumber.ToLower() + p.PhoneNumber.ToLower()).Contains(_searchText)).OrderBy(p => p.StaffName).ToList();
                }
                else//search for a certain year
                {
                    //get the  year
                    int _leaveYear = Convert.ToInt32(ddlSearchLeaveApplicationPerYear.SelectedValue);
                    _searchDetails = db.PROC_LeavesApprovedForAllStaffPerYear(_leaveYear).Where(p => (p.StaffRef.ToLower() + p.StaffName.ToLower() + p.IDNumber.ToLower() + p.PhoneNumber.ToLower()).Contains(_searchText)).OrderBy(p => p.StaffName).ToList();
                }
                gvLeavesApplicationsMade.DataSourceID = null;
                gvLeavesApplicationsMade.DataSource = _searchDetails;
                Session["gvLeavesApplicationsMadeData"] = _searchDetails;
                gvLeavesApplicationsMade.DataBind();
                gvLeavesApplicationsMade.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaves);
                return;
            }
        }
        //search for rejected leave by using the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForApprovedLeaveApplications();
            return;
        }
        //method for performing a task depending on a selected value in the header dropdownlist in gvLeavesApplicationsMade gridview
        protected void ddlGvLeavesApplicationsMadeHeaderDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlHeader = (DropDownList)gvLeavesApplicationsMade.HeaderRow.FindControl("ddlHeader");
                if (ddlHeader.SelectedIndex == 0) { }
                else
                {
                    int _selectedIndex = ddlHeader.SelectedIndex;
                    ddlHeader.SelectedIndex = 0;//select the first index after geting the selected index
                    if (!_hrClass.isGridviewItemSelected(gvLeavesApplicationsMade))//check if any record is selected
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "There is no leave application selected.<br>Select a leave application that you want to edit or erase !", this, PanelLeaves);
                        return;
                    }
                    else
                    {
                        int x = 0;
                        foreach (GridViewRow _grv in gvLeavesApplicationsMade.Rows)
                        {
                            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                            if (_cb.Checked) ++x;
                        }
                        if (x > 1)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one leave application. <br>Select only one leave application to edit or erase at a time!", this, PanelLeaves);
                            return;
                        }
                        foreach (GridViewRow _grv in gvLeavesApplicationsMade.Rows)
                        {
                            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                            if (_cb.Checked) ++x;
                            if (_cb.Checked)
                            {
                                HiddenField _HFLeaveID = (HiddenField)_grv.FindControl("HFID");
                                int _leaveID = Convert.ToInt32(_HFLeaveID.Value);

                                if (_selectedIndex == 1)//editing
                                {
                                    LoadLeaveDetailsForEdit(_leaveID);
                                    return;
                                }
                                else if (_selectedIndex == 2)//erasing
                                {
                                    Session["EraseLeaveID"] = _HFLeaveID.Value;
                                    LoadLeaveApplicationDetailsToBeErased(_leaveID);
                                    return;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured ! " + ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //method for loading leave application selected for edit
        private void LoadLeaveDetailsForEdit(int LeaveID)
        {
            Leave getLeave = db.Leaves.Single(p => p.leave_iLeaveID == LeaveID);
            HiddenFieldLeaveID.Value = LeaveID.ToString();
            ddlStaffName.SelectedValue = getLeave.leave_uiStaffID.ToString();
            ddlLeaveType.SelectedValue = getLeave.leave_siLeaveTypeID.ToString();
            txtLeaveFromDate.Text = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString());
            txtEffectiveFromDate.Text = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString());
            txtLeaveToDate.Text = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString());
            txtEffectiveTillDate.Text = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString());

            if (getLeave.leave_cEffFromSess.ToString() == "M")//check if itz morning session
            {
                rblLeaveFromSession.SelectedIndex = 0;
            }
            else rblLeaveFromSession.SelectedIndex = 1;
            if (getLeave.leave_cEffTillSess.ToString() == "M")
            {
                rblLeaveToSession.SelectedIndex = 0;
            }
            else rblLeaveToSession.SelectedIndex = 1;
            txtLeaveDays.Text = getLeave.leave_fNoDaysAlloc.ToString();
            txtRemarks.Text = getLeave.leave_vRemarks;
            txtLeaveSerialNumber.Text = getLeave.leave_vSerialNumber;

            TabContainerLeaves.ActiveTabIndex = 0;
            lnkBtnSaveLeave.Visible = false;
            lnkBtnUpdateLeave.Visible = true;
            lnkBtnUpdateLeave.Enabled = true;
            lbleaveStatusInfo.Text = "";
        }
        //get leave application made depending on the year selected in the serach drop down
        protected void ddlSearchLeaveApplicationPerYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSearchLeaveApplicationPerYear.SelectedValue == "ALL")//check if ALL is selected
            {
                GetAllLeavesApplicationsMade();//get all leave application made
                return;
            }
            else
            {
                //display leave application for the selected year
                int _selectedYear = Convert.ToInt32(ddlSearchLeaveApplicationPerYear.SelectedValue);
                GetLeavesApplicationsMadePerYear(_selectedYear);
                return;
            }
        }
        //loads leave application details to be erased
        private void LoadLeaveApplicationDetailsToBeErased(int _leaveID)
        {
            Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == _leaveID);
            string employeeNames = getLeave.StaffName;
            string leaveType = getLeave.leavetype_vName;
            //lbEraseLeaveApplicationHeader.Text = ("Erase " + employeeNames + "'s " + leaveType + " Application").ToUpper();
            lbEraseLeaveApplicationHeader.Text = "Erase " + employeeNames + "'s " + leaveType + " application";
            txtEraseLeaveApplicationReasons.Text = "";
            ModalPopupExtenderEraseLeaveApplication.Show();
            return;
        }
        //confirm if we are erasing the leave application
        protected void lnkBtnEraseLeaveApplication_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderEraseLeaveApplication.Show();
            _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmEraseLeaveApplication, "Are you sure you want to erase the leave application?");
            return;
        }
        //method for erasing leave application
        private void EraseALeaveApplication()
        {
            try
            {
                ModalPopupExtenderEraseLeaveApplication.Show();
                if (txtEraseLeaveApplicationReasons.Text.Trim() == "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Enter reasons why the leave application is being erased!", this, PanelLeaves); ;
                    txtEraseLeaveApplicationReasons.Focus();
                    return;
                }
                else
                {
                    string removalReasons = txtEraseLeaveApplicationReasons.Text.Trim();
                    //erase the leave application
                    int leaveID = Convert.ToInt32(Session["EraseLeaveID"]);
                    HRManagerClasses.Leave eraseLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
                    eraseLeave.leave_bIsRemoved = true;
                    eraseLeave.leave_uiRemovedByStaffID = _StaffMasterID;//logged in employee
                    eraseLeave.leave_dtDateRemoved = DateTime.Now;
                    eraseLeave.leave_vRemovalReasons = removalReasons;
                    db.SubmitChanges();
                    ClearEraseLeaveApplicationControls();
                    _hrClass.LoadHRManagerMessageBox(1, "Leave application has been erased from leave applications!", this, PanelLeaves);
                    //display leave applications made in the current year
                    GetLeavesApplicationsMadePerYear(DateTime.Now.Year);
                    //send leave erased mail to the employee who had applied the leave and hr department
                    SendLeaveErasedEmailNotification(_StaffMasterID, leaveID, removalReasons);
                    //GetRejectedLeavesHistory();//refresh rejetcted leaves details
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaves);
                return;
            }
        }
        //method for sending a leave erased notification 
        private void SendLeaveErasedEmailNotification(Guid erasedByStaffMasterID, int leaveID, string removalReasons)
        {
            //try
            //{
            //    //get first name of the employee who erased the leave applicaetion
            //    string erasedByFirstName = _hrClass.getStaffFirstName(erasedByStaffMasterID);
            //    HRManagerClasses.Leave getErasedLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
            //    string _keepStaffOneInformedFirstName, _keepStaffOneEmailAddress, emailSubject, emailBody, hrEmailBody, fromDate, toDate, leaveType, leaveDays;//get leave details
            //    //get staff one to be informed
            //    Guid _keepStaffOneInformedID = _hrClass.ReturnEscalationKeepStaffOneInformedID(_escalationMasterID);
            //    _keepStaffOneInformedFirstName = _hrClass.getStaffFirstName(_keepStaffOneInformedID);
            //    //get email address
            //    _keepStaffOneEmailAddress = _hrClass.getEmployeeEmailAddress(_keepStaffOneInformedID);
            //    try
            //    {
            //        if (getErasedLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
            //        {
            //            fromDate = _hrClass.ShortDateDayStart(getErasedLeave.leave_dtFrom.ToString()) + " AM";
            //        }
            //        else
            //        {
            //            fromDate = _hrClass.ShortDateDayStart(getErasedLeave.leave_dtFrom.ToString()) + " PM";
            //        }
            //        if (getErasedLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
            //        {
            //            toDate = _hrClass.ShortDateDayStart(getErasedLeave.leave_dtTill.ToString()) + " AM";
            //        }
            //        else
            //        {
            //            toDate = _hrClass.ShortDateDayStart(getErasedLeave.leave_dtTill.ToString()) + " PM";
            //        }
            //        leaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getErasedLeave.leave_siLeaveTypeID).leavetype_vName.ToString();
            //        leaveDays = getErasedLeave.leave_fNoDaysAlloc.ToString();
            //        //get employee who had applied for leave
            //        Guid leaveApplicantStaffMstID = (Guid)getErasedLeave.leave_uiStaffID;
            //        StaffMaster_View getEmployee = db.StaffMaster_Views.Single(p => p.StaffID == leaveApplicantStaffMstID);
            //        string employeeNames = getEmployee.StaffName;
            //        string employeeNumber = getEmployee.StaffRef;
            //        //string employeeManager = getEmployee.staffmst_vStaffManager;
            //        string employeeFirstName = _hrClass.getStaffFirstName(leaveApplicantStaffMstID);//get leave applicant first name
            //        string employeeEmailAddress = getEmployee.EmailAddress.ToString().Trim();

            //        emailSubject = "Leave Application Erased Notification";
            //        hrEmailBody = "Dear " + _keepStaffOneInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + ", employee number " + employeeNumber + "<br>who had applied applied for " + leaveType + " for a period of " + leaveDays + " days<br>starting from " + fromDate + " to " + toDate + "<br>and has been erased from leave application by " + erasedByFirstName + ".<br><br>Reasons being: <br><ul><li>" + removalReasons + "</li></ul>";

            //        //check if the leave applicant has an email address and send the notyfication email
            //        if (employeeEmailAddress != "")//
            //        {
            //            emailBody = "Dear  " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + " you had earlier applied for <br>from " + fromDate + " to " + toDate + " for a period of " + leaveDays + " days<br>has been erased from leave applications by " + erasedByFirstName + ".<br><br>The reasons cited for erasing the application are:<br><ul><li>" + removalReasons + "</li></ul>";
            //            //send leave erased mail
            //            try
            //            {
            //                _hrClass.SendEmail(employeeEmailAddress, emailSubject, emailBody);
            //            }
            //            catch { }
            //        }
            //        //send mail to hr department
            //        try
            //        {
            //            _hrClass.SendEmail(_keepStaffOneEmailAddress, emailSubject, hrEmailBody);
            //        }
            //        catch { }
            //    }
            //    catch { }
            //    return;
            //}
            //catch (Exception ex)
            //{
            //    _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelLeaves);
            //    return;
            //}
        }
        //clear erase leave application controls
        private void ClearEraseLeaveApplicationControls()
        {
            Session["EraseLeaveID"] = null;
            txtEraseLeaveApplicationReasons.Text = "";
            lbEraseLeaveApplicationHeader.Text = "Ease Leave Apppication";
            ModalPopupExtenderEraseLeaveApplication.Hide();
            return;
        }
        //erase leave application
        protected void lnkBtnConfirmEraseLeaveApplication_Click(object sender, EventArgs e)
        {
            EraseALeaveApplication(); return;
        }

        //LEAVE APPLICATIONS DUE FOR APPROVAL
        //get leave due for approval(i.e leaves awaiting for approval by the relevant managers)
        private void GetLeavesDueForApproval()
        {
            try
            {
                object _display;
                _display = db.PROC_LeaveDueForApprovalForAllStaff();
                gvLeavesDueForApproval.DataSourceID = null;
                gvLeavesDueForApproval.DataSource = _display;
                gvLeavesDueForApproval.DataBind();
                Session["gvLeavesDueForApprovalData"] = _display;
                return;
            }
            catch { }
        }

        //check if a leave application is being approved or being cancelled
        protected void rblApproveOrCancel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderApproveOrCancelLeave.Show();
            if (rblApproveOrCancel.SelectedIndex == 0)//check if it an approval
            {
                lbCancelationReason.Visible = false;
                lbCancelationReasonError.Visible = false;
                txtLeaveCancelationReasons.Visible = false;
                lnkBtnApproveOrCancelLeave.Text = "Approve Leave";
                lnkBtnApproveOrCancelLeave.ToolTip = "Approve Leave";
                return;
            }
            else//if its a cancelation
            {
                lbCancelationReason.Visible = true;
                lbCancelationReasonError.Visible = true;
                txtLeaveCancelationReasons.Visible = true;
                lnkBtnApproveOrCancelLeave.Text = "Disapprove Leave";
                lnkBtnApproveOrCancelLeave.ToolTip = "Disapprove Leave";
                return;
            }

        }
        //check who is authorizing the approval or the cancellation
        protected void rblApprovedOrCancelledBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderApproveOrCancelLeave.Show();
            if (rblApprovedOrCancelledBy.SelectedIndex == 0)//check if it is being approved/canceled by me(logged in employee)
            {
                lbManagerName.Visible = false;
                lbManagerNameError.Visible = false;
                ddlApprovedOrCanceledByManager.Visible = false;
                return;
            }
            else//if its being approved / canceled by another manager
            {
                lbManagerName.Visible = true;
                lbManagerNameError.Visible = true;
                ddlApprovedOrCanceledByManager.Visible = true;
                return;
            }
        }
        //get the leave that is to be updated on gvLeavesToApprove or cancel a leave
        protected void gvLeavesDueForApproval_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("ApproveOrCancelLeave") == 0 || e.CommandName.CompareTo("EditLeave") == 0 || e.CommandName.CompareTo("ViewAttachedDocument") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFLeaveID = (HiddenField)gvLeavesDueForApproval.Rows[ID].FindControl("HFID");
                    if (e.CommandName.CompareTo("ApproveOrCancelLeave") == 0)//check if we are approving or cancelling a leave
                    {
                        Session["ApproveOrCancelLeaveID"] = _HFLeaveID.Value;
                        LoadLeaveApplicationDetailsForApprovalOrCancellation(Convert.ToInt32(_HFLeaveID.Value));
                        return;
                    }
                    else if (e.CommandName.CompareTo("EditLeave") == 0)//check if we are approving or cancelling a leave
                    {
                        LoadLeaveDetailsForEdit(Convert.ToInt32(_HFLeaveID.Value));
                        return;
                    }
                    else if (e.CommandName.CompareTo("ViewAttachedDocument") == 0)//viweing attached leave document
                    {
                        Session["DocumentToViewCase"] = 3;//case 3 for viewing sick leave document on the human resource document viewer page
                        //  Response.Redirect(string.Format("~/Human_Resource/Human_Resource_Document_Viewer.aspx?id={0}", _HFLeaveID.Value));
                        string strPageUrl = "Human_Resource_Document_Viewer.aspx?id=" + _HFLeaveID.Value;
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strPageUrl + "','_blank')", true);//to open in a new window
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occurred." + ex.Message.ToString() + ". Please try again.", this, PanelLeaves);
                return;
            }
        }

        //method for performing a task depending on a selected value in the header dropdownlist in gvLeavesDueForApproval gridview
        protected void ddlGvLeavesDueForApprovalHeaderDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList ddlHeader = (DropDownList)gvLeavesDueForApproval.HeaderRow.FindControl("ddlHeader");
                if (ddlHeader.SelectedIndex == 0) { }
                else
                {
                    int _selectedIndex = ddlHeader.SelectedIndex;
                    ddlHeader.SelectedIndex = 0;//select the first index after geting the selected index
                    ClearApproveOrCancelLeaveApplicationControls();//clear controls
                    if (!_hrClass.isGridviewItemSelected(gvLeavesDueForApproval))//check if any record is selected
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "There is no leave due for approval selected!.<br>Select a leave due for approval that you want to approve, cancel or edit !", this, PanelLeaves);
                        return;
                    }
                    else
                    {
                        int x = 0;
                        foreach (GridViewRow _grv in gvLeavesDueForApproval.Rows)
                        {
                            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                            if (_cb.Checked) ++x;
                        }
                        if (x > 1)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one leave due for approval!. <br>Select only one leave due for approval at a time!", this, PanelLeaves);
                            return;
                        }
                        foreach (GridViewRow _grv in gvLeavesDueForApproval.Rows)
                        {
                            CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                            if (_cb.Checked) ++x;
                            if (_cb.Checked)
                            {
                                HiddenField _HFLeaveID = (HiddenField)_grv.FindControl("HFID");
                                int _leaveID = Convert.ToInt32(_HFLeaveID.Value);

                                if (_selectedIndex == 1)//approving or canceling
                                {
                                    Session["ApproveOrCancelLeaveID"] = _HFLeaveID.Value;
                                    LoadLeaveApplicationDetailsForApprovalOrCancellation(_leaveID);
                                    return;
                                }
                                else if (_selectedIndex == 2)//editing
                                {
                                    LoadLeaveDetailsForEdit(_leaveID);
                                    return;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured ! " + ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //loads leave application details to be approved or canceled
        private void LoadLeaveApplicationDetailsForApprovalOrCancellation(int leaveID)
        {
            Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == leaveID);
            string employeeNames = getLeave.StaffName;
            string leaveType = getLeave.leavetype_vName;
            //lbApproveOrCancelLeaveHeader.Text = ("Approve / Disapprove " + employeeNames + "'s " + leaveType + " Application").ToUpper();
            lbApproveOrCancelLeaveHeader.Text = "Approve / Disapprove " + employeeNames + "'s " + leaveType + " Application";
            ModalPopupExtenderApproveOrCancelLeave.Show();
        }
        //clear approve or cancel leave application controls
        private void ClearApproveOrCancelLeaveApplicationControls()
        {
            Session["ApproveOrCancelLeaveID"] = null;
            txtLeaveCancelationReasons.Text = "";
            rblApproveOrCancel.SelectedIndex = 0;
            lbManagerName.Visible = false;
            lbManagerNameError.Visible = false;
            ddlApprovedOrCanceledByManager.SelectedIndex = 0;
            ddlApprovedOrCanceledByManager.Visible = false;
            rblApprovedOrCancelledBy.SelectedIndex = 0;
            lbCancelationReason.Visible = false;
            lbCancelationReasonError.Visible = false;
            txtLeaveCancelationReasons.Visible = false;
            lnkBtnApproveOrCancelLeave.Text = "Approve Leave";
            ModalPopupExtenderApproveOrCancelLeave.Hide();
            return;
        }
        //validate appprove or cancele leave application controls
        private string ValidateApproveOrCancelLeaveApplicationControls()
        {
            if (rblApproveOrCancel.SelectedIndex == -1)
            {
                rblApproveOrCancel.Focus();
                return "Select whether the leave is being approved or disapproved!";
            }
            else if (rblApprovedOrCancelledBy.SelectedIndex == -1)
            {
                rblApprovedOrCancelledBy.Focus();
                return "Select who is authorizing the leave approval or disapproval!";
            }
            else if (rblApproveOrCancel.SelectedIndex == 1 && txtLeaveCancelationReasons.Text.Trim() == "")
            {
                txtLeaveCancelationReasons.Focus();
                return "Enter leave disapproval reasons!";
            }
            else if (rblApprovedOrCancelledBy.SelectedIndex == 1 && ddlApprovedOrCanceledByManager.SelectedIndex == 0)
            {
                ddlApprovedOrCanceledByManager.Focus();
                return "Select manager who has authorized the leave approval or disapproval!";
            }
            else return "";
        }
        //method for approving or canceling a leave application
        private void AprroveOrCancelLeaveApplication()
        {
            ModalPopupExtenderApproveOrCancelLeave.Show();
            try
            {

                Guid approvedOrCancelledByStaffMasterID = _StaffMasterID;//get the employee logged to be the approving manager by default
                //check if its being approved by a different manager from the one logged in
                if (rblApprovedOrCancelledBy.SelectedIndex == 1)
                {
                    approvedOrCancelledByStaffMasterID = _hrClass.ReturnGuid(ddlApprovedOrCanceledByManager.SelectedValue);
                }
                //approve or cancel leave application
                int approveOrCancelLeaveID = Convert.ToInt32(Session["ApproveOrCancelLeaveID"]);
                HRManagerClasses.Leave approveOrCancelLeave = db.Leaves.Single(p => p.leave_iLeaveID == approveOrCancelLeaveID);
                //check if its an approval
                if (rblApproveOrCancel.SelectedIndex == 0)
                {
                    //then approve the leave
                    approveOrCancelLeave.leave_uiAuthByStaffID = approvedOrCancelledByStaffMasterID;
                    approveOrCancelLeave.leave_dtDateApproved = DateTime.Now;
                    approveOrCancelLeave.leave_bLeaveStatus = true;
                    db.SubmitChanges();
                    //update leaves in the escalation  tranasction table with the leave serial number number being  approved
                    UpdateLeavesInEscalationTransactionTable(approveOrCancelLeave.leave_vSerialNumber, "Approved", approvedOrCancelledByStaffMasterID);

                    //lbApproveOrCancelLeaveInfo.Text = "Leave application has been approved!";
                    ModalPopupExtenderApproveOrCancelLeave.Hide();
                    _hrClass.LoadHRManagerMessageBox(2, "Leave application has been approved!", this, PanelLeaves);
                }
                else if (rblApproveOrCancel.SelectedIndex == 1)//check if its a leave cancellation
                {
                    //then cancel the leave
                    approveOrCancelLeave.leave_bIsCancelled = true;
                    approveOrCancelLeave.leave_vCancellationReason = txtLeaveCancelationReasons.Text.Trim();
                    approveOrCancelLeave.leave_uiCancelledByStaffID = approvedOrCancelledByStaffMasterID;
                    approveOrCancelLeave.leave_dtDateCancelled = DateTime.Now;
                    db.SubmitChanges();

                    //update leaves in the escalation transtion table with leave serial number being canceled
                    UpdateLeavesInEscalationTransactionTable(approveOrCancelLeave.leave_vSerialNumber, "Cancelled", approvedOrCancelledByStaffMasterID);

                    // lbApproveOrCancelLeaveInfo.Text = "Leave application has been cancelled!";
                    ModalPopupExtenderApproveOrCancelLeave.Hide();
                    _hrClass.LoadHRManagerMessageBox(2, "Leave application has been disapproved!", this, PanelLeaves);
                }
                //refresh leaves due for approval by the managers
                GetLeavesDueForApproval();
                //refresh leave applications made in the current year
                GetLeavesApplicationsMadePerYear(DateTime.Now.Year);
                //send leave approval or notification email to the leave applicant
                SendLeaveApprovalOrCancelationEmailNotification(approvedOrCancelledByStaffMasterID, approveOrCancelLeaveID);
                //clear controls
                ClearApproveOrCancelLeaveApplicationControls();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error has occured. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaves);
                return;
            }
        }
        //method for sending a leave approval notification to the leave applicant
        private void SendLeaveApprovalOrCancelationEmailNotification(Guid approvedOrCancelledByStaffMasterID, int leaveID)
        {
            try
            {
                //get first name of the manager who approved or cancelled the leave
                string approvedOrCancelledByFirstName = _hrClass.getStaffFirstName(approvedOrCancelledByStaffMasterID);
                HRManagerClasses.Leave getApprovedLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
                string _keepStaffOneInformedFirstName, _keepStaffOneInformedEmailAddress, emailSubject, emailBody, hrEmailBody, fromDate, toDate, leaveType, leaveDays;//get leave details
                short _escalationMasterID = Convert.ToInt16(_hrClass.ReturnEscalationMasterID());
                //get staff one to be keept informed
                Guid _keepStaffOneInformedID = _hrClass.ReturnEscalationKeepStaffOneInformedID(_escalationMasterID);
                _keepStaffOneInformedFirstName = _hrClass.getStaffFirstName(_keepStaffOneInformedID);
                _keepStaffOneInformedEmailAddress = _hrClass.getStaffEmailAddress(_keepStaffOneInformedID);
                try
                {
                    if (getApprovedLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                    {
                        fromDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtFrom.ToString()) + " AM";
                    }
                    else
                    {
                        fromDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtFrom.ToString()) + " PM";
                    }
                    if (getApprovedLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                    {
                        toDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtTill.ToString()) + " AM";
                    }
                    else
                    {
                        toDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtTill.ToString()) + " PM";
                    }
                    leaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getApprovedLeave.leave_siLeaveTypeID).leavetype_vName.ToString();
                    leaveDays = getApprovedLeave.leave_fNoDaysAlloc.ToString();
                    //get employee who had applied for leave
                    Guid leaveApplicantStaffMstID = (Guid)getApprovedLeave.leave_uiStaffID;
                    StaffMaster_View getEmployee = db.StaffMaster_Views.Single(p => p.StaffID == leaveApplicantStaffMstID);
                    string employeeNames = getEmployee.StaffName;
                    string employeeNumber = getEmployee.StaffRef;
                    string employeeFirstName = _hrClass.getStaffFirstName(leaveApplicantStaffMstID);//get leave applicant first name
                    string employeeEmailAddress = getEmployee.EmailAddress.ToString().Trim();
                    string loggedInEmployeeFirstName = _hrClass.getStaffFirstName(_StaffMasterID);//get the logged in employee fisrt name
                    //string genderPossesion = "his";
                    //if (getEmployee.staffmst_cGender == "Female")
                    //{
                    //    genderPossesion = "her";
                    //}
                    //check if the leave applicant has an email address and send the notyfication email
                    if (employeeEmailAddress != "")//
                    {
                        //check if itz a leave approval 
                        if (rblApproveOrCancel.SelectedIndex == 0)
                        {
                            emailSubject = "Leave Application Approval Alert";
                            //check who is approving the leave
                            if (rblApprovedOrCancelledBy.SelectedIndex == 0)//by the logged in employee
                            {
                                emailBody = "Dear " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + " you had earlier applied for from " + fromDate + " to " + toDate + " for a period of " + leaveDays + " days<br>has been approved by " + loggedInEmployeeFirstName + ".";
                                hrEmailBody = "Dear " + _keepStaffOneInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + ", employee number " + employeeNumber + " has applied for " + leaveType + " for a period of " + leaveDays + " days starting from " + fromDate + " to " + toDate + " and has been approved by " + loggedInEmployeeFirstName + ".";
                            }
                            else//on behalf of the employee's manager
                            {
                                emailBody = "Dear " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + " you had earlier applied for <br>from " + fromDate + " to " + toDate + " for a period of " + leaveDays + " days<br>has been approved by " + loggedInEmployeeFirstName + " on behalf of your manager " + approvedOrCancelledByFirstName + ".";
                                hrEmailBody = "Dear " + _keepStaffOneInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + ", employee number " + employeeNumber + "<br>has applied for " + leaveType + " for a period of " + leaveDays + " days<br>starting from " + fromDate + " to " + toDate + "<br>and has been approved by " + loggedInEmployeeFirstName + " on behalf of " + approvedOrCancelledByFirstName + ".";
                            }
                            //send approval mail
                            try
                            {
                                _hrMail.SendEmail(employeeEmailAddress, emailSubject, emailBody);
                            }
                            catch
                            { }
                            //send mail to hr department
                            try
                            {
                                _hrMail.SendEmail(_keepStaffOneInformedEmailAddress, emailSubject, hrEmailBody);
                            }
                            catch
                            { }
                        }
                        //ckeck if its a leave cancellation
                        else if (rblApproveOrCancel.SelectedIndex == 1)
                        {
                            string leaveCancellationReasons = txtLeaveCancelationReasons.Text.Trim();
                            emailSubject = "Leave Application Disapproval Alert";
                            //check who is disapproving the leave application
                            if (rblApprovedOrCancelledBy.SelectedIndex == 0)//by the logged in employee
                            {
                                emailBody = "Dear " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + "  that you had earlier applied for from " + fromDate + " to " + toDate + "<br>has been dispproved by " + loggedInEmployeeFirstName + ".<br><br>The reasons cited for not approving the leave are:<br><br><ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveCancellationReasons + "</li></ul>";
                            }
                            else
                            {
                                emailBody = "Dear " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + "  that you had earlier applied for from " + fromDate + " to " + toDate + "<br>has been dispproved by " + loggedInEmployeeFirstName + " on behalf of " + approvedOrCancelledByFirstName + ".<br><br>The reasons cited for not approving the leave are:<br><br><ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveCancellationReasons + "</li></ul>";
                            }
                            //send cancellation mail
                            try
                            {
                                _hrMail.SendEmail(employeeEmailAddress, emailSubject, emailBody);
                            }
                            catch { }
                        }
                    }
                }
                catch { }
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error ocured. " + ex.Message.ToString(), this, PanelLeaves);
                return;
            }
        }
        //method for updating a leave in the escalation  transaction table once an update,a delete or a leave cancelation is made
        private void UpdateLeavesInEscalationTransactionTable(string leaveSerialNumber, string closeType, Guid closedByStaffID)
        {
            foreach (EscalationTransaction updateLeavesEscalated in db.EscalationTransactions.Where(p => p.escalationtransaction_vTransactionNumber == leaveSerialNumber))
            {
                updateLeavesEscalated.escalationtransaction_bIsClosed = true;
                updateLeavesEscalated.escalationtransaction_vCloseType = closeType;
                updateLeavesEscalated.escalationtransaction_uiClosedByStaffID = closedByStaffID;
                db.SubmitChanges();
            }
        }
        //load confirm leave approval or dissapproval
        private void LoadConfirmLeaveApprovalOrDisapproval()
        {
            ModalPopupExtenderApproveOrCancelLeave.Show();
            try
            {
                string _error = ValidateApproveOrCancelLeaveApplicationControls();//check if there are any errors found
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaves);
                    return;
                }
                else
                {
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirmLeaveApprovalOrDisapproval, "Are you sure you want to " + rblApproveOrCancel.SelectedValue.ToLower() + " the leave application?");
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error has occured. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaves);
                return;
            }
        }
        //confirm leave approval or disapproval
        protected void lnkBtnApproveOrCancelLeave_Click(object sender, EventArgs e)
        {
            LoadConfirmLeaveApprovalOrDisapproval(); return;
        }
        //leave application
        protected void lnkBtnConfirmLEaveApprovalOrDisapproval_Click(object sender, EventArgs e)
        {
            AprroveOrCancelLeaveApplication(); return;
        }
        //get rejected leave history
        private void GetRejectedLeavesHistory()
        {
            try
            {
                object _display;
                _display = db.PROC_LeavesRejectedlForAllStaff();
                gvRejectedLeaveHistory.DataSourceID = null;
                Session["gvRejectedLeaveHistoryData"] = _display;
                gvRejectedLeaveHistory.DataSource = _display;
                gvRejectedLeaveHistory.DataBind();
                return;
            }
            catch { }
        }

        protected void gvRejectedLeaveHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRejectedLeaveHistory.PageIndex = e.NewPageIndex;
            Session["gvRejectedLeaveHistoryPageIndex"] = e.NewPageIndex;
            gvRejectedLeaveHistory.DataSource = (object)Session["gvRejectedLeaveHistoryData"];
            gvRejectedLeaveHistory.DataBind();
            return;
        }
        protected void gvRejectedLeaveHistory_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvRejectedLeaveHistoryageIndex"] == null)//check if page index is empty
                {
                    gvRejectedLeaveHistory.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvRejectedLeaveHistoryPageIndex"]);
                    gvRejectedLeaveHistory.PageIndex = pageIndex;
                    return;
                }
            }
            catch
            {//
            }
        }
        //method for searching for rejected by use of reference number, name, id number or phone number
        private void SearchForRejectedLeaves()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearchRejectedLeaves.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.PROC_LeavesRejectedlForAllStaff().Where(p => (p.StaffRef.ToLower() + p.StaffName.ToLower() + p.IDNumber.ToLower() + p.PhoneNumber.ToLower()).Contains(_searchText)).OrderBy(p => p.StaffName).ToList();
                gvRejectedLeaveHistory.DataSourceID = null;
                gvRejectedLeaveHistory.DataSource = _searchDetails;
                Session["gvRejectedLeaveHistoryData"] = _searchDetails;
                gvRejectedLeaveHistory.DataBind();
                txtSearchRejectedLeaves.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelLeaves);
                return;
            }
        }
        //search for rejected leave by using the text entered 
        protected void txtSearchRejectedLeaves_TextChanged(object sender, EventArgs e)
        {
            SearchForRejectedLeaves();
            return;
        }
    }
}