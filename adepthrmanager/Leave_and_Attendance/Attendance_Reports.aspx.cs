﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Attendance_Reports : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerReports _hrReports = new HRManagerReports();
        static short loggedStaffPostedAtID = 0;
        ReportDataSource _reportSource = new ReportDataSource();
        DataTable dt = new DataTable();
        ReportDocument attendanceReport = new ReportDocument();
        string reportPath = "";
        //page init method for viewing leave leave reports
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["ReportType"] != null)
                {
                    AttendanceReportViewing();//VIEW ATTENDANCE REPORTS
                }
            }
            catch { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateReportsDropDowns();//populate reports drop downs
            }
            try
            {
                loggedStaffPostedAtID = Convert.ToInt16(Session["LoggedStaffPostedAtID"]);//get the location where the logged in employee is posed at
            }
            catch { }

        }
        //populate reports drop downs
        private void PopulateReportsDropDowns()
        {
            _hrClass.GetStaffNames(ddlSingleStaffAttendanceReportStaffName, "Staff Name");//populate staff name dropdown for single staff attendance report
            _hrClass.GetListingItems(ddlEmployeesDailyAttendanceReportPostingLocation, 2000, "Company Branch");//display company branches(2000) available for daily employees attendance report
            _hrClass.GetListingItems(ddlEmployeesDailyAttendanceReportDepartment, 3000, "Department");//display departments(3000) available for daily employees' attendance report
            _hrClass.GetListingItems(ddlEmployeesWeeklyAttendanceReportPostingLocation, 2000, "Company Branch");//display company branches(2000) available for weekly employees attendance report
            _hrClass.GetListingItems(ddlEmployeesWeeklyAttendanceReportDepartment, 3000, "Department");//display departments(3000) available for weekly employees' attendance report
            return;
        }
        //single staff attendnace per duration report
        //validate single staff attendance report
        private string ValidateSingleStaffAttendanceReportControls()
        {
            if (ddlSingleStaffAttendanceReportStaffName.SelectedIndex == 0)
            {
                return "No staff selected. Select a staff!";
            }
            else if (_hrClass.isDateValid(txtSingleStaffAttendanceFromDate.Text.Trim()) == false)
            {
                return "Invalid from date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtSingleStaffAttendanceFromDate) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.isDateValid(txtSingleStaffAttendanceToDate.Text.Trim()) == false)
            {
                return "Invalid end date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtSingleStaffAttendanceToDate) == false)
            {
                return "Invalid end date entered!";
            }
            else if (Convert.ToDateTime(txtSingleStaffAttendanceFromDate.Text.Trim()) > Convert.ToDateTime(txtSingleStaffAttendanceToDate.Text.Trim()))
            {
                return "Start date cannot be greater than end date!";
            }
            else return "";
        }
        //view single staff attendance report
        private void ViewSingleStaffAttendanceReport()
        {
            ModalPopupExtenderSingleStaffAttendanceReport.Show();
            try
            {
                string _error = ValidateSingleStaffAttendanceReportControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelAttendanceReports);
                    return;
                }
                else
                {

                    Guid _staffID = _hrClass.ReturnGuid(ddlSingleStaffAttendanceReportStaffName.SelectedValue.ToString());
                    string _staffRef = _hrClass.GetStaffReferenceByStaffID(_staffID);
                    string _fromDate = txtSingleStaffAttendanceFromDate.Text.Trim(), _toDate = txtSingleStaffAttendanceToDate.Text.Trim();
                    //check if there are any attendance details for the selected staff during the the selected period
                    if (db.PROC_SingleEmployeeCheckInCheckOutPerDuration(_staffRef, Convert.ToDateTime(_fromDate).Date, Convert.ToDateTime(_toDate).Date).Any())
                    {
                        Session["StaffRef"] = _staffRef;
                        Session["ReportType"] = "SingleStaffAttendanceReport";
                        Session["ReportTitle"] = "Single Staff Attendance Report";
                        Session["FromDate"] = _fromDate.Replace("/", "-");
                        Session["ToDate"] = _toDate.Replace("/", "-");
                        ModalPopupExtenderSingleStaffAttendanceReport.Hide();
                        AttendanceReportViewing();//view the report
                        TabContainerAttendanceReports.ActiveTabIndex = 1;//report viewer
                        return;
                    }
                    else
                    {
                        _hrClass.LoadHRManagerMessageBox(2, "Load report failed There are no attendance details for the selected staff during the selected period!", this, PanelAttendanceReports);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelAttendanceReports);
                return;
            }
        }
        //view single staff leave status report
        protected void lnkBtnViewSingleStaffAttendanceReport_Click(object sender, EventArgs e)
        {
            ViewSingleStaffAttendanceReport();
        }

        //employees' daily attendance report
        //check if which report are viewing if/itz for all the employee's or for all per a posting location
        protected void rblEmployeesDailyAttendanceReportsFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderEmployeesDailyAttendanceReport.Show();
            if (rblEmployeesDailyAttendanceReportsFor.SelectedIndex == 0)//viewing all employees leave status
            {
                ddlEmployeesDailyAttendanceReportPostingLocation.Enabled = false;
                ddlEmployeesDailyAttendanceReportPostingLocation.SelectedIndex = 0;
                return;
            }
            else if (rblEmployeesDailyAttendanceReportsFor.SelectedIndex == 1)//viewing leave status for a particular posting location
            {
                ddlEmployeesDailyAttendanceReportPostingLocation.Enabled = true;
                return;
            }
        }
        //check if we are viewing employees' atytendance per department
        protected void cbEmployeesDailyAttendanceReportPerDepartment_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderEmployeesDailyAttendanceReport.Show();
            ddlEmployeesDailyAttendanceReportDepartment.SelectedIndex = 0;
            if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == true)
            {
                ddlEmployeesDailyAttendanceReportDepartment.Enabled = true;
            }
            else if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == false)
            {
                ddlEmployeesDailyAttendanceReportDepartment.Enabled = false;
            }
        }
        //validate view employees daily attendance report
        private string ValidateEmployeesDailyAttendanceReportControls()
        {
            if (_hrClass.isDateValid(txtEmployeesDailyAttendanceDate.Text.Trim()) == false)
            {
                return "Invalid attendance date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtEmployeesDailyAttendanceDate) == false)
            {
                return "Invalid attendance date entered!";
            }
            else if (rblEmployeesDailyAttendanceReportsFor.SelectedIndex == 1 && ddlEmployeesDailyAttendanceReportPostingLocation.SelectedIndex == 0)
            {
                return "Select a posting / company branch!";
            }
            else if (cbEmployeesDailyAttendanceReportPerDepartment.Checked && ddlEmployeesDailyAttendanceReportDepartment.SelectedIndex == 0)
            {
                return "Select the department!";
            }
            else return "";
        }
        //method for view emplyees daily attendnace report
        private void ViewEmployeeesDailyAttendanceReport()
        {
            ModalPopupExtenderEmployeesDailyAttendanceReport.Show();
            try
            {
                string _error = ValidateEmployeesDailyAttendanceReportControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelAttendanceReports);
                    return;
                }
                else
                {
                    string attendanceDate = txtEmployeesDailyAttendanceDate.Text.Trim().Replace("/", "-");
                    Session["AttendanceDate"] = attendanceDate;
                    //check if the we are viewing daily attendance report for all the emeployees 
                    if (rblEmployeesDailyAttendanceReportsFor.SelectedIndex == 0)//attendance reprt for all the employees
                    {
                        //check if we are viewing daily attendnace report for all employees or per department
                        if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == false)//for all company employees
                        {
                            Session["ReportType"] = "AllStaffDailyAttendanceReport";
                            Session["ReportTitle"] = "All Employees' Daily Attendance Report";
                        }
                        else if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlEmployeesDailyAttendanceReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffDailyAttendancePerDepartment";
                            Session["ReportTitle"] = "Employee Daily Attendance in " + _departmentName + " department";
                        }
                    }
                    else if (rblEmployeesDailyAttendanceReportsFor.SelectedIndex == 1)//attendance report for employees in a posting location
                    {
                        string _employeeLeaveStatusPostingLocation = ddlEmployeesDailyAttendanceReportPostingLocation.SelectedItem.ToString();
                        HttpContext.Current.Session["StaffPostingLocation"] = _employeeLeaveStatusPostingLocation;
                        if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == false)//for all employees in a posting location
                        {
                            Session["ReportType"] = "AllStaffDailyAttendancePerPostingLocation";
                            Session["ReportTitle"] = "Employees' Daily Attendance For " + _employeeLeaveStatusPostingLocation;
                        }
                        else if (cbEmployeesDailyAttendanceReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlEmployeesDailyAttendanceReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffDailyAttendancePerDepartmentInAPostingLocation";
                            Session["ReportTitle"] = "Employees' Daily Attendance in " + _employeeLeaveStatusPostingLocation + ", " + _departmentName + " department";
                        }
                    }
                    ModalPopupExtenderEmployeesDailyAttendanceReport.Hide();
                    AttendanceReportViewing();//view the report
                    TabContainerAttendanceReports.ActiveTabIndex = 1;//report viewer
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelAttendanceReports);
                return;
            }
        }
        //view staff daily attendance report
        protected void lnkBtnViewEmployeesDailyAttendanceReport_Click(object sender, EventArgs e)
        {
            ViewEmployeeesDailyAttendanceReport();
        }
        //Employees' Weekly attendance report

        //employees' Weekly attendance report
        //check if which report are viewing if/itz for all the employee's or for all per a posting location
        protected void rblEmployeesWeeklyAttendanceReportsFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderEmployeesWeeklyAttendanceReport.Show();
            if (rblEmployeesWeeklyAttendanceReportsFor.SelectedIndex == 0)//viewing all employees leave status
            {
                ddlEmployeesWeeklyAttendanceReportPostingLocation.Enabled = false;
                ddlEmployeesWeeklyAttendanceReportPostingLocation.SelectedIndex = 0;
                return;
            }
            else if (rblEmployeesWeeklyAttendanceReportsFor.SelectedIndex == 1)//viewing leave status for a particular posting location
            {
                ddlEmployeesWeeklyAttendanceReportPostingLocation.Enabled = true;
                return;
            }
        }
        //check if we are viewing employees' attendance per department
        protected void cbEmployeesWeeklyAttendanceReportPerDepartment_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderEmployeesWeeklyAttendanceReport.Show();
            ddlEmployeesWeeklyAttendanceReportDepartment.SelectedIndex = 0;
            if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == true)
            {
                ddlEmployeesWeeklyAttendanceReportDepartment.Enabled = true;
            }
            else if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == false)
            {
                ddlEmployeesWeeklyAttendanceReportDepartment.Enabled = false;
            }
        }

        //validate view employees daily attendance report
        private string ValidateEmployeesWeeklyAttendanceReportControls()
        {
            if (_hrClass.isDateValid(txtEmployeesWeeklyAttendanceWeekStartDate.Text.Trim()) == false)
            {
                return "Invalid week start date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtEmployeesWeeklyAttendanceWeekStartDate) == false)
            {
                return "Invalid week start date entered!";
            }
            else if (Convert.ToDateTime(txtEmployeesWeeklyAttendanceWeekStartDate.Text.Trim()).DayOfWeek.ToString() != "Monday")
            {
                return "The selected week start date does not fall on a monday. Week start date should be on a monday!";
            }
            else if (rblEmployeesWeeklyAttendanceReportsFor.SelectedIndex == 1 && ddlEmployeesWeeklyAttendanceReportPostingLocation.SelectedIndex == 0)
            {
                return "Select a posting / company branch!";
            }
            else if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked && ddlEmployeesWeeklyAttendanceReportDepartment.SelectedIndex == 0)
            {
                return "Select the department!";
            }
            else return "";
        }
        //method for view emplyees daily attendnace report
        private void ViewEmployeeesWeeklyAttendanceReport()
        {
            ModalPopupExtenderEmployeesWeeklyAttendanceReport.Show();
            try
            {
                string _error = ValidateEmployeesWeeklyAttendanceReportControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelAttendanceReports);
                    return;
                }
                else
                {
                    string _weekStartDate = txtEmployeesWeeklyAttendanceWeekStartDate.Text.Trim().Replace("/", "-");
                    Session["WeekStartDate"] = _weekStartDate;

                    //check if the we are viewing Weekly attendance report for all the emeployees 
                    if (rblEmployeesWeeklyAttendanceReportsFor.SelectedIndex == 0)//attendance reprt for all the employees
                    {
                        //check if we are viewing Weekly attendnace report for all employees or per department
                        if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == false)//for all company employees
                        {
                            Session["ReportType"] = "AllStaffWeeklyAttendanceReport";
                            Session["ReportTitle"] = "Weekly Attendance Report For All Employees";
                        }
                        else if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlEmployeesWeeklyAttendanceReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffWeeklyAttendancePerDepartment";
                            Session["ReportTitle"] = "Weekly Attendance Report Per department";
                        }
                    }
                    else if (rblEmployeesWeeklyAttendanceReportsFor.SelectedIndex == 1)//attendance report for employees in a posting location
                    {
                        string _employeeLeaveStatusPostingLocation = ddlEmployeesWeeklyAttendanceReportPostingLocation.SelectedItem.ToString();
                        HttpContext.Current.Session["StaffPostingLocation"] = _employeeLeaveStatusPostingLocation;
                        if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == false)//for all employees in a posting location
                        {
                            Session["ReportType"] = "AllStaffWeeklyAttendancePerPostingLocation";
                            Session["ReportTitle"] = "Weekly Attendance Report Per Branch";
                        }
                        else if (cbEmployeesWeeklyAttendanceReportPerDepartment.Checked == true)//for all company employees per department
                        {
                            string _departmentName = ddlEmployeesWeeklyAttendanceReportDepartment.SelectedItem.ToString();
                            Session["ReportDepartmentName"] = _departmentName;
                            Session["ReportType"] = "StaffWeeklyAttendancePerDepartmentInAPostingLocation";
                            Session["ReportTitle"] = "Weekly Attendance Report Per Depart Per Branch";
                        }
                    }
                    ModalPopupExtenderEmployeesWeeklyAttendanceReport.Hide();
                    AttendanceReportViewing();//view the report
                    TabContainerAttendanceReports.ActiveTabIndex = 1;//report viewer
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelAttendanceReports);
                return;
            }
        }
        //view staff Weekly attendance report
        protected void lnkBtnViewEmployeesWeeklyAttendanceReport_Click(object sender, EventArgs e)
        {
            ViewEmployeeesWeeklyAttendanceReport();
        }

        //method for viewing attendance reports
        private void AttendanceReportViewing()
        {
            if (Session["ReportType"].ToString() == "SingleStaffAttendanceReport")//SINGLE STAFF ATTENDANCE  REPORT
            {
                string _staffRef = Session["StaffRef"].ToString(), fromDate = Session["FromDate"].ToString(), toDate = Session["ToDate"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_SingleEmployeeAttendancePerDuration.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_SingleStaffAttendancePerDuration(_staffRef, fromDate, toDate);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("From Date", fromDate);
                attendanceReport.SetParameterValue("To Date", toDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "AllStaffDailyAttendanceReport")//ALL STAFF DAILY ATTENDANCE REPORT
            {
                string attendanceDate = Session["AttendanceDate"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllStaffDailyAttendance.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffDailyAttendance(attendanceDate);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("AttendanceDate", attendanceDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "AllStaffDailyAttendancePerPostingLocation")//ALL STAFF DAILY ATTENDANCE REPORT FOR A PARTICULAR POSTING LOCATION/BRANCH
            {
                string attendanceDate = Session["AttendanceDate"].ToString();
                string _postingLocation = Session["StaffPostingLocation"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllStaffDailyAttendance.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffDailyAttendance(attendanceDate, _postingLocation);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("AttendanceDate", attendanceDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "StaffDailyAttendancePerDepartment")//STAFF DAILY ATTENDANCE REPORT FOR A PARTICULAR DEPARTMENT
            {
                string attendanceDate = Session["AttendanceDate"].ToString();
                string _department = Session["ReportDepartmentName"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_StaffDailyAttendancePerDepartment.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffDailyAttendancePerDepartment(attendanceDate, _department);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("AttendanceDate", attendanceDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "StaffDailyAttendancePerDepartmentInAPostingLocation")//STAFF DAILY ATTENDANCE REPORT FOR A PARTICULAR DEPARTMENT WITHIN A PARICULAR POSTING LOCATION/BRANCH
            {
                string attendanceDate = Session["AttendanceDate"].ToString();
                string _postingLocation = Session["StaffPostingLocation"].ToString();
                string _department = Session["ReportDepartmentName"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_StaffDailyAttendancePerDepartment.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_StaffDailyAttendancePerDepartment(attendanceDate, _postingLocation, _department);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("AttendanceDate", attendanceDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "AllStaffWeeklyAttendanceReport")//WEEKLY ATTENDANCE FOR ALL THE EMPLOYEES
            {
                string weekStartDate = Session["WeekStartDate"].ToString();
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_AllEmployeesWeeklyAttendance.rpt");
                attendanceReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_WeeklyAttendanceForAllEmployees(weekStartDate);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("WeekStartDate", weekStartDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "StaffWeeklyAttendancePerDepartment")//WEEKLY ATTENDANCE FOR A PARTICULAR DEPARTMENT
            {
                string weekStartDate = Session["WeekStartDate"].ToString();
                string departmentName = Session["ReportDepartmentName"].ToString();
                DataSet _reportData = _hrReports.rpt_WeeklyAttendancePerDepartment(weekStartDate, departmentName);
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_WeeklyEmployeesAttendancePerDepartment.rpt");
                attendanceReport.Load(reportPath);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("WeekStartDate", weekStartDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "AllStaffWeeklyAttendancePerPostingLocation")//WEEKLY ATTENDANCE FOR ALL EMPLOYEES IN A POSTING LOCATION
            {
                string weekStartDate = Session["WeekStartDate"].ToString();
                string PostedAt = Session["StaffPostingLocation"].ToString();
                DataSet _reportData = _hrReports.rpt_WeeklyAttendanceForAllEmployeesInAPostingLocation(weekStartDate, PostedAt);
                reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_WeeklyEmployeesAttendancePerBranch.rpt");
                attendanceReport.Load(reportPath);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("WeekStartDate", weekStartDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
            else if (Session["ReportType"].ToString() == "StaffWeeklyAttendancePerDepartmentInAPostingLocation")//WEEKLY ATTENDANCE PER DEPARTMENT IN A POSTING LOCATION
            {
                string weekStartDate = Session["WeekStartDate"].ToString();
                string PostedAt = Session["StaffPostingLocation"].ToString();
                string departmentName = Session["ReportDepartmentName"].ToString();
                 DataSet _reportData = _hrReports.rpt_WeeklyAttendancePerDepartmentInAPostingLocation(weekStartDate, PostedAt, departmentName);

                 reportPath = Server.MapPath("~/Leave_and_Attendance/HR_Leave_Reports/report_WeeklyEmployeesAttendancePerDepartmentInAPostingLocation.rpt");
                attendanceReport.Load(reportPath);
                attendanceReport.SetDataSource(_reportData);
                attendanceReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                attendanceReport.SetParameterValue("WeekStartDate", weekStartDate);
                CrystalReportViewerAttendanceReports.ReportSource = attendanceReport;
                return;
            }
        }
    }
}