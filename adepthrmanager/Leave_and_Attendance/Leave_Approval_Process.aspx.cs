﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Leave_Approval_Process : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //display active employee banes into the relevant dropdowns
                DisplayActiveEmployeesIntoDropDowns();
                LoadLeaveApprovalProcessDetails();//load leave approval process details already set
            }

        }
        //load active employee into the relevant drop downs
        private void DisplayActiveEmployeesIntoDropDowns()
        {
            _hrClass.GetStaffNames(ddlApprovalLevel1, "Approval Level 1 Staff");
            _hrClass.GetStaffNames(ddlApprovalLevel2, "Approval Level 2 Staff");
            _hrClass.GetStaffNames(ddlApprovalLevel3, "Approval Level 3 Staff");
            _hrClass.GetStaffNames(ddlOptionalApprovalLevel1, "Optional Approval Level 1");
            _hrClass.GetStaffNames(ddlOptionalApprovalLevel2, "Optional Approval Level 2");
            _hrClass.GetStaffNames(ddlOptionalApprovalLevel3, "Optional Approval Level 3");
            _hrClass.GetStaffNames(ddlKeepStaff1Informed, "Keep Staff 1 Informed");
            _hrClass.GetStaffNames(ddlKeepStaff2Informed, "Keep Staff 2 Informed");
            _hrClass.GetStaffNames(ddlKeepStaff3Informed, "Keep Staff 3 Informed");
        }
        //check who is approval level 1
        protected void rblApprovalLevel1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblApprovalLevel1.SelectedIndex == 0)//check if approval level 1 is the imeediate manager
            {
                //clear approval level 1 drop down and disable it
                ddlApprovalLevel1.SelectedIndex = 0;
                ddlApprovalLevel1.Enabled = false;
            }
            else//enable them
            {
                ddlApprovalLevel1.Enabled = true;
            }
        }
        //check who is approval level 2
        protected void rblApprovalLevel2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblApprovalLevel2.SelectedIndex == 0)//check if approval 2 is the next manager
            {
                //clearapproval level 2 drop down and disable it
                ddlApprovalLevel2.SelectedIndex = 0;
                ddlApprovalLevel2.Enabled = false;
                return;
            }
            else//enable them
            {
                ddlApprovalLevel2.Enabled = true;
                return;
            }
        }
        //check who is approval level 3
        protected void rblApprovalLevel3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblApprovalLevel3.SelectedIndex == 0)//check if approval 3 is the next manager
            {
                //clear approval level 3 drop down and disable it
                ddlApprovalLevel3.SelectedIndex = 0;
                ddlApprovalLevel3.Enabled = false;
                return;
            }
            else//enable them
            {
                ddlApprovalLevel3.Enabled = true;
                return;
            }
        }
        //check if no item is selected on keep informed 2
        protected void ddlKeepStaff2Informed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKeepStaff2Informed.SelectedIndex == 0)
                cblKeepStaff2InformedDuration.ClearSelection();
        }
        //check if no item is selected on keep informed 3
        protected void ddlKeepStaff3Informed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlKeepStaff3Informed.SelectedIndex == 0)
                cblKeepStaff3InformedDuration.ClearSelection();
        }
        //validate add escalation type controls
        private string ValidateAddEscalationProcessControls()
        {
            if (txtTransactionName.Text.Trim() == "")
            {
                txtTransactionName.Focus();
                return "Enter transaction name!";
            }
            else if (rblApprovalLevel1.SelectedIndex == -1)
            {
                rblApprovalLevel1.Focus();
                return "Select  who is approval level one!";
            }
            else if (rblApprovalLevel1.SelectedIndex == 1 && ddlApprovalLevel1.SelectedIndex == 0)
            {
                ddlApprovalLevel1.Focus();
                return "Select staff who is supposed to be approval level one!";
            }
            else if (rblApprovalLevel2.SelectedIndex == 1 && ddlApprovalLevel2.SelectedIndex == 0)
            {
                ddlApprovalLevel2.Focus();
                return "Select staff who is supposed to be approval level two!";
            }
            else if (rblApprovalLevel3.SelectedIndex == 1 && ddlApprovalLevel3.SelectedIndex == 0)
            {
                ddlApprovalLevel3.Focus();
                return "Select staff who is supposed to be approval level three!";
            }
            else if (ddlKeepStaff1Informed.SelectedIndex == 0)
            {
                ddlKeepStaff1Informed.Focus();
                return "Select keep staff one informed!";
            }
            else if (cblKeepStaff1InformedDuration.SelectedIndex == -1)
            {
                cblKeepStaff1InformedDuration.Focus();
                return "Select when staff one should be informed!";
            }
            else if (ddlKeepStaff2Informed.SelectedIndex != 0 && cblKeepStaff2InformedDuration.SelectedIndex == -1)
            {
                cblKeepStaff2InformedDuration.Focus();
                return "Select when staff two should be informed!";
            }
            else if (ddlKeepStaff3Informed.SelectedIndex != 0 && cblKeepStaff3InformedDuration.SelectedIndex == -1)
            {
                cblKeepStaff3InformedDuration.Focus();
                return "Select when staff three should be informed!";
            }
            else if (txtEscalationAlert1Time.Text.Trim() == "")
            {
                txtEscalationAlert1Time.Focus();
                return "Enter escalation alert one time!";
            }
            else if (_hrClass.isNumberValid(txtEscalationAlert1Time.Text.Trim()) == false)
            {
                txtEscalationAlert1Time.Focus();
                return "Invalid escalation alert one time entered. Enter a numeric value!";
            }
            else if (rblApprovalLevel2.SelectedIndex != -1 && txtEscalationAlert2Time.Text.Trim() == "")
            {
                txtEscalationAlert2Time.Focus();
                return "Enter escalation alert two time!";
            }
            else if (txtEscalationAlert2Time.Text.Trim() != "" && _hrClass.isNumberValid(txtEscalationAlert2Time.Text.Trim()) == false)
            {
                txtEscalationAlert2Time.Focus();
                return "Invalid escalation alert two time entered. Enter a numeric value!";
            }
            else if (rblApprovalLevel3.SelectedIndex != -1 && txtEscalationAlert3Time.Text.Trim() == "")
            {
                txtEscalationAlert3Time.Focus();
                return "Enter escalation alert three time!";
            }
            else if (txtEscalationAlert3Time.Text.Trim() != "" && _hrClass.isNumberValid(txtEscalationAlert3Time.Text.Trim()) == false)
            {
                txtEscalationAlert3Time.Focus();
                return "Invalid escalation alert three time entered. Enter a numeric value!";
            }
            else if (txtDefaultNotificationTime.Text.Trim() == "")
            {
                txtDefaultNotificationTime.Focus();
                return "Enter default escalation notification time  !";
            }
            else
                return "";
        }
        //method for adding a new escalation process
        private void AddLeaveApprovalProcess()
        {
            try
            {
                string _error = ValidateAddEscalationProcessControls();//loook for any errors validated
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveApprovalProcess); return;
                }
                else if (db.LeaveApprovalProcesses.Any(p => p.leaveapprovalproc_vName.ToLower() == txtTransactionName.Text.Trim().ToLower()))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Transaction name entered already exists!", this, PanelLeaveApprovalProcess);
                    return;
                }
                else
                {
                    //add new leave approval process
                    LeaveApprovalProcess newLeaveApprovalProcess = new LeaveApprovalProcess();
                    newLeaveApprovalProcess.leaveapprovalproc_vName = txtTransactionName.Text.Trim();
                    //aprroval levels
                    if (rblApprovalLevel1.SelectedIndex == 0)//check if approval level one is the immediate manager
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1 = Guid.Empty;//set empty
                    }
                    else//another staff
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1 = _hrClass.ReturnGuid(ddlApprovalLevel1.SelectedValue.ToString());
                    if (rblApprovalLevel2.SelectedIndex == 0)//check if approval level two is the next manager or not selected
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2 = Guid.Empty;
                    }
                    else if (rblApprovalLevel2.SelectedIndex == 1)//another staff
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2 = _hrClass.ReturnGuid(ddlApprovalLevel2.SelectedValue.ToString());
                    if (rblApprovalLevel3.SelectedIndex == 0)//check if approval level three is the next manager or not selected
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3 = Guid.Empty;
                    }
                    else if (rblApprovalLevel3.SelectedIndex == 1)//approval level three is another staff
                        newLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3 = _hrClass.ReturnGuid(ddlApprovalLevel3.SelectedValue.ToString());
                    ///optional approval level
                    ///
                    if (ddlOptionalApprovalLevel1.SelectedIndex != 0)
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel1 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel1.SelectedValue);
                    }
                    if (ddlOptionalApprovalLevel2.SelectedIndex != 0)
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel2 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel2.SelectedValue);
                    }
                    if (ddlOptionalApprovalLevel3.SelectedIndex != 0)
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel3 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel3.SelectedValue);
                    }
                    //keep infomed
                    newLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff1Informed = _hrClass.ReturnGuid(ddlKeepStaff1Informed.SelectedValue);
                    newLeaveApprovalProcess.leaveapprovalproc_vKeepStaff1InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff1InformedDuration);
                    if (ddlKeepStaff2Informed.SelectedIndex != 0)
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff2Informed = _hrClass.ReturnGuid(ddlKeepStaff2Informed.SelectedValue);
                        newLeaveApprovalProcess.leaveapprovalproc_vKeepStaff2InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff2InformedDuration);
                    }
                    if (ddlKeepStaff3Informed.SelectedIndex != 0)
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff3Informed = _hrClass.ReturnGuid(ddlKeepStaff3Informed.SelectedValue);
                        newLeaveApprovalProcess.leaveapprovalproc_vKeepStaff3InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff3InformedDuration);
                    }
                    newLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv1 = Convert.ToInt32(txtEscalationAlert1Time.Text.Trim());
                    //escalation times
                    if (txtEscalationAlert2Time.Text.Trim() != "")
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv2 = Convert.ToInt32(txtEscalationAlert2Time.Text.Trim());
                    }
                    if (txtEscalationAlert3Time.Text.Trim() != "")
                    {
                        newLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv3 = Convert.ToInt32(txtEscalationAlert3Time.Text.Trim());
                    }
                    newLeaveApprovalProcess.leaveapprovalproc_iDefaultNotificationTime = Convert.ToInt32(txtDefaultNotificationTime.Text.Trim());
                    db.LeaveApprovalProcesses.InsertOnSubmit(newLeaveApprovalProcess);
                    db.SubmitChanges();
                    HiddenFieldLeaveApprovalProcessID.Value = newLeaveApprovalProcess.leaveapprovalproc_siLeaveApprovalProcessID.ToString();
                    _hrClass.LoadHRManagerMessageBox(2, "Leave approval process has been successfully set!", this, PanelLeaveApprovalProcess);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again!", this, PanelLeaveApprovalProcess); return;
            }
        }
        //method for updating an  escaltion process
        private void UpdateLeaveApprovalProcess(int _leaveApprovalProcessID)
        {
            try
            {
                string _error = ValidateAddEscalationProcessControls();//check for errors while updating the escaltion process

                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelLeaveApprovalProcess); return;
                }
                else if (db.LeaveApprovalProcesses.Any(p => p.leaveapprovalproc_siLeaveApprovalProcessID != _leaveApprovalProcessID && p.leaveapprovalproc_vName.ToLower() == txtTransactionName.Text.Trim().ToLower()))
                {
                    txtTransactionName.Focus();
                    _hrClass.LoadHRManagerMessageBox(1, "Update Failed! The transaction name you are trying to update with already exists!", this, PanelLeaveApprovalProcess);
                    return;
                }
                else
                {
                    //update the leave approval process
                    LeaveApprovalProcess updateLeaveApprovalProcess = db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == _leaveApprovalProcessID);

                    updateLeaveApprovalProcess.leaveapprovalproc_vName = txtTransactionName.Text.Trim();
                    //aprroval levels
                    if (rblApprovalLevel1.SelectedIndex == 0)//check if approval level one is the immediate manager
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1 = Guid.Empty;
                    }
                    else//another staff
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1 = _hrClass.ReturnGuid(ddlApprovalLevel1.SelectedValue.ToString());
                    if (rblApprovalLevel2.SelectedIndex == 0)//check if approval level two is the next manager or not selected
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2 = Guid.Empty;
                    }
                    else if (rblApprovalLevel2.SelectedIndex == 1)//another staff
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2 = _hrClass.ReturnGuid(ddlApprovalLevel2.SelectedValue.ToString());
                    if (rblApprovalLevel3.SelectedIndex == 0)//check if approval level three is the next manager or not selected
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3 = Guid.Empty;
                    }
                    else if (rblApprovalLevel3.SelectedIndex == 1)//approval level three is another staff
                        updateLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3 = _hrClass.ReturnGuid(ddlApprovalLevel3.SelectedValue.ToString());

                    ///optional approval
                    ///

                    if (ddlOptionalApprovalLevel1.SelectedIndex != 0)
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel1 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel1.SelectedValue);
                    }
                    else updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel1 = null;
                    if (ddlOptionalApprovalLevel2.SelectedIndex != 0)
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel2 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel2.SelectedValue);
                    }
                    else updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel2 = null;
                    if (ddlOptionalApprovalLevel3.SelectedIndex != 0)
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel3 = _hrClass.ReturnGuid(ddlOptionalApprovalLevel3.SelectedValue);
                    }
                    else updateLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel3 = null;

                    ///keep informedd
                    updateLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff1Informed = _hrClass.ReturnGuid(ddlKeepStaff1Informed.SelectedValue);
                    updateLeaveApprovalProcess.leaveapprovalproc_vKeepStaff1InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff1InformedDuration);
                    if (ddlKeepStaff2Informed.SelectedIndex != 0)
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff2Informed = _hrClass.ReturnGuid(ddlKeepStaff2Informed.SelectedValue);
                        updateLeaveApprovalProcess.leaveapprovalproc_vKeepStaff2InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff2InformedDuration);
                    }
                    else
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff2Informed = null;
                        updateLeaveApprovalProcess.leaveapprovalproc_vKeepStaff2InformedDuration = null;
                    }
                    if (ddlKeepStaff3Informed.SelectedIndex != 0)
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff3Informed = _hrClass.ReturnGuid(ddlKeepStaff3Informed.SelectedValue);
                        updateLeaveApprovalProcess.leaveapprovalproc_vKeepStaff3InformedDuration = _hrClass.ReturnCheckBoxSelectedItems(cblKeepStaff3InformedDuration);
                    }
                    else
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff3Informed = null;
                        updateLeaveApprovalProcess.leaveapprovalproc_vKeepStaff3InformedDuration = null;
                    }
                    //escalation leave times
                    updateLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv1 = Convert.ToInt32(txtEscalationAlert1Time.Text.Trim());
                    if (txtEscalationAlert2Time.Text.Trim() != "")
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv2 = Convert.ToInt32(txtEscalationAlert2Time.Text.Trim());
                    }
                    else updateLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv2 = null;
                    if (txtEscalationAlert3Time.Text.Trim() != "")
                    {
                        updateLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv3 = Convert.ToInt32(txtEscalationAlert3Time.Text.Trim());
                    }
                    else updateLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv3 = null;
                    updateLeaveApprovalProcess.leaveapprovalproc_iDefaultNotificationTime = Convert.ToInt32(txtDefaultNotificationTime.Text.Trim());
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "Leave approval process details have been successfully updated!", this, PanelLeaveApprovalProcess);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed." + ex.Message.ToString() + ". Please try again!", this, PanelLeaveApprovalProcess);
                return;
            }
        }
        //set leave application process 
        private void SetLeaveEscalationProcess()
        {
            if (HiddenFieldLeaveApprovalProcessID.Value == "")
            {
                AddLeaveApprovalProcess();
                return;
            }
            else
            {
                UpdateLeaveApprovalProcess(Convert.ToInt32(HiddenFieldLeaveApprovalProcessID.Value));
                return;
            }
        }
        protected void lnkBtnSaveLeaveApprocalProcess_Click(object sender, EventArgs e)
        {
            SetLeaveEscalationProcess(); return;
        }
        //load leave approval process details already set
        private void LoadLeaveApprovalProcessDetails()
        {
            try
            {
                if (db.LeaveApprovalProcesses.Any())
                {
                    //get the fisrt or default record

                    LeaveApprovalProcess getLeaveApprovalProcess = db.LeaveApprovalProcesses.Select(p => p).FirstOrDefault();
                    HiddenFieldLeaveApprovalProcessID.Value = getLeaveApprovalProcess.leaveapprovalproc_siLeaveApprovalProcessID.ToString();

                    txtTransactionName.Text = getLeaveApprovalProcess.leaveapprovalproc_vName;
                    txtTransactionName.Enabled = false;//should not edit the transcation name
                    //aprroval levels
                    //check approval level one
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1 == Guid.Empty)
                    {
                        rblApprovalLevel1.SelectedIndex = 0;
                        ddlApprovalLevel1.SelectedIndex = 0;
                        ddlApprovalLevel1.Enabled = false;
                    }
                    else
                    {
                        rblApprovalLevel1.SelectedIndex = 1;
                        ddlApprovalLevel1.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel1.ToString();
                        ddlApprovalLevel1.Enabled = true;
                    }

                    //check approval level 2
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2 == Guid.Empty)//check if its the next manager
                    {
                        rblApprovalLevel2.SelectedIndex = 0;
                        ddlApprovalLevel2.SelectedIndex = 0;
                        ddlApprovalLevel2.Enabled = false;
                    }
                    else//another employee 
                    {
                        rblApprovalLevel2.SelectedIndex = 1;
                        ddlApprovalLevel2.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel2.ToString();
                        ddlApprovalLevel2.Enabled = true;
                    }
                    //check approval level three
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3 == Guid.Empty)
                    {
                        rblApprovalLevel3.SelectedIndex = 0;
                        ddlApprovalLevel3.SelectedIndex = 0;
                        ddlApprovalLevel3.Enabled = false;
                    }
                    else
                    {
                        rblApprovalLevel3.SelectedIndex = 1;
                        ddlApprovalLevel3.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiApprovalLevel3.ToString();
                        ddlApprovalLevel3.Enabled = true;
                    }

                    //optional approval
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel1 != null)
                        ddlOptionalApprovalLevel1.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel1.ToString();
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel2 != null)
                        ddlOptionalApprovalLevel1.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel2.ToString();
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel3 != null)
                        ddlOptionalApprovalLevel1.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiOptionalApprovalLevel3.ToString();

                    //keep staff informed
                    //keep staff 1 informed
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff1Informed != null)
                    {
                        ddlKeepStaff1Informed.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff1Informed.ToString();
                        _hrClass.PopulateSelectCheckBoxList(getLeaveApprovalProcess.leaveapprovalproc_vKeepStaff1InformedDuration, cblKeepStaff1InformedDuration);
                    }
                    else
                    {
                        ddlKeepStaff1Informed.SelectedIndex = 0;
                        cblKeepStaff1InformedDuration.ClearSelection();
                    }
                    //check if there is keep staff 2 informed
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff2Informed != null)
                    {
                        ddlKeepStaff2Informed.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff2Informed.ToString();
                        _hrClass.PopulateSelectCheckBoxList(getLeaveApprovalProcess.leaveapprovalproc_vKeepStaff2InformedDuration, cblKeepStaff2InformedDuration);
                    }
                    else
                    {
                        ddlKeepStaff2Informed.SelectedIndex = 0;
                        cblKeepStaff2InformedDuration.ClearSelection();
                    }
                    //check if there is keep staff 3 informed
                    if (getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff3Informed != null)
                    {
                        ddlKeepStaff3Informed.SelectedValue = getLeaveApprovalProcess.leaveapprovalproc_uiKeepStaff3Informed.ToString();
                        _hrClass.PopulateSelectCheckBoxList(getLeaveApprovalProcess.leaveapprovalproc_vKeepStaff3InformedDuration, cblKeepStaff3InformedDuration);
                    }
                    else
                    {
                        ddlKeepStaff3Informed.SelectedIndex = 0;
                        cblKeepStaff3InformedDuration.ClearSelection();
                    }

                    //escalation times
                    txtEscalationAlert1Time.Text = getLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv1.ToString();
                    txtEscalationAlert2Time.Text = getLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv2.ToString();
                    txtEscalationAlert3Time.Text = getLeaveApprovalProcess.leaveapprovalproc_iEscalationTimeAprvLv3.ToString();
                    txtDefaultNotificationTime.Text = getLeaveApprovalProcess.leaveapprovalproc_iDefaultNotificationTime.ToString();
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while trying to load leave approval process details. " + ex.Message.ToString(), this, PanelLeaveApprovalProcess);
                return;
            }
        }

    }

}