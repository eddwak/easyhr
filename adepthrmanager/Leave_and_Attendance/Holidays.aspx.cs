﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.Leave_and_Attendance
{
    public partial class Holidays : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCompanyBranches();//get branches to which the holiday is applicable to
                DisplayHolidays();//load holidays available
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddHolidayControls();
            ModalPopupExtenderAddHoliday.Show();
            return;
        }
        //clear add holiday controls
        private void ClearAddHolidayControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddHoliday);
            HiddenFieldHolidayID.Value = "";
            lnkBtnSaveHoliday.Visible = true;
            lnkBtnUpdateHoliday.Enabled = false;
            lnkBtnUpdateHoliday.Visible = true;
            lnkBtnClearHoliday.Visible = true;
            lbAddHolidayHeader.Text = "Add New Holiday / Non-Working Day";
            ImageAddHolidayHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //get employee being allocated the leave
        private void GetCompanyBranches()
        {
            try
            {
                ddlHolidayApplicableTo.Items.Clear();
                object _getPostingLocations = db.ListingItems.Where(p => p.ListingMasterID == 2000).OrderBy(p => p.LisitingDetailValue);//master id 2000 for  company branches
                ddlHolidayApplicableTo.DataSource = _getPostingLocations;
                ddlHolidayApplicableTo.Items.Insert(0, new ListItem("Select Holiday Applicable To", "0"));
                ddlHolidayApplicableTo.Items.Insert(1, new ListItem("All Employees", "1"));
                ddlHolidayApplicableTo.DataTextField = "LisitingDetailValue";
                ddlHolidayApplicableTo.DataValueField = "ListingItemIID";
                ddlHolidayApplicableTo.AppendDataBoundItems = true;
                ddlHolidayApplicableTo.DataBind();
                return;
            }
            catch { }
        }
        //validate add holiday entry controls
        private string ValidateAddHolidayControls()
        {
            if (txtHolidayDate.Text.Trim() == "")
            {
                txtHolidayDate.Focus();
                return "Enter / select holiday date!";
            }
            else if (_hrClass.isDateValid(txtHolidayDate.Text.Trim()) == false)
            {
                txtHolidayDate.Focus();
                return "Invalid holiday date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtHolidayDate) == false)
            {
                return "Invalid holiday date entered!";
            }
            else if (rblHolidayDuration.SelectedIndex == -1)
            {
                rblHolidayDuration.Focus();
                return "Check whether the holiday duration will be a full day or half a day!";
            }
            else if (ddlHolidayApplicableTo.SelectedIndex == 0)
            {
                ddlHolidayApplicableTo.Focus();
                return "Select to whom is the holiday applicable to !";
            }
            //else if (txtDescription.Text.Trim() == "")
            //{
            //    txtDescription.Focus();
            //    return "Enter holiday description!";
            //}
            return "";
        }
        //save new holiday
        private void SaveNewHoliday()
        {
            ModalPopupExtenderAddHoliday.Show();
            try
            {
                string _error = ValidateAddHolidayControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelHolidays);
                    return;
                }
                else if (db.Holidays.Any(p => p.holiday_dtDate == Convert.ToDateTime(txtHolidayDate.Text.Trim()).Date && (p.holiday_siApplicableToBranchID == 1 || p.holiday_siApplicableToBranchID == Convert.ToInt16(ddlHolidayApplicableTo.SelectedValue))))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save Failed! A holiday with the date selected already exists!", this, PanelHolidays);
                    return;
                }
                else if (db.Holidays.Any(p => p.holiday_dtDate == Convert.ToDateTime(txtHolidayDate.Text.Trim()) && p.holiday_siApplicableToBranchID != 1 && ddlHolidayApplicableTo.SelectedIndex == 1))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Save Failed! A holiday with the date selected already exists for another posting location. Edit the holiday date to accommodate all employees!", this, PanelHolidays);
                    return;
                }
                else
                {
                    Holiday newHoliday = new Holiday();
                    newHoliday.holiday_dtDate = Convert.ToDateTime(txtHolidayDate.Text.Trim()).Date;
                    if (rblHolidayDuration.SelectedIndex == 0)//check if full day is selected and assign 1
                    {
                        newHoliday.holiday_fDuration = 1;
                    }
                    else newHoliday.holiday_fDuration = 0.5;
                    ////CHECK IF ITZ APPLICABLE TO ALL EMPLOYEE
                    //if (ddlApplicableTo.SelectedIndex == 1)
                    //{
                    //    newHoliday.holidays_siApplicableToPostingAtID = 1;
                    //}
                    //else
                    newHoliday.holiday_siApplicableToBranchID = Convert.ToInt16(ddlHolidayApplicableTo.SelectedValue);
                    newHoliday.holiday_vDescription = txtDescription.Text.Trim();
                    db.Holidays.InsertOnSubmit(newHoliday);
                    db.SubmitChanges();
                    HiddenFieldHolidayID.Value = newHoliday.holiday_iHolidayID.ToString();
                    DisplayHolidays();
                    //lbError.Text = "";
                    //lbInfo.Text = "Holiday details have been successfully saved.";
                    _hrClass.LoadHRManagerMessageBox(2, "Holiday details have been successfully saved.", this, PanelHolidays);
                    lnkBtnUpdateHoliday.Enabled = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again!", this, PanelHolidays);
                return;
            }
        }
        //METHOD FOR UPDATING HOLIDAY
        private void UpdateHoliday(int _holidayID)
        {
            ModalPopupExtenderAddHoliday.Show();
            try
            {
                string _error = ValidateAddHolidayControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelHolidays);
                    return;
                }
                else if (db.Holidays.Any(p => p.holiday_iHolidayID != _holidayID && p.holiday_dtDate == Convert.ToDateTime(txtHolidayDate.Text.Trim()).Date && p.holiday_siApplicableToBranchID == 1 && ddlHolidayApplicableTo.SelectedIndex == 1))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update Failed! A holiday with the date selected already exists for all employees!", this, PanelHolidays);
                    return;
                }
                else if (db.Holidays.Any(p => p.holiday_iHolidayID != _holidayID && p.holiday_dtDate == Convert.ToDateTime(txtHolidayDate.Text.Trim()).Date && (p.holiday_siApplicableToBranchID == 1 || p.holiday_siApplicableToBranchID == Convert.ToInt16(ddlHolidayApplicableTo.SelectedValue))))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Update Failed! A holiday with the date selected already exists!", this, PanelHolidays);
                    return;
                }
                else
                {
                    Holiday updateHoliday = db.Holidays.Single(p => p.holiday_iHolidayID == _holidayID);
                    updateHoliday.holiday_dtDate = Convert.ToDateTime(txtHolidayDate.Text.Trim()).Date;
                    if (rblHolidayDuration.SelectedIndex == 0)//if full day is selected
                    {
                        updateHoliday.holiday_fDuration = 1;
                    }
                    else updateHoliday.holiday_fDuration = 0.5;
                    ////check if the holiday is applicable to all the employess
                    //if (ddlHolidayApplicableTo.SelectedIndex == 1)
                    //{
                    //    updateHoliday.holiday_siApplicableToBranchID = 1;
                    //}
                    //else
                    updateHoliday.holiday_siApplicableToBranchID = Convert.ToInt16(ddlHolidayApplicableTo.SelectedValue);
                    updateHoliday.holiday_vDescription = txtDescription.Text.Trim();
                    db.SubmitChanges();
                    DisplayHolidays();
                    _hrClass.LoadHRManagerMessageBox(2, "Holiday details have been successfully updated.", this, PanelHolidays);
                    return;
                    //}
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update Failed. " + ex.Message.ToString() + ". Please try again!", this, PanelHolidays);
                return;
            }
        }
        protected void lnkBtnSaveHoliday_Click(object sender, EventArgs e)
        {
            SaveNewHoliday();
            return;
        }

        protected void lnkBtnUpdateHoliday_Click(object sender, EventArgs e)
        {
            UpdateHoliday(Convert.ToInt32(HiddenFieldHolidayID.Value));
            return;
        }
        //clear entry controls
        protected void lnkBtnClearHoliday_Click(object sender, EventArgs e)
        {
            ClearAddHolidayControls();
            ModalPopupExtenderAddHoliday.Show();
            return;
        }
        //display holidays
        public void DisplayHolidays()
        {
            try
            {
                dt.Columns.Add(new DataColumn("ID", typeof(int)));
                dt.Columns.Add(new DataColumn("HolidayDate", typeof(string)));
                dt.Columns.Add(new DataColumn("Duration", typeof(string)));
                dt.Columns.Add(new DataColumn("ApplicableTo", typeof(string)));
                dt.Columns.Add(new DataColumn("Description", typeof(string)));
                DataRow dr;
                //get holidays which fall within the current year
                foreach (Holiday_View _holiday in db.Holiday_Views.Where(p => p.holiday_dtDate.Value.Year.ToString() == DateTime.Now.Year.ToString()).OrderByDescending(p => p.holiday_dtDate))
                {
                    dr = dt.NewRow();
                    dr[0] = _holiday.holiday_iHolidayID;
                    dr[1] = _hrClass.ConvertDateDayStart(_holiday.holiday_dtDate.ToString());
                    dr[2] = _holiday.holiday_fDuration.ToString() + " Day"; ;
                    dr[3] = _holiday.holiday_vApplicableToBranch;
                    dr[4] = _holiday.holiday_vDescription;
                    dt.Rows.Add(dr);
                }
                gvHolidays.DataSource = dt;
                gvHolidays.DataBind();
                Session["gvHolidaysData"] = dt;
                return;
            }
            catch { }
        }
        // populate the edit  controls
        private void LoadEditHolidayControls(int _holidayID)
        {
            ClearAddHolidayControls();
            Holiday getHoliday = db.Holidays.Single(p => p.holiday_iHolidayID == _holidayID);
            HiddenFieldHolidayID.Value = _holidayID.ToString();
            txtHolidayDate.Text = _hrClass.ShortDateDayStart(getHoliday.holiday_dtDate.ToString());
            rblHolidayDuration.SelectedValue = getHoliday.holiday_fDuration.ToString();
            ddlHolidayApplicableTo.SelectedValue = getHoliday.holiday_siApplicableToBranchID.ToString();
            txtDescription.Text = getHoliday.holiday_vDescription.ToString();

            lnkBtnSaveHoliday.Visible = false;
            lnkBtnClearHoliday.Visible = true;
            lnkBtnUpdateHoliday.Enabled = true;
            lbAddHolidayHeader.Text = "Edit Holiday / Non-Working Day Details";
            ImageAddHolidayHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddHoliday.Show();
            return;
        }
        //check if a holiday is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvHolidays))//check if a holiday is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a holiday to edit!", this, PanelHolidays);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvHolidays.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one holiday record. Select only one record to edit at a time!", this, PanelHolidays);
                        return;
                    }
                    foreach (GridViewRow _grv in gvHolidays.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFHolidayID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit holiday details
                            LoadEditHolidayControls(Convert.ToInt32(_HFHolidayID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading holiday details for edit." + ex.Message.ToString() + ". Please try again!", this, PanelHolidays);
            }
        }
        //check if a holiday is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvHolidays))//check if a holiday is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a holiday to view its details!", this, PanelHolidays);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvHolidays.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one holiday record. Select only one holiday to view the details at a time!", this, PanelHolidays);
                        return;
                    }
                    foreach (GridViewRow _grv in gvHolidays.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFHolidayID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view holiday details
                            LoadViewHolidayControls(Convert.ToInt32(_HFHolidayID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the holiday details for view purpose!" + ex.Message.ToString(), this, PanelHolidays);
                return;
            }
        }
        // populate the view holiday controls
        private void LoadViewHolidayControls(int _holidayID)
        {
            Holiday getHoliday = db.Holidays.Single(p => p.holiday_iHolidayID == _holidayID);
            HiddenFieldHolidayID.Value = _holidayID.ToString();
            txtHolidayDate.Text = _hrClass.ShortDateDayStart(getHoliday.holiday_dtDate.ToString());
            rblHolidayDuration.SelectedValue = getHoliday.holiday_fDuration.ToString();
            ddlHolidayApplicableTo.SelectedValue = getHoliday.holiday_siApplicableToBranchID.ToString();
            txtDescription.Text = getHoliday.holiday_vDescription.ToString();

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddHoliday);
            lnkBtnSaveHoliday.Visible = false;
            lnkBtnClearHoliday.Visible = false;
            lnkBtnUpdateHoliday.Visible = false;
            lbAddHolidayHeader.Text = "View Holiday / Non-Working Day Details";
            ImageAddHolidayHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddHoliday.Show();
        }
        //load delete holiday details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvHolidays))//check if there is any holiday record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a holiday to delete!", this, PanelHolidays);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvHolidays.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected holiday?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected holidays?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelHolidays);
                return;
            }
        }
        //delete holiday record
        private void DeleteHoliday()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvHolidays.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFHolidayID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _holidayID = Convert.ToInt32(_HFHolidayID.Value);
                        Holiday deleteHoliday = db.Holidays.Single(p => p.holiday_iHolidayID == _holidayID);

                        db.Holidays.DeleteOnSubmit(deleteHoliday);
                        db.SubmitChanges();
                    }
                }
                //refresh holidays after the delete is done
                DisplayHolidays();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected holiday has been deleted.", this, PanelHolidays);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected holidays have been deleted.", this, PanelHolidays);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed." + ex.Message.ToString() + ". Please try again!", this, PanelHolidays);
                return;
            }
        }
        //delete the selected holiday
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteHoliday();//delete selected holiday
            return;
        }
    }
}