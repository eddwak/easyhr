﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Timers;
using System.Configuration;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            //Code that runs on application startup
            //start timers
            StartAdeptHRManagerServiceTimers();
        }

        //start service timers
        private void StartAdeptHRManagerServiceTimers()
        {
            //initialize run adept hr manager mail sending alerts timer
            InitializeRunAdeptHRManaSendingMailAlertsTimer();
        }
        //initialize timer to run sending adept hr manager mail alerts timer
        private void InitializeRunAdeptHRManaSendingMailAlertsTimer()
        {
            Timer runEssEmailAlertsTimer = new Timer();
            runEssEmailAlertsTimer.AutoReset = true;
            //60000 miliseconds=1 minute
            runEssEmailAlertsTimer.Interval = 60000 * Convert.ToDouble(ConfigurationManager.AppSettings.Get("UpdateMailSendingIntervalMinutes"));
            runEssEmailAlertsTimer.Enabled = true;
            runEssEmailAlertsTimer.Elapsed += new ElapsedEventHandler(runEssEmailAlertsTimer_Elapsed);
            runEssEmailAlertsTimer.Enabled = true;
        }
        //checking if the run hr manager email alerts sending timer time has elapsed
        public void runEssEmailAlertsTimer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            HRManagerMailWebService _hrMailWebService = new HRManagerMailWebService();
            //run email sending alerts 
            _hrMailWebService.RunHRManagerSendingMailTransactionProcess();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }
    }
}
