﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/AdeptHRManager.master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdeptHRManager._Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="text-align: center; padding-top: 20px; color: #004A5F; font-size: 28px;">
        <p>
            A comprehensive tool for Human Resources Management
        </p>
    </div>
    <asp:Panel ID="PanelDashBoard" Visible="false" runat="server">
        <div class="dashboard">
            <div class="dashboard-row">
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        NATIONALITIES</div>
                    <asp:GridView ID="gvNationalities" runat="server" AllowPaging="True" Width="65%"
                        AutoGenerateColumns="False" ShowHeader="False" CssClass="dashboard-gridview">
                        <Columns>
                            <asp:BoundField DataField="Nationality" />
                            <asp:BoundField DataField="CountNationality" ItemStyle-HorizontalAlign="Right" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        OFFICES</div>
                    <asp:GridView ID="gvEmpoyeesPerBranches" runat="server" AllowPaging="True" Width="65%"
                        AutoGenerateColumns="False" ShowHeader="False" CssClass="dashboard-gridview">
                        <Columns>
                            <asp:BoundField DataField="Branch" />
                            <asp:BoundField DataField="CountBranch" ItemStyle-HorizontalAlign="Right" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        DEPARTMENTS</div>
                    <div style="height: 140px; overflow-y: scroll;">
                        <asp:GridView ID="gvEmployeesPerDepartments" runat="server" AllowPaging="True" Width="65%"
                            AutoGenerateColumns="False" ShowHeader="False" CssClass="dashboard-gridview">
                            <Columns>
                                <asp:BoundField DataField="Department" />
                                <asp:BoundField DataField="CountDepartment" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="dashboard-row">
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        OVERALL GENDER DISTRIBUTION</div>
                    <%--<asp:Image ID="Image1" Visible="true" ImageUrl="~/images/background/Overall Gender.png"
                        runat="server" />--%>
                    <asp:Chart ID="ChartOverallGender" Palette="Grayscale" runat="server" Visible="true"
                        Height="300px" Width="340px" BorderlineColor="Blue">
                        <Titles>
                            <asp:Title ShadowOffset="3" Name="Items" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Right" IsTextAutoFit="false" Name="Default"
                                LegendStyle="Column" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" ShadowColor="#FFFFFF" />
                        </ChartAreas>
                    </asp:Chart>
                    <%-- <asp:Chart ID="Chart2" runat="server" Height="400px" Width="358px" BackColor="255, 255, 255">
                        <series>
                                <asp:Series ChartArea="ChartArea1" Name="Target 2015" XValueType="String" IsValueShownAsLabel="true"
                                    Label='#VALY%'>
                                </asp:Series>
                                <asp:Series ChartArea="ChartArea1" Name="Achieved" XValueType="String" IsValueShownAsLabel="true"
                                    Label='#VALY%'>
                                </asp:Series>
                            </series>
                        <chartareas>
                                <asp:ChartArea Name="ChartArea1">
                                    <AxisX LineColor="#89B4FF">
                                        <MajorGrid LineColor="#D9EFFD" />
                                    </AxisX>
                                    <AxisY LineColor="#89B4FF" TitleFont="corbelregular, 9pt">
                                        <MajorGrid LineColor="#D9EFFD" />
                                    </AxisY>
                                </asp:ChartArea>
                            </chartareas>
                        <titles>
                                <asp:Title Name="Title1">
                                </asp:Title>
                            </titles>
                        <legends>
                                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center">
                                </asp:Legend>
                            </legends>
                    </asp:Chart>--%>
                </div>
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        GENDER DISTRIBUTION PER OFFICES</div>
                    <%-- <asp:Image ID="Image2" ImageUrl="~/images/background/Gender Per Offices.png" runat="server" />--%>
                    <asp:Chart ID="ChartGenderPerOffices" Palette="Grayscale" runat="server" Visible="true"
                        Height="300px" Width="360px" BorderlineColor="Blue">
                        <Titles>
                            <asp:Title ShadowOffset="3" Name="Items" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="false" Name="Default"
                                LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Female" XValueMember="Office" YValueMembers="Female_Count" Color="#3469BA"
                                IsValueShownAsLabel="True" />
                            <asp:Series Name="Male" XValueMember="Office" YValueMembers="Male_Count" Color="#E60000"
                                IsValueShownAsLabel="True" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" ShadowColor="#FFFFFF" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div class="dashboard-cell">
                    <div class="dashboard-header">
                        AGE DISTRIBUTION</div>
                    <asp:Image ID="Image3" ImageUrl="~/images/background/Age Distribution.png" runat="server" />
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
