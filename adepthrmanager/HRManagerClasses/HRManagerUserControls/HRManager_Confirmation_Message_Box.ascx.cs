﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdeptHRManager.HRManagerClasses.HRManagerUserControls
{
    public partial class HRManager_Confirmation_Message_Box : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public event EventHandler lnkBtnYesConfirmationClick;//click event to be called on user pages for purpose of deleting/ confirmation
        protected void lnkBtnYes_Click(object sender, EventArgs e)
        {
            lnkBtnYesConfirmationClick(sender, e);//call the confirmation event
            return;
        }
       
    }
}