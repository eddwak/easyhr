﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdeptHRManager.HRManagerClasses.HRManagerUserControls
{
    public partial class HRManager_Message_Box : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            DisplayHRManagerMessageBox();//load the message box
        }

        //loading the message box panel
        public void DisplayHRManagerMessageBox()
        {
            //message type checks which kind of message is being loaded, 1 for error and 2 for information
            int MessageType = Convert.ToInt32(Session["MessageType"]);
            lbMessageBoxMessage.Text = Session["HRManagerMessage"].ToString();
            if (MessageType == 1)
            {
                lbMessageBoxHeader.Text = "Message";
                ImageMessageBox.ImageUrl = "~/Images/icons/Small/Danger.png";
            }
            else if (MessageType == 2)
            {
                lbMessageBoxHeader.Text = "Message";
                ImageMessageBox.ImageUrl = "~/Images/icons/Small/Info.png";
            }
            ModalPopupExtenderHRManagerMessageBox.Show();
            return;
        }
    }
}