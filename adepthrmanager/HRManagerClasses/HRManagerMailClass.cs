﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace AdeptHRManager.HRManagerClasses
{
    public class HRManagerMailClass
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        public string _smtpServer = "";
        public int? _smtpPortNumber = null;
        public string _appSenderEmailAddress = "";
        public string _appSenderEmailAddressPassword = "";
        //create the stye for the email body
        public string emailBodyCssStyle = "style='color: black; font-family: Calibri Light; font-size:16px; line-height: 1.2;'";
        private string _hrApplicationLink = "";
        public HRManagerMailClass()
        {
            //check if there are any email setting detail and get them
            if (db.EmailSettings.Any())
            {
                EmailSetting getEmailSetting = db.EmailSettings.FirstOrDefault();
                _smtpServer = getEmailSetting.SMTPServerAddress;
                _smtpPortNumber = getEmailSetting.PortNumber;
                _appSenderEmailAddress = getEmailSetting.EmailAddress;
                _appSenderEmailAddressPassword = getEmailSetting.EmailAddressPassword;
            }
            _hrApplicationLink = ConfigurationManager.AppSettings["HRApplicationLink"];
        }
        //method for sending email 
        public void SendEmail(string _emailTo, string _emailSubject, string _emailBody)
        {
            MailMessage newMail = new MailMessage();
            newMail.To.Add(_emailTo);//to who is the email going to
            newMail.From = new MailAddress(_appSenderEmailAddress);//from who is the email coming from
            newMail.Subject = _emailSubject;//email subject

            newMail.Body = "<p " + emailBodyCssStyle + " >"
               + _emailBody + "</p><p " + emailBodyCssStyle + " >Kind Regards,<br><b>HR Department</b><br><p " + emailBodyCssStyle + " >HR Application Link: " + _hrApplicationLink + "</p><br><u>Note:</u><br>This is an auto generated email from the HR application, please don't reply to it.<br>For any clarifications or queries kindly contact the HR department.</p>";//emaill body

            newMail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = _smtpServer;
            if (_smtpPortNumber != null)//check if the port number is not null
                smtp.Port = (int)_smtpPortNumber;
            smtp.EnableSsl = true;
            //smtp.Timeout = 500;
            smtp.Credentials = new NetworkCredential(_appSenderEmailAddress, _appSenderEmailAddressPassword);
            smtp.Send(newMail);
            return;
        }
        //method for sending an email with an attached document
        public void SendEmailWithAnAttachment(string emailTo, string emailSubject, string emailBody, Attachment attachedDoc)
        {

            MailMessage newMail = new MailMessage();
            newMail.To.Add(emailTo);//to who is the email going to
            newMail.From = new MailAddress(_appSenderEmailAddress);//from who is the email coming from
            newMail.Subject = emailSubject;//email subject
            newMail.Attachments.Add(attachedDoc);

            //newMail.Body = "<font face='Calibri Light' size=3>" + emailBody + "<br><br>Kind Regards,<br><b>HR Department</b><br><br>NB: This is an auto generated email from HR application, please don't reply to it. For any clarifications or queries kindly contact the HR department.</font>";//emaill body
            newMail.Body = "<p style='color: black; font-family: Calibri Light; font-size:16px; line-height: 1.2;' >"
                + emailBody + "<br><br>Kind Regards,<br><b>HR Department</b><br><br><u>Not:</u><br>This is an auto generated email from HR application, please don't reply to it.<br>For any clarifications or queries kindly contact the HR department.</p>";//emaill body
            newMail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = _smtpServer;
            if (_smtpPortNumber != null)//check if the port number is not null
                smtp.Port = (int)_smtpPortNumber;
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential(_appSenderEmailAddress, _appSenderEmailAddressPassword);
            smtp.Send(newMail);
            return;
        }
    }
}