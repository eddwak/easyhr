﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace AdeptHRManager.HRManagerClasses
{
    /// <summary>
    /// Summary description for HRManagerMailWebService
    /// </summary>
    [WebService(Namespace = "http://www.adeptsystems.co.ke/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HRManagerMailWebService : System.Web.Services.WebService
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerMailClass _hrMail = new HRManagerMailClass();

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}
        //
        [WebMethod]
        public void TestMailSending(string _emailTo, string _emailSubject, string _emailBody)
        {
            _hrMail.SendEmail(_emailTo, _emailSubject, _emailBody);
        }
        //create mail sending transtion method
        public void CreateMailSendingTransaction(short sendingCase, int transactingID, Guid transactingStaffID, int? _2ndTransactingID)//int? shows that 2ndtransating  id can be null
        {
            db = new HRManagerDataContext();
            MailSendingTransaction newMailSendingTransaction = new MailSendingTransaction();
            newMailSendingTransaction.mailsendingtransaction_siSendingCase = sendingCase;
            newMailSendingTransaction.mailsendingtransaction_iTransactingID = transactingID;
            newMailSendingTransaction.mailsendingtransaction_uiTransactingStaffID = transactingStaffID;
            newMailSendingTransaction.mailsendingtransaction_dtDateCreated = DateTime.Now;
            newMailSendingTransaction.mailsendingtransaction_bIsClosed = false;
            newMailSendingTransaction.mailsendingtransaction_i2ndTransactingID = _2ndTransactingID;//to store a second transacting id that might be used
            db.MailSendingTransactions.InsertOnSubmit(newMailSendingTransaction);
            db.SubmitChanges();
        }
        //method for updating  a mail sending transaction
        private void UpdateMailSendingTransaction(int mailsendingTransactionID)
        {
            MailSendingTransaction updateMailSendingTransaction = db.MailSendingTransactions.Single(p => p.mailsendingtransaction_iMailSendingTransactionID == mailsendingTransactionID);
            updateMailSendingTransaction.mailsendingtransaction_bIsClosed = true;
            updateMailSendingTransaction.mailsendingtransaction_dtDateTimeClosed = DateTime.Now;
            db.SubmitChanges();
        }
        //web method for sending leave application made mail to the approving manager
        [WebMethod]
        public void SendLeaveApplicationNotificationEmailToApprovingManager(int LeaveID)//case 1
        {
            try
            {
                //get leave details
                Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == LeaveID);
                string fromDate, toDate, fromEffDate, toEffDate, leaveType, leaveDays, leaveReasons;//get leave details
                string leaveSerialNumber = getLeave.leave_vSerialNumber;
                if (getLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = getLeave.leavetype_vName;
                leaveDays = getLeave.leave_fNoDaysAlloc.ToString();
                //create meassadge ro leave days
                string _leaveDaysMessage = "1 day";
                if (leaveDays != "1")//
                    _leaveDaysMessage = leaveDays + " days";
                leaveReasons = getLeave.leave_vRemarks.Trim();
                string _leaveReasonsMessage = "";
                //check if there are any reasond added
                if (leaveReasons != "")
                    _leaveReasonsMessage = "Reasons cited for " + leaveType + " application are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul><br>";


                Guid _staffID = (Guid)getLeave.leave_uiStaffID;//get employee staff master id
                //get employee
                StaffMaster getEmployee = db.StaffMasters.Single(p => p.StaffID == _staffID);
                string employeeNames = getEmployee.StaffName;
                employeeNames = _hrClass.ConvertFirstStringLettersToUpper(employeeNames);
                string employeeNumber = getEmployee.StaffRef.ToString();
                // string employeeManager = getEmployee.staffmst_vStaffManager;
                //employee gender
                string employeeGender = "His", employeeGender2 = "his";//set his to be the default
                if (getEmployee.Gender == "Female")
                {
                    employeeGender = "Her";
                    employeeGender2 = "her";
                }

                //get manager id
                Guid managerID = (Guid)getEmployee.ReportingToStaffID;
                //get manager's first name
                string managerFirstName = _hrClass.getStaffFirstName(managerID);
                //get manager's company email address
                string managerEmailAddress = string.Empty;
                //get leave status totals for the leave being applied for
                PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_staffID, getLeave.leave_siLeaveTypeID).Single();
                //get leave days balance
                double _balanceDays = 0;
                string _leaveDaysBalanceMessage = "";
                //check if the leave type balance is to be calculated
                if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeave.leave_siLeaveTypeID).leavetype_bCalculateLeaveBalance == true)
                {
                    _balanceDays = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed));
                    string _balanceDaysMessage = " day balance is 1 day.";
                    if (_balanceDays != 1)
                        _balanceDaysMessage = " days balance is " + _balanceDays + " days";
                    _leaveDaysBalanceMessage = "<br>" + employeeGender + " " + leaveType + _balanceDaysMessage + ".<br>";
                }
                //check if the leave type has any there are attachments 
                Attachment leaveAttachmentDocument = null;
                if (getLeave.leave_vbAttachments != null)
                {
                    //get the attached document
                    MemoryStream memoryStream = new MemoryStream();
                    //get leave document details from leaveDocs table
                    LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == getLeave.leave_iLeaveID);
                    string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                    string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                    byte[] file = getLeave.leave_vbAttachments.ToArray(); ;
                    memoryStream.Write(file, 0, file.Length);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    StreamWriter strWriter = new StreamWriter(memoryStream);
                    strWriter.Flush();

                    memoryStream.Position = 0;

                    //get the content type for the file loaded as per the document extension
                    string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                    leaveAttachmentDocument = new Attachment(memoryStream, _documentName, contentType);
                }
                if (db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress.ToString().TrimEnd() != "")
                {
                    managerEmailAddress = db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress;
                    string emailSubject = "Leave Application Alert";
                    string emailBody = "Dear " + managerFirstName + ",<br><br>This is to notify you that " + employeeNames + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + "<br>starting from " + fromDate + " to " + toDate + " and is waiting for your approval.<br>" + _leaveDaysBalanceMessage + "<br>" + _leaveReasonsMessage;
                    //send mail
                    try
                    {
                        //check if the leave type has any there are attachments
                        if (getLeave.leave_vbAttachments != null)
                        {
                            //send an email with an attachment to the approving manager
                            _hrMail.SendEmailWithAnAttachment(managerEmailAddress, emailSubject, emailBody, leaveAttachmentDocument);
                        }
                        else
                        {
                            //send an email to the manager without an attachment
                            _hrMail.SendEmail(managerEmailAddress, emailSubject, emailBody);
                        }
                    }
                    catch { }
                }
                //check if the manager is on leave
                if (db.EmployeesCurrentlyOnLeave_Views.Any(p => p.leave_uiStaffID == managerID))
                ////check if the manager is on leave and if his/her leave days are more than 1 and the date the leave is ending not the current date
                //if (db.EmployeesCurrentlyOnLeave_Views.Any(p => p.leave_uiStaffID == managerID && p.leave_fNoDaysAlloc > 1 && p.leave_dtTill.Value.Date != DateTime.Now.Date))
                {
                    //get leave applicant's next level manager
                    try
                    {
                        //get eslation master for leave application
                        short _escalationMasterID = Convert.ToInt16(_hrClass.ReturnEscalationMasterID());
                        Guid secondApprovalLevelManagerID = (Guid)db.StaffMasters.Single(p => p.StaffID == managerID).ReportingToStaffID;
                        //get next level manager's first name
                        string secondApprovalLevelManagerFirstName = _hrClass.getStaffFirstName(secondApprovalLevelManagerID);
                        //get email address for the second level manager
                        StaffMaster getSecondApprovalLeveldetails = db.StaffMasters.Single(p => p.StaffID == secondApprovalLevelManagerID);
                        string secondApprovalLevelEmailAddress = getSecondApprovalLeveldetails.EmailAddress.ToString();
                        //update the first escalation leave transaction exit time so as to be able to 
                        //create another escalation record to be sent to the next manager so that he/she can be able to approve the leave application
                        //and also the first level manager to be able to approve the leave incase he comes out of leave

                        EscalationTransaction updateStage1EscalatedLeaveTransaction = db.EscalationTransactions.Single(p => p.escalationtransaction_vTransactionNumber == leaveSerialNumber);
                        updateStage1EscalatedLeaveTransaction.escalationtransaction_dtExitDateTime = updateStage1EscalatedLeaveTransaction.escalationtransaction_dtDateTimeCreated;
                        db.SubmitChanges();

                        //insert a new escalation transaction record now at stage 2
                        //get first level escalation time hours
                        int firstLevelEscalationTimeHours = Convert.ToInt32(db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == _escalationMasterID).leaveapprovalproc_iEscalationTimeAprvLv1);

                        //get the third level manager
                        Guid thirdApprovalLevelManagerID = Guid.Empty;
                        //check if the second approval is not the head
                        if (secondApprovalLevelManagerID != Guid.Empty)
                        {
                            thirdApprovalLevelManagerID = (Guid)getSecondApprovalLeveldetails.ReportingToStaffID;
                        }
                        //escalate to the second level
                        _hrClass.InsertRecordIntoEscalationTransactionsTable(_escalationMasterID, leaveSerialNumber, 2, secondApprovalLevelManagerID, firstLevelEscalationTimeHours, false, "Escalated", thirdApprovalLevelManagerID, null);
                        //send a mail to the second approval level manager for the leave that is escalated to
                        string secondApprovalEmailSubject = "Leave Application Alert";
                        string secondApprovalEmailBody = "Dear " + secondApprovalLevelManagerFirstName + ",<br><br>This is to notify you that " + employeeNames + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + " <br>starting from " + fromDate + " to " + toDate + " and is waiting approval.<br>" + _leaveDaysBalanceMessage + "<br>This has been escalated to you because " + employeeGender2 + " manager " + managerFirstName + " is on leave.<br><br>" + _leaveReasonsMessage;
                        //send mail
                        try
                        {
                            //check if the leave application has got any attachments
                            if (getLeave.leave_vbAttachments != null)
                            {
                                //send an email to the second approval with attachments
                                _hrMail.SendEmailWithAnAttachment(secondApprovalLevelEmailAddress, secondApprovalEmailSubject, secondApprovalEmailBody, leaveAttachmentDocument);
                            }
                            else
                            {
                                //send an email to the second approval without an attachment
                                _hrMail.SendEmail(secondApprovalLevelEmailAddress, secondApprovalEmailSubject, secondApprovalEmailBody);
                            }
                        }
                        catch { }

                        //send a mail to the hr informing him that the employee's manager is on leave
                        //get the list of employee to be kept informed always in the leave application escalation process
                        var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_escalationMasterID);

                        //notify all the employee who have to be infomed once a leave application is made
                        foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                        {
                            //get employee to be informed first name
                            string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                            //email to be sent to the staff to be kept informed always
                            string staffToBeInformedEmailSubject = "Leave Application Alert";
                            string staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + "<br>starting from " + fromDate + " to " + toDate + ".<br>" + _leaveDaysBalanceMessage + employeeGender + " manager " + managerFirstName + " is on leave, therefore confirm with the next level manager and approve or disapprove the leave application on their behalf.<br><br>" + _leaveReasonsMessage;
                            //get employee to be informed email address
                            string staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                            //send mail
                            try
                            {
                                //check if the leave type is a sick or day off and there are attachments
                                if (getLeave.leave_vbAttachments != null)
                                {
                                    //send email to the hr with an attachment
                                    _hrMail.SendEmailWithAnAttachment(staffToBeInformedEmailAddress, staffToBeInformedEmailSubject, staffToBeInformedEmailBody, leaveAttachmentDocument);
                                }
                                else
                                {
                                    //send emailto hr without an attachment
                                    _hrMail.SendEmail(staffToBeInformedEmailAddress, staffToBeInformedEmailSubject, staffToBeInformedEmailBody);
                                }
                            }
                            catch { }
                        }

                    }
                    catch { return; }
                }
                //update mail sending transaction affected 
                //UpdateMailSendingTransaction(_mailSendingTransactionID);
            }
            catch { return; }
        }
        //method sending a  leave approval message when a leave application is approved
        //pass the escalation master for the leave, the leave id an the approving manager id
        [WebMethod]
        public void SendLeaveApplicationApprovalEmailNotification(int leaveID, Guid approvingStaffID)//case 2
        {
            try
            {
                //get approved leave application
                Leave getApprovedLeave = db.Leaves.Single(p => p.leave_iLeaveID == leaveID);
                string emailSubject, employeeEmailBody, staffToBeInformedEmailBody, fromDate, toDate, fromEffDate, toEffDate, leaveType, leaveDays, leaveReasons;//get leave details
                emailSubject = "Leave Application Approval Alert";
                if (getApprovedLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getApprovedLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getApprovedLeave.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getApprovedLeave.leave_siLeaveTypeID).leavetype_vName.ToString();
                leaveDays = getApprovedLeave.leave_fNoDaysAlloc.ToString();
                string _leaveDaysMessage = "1 day";
                if (leaveDays != "1")
                    _leaveDaysMessage = leaveDays + " days";
                leaveReasons = getApprovedLeave.leave_vRemarks.Trim();
                string _leaveReasonsMessage = "";
                if (leaveReasons != "")
                    _leaveReasonsMessage = "<br>Reasons for " + leaveType + " application are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul>";
                //get employee who had applied for leave
                Guid leaveApplicantStaffMstID = (Guid)getApprovedLeave.leave_uiStaffID;
                StaffMaster getEmployee = db.StaffMasters.Single(p => p.StaffID == leaveApplicantStaffMstID);
                string employeeNames = getEmployee.StaffName;
                string employeeFirstName = _hrClass.getStaffFirstName(leaveApplicantStaffMstID);//get leave applicant first name
                string employeeNumber = getEmployee.StaffRef.ToString();
                string approvingManagerNames = _hrClass.getStaffNames(approvingStaffID);
                string employeeEmailAddress = getEmployee.EmailAddress.ToString().Trim();
                string genderPossesion = "His";//
                if (getEmployee.Gender == "Female")
                {
                    genderPossesion = "Her";
                }
                string _leaveDaysBalanceMessageForApplicant = "", _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "";
                //check if the leave balance for the leve type is to be calculated
                if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getApprovedLeave.leave_siLeaveTypeID).leavetype_bCalculateLeaveBalance == true)
                {
                    //get leave status totals for the leave approved after it is completed
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(leaveApplicantStaffMstID, getApprovedLeave.leave_siLeaveTypeID).Single();
                    //get leave days balance after leave is completed
                    double _balanceDays = 0;
                    _balanceDays = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed));
                    string _balanceDaysMessage = "1 day.";
                    if (_balanceDays != 1)
                        _balanceDaysMessage = _balanceDays + " days.";
                    //create a meassage
                    _leaveDaysBalanceMessageForApplicant = "<br><br>Your " + leaveType + " balance after the leave is completed will be " + _balanceDaysMessage;
                    _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "<br><br>" + genderPossesion + " " + leaveType + " balance upon completion of the leave will be " + _balanceDaysMessage;
                }
                //email to be send to the leave applicant
                employeeEmailBody = "Dear  " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + " you had earlier applied for from " + fromDate + " to " + toDate + " for a period of " + _leaveDaysMessage + " has been approved by " + approvingManagerNames + "." + _leaveDaysBalanceMessageForApplicant;

                //get escalation master  id for leave application
                short _escalationMasterID = Convert.ToInt16(_hrClass.ReturnEscalationMasterID());
                //get the list of employee to be kept informed always
                var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_escalationMasterID);

                //check if the leave has any attached document
                if (getApprovedLeave.leave_vbAttachments == null)//if itz null, then send an email without the atachmemnt
                {
                    //check if the leave applicant has an emeil address, and send an email if he/she has one
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmail(employeeEmailAddress, emailSubject, employeeEmailBody);
                        }
                        catch { }
                    }
                    //notify all the employee who have to be infomed once a leave approval is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + ", of staff number " + employeeNumber + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + " and has been approved by " + approvingManagerNames + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br>" + _leaveReasonsMessage;
                        //get employee to be informed email address
                        string staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        //send a mail to the employee to be kept informed
                        try
                        {
                            _hrMail.SendEmail(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody);
                        }
                        catch { }
                    }
                }
                else//if the leave has an attached document
                {
                    //get the attached document
                    MemoryStream memoryStream = new MemoryStream();
                    //get leave document details from leaveDocs table
                    LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == leaveID);
                    string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                    string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                    byte[] file = getApprovedLeave.leave_vbAttachments.ToArray(); ;
                    memoryStream.Write(file, 0, file.Length);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    StreamWriter strWriter = new StreamWriter(memoryStream);
                    strWriter.Flush();

                    memoryStream.Position = 0;

                    //get the content type for the file loaded as per the document extension
                    string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                    Attachment netAttachment = new Attachment(memoryStream, _documentName, contentType);

                    //send an email with an attchment to the employee who applied for the leave if he/she has an email address
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(employeeEmailAddress, emailSubject, employeeEmailBody, netAttachment);
                        }
                        catch { }
                    }
                    //notify all the staff who have to be infomed once a leave approval is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + employeeNames + ", of staff number " + employeeNumber + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + " and has been approved by " + approvingManagerNames + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br>" + _leaveReasonsMessage;
                        //get employee to be informed email address
                        string staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody, netAttachment);
                        }
                        catch { }
                    }
                }
            }
            catch { return; }
        }
        // web method for sending leave cancelation notification to the employee who had applied for a leave that has been cancelled
        [WebMethod]
        public void SendLeaveNotificationCancellationEmailAlert(int LeaveID)//case 3
        {
            try
            {
                //get leave details
                Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == LeaveID);
                string fromDate, toDate, fromEffDate, toEffDate, leaveType;//get leave details
                if (getLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = getLeave.leavetype_vName;
                string leavePeriodDays = getLeave.leave_fNoDaysAlloc.ToString();
                string _leavePeriodDaysMessage = "1 day";
                if (leavePeriodDays != "")
                    _leavePeriodDaysMessage = leavePeriodDays + " days";
                string _leaveCancellationReasons = getLeave.leave_vCancellationReason.Trim();

                //get employee who hadd applied for the leave
                Guid _leaveApplicantID = (Guid)getLeave.leave_uiStaffID;
                StaffMaster getLeaveApplicant = db.StaffMasters.Single(p => p.StaffID == _leaveApplicantID);
                //string employeeNames = getLeaveApplicant.staffmst_vStaffName;
                string employeeNumber = getLeaveApplicant.StaffRef.ToString();
                // string leaveApplicantManager = getLeaveApplicant.staffmst_vStaffManager;
                //get leave applicant's company email address
                string leaveApplicantEmaillAddress = string.Empty;
                //get employee's first name
                string employeeFirstName = _hrClass.getStaffFirstName(_leaveApplicantID);
                //get employee's manager first name
                // string managerFirstName = _hrClass.getEmployeeFirstName((int)getLeaveApplicant.staffmst_iManager);
                string cancellingManagerName = _hrClass.getStaffNames((Guid)getLeave.leave_uiCancelledByStaffID);

                if (getLeaveApplicant.EmailAddress.ToString().Trim() != "")
                {
                    leaveApplicantEmaillAddress = getLeaveApplicant.EmailAddress.ToString();
                    string emailSubject = "Leave Application Not Approved Alert";
                    string emailBody = "Dear " + employeeFirstName + ",<br><br>This is to notify you that the " + leaveType + "  that you had earlier applied for from " + fromDate + " to " + toDate + ",for a period of " + _leavePeriodDaysMessage + ", has not been approved by " + cancellingManagerName + ".<br><br>The reasons cited for not approving are:<br><ul " + _hrMail.emailBodyCssStyle + "><li>" + _leaveCancellationReasons + "</li></ul><br>";
                    try
                    {
                        _hrMail.SendEmail(leaveApplicantEmaillAddress, emailSubject, emailBody);
                    }
                    catch { }
                    //update mail sending transaction affected 
                    //  UpdateMailSendingTransaction(_mailSendingTransactionID);
                }
                else//no email address
                {
                    //update mail sending transaction affected 
                    //UpdateMailSendingTransaction(_mailSendingTransactionID);
                }
            }
            catch { }
        }
        //webmethod for sending a leave application details that have been updated by a manager to the leave applicant
        [WebMethod]
        public void SendLeaveDetailsUpdatedByManagerToLeaveApplicant(int LeaveID, Guid leaveEditedByID)//case 4
        {
            try
            {
                //get leave application details
                Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == LeaveID);
                //get leave applicant id
                Guid _leaveApplicantID = (Guid)getLeave.leave_uiStaffID;
                StaffMaster getLeaveApplicantDetails = db.StaffMasters.Single(p => p.StaffID == _leaveApplicantID);
                string leaveApplicantEmailAddress = getLeaveApplicantDetails.EmailAddress.ToString().Trim();
                //check if the leave applicant has an email addresss
                if (leaveApplicantEmailAddress != "")
                {
                    string _leaveDateFrom = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " Afternoon";
                    string _leaveDateTo = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " Afternoon";
                    if (getLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                    {
                        _leaveDateFrom = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " Morning";
                    }
                    if (getLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                    {
                        _leaveDateTo = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " Morning";
                    }
                    string leaveType = getLeave.leavetype_vName;
                    string leaveApplicantFirstName = _hrClass.getStaffFirstName(_leaveApplicantID);
                    string leaveUpdatedByEmployeeNames = _hrClass.getStaffNames(leaveEditedByID);

                    string leaveDateCreated = getLeave.leave_dtDateCreated.Value.ToLongDateString();
                    string leaveTimeCreated = getLeave.leave_dtDateCreated.Value.ToShortTimeString();
                    string leaveSerialNo = getLeave.leave_vSerialNumber;
                    string daysAllocated = getLeave.leave_fNoDaysAlloc.ToString();
                    string leaveReasons = getLeave.leave_vRemarks;
                    string emailSubject = "Leave Application Details Changed Alert";
                    string emailBody = "Dear " + leaveApplicantFirstName + ",<br><br>This is to notify you that the " +
                        "leave application you had earlier made on " + leaveDateCreated + " at " + leaveTimeCreated + ",<br>" +
                        "with Serial Number: " + leaveSerialNo + " details have been changed by " + leaveUpdatedByEmployeeNames +
                        "<br>The leave details as changed are :<ul><li>Leave Type : " + leaveType + "</li><br><li>Leave From Date : " + _leaveDateFrom + "</li><br><li>" +
                        "Leave Till Date : " + _leaveDateTo + "</li><br><li>Days Allocated : " + daysAllocated + "</li><br><li>Reasons : " + leaveReasons + "</li></ul> ";
                    //check if the leave type has any there are attachments 
                    Attachment leaveAttachmentDocument = null;
                    if (getLeave.leave_vbAttachments != null)
                    {
                        //get the attached document
                        MemoryStream memoryStream = new MemoryStream();
                        //get leave document details from leaveDocs table
                        LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == getLeave.leave_iLeaveID);
                        string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                        string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                        byte[] file = getLeave.leave_vbAttachments.ToArray(); ;
                        memoryStream.Write(file, 0, file.Length);
                        memoryStream.Seek(0, SeekOrigin.Begin);

                        StreamWriter strWriter = new StreamWriter(memoryStream);
                        strWriter.Flush();

                        memoryStream.Position = 0;

                        //get the content type for the file loaded as per the document extension
                        string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                        leaveAttachmentDocument = new Attachment(memoryStream, _documentName, contentType);
                    }
                    //send the email to the leave applicant

                    try
                    {
                        //check if there is a aleave attachment for the leave application
                        if (getLeave.leave_vbAttachments != null)
                            //send mail with an attachament
                            _hrMail.SendEmailWithAnAttachment(leaveApplicantEmailAddress, emailSubject, emailBody, leaveAttachmentDocument);
                        else //send mail without an attachment
                            _hrMail.SendEmail(leaveApplicantEmailAddress, emailSubject, emailBody);
                    }
                    catch { }
                    //update mail sending transaction affected 
                    //UpdateMailSendingTransaction(_mailSendingTransactionID);
                }
                else//no email address
                {
                    //update mail sending transaction affected 
                    // UpdateMailSendingTransaction(_mailSendingTransactionID);
                }
            }
            catch { }
        }
        //webmethod for sending a mail to the approving manager when a leave applicant has update the leave details
        [WebMethod]
        public void SendLeaveDetailsUpdatedByLeaveApplicantToApprovingManager(int LeaveID)//case 5
        {
            try
            {
                //get leave application details
                Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == LeaveID);
                string _leaveDateFrom = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " Afternoon";
                string _leaveDateTo = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " Afternoon";
                if (getLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    _leaveDateFrom = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " Morning";
                }
                if (getLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    _leaveDateTo = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " Morning";
                }
                string leaveType = getLeave.leavetype_vName;

                string leaveDateCreated = getLeave.leave_dtDateCreated.Value.ToLongDateString();
                string leaveTimeCreated = getLeave.leave_dtDateCreated.Value.ToShortTimeString();
                string leaveSerialNo = getLeave.leave_vSerialNumber;
                string daysAllocated = getLeave.leave_fNoDaysAlloc.ToString();
                string leaveReasons = getLeave.leave_vRemarks;

                //check if the leave type has any there are attachments 
                Attachment leaveAttachmentDocument = null;
                if (getLeave.leave_vbAttachments != null)
                {
                    //get the attached document
                    MemoryStream memoryStream = new MemoryStream();
                    //get leave document details from leaveDocs table
                    LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == getLeave.leave_iLeaveID);
                    string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                    string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                    byte[] file = getLeave.leave_vbAttachments.ToArray(); ;
                    memoryStream.Write(file, 0, file.Length);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    StreamWriter strWriter = new StreamWriter(memoryStream);
                    strWriter.Flush();

                    memoryStream.Position = 0;

                    //get the content type for the file loaded as per the document extension
                    string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                    leaveAttachmentDocument = new Attachment(memoryStream, _documentName, contentType);
                }

                //get leave applicant details
                Guid _leaveApplicantID = (Guid)getLeave.leave_uiStaffID;
                StaffMaster getLeaveApplicant = db.StaffMasters.Single(p => p.StaffID == _leaveApplicantID);
                string leaveApplicantNames = getLeaveApplicant.StaffName;
                //employee gender
                string employeeGender = "His";//set his to be the default
                if (getLeaveApplicant.Gender == "Female")
                {
                    employeeGender = "Her";
                }
                //get leave days balance
                double _balanceDays = 0;
                string _leaveDaysBalanceMessage = "";
                //check if the leave type balance is to be calculated
                if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeave.leave_siLeaveTypeID).leavetype_bCalculateLeaveBalance == true)
                {
                    //get leave status totals for the leave being applied for
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(_leaveApplicantID, getLeave.leave_siLeaveTypeID).Single();
                    _balanceDays = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed));
                    string _balanceDaysMessage = "1 day.";
                    if (_balanceDays != 1)
                        _balanceDaysMessage = _balanceDays + " days.";
                    _leaveDaysBalanceMessage = employeeGender + " " + leaveType + " days balance is " + _balanceDaysMessage + "<br>";
                }
                //send the email to the approving manager for the leave in the escalation transactions
                foreach (EscalationTransaction _getEscalationTransaction in db.EscalationTransactions.Where(p => p.escalationtransaction_vTransactionNumber.Trim() == leaveSerialNo.Trim()))
                {
                    string emailSubject = "Leave Application Details Changed Alert";
                    Guid approvingManagerID = (Guid)_getEscalationTransaction.escalationtransaction_uiApprovalStaffID;
                    string approvingManagerFirstName = _hrClass.getStaffFirstName(approvingManagerID);
                    string approvingManagerEmailAddress = _hrClass.getStaffEmailAddress(approvingManagerID);

                    //check if the email i not null
                    if (approvingManagerEmailAddress != "")
                    {
                        string emailBody = "Dear " + approvingManagerFirstName + ",<br><br>This is to notify you that " + leaveApplicantNames +
                            " has modified details for a leave application earlier made on " + leaveDateCreated + " at " + leaveTimeCreated + ".<br>" +
                            "<br>The leave details as changed are :<ul style='color: black; font-family: Calibri Light; font-size:16px; '><li>Leave Type : " + leaveType + "</li><br><li>Leave From Date : " + _leaveDateFrom + "</li><br><li>" +
                            "Leave Till Date : " + _leaveDateTo + "</li><br><li>Days Allocated : " + daysAllocated + "</li><br><li>Reasons : " + leaveReasons + "</li></ul><br>" +
                          "<p " + _hrMail.emailBodyCssStyle + ">" + _leaveDaysBalanceMessage + "The leave application is awaiting approval.</p>";
                        try
                        {
                            //check if there is a a leave attachment for the leave application
                            if (getLeave.leave_vbAttachments != null)
                                //send mail with an attachament
                                _hrMail.SendEmailWithAnAttachment(approvingManagerEmailAddress, emailSubject, emailBody, leaveAttachmentDocument);
                            else //send mail without an attachment
                                _hrMail.SendEmail(approvingManagerEmailAddress, emailSubject, emailBody);
                        }
                        catch { }
                    }
                }
                //update mail sending transaction affected 
                //UpdateMailSendingTransaction(_mailSendingTransactionID);
            }
            catch
            {
                //_hrClass.SendEmail("dickson@adeptsystems.co.ke", "Mail sending error", ex.Message.ToString());
            }
        }
        //webmethod for sending a leave deleted notification email to the employee's manager
        [WebMethod]
        public void SendLeaveDeletedNotificationEmailToApprovingManager(int LeaveID)//case 6
        {
            try
            {
                //get leave details
                Leave_View getLeave = db.Leave_Views.Single(p => p.leave_iLeaveID == LeaveID);
                string fromDate, toDate, fromEffDate, toEffDate, leaveType, leaveDays;//get leave details
                if (getLeave.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeave.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getLeave.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getLeave.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeave.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = getLeave.leavetype_vName;
                leaveDays = getLeave.leave_fNoDaysAlloc.ToString();
                string _leaveDaysMessage = "1 day";
                if (leaveDays != "1")
                    _leaveDaysMessage = leaveDays + " days";
                string leaveSerialNo = getLeave.leave_vSerialNumber;
                string leaveDeleteReasons = getLeave.leave_vRemovalReasons;

                //get employee
                Guid _leaveApplicantID = (Guid)getLeave.leave_uiStaffID;
                StaffMaster getLeaveApplicant = db.StaffMasters.Single(p => p.StaffID == _leaveApplicantID);
                string leaveApplicantNames = getLeaveApplicant.StaffName;

                string emailSubject = "Leave Application Deleted Alert";
                //send the email to the approving manager for the leave in the escalation transactions
                foreach (EscalationTransaction _getEscalationTransaction in db.EscalationTransactions.Where(p => p.escalationtransaction_vTransactionNumber.Trim() == leaveSerialNo.Trim()))
                {
                    //get approving manager
                    Guid approvingManagerID = (Guid)_getEscalationTransaction.escalationtransaction_uiApprovalStaffID;
                    string approvingManagerFirstName = _hrClass.getStaffFirstName(approvingManagerID);
                    //get manager's company email address
                    string approvingManagerEmaillAddress = string.Empty;
                    approvingManagerEmaillAddress = _hrClass.getStaffEmailAddress(approvingManagerID);
                    //check if there is an email address for the approving manager
                    if (approvingManagerEmaillAddress != "")
                    {
                        string emailBody = "Dear " + approvingManagerFirstName + ",<br><br>This is to notify you that " + leaveApplicantNames + " who had earlier applied for " + leaveType + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + " has deleted the leave application. In that case, you do not need to approve the leave application.<br><br>Delete reasons cited are:<br><ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveDeleteReasons + "</li></ul><br>";
                        //send mail to the employee manager
                        try
                        {
                            _hrMail.SendEmail(approvingManagerEmaillAddress, emailSubject, emailBody);
                        }
                        catch { }
                    }
                }
                //update mail sending transaction affected 
                //UpdateMailSendingTransaction(_mailSendingTransactionID);
            }
            catch { }
        }
        ////webmethod for sending a mail alert when a leave application is made vy a staff to the to the leave applicant,applicant's manger and notify the HR staff to be informed
        [WebMethod]
        public void SendLeaveApplicationMadeByStaffAndRequireNoApprovalEmailNotification(int LeaveID, Guid leaveCreatedByStaffID)//case 7
        {
            try
            {
                //send email to the hr informing them that  a leave has been approved
                string staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody, fromDate, toDate, fromEffDate, toEffDate, leaveType, leaveDays, leaveReasons;//get leave details
                short _leaveEscalationMasterID = Convert.ToInt16(_hrClass.ReturnEscalationMasterID());//get leave escalation transation master
                Leave getLeaveApplicationMade = db.Leaves.Single(p => p.leave_iLeaveID == LeaveID);
                if (getLeaveApplicationMade.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getLeaveApplicationMade.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeaveApplicationMade.leave_siLeaveTypeID).leavetype_vName.ToString();
                leaveDays = getLeaveApplicationMade.leave_fNoDaysAlloc.ToString();
                string _leaveDaysMessage = "1 day";
                if (leaveDays != "1")
                    _leaveDaysMessage = leaveDays + " days";
                leaveReasons = getLeaveApplicationMade.leave_vRemarks;
                //get employee who had applied for leave
                Guid leaveApplicantStaffMstID = (Guid)getLeaveApplicationMade.leave_uiStaffID;
                StaffMaster getEmployee = db.StaffMasters.Single(p => p.StaffID == leaveApplicantStaffMstID);
                string employeeNames = getEmployee.StaffName;
                string employeeFirstName = _hrClass.getStaffFirstName(leaveApplicantStaffMstID);//get leave applicant first name
                string employeeNumber = getEmployee.StaffRef.ToString();
                string employeeEmailAddress = getEmployee.EmailAddress.ToString().Trim();
                string genderPossesion = "His";//gender position 2 for starting a sentence with a capital letter
                if (getEmployee.Gender == "Female")
                {
                    genderPossesion = "Her";
                }
                //get employee who made the leave application
                string leaveApplicationMadeBy = _hrClass.getStaffNames(leaveCreatedByStaffID);
                string _leaveDaysBalanceMessageForApplicant = "", _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "";
                //get manager id
                Guid managerID = (Guid)getEmployee.ReportingToStaffID;
                //get manager's first name
                string managerFirstName = _hrClass.getStaffFirstName(managerID);
                //get manager's company email address
                string managerEmailAddress = string.Empty;
                //check  if the leave type for the leave application is to be calculated
                if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeaveApplicationMade.leave_siLeaveTypeID).leavetype_bCalculateLeaveBalance == true)
                {
                    //get leave status totals for the leave approved after it is completed
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(leaveApplicantStaffMstID, getLeaveApplicationMade.leave_siLeaveTypeID).Single();
                    //get leave days balance after leave is completed
                    double _balanceDays = 0;
                    _balanceDays = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed));
                    string _balanceDaysMessage = "1 day.";
                    if (_balanceDays != 1)
                        _balanceDaysMessage = _balanceDays + " days.";
                    //create a meassage
                    _leaveDaysBalanceMessageForApplicant = "<br>Your " + leaveType + " balance after the leave is completed will be " + _balanceDaysMessage;

                    _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "<br>" + genderPossesion + " " + leaveType + " balance upon completion of the leave will be " + _balanceDays + " days.";
                }
                emailSubject = getLeaveApplicationMade.LeaveType.leavetype_vName + " Leave Application Alert";
                string _emailNote = "<p " + _hrMail.emailBodyCssStyle + ">Note: This type of leave doesn't require any approval as it has been automatically approved.</p><br>";
                string employeeEmailBody = "Dear  " + employeeFirstName + ",<br><br>This is to notify you that you have made a " + leaveType + " application from " + fromDate + " to " + toDate + " for a period of " + _leaveDaysMessage + "." + _leaveDaysBalanceMessageForApplicant + "<br>" + _emailNote;

                //check if the leave has any attached document
                if (getLeaveApplicationMade.leave_vbAttachments == null)//if itz null, then send an email without the atachmemnt
                {
                    //send mail to the employees to be informed
                    //get the list of employee to be kept informed always in the leave application escalation process
                    var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_leaveEscalationMasterID);

                    //notify all the employee who have to be infomed once a leave application is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + ", employee number " + employeeNumber + ", has made " + leaveType + " application for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons for " + leaveType + " application cited are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul>" + _emailNote;
                        //get employee to be informed email address
                        staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        //send the mail to employees to be informed
                        try
                        {
                            _hrMail.SendEmail(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody);
                        }
                        catch { }
                    }
                    //email body to be sent to the employee who applied for the leave after the leave has been approved
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmail(employeeEmailAddress, emailSubject, employeeEmailBody);
                        }
                        catch { }
                    }
                    //send mail alert to the manager

                    if (db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress.ToString().TrimEnd() != "")
                    {
                        managerEmailAddress = db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress;
                        string managerEmailBody = "Dear " + managerFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + " ." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons cited for " + leaveType + " application are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul>" + _emailNote;
                        //send mail to the manager
                        try
                        {
                            //send an email to the manager without an attachment
                            _hrMail.SendEmail(managerEmailAddress, emailSubject, managerEmailBody);
                        }
                        catch { }
                    }
                }
                else
                {
                    //get the attached document
                    MemoryStream memoryStream = new MemoryStream();
                    //get leave document details from leaveDocs table
                    LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == getLeaveApplicationMade.leave_iLeaveID);
                    string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                    string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                    byte[] file = getLeaveApplicationMade.leave_vbAttachments.ToArray(); ;
                    memoryStream.Write(file, 0, file.Length);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    StreamWriter strWriter = new StreamWriter(memoryStream);
                    strWriter.Flush();

                    memoryStream.Position = 0;

                    //get the content type for the file loaded as per the document extension
                    string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                    Attachment netAttachment = new Attachment(memoryStream, _documentName, contentType);

                    //get the list of employee to be kept informed always in the leave application escalation process
                    var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_leaveEscalationMasterID);

                    //notify all the employee who have to be infomed once a leave application is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + " employee number " + employeeNumber + ", has made a " + leaveType + " application for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons for " + leaveType + " application cited are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul>" + _emailNote;
                        //get employee to be informed email address
                        staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        //send the mail to employees to be informed
                        //send an email with an attchment
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody, netAttachment);
                        }
                        catch { }
                    }
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(employeeEmailAddress, emailSubject, employeeEmailBody, netAttachment);
                        }
                        catch { }
                    }
                    //send mail alert to the employee's manager
                    if (db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress.ToString().TrimEnd() != "")
                    {
                        managerEmailAddress = db.StaffMasters.Single(p => p.StaffID == managerID).EmailAddress;
                        string managerEmailBody = "Dear " + managerFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + " has applied for " + leaveType + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + "<br>" + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons cited for " + leaveType + " application are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul>" + _emailNote;
                        //send mail to the manager
                        try
                        {
                            //send an email to the manager with an attachment
                            _hrMail.SendEmailWithAnAttachment(managerEmailAddress, emailSubject, managerEmailBody, netAttachment);
                        }
                        catch { }
                    }
                }
            }
            catch { return; }
        }
        ////webmethod for sending a mail alert when a leave application is made by the HR to the leave applicant and notify the HR staff to be informed
        [WebMethod]
        public void SendLeaveApplicationMadeByHREmailNotification(int LeaveID, Guid leaveCreatedByStaffID)//case 8
        {
            try
            {
                //send email to the hr informing them that  a leave has been approved
                string staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody, fromDate, toDate, fromEffDate, toEffDate, leaveType, leaveDays, leaveReasons;//get leave details
                short _leaveEscalationMasterID = Convert.ToInt16(_hrClass.ReturnEscalationMasterID());//get leave escalation transation master
                Leave getLeaveApplicationMade = db.Leaves.Single(p => p.leave_iLeaveID == LeaveID);
                if (getLeaveApplicationMade.leave_cEffFromSess.ToString() == "M")//if session begins in the morning
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtFrom.ToString()) + " AM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffFrom.ToString()) + " AM";
                }
                else
                {
                    fromDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtFrom.ToString()) + " PM";
                    fromEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffFrom.ToString()) + " PM";
                }
                if (getLeaveApplicationMade.leave_cEffTillSess.ToString() == "M")//if session ends in the morning
                {
                    toDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtTill.ToString()) + " AM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffTill.ToString()) + " AM";
                }
                else
                {
                    toDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtTill.ToString()) + " PM";
                    toEffDate = _hrClass.ShortDateDayStart(getLeaveApplicationMade.leave_dtEffTill.ToString()) + " PM";
                }
                leaveType = db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeaveApplicationMade.leave_siLeaveTypeID).leavetype_vName.ToString();
                leaveDays = getLeaveApplicationMade.leave_fNoDaysAlloc.ToString();
                string _leaveDaysMessage = "1 day";
                if (leaveDays != "1")
                    _leaveDaysMessage = leaveDays + " days";
                leaveReasons = getLeaveApplicationMade.leave_vRemarks;
                //get employee who had applied for leave
                Guid leaveApplicantStaffMstID = (Guid)getLeaveApplicationMade.leave_uiStaffID;
                StaffMaster getEmployee = db.StaffMasters.Single(p => p.StaffID == leaveApplicantStaffMstID);
                string employeeNames = getEmployee.StaffName;
                string employeeFirstName = _hrClass.getStaffFirstName(leaveApplicantStaffMstID);//get leave applicant first name
                string employeeNumber = getEmployee.StaffRef.ToString();
                string employeeEmailAddress = getEmployee.EmailAddress.ToString().Trim();
                string genderPossesion = "His";//gender position 2 for starting a sentence with a capital letter
                if (getEmployee.Gender == "Female")
                {
                    genderPossesion = "Her";
                }
                //get employee who made the leave application
                string leaveApplicationMadeBy = _hrClass.getStaffNames(leaveCreatedByStaffID);
                string _leaveDaysBalanceMessageForApplicant = "", _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "";
                //check  if the leave type for the leave application is to be calculated
                if (db.LeaveTypes.Single(p => p.leavetype_siLeaveTypeID == getLeaveApplicationMade.leave_siLeaveTypeID).leavetype_bCalculateLeaveBalance == true)
                {
                    //get leave status totals for the leave approved after it is completed
                    PROC_SumLeaveStatusResult _sumLeaveDays = db.PROC_SumLeaveStatus(leaveApplicantStaffMstID, getLeaveApplicationMade.leave_siLeaveTypeID).Single();
                    //get leave days balance after leave is completed
                    double _balanceDays = 0;
                    _balanceDays = (Convert.ToDouble(_sumLeaveDays.approved_earned) - Convert.ToDouble(_sumLeaveDays.approved_consumed));
                    string _balanceDaysMessage = "1 day.";
                    if (_balanceDays != 1)
                        _balanceDaysMessage = _balanceDays + " days.";
                    //create a meassage
                    _leaveDaysBalanceMessageForApplicant = "<br>Your " + leaveType + " balance after the leave is completed will be " + _balanceDaysMessage;
                    _leaveDaysBalanceMessageForEmployeeBiengKeptInform = "<br>" + genderPossesion + " " + leaveType + " balance upon completion of the leave will be " + _balanceDaysMessage;
                }
                emailSubject = "Leave Application Made Alert";
                string employeeEmailBody = "Dear  " + employeeFirstName + ",<br><br>This is to notify you that " + leaveType + " application has been made from " + fromDate + " to " + toDate + " for a period of " + _leaveDaysMessage + " by " + leaveApplicationMadeBy + "." + _leaveDaysBalanceMessageForApplicant;

                //check if the leave has any attached document
                if (getLeaveApplicationMade.leave_vbAttachments == null)//if itz null, then send an email without the atachmemnt
                {
                    //send mail to the employees to be informed
                    //get the list of employee to be kept informed always in the leave application escalation process
                    var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_leaveEscalationMasterID);

                    //notify all the employee who have to be infomed once a leave application is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + " has made a " + leaveType + " application on behalf of<br>" + employeeNames + ", employee number " + employeeNumber + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons for " + leaveType + " application cited are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul><br>";
                        //get employee to be informed email address
                        staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        //send the mail to employees to be informed
                        try
                        {
                            _hrMail.SendEmail(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody);
                        }
                        catch { }
                    }
                    //email body to be sent to the employee who applied for the leave after the leave has been approved
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmail(employeeEmailAddress, emailSubject, employeeEmailBody);
                        }
                        catch { }
                    }
                }
                else
                {
                    //get the attached document
                    MemoryStream memoryStream = new MemoryStream();
                    //get leave document details from leaveDocs table
                    LeaveDoc getLeaveDocDetails = db.LeaveDocs.Single(p => p.leavedoc_iLeaveID == getLeaveApplicationMade.leave_iLeaveID);
                    string _docExtension = getLeaveDocDetails.leavedoc_vDocumentExtension;
                    string _documentName = getLeaveDocDetails.leavedoc_vDocumentName;

                    byte[] file = getLeaveApplicationMade.leave_vbAttachments.ToArray(); ;
                    memoryStream.Write(file, 0, file.Length);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    StreamWriter strWriter = new StreamWriter(memoryStream);
                    strWriter.Flush();

                    memoryStream.Position = 0;

                    //get the content type for the file loaded as per the document extension
                    string contentType = _hrClass.ReturnDocumentContentType(_docExtension);
                    Attachment netAttachment = new Attachment(memoryStream, _documentName, contentType);

                    //get the list of employee to be kept informed always in the leave application escalation process
                    var employeesToBeInfomed = GetEmployeesToKeepInformedAlwaysInEscalationProcess(_leaveEscalationMasterID);

                    //notify all the employee who have to be infomed once a leave application is made
                    foreach (Guid _staffToBeInformedID in employeesToBeInfomed)
                    {
                        //get employee to be informed first name
                        string staffTobeInformedFirstName = _hrClass.getStaffFirstName(_staffToBeInformedID);
                        //email to be sent to the staff ton be kept informed
                        staffToBeInformedEmailBody = "Dear " + staffTobeInformedFirstName + ",<br><br>This is to notify you that " + leaveApplicationMadeBy + " has made a " + leaveType + " application on behalf of<br>" + employeeNames + ", employee number " + employeeNumber + " for a period of " + _leaveDaysMessage + " starting from " + fromDate + " to " + toDate + "." + _leaveDaysBalanceMessageForEmployeeBiengKeptInform + "<br><br>Reasons for leave application are:<ul " + _hrMail.emailBodyCssStyle + "><li>" + leaveReasons + "</li></ul><br>";
                        //get employee to be informed email address
                        staffToBeInformedEmailAddress = db.StaffMasters.Single(p => p.StaffID == _staffToBeInformedID).EmailAddress.ToString().Trim();
                        //send the mail to employees to be informed
                        //send an email with an attchment
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(staffToBeInformedEmailAddress, emailSubject, staffToBeInformedEmailBody, netAttachment);
                        }
                        catch { }
                    }
                    if (employeeEmailAddress != "")//check if there is an email for the employee
                    {
                        try
                        {
                            _hrMail.SendEmailWithAnAttachment(employeeEmailAddress, emailSubject, employeeEmailBody, netAttachment);
                        }
                        catch { }
                    }
                }
            }
            catch { return; }
        }
        //webmethod for sending email notification from a service
        [WebMethod]
        public void RunHRManagerSendingMailTransactionProcess()
        {
            //get all transaction in the sending mail transaction table that have not been closed
            foreach (MailSendingTransaction getMailSendingTransaction in db.MailSendingTransactions.Where(p => p.mailsendingtransaction_bIsClosed == false))
            {
                int _sendingCase = Convert.ToInt32(getMailSendingTransaction.mailsendingtransaction_siSendingCase);
                int _transactingID = Convert.ToInt32(getMailSendingTransaction.mailsendingtransaction_iTransactingID);//get transatcing id
                int? _2ndTransactingID = getMailSendingTransaction.mailsendingtransaction_i2ndTransactingID;//get second transacting id whic can be null
                int _mailSendingTransactionID = Convert.ToInt32(getMailSendingTransaction.mailsendingtransaction_iMailSendingTransactionID);
                Guid _mailSendingTransactingStaffID = (Guid)getMailSendingTransaction.mailsendingtransaction_uiTransactingStaffID;
                DateTime? _dateTransactionCreated = getMailSendingTransaction.mailsendingtransaction_dtDateCreated;//get the date the transation was created
                switch (_sendingCase)
                {
                    case 1: //case 1 for sending a leave application to the approving manager
                        SendLeaveApplicationNotificationEmailToApprovingManager(_transactingID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 2://case 2 for sending a leave approval notification mail (to the leave applicant end employees to be notified)
                        SendLeaveApplicationApprovalEmailNotification(_transactingID, _mailSendingTransactingStaffID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 3://case 3 for sending a leave cancellation mail alert to the leave applicant
                        SendLeaveNotificationCancellationEmailAlert(_transactingID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 4://case 4 for sending alert mail to an employee when a leave application has been edited by a manager
                        SendLeaveDetailsUpdatedByManagerToLeaveApplicant(_transactingID, _mailSendingTransactingStaffID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 5://case 5 for sending an  alert mail to the approving manager incase a leave applicant edits the leave details
                        SendLeaveDetailsUpdatedByLeaveApplicantToApprovingManager(_transactingID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 6://case 6 for sending an alert mail to the approving manager incase an employee deletes a leave application
                        SendLeaveDeletedNotificationEmailToApprovingManager(_transactingID);
                        //update mail sending transaction affected 
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 7://case 7 for sending a mail alert notifivation to the leave applicat,employee's manager and the HR to be kept informed when the leave doe not require any approval
                        SendLeaveApplicationMadeByStaffAndRequireNoApprovalEmailNotification(_transactingID, _mailSendingTransactingStaffID);
                        //update the affected mail sending transaction record
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    case 8://case 8 for a sending a mail alert an employee when a leave application is made by the hr plus informing the HR person who is s to be notyfied when  aleave apllication is made
                        SendLeaveApplicationMadeByHREmailNotification(_transactingID, _mailSendingTransactingStaffID);
                        //update the affected mail sending transaction record
                        UpdateMailSendingTransaction(_mailSendingTransactionID);
                        break;
                    //case 13://case 13 for sending mail alerts to a leave applicant when a leave application has been kept on hold by a manager
                    //    SendLeaveApplicationKeptOnHoldEmailNotification(_transactingID);
                    //    //update mail sending transaction affected 
                    //    UpdateMailSendingTransaction(_mailSendingTransactionID);
                    //    break;


                    default:
                        break;
                }
            }
        }
        [WebMethod]
        public List<Guid> GetEmployeesToKeepInformedInEscalationProcess(short escalationMasterID)
        {
            var _inform = new List<Guid>();
            //get the esclation master details
            LeaveApprovalProcess getEscalationMaster = db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == escalationMasterID);
            //get keep staff one informed
            _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff1Informed);
            //check if there is keep staff two informed
            if (getEscalationMaster.leaveapprovalproc_uiKeepStaff2Informed != null)
            {
                _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff2Informed);
            }
            //check if there is keep staff three informed
            if (getEscalationMaster.leaveapprovalproc_uiKeepStaff3Informed != null)
            {
                _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff3Informed);
            }

            return _inform;
        }
        //method for getting employees to be informed (ALWAYS) in an esclation process depending on the escalation master
        [WebMethod]
        public List<Guid> GetEmployeesToKeepInformedAlwaysInEscalationProcess(short escalationMasterID)
        {
            var _inform = new List<Guid>();
            //get the esclation master details
            LeaveApprovalProcess getEscalationMaster = db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == escalationMasterID);
            //get keep staff one informed
            if (getEscalationMaster.leaveapprovalproc_vKeepStaff1InformedDuration.ToLower().Contains("always"))
            {
                _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff1Informed);
            }
            //check if there is keep staff two informed
            if (getEscalationMaster.leaveapprovalproc_uiKeepStaff2Informed != null && getEscalationMaster.leaveapprovalproc_vKeepStaff2InformedDuration.ToLower().Contains("always"))
            {
                _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff1Informed);
            }
            //check if there is keep staff three informed
            if (getEscalationMaster.leaveapprovalproc_uiKeepStaff3Informed != null && getEscalationMaster.leaveapprovalproc_vKeepStaff3InformedDuration.ToLower().Contains("always"))
            {
                _inform.Add((Guid)getEscalationMaster.leaveapprovalproc_uiKeepStaff3Informed);
            }
            return _inform;
        }

    }
}
