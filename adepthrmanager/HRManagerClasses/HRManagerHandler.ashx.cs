﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;

namespace AdeptHRManager.HRManagerClasses
{
    /// <summary>
    /// Summary description for HRManagerHandler
    /// </summary>
    /// 
    [WebService(Namespace = "http://www.adeptsystems.co.ke")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class HRManagerHandler : IHttpHandler
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            MemoryStream memoryStream = new MemoryStream();
            //load staff photo
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(context.Request.QueryString["staffID"]);
                if (_staffID != Guid.Empty)
                {
                    PROC_GetStaffPhotoResult getStaffPhoto = db.PROC_GetStaffPhoto(_staffID).FirstOrDefault();
                    //check if the staff has got any photo 
                    if (getStaffPhoto.StaffPhoto != null)
                    {
                        byte[] file = getStaffPhoto.StaffPhoto.ToArray();
                        memoryStream.Write(file, 0, file.Length);
                        context.Response.Buffer = true;
                        context.Response.BinaryWrite(file);
                        memoryStream.Dispose();
                        return;
                    }
                    else //if there is no staff photo
                    {
                        //load the default image
                        string fileLocation = HttpContext.Current.Server.MapPath("~/images/background/person.png");
                        FileStream fs = new FileStream(fileLocation, FileMode.Open);//

                        FileInfo imageInfo = new FileInfo(fileLocation);
                        byte[] fileDocument = new byte[imageInfo.Length];
                        fs.Read(fileDocument, 0, (int)imageInfo.Length);//read the document from the file stream
                        memoryStream.Write(fileDocument, 0, fileDocument.Length);
                        context.Response.Buffer = true;
                        context.Response.BinaryWrite(fileDocument);
                        memoryStream.Dispose();
                        fs.Close();//close the file opened
                        return;
                    }
                }
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}