﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace AdeptHRManager.HRManagerClasses
{
    /// <summary>
    /// Summary description for HRManagerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HRManagerService : System.Web.Services.WebService
    {
        HRManagerDataContext db = new HRManagerDataContext();
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        //web method for getting staff records names
        [WebMethod]
        public string[] GetStaffRecords(string prefixText, int count)
        {
            return db.StaffMaster_Views.Where(p => p.StaffRef.Contains(prefixText) || p.StaffName.Contains(prefixText) || p.IDNumber.Contains(prefixText) || p.PhoneNumber.Contains(prefixText)).OrderBy(p => p.StaffName).Select(p => p.StaffRef + ":" + p.StaffName).Take(count).ToArray();
        }
        //web method for getting application users
        [WebMethod]
        public string[] GetAppUsers(string prefixText, int count)
        {
            return db.Users_Views.Where(p => p.StaffName.Contains(prefixText)).OrderBy(p => p.StaffName).Select(p => p.StaffName).Take(count).ToArray();
        }
        //web method for getting courses
        [WebMethod]
        public string[] GetCourses(string prefixText, int count)
        {
            return db.Courses.Where(p => p.CourseRef.Contains(prefixText) || p.CourseTitle.Contains(prefixText)).OrderBy(p => p.CourseRef).Select(p => p.CourseRef + ":" + p.CourseTitle).Take(count).ToArray();
        }
        //web method for getting staff by staff reference
        [WebMethod]
        public string[] GetStaffByStaffReference(string prefixText, int count)
        {
            return db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffRef.Contains(prefixText)).OrderBy(p => p.StaffRef).Select(p => p.StaffRef).Take(count).ToArray();//remove super admin
        }
        //web method for getting staff by staff name
        [WebMethod]
        public string[] GetStaffByStaffName(string prefixText, int count)
        {
            return db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffName.Contains(prefixText)).OrderBy(p => p.StaffName).Select(p => p.StaffName).Take(count).ToArray();//remove super admin
        }
        //web method for getting staff by id number
        [WebMethod]
        public string[] GetStaffByIDNumber(string prefixText, int count)
        {
            return db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.IDNumber.Contains(prefixText)).OrderBy(p => p.StaffName).Select(p => p.IDNumber).Take(count).ToArray();//remove super admin
        }
        //web method for getting staff by phone number
        [WebMethod]
        public string[] GetStaffByMobileNumber(string prefixText, int count)
        {
            return db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.PhoneNumber.Contains(prefixText)).OrderBy(p => p.StaffName).Select(p => p.PhoneNumber).Take(count).ToArray();//remove super admin
        }
        //web method for getting job decsriptions
        [WebMethod]
        public string[] GetJobDescriptions(string prefixText, int count)
        {
            return db.Jobs_Views.Where(p => p.JobReference.Contains(prefixText) || p.JobType.Contains(prefixText) || p.JobCategory.Contains(prefixText) || p.JobTitle.Contains(prefixText)).OrderByDescending(p => p.JobReference).Select(p => p.JobReference + "::" + p.JobType + "::" + p.JobCategory + "::" + p.JobTitle).Take(count).ToArray();
        }
        //web method for getting jobs advertised and not yet closed
        [WebMethod]
        public string[] GetJobsAdvertisedAndNotClosed(string prefixText, int count)
        {
            prefixText = prefixText.ToLower();
            return db.PROC_JobsAdvertisedAndNotClosed().Where(p => p.JobReference.ToLower().Contains(prefixText) || p.JobType.ToLower().Contains(prefixText) || p.JobCategory.ToLower().Contains(prefixText) || p.JobTitle.ToLower().Contains(prefixText)).OrderByDescending(p => p.JobReference).Select(p => p.JobReference + "::" + p.JobType + "::" + p.JobCategory + "::" + p.JobTitle).Take(count).ToArray();
        }
    }
}
