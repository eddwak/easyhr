﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace AdeptHRManager.HRManagerClasses
{
    public class HRManagerReports
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManager_DataSet hrDS = new HRManager_DataSet();
        DataSet ds = new DataSet();

        SqlConnection sqlCon = new SqlConnection();
        SqlCommand sqlComm = new SqlCommand();
        SqlDataAdapter sqlDA = new SqlDataAdapter();
        DataSet sqlDS = new DataSet();
        string sqlConnectionString = "", sqlString = "";
        DataTable dt = new DataTable();
        public HRManagerReports()
        {
            sqlConnectionString = db.Connection.ConnectionString;
            ds = new DataSet();
            ds = hrDS.Clone();
        }
        //LEAVE REPORTS
        //dataset for getting all staff leave status report
        public DataSet rpt_AllStaffLeaveStatusReport()
        {
            sqlString = "SELECT * FROM [HR].[FUNC_GetEmployeesLeavesStatus]()";
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetEmployeesLeavesStatus");
            sqlCon.Close(); ;
            return sqlDS;
        }
        //dataset for getting staff leave status report for a particular department
        public DataSet rpt_StaffLeaveStatusReportPerDepartment(string _department)
        {
            sqlString = "SELECT * FROM [HR].[FUNC_GetEmployeesLeavesStatus]() WHERE DEPARTMENT='" + _department + "'";
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetEmployeesLeavesStatus");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting staff leave status report posting location
        public DataSet rpt_StaffLeaveStatusReportPerPostingLocation(string _postedAt)
        {
            sqlString = "SELECT * FROM [HR].[FUNC_GetEmployeesLeavesStatus]() WHERE [POSTED AT]='" + _postedAt + "'";
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetEmployeesLeavesStatus");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting staff leave status report posting location per department
        public DataSet rpt_StaffLeaveStatusReportPerPostingLocationPerDepartment(string _postedAt, string _department)
        {
            sqlString = "SELECT * FROM [HR].[FUNC_GetEmployeesLeavesStatus]() WHERE [POSTED AT]='" + _postedAt + "' AND DEPARTMENT='" + _department + "'";
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetEmployeesLeavesStatus");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving a single staff leave status details from a function
        public DataSet rpt_SingleStaffLeaveStatus(Guid _staffID)
        {
            sqlString = "SELECT * FROM [HR].[FUNC_GetSingleStaffLeavesStatus]('" + _staffID + "')";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetSingleStaffLeavesStatus");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting approved leaves per duration
        public DataSet rpt_LeaveApplicationsApprovedPerDuration(string _fromDate, string _toDate)
        {
            sqlString = "EXEC [HR].[PROC_ApprovedLeavesPerDuration] '" + _fromDate + "', '" + _toDate + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_ApprovedLeavesPerDuration");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }

        //TRAINING REPORTS
        //dataset for getting approved leaves per duration
        public DataSet rpt_TrainingsAttendedPerDuration(string _fromDate, string _toDate)
        {
            sqlString = "EXEC [Training].[PROC_TrainingsAttendedPerDuration] '" + _fromDate + "', '" + _toDate + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_TrainingsAttendedPerDuration");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for retrieving a single staff training attended details from a procedure
        public DataSet rpt_SingleStaffTrainingAttended(Guid _staffID)
        {
            sqlString = "[Training].[PROC_StaffTrainingAttendedReport] '" + _staffID + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_StaffTrainingAttendedReport");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }

        //PERFORMANCE REVIEW REPORTS
        //dataset for retrieving all performance details for the organisation
        public DataSet rpt_AllOrganisationPerformance()
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetOrganisationPerformance]()";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetOrganisationPerformance");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving performance details for the organisation per year
        public DataSet rpt_OrganisationPerformancePerYear(string _year)
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetOrganisationPerformance]() WHERE [YEAR]='" + _year + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetOrganisationPerformance");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving performance details for a department
        public DataSet rpt_PerformancePerDepartment(string _departmentName)
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetDepartmentPerformance]() WHERE [DEPARTMENT]='" + _departmentName + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetDepartmentPerformance");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving performance details for a department for a particular year
        public DataSet rpt_PerformancePerDepartmentPerYear(string _departmentName, string _year)
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetDepartmentPerformance]() WHERE [DEPARTMENT]='" + _departmentName + "' AND [YEAR]='" + _year + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetDepartmentPerformance");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving performance details for a single staff
        public DataSet rpt_PerformancePerStaff(Guid _staffID)
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetPerformancePerStaff]('" + _staffID + "')";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetPerformancePerStaff");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retrieving performance details for a single staff for a particular year
        public DataSet rpt_PerformancePerStaff(Guid _staffID, string _year)
        {
            sqlString = "SELECT * FROM [Performance].[FUNC_GetPerformancePerStaff]('" + _staffID + "') WHERE [YEAR]='" + _year + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "FUNC_GetPerformancePerStaff");
            sqlCon.Close();
            return sqlDS;
        }
        ///
        ///ATTENDANCE REPORTS
        ///
        //dataset for retrieving a single staff attendance details per duration from a procedure
        public DataSet rpt_SingleStaffAttendancePerDuration(string _staffRef, string _from, string _to)
        {
            sqlString = "[HR].[PROC_SingleEmployeeCheckInCheckOutPerDuration] '" + _staffRef + "', '" + _from + "','" + _to + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_SingleEmployeeCheckInCheckOutPerDuration");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for getting all employees daily attendance
        public DataSet rpt_StaffDailyAttendance(string _date)
        {
            sqlString = "SELECT * FROM HR.Func_GetEmployeesCheckInCheckOutAndLeavePerDay('" + _date + "') ";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "Func_GetEmployeesCheckInCheckOutAndLeavePerDay");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting all employees daily attendance in a particular posting location
        public DataSet rpt_StaffDailyAttendance(string _date, string _postedAt)
        {
            sqlString = "SELECT * FROM HR.Func_GetEmployeesCheckInCheckOutAndLeavePerDay('" + _date + "') " +
            "WHERE [POSTED AT]='" + _postedAt + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "Func_GetEmployeesCheckInCheckOutAndLeavePerDay");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting employees daily attendance in a particular department
        public DataSet rpt_StaffDailyAttendancePerDepartment(string _date, string _department)
        {
            sqlString = "SELECT * FROM HR.Func_GetEmployeesCheckInCheckOutAndLeavePerDay('" + _date + "') " +
            "WHERE [DEPARTMENT]='" + _department + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "Func_GetEmployeesCheckInCheckOutAndLeavePerDay");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for getting employees daily attendance in a particular department within a particular posting location/branch
        public DataSet rpt_StaffDailyAttendancePerDepartment(string _date, string _postedAt, string _department)
        {
            sqlString = "SELECT * FROM HR.Func_GetEmployeesCheckInCheckOutAndLeavePerDay('" + _date + "') " +
            "WHERE [POSTED AT]='" + _postedAt + "' AND [DEPARTMENT]='" + _department + "'";
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlComm = new SqlCommand(sqlString, sqlCon);
            sqlComm.CommandTimeout = 30;
            sqlDA = new SqlDataAdapter();
            sqlDA.SelectCommand = sqlComm;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "Func_GetEmployeesCheckInCheckOutAndLeavePerDay");
            sqlCon.Close();
            return sqlDS;
        }
        //dataset for retreiving employees attendance weekly
        public DataSet rpt_WeeklyAttendanceForAllEmployees(string date)
        {
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlString = "EXEC [HR].[PROC_WeeklyCheckInCheckOutForAllEmployees] '" + date + "'";
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_WeeklyCheckInCheckOutForAllEmployees");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for retreiving employees attendance weekly per department
        public DataSet rpt_WeeklyAttendancePerDepartment(string date, string departmentName)
        {
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlString = "EXEC [HR].[PROC_WeeklyCheckInCheckOutPerDepartment] '" + date + "','" + departmentName + "'";
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_WeeklyCheckInCheckOutPerDepartment");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for retreiving employees weekly attendance in a posting location
        public DataSet rpt_WeeklyAttendanceForAllEmployeesInAPostingLocation(string date, string postedAt)
        {
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlString = "EXEC [HR].[PROC_WeeklyCheckInCheckOutPerBranch] '" + date + "','" + postedAt + "'";
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_WeeklyCheckInCheckOutPerBranch");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        public DataSet rpt_WeeklyAttendancePerDepartmentInAPostingLocation(string date, string postedAt, string departmentName)
        {
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection(sqlConnectionString);
            sqlCon.Open();
            sqlComm = new SqlCommand("HR.PROC_WeeklyCheckInCheckOutPerBranchPerDepartment");
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.Add(new SqlParameter("@date", SqlDbType.VarChar)).Value = date;
            sqlComm.Parameters.Add(new SqlParameter("@postedAt", SqlDbType.VarChar)).Value = postedAt;
            sqlComm.Parameters.Add(new SqlParameter("departmentName", SqlDbType.VarChar)).Value = departmentName;
            sqlComm.Connection = sqlCon;
            sqlDA = new SqlDataAdapter(sqlComm);
            sqlDS = new DataSet();
            sqlDA.Fill(sqlDS, "PROC_WeeklyCheckInCheckOutPerBranchPerDepartment");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }

        //RECRUIRMENT REPORTS
        //dataset for getting interview report for a particular jon
        public DataSet rpt_InterviewReportPerJob(int _jobID)
        {
            sqlString = "EXEC [Recruitment].[PROC_InterviewAppointment] "+ _jobID+ "";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_InterviewAppointment");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for getting an applicant's CV
        public DataSet rpt_ApplicantCV(Guid _applicantID)
        {
            sqlString = "EXEC [Recruitment].[PROC_ApplicantCV] '" + _applicantID + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_ApplicantCV");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
        //dataset for getting an applicant's education details
        public DataSet rpt_ApplicantEducation(Guid _applicantID)
        {
            sqlString = "EXEC [Recruitment].[PROC_ApplicantEducation] '" + _applicantID + "'";
            sqlConnectionString = db.Connection.ConnectionString;
            sqlCon = new SqlConnection();
            sqlCon.ConnectionString = sqlConnectionString;
            sqlCon.Open();
            sqlDS = new DataSet();
            sqlDA = new SqlDataAdapter();
            sqlDA = new SqlDataAdapter(sqlString, sqlCon);
            sqlDA.Fill(sqlDS, "PROC_ApplicantEducation");
            sqlCon.Close();
            return (DataSet)sqlDS;
        }
    }
}