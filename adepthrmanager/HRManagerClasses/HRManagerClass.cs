﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Security;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Threading;

namespace AdeptHRManager.HRManagerClasses
{

    public class HRManagerClass
    {
        HRManagerDataContext db = new HRManagerDataContext();
        //change the css of the module main menu link bassed on the link being viewed
        public void ChangeModuleMainMenuActiveLinkCSS(Page _page, Panel _panelModuleMainMenu)
        {
            foreach (Control _control in _panelModuleMainMenu.Controls)
            {
                // Check and see if it's a HyperLink
                if ((_control.GetType() == typeof(HyperLink)))
                {
                    HyperLink _menulink = ((HyperLink)(_control));
                    if (_page.AppRelativeVirtualPath.ToLower() == _menulink.NavigateUrl.ToString().ToLower())
                    {
                        _menulink.CssClass = "active-module-menu";

                        break;
                    }
                    else { }
                }
            }
        }
        //get a listing item detail name
        public void GetListingItems(DropDownList _dropDown, int _masterID, string _displayText)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getReferenceDetail;
               // _getReferenceDetail = db.ListingItems.Where(p => p.ListingMasterID == _masterID).OrderBy(p => p.LisitingDetailValue);
                _getReferenceDetail = db.PROC_ListingItem(_masterID);
                _dropDown.DataSource = _getReferenceDetail;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "LisitingDetailValue";
                _dropDown.DataValueField = "ListingItemIID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }
        //get a listing item VALUE 1 name
        public void GetListingItemsValue1(DropDownList _dropDown, int _masterID, string _displayText)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getReferenceDetail;
                // _getReferenceDetail = db.ListingItems.Where(p => p.ListingMasterID == _masterID).OrderBy(p => p.LisitingDetailValue);
                _getReferenceDetail = db.PROC_ListingItem(_masterID);
                _dropDown.DataSource = _getReferenceDetail;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "LisitingDetailValue1";
                _dropDown.DataValueField = "ListingItemIID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }

        //get a listing item detail name
        public void GetListingItemsWithTableOrder(DropDownList _dropDown, int _masterID, string _displayText)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getReferenceDetail;
                _getReferenceDetail = db.ListingItems.Where(p => p.ListingMasterID == _masterID);
                _dropDown.DataSource = _getReferenceDetail;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "LisitingDetailValue";
                _dropDown.DataValueField = "ListingItemIID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }
        //get staff names
        public void GetStaffNames(DropDownList _dropDown, string _displayText)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getStaff;
                _getStaff = db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686").OrderBy(p => p.StaffName);//remove super administrator(c8f451c1-b573-e411-8a52-68a3c4aec686) 
                _dropDown.DataSource = _getStaff;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "StaffName";
                _dropDown.DataValueField = "StaffID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }

        //get staff names per department
        public void GetStaffNames(DropDownList _dropDown, string _displayText, int _departmentID)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getStaff;
                _getStaff = db.StaffMasters.Where(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.DepartmentID == _departmentID).OrderBy(p => p.StaffName);//remove super administrator(c8f451c1-b573-e411-8a52-68a3c4aec686) 
                _dropDown.DataSource = _getStaff;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "StaffName";
                _dropDown.DataValueField = "StaffID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }

        //get leave type names
        public void GetLeaveTypes(DropDownList _dropDown, string _displayText)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getStaff;
                _getStaff = db.LeaveTypes.Select(p => p).OrderBy(p => p.leavetype_vName);
                _dropDown.DataSource = _getStaff;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "leavetype_vName";
                _dropDown.DataValueField = "leavetype_siLeaveTypeID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }
        //method for loading HRManager message box
        public void LoadHRManagerMessageBox(int _messageType, string _hrManagerMessage, Page _thisPage, Control _bindingControl)
        {
            HttpContext.Current.Session["MessageType"] = _messageType;//message type checks which kind of message is being loaded, 1 for error and 2  information
            HttpContext.Current.Session["HRManagerMessage"] = _hrManagerMessage;//message is for the displaying the message
            Control ctrl = _thisPage.LoadControl(string.Format("~/HRManagerClasses/HRManagerUserControls/HRManager_Message_Box.ascx"));
            _bindingControl.Controls.Add(ctrl);
        }
        //method for loading HRManager confirmation message box
        public void LoadHRManagerConfirmationMessageBox(Control ucConfirm, string _hrManagerConfirmationMessage)
        {
            Label _lbDeleteMessage = (Label)ucConfirm.FindControl("lbConfirmationMessageBoxMessage");
            _lbDeleteMessage.Text = _hrManagerConfirmationMessage;
            ModalPopupExtender _confirmationPopup = (ModalPopupExtender)ucConfirm.FindControl("ModalPopupExtenderHRManagerConfirmationMessageBox");
            _confirmationPopup.Show();
        }
        //checking if a fgridview row checkbox  is selected in the grid views
        public Boolean isGridviewItemSelected(GridView _gridView)
        {
            try
            {
                int x = 0;
                foreach (GridViewRow _gvr in _gridView.Rows)
                {
                    CheckBox _cb = (CheckBox)_gvr.FindControl("CheckBox1");
                    if (_cb.Checked) ++x;
                }
                if (x > 0) return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //checking of a radio button item is selected in  gridview
        public Boolean isGridviewRadioButtonSelected(GridView _gridView)
        {
            try
            {
                int x = 0;
                foreach (GridViewRow _gvr in _gridView.Rows)
                {
                    RadioButton _rb = (RadioButton)_gvr.FindControl("RadioButton1");
                    if (_rb.Checked)
                    {
                        ++x;
                        break;
                    }
                }
                if (x > 0) return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //method for adding a new listing
        public void AddNewListingItem(int _listingMasterID, string _listingType, string _listingValue, string _listingValue1, string _listingValue2, int? _listingValueID, int? _listingValue1ID, int? _listingValue2ID, int? _parentID, string _listingDescription, Guid? _listingValueGuidID, HiddenField _hfID)
        {
            try
            {
                //get the last listing id for the listing master
                int _lastListingItemID = _listingMasterID;//initialize with the master id
                try
                {
                    _lastListingItemID = Convert.ToInt32(db.ListingItems.Where(p => p.ListingMasterID == _listingMasterID).Max(p => p.ListingItemIID));
                }
                catch { }
                int _newListingItemID = _lastListingItemID + 1;
                //add a new listing
                ListingItem newListingItem = new ListingItem();
                newListingItem.ListingItemIID = _newListingItemID;
                newListingItem.ListingMasterID = _listingMasterID;
                newListingItem.LisitingDetailValue = _listingValue;
                newListingItem.LisitingDetailValue1 = _listingValue1;
                newListingItem.LisitingDetailValue2 = _listingValue2;
                newListingItem.ListingValueID = _listingValueID;
                newListingItem.ListingValue1ID = _listingValue1ID;
                newListingItem.ListingValue2ID = _listingValue2ID;
                newListingItem.ListingParentID = _parentID;
                newListingItem.LisitingDetailDescription = _listingDescription;
                newListingItem.ListingValueGuidID = _listingValueGuidID;

                db.ListingItems.InsertOnSubmit(newListingItem);
                db.SubmitChanges();
                _hfID.Value = newListingItem.ListingItemIID.ToString();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("Save failed. " + ex.Message.ToString() + ". Please try again.");
            }
        }
        //method for updating a listing item
        public void UpdateListingItem(int _listingMasterID, int _listingItemID, string _listingType, string _listingValue, string _listingValue1, string _listingValue2, int? _listingValueID, int? _listingValue1ID, int? _listingValue2ID, int? _parentID, string _listingDescription, Guid? _listingValueGuidID)
        {
            try
            {

                //add a new listing
                ListingItem updateListingItem = db.ListingItems.Single(p => p.ListingItemIID == _listingItemID);
                updateListingItem.LisitingDetailValue = _listingValue;
                updateListingItem.LisitingDetailValue1 = _listingValue1;
                updateListingItem.LisitingDetailValue2 = _listingValue2;
                updateListingItem.ListingValueID = _listingValueID;
                updateListingItem.ListingValue1ID = _listingValue1ID;
                updateListingItem.ListingValue2ID = _listingValue2ID;
                updateListingItem.ListingParentID = _parentID;
                updateListingItem.LisitingDetailDescription = _listingDescription;
                updateListingItem.ListingValueGuidID = _listingValueGuidID;
                db.SubmitChanges();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("Save failed. " + ex.Message.ToString() + ". Please try again.");
            }
        }
        //method for checking if a listing item exists during new addition on one value
        public bool DoesListingItemExist(int _listingMasterID, string _listingValue)
        {
            if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue.ToLower() == _listingValue.ToString().ToLower()))
                return true;
            else return false;
        }

        //method for checking if a listing item exists during new addition
        public bool DoesListingItemExist(int _listingMasterID, string _listingValue, string _listingValue1)
        {
            if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue.ToLower() == _listingValue.ToString().ToLower()))
                return true;
            else if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue1.ToLower() == _listingValue1.ToString().ToLower()))
                return true;
            else return false;
        }
        //method for checking if a listing item exists durring updation(one value)
        public bool DoesListingItemExist(int _listingMasterID, int _listingItemID, string _listingValue)
        {
            if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue.ToLower() == _listingValue.ToLower() && p.ListingItemIID != _listingItemID))
                return true;
            else return false;
        }
        //method for checking if a listing item exists durring updation
        public bool DoesListingItemExist(int _listingMasterID, int _listingItemID, string _listingValue, string _listingValue1)
        {
            if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue.ToLower() == _listingValue.ToLower() && p.ListingItemIID != _listingItemID))
                return true;
            else if (db.ListingItems.Any(p => p.ListingMasterID == _listingMasterID && p.LisitingDetailValue1.ToLower() == _listingValue1.ToLower() && p.ListingItemIID != _listingItemID))
                return true;
            else return false;
        }
        //method for clearing all entry  controls
        public void ClearEntryControls(Control Parent)
        {
            foreach (Control _control in Parent.Controls)
            {
                // Check and see if it's a textbox
                if ((_control.GetType() == typeof(TextBox)))
                {	            // Since its a textbox clear out the text            
                    ((TextBox)(_control)).Text = "";
                    ((TextBox)(_control)).Enabled = true;
                }
                //check to see if it is a dropdownlist
                else if ((_control.GetType() == typeof(DropDownList)))
                {
                    ((DropDownList)(_control)).SelectedIndex = 0;
                    ((DropDownList)(_control)).Enabled = true;
                }
                //check to see if the control is a radio button list
                else if ((_control.GetType() == typeof(RadioButtonList)))
                {
                    ((RadioButtonList)(_control)).SelectedIndex = -1;
                    ((RadioButtonList)(_control)).Enabled = true;
                }
                //check if the controll os a check box list
                else if ((_control.GetType() == typeof(CheckBoxList)))
                {
                    ((CheckBoxList)(_control)).SelectedIndex = -1;
                    ((CheckBoxList)(_control)).Enabled = true;
                }
                //check if the control is a checkbox
                else if ((_control.GetType() == typeof(CheckBox)))
                {
                    ((CheckBox)(_control)).Checked = false;
                    ((CheckBox)(_control)).Enabled = true;
                }
                // Now we need to call itself (recursive) because
                // all items (Panel, GroupBox, etc) is a container
                // so we need to check all containers for any
                // textboxes so we can clear them	        
                if (_control.HasControls())
                {
                    ClearEntryControls(_control);
                }
            }
        }
        //method for disable entry  controls
        public void DisableEntryControls(Control Parent)
        {
            foreach (Control _control in Parent.Controls)
            {
                // Check and see if it's a textbox
                if ((_control.GetType() == typeof(TextBox)))
                {
                    ((TextBox)(_control)).Enabled = false;
                }
                //check to see if it is a dropdownlist
                else if ((_control.GetType() == typeof(DropDownList)))
                {
                    ((DropDownList)(_control)).Enabled = false;
                }
                //check to see if the control is a radio button list
                else if ((_control.GetType() == typeof(RadioButtonList)))
                {
                    ((RadioButtonList)(_control)).Enabled = false;
                }
                //check if the controll os a check box list
                else if ((_control.GetType() == typeof(CheckBoxList)))
                {
                    ((CheckBoxList)(_control)).Enabled = false;
                }
                //check if the control is a checkbox
                else if ((_control.GetType() == typeof(CheckBox)))
                {
                    ((CheckBox)(_control)).Enabled = false;
                }
                // Now we need to call itself (recursive) because
                // all items (Panel, GroupBox, etc) is a container
                // so we need to check all containers for any
                // textboxes so we can clear them	        
                if (_control.HasControls())
                {
                    DisableEntryControls(_control);
                }
            }
        }
        //clear gridview
        public void ClearGridView(GridView _gridView)
        {
            _gridView.DataSource = null;
            _gridView.DataBind();
        }
        //method for validating email addresses
        public bool IsValidEmail(string Email)
        {
            return Regex.IsMatch(Email, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
        //method for validating a date value
        public bool isDateValid(string dateValue)
        {
            try
            {
                Convert.ToDateTime(dateValue);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //method for validating 24 hr time
        public bool is24HourTimeValid(string timeValue)
        {
            try
            {
                int _time;
                try
                {
                    _time = Convert.ToInt16(timeValue.Replace(":", ""));//try to convert the value into int
                }
                catch { return false; }
                if (_time > 2359)//check if the value entered is greater them 2359
                {
                    return false;
                }
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        //convert string to guid
        public Guid ReturnGuid(string _stringValue)
        {
            Guid _guidValue = new Guid(_stringValue);
            return _guidValue;
        }
        //convert string to guid when a staff recorded is being searched
        public Guid ReturnGuidForSearchedStaffID(string _stringValue)
        {
            Guid _guidValue = new Guid(_stringValue);
            HttpContext.Current.Session["SearchedStaffID"] = _guidValue;
            return _guidValue;
        }
        //convert to curency
        public string ConvertToCurrencyValue(decimal currencyValue)
        {
            return string.Format("{0:N2}", currencyValue);
        }
        //generate years to populate in years dates
        public void GenerateYears(DropDownList _dropDown)
        {
            _dropDown.Items.Clear();
            _dropDown.Items.Insert(0, new ListItem("-Year-"));
            //get current year
            int _currentYear = DateTime.Now.Year;
            for (int i = _currentYear; i >= 1960; i--)
            {
                _dropDown.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
        //generate years with extra five years from current to populate in years dates
        public void GenerateYearsWithExtraYears(DropDownList _dropDown)
        {
            _dropDown.Items.Clear();
            _dropDown.Items.Insert(0, new ListItem("-Year-"));
            //get current year
            int _currentYear = DateTime.Now.Year + 5;//add five years from current year
            for (int i = _currentYear; i >= 1960; i--)
            {
                _dropDown.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
        //generate months to populate in months dates
        public void GenerateMonths(DropDownList _dropDown)
        {
            _dropDown.Items.Clear();
            _dropDown.Items.Insert(0, new ListItem("-Month-"));
            for (int i = 1; i <= 12; i++)
            {
                //_dropDown.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(i), i.ToString()));
                string _monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i);
                _dropDown.Items.Add(new ListItem(_monthName, _monthName));
            }

        }
        //method for validating a user password
        public bool IsValidPassword(string _password)
        {
            return Regex.IsMatch(_password, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$");
        }
        //hash password
        private string hashPassword(string _stringValue)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(_stringValue, "sha1");
        }
        //Key for the crypto provider
        //private static readonly byte[] _key = { 0x, 0xF1, 0xA6, 0xBB, 0A2, 0x5A, 0x37, 0x6F, 0x81, 0x2E, 0x17, 0x41, 0x72, 0x2C, 0x43, 0x27 };
        private static readonly byte[] _key = { 0xC2, 0x53, 0x98, 0xDC, 0x77, 0x75, 0x21, 0x52, 0xF1, 0xEE, 0x6A, 0xD2, 0x45, 0x33, 0xAD, 0x5A };
        // Initialization vector for the crypto provider
        private static readonly byte[] _initVector = { 0xFE, 0xA8, 0xC4, 0x3E, 0xA9, 0xA9, 0xDE, 0xFF, 0xA9, 0x2E, 0x49, 0xAE, 0xE0, 0xD1, 0xAA, 0x74 };

        // Encrypt a password
        private string pwdEncrypt(string _pwd)
        {
            if (string.IsNullOrEmpty(_pwd))
                return string.Empty;

            byte[] Value = Encoding.UTF8.GetBytes(_pwd);
            SymmetricAlgorithm mCSP = new RijndaelManaged();
            mCSP.Key = _key;
            mCSP.IV = _initVector;
            using (ICryptoTransform ct = mCSP.CreateEncryptor(mCSP.Key, mCSP.IV))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                    {
                        cs.Write(Value, 0, Value.Length);
                        cs.FlushFinalBlock();
                        cs.Close();
                        return Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
        }
        //public retun has & encrypt to validate the entered password (validate 4th and 5th part)
        public bool ValidateHashAndEncrytPwd(string _password, string _correctPassword)
        {
            char[] delimiter = { ':' };
            string[] split = _correctPassword.Split(delimiter);
            int iterations = Int32.Parse(split[_iterationIndex]);//get iterations
            string _salt = (split[_saltIndex]);//get the salt string
            string _hash = (split[_PBKDF2Index]);//get hasshed string
            //get the 4th and 5th part of the password
            string _reminderPwd = _correctPassword.Replace(iterations.ToString() + ":", "").Replace(_salt + ":", "").Replace(_hash + ":", "");
            //hash and  encrypt the passhed being validated
            string _enteredPassword = hashPassword(_password) + ":" + pwdEncrypt(_password);
            if (_enteredPassword == _reminderPwd)
                return true;
            else return false;
        }
        private const int _saltSize = 24;//set salt size
        private const int _hashSize = 24;//set hash size
        private const int _iterations = 1111;//set number of iterations
        private const int _iterationIndex = 0;
        private const int _saltIndex = 1;
        private const int _PBKDF2Index = 2;
        //create a hashed,salted and encrypted password
        public string GenerateHashSaltedEnctryptedPwd(string password)
        {
            // Generate a random salt
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[_saltSize];
            csprng.GetBytes(salt);

            // Hash the password and encode the parameters
            byte[] hash = PBKDF2(password, salt, _iterations, _hashSize);
            return _iterations + ":" + Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash) + ":" + hashPassword(password) + ":" + pwdEncrypt(password); ;
        }
        //derive the bites
        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
        {
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt);
            pbkdf2.IterationCount = iterations;
            return pbkdf2.GetBytes(outputBytes);
        }
        //validate the entered password (2nd and 3rd part)
        public bool ValidatePassword(string password, string correctHash)
        {
            // Extract the parameters from the hash
            //validate the 2nd and 3rd part of the password
            char[] delimiter = { ':' };
            string[] split = correctHash.Split(delimiter);
            int iterations = Int32.Parse(split[_iterationIndex]);
            byte[] salt = Convert.FromBase64String(split[_saltIndex]);//get salt part
            byte[] hash = Convert.FromBase64String(split[_PBKDF2Index]);//get hash part

            byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }
        //method for validating numeric values
        public bool isNumberValid(string numericValue)
        {
            decimal value;
            if (decimal.TryParse(numericValue, out value))
                return true;
            else return false;
        }
        //method for validating currency values
        public bool IsCurrencyValid(string currencyValue)
        {
            string _currencyValue = currencyValue.Replace(",", "");//remove commas
            return Regex.IsMatch(_currencyValue, @"^\d+(\.\d+)?$");//many decimal places
        }
        //return item selected in the check box list
        public string ReturnCheckBoxSelectedItems(CheckBoxList _checkBoxList)
        {
            string _selectedItems = "";
            foreach (ListItem _item in _checkBoxList.Items)
            {
                if (_item.Selected)
                    _selectedItems += _item.Value.ToString() + ",";
            }
            return _selectedItems;
        }
        //populate a checkbox list
        public void PopulateSelectCheckBoxList(string _value, CheckBoxList _checkBoxList)
        {
            _checkBoxList.SelectedIndex = -1;//clear previous selected  items
            if (_value != "")
            {
                var _listValues = _value.Split(',').Select(p => p).ToArray();//get array for the string value passed
                foreach (ListItem _listItem in _checkBoxList.Items)
                {
                    foreach (string _listValue in _listValues)
                    {
                        if (_listItem.Value == _listValue)//check if the value is to be selected
                            _listItem.Selected = true;
                    }
                }
            }
        }
        //get employee's all   names
        public string getStaffNames(Guid _staffID)
        {
            try
            {
                string _employeeNames = db.StaffMasters.Single(p => p.StaffID == _staffID).StaffName;
                return ConvertFirstStringLettersToUpper(_employeeNames);
            }
            catch { return ""; }
        }
        //get staff's first name
        public string getStaffFirstName(Guid _staffID)
        {
            try
            {
                string _staffNames = db.StaffMasters.Single(p => p.StaffID == _staffID).StaffName;
                string _staffFirstName = "";
                if (_staffNames.Contains(" "))//chec if the names habe got any spaces

                    _staffFirstName = _staffNames.Substring(0, _staffNames.IndexOf(" "));//get staff's first name
                else _staffFirstName = _staffNames;
                return _staffFirstName;
            }
            catch { return ""; }
        }
        //return first name
        public string ReturnFirstName(string _names)
        {
            if (String.IsNullOrEmpty(_names))
                throw new ArgumentException("No name entered !");
            string _firstName = "";
            if (_names.Contains(" "))//check if thre are spaces
                _firstName = _names.Substring(0, _names.IndexOf(" "));//get first name
            else _firstName = _names;
            return _firstName.First().ToString().ToUpper() + _firstName.Substring(1);//capitalize the first character

        }
        //get staff posting location
        public int GetStaffPostingLocationID(Guid _staffID)
        {
            return (int)db.StaffMasters.Single(p => p.StaffID == _staffID).PostedAtID;
        }
        //get staff email  address
        public string getStaffEmailAddress(Guid _staffID)
        {
            try
            {
                return db.StaffMasters.Single(p => p.StaffID == _staffID).EmailAddress.ToString().Trim();
            }
            catch { return ""; }
        }
        // get document content type per extension
        public void GetDocumentContentType(string _docExtension, string _documentName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = _documentName.Replace(" ", "_");//replace space betwen words with a underscore to get the full document name

            // looks for a content type with extension
            //Note : This would be problem if  multiple extensions associate with one content type.
            RegistryKey typeKey = Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type");

            foreach (string keyname in typeKey.GetSubKeyNames())
            {
                RegistryKey curKey = typeKey.OpenSubKey(keyname);
                if (curKey != null)
                {
                    object extension = curKey.GetValue("Extension");
                    if (extension != null)
                    {
                        if (extension.ToString().ToLower() == _docExtension)
                        {
                            HttpContext.Current.Response.ContentType = keyname;
                        }
                    }
                }
            }


            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);
        }
        // return document content type() as a string value per extension 
        public string ReturnDocumentContentType(string _docExtension)
        {
            if (_docExtension == ".doc" || _docExtension == ".docx")//word document
            {
                return "application/msword";
            }
            else if (_docExtension == ".xls" || _docExtension == ".xlsx")//excel document
            {
                return "application/excel";
            }
            else if (_docExtension == ".pdf")//pdf
            {
                return "application/pdf";
            }
            else if (_docExtension == ".vsd" || _docExtension == ".vsdx")//ms visio document
            {
                return "application/vsd";
            }
            else if (_docExtension == ".mdb")//access database
            {
                return "application/access";
            }
            else if (_docExtension == ".txt")//text file(notepad)
            {
                return "application/notepad";
            }
            else
            {
                return "image/jpg";
            }
        }
        //check if the upload file is more than 3mb
        public bool IsUploadedFileBig(int _fileLength)
        {
            //check if the file  is 3mb i.e 3 *1024 *1024 (3145728) bytes
            if (_fileLength > 3145728)
                return true;
            else return false;
        }
        //validate date textbox with a watermark
        public bool ValidateDateTextBox(TextBox _txtDate)
        {
            if (_txtDate.Text.Trim() != "__/___/____" && isDateValid(_txtDate.Text.Trim()) == false)
                return false;
            else
                return true;
        }
        //get staff id by using staff reference
        public string GetStaffIDByStaffReference(string _staffReference)
        {
            if (db.StaffMasters.Any(p => p.StaffRef.ToLower() == _staffReference.ToLower()))
            {
                string _staffID = db.StaffMasters.Single(p => p.StaffRef.ToLower() == _staffReference).StaffID.ToString();
                return _staffID;
            }
            else return "";
        }
        //get staff referenece by using staff reference
        public string GetStaffReferenceByStaffID(Guid _staffID)
        {
            if (db.StaffMasters.Any(p => p.StaffID == _staffID))
            {
                string _staffRef = db.StaffMasters.Single(p => p.StaffID == _staffID).StaffRef.ToString();
                return _staffRef;
            }
            else return "";
        }
        //get staff id by using staff reference
        public string GetSearchStaffIDByStaffReference(string _staffReference, HiddenField _hiddenStaffID, TextBox _txtSearchByStaffName, TextBox _txtSearchByIDNumber, TextBox _txtSearchByMobileNumber, Image _imageStaffPhoto)
        {
            if (db.StaffMasters.Any(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffRef.ToLower() == _staffReference.ToLower()))//remove super admin
            {
                StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffRef.ToLower() == _staffReference);
                string _staffID = getStaff.StaffID.ToString();
                _hiddenStaffID.Value = _staffID;
                _txtSearchByStaffName.Text = getStaff.StaffName;
                _txtSearchByIDNumber.Text = getStaff.IDNumber;
                _txtSearchByMobileNumber.Text = getStaff.PhoneNumber;
                LoadStaffPhoto(getStaff.StaffID, _imageStaffPhoto);//load staff photo
                return _staffID;
            }
            else
            {
                _hiddenStaffID.Value = Guid.Empty.ToString();
                _txtSearchByStaffName.Text = "";
                _txtSearchByIDNumber.Text = "";
                _txtSearchByMobileNumber.Text = "";
                _imageStaffPhoto.ImageUrl = "~/images/background/person.png";
                return "";

            }
        }
        //get staff id by using staff name
        public string GetSearchStaffIDByStaffName(string _staffName, HiddenField _hiddenStaffID, TextBox _txtSearchByStaffRef, TextBox _txtSearchByIDNumber, TextBox _txtSearchByMobileNumber, Image _imageStaffPhoto)
        {
            if (db.StaffMasters.Any(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffName.ToLower() == _staffName.ToLower()))//remove super admin
            {
                StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.StaffName.ToLower() == _staffName);
                string _staffID = getStaff.StaffID.ToString();
                _hiddenStaffID.Value = _staffID;
                _txtSearchByStaffRef.Text = getStaff.StaffRef;
                _txtSearchByIDNumber.Text = getStaff.IDNumber;
                _txtSearchByMobileNumber.Text = getStaff.PhoneNumber;
                LoadStaffPhoto(getStaff.StaffID, _imageStaffPhoto);//load staff photo
                return _staffID;

            }
            else
            {
                _hiddenStaffID.Value = Guid.Empty.ToString();
                _txtSearchByStaffRef.Text = "";
                _txtSearchByIDNumber.Text = "";
                _txtSearchByMobileNumber.Text = "";
                _imageStaffPhoto.ImageUrl = "~/images/background/person.png";
                return "";
            }
        }
        //get staff id by using national ID number
        public string GetSearchStaffIDByIDNumber(string _idNumber, HiddenField _hiddenStaffID, TextBox _txtSearchByStaffName, TextBox _txtSearchByStaffRef, TextBox _txtSearchByMobileNumber, Image _imageStaffPhoto)
        {
            if (db.StaffMasters.Any(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.IDNumber.ToLower() == _idNumber.ToLower()))//remove super admin
            {
                StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.IDNumber.ToLower() == _idNumber);
                string _staffID = getStaff.StaffID.ToString();
                _hiddenStaffID.Value = _staffID;
                _txtSearchByStaffName.Text = getStaff.StaffName;
                _txtSearchByStaffRef.Text = getStaff.StaffRef;
                _txtSearchByMobileNumber.Text = getStaff.PhoneNumber;
                LoadStaffPhoto(getStaff.StaffID, _imageStaffPhoto);//load staff photo
                return _staffID;

            }
            else
            {
                _hiddenStaffID.Value = Guid.Empty.ToString();
                _txtSearchByStaffName.Text = "";
                _txtSearchByStaffRef.Text = "";
                _txtSearchByMobileNumber.Text = "";
                _imageStaffPhoto.ImageUrl = "~/images/background/person.png";
                return "";
            }
        }
        //get staff id by using mobile number
        public string GetSearchStaffIDByMobileNumber(string _mobileNumber, HiddenField _hiddenStaffID, TextBox _txtSearchByStaffName, TextBox _txtSearchByStaffRef, TextBox _txtSearchByIDNumber, Image _imageStaffPhoto)
        {
            if (db.StaffMasters.Any(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.PhoneNumber.ToLower() == _mobileNumber.ToLower()))//remove super admin
            {
                StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID.ToString() != "c8f451c1-b573-e411-8a52-68a3c4aec686" && p.PhoneNumber == _mobileNumber);
                string _staffID = getStaff.StaffID.ToString();
                _hiddenStaffID.Value = _staffID;
                _txtSearchByStaffName.Text = getStaff.StaffName;
                _txtSearchByStaffRef.Text = getStaff.StaffRef;
                _txtSearchByIDNumber.Text = getStaff.IDNumber;
                LoadStaffPhoto(getStaff.StaffID, _imageStaffPhoto);//load staff photo
                return _staffID;
            }
            else
            {
                _hiddenStaffID.Value = Guid.Empty.ToString();
                _txtSearchByStaffName.Text = "";
                _txtSearchByStaffRef.Text = "";
                _txtSearchByIDNumber.Text = "";
                _imageStaffPhoto.ImageUrl = "~/images/background/person.png";
                return "";
            }
        }
        //load staff photo by checking if the employee has a photo
        public void LoadStaffPhoto(Guid _staffID, Image _imageStaffPhoto)
        {
            PROC_GetStaffPhotoResult getStaffPhoto = db.PROC_GetStaffPhoto(_staffID).FirstOrDefault();
            if (getStaffPhoto.StaffPhoto != null)
            {
                _imageStaffPhoto.ImageUrl = "~/HRManagerClasses/HRManagerHandler.ashx?staffID=" + _staffID;
            }
            else
            {
                _imageStaffPhoto.ImageUrl = "~/images/background/person.png";
            }
        }
        //load seearch staff fields
        public void LoadSearchStaffControls(TextBox _txtSearchByStaffName, string _staffName, TextBox _txtSearchByStaffRef, string _staffRef, TextBox _txtSearchByIDNumber, string _idNumber, TextBox _txtSearchByMobileNumber, string _mobileNumber)
        {
            _txtSearchByStaffName.Text = _staffName.Trim();
            _txtSearchByStaffRef.Text = _staffRef.Trim();
            _txtSearchByIDNumber.Text = _idNumber.Trim();
            _txtSearchByMobileNumber.Text = _mobileNumber.Trim();
        }
        //populate staff header details
        public void PopupateStaffModuleHeader(string _staffID, HiddenField _hiddenStaffID, TextBox _txtSearchByStaffName, TextBox _txtSearchByStaffRef, TextBox _txtSearchByIDNumber,TextBox _txtSearchByMobileNumber, Image _imageStaffPhoto)
        {
            StaffMaster getStaff = db.StaffMasters.Single(p => p.StaffID.ToString() == _staffID);
            _hiddenStaffID.Value = _staffID;
            _txtSearchByStaffName.Text = getStaff.StaffName;
            _txtSearchByStaffRef.Text = getStaff.StaffRef;
            _txtSearchByIDNumber.Text = getStaff.IDNumber;
            _txtSearchByMobileNumber.Text = getStaff.PhoneNumber;
            LoadStaffPhoto(getStaff.StaffID, _imageStaffPhoto);//load staff photo
            return;
        }
        public string ConvertDateDayStart(string date)
        {
            string dateConverted;
            // dateConverted = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
            dateConverted = Convert.ToDateTime(date).ToLongDateString();
            return dateConverted;
        }
      public string ShortDateDayStart(string date)
        {
            string dateConverted;
            dateConverted = Convert.ToDateTime(date).ToString("dd/MMM/yyyy");
            return dateConverted;
        }
        public string ShortDateDayStart(DateTime date)
        {
            string dateConverted;
            dateConverted = Convert.ToDateTime(date).ToString("dd/MMM/yyyy");
            return dateConverted;
        }
        
        public string ReturnLongTime(string date)
        {
            string timeConverted;
            timeConverted = Convert.ToDateTime(date).ToString("h:mm:ss tt");
            return timeConverted;
        }
        //return super admin id 
        public Guid SuperAdminID()
        {
            return ReturnGuid("c8f451c1-b573-e411-8a52-68a3c4aec686");
        }
        //method for inserting data into the escalation transaction table
        public void InsertRecordIntoEscalationTransactionsTable(short escalationMasterID, string transactionNumber, int stageAt, Guid approvalStaffID, int nextEscalationTimeHours, bool isClosed, string closeType, Guid? nextApprovalStaffID, Guid? closedByStaffID)
        {
            EscalationTransaction newEscalationTransaction = new EscalationTransaction();
            newEscalationTransaction.escalationtransaction_siEscalationMstID = escalationMasterID;
            newEscalationTransaction.escalationtransaction_vTransactionNumber = transactionNumber;
            newEscalationTransaction.escalationtransaction_iStageAt = stageAt;
            newEscalationTransaction.escalationtransaction_uiApprovalStaffID = approvalStaffID;
            newEscalationTransaction.escalationtransaction_dtDateTimeCreated = DateTime.Now;
            newEscalationTransaction.escalationtransaction_dtExitDateTime = DateTime.Now.AddHours(nextEscalationTimeHours);
            newEscalationTransaction.escalationtransaction_bIsClosed = isClosed;
            newEscalationTransaction.escalationtransaction_vCloseType = closeType;
            if (nextApprovalStaffID != null)
            {
                newEscalationTransaction.escalationtransaction_uiNextApprovalStaffID = nextApprovalStaffID;
            }
            if (closedByStaffID != null)
            {
                newEscalationTransaction.escalationtransaction_uiClosedByStaffID = closedByStaffID;
            }
            newEscalationTransaction.escalationtransaction_bIsTransactionKeptOnHold = false;
            db.EscalationTransactions.InsertOnSubmit(newEscalationTransaction);
            db.SubmitChanges();
        }
        //get leave application years
        public void GetLeaveApplicationYears(DropDownList _dropDown)
        {
            try
            {
                string _currentYear = DateTime.Now.Year.ToString();
                _dropDown.Items.Clear();
                object _getLeaveYears = db.PROC_GetLeaveApplicationYears();
                _dropDown.DataSource = _getLeaveYears;
                _dropDown.Items.Insert(0, new ListItem("ALL"));
                _dropDown.DataTextField = "leave_year";
                _dropDown.DataValueField = "leave_year";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.SelectedValue = _currentYear;//select the current year
                _dropDown.DataBind();
                return;
            }
            catch { }
        }
        //method for capitalizing the first letters of a word in a string
        public string ConvertFirstStringLettersToUpper(string _stringValue)
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(_stringValue.ToLower());
        }
        ////method for returning an esaclation process id by passing the transaction name()
        //public short ReturnEscalationMasterID(string escalationTransactionName)
        //{
        //    return db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_vName.ToLower().Contains(escalationTransactionName.ToLower())).leaveapprovalproc_siLeaveApprovalProcessID;
        //}
        //method for returning an esaclation process id
        public short ReturnEscalationMasterID()
        {
            return Convert.ToInt16(db.LeaveApprovalProcesses.Select(p => p).FirstOrDefault().leaveapprovalproc_siLeaveApprovalProcessID);//for leave application
        }
        //method for returning staff one to be kept informed
        public Guid ReturnEscalationKeepStaffOneInformedID(int _escalationMasterID)
        {
            return (Guid)db.LeaveApprovalProcesses.Single(p => p.leaveapprovalproc_siLeaveApprovalProcessID == _escalationMasterID).leaveapprovalproc_uiKeepStaff1Informed;
        }
        //get locations per country
        public void GetLocations(DropDownList _dropDown, string _displayText, int _countryID)
        {
            try
            {
                _dropDown.Items.Clear();
                object _getStaff;
                _getStaff = db.Locations_Views.Where(p => p.CountryID == _countryID).OrderBy(p => p.Location);
                _dropDown.DataSource = _getStaff;
                _dropDown.Items.Insert(0, new ListItem("-Select " + _displayText + "-"));
                _dropDown.DataTextField = "Location";
                _dropDown.DataValueField = "LocationID";
                _dropDown.AppendDataBoundItems = true;
                _dropDown.DataBind();
                return;
            }
            catch { }
        }

        //update user log for the logged employee
        public void UpdateUserLog(int _userLogId, short? _pageID)
        {
            UserLog updateUserLog = db.UserLogs.Single(p => p.userlog_iUserLogID == _userLogId);
            updateUserLog.userlog_dtLogoutTime = DateTime.Now;
            updateUserLog.userlog_siLastAppPagesID = _pageID;
            db.SubmitChanges();
        }
    }
}