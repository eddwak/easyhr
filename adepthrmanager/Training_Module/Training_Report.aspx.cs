﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using AdeptHRManager.HRManagerClasses;
using System.Data;

namespace AdeptHRManager.Training_Module
{
    public partial class Training_Report : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerReports _hrReports = new HRManagerReports();
        ReportDataSource _reportSource = new ReportDataSource();
        DataTable dt = new DataTable();
        ReportDocument _trainingReport = new ReportDocument();
        string reportPath = "";

        //page init method for viewing training reports
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["ReportType"] != null)
                    ViewTrainingReport();
            }
            catch { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetStaffNames(ddlSingleStaffTrainingAttendedReportStaffName, "Staff Name");//populate staff name dropdown
            }
        }

        //validate view trainings attended per duration report controls
        private string ValidateViewTrainingsAttendedPerDurationReportControls()
        {
            if (_hrClass.isDateValid(txtFromDate.Text.Trim()) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtFromDate) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.isDateValid(txtToDate.Text.Trim()) == false)
            {
                return "Invalid end date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtToDate) == false)
            {
                return "Invalid end date entered!";
            }
            else if (Convert.ToDateTime(txtFromDate.Text.Trim()) > Convert.ToDateTime(txtToDate.Text.Trim()))
            {
                return "Start date cannot be greater than end date!";
            }
            else return "";
        }
        //method for viewing trainings attended during a specified durations report
        private void ViewTrainingsAttendedPerDurationReport()
        {

            ModalPopupExtenderTrainingsAttendedPerDurationReport.Show();
            try
            {
                string _error = ValidateViewTrainingsAttendedPerDurationReportControls();//check for error
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelTrainingReports);
                    return;
                }
                else
                {
                    Session["TrainingFromDate"] = txtFromDate.Text.Trim().ToString();
                    Session["TrainingToDate"] = txtToDate.Text.Trim().ToString();
                    Session["ReportType"] = "TrainingsAttendedPerDuration";
                    Session["ReportTitle"] = "Trainings Attended Per Duration";

                    ModalPopupExtenderTrainingsAttendedPerDurationReport.Hide();
                    ViewTrainingReport();//view the report
                    TabContainerTrainingReports.ActiveTabIndex = 1;//report viewer
                }

            }

            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelTrainingReports);
                return;
            }
        }
        //view trainings attended during a specified duration
        protected void lnkBtnViewTrainingsAttendedPerDurationReport_Click(object sender, EventArgs e)
        {
            ViewTrainingsAttendedPerDurationReport(); return;
        }

        //method for viewing a single staff training attended report
        private void ViewSingleStaffTrainingAttendedReport()
        {

            ModalPopupExtenderSingleStaffTrainingAttendedReport.Show();
            try
            {
                if (ddlSingleStaffTrainingAttendedReportStaffName.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No staff selected. Please select an employee!", this, PanelTrainingReports);
                    return;
                }
                else
                {
                    string _staffID = ddlSingleStaffTrainingAttendedReportStaffName.SelectedValue.ToString();
                    Session["StaffID"] = _staffID.ToString();
                    Session["ReportType"] = "SingleStaffTrainingAttended";
                    Session["ReportTitle"] = "Single Staff Training Attended";

                    ModalPopupExtenderSingleStaffTrainingAttendedReport.Hide();
                    ViewTrainingReport();//view the report
                    TabContainerTrainingReports.ActiveTabIndex = 1;//report viewer
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelTrainingReports);
                return;
            }
        }
        //view single staff training report
        protected void lnkBtnViewSingleStaffTrainingAttendedReport_Click(object sender, EventArgs e)
        {
            ViewSingleStaffTrainingAttendedReport(); return;
        }

        private void ViewTrainingReport()
        {
            if (Session["ReportType"].ToString() == "TrainingsAttendedPerDuration")//TRAININGS ATTENDED PER DURATION REPORT
            {
                string _fromDate = Session["TrainingFromDate"].ToString();
                string _toDate = Session["TrainingToDate"].ToString();
                reportPath = Server.MapPath("~/Training_Module/Trainings_Reports/report_TrainingsAttendedPerDuration.rpt");
                _trainingReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_TrainingsAttendedPerDuration(_fromDate, _toDate);
                _trainingReport.SetDataSource(_reportData);
                string _trainingAttendedBetween = "Training Attended Between " + _fromDate + " and " + _toDate + " Report";
                //_trainingReport.SetParameterValue("AttendedBetween", _trainingAttendedBetween.ToUpper());
                _trainingReport.SetParameterValue("ReportTitle", _trainingAttendedBetween.ToUpper().ToUpper());
                CrystalReportViewerTrainingReports.ReportSource = _trainingReport;
            }
            else if (Session["ReportType"].ToString() == "SingleStaffTrainingAttended")//SINGLE STAFF TRAINING ATTENDED REPORT
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["StaffID"].ToString());//GET STAFF
                reportPath = Server.MapPath("~/Training_Module/Trainings_Reports/report_SingleEmployeeTrainingAttended.rpt");
                _trainingReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_SingleStaffTrainingAttended(_staffID);
                _trainingReport.SetDataSource(_reportData);
                _trainingReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerTrainingReports.ReportSource = _trainingReport;
            }
        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            _trainingReport.Close();
            _trainingReport.Dispose();
            return;
        }
        protected void CrystalReportVieweeTrainingReports_Unload(object sender, EventArgs e)
        {
            _trainingReport.Close();
            _trainingReport.Dispose();
            return;
        }
    }
}