﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Training_Module
{
    public partial class Courses : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayCourses();//load courses
            }
            //add delete course event
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddCourseControls();
            ModalPopupExtenderAddCourse.Show();
            return;
        }

        //clear add course controls
        private void ClearAddCourseControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddCourse);
            HiddenFieldCourseID.Value = "";
            lnkBtnSaveCourse.Visible = true;
            lnkBtnUpdateCourse.Enabled = false;
            lnkBtnUpdateCourse.Visible = true;
            lnkBtnClearCourse.Visible = true;
            lbAddCourseHeader.Text = "Add New Course";
            ImageAddCourseHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            ModalPopupExtenderAddCourse.Show();
            lbCoursesError.Text = "";
            return;
        }
        //validate add coourse details
        private string ValidateAddCourseControls()
        {
            if (txtCourseReference.Text == "")
            {
                txtCourseReference.Focus();
                return "Enter course reference number!";
            }
            else if (txtCourseTitle.Text.Trim() == "")
            {
                txtCourseTitle.Focus();
                return "Enter course title!";
            }
            else return "";
        }
        //save course details
        private void SaveCourseDetails()
        {
            ModalPopupExtenderAddCourse.Show();
            try
            {
                string _error = ValidateAddCourseControls();//check for errors
                if (_error != "")
                {
                    lbCoursesError.Text = _error;
                    return;
                }
                else
                {
                    //save the course details
                    string _courseRef = txtCourseReference.Text.Trim(), _courseTitle = txtCourseTitle.Text.Trim();
                    string _requirements = txtCourseRequirements.Text.Trim();
                    //check if the course has been saved
                    if (db.Courses.Any(p => p.CourseRef.ToLower() == _courseRef.ToLower() && p.CourseTitle.ToLower() == _courseTitle.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed. There exists a course with the details entered.", this, PanelCourses);
                        return;
                    }
                    else
                    {
                        //save course details
                        Course newCourse = new Course();
                        newCourse.CourseRef = _courseRef;
                        newCourse.CourseTitle = _courseTitle;
                        newCourse.Requirements = _requirements;
                        db.Courses.InsertOnSubmit(newCourse);
                        db.SubmitChanges();
                        HiddenFieldCourseID.Value = newCourse.CourseID.ToString();
                        lnkBtnUpdateCourse.Enabled = true;
                        //display courses
                        DisplayCourses();
                        lbCoursesError.Text = "";
                        _hrClass.LoadHRManagerMessageBox(2, "Course details have been successfully saved.", this, PanelCourses);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbCoursesError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again.";
                return;
            }
        }
        //update course details
        private void UpdateCourseDetails()
        {
            ModalPopupExtenderAddCourse.Show();
            try
            {
                string _error = ValidateAddCourseControls();//check for errors
                if (_error != "")
                {
                    lbCoursesError.Text = _error;
                    return;
                }
                else
                {
                    //update the course details
                    int _courseID = Convert.ToInt32(HiddenFieldCourseID.Value);
                    string _courseRef = txtCourseReference.Text.Trim(), _courseTitle = txtCourseTitle.Text.Trim();
                    string _requirements = txtCourseRequirements.Text.Trim();
                    //check if the course has been saved
                    if (db.Courses.Any(p => p.CourseID != _courseID && p.CourseRef.ToLower() == _courseRef.ToLower() && p.CourseTitle.ToLower() == _courseTitle.ToLower()))
                    {
                        lbCoursesError.Text = "Update failed. There exists a course with the details entered.";
                        return;
                    }
                    else
                    {
                        //update course details
                        Course updateCourse = db.Courses.Single(p => p.CourseID == _courseID);
                        updateCourse.CourseRef = _courseRef;
                        updateCourse.CourseTitle = _courseTitle;
                        updateCourse.Requirements = _requirements;
                        db.SubmitChanges();
                        lnkBtnUpdateCourse.Enabled = true;
                        //display courses
                        DisplayCourses();
                        _hrClass.LoadHRManagerMessageBox(2, "Course details have been successfully updated.", this, PanelCourses);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbCoursesError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again.";
                return;
            }
        }
        //save course
        protected void lnkBtnSaveCourse_Click(object sender, EventArgs e)
        {
            SaveCourseDetails();
            return;
        }
        //update course
        protected void lnkBtnUpdateCourse_Click(object sender, EventArgs e)
        {
            UpdateCourseDetails();
            return;
        }
        //update course form
        protected void lnkBtnClearCourse_Click(object sender, EventArgs e)
        {
            ClearAddCourseControls();
            return;
        }
        //display courses
        private void DisplayCourses()
        {
            try
            {
                object _display;
                _display = db.Courses.Select(p => p).OrderBy(p => p.CourseRef);
                gvCourses.DataSourceID = null;
                gvCourses.DataSource = _display;
                Session["gvCoursesData"] = _display;
                gvCourses.DataBind();
                return;
            }
            catch { }
        }
        //page index changing
        protected void gvCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvCourses.PageIndex = e.NewPageIndex;
                Session["gvCoursesPageIndex"] = e.NewPageIndex;
                gvCourses.DataSource = (object)Session["gvCoursesData"];
                gvCourses.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelCourses);
                return;
            }
        }
        //protected void gvCourses_OnLoad(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Session["gvCoursesPageIndex"] == null)//check if page index is empty
        //        {
        //            gvCourses.PageIndex = 0;
        //            return;
        //        }
        //        else
        //        {
        //            //get grid view's page index from the session
        //            int pageIndex = Convert.ToInt32(Session["gvCoursesPageIndex"]);
        //            gvCourses.PageIndex = pageIndex;
        //            return;
        //        }
        //    }
        //    catch { }
        //}
        //check if a course is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvCourses))//check if a course is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a course to edit!", this, PanelCourses);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvCourses.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one course. Select only one course to edit at a time!", this, PanelCourses);
                        return;
                    }
                    foreach (GridViewRow _grv in gvCourses.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFCourseID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit course details
                            LoadEditCourseControls(Convert.ToInt32(_HFCourseID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading course details for edit. " + ex.Message.ToString() + ". Please try again.", this, PanelCourses);
                return;
            }
        }
        // populate the edit course controls
        private void LoadEditCourseControls(int _courseID)
        {
            ClearAddCourseControls();
            Course getCourse = db.Courses.Single(p => p.CourseID == _courseID);
            HiddenFieldCourseID.Value = _courseID.ToString();
            txtCourseReference.Text = getCourse.CourseRef;
            txtCourseTitle.Text = getCourse.CourseTitle;
            txtCourseRequirements.Text = getCourse.Requirements;

            lnkBtnSaveCourse.Visible = false;
            lnkBtnClearCourse.Visible = true;
            lnkBtnUpdateCourse.Enabled = true;
            lbAddCourseHeader.Text = "Edit Courses Details";
            ImageAddCourseHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ModalPopupExtenderAddCourse.Show();
            return;
        }
        //check if a course is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvCourses))//check if a course is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a course to view its details!", this, PanelCourses);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvCourses.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one course. Select only one course to view the details at a time!", this, PanelCourses);
                        return;
                    }
                    foreach (GridViewRow _grv in gvCourses.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFCourseID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view course details
                            LoadViewCourseControls(Convert.ToInt32(_HFCourseID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the selected course details for view purpose ! " + ex.Message.ToString(), this, PanelCourses);
                return;
            }
        }
        // populate the view course controls
        private void LoadViewCourseControls(int _courseID)
        {
            ClearAddCourseControls();
            Course getCourse = db.Courses.Single(p => p.CourseID == _courseID);
            HiddenFieldCourseID.Value = _courseID.ToString();
            txtCourseReference.Text = getCourse.CourseRef;
            txtCourseTitle.Text = getCourse.CourseTitle;
            txtCourseRequirements.Text = getCourse.Requirements;

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddCourse);
            lnkBtnSaveCourse.Visible = false;
            lnkBtnClearCourse.Visible = false;
            lnkBtnUpdateCourse.Visible = false;
            lbAddCourseHeader.Text = "View Course Details";
            ImageAddCourseHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddCourse.Show();
            return;
        }
        //load delete course details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvCourses))//check if there is any course record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a course to delete !", this, PanelCourses);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvCourses.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected course?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected courses?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelCourses);
                return;
            }
        }
        //delete course record
        private void DeleteCourse()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvCourses.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFCourseID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _courseID = Convert.ToInt32(_HFCourseID.Value);

                        Course deleteCourse = db.Courses.Single(p => p.CourseID == _courseID);
                        //check if there are any training schedule associated with the course
                        if (db.TrainingSchedules.Any(p => p.CourseID == _courseID))
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Delete Failed. The selected course is already assosiated with a training schedule!", this, PanelCourses);
                            return;
                        }
                        else
                        {
                            //delete the course if there is no conflict
                            db.Courses.DeleteOnSubmit(deleteCourse);
                            db.SubmitChanges();
                        }
                    }
                }
                //refresh courses after the delete is done
                DisplayCourses();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected course has been deleted.", this, PanelCourses);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected courses have been deleted.", this, PanelCourses);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelCourses);
                return;
            }
        }
        //delete the selected course
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteCourse();//delete selected course
            return;
        }
        //method for searching for course by use of reference number or course title
        private void SearchForCourse()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.Courses.Where(p => (p.CourseRef.ToLower() + p.CourseTitle.ToLower()).Contains(_searchText)).OrderBy(p => p.CourseRef);
                gvCourses.DataSourceID = null;
                gvCourses.DataSource = _searchDetails;
                Session["gvCoursesData"] = _searchDetails;
                gvCourses.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelCourses);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForCourse();
            return;
        }



        ////load courses
        //private void DisplayCourses()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("CourseID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("CourseRef", typeof(string)));
        //    dt.Columns.Add(new DataColumn("CourseTitle", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Trainer", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Venue", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Duration", typeof(string)));
        //    dt.Columns.Add(new DataColumn("CourseSchedule", typeof(string)));
        //    DataRow dr = dt.NewRow();
        //    dr[0] = 1;
        //    dr[1] = "C0001";
        //    dr[2] = "Marketing Products";
        //    dr[3] = "xxxxxxx";
        //    dr[4] = "Nairobi";
        //    dr[5] = "2 Weeks";
        //    dr[6] = "17/Sep/2014 to 01/Oct/2014";
        //    dt.Rows.Add(dr);

        //    DataRow dr2 = dt.NewRow();
        //    dr2[0] = 2;
        //    dr2[1] = "C0002";
        //    dr2[2] = "Sever instalation and Maintainance";
        //    dr2[3] = "xxxxxx";
        //    dr2[4] = "Nairobi";
        //    dr2[5] = "3 Weeks";
        //    dr2[6] = "14/Sep/2014 to 28/Sep/2014";
        //    dt.Rows.Add(dr2);

        //    DataRow dr3 = dt.NewRow();
        //    dr3[0] = 3;
        //    dr3[1] = "C0003";
        //    dr3[2] = "Project Analysis";
        //    dr3[3] = "xxxxxx";
        //    dr3[4] = "Nairobi";
        //    dr3[5] = "4 Weeks";
        //    dr3[6] = "01/Oct/2014 to 29/Oct/2014";
        //    dt.Rows.Add(dr3);

        //    DataRow dr4 = dt.NewRow();
        //    dr4[0] = 4;
        //    dr4[1] = "C0004";
        //    dr4[2] = "Computer Maintainance";
        //    dr4[3] = "xxxxxx";
        //    dr4[4] = "Nairobi";
        //    dr4[5] = "2 Weeks";
        //    dr4[6] = "21/Oct/2014 to 03/Nov/2014";
        //    dt.Rows.Add(dr4);
        //    gvCourses.DataSource = dt;
        //    gvCourses.DataBind();
        //}
        ////load course attendees
        //private void DisplayCourseAttendees()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("CourseAttendeeID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("StaffName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("DepartmentName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Attended", typeof(string)));
        //    dt.Columns.Add(new DataColumn("Comments", typeof(string)));
        //    DataRow dr = dt.NewRow();
        //    dr[0] = 1;
        //    dr[1] = "Dickosn Kiriinya";
        //    dr[2] = "I.T";
        //    dr[3] = "Yes";
        //    dr[4] = "Attended the course";
        //    dt.Rows.Add(dr);

        //    DataRow dr2 = dt.NewRow();
        //    dr2[0] = 2;
        //    dr2[1] = "Richard Ngari";
        //    dr2[2] = "I.T";
        //    dr2[3] = "Yes";
        //    dr2[4] = "";
        //    dt.Rows.Add(dr2);

        //    DataRow dr3 = dt.NewRow();
        //    dr3[0] = 3;
        //    dr3[1] = "Edwin Wambua";
        //    dr3[2] = "I.T";
        //    dr3[3] = "No";
        //    dr3[4] = "There was no time available time for the training.";
        //    dt.Rows.Add(dr3);

        //    DataRow dr4 = dt.NewRow();
        //    dr4[0] = 4;
        //    dr4[1] = "Jimmy Mutungi";
        //    dr4[2] = "H.R";
        //    dr4[3] = "No";
        //    dr4[4] = "";
        //    dt.Rows.Add(dr4);
        //    gvCourseAttendees.DataSource = dt;
        //    gvCourseAttendees.DataBind();
        //}
    }
}