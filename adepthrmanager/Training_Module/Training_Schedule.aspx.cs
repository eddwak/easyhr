﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Training_Module
{
    public partial class Training_Schedule : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlTrainingVenue, 9000, "Course Venue");//display course training venues(9000) available
                _hrClass.GetListingItems(ddlAttendeeDepartment, 3000, "Department");//display departments(3000) available
                GetCourseTitles();//display course titles
                DisplayTrainingSchedules();//load training schedules already saved
            }
            //add delete course event
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
            //add delete course traing attendee event
            ucDeleteCourseTrainingAttendee.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnDeleteCourseTrainingAttendee_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddTrainingScheduleControls();
            PanelTrainingScheduleList.Visible = false;
            PanelAddTrainingSchedule.Visible = true;
            PanelAddCourseTrainingAttendees.Visible = true;
            return;
        }

        //clear add training schedule controls
        private void ClearAddTrainingScheduleControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddTrainingSchedule);
            HiddenFieldTrainingScheduleID.Value = "";
            lnkBtnSaveTrainingSchedule.Visible = true;
            lnkBtnUpdateTrainingSchedule.Enabled = false;
            lnkBtnUpdateTrainingSchedule.Visible = true;
            lnkBtnClearForm.Visible = true;
            lnkBtnSaveCourseTrainingAttendee.Visible = true;
            lnkBtnClearCourseTrainingAttendee.Visible = false;
            lnkBtnUpdateCourseTrainingAttendee.Visible = true;
            lnkBtnUpdateCourseTrainingAttendee.Enabled = false; ;
            lbAddTrainingScheduleHeader.Text = "Add New Training Schedule";
            ImageAddTrainingScheduleHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            ImageButtonTrainingStartDate.Visible = true; ;
            ImageButtonTrainingEndDate.Visible = true;
            DisplayCourseTrainingAttendees(0);//clear course attendee gridview
            gvCourseTrainingAttendees.Columns[5].Visible = true;//hide edit column
            gvCourseTrainingAttendees.Columns[6].Visible = true;//hide delete column 
            txtCourseReference.Enabled = false;
            txtCourseRequirements.Enabled = false;
            return;
        }
        //close add training schedule panel
        protected void ImageButtonCloseAddTrainingSchedule_Click(object sender, EventArgs e)
        {
            PanelAddTrainingSchedule.Visible = false;
            PanelTrainingScheduleList.Visible = true;
            return;
        }
        //load available courses in the courses dropdown
        public void GetCourseTitles()
        {
            try
            {
                ddlCourseTitle.Items.Clear();
                object _getCourseTitle;
                _getCourseTitle = db.Courses.Select(p => p).OrderBy(p => p.CourseTitle);
                ddlCourseTitle.DataSource = _getCourseTitle;
                ddlCourseTitle.Items.Insert(0, new ListItem("-Select Course Title-"));
                ddlCourseTitle.DataTextField = "CourseTitle";
                ddlCourseTitle.DataValueField = "CourseID";
                ddlCourseTitle.AppendDataBoundItems = true;
                ddlCourseTitle.DataBind();
                return;
            }
            catch { }
        }
        //display selected course details
        private void LoadSelectedCourseDetails(int _courseID)
        {
            Course getCourse = db.Courses.Single(p => p.CourseID == _courseID);
            txtCourseReference.Text = getCourse.CourseRef;
            txtCourseRequirements.Text = getCourse.Requirements;
            return;
        }
        //load staff name as attendess based on the selected department
        protected void ddlCourseTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _courseID = 0;
                if (ddlCourseTitle.SelectedIndex != 0)
                    _courseID = Convert.ToInt32(ddlCourseTitle.SelectedValue);
                LoadSelectedCourseDetails(_courseID);//load selected course details
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelTrainingSchedule);
                return;
            }
        }
        //validate add training schedule details
        private string ValidateAddCourseControls()
        {

            if (ddlCourseTitle.SelectedIndex == 0)
            {
                ddlCourseTitle.Focus();
                return "Select course title!";
            }
            else if (txtTrainer.Text.Trim() == "")
            {
                txtTrainer.Focus();
                return "Enter course trainer!";
            }
            else if (ddlTrainingVenue.SelectedIndex == 0)
            {
                ddlTrainingVenue.Focus();
                return "Select course venue!";
            }
            else if (_hrClass.isDateValid(txtTrainingStartDate.Text.Trim()) == false)
            {
                txtTrainingStartDate.Focus();
                return "Invalid start date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtTrainingStartDate) == false)
            {
                return "Invalid start date entered!";
            }
            else if (_hrClass.isDateValid(txtTrainingEndDate.Text.Trim()) == false)
            {
                txtTrainingEndDate.Focus();
                return "Invalid end date entered!";
            }
            else if (_hrClass.ValidateDateTextBox(txtTrainingEndDate) == false)
            {
                return "Invalid end date entered!";
            }
            else if (Convert.ToDateTime(txtTrainingStartDate.Text.Trim()) > Convert.ToDateTime(txtTrainingEndDate.Text.Trim()))
            {
                return "Save failed. Start date cannot be greater than end date!";

            }
            else if (rblTrainingStatus.SelectedIndex == -1)
            {
                rblTrainingStatus.Focus();
                return "Select training status!";
            }
            else return "";
        }
        //save training schedule details
        private void SaveTrainingScheduleDetails()
        {
            try
            {
                string _error = ValidateAddCourseControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    //save the course details
                    int _courseID = Convert.ToInt32(ddlCourseTitle.SelectedValue);
                    string _trainer = txtTrainer.Text.Trim();
                    int _courseVenueID = Convert.ToInt32(ddlTrainingVenue.SelectedValue);
                    DateTime _startDate = Convert.ToDateTime(txtTrainingStartDate.Text), _endDate = Convert.ToDateTime(txtTrainingEndDate.Text);
                    string _comments = txtCourseComments.Text.Trim(), _courseStatus = rblTrainingStatus.SelectedValue;
                    //check if the training schedule has been saved
                    if (db.TrainingSchedules.Any(p => p.CourseID == _courseID && p.Trainer.ToLower() == _trainer.ToLower() && p.VenueID == _courseVenueID && p.StartDate.Value.Date == _startDate.Date && p.EndDate.Value.Date == _endDate.Date))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed. There exists a training schedule with the details entered.", this, PanelTrainingSchedule);
                        return;
                    }
                    else
                    {
                        //save training schedule details
                        TrainingSchedule newTrainingSchedule = new TrainingSchedule();
                        newTrainingSchedule.CourseID = _courseID;
                        newTrainingSchedule.Trainer = _trainer;
                        newTrainingSchedule.VenueID = _courseVenueID;
                        newTrainingSchedule.StartDate = _startDate.Date;
                        newTrainingSchedule.EndDate = _endDate.Date;
                        newTrainingSchedule.Comments = _comments;
                        newTrainingSchedule.Status = _courseStatus;
                        db.TrainingSchedules.InsertOnSubmit(newTrainingSchedule);
                        db.SubmitChanges();
                        HiddenFieldTrainingScheduleID.Value = newTrainingSchedule.TrainingScheduleID.ToString();
                        lnkBtnUpdateTrainingSchedule.Enabled = true;
                        lnkBtnSaveCourseTrainingAttendee.Enabled = true;
                        lnkBtnUpdateCourseTrainingAttendee.Enabled = true;
                        //display courses
                        DisplayTrainingSchedules();
                        //display course attendees

                        _hrClass.LoadHRManagerMessageBox(2, "Training schedule details have been successfully saved.", this, PanelTrainingSchedule);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //update training schedule details
        private void UpdateTrainingScheduleDetails()
        {
            try
            {
                string _error = ValidateAddCourseControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    //update the training schedule details
                    int _trainingScheduleID = Convert.ToInt32(HiddenFieldTrainingScheduleID.Value);
                    int _courseID = Convert.ToInt32(ddlCourseTitle.SelectedValue);
                    string _trainer = txtTrainer.Text.Trim();
                    int _courseVenueID = Convert.ToInt32(ddlTrainingVenue.SelectedValue);
                    DateTime _startDate = Convert.ToDateTime(txtTrainingStartDate.Text), _endDate = Convert.ToDateTime(txtTrainingEndDate.Text);
                    string _requirements = txtCourseRequirements.Text.Trim(), _comments = txtCourseComments.Text.Trim(), _courseStatus = rblTrainingStatus.SelectedValue;
                    //check if the training schedule has been saved
                    if (db.TrainingSchedules.Any(p => p.TrainingScheduleID != _trainingScheduleID && p.CourseID == _courseID && p.Trainer.ToLower() == _trainer.ToLower() && p.VenueID == _courseVenueID && p.StartDate.Value.Date == _startDate.Date && p.EndDate.Value.Date == _endDate.Date))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Update failed. There exists a training schedule with the details entered.", this, PanelTrainingSchedule);
                        return;
                    }
                    else
                    {
                        //update training schedule details
                        TrainingSchedule updateTrainingSchedule = db.TrainingSchedules.Single(p => p.TrainingScheduleID == _trainingScheduleID);
                        updateTrainingSchedule.Trainer = _trainer;
                        updateTrainingSchedule.VenueID = _courseVenueID;
                        updateTrainingSchedule.StartDate = _startDate.Date;
                        updateTrainingSchedule.EndDate = _endDate.Date;
                        updateTrainingSchedule.Comments = _comments;
                        updateTrainingSchedule.Status = _courseStatus;
                        db.SubmitChanges();
                        lnkBtnUpdateTrainingSchedule.Enabled = true;
                        lnkBtnSaveCourseTrainingAttendee.Enabled = true;
                        lnkBtnUpdateCourseTrainingAttendee.Enabled = true;
                        //display training schedules
                        DisplayTrainingSchedules();
                        //display course training attendees
                        _hrClass.LoadHRManagerMessageBox(2, "Training schedule details have been successfully updated.", this, PanelTrainingSchedule);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //save training schedule
        protected void lnkBtnSaveTrainingSchedule_Click(object sender, EventArgs e)
        {
            SaveTrainingScheduleDetails(); return;
        }
        //update training schedule
        protected void lnkBtnUpdateTrainingSchedule_Click(object sender, EventArgs e)
        {
            UpdateTrainingScheduleDetails();
            return;
        }
        //update training schedule form
        protected void lnkBtnClearForm_Click(object sender, EventArgs e)
        {
            ClearAddTrainingScheduleControls();
            return;
        }
        //display training schedules
        private void DisplayTrainingSchedules()
        {
            try
            {
                object _display;
                _display = db.TrainingSchedule_Views.Select(p => p).OrderByDescending(p => p.StartDate);
                gvTrainingSchedules.DataSourceID = null;
                gvTrainingSchedules.DataSource = _display;
                Session["gvTrainingSchedulesData"] = _display;
                gvTrainingSchedules.DataBind();
                return;
            }
            catch { }
        }

        //page index changing
        protected void gvTrainingSchedules_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvTrainingSchedules.PageIndex = e.NewPageIndex;
                Session["gvTrainingSchedulesPageIndex"] = e.NewPageIndex;
                gvTrainingSchedules.DataSource = (object)Session["gvTrainingSchedulesData"];
                gvTrainingSchedules.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelTrainingSchedule); return;
            }
        }
        protected void gvTrainingSchedules_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvTrainingSchedulesPageIndex"] == null)//check if page index is empty
                {
                    gvTrainingSchedules.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvTrainingSchedulesPageIndex"]);
                    gvTrainingSchedules.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //check if a training schedule that is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTrainingSchedules))//check if a training schedulw is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select training schedule to edit!", this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one training schedule. Select only one training schedule to edit at a time!", this, PanelTrainingSchedule);
                        return;
                    }
                    foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFTrainingScheduleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit training schedule details
                            LoadEditTrainingScheduleControls(Convert.ToInt32(_HFTrainingScheduleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the training schedule details for edit. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        // populate the edit training schedule controls
        private void LoadEditTrainingScheduleControls(int _trainingScheduleID)
        {
            ClearAddTrainingScheduleControls();
            PROC_TrainingScheduleResult getTrainingSchedule = db.PROC_TrainingSchedule(_trainingScheduleID).FirstOrDefault();
            HiddenFieldTrainingScheduleID.Value = _trainingScheduleID.ToString();
            ddlCourseTitle.SelectedValue = getTrainingSchedule.CourseID.ToString();
            txtCourseReference.Text = getTrainingSchedule.CourseRef;
            txtCourseRequirements.Text = getTrainingSchedule.Requirements;
            txtTrainer.Text = getTrainingSchedule.Trainer;
            ddlTrainingVenue.SelectedValue = getTrainingSchedule.VenueID.ToString();
            txtTrainingStartDate.Text = _hrClass.ShortDateDayStart(getTrainingSchedule.StartDate.ToString());
            txtTrainingEndDate.Text = _hrClass.ShortDateDayStart(getTrainingSchedule.EndDate.ToString());
            txtCourseComments.Text = getTrainingSchedule.Comments;
            rblTrainingStatus.SelectedValue = getTrainingSchedule.Status;

            //display course training attendees
            DisplayCourseTrainingAttendees(_trainingScheduleID);
            lnkBtnSaveTrainingSchedule.Visible = false;
            lnkBtnClearForm.Visible = true;
            lnkBtnUpdateTrainingSchedule.Enabled = true;
            lnkBtnSaveCourseTrainingAttendee.Enabled = true;
            lbAddTrainingScheduleHeader.Text = "Edit Training Schedule Details";
            ImageAddTrainingScheduleHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            PanelTrainingScheduleList.Visible = false;
            PanelAddTrainingSchedule.Visible = true;
            PanelAddCourseTrainingAttendees.Visible = true;
            return;
        }
        //check if a traaining schedule is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTrainingSchedules))//check if a training schedule is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a training schedule to view its details!", this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one training schedule. Select only one training schedule to view the details at a time!", this, PanelTrainingSchedule);
                        return;
                    }
                    foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFTrainingScheduleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view training schedule details
                            LoadViewTrainingScheduleControls(Convert.ToInt32(_HFTrainingScheduleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the selected training schedule details for view purpose ! " + ex.Message.ToString(), this, PanelTrainingSchedule);
                return;
            }
        }
        // populate view training schedule controls
        private void LoadViewTrainingScheduleControls(int _trainingScheduleID)
        {
            ClearAddTrainingScheduleControls();
            PROC_TrainingScheduleResult getTrainingSchedule = db.PROC_TrainingSchedule(_trainingScheduleID).FirstOrDefault();
            HiddenFieldTrainingScheduleID.Value = _trainingScheduleID.ToString();
            ddlCourseTitle.SelectedValue = getTrainingSchedule.CourseID.ToString();
            txtCourseReference.Text = getTrainingSchedule.CourseRef;
            txtCourseRequirements.Text = getTrainingSchedule.Requirements;
            txtTrainer.Text = getTrainingSchedule.Trainer;
            ddlTrainingVenue.SelectedValue = getTrainingSchedule.VenueID.ToString();
            txtTrainingStartDate.Text = _hrClass.ShortDateDayStart(getTrainingSchedule.StartDate.ToString());
            txtTrainingEndDate.Text = _hrClass.ShortDateDayStart(getTrainingSchedule.EndDate.ToString());
            ImageButtonTrainingStartDate.Visible = false;
            ImageButtonTrainingEndDate.Visible = false;
            txtCourseComments.Text = getTrainingSchedule.Comments;
            rblTrainingStatus.SelectedValue = getTrainingSchedule.Status;
            //display course training attendees
            DisplayCourseTrainingAttendees(_trainingScheduleID);
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddTrainingSchedule);
            lnkBtnSaveTrainingSchedule.Visible = false;
            lnkBtnClearForm.Visible = false;
            lnkBtnUpdateTrainingSchedule.Visible = false;
            lnkBtnSaveCourseTrainingAttendee.Visible = false;
            lnkBtnClearCourseTrainingAttendee.Visible = false;
            lnkBtnUpdateCourseTrainingAttendee.Visible = false;
            lbAddTrainingScheduleHeader.Text = "View Training Schedule Details";
            ImageAddTrainingScheduleHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            PanelAddTrainingSchedule.Visible = true;
            PanelAddCourseTrainingAttendees.Visible = false;
            PanelTrainingScheduleList.Visible = false;
            gvCourseTrainingAttendees.Columns[5].Visible = false;//hide edit column
            gvCourseTrainingAttendees.Columns[6].Visible = false;//hide delete column 
            return;
        }
        //load delete training schedule details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvTrainingSchedules))//check if there is any training schedule record that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a training schedule to delete !", this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected training schedule?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected training schedules?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelTrainingSchedule);
                return;
            }
        }
        //delete training schedule record
        private void DeleteTrainingSchedule()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvTrainingSchedules.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFTrainingScheduleID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _trainingScheduleID = Convert.ToInt32(_HFTrainingScheduleID.Value);

                        TrainingSchedule deleteTrainingSchedule = db.TrainingSchedules.Single(p => p.TrainingScheduleID == _trainingScheduleID);
                        //check if there are any attendees associated with the training
                        if (db.CourseTrainingAttendees.Any(p => p.TrainingScheduleID == _trainingScheduleID))
                        {
                            foreach (CourseTrainingAttendee _deleteCourseTrainingAttendee in db.CourseTrainingAttendees.Where(p => p.TrainingScheduleID == _trainingScheduleID))
                            {
                                db.CourseTrainingAttendees.DeleteOnSubmit(_deleteCourseTrainingAttendee);
                            }
                        }

                        //delete the training schedule if there is no conflict
                        db.TrainingSchedules.DeleteOnSubmit(deleteTrainingSchedule);
                        db.SubmitChanges();
                    }
                }
                //refresh training schedules after the delete is done
                DisplayTrainingSchedules();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected training schedule has been deleted.", this, PanelTrainingSchedule);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected training schedules have been deleted.", this, PanelTrainingSchedule);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelTrainingSchedule);
                return;
            }
        }
        //delete the selected course
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteTrainingSchedule();//delete selected training schedule(s)
            return;
        }
        /*COURSE ATTENDEES*/
        //clear add course training attendees controls
        private void ClearAddCourseTrainingAttendeeControls()
        {
            _hrClass.ClearEntryControls(PanelAddCourseTrainingAttendees);
            HiddenFieldCourseTrainingAttendeeID.Value = "";
            lnkBtnSaveCourseTrainingAttendee.Visible = true;
            lnkBtnUpdateCourseTrainingAttendee.Enabled = false;
            lnkBtnUpdateCourseTrainingAttendee.Visible = true;
            lnkBtnClearCourseTrainingAttendee.Visible = false;
            return;
        }
        //load staff name as attendess based on the selected department
        protected void ddlAttendeeDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _departmentID = 0;
                if (ddlAttendeeDepartment.SelectedIndex != 0)
                    _departmentID = Convert.ToInt32(ddlAttendeeDepartment.SelectedValue);
                _hrClass.GetStaffNames(ddlAttendeeStaff, "Staff Name", _departmentID);//populate  staff name dropdown that are under the selected department
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelTrainingSchedule);
                return;
            }
        }
        //validate add course training attendee controls
        private string ValidateAddCourseTrainingAttendeeControls()
        {
            if (HiddenFieldTrainingScheduleID.Value.ToString() == string.Empty)
                return "Save failed. First save the training schedule before you can add the attendees!";
            else if (ddlAttendeeDepartment.SelectedIndex == 0)
                return "Select attendee department!";
            else if (ddlAttendeeStaff.SelectedIndex == 0)
                return "Select staff to attend the training!";
            else if (rblAttendeeAttended.SelectedIndex == -1)
                return "Select whether the attendee has attended the training or not!";
            else if (rblAttendeeAttended.SelectedIndex == 0 && rblAttendeeCertified.SelectedIndex == -1)
                return "Select whether the attendee was certified or not!";
            else if (rblAttendeeCertified.SelectedIndex == 0 && txtCertification.Text.Trim() == "")
                return "Enter the certification achieved!";
            else return "";
        }
        //save a new course training attendee
        private void SaveCourseTrainingAttendee(int _trainingScheduleID)
        {
            try
            {
                string _error = ValidateAddCourseTrainingAttendeeControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    //check if the selected staff is already added
                    Guid _staffID = _hrClass.ReturnGuid(ddlAttendeeStaff.SelectedValue);
                    if (db.CourseTrainingAttendees.Any(p => p.TrainingScheduleID == _trainingScheduleID && p.StaffID == _staffID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed. The selected staff is already added as an attendee!", this, PanelTrainingSchedule);
                        return;
                    }
                    else
                    {
                        //save the attendee
                        bool _hasAttended = Convert.ToBoolean(rblAttendeeAttended.SelectedValue), _certified = false;
                        string _comments = txtAttendeeComments.Text.Trim(), _certification = txtCertification.Text.Trim();
                        if (rblAttendeeCertified.SelectedIndex == 0)
                            _certified = true;

                        CourseTrainingAttendee newCourseTrainingAttendee = new CourseTrainingAttendee();
                        newCourseTrainingAttendee.TrainingScheduleID = _trainingScheduleID;
                        newCourseTrainingAttendee.StaffID = _staffID;
                        newCourseTrainingAttendee.HasAttended = _hasAttended;
                        newCourseTrainingAttendee.Comments = _comments;
                        newCourseTrainingAttendee.AttendeeCertified = _certified;
                        newCourseTrainingAttendee.Certification = _certification;
                        db.CourseTrainingAttendees.InsertOnSubmit(newCourseTrainingAttendee);
                        db.SubmitChanges();
                        HiddenFieldCourseTrainingAttendeeID.Value = newCourseTrainingAttendee.CourseTrainingAttendeeID.ToString();
                        lnkBtnUpdateCourseTrainingAttendee.Enabled = true;
                        //display course trainung attendees
                        DisplayCourseTrainingAttendees(_trainingScheduleID);
                        _hrClass.LoadHRManagerMessageBox(2, "Course training attendee details have been successfully updated.", this, PanelTrainingSchedule);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //update course training attendee
        private void UpdateCourseTrainingAttendee(int _trainingScheduleID)
        {
            try
            {
                string _error = ValidateAddCourseTrainingAttendeeControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelTrainingSchedule);
                    return;
                }
                else
                {
                    //check if the selected emmloyee is already added
                    Guid _staffID = _hrClass.ReturnGuid(ddlAttendeeStaff.SelectedValue);
                    int _courseTrainingAttendeeID = Convert.ToInt32(HiddenFieldCourseTrainingAttendeeID.Value);
                    if (db.CourseTrainingAttendees.Any(p => p.TrainingScheduleID == _trainingScheduleID && p.StaffID == _staffID && p.CourseTrainingAttendeeID != _courseTrainingAttendeeID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Update failed. The selected staff is already added as an attendee!", this, PanelTrainingSchedule);
                        return;
                    }
                    else
                    {
                        //update the attendee
                        bool _hasAttended = Convert.ToBoolean(rblAttendeeAttended.SelectedValue), _certified = false;
                        string _comments = txtAttendeeComments.Text.Trim(), _certification = txtCertification.Text.Trim();
                        if (rblAttendeeCertified.SelectedIndex == 0)
                            _certified = true;
                        CourseTrainingAttendee updateCourseTrainingAttendee = db.CourseTrainingAttendees.Single(p => p.CourseTrainingAttendeeID == _courseTrainingAttendeeID);
                        updateCourseTrainingAttendee.StaffID = _staffID;
                        updateCourseTrainingAttendee.HasAttended = _hasAttended;
                        updateCourseTrainingAttendee.Comments = _comments;
                        updateCourseTrainingAttendee.AttendeeCertified = _certified;
                        updateCourseTrainingAttendee.Certification = _certification;
                        db.SubmitChanges();
                        //display course training attendees
                        DisplayCourseTrainingAttendees(_trainingScheduleID);
                        _hrClass.LoadHRManagerMessageBox(2, "Course training attendee details have been successfully updated.", this, PanelTrainingSchedule);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //save course training attendee
        protected void lnkBtnSaveCourseTrainingAttendee_Click(object sender, EventArgs e)
        {
            SaveCourseTrainingAttendee(Convert.ToInt32(HiddenFieldTrainingScheduleID.Value));
            return;
        }
        //update course training attendee
        protected void lnkBtnUpdateCourseTrainingAttendee_Click(object sender, EventArgs e)
        {
            UpdateCourseTrainingAttendee(Convert.ToInt32(HiddenFieldTrainingScheduleID.Value));
            return;
        }
        //clear course attendee
        protected void lnkBtnClearCourseTrainingAttendee_Click(object sender, EventArgs e)
        {
            ClearAddCourseTrainingAttendeeControls(); return;
        }
        //display course training attendees
        private void DisplayCourseTrainingAttendees(int _trainingScheduleID)
        {
            try
            {
                object _display;
                _display = db.PROC_CourseTrainingAttendee(_trainingScheduleID);
                gvCourseTrainingAttendees.DataSourceID = null;
                gvCourseTrainingAttendees.DataSource = _display;
                Session["gvCourseTrainingAttendeesData"] = _display;
                gvCourseTrainingAttendees.DataBind();
                return;
            }
            catch { }
        }
        //page index changing
        protected void gvCourseTrainingAttendees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvCourseTrainingAttendees.PageIndex = e.NewPageIndex;
                Session["gvCourseTrainingAttendeesPageIndex"] = e.NewPageIndex;
                gvCourseTrainingAttendees.DataSource = (object)Session["gvCourseTrainingAttendeesData"];
                gvCourseTrainingAttendees.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelTrainingSchedule); return;
            }
        }
        protected void gvCourseTrainingAttendees_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvCourseTrainingAttendeesPageIndex"] == null)//check if page index is empty
                {
                    gvCourseTrainingAttendees.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvCourseTrainingAttendeesPageIndex"]);
                    gvCourseTrainingAttendees.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //course attendees gridview row command details when the edit button is pressed or  delete when the delete button is clicked
        protected void gvCourseTrainingAttendees_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("EditCourseTrainingAttendee") == 0 || e.CommandName.CompareTo("DeleteCourseTrainingAttendee") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);

                    HiddenField _HFCourseTrainingAttendeeID = (HiddenField)gvCourseTrainingAttendees.Rows[ID].FindControl("HiddenField1");
                    int _courseTrainingAttendeeID = Convert.ToInt32(_HFCourseTrainingAttendeeID.Value);
                    if (e.CommandName.CompareTo("EditCourseTrainingAttendee") == 0)//check if we are editing the course training attendee
                    {
                        //load edit course training atendeee details
                        LoadEditCourseTrainingAttendeeControls(_courseTrainingAttendeeID);
                        return;
                    }
                    else if (e.CommandName.CompareTo("DeleteCourseTrainingAttendee") == 0)//check if we are are deleting the course training attendee
                    {
                        HiddenFieldCourseTrainingAttendeeID.Value = _courseTrainingAttendeeID.ToString();
                        string _deleteMessage = "Are you sure you want to delete the training attendee?";
                        _hrClass.LoadHRManagerConfirmationMessageBox(ucDeleteCourseTrainingAttendee, _deleteMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //load edit course attendee controls
        private void LoadEditCourseTrainingAttendeeControls(int _courseAttendeeID)
        {
            int _courseID = Convert.ToInt32(HiddenFieldTrainingScheduleID.Value);//get course id
            HiddenFieldCourseTrainingAttendeeID.Value = _courseAttendeeID.ToString();
            PROC_CourseTrainingAttendeeResult getCourseAttendee = db.PROC_CourseTrainingAttendee(_courseID).Single(p => p.CourseTrainingAttendeeID == _courseAttendeeID);
            int _departmentID = (int)getCourseAttendee.DepartmentID;
            ddlAttendeeDepartment.SelectedValue = _departmentID.ToString();
            _hrClass.GetStaffNames(ddlAttendeeStaff, "Staff Name", _departmentID);//populate  staff name dropdown that are under the selected department
            ddlAttendeeStaff.SelectedValue = getCourseAttendee.StaffID.ToString();
            rblAttendeeAttended.SelectedValue = getCourseAttendee.HasAttended.ToString();
            rblAttendeeCertified.SelectedValue = getCourseAttendee.AttendeeCertified.ToString();
            txtCertification.Text = getCourseAttendee.Certification;
            txtAttendeeComments.Text = getCourseAttendee.Comments;
            lnkBtnSaveCourseTrainingAttendee.Visible = false;
            lnkBtnUpdateCourseTrainingAttendee.Enabled = true;
            lnkBtnClearCourseTrainingAttendee.Visible = true;
            return;
        }
        //method for deleting course training attendee
        private void DeleteCourseTrainingAttendee()
        {
            try
            {
                int _trainingScheduleID = Convert.ToInt32(HiddenFieldTrainingScheduleID.Value);
                int _courseTrainingAttendeeID = Convert.ToInt32(HiddenFieldCourseTrainingAttendeeID.Value);
                CourseTrainingAttendee deleteCourseTrainingAttendee = db.CourseTrainingAttendees.Single(p => p.CourseTrainingAttendeeID == _courseTrainingAttendeeID);
                db.CourseTrainingAttendees.DeleteOnSubmit(deleteCourseTrainingAttendee);
                db.SubmitChanges();
                DisplayCourseTrainingAttendees(_trainingScheduleID);
                _hrClass.LoadHRManagerMessageBox(2, "Course training attendee selected has been successfully deleted.", this, PanelTrainingSchedule);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed. " + ex.Message.ToString() + " Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //delete course training attendee
        protected void lnkBtnDeleteCourseTrainingAttendee_Click(object sender, EventArgs e)
        {
            DeleteCourseTrainingAttendee();//delete course training attendee
            ClearAddCourseTrainingAttendeeControls();//clear entry controls
            return;
        }

        //method for searching for training schedules by use of reference number or course title
        private void SearchForTrainingSchedule()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.TrainingSchedule_Views.Where(p => (p.CourseRef.ToLower() + p.CourseTitle.ToLower()).Contains(_searchText)).OrderBy(p => p.StartDate);
                gvTrainingSchedules.DataSourceID = null;
                gvTrainingSchedules.DataSource = _searchDetails;
                Session["gvTrainingSchedulesData"] = _searchDetails;
                gvTrainingSchedules.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelTrainingSchedule);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForTrainingSchedule();
            return;
        }
    }
}