﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager
{
    public partial class Admin_Login : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserName.Text = "admin";//for test
            txtPassword.Text = "Admin1!";//for test
        }
        //perform submit user login
        private void SubmitUserLogin()
        {
            try
            {
                string _password = txtPassword.Text.Trim();
                string _userName = txtUserName.Text.Trim().ToLower();
                if (_password != "")
                    txtPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                else txtPassword.Text = "";
                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.Focus();
                    lbError.Text = "Enter user name !";
                    return;
                }
                else if (txtPassword.Text.Trim() == "")
                {
                    txtPassword.Focus();
                    lbError.Text = "Enter  password !";
                    return;
                }
                else if (db.Users.Any(p => p.UserName.ToLower() == _userName) == false)
                {
                    txtUserName.Focus();
                    lbError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else
                {
                    //get the user with the username entered
                    Users_View getUser = db.Users_Views.Single(p => p.UserName.Trim().ToLower() == _userName);
                    string _getPassword = getUser.Password;
                    byte _accountStatus = Convert.ToByte(getUser.IsActive);//get user account status
                    if (_accountStatus == 0)//
                    {
                        txtUserName.Focus();
                        lbError.Text = "Login failed. The user account is no longer active. Contact the administrator for help !";
                        return;
                    }
                    else
                    {

                        //get the user login attempts
                        Int16 _loginAttempts = (short)getUser.LoginAttempts;
                        if (_loginAttempts == 3)
                        {
                            txtUserName.Focus();
                            lbError.Text = "Your account has been de-activated beacuse you have exhausted the defined login attempts.<br>Contact the administrator for help!";
                            return;
                        }
                        else
                        {
                            //validate user entry details
                            if (_hrClass.ValidateHashAndEncrytPwd(_password, _getPassword) == true && _hrClass.ValidatePassword(_password, _getPassword) == true)
                            {
                                //get role associated with the user
                                string _userRole = db.Roles_Views.Single(p => p.RoleID == getUser.UserRoleID).RoleName;
                                if (_userRole != "Administrator")
                                {
                                    lbError.Text = "Login failed. Your account does not have access to this module!";
                                    return;
                                }
                                else
                                {
                                    FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, cbRememberMe.Checked);
                                    //Get the requested page from the url
                                    bool rememberUserName = cbRememberMe.Checked;
                                    HttpContext.Current.Session["LoggedUserID"] = getUser.UserID;//pass user id
                                    HttpContext.Current.Session["LoggedStaffID"] = getUser.StaffID;//pass staff id
                                    HttpContext.Current.Session["LoggedStaffName"] = getUser.StaffName;//pass staff name
                                    HttpContext.Current.Session["LoggedStaffPostedAtID"] = getUser.PostedAtID;//pass staff posted at ID
                                    //update user account details after login
                                    getUser.LoginAttempts = 0;
                                    db.SubmitChanges();
                                    //redirect the user to the default page
                                    Response.Redirect("~/Default.aspx");
                                    return;
                                }
                            }
                            else
                            {
                                //update login attempts made
                                _loginAttempts++;//increase login attempts by one
                                getUser.LoginAttempts = _loginAttempts;
                                db.SubmitChanges();
                                int _attemptsLeft = (Convert.ToInt16(3) - Convert.ToInt16(getUser.LoginAttempts));

                                //count the  number of login
                                string _attemptsMsg = "";
                                if (_attemptsLeft > 1)
                                    _attemptsMsg = " login attempts ";
                                else
                                    _attemptsMsg = " login attempt ";
                                lbLoginAttempts.Text = "<br><br>You are left with " + _attemptsLeft + _attemptsMsg + "out of 3 attempts!";

                                lbError.Text = "Invalid username and/or password entered. Please try again!";
                                txtUserName.Focus();
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Login failed. " + ex.Message.ToString() + " Please try again !";
            }
        }
        //login user
        protected void lnkBtnLogin_Click(object sender, EventArgs e)
        {
            SubmitUserLogin();
            return;
        }
    }
}