﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Web.Security;
using System.Net;

namespace AdeptHRManager
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {

        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Init(object sender, EventArgs e)
        {
            ////session for testing
            ////Session["LoggedStaffID"] = "25af7b04-076f-e411-8c43-68a3c4aec686";//d
            //Session["LoggedStaffID"] = "c8f451c1-b573-e411-8a52-68a3c4aec686";//admin staff id
            //Session["LoggedUserID"] = "ee55753b-3776-e411-8a52-68a3c4aec686";//admin user id
            //Session["LoggedStaffPostedAtID"] = 2001;//posting location 
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //Response.Cache.SetValidUntilExpires(false);
            //Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetNoStore();

            ChangeMainMenuActiveLinkCSS();//change active main menu css
            GetLoggedUser();//get logged in user
            if (!IsPostBack)
            {
                //disable main menu items for modules that the user is not allowed to access an allow the one that he/she has access to
                CheckModulesAllowedAccess();
                //hide menuitems link buttons the user is not supposed to access
                HideMenuItemsHyperLinksNotSupposedToBeAccessedByUser();
                ////restrict the user from accessing the page incase the user is not allowed access the page
                //CheckLoggedUserPageAccess(Page.AppRelativeVirtualPath);
            }

        }
        private void GetLoggedUser()
        {
            try
            {
                var _staffSession = HttpContext.Current.Session["LoggedStaffID"];
                //check if the staff is logged in
                if (_staffSession == null)
                {
                    //HttpContext.Current.Response.Redirect("~/Login.aspx");
                    if (Page.AppRelativeVirtualPath.ToLower() != "~/default.aspx")//check if we are not on the default
                        HttpContext.Current.Response.Redirect("~/Default.aspx");
                    lnkBtnChangePassword.Visible = false;
                    lnkBtnSignOut.Visible = false;
                    lnkBtnSignIn.Visible = true;
                    lbLoggedUserName.InnerText = "Welcome Guest";
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    // string _staffName = _hrClass.ReturnFirstName(Session["LoggedStaffName"].ToString());
                    var _staffName = _hrClass.ReturnFirstName(HttpContext.Current.Session["LoggedStaffName"].ToString());
                    if (_staffName == "Super")
                    {
                        lbLoggedUserName.InnerText = "Welcome Administrator, ";
                    }
                    else
                    {
                        lbLoggedUserName.InnerText = "Welcome, " + _staffName;
                    }
                    lnkBtnChangePassword.Visible = true;
                    lnkBtnSignOut.Visible = true;
                    lnkBtnSignIn.Visible = false;
                    return;
                }
            }
            catch { }
        }
        //change main menu css based on the selected page/ module under wich hpg fals
        private void ChangeMainMenuActiveLinkCSS()
        {
            //string _virtualPath = Page.AppRelativeVirtualPath;
            //string _module = _virtualPath.Substring(0, _virtualPath.LastIndexOf("/")).Replace("~", "").Replace("/", "").ToLower();//get module part
            //if (_module == "HR_Module".ToLower())
            //    HyperLinkHRModuleMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Leave_and_Attendance".ToLower())
            //    HyperLinkLeaveAndAttendanceMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Recruitment_Module".ToLower())
            //    HyperLinkRecruitmentModuleMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Payroll_Module".ToLower())
            //    HyperLinkPayrollModuleMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Training_Module".ToLower())
            //    HyperLinkTrainingModuleMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Performance_Review".ToLower())
            //    HyperLinkPerformanceReviewMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Settings_Module".ToLower())
            //    HyperLinkSettingsModuleMainMenu.CssClass = "active-menuitem";
            //else if (_module == "Security_Module".ToLower())
            //    HyperLinkSecurityModuleMainMenu.CssClass = "active-menuitem";
            //else
            //    HyperLinkHomeMenu.CssClass = "active-menuitem";
            //return;
        }
        //check and allow user access to the modules that the user is supposed to access
        private void CheckModulesAllowedAccess()
        {
            try
            {
                Guid _loggedUserID = _hrClass.ReturnGuid(Session["LoggedUserID"].ToString());
                if (db.UserAccessRights.Any(p => p.UserID == _loggedUserID))//check if therevare any access rights set
                {
                    //get modules assigned to the user
                    string _assignedModules = db.UserAccessRights.Single(p => p.UserID == _loggedUserID).AccessRightModules;
                    var _listModulues = _assignedModules.Split(',').Select(p => p);//get array for the assigned modules

                    //get main menu HyperLinks in PanelMainMenu 
                    //foreach (Control _control in PanelMainMenu.Controls)
                    //{
                    //    if ((_control.GetType() == typeof(HyperLink)))
                    //    {
                    //        HyperLink _mainMenuHyperLink = ((HyperLink)_control);
                    //        //check if the user is assigned the module
                    //        foreach (string _assignedModule in _listModulues)
                    //        {
                    //            if (_mainMenuHyperLink.ToolTip.Trim() == _assignedModule.Trim())
                    //            {
                    //                _mainMenuHyperLink.Visible = true;
                    //                _mainMenuHyperLink.Attributes.Add("onclick", ";return false;");//prevent the HyperLink from performing an autopostback
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            catch { }
        }
        //class for geting menu items link buttons and their postback urls
        public class NavigationHyperLinkIDs
        {
            public string _linkId { get; set; }
            public string _linkUrl { get; set; }
        }
        //hide menu link items that the user id not suposed to access
        private void HideMenuItemsHyperLinksNotSupposedToBeAccessedByUser()
        {
            //var _links = new List<NavigationHyperLinkIDs>();//va
            ////get menuitems HyperLinks in the HR module menu panel
            //foreach (Control _control in PanelHRModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitems HyperLinks in recruitment module menu panel
            //foreach (Control _control in PanelRecruitmentModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitems HyperLinks in payroll data module menu panel
            //foreach (Control _control in PanelPayrollModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitem link buttons in  training module menu panel
            //foreach (Control _control in PanelTrainingModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitems HyperLinks in performance review menu panel
            //foreach (Control _control in PanelPerformanceReviewModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitems link buttons in settings menu panel
            //foreach (Control _control in PanelSettingsModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get menuitems link buttons in security menu panel
            //foreach (Control _control in PanelSecurityModule.Controls)
            //{
            //    if ((_control.GetType() == typeof(HyperLink)))
            //    {
            //        _links.Add(new NavigationHyperLinkIDs
            //        {
            //            _linkId = _control.ID,
            //            _linkUrl = ((HyperLink)(_control)).NavigateUrl.ToString()
            //        });
            //    }
            //}
            ////get logeed in user id
            //Guid _userID = _hrClass.ReturnGuid(Session["LoggedUserID"].ToString());
            //foreach (var _navigationLink in _links.ToArray())
            //{
            //    foreach (PROC_UserPagesAccessResult _userPageAccess in db.PROC_UserPagesAccess(_userID))
            //    {
            //        if (_navigationLink._linkUrl == _userPageAccess.apppages_Url)//check if the user is allowed to view the page
            //        {
            //            _links.Remove(_navigationLink);//remove the link for the list of the links
            //        }
            //    }
            //}
            ////loop through the remaining links and hide them so that the user cannot click on them
            //foreach (var _nav in _links.ToArray())
            //{
            //    HyperLink _navigationHyperLink = new HyperLink();
            //    string _HyperLinkID = _nav._linkId;
            //    //check if the menuitem link button is on the HR module menu panel
            //    _navigationHyperLink = (HyperLink)PanelHRModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the menuitem link button is in the recruitment module menu panel
            //        _navigationHyperLink = (HyperLink)PanelRecruitmentModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the menuitem link button is in the payroll module menu panel
            //        _navigationHyperLink = (HyperLink)PanelPayrollModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the menuitem link button is in the training menu panel
            //        _navigationHyperLink = (HyperLink)PanelTrainingModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the meniitem link button is in the performance review menu panel
            //        _navigationHyperLink = (HyperLink)PanelPerformanceReviewModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the meniitem link button is in the setings menu panel
            //        _navigationHyperLink = (HyperLink)PanelSettingsModule.FindControl(_HyperLinkID);
            //    if (_navigationHyperLink == null)
            //        //check if the meniitem link button is in the security menu panel
            //        _navigationHyperLink = (HyperLink)PanelSecurityModule.FindControl(_HyperLinkID);
            //    //check if the HyperLink has been found
            //    if (_navigationHyperLink != null)
            //        _navigationHyperLink.Visible = false;
            //}
        }
        //check logged user page access rights
        public void CheckLoggedUserPageAccess(string _pageUrl)
        {
            try
            {
                if (_pageUrl.ToLower() == "~/default.aspx" || _pageUrl.ToLower() == "~//login.aspx" || _pageUrl.ToLower() == "~/security_module/access_denied.aspx") { }
                else
                {
                    Guid _userID = _hrClass.ReturnGuid(HttpContext.Current.Session["LoggedUserID"].ToString());
                    int _applicationPageID = 0;
                    if (db.AppPages.Any(p => p.apppages_Url == _pageUrl) == false)//check if the page exists
                    {
                        Session["UserAccessDeniedMessage"] = "The page you are trying to access is not installed or saved. Contact the administrator for help!";
                        Response.Redirect("~/Security_Module/Access_Denied.aspx");
                        return;
                    }
                    else
                    {
                        //get the page id for the url
                        _applicationPageID = db.AppPages.Single(p => p.apppages_Url == _pageUrl).apppages_PageID;
                        if (db.UserPagesAccesses.Any(p => p.apppages_PageID == _applicationPageID && p.UserID == _userID) == false)
                        {
                            Session["UserAccessDeniedMessage"] = "Access denied. You do not have the required access rights to access the selected page.<br>Contact the administrator for help!";
                            Response.Redirect("~/Security_Module/Access_Denied.aspx");
                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Session["UserAccessDeniedMessage"] = "Access Denied. " + ex.Message.ToString()+"!!";
                //Response.Redirect("~/Security_Module/Access_Denied.aspx"); return;
            }
        }
        //clear session
        public void ClearSessions()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session["LoggedUserID"] = null;
            HttpContext.Current.Session["LoggedStaffID"] = null;
            HttpContext.Current.Session["LoggedStaffName"] = null;
            //HttpContext.Current.Response.Redirect("~/Login.aspx");
            if (Page.AppRelativeVirtualPath.ToLower() != "~/default.aspx")//check if we are not on the default
                HttpContext.Current.Response.Redirect("~/Default.aspx");
        }
        //sign out logged user
        public void StaffLogOut()
        {
            //ClearSessions(); return;
            try
            {
                string _pageURL = Page.AppRelativeVirtualPath;
                int _userLogId = Convert.ToInt32(Session["LoggedUserLogID"]);
                short _pageID = 0;

                if (_pageURL == "~/Default.aspx")
                {
                    //update logout time only
                    _hrClass.UpdateUserLog(_userLogId, null);
                    db.SubmitChanges();
                    ClearSessions();
                    return;
                }
                else
                {
                    //get page id
                    _pageID = db.AppPages.Single(p => p.apppages_Url.ToString().ToLower().Trim() == _pageURL.ToLower().Trim()).apppages_PageID;
                    _hrClass.UpdateUserLog(_userLogId, _pageID);
                    ClearSessions();
                    return;
                }
            }
            catch { ClearSessions(); }
        }
        protected void lnkBtnSignOut_Click(object sender, EventArgs e)
        {
            StaffLogOut();
            return;
        }
        //perform submit user login
        private void SubmitStaffUserLogin()
        {
            ModalPopupExtenderUserSignIn.Show();
            try
            {
                string _password = txtUserPassword.Text.Trim();
                string _userName = txtUserName.Text.Trim().ToLower();
                lbSignInAttempts.Text = "";
                if (_password != "")
                    txtUserPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                else txtUserPassword.Text = "";
                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.Focus();
                    lbSignInError.Text = "Enter user name !";
                    return;
                }
                else if (txtUserPassword.Text.Trim() == "")
                {
                    txtUserPassword.Focus();
                    lbSignInError.Text = "Enter  password !";
                    return;
                }
                else if (_userName == "admin")//check if its the admin 
                {
                    lbSignInError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else if (db.Users.Any(p => p.UserName.ToLower() == _userName) == false)
                {
                    txtUserName.Focus();
                    lbSignInError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else
                {
                    //get the user with the username entered
                    Users_View getUser = db.Users_Views.Single(p => p.UserName.Trim().ToLower() == _userName);
                    string _getPassword = getUser.Password;
                    byte _accountStatus = Convert.ToByte(getUser.IsActive);//get user account status
                    if (_accountStatus == 0)//
                    {
                        txtUserName.Focus();
                        lbSignInError.Text = "Login failed. The user account is no longer active. Contact the administrator for help !";
                        return;
                    }
                    else
                    {
                        //get the user login attempts
                        Int16 _loginAttempts = (short)getUser.LoginAttempts;
                        if (_loginAttempts == 3)
                        {
                            txtUserName.Focus();
                            lbSignInError.Text = "Your account has been de-activated beacuse you have exhausted the defined login attempts.<br>Contact the administrator for help!";
                            return;
                        }
                        else
                        {
                            //validate user entry details
                            if (_hrClass.ValidateHashAndEncrytPwd(_password, _getPassword) == true && _hrClass.ValidatePassword(_password, _getPassword) == true)
                            {
                                FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, cbUserRememberMe.Checked);
                                //Get the requested page from the url
                                bool rememberUserName = cbUserRememberMe.Checked;
                                HttpContext.Current.Session["LoggedUserID"] = getUser.UserID;//pass user id
                                HttpContext.Current.Session["LoggedStaffID"] = getUser.StaffID;//pass staff id
                                HttpContext.Current.Session["LoggedStaffName"] = getUser.StaffName;//pass staff name
                                HttpContext.Current.Session["LoggedStaffPostedAtID"] = getUser.PostedAtID;//pass staff posted at ID
                                //update user account details after login
                                getUser.LoginAttempts = 0;
                                db.SubmitChanges();
                                //redirect the user to the default page
                                //Response.Redirect("~/User_Module/PersonalInformation.aspx");
                                string _defaultPage = "~/User_Module/PersonalInformation.aspx";

                                Guid _staffID = (Guid)getUser.StaffID;
                                //login user
                                LogInUser(_staffID, _defaultPage);
                                return;
                            }
                            else
                            {
                                //update login attempts made
                                _loginAttempts++;//increase login attempts by one
                                getUser.LoginAttempts = _loginAttempts;
                                db.SubmitChanges();
                                int _attemptsLeft = (Convert.ToInt16(3) - Convert.ToInt16(getUser.LoginAttempts));

                                //count the  number of login
                                string _attemptsMsg = "";
                                if (_attemptsLeft > 1)
                                    _attemptsMsg = " login attempts ";
                                else
                                    _attemptsMsg = " login attempt ";
                                lbSignInAttempts.Text = "<br><br>You are left with " + _attemptsLeft + _attemptsMsg + "out of 3 attempts!";

                                lbSignInError.Text = "Invalid username and/or password entered. Please try again!";
                                txtUserName.Focus();
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbSignInError.Text = "Login failed. " + ex.Message.ToString() + " Please try again !";
                return;
            }
        }

        //perform submit admin user login
        private void SubmitAdminUserLogin()
        {
            ModalPopupExtenderUserSignIn.Show();
            try
            {
                string _password = txtUserPassword.Text.Trim();
                string _userName = txtUserName.Text.Trim().ToLower();
                if (_password != "")
                    txtUserPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                else txtUserPassword.Text = "";
                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.Focus();
                    lbSignInError.Text = "Enter user name !";
                    return;
                }
                else if (txtUserPassword.Text.Trim() == "")
                {
                    txtUserPassword.Focus();
                    lbSignInError.Text = "Enter  password !";
                    return;
                }
                else if (db.Users.Any(p => p.UserName.ToLower() == _userName) == false)
                {
                    txtUserName.Focus();
                    lbSignInError.Text = "Invalid username and/or password entered.";
                    return;
                }
                else
                {
                    //get the user with the username entered
                    Users_View getUser = db.Users_Views.Single(p => p.UserName.Trim().ToLower() == _userName);
                    string _getPassword = getUser.Password;
                    byte _accountStatus = Convert.ToByte(getUser.IsActive);//get user account status
                    if (_accountStatus == 0)//
                    {
                        txtUserName.Focus();
                        lbSignInError.Text = "Login failed. The user account is no longer active. Contact the administrator for help !";
                        return;
                    }
                    else
                    {

                        //get the user login attempts
                        Int16 _loginAttempts = (short)getUser.LoginAttempts;
                        if (_loginAttempts == 3)
                        {
                            txtUserName.Focus();
                            lbSignInError.Text = "Your account has been de-activated beacuse you have exhausted the defined login attempts.<br>Contact the administrator for help!";
                            return;
                        }
                        else
                        {
                            //validate user entry details
                            if (_hrClass.ValidateHashAndEncrytPwd(_password, _getPassword) == true && _hrClass.ValidatePassword(_password, _getPassword) == true)
                            {
                                //get role associated with the user
                                string _userRole = db.Roles_Views.Single(p => p.RoleID == getUser.UserRoleID).RoleName;
                                if (_userRole != "Administrator")
                                {
                                    lbSignInError.Text = "Login failed. Your account does not have access to this module!";
                                    return;
                                }
                                else
                                {
                                    FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, cbUserRememberMe.Checked);
                                    //Get the requested page from the url
                                    bool rememberUserName = cbUserRememberMe.Checked;
                                    HttpContext.Current.Session["LoggedUserID"] = getUser.UserID;//pass user id
                                    HttpContext.Current.Session["LoggedStaffID"] = getUser.StaffID;//pass staff id
                                    HttpContext.Current.Session["LoggedStaffName"] = getUser.StaffName;//pass staff name
                                    HttpContext.Current.Session["LoggedStaffPostedAtID"] = getUser.PostedAtID;//pass staff posted at ID
                                    //update user account details after login
                                    getUser.LoginAttempts = 0;
                                    db.SubmitChanges();
                                    //redirect the user to the default page
                                    string _defaultPage = "~/default.aspx";
                                    Guid _staffID = (Guid)getUser.StaffID;
                                    //login user
                                    LogInUser(_staffID, _defaultPage);
                                    return;
                                }
                            }
                            else
                            {
                                //update login attempts made
                                _loginAttempts++;//increase login attempts by one
                                getUser.LoginAttempts = _loginAttempts;
                                db.SubmitChanges();
                                int _attemptsLeft = (Convert.ToInt16(3) - Convert.ToInt16(getUser.LoginAttempts));

                                //count the  number of login
                                string _attemptsMsg = "";
                                if (_attemptsLeft > 1)
                                    _attemptsMsg = " login attempts ";
                                else
                                    _attemptsMsg = " login attempt ";
                                lbSignInAttempts.Text = "<br><br>You are left with " + _attemptsLeft + _attemptsMsg + "out of 3 attempts!";

                                lbSignInError.Text = "Invalid username and/or password entered. Please try again!";
                                txtUserName.Focus();
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lbSignInError.Text = "Login failed. " + ex.Message.ToString() + " Please try again !";
                return;
            }
        }
        //create new user log record
        private void CreateUserLog(Guid _staffID)
        {
            UserLog newUserLog = new UserLog();
            newUserLog.userlog_uiStaffID = _staffID;
            newUserLog.userlog_dtLoginTime = DateTime.Now;
            newUserLog.userlog_vMachineIP = HttpContext.Current.Request.UserHostName;//get ip address
            newUserLog.userlog_vMachineName = Dns.GetHostName();//WindowsIdentity.GetCurrent().Name.ToString();//Environment.MachineName;//get machine name
            newUserLog.userlog_vMachineUser = HttpContext.Current.Request.LogonUserIdentity.Name;//get the logged machene user
            //newUserLog.userlog_vMachineUser = Environment.UserName;
            //newUserLog.userlog_vMachineUser = HttpContext.Current.User.Identity.Name.ToString();
            //newUserLog.userlog_siModulesID = Convert.ToInt16(ddlModule.SelectedValue);
            //newUserLog.userlog_dtLogoutTime
            //newUserLog.userlog_siLastAppPagesID
            db.UserLogs.InsertOnSubmit(newUserLog);
            db.SubmitChanges();
            Session["LoggedUserLogID"] = newUserLog.userlog_iUserLogID;//create as session for user logid to be used when logging out
        }
        //log user

        private void LogInUser(Guid _staffID, string _defaultPage)
        {
            string pageRedirect = _defaultPage;
            int lastUserLogID = 0;
            //get lastUserLogID for the employee for the module selected
            try
            {
                lastUserLogID = Convert.ToInt16(db.UserLogs.Where(p => p.userlog_uiStaffID == _staffID).Max(p => p.userlog_iUserLogID));
            }
            catch { }
            if (lastUserLogID == 0)//
            {
                CreateUserLog(_staffID);//create a new user log
                Response.Redirect(pageRedirect);
                return;
            }
            else
            {
                //get last application page id accessed
                int lastAppPageID = Convert.ToInt32(db.UserLogs.Single(p => p.userlog_iUserLogID == lastUserLogID).userlog_siLastAppPagesID);
                if (lastAppPageID == 0)//check if the last accessed page was saved
                {
                    CreateUserLog(_staffID);
                    //Response.Redirect(pageRedirect);
                    //return;
                }
                else
                {
                    //GET LAST PAGE URL
                    //string _lastPageUrl = db.AppPages.Single(p => p.apppages_PageID == lastAppPageID).apppages_Url;
                    CreateUserLog(_staffID);
                    //Response.Redirect(_lastPageUrl);
                    //return;
                }
                Response.Redirect(pageRedirect);
                return;
            }
        }
        //submit user sign in
        protected void lnkBtnSubmitSignIn_Click(object sender, EventArgs e)
        {
            if (rblUserLoginAs.SelectedIndex == 0)
            {
                //txtUserName.Text = "admin";
                //txtUserPassword.Text = "Admin1!";
                //submit hr/ admin sign in
                SubmitAdminUserLogin(); return;
            }
            else
            {
                //txtUserName.Text = "kirinya";
                //txtUserPassword.Text = "Kirinya1!";
                SubmitStaffUserLogin();
                return;
            }
        }
        //load popup for changing the password
        protected void lnkBtnChangePassword_Click(object sender, EventArgs e)
        {
            ClearChangePasswordControls();
            ModalPopupExtenderChangePassword.Show();
            return;
        }
        //clear change passsword controls
        public void ClearChangePasswordControls()
        {
            txtOldPassword.Text = "";
            txtNewPassword.Text = "";
            txtConfirmPassword.Text = "";
            lbChangePasswordError.Text = "";
        }
        //change logged user password
        public void ChangeLoggedUserPassword()
        {
            ModalPopupExtenderChangePassword.Show();
            try
            {
                Guid _userID = _hrClass.ReturnGuid(Session["LoggedUserID"].ToString());
                string _oldPassword = txtOldPassword.Text.Trim(), _newPassword = txtNewPassword.Text.Trim(), _confirmationPassword = txtConfirmPassword.Text.Trim();

                ////maintain the old password entered in case of a postback
                if (_oldPassword != "")
                    txtOldPassword.Attributes.Add("value", _oldPassword);
                //maintain the new password entered in case of a postback
                if (_newPassword != "")
                    txtNewPassword.Attributes.Add("value", _newPassword);
                //maintain the old password entered in case of a postback
                if (_confirmationPassword != "")
                    txtConfirmPassword.Attributes.Add("value", _confirmationPassword);

                //validate change password controls
                if (_oldPassword == "")
                {
                    txtOldPassword.Focus();
                    lbChangePasswordError.Text = "Enter your old password!";
                    return;
                }
                else if (_newPassword == "")
                {
                    lbChangePasswordError.Text = "Please enter new password!";
                    txtNewPassword.Focus();
                    return;
                }
                else if (_hrClass.IsValidPassword(_newPassword) == false)
                {
                    txtNewPassword.Focus();
                    lbChangePasswordError.Text = "Password must be 6 characters long with at least one numeric,one uppercase character and one special character!";
                }
                else if (_confirmationPassword == "")
                {
                    lbChangePasswordError.Text = "Please enter confirmation password!!";
                    txtConfirmPassword.Focus();
                    return;
                }
                else if (_confirmationPassword != _newPassword)
                {
                    lbChangePasswordError.Text = "Confirmation password does not match with the new password entered!";
                    txtConfirmPassword.Focus();
                    return;
                }

                else
                {
                    //update the user password if there no errors by changing the password
                    User _changePassword = db.Users.Single(p => p.UserID == _userID);

                    string _getPassword = _changePassword.Password;
                    //check if the old password entered matches with the one entered
                    if (_hrClass.ValidateHashAndEncrytPwd(_oldPassword, _getPassword) == true && _hrClass.ValidatePassword(_oldPassword, _getPassword) == true)
                    {
                        //change password
                        _changePassword.Password = _hrClass.GenerateHashSaltedEnctryptedPwd(_newPassword);
                        db.SubmitChanges();
                        txtOldPassword.Attributes.Add("value", "");
                        txtNewPassword.Attributes.Add("value", "");
                        txtConfirmPassword.Attributes.Add("value", "");
                        //lbChangePasswordError.Text = "Your password has been successfully changed.";
                        lbChangePasswordError.Text = "";
                        ModalPopupExtenderChangePassword.Hide();
                        _hrClass.LoadHRManagerMessageBox(2, "Your password has been successfully changed.!", this.Page, formAdeptHRManager);
                        return;
                    }
                    else
                    {
                        lbChangePasswordError.Text = "The old password entered is incorrect !!";
                        txtOldPassword.Focus();
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                lbChangePasswordError.Text = "Password change failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //save password changes
        protected void lnkBtnSavePasswordChanges_Click(object sender, EventArgs e)
        {
            ChangeLoggedUserPassword(); return;
        }
    }
}
