﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Job_Description.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Job_Description" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>--%>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelJobDescription" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelJobs" runat="server">
                <asp:Panel ID="PanelJobList" Visible="true" runat="server">
                    <div class="content-menu-bar">
                        <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new job"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                            Add New</asp:LinkButton>
                        <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit job details"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                            Edit</asp:LinkButton>
                        <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View job details"
                            CausesValidation="false" runat="server">
                            <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                            View</asp:LinkButton>
                        <asp:Label ID="LabelAdvert" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonPushToAdvert" Visible="false" OnClick="LinkButtonPushToAdvert_Click"
                            ToolTip="Push to advertise" CausesValidation="false"
                            runat="server">
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Play.png" />
                            Push To Advert</asp:LinkButton>
                        <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                        <asp:LinkButton ID="LinkButtonDelete" CausesValidation="false" ToolTip="Delete job"
                            runat="server">
                            <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                            Delete</asp:LinkButton>
                    </div>
                    <div class="search-div">
                        Search:
                        <asp:TextBox ID="txtSearch" Width="550px" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"
                            runat="server"></asp:TextBox>
                        <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                            Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by job reference/type/category/title:"
                            WatermarkCssClass="water-mark-text-extender">
                        </asp:TextBoxWatermarkExtender>
                        <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                            Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetJobDescriptions"
                            TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                            CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                            CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                        </asp:AutoCompleteExtender>
                        <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                            width: 543px !important;">
                        </div>
                    </div>
                    <div class="panel-details">
                        <asp:GridView ID="gvJobs" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                            CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No job available!">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1+"." %>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("JobID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="4px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="JobReference" HeaderText="Job Ref." />
                                <asp:BoundField DataField="JobType" HeaderText="Job Type" />
                                <asp:BoundField DataField="JobCategory" HeaderText="Job Category" />
                                <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                                <asp:BoundField DataField="JobStatus" HeaderText="Job Status" />
                                <asp:BoundField DataField="AdvertFromDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Advert From Date" />
                                <asp:BoundField DataField="AdvertToDate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Advert To Date" />
                            </Columns>
                            <FooterStyle CssClass="PagerStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelAddJobDescription" Visible="false" runat="server">
                    <div class="panel-details-header">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 57%;">
                                    <asp:Image ID="ImageAddJobDescriptionHeader" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                    <asp:Label ID="lbAddJobDescriptionHeader" runat="server" Text="Add New Job Description"></asp:Label>
                                </td>
                                <td>
                                    <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                    <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk (*) are required"></asp:Label>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonCloseAddJobDescription" OnClick="ImageButtonCloseAddJobDescription_Click"
                                        ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-details">
                        <table>
                            <tr>
                                <td>
                                    Job Reference:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtJobReference" ReadOnly="true" Text="Auto Generated" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Job Type:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobType" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Category:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCategory" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Job Title:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtJobTitle" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Country:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                        runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Job Location:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLocation" runat="server">
                                        <asp:ListItem Value="0">-Select Location-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Job Summary</b><span class="errorMessage">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="height: 170px; width: 1052px;" class="div-html-ajax-editor">
                                        <asp:TextBox ID="txtJobSummary" TextMode="MultiLine" Width="1050" Height="160" runat="server"></asp:TextBox>
                                      
                                        <asp:HtmlEditorExtender ID="txtJobSummary_HtmlEditorExtender" EnableSanitization="false" TargetControlID="txtJobSummary"
                                            runat="server">
                                            <Toolbar>
                                                <asp:Bold />
                                                <asp:Italic />
                                                <asp:Underline />
                                                <asp:Undo />
                                                <asp:Redo />
                                                <asp:InsertOrderedList />
                                                <asp:InsertUnorderedList />
                                                <asp:ForeColorSelector />
                                                <asp:FontSizeSelector />
                                            </Toolbar>
                                        </asp:HtmlEditorExtender>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Key Responsibilities</b><span class="errorMessage">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="height: 230px; width: 1052px;" class="div-html-ajax-editor">
                                        <asp:TextBox ID="txtKeyResponsibilities" TextMode="MultiLine" Width="1050" Height="220"
                                            runat="server"></asp:TextBox>
                                        <asp:HtmlEditorExtender ID="txtKeyResponsibilities_HtmlEditorExtender" EnableSanitization="false" TargetControlID="txtKeyResponsibilities"
                                            runat="server">
                                            <Toolbar>
                                                <asp:Bold />
                                                <asp:Italic />
                                                <asp:Underline />
                                                <asp:Undo />
                                                <asp:Redo />
                                                <asp:InsertOrderedList />
                                                <asp:InsertUnorderedList />
                                                <asp:ForeColorSelector />
                                                <asp:FontSizeSelector />
                                            </Toolbar>
                                        </asp:HtmlEditorExtender>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Job Requirements / Qualifications</b><span class="errorMessage">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="height: 230px; width: 1052px;" class="div-html-ajax-editor">
                                        <asp:TextBox ID="txtJobRequirements" TextMode="MultiLine" Width="1050" Height="220"
                                            runat="server"></asp:TextBox>
                                        <asp:HtmlEditorExtender ID="txtJobRequirements_HtmlEditorExtender" EnableSanitization="false" TargetControlID="txtJobRequirements"
                                            runat="server">
                                            <Toolbar>
                                                <asp:Bold />
                                                <asp:Italic />
                                                <asp:Underline />
                                                <asp:Undo />
                                                <asp:Redo />
                                                <asp:InsertOrderedList />
                                                <asp:InsertUnorderedList />
                                                <asp:ForeColorSelector />
                                                <asp:FontSizeSelector />
                                            </Toolbar>
                                        </asp:HtmlEditorExtender>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Budget Currency:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlBudgetCurrency" Width="200px" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Budget Amount:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBudgetAmount" Width="200px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Advert Running From Date:<span class="errorMessage">*</span>
                                </td>
                                <td style="vertical-align: bottom;">
                                    <asp:TextBox ID="txtAdvertRunningFromDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonAdvertRunningFromDate" ToolTip="Pick advert runs from which date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtAdvertRunningFromDate_CalendarExtender" runat="server"
                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtAdvertRunningFromDate"
                                        PopupButtonID="ImageButtonAdvertRunningFromDate" PopupPosition="TopRight">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtAdvertRunningFromDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtAdvertRunningFromDate"
                                        UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtAdvertRunningFromDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtAdvertRunningFromDate_MaskedEditExtender" ControlToValidate="txtAdvertRunningFromDate"
                                        CssClass="errorMessage" ErrorMessage="txtAdvertRunningFromDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                <td>
                                    Advert Running To Date:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAdvertRunningToDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonAdvertRunningToDate" ToolTip="Pick advert runs from date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtAdvertRunningToDate_CalendarExtender" runat="server"
                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtAdvertRunningToDate"
                                        PopupButtonID="ImageButtonAdvertRunningToDate" PopupPosition="TopRight">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtAdvertRunningToDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtAdvertRunningToDate"
                                        UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtAdvertRunningToDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtAdvertRunningToDate_MaskedEditExtender" ControlToValidate="txtAdvertRunningToDate"
                                        CssClass="errorMessage" ErrorMessage="txtAdvertRunningToDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid to date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Status:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobStatus" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Assigned to Staff:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlStaffAssignedTo" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldJobID" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveJobDescription" OnClick="lnkBtnSaveJobDescription_Click"
                                            ToolTip="Save job description" runat="server">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnUpdateJobDescription" OnClick="lnkBtnUpdateJobDescription_Click"
                                            CausesValidation="false" ToolTip="Update job description" Enabled="false" runat="server">
                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                ImageAlign="AbsMiddle" />
                                            Update</asp:LinkButton>
                                        <asp:LinkButton ID="lnkBtnClearJobDescription" OnClick="lnkBtnClearJobDescription_Click"
                                            CausesValidation="false" ToolTip="Clear" runat="server">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                ImageAlign="AbsMiddle" />
                                            Clear</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <uc:ConfirmMessageBox ID="ucPushJobForAdvertisement" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
