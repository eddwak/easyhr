﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace AdeptHRManager.Recruitment_Module
{
    public partial class Recruitment_Report : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        HRManagerReports _hrReports = new HRManagerReports();
        static short loggedStaffPostedAtID = 0;
        ReportDataSource _reportSource = new ReportDataSource();
        DataTable dt = new DataTable();
        ReportDocument recruitmentReport = new ReportDocument();
        string reportPath = "";

        //page init method for viewing recruitment reports
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["ReportType"] != null)
                {
                    RecruitmentReportViewing();
                }
            }
            catch { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetJobs();//get job applcations that have shortlisted and interviewed
                GetApplicantNames();// populate ddlSingleApplicantCVApplicantName with applicant namaes
            }

        }
        //display jobs with applications that have been shortlisted and interviewed
        public void GetJobs()
        {
            try
            {
                ddlInterviewReportPerJobJobReferenceAndTitle.Items.Clear();
                object _getJobs;
                _getJobs = db.PROC_DistinctJobsJobApplicationsShortlist();
                ddlInterviewReportPerJobJobReferenceAndTitle.DataSource = _getJobs;
                ddlInterviewReportPerJobJobReferenceAndTitle.Items.Insert(0, new ListItem("-Select Job-"));
                ddlInterviewReportPerJobJobReferenceAndTitle.DataTextField = "JobReferenceAndTitle";
                ddlInterviewReportPerJobJobReferenceAndTitle.DataValueField = "JobID";
                ddlInterviewReportPerJobJobReferenceAndTitle.AppendDataBoundItems = true;
                ddlInterviewReportPerJobJobReferenceAndTitle.DataBind();
                return;
            }
            catch { }
        }
        //method for viewing an interview report per job
        private void ViewInterviewReportPerJob()
        {
            ModalPopupExtenderInterviewReportPerJob.Show();
            try
            {
                if (ddlInterviewReportPerJobJobReferenceAndTitle.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No job selected. Select the job vancany for which you want to generate an interview report", this, PanelRecruitmentReports);
                    return;
                }
                else
                {
                    Session["ReportJobID"] = ddlInterviewReportPerJobJobReferenceAndTitle.SelectedValue;
                    Session["ReportType"] = "InterviewReportPerJob";
                    Session["ReportTitle"] = "Interview Report Per Job";

                    ModalPopupExtenderInterviewReportPerJob.Hide();
                    RecruitmentReportViewing();//view report
                    TabContainerRecruitmentReports.ActiveTabIndex = 1;//report viewer
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelRecruitmentReports);
                return;
            }
        }
        //view interview report per job
        protected void lnkBtnViewInterviewReportPerJob_Click(object sender, EventArgs e)
        {
            ViewInterviewReportPerJob();
            return;
        }
        //APPLICANT CV REPORT
        //populate an applicant name dropdown
        public void GetApplicantNames()
        {
            try
            {
                ddlSingleApplicantCVApplicantName.Items.Clear();
                object _getReferenceDetail;
                _getReferenceDetail = db.PROC_ApplicantNameAndReference();
                ddlSingleApplicantCVApplicantName.DataSource = _getReferenceDetail;
                ddlSingleApplicantCVApplicantName.Items.Insert(0, new ListItem("-Select Applicant-"));
                ddlSingleApplicantCVApplicantName.DataTextField = "ApplicantReferenceAndName";
                ddlSingleApplicantCVApplicantName.DataValueField = "ApplicantID";
                ddlSingleApplicantCVApplicantName.AppendDataBoundItems = true;
                ddlSingleApplicantCVApplicantName.DataBind();
                return;
            }
            catch { }
        }
        //method for viewing a single applicant's CV report
        private void ViewSingleApplicantCVReport()
        {

            ModalPopupExtenderSingleApplicantCVReport.Show();
            try
            {
                if (ddlSingleApplicantCVApplicantName.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No applicant selected. Please select an applicant!", this, PanelRecruitmentReports);
                    return;
                }
                else
                {
                    string _applicantID = ddlSingleApplicantCVApplicantName.SelectedValue.ToString();
                    Session["ReportApplicantID"] = _applicantID.ToString();
                    Session["ReportType"] = "SingleApplicantCV";
                    Session["ReportTitle"] = "Applicant C.V.";
                    ModalPopupExtenderSingleApplicantCVReport.Hide();
                    RecruitmentReportViewing();//view the report
                    TabContainerRecruitmentReports.ActiveTabIndex = 1;//report viewer
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "View report failed. " + ex.Message.ToString() + ". Please try again!", this, PanelRecruitmentReports);
                return;
            }
        }
        //view single applicant cv report
        protected void lnkBtnViewSingleApplicantCVReport_Click(object sender, EventArgs e)
        {
            ViewSingleApplicantCVReport(); return;
        }

        //method for viewing recruitment reports
        private void RecruitmentReportViewing()
        {

            if (Session["ReportType"].ToString() == "InterviewReportPerJob")//INTERVIEW REPORT PER JOB REPORT
            {
                int _jobID = Convert.ToInt32(Session["ReportJobID"]);
                reportPath = Server.MapPath("~/Recruitment_Module/Recruitment_Module_Reports/report_InterviewPerJob.rpt");
                recruitmentReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_InterviewReportPerJob(_jobID);
                recruitmentReport.SetDataSource(_reportData);
                recruitmentReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerRecruitmentReports.ReportSource = recruitmentReport;
            }
            else if (Session["ReportType"].ToString() == "SingleApplicantCV")//APPLICANT CV REPORT
            {
                Guid _applicantID = _hrClass.ReturnGuid(Session["ReportApplicantID"].ToString());
                reportPath = Server.MapPath("~/Recruitment_Module/Recruitment_Module_Reports/report_ApplicantCV.rpt");
                recruitmentReport.Load(reportPath);
                DataSet _reportData = _hrReports.rpt_ApplicantCV(_applicantID);//get basic information and workexperince dataset
                DataSet _educationDS= _hrReports.rpt_ApplicantEducation(_applicantID);//get education data
                //merge the two datasets
                _reportData.Merge(_educationDS);
                recruitmentReport.SetDataSource(_reportData);
                recruitmentReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
                CrystalReportViewerRecruitmentReports.ReportSource = recruitmentReport;

               // Guid _applicantID = _hrClass.ReturnGuid(Session["ReportApplicantID"].ToString());
               // reportPath = Server.MapPath("~/Recruitment_Module/Recruitment_Module_Reports/report_ApplicantEducation.rpt");
               // recruitmentReport.Load(reportPath);
               // DataSet _reportData = _hrReports.rpt_ApplicantEducation(_applicantID);
               // recruitmentReport.SetDataSource(_reportData);
               //// recruitmentReport.SetParameterValue("ReportTitle", Session["ReportTitle"].ToString().ToUpper());
               // CrystalReportViewerRecruitmentReports.ReportSource = recruitmentReport;
            }
        }
    }
}