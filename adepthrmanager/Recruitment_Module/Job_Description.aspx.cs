﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;


namespace AdeptHRManager.Recruitment_Module
{
    public partial class Job_Description : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropDowns();//populate dropdowns
                DisplayJobs();//load jobs 
            }
            //add push job for advertisement click event
            ucPushJobForAdvertisement.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnPushJobForAdvertisement_Click);
        }
        //populate dropdowns
        private void PopulateDropDowns()
        {
            _hrClass.GetListingItems(ddlJobType, 10000, "Job Type");//display job type(10000) available
            _hrClass.GetListingItems(ddlJobCategory, 11000, "Job Category");//display job category(11000) available
            _hrClass.GetListingItems(ddlCountry, 1000, "Country");//display country(1000) available
            _hrClass.GetListingItems(ddlBudgetCurrency, 14000, "Currency");//display curiencies(14000) available
            _hrClass.GetListingItems(ddlJobStatus, 13000, "Status");//display job status(13000) available
            _hrClass.GetStaffNames(ddlStaffAssignedTo, "Assigned To");//populate job assigned to dropdown
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddJobDescriptionControls();
            PanelJobList.Visible = false;
            PanelAddJobDescription.Visible = true;
            return;
        }
        //clear add jos description controls
        private void ClearAddJobDescriptionControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddJobDescription);
            HiddenFieldJobID.Value = "";
            lnkBtnSaveJobDescription.Visible = true;
            lnkBtnUpdateJobDescription.Enabled = false;
            lnkBtnUpdateJobDescription.Visible = true;
            lnkBtnClearJobDescription.Visible = true; ;
            lbAddJobDescriptionHeader.Text = "Add New Job Description";
            ImageAddJobDescriptionHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            txtJobReference.Text = "Auto Generated";
            return;
        }
        //close add job description panel
        protected void ImageButtonCloseAddJobDescription_Click(object sender, EventArgs e)
        {
            PanelAddJobDescription.Visible = false;
            PanelJobList.Visible = true;
            return;
        }
        //display job locations that fall under a country
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _countryID = 0;
                if (ddlCountry.SelectedIndex != 0)
                    _countryID = Convert.ToInt32(ddlCountry.SelectedValue);
                _hrClass.GetLocations(ddlLocation, "Location", _countryID);//populate locations dropdown with locations that are under the selected country
                return;
            }
            catch (Exception)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelJobs);
                return;
            }
        }
        //validate add job description controls
        private string ValidateAddJobDescriptionControls()
        {
            if (ddlJobType.SelectedIndex == 0)
            {
                ddlJobType.Focus();
                return "Select job type!";
            }
            else if (ddlJobCategory.SelectedIndex == 0)
            {
                ddlJobCategory.Focus();
                return "Select job category!";
            }
            else if (txtJobTitle.Text.Trim() == "")
            {
                txtJobTitle.Focus();
                return "Enter job title!";
            }
            else if (ddlCountry.SelectedIndex == 0)
            {
                ddlCountry.Focus();
                return "Select country!";
            }
            else if (ddlLocation.SelectedIndex == 0)
            {
                ddlLocation.Focus();
                return "Select job location!";
            }
            else if (txtJobSummary.Text.Trim() == "")
            {
                txtJobSummary.Focus();
                return "Enter job summary!";
            }
            else if (txtKeyResponsibilities.Text.Trim() == "")
            {
                txtKeyResponsibilities.Focus();
                return "Enter key responsibilities!";
            }
            else if (txtJobRequirements.Text.Trim() == "")
            {
                txtJobRequirements.Focus();
                return "Enter job requirements/qualifications!";
            }
            else if (ddlBudgetCurrency.SelectedIndex == 0)
            {
                ddlBudgetCurrency.Focus();
                return "Select budget currency type!";
            }
            else if (txtBudgetAmount.Text.Trim() == "")
            {
                txtBudgetAmount.Focus();
                return "Enter budget amount!";
            }
            else if (_hrClass.isNumberValid(txtBudgetAmount.Text.Trim()) == false)
            {
                txtBudgetAmount.Focus();
                return "Invalid budget amount value entered!";
            }
            else if (txtAdvertRunningFromDate.Text.Trim() == "")
            {
                txtAdvertRunningFromDate.Focus();
                return "Enter date from which the advert will start running!";
            }
            else if (_hrClass.isDateValid(txtAdvertRunningFromDate.Text.Trim()) == false)
            {
                txtAdvertRunningFromDate.Focus();
                return "Invalid advert running from date entered!";
            }
            else if (txtAdvertRunningToDate.Text.Trim() == "")
            {
                txtAdvertRunningToDate.Focus();
                return "Enter date which the advert will stop running!";
            }
            else if (_hrClass.isDateValid(txtAdvertRunningToDate.Text.Trim()) == false)
            {
                txtAdvertRunningToDate.Focus();
                return "Invalid advert running to date entered!";
            }
            else if (Convert.ToDateTime(txtAdvertRunningFromDate.Text.Trim()) > Convert.ToDateTime(txtAdvertRunningToDate.Text.Trim()))
            {
                ImageButtonAdvertRunningFromDate.Focus();
                return "'Advert running from date' cannot be after 'advert running to date'!";
            }
            else if (ddlJobStatus.SelectedIndex == 0)
            {
                ddlJobStatus.Focus();
                return "Selct job status!";
            }
            else if (ddlStaffAssignedTo.SelectedIndex == 0)
            {
                ddlStaffAssignedTo.Focus();
                return "Select the staff assigned the job!";
            }
            else return "";
        }
        //save job descripiton
        private void SaveJobDescription()
        {
            try
            {
                string _error = ValidateAddJobDescriptionControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelJobs);
                    return;
                }
                else
                {
                    //generate job reference number
                    string _jobReferenceNumber = GenerateJobReferenceNumber();//
                    //save job details
                    Job newJob = new Job();
                    newJob.JobReference = _jobReferenceNumber;
                    newJob.JobTypeID = Convert.ToInt32(ddlJobType.SelectedValue);
                    newJob.JobCategoryID = Convert.ToInt32(ddlJobCategory.SelectedValue);
                    newJob.JobTitle = txtJobTitle.Text.Trim();
                    newJob.JobSummary = txtJobSummary.Text.Trim();
                    newJob.KeyResponsibilities = txtKeyResponsibilities.Text.Trim();
                    newJob.JobRequirements = txtJobRequirements.Text.Trim();
                    newJob.BudgetCurrencyID = Convert.ToInt32(ddlBudgetCurrency.SelectedValue);
                    newJob.BudgetAmount = Convert.ToDecimal(txtBudgetAmount.Text.Trim());
                    newJob.JobStatusID = Convert.ToInt32(ddlJobStatus.SelectedValue);
                    newJob.AssignedToStaffID = _hrClass.ReturnGuid(ddlStaffAssignedTo.SelectedValue);
                    newJob.CreatedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    newJob.DateCreated = DateTime.Now;
                    newJob.AdvertFromDate = Convert.ToDateTime(txtAdvertRunningFromDate.Text.Trim()).Date;
                    newJob.AdvertToDate = Convert.ToDateTime(txtAdvertRunningToDate.Text.Trim()).Date;
                    newJob.IsJobClosed = false;
                    newJob.IsAdvertised = false;

                    //newJob.DateClosed
                    //newJob.PlacedApplicantID
                    db.Jobs.InsertOnSubmit(newJob);
                    db.SubmitChanges();
                    HiddenFieldJobID.Value = newJob.JobID.ToString();

                    txtJobReference.Text = _jobReferenceNumber;
                    _hrClass.LoadHRManagerMessageBox(2, "Job description details have been successfully saved!", this, PanelJobs);
                    lnkBtnUpdateJobDescription.Enabled = true;
                    //refresh job
                    DisplayJobs();
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelJobs);
                return;
            }
        }
        //update job descripiton
        private void UpdateJobDescription()
        {
            try
            {
                string _error = ValidateAddJobDescriptionControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelJobs);
                    return;
                }
                else
                {
                    //update job details
                    int _jobID = Convert.ToInt32(HiddenFieldJobID.Value);
                    Job updateJob = db.Jobs.Single(p => p.JobID == _jobID);
                    updateJob.JobTypeID = Convert.ToInt32(ddlJobType.SelectedValue);
                    updateJob.JobCategoryID = Convert.ToInt32(ddlJobCategory.SelectedValue);
                    updateJob.JobTitle = txtJobTitle.Text.Trim();
                    updateJob.JobSummary = txtJobSummary.Text.Trim();
                    updateJob.KeyResponsibilities = txtKeyResponsibilities.Text.Trim();
                    updateJob.JobRequirements = txtJobRequirements.Text.Trim();
                    updateJob.BudgetCurrencyID = Convert.ToInt32(ddlBudgetCurrency.SelectedValue);
                    updateJob.BudgetAmount = Convert.ToDecimal(txtBudgetAmount.Text.Trim());
                    updateJob.JobStatusID = Convert.ToInt32(ddlJobStatus.SelectedValue);
                    updateJob.AssignedToStaffID = _hrClass.ReturnGuid(ddlStaffAssignedTo.SelectedValue);
                    //updateJob.CreatedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    updateJob.IsJobClosed = false;
                    //newJob.DateClosed
                    //newJob.PlacedApplicantID
                    updateJob.AdvertFromDate = Convert.ToDateTime(txtAdvertRunningFromDate.Text.Trim()).Date;
                    updateJob.AdvertToDate = Convert.ToDateTime(txtAdvertRunningToDate.Text.Trim()).Date;
                    db.SubmitChanges();
                    DisplayJobs();//refresg=h jobs gridview
                    _hrClass.LoadHRManagerMessageBox(2, "Job description details have been successfully updated!", this, PanelJobs);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. Please try again!", this, PanelJobs);
                return;
            }
        }
        //generate job reference number
        private string GenerateJobReferenceNumber()
        {
            string jobRefNumber = "", firstPart = "J", secondPart = "", thirdPart = "001";
            secondPart = DateTime.Now.Year.ToString();
            if (db.Jobs.Any() == false)//no job saved
            {
                jobRefNumber = firstPart + secondPart + thirdPart;
            }
            else//there is a job saved
            {
                //check if there is a job for the current year
                if (db.Jobs.Any(p => p.JobReference.Contains(secondPart)) == false)//no job for the current year
                    jobRefNumber = firstPart + secondPart + thirdPart;
                else//a job exists
                {
                    int _lastJobID = 0;//get the last job id
                    _lastJobID = db.Jobs.Where(p => p.JobReference.Contains(secondPart)).Max(p => p.JobID);
                    string _lastJobRefNumber = "";//get last reference number
                    _lastJobRefNumber = db.Jobs.Single(p => p.JobID == _lastJobID).JobReference;
                    //get the last reference number third part
                    _lastJobRefNumber = _lastJobRefNumber.Replace(firstPart, "").Replace(secondPart, "");
                    int _3rdPart = Convert.ToInt32(_lastJobRefNumber) + 1;//increment by 1
                    thirdPart = _3rdPart.ToString();
                    jobRefNumber = firstPart + secondPart;
                    //check the length of the third part
                    if (thirdPart.Length == 1)
                        jobRefNumber += "00" + thirdPart;
                    else if (thirdPart.Length == 2)
                        jobRefNumber += "0" + thirdPart;
                    else
                        jobRefNumber += thirdPart;
                }
            }
            return jobRefNumber;
        }
        //save details
        protected void lnkBtnSaveJobDescription_Click(object sender, EventArgs e)
        {
            SaveJobDescription(); return;
        }
        //update details
        protected void lnkBtnUpdateJobDescription_Click(object sender, EventArgs e)
        {
            UpdateJobDescription(); return;
        }
        //clear controls
        protected void lnkBtnClearJobDescription_Click(object sender, EventArgs e)
        {
            ClearAddJobDescriptionControls(); return;
        }

        //display jobs
        private void DisplayJobs()
        {
            try
            {
                object _display;
                _display = db.Jobs_Views.Select(p => p).OrderByDescending(p => p.JobReference);
                gvJobs.DataSourceID = null;
                gvJobs.DataSource = _display;
                gvJobs.DataBind();
                return;
            }
            catch { }
        }

        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobs))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this, PanelJobs);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelJobs);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit job
                            LoadEditJobControls(Convert.ToInt32(_HFJobID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelJobs);
            }
        }
        // populate the edit job controls
        private void LoadEditJobControls(int _jobID)
        {
            ClearAddJobDescriptionControls();
            Jobs_View getJob = db.Jobs_Views.Single(p => p.JobID == _jobID);
            HiddenFieldJobID.Value = _jobID.ToString();
            txtJobReference.Text = getJob.JobReference;
            ddlJobType.SelectedValue = getJob.JobTypeID.ToString();
            ddlJobCategory.SelectedValue = getJob.JobCategoryID.ToString();
            txtJobTitle.Text = getJob.JobTitle;
            ddlCountry.SelectedValue = getJob.CountryID.ToString();
            _hrClass.GetLocations(ddlLocation, "Location", (int)getJob.CountryID);//populate locations dropdown with locations that are under the selected
            ddlLocation.SelectedValue = getJob.JobLocationID.ToString();
            txtJobSummary.Text = getJob.JobSummary;
            txtKeyResponsibilities.Text = getJob.KeyResponsibilities;
            txtJobRequirements.Text = getJob.JobRequirements;
            ddlBudgetCurrency.SelectedValue = getJob.BudgetCurrencyID.ToString();
            txtBudgetAmount.Text = getJob.BudgetAmount.ToString();
            txtAdvertRunningFromDate.Text = _hrClass.ShortDateDayStart(getJob.AdvertFromDate.ToString());
            txtAdvertRunningToDate.Text = _hrClass.ShortDateDayStart(getJob.AdvertToDate.ToString());
            ddlJobStatus.SelectedValue = getJob.JobStatusID.ToString();
            ddlStaffAssignedTo.SelectedValue = getJob.AssignedToStaffID.ToString();

            lnkBtnSaveJobDescription.Visible = false;
            lnkBtnClearJobDescription.Visible = true;
            lnkBtnUpdateJobDescription.Enabled = true;
            lbAddJobDescriptionHeader.Text = "Edit Job Description";
            ImageAddJobDescriptionHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ImageButtonAdvertRunningFromDate.Visible = true;
            ImageButtonAdvertRunningToDate.Visible = true;
            txtJobSummary_HtmlEditorExtender.Enabled = true;
            txtKeyResponsibilities_HtmlEditorExtender.Enabled = true;
            txtJobRequirements_HtmlEditorExtender.Enabled = true;
            PanelAddJobDescription.Visible = true;
            PanelJobList.Visible = false;
            return;
        }
        //check if a job record is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobs))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to view the details!", this, PanelJobs);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record!. Select only one record to view the details at a time !", this, PanelJobs);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view job description
                            LoadViewJobControls(Convert.ToInt32(_HFJobID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the record for view purpose ! " + ex.Message.ToString(), this, PanelJobs);
                return;
            }
        }
        // populate the view jobs
        private void LoadViewJobControls(int _jobID)
        {
            ClearAddJobDescriptionControls();
            Jobs_View getJob = db.Jobs_Views.Single(p => p.JobID == _jobID);
            HiddenFieldJobID.Value = _jobID.ToString();
            txtJobReference.Text = getJob.JobReference;
            ddlJobType.SelectedValue = getJob.JobTypeID.ToString();
            ddlJobCategory.SelectedValue = getJob.JobCategoryID.ToString();
            txtJobTitle.Text = getJob.JobTitle;
            ddlCountry.SelectedValue = getJob.CountryID.ToString();
            _hrClass.GetLocations(ddlLocation, "Location", (int)getJob.CountryID);//populate locations dropdown with locations that are under the selected
            ddlLocation.SelectedValue = getJob.JobLocationID.ToString();
            txtJobSummary.Text = getJob.JobSummary;
            txtKeyResponsibilities.Text = getJob.KeyResponsibilities;
            txtJobRequirements.Text = getJob.JobRequirements;
            ddlBudgetCurrency.SelectedValue = getJob.BudgetCurrencyID.ToString();
            txtBudgetAmount.Text = getJob.BudgetAmount.ToString();
            txtAdvertRunningFromDate.Text = _hrClass.ShortDateDayStart(getJob.AdvertFromDate.ToString());
            txtAdvertRunningToDate.Text = _hrClass.ShortDateDayStart(getJob.AdvertToDate.ToString());
            ddlJobStatus.SelectedValue = getJob.JobStatusID.ToString();
            ddlStaffAssignedTo.SelectedValue = getJob.AssignedToStaffID.ToString();

            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddJobDescription);
            lnkBtnSaveJobDescription.Visible = false;
            lnkBtnClearJobDescription.Visible = false;
            lnkBtnUpdateJobDescription.Visible = false;
            lbAddJobDescriptionHeader.Text = "View Job Description";
            ImageAddJobDescriptionHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ImageButtonAdvertRunningFromDate.Visible = false;
            ImageButtonAdvertRunningToDate.Visible = false;
            txtJobSummary_HtmlEditorExtender.Enabled = false;
            txtKeyResponsibilities_HtmlEditorExtender.Enabled = false;
            txtJobRequirements_HtmlEditorExtender.Enabled = false;
            PanelAddJobDescription.Visible = true;
            PanelJobList.Visible = false;
            return;
        }
        //method for searching for a job description
        private void SearchForJobDescription()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.Jobs_Views.Where(p => (p.JobReference.ToLower() + p.JobType.ToLower() + p.JobCategory.ToLower() + p.JobTitle.ToLower()).Contains(_searchText)).OrderByDescending(p => p.JobReference);
                gvJobs.DataSourceID = null;
                gvJobs.DataSource = _searchDetails;
                Session["gvJobsData"] = _searchDetails;
                gvJobs.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelJobs);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForJobDescription();
            return;
        }
        //check if there is a job record that is selected before pushing it for advertisement to Adept Systems Job Site
        protected void LinkButtonPushToAdvert_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobs))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the job that you want to push for advertisement at the Adept Systems jobsite!", this, PanelJobs);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one job description!. Select only one job description to push for advertisement at a time !", this, PanelJobs);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobs.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenField1");
                            //string _confirmationMessage = "Are you sure you want to to push the selected job with the following details for advertisement at the Adept Systems Jobsite?";
                            string _confirmationMessage = PushJobForAdvertMessage(Convert.ToInt32(_HFJobID.Value));
                            if (_confirmationMessage != "")//check if there is a message i.e the job is not already advertised
                                _hrClass.LoadHRManagerConfirmationMessageBox(ucPushJobForAdvertisement, _confirmationMessage);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the job description for advertisememnt purpose ! " + ex.Message.ToString(), this, PanelJobs);
                return;
            }
        }
        //generate a string infrnation message for advertising the job 
        private string PushJobForAdvertMessage(int _jobID)
        {
            string _message = "";
            Jobs_View getJob = db.Jobs_Views.Single(p => p.JobID == _jobID);
            //check if the job is already advertised
            if (getJob.IsAdvertised == true)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Pushing the job for advertisement failed.<br>The selected job was already pushed for advertisement on " + _hrClass.ShortDateDayStart(getJob.DateAdvertised.ToString()) + " at " + _hrClass.ReturnLongTime(getJob.DateAdvertised.ToString()) + " by " + _hrClass.getStaffNames((Guid)getJob.AdvertisedByStaffID) + ".", this, PanelJobs);
            }
            else
            {
                HiddenFieldJobID.Value = _jobID.ToString();
                //generate the message
                _message = "Are you sure you want to to push the selected job with the following details for advertisement at the Adept Systems Jobsite?";
                _message += "<hr><table>";
                _message += "<tr><td>Job Reference:</td><td>" + getJob.JobReference + "</td></tr>";
                _message += "<tr><td>Job Type:</td><td>" + getJob.JobType + "</td></tr>";
                _message += "<tr><td>Job Category:</td><td>" + getJob.JobCategory + "</td></tr>";
                _message += "<tr><td>Advertise From:</td><td>" + _hrClass.ShortDateDayStart(getJob.AdvertFromDate.ToString()) + "</td></tr>";
                _message += "<tr><td>Advertise To:</td><td>" + _hrClass.ShortDateDayStart(getJob.AdvertToDate.ToString()) + "</td></tr>";
                _message += "</table><hr>";
            }
            return _message;
        }
        //push the job for the advertisement
        private void PushJobForAdvertisement()
        {
            try
            {
                int _jobID = Convert.ToInt32(HiddenFieldJobID.Value);
                Job advertiseJob = db.Jobs.Single(p => p.JobID == _jobID);
                advertiseJob.IsAdvertised = true;
                advertiseJob.DateAdvertised = DateTime.Now;
                advertiseJob.AdvertisedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                db.SubmitChanges();
                //write code for pushing the job to Adept Systems Jobsite
                _hrClass.LoadHRManagerMessageBox(2, "The job has been pushed to Adept Systems jobsite and will be subject to the job site admin approval!", this, PanelJobs);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Pushing the job for advertisement failed. " + ex.Message.ToString() + ". Please try again!", this, PanelJobs);
            }
        }
        //advretise
        protected void lnkBtnPushJobForAdvertisement_Click(object sender, EventArgs e)
        {
            PushJobForAdvertisement();
            return;
        }

      
    }
}