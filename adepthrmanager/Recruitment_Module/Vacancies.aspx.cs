﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AdeptHRManager.Recruitment_Module
{
    public partial class Vacancies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            DisplayVacancies();
        }
        //load vacancies
        private void DisplayVacancies()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("VacancyID", typeof(int)));
            dt.Columns.Add(new DataColumn("VacancyRef", typeof(string)));
            dt.Columns.Add(new DataColumn("JobRef", typeof(string)));
            dt.Columns.Add(new DataColumn("StartDate", typeof(string)));
            dt.Columns.Add(new DataColumn("JobTitle", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            DataRow dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "V0001";
            dr[2] = "J0001";
            dr[3] = "17/Sep/2014";
            dr[4] = "HR Manager";
            dr[5] = "";
            dt.Rows.Add(dr);

            DataRow dr2 = dt.NewRow();
            dr2[0] = 2;
            dr2[1] = "V0002";
            dr2[2] = "J0002";
            dr2[3] = "19/Jun/2014";
            dr2[4] = "Accountant";
            dr2[5] = "";
            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();
            dr3[0] = 3;
            dr3[1] = "V0003";
            dr3[2] = "J0003";
            dr3[3] = "20/Mar/2014";
            dr3[4] = "Software Developer";
            dr3[5] = "";
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4[0] = 4;
            dr4[1] = "V0004";
            dr4[2] = "J0004";
            dr4[3] = "05/Jan/2014";
            dr4[4] = "Sales Manager";
            dr4[5] = "Filled";
            dt.Rows.Add(dr4);
            gvVacancies.DataSource = dt;
            gvVacancies.DataBind();
        }

    }

}