﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;
using AjaxControlToolkit;

namespace AdeptHRManager.Recruitment_Module
{
    public partial class Interview_Appointments : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetJobs();//get jobs with shortlisting
                DisplayInterviewAppointments();//load interview appointments
                //DisplayInterviewAppointments2();
                _hrClass.GetStaffNames(ddlInterviewedBy, "Interviewed By");//populate interviewed by dropdown
            }
            //add delete record event to delete an interview appointment
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddInterviewAppointmentControls();
            return;
        }

        //clear add new appointment controls
        private void ClearAddInterviewAppointmentControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddInterviewAppointment);

            GetShortlistedJobApplicantsPerJob(0);
            HiddenFieldInterviewAppointmentID.Value = "";
            lnkBtnSaveInterviewAppointment.Visible = true;
            lnkBtnUpdateInterviewAppointment.Enabled = false;
            lnkBtnUpdateInterviewAppointment.Visible = true;
            lnkBtnClearInterviewAppointment.Visible = true;
            lbAddInterviewAppointmentHeader.Text = "Add New Interview Appointment";
            ImageAddInterviewAppointmentHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            ModalPopupExtenderAddInterviewAppointment.Show();
            return;
        }
        //display jobs with applicatins that have been shortlisted
        public void GetJobs()
        {
            try
            {
                ddlJobReferenceAndTitle.Items.Clear();
                object _getJobs;
                _getJobs = db.PROC_DistinctJobsJobApplicationsShortlist();
                ddlJobReferenceAndTitle.DataSource = _getJobs;
                ddlJobReferenceAndTitle.Items.Insert(0, new ListItem("-Select Job-"));
                ddlJobReferenceAndTitle.DataTextField = "JobReferenceAndTitle";
                ddlJobReferenceAndTitle.DataValueField = "JobID";
                ddlJobReferenceAndTitle.AppendDataBoundItems = true;
                ddlJobReferenceAndTitle.DataBind();
                return;
            }
            catch { }
        }
        //display applicants shortlisetd against a job
        public void GetShortlistedJobApplicantsPerJob(int _jobID)
        {
            try
            {
                ddlApplicantReferenceAndName.Items.Clear();
                object _getJobs;
                _getJobs = db.PROC_GetShortlistedApplicantsPerJob(_jobID);
                ddlApplicantReferenceAndName.DataSource = _getJobs;
                ddlApplicantReferenceAndName.Items.Insert(0, new ListItem("-Select Job Applicant-"));
                ddlApplicantReferenceAndName.DataTextField = "ApplicantReferenceAndName";
                ddlApplicantReferenceAndName.DataValueField = "ApplicantID";
                ddlApplicantReferenceAndName.AppendDataBoundItems = true;
                ddlApplicantReferenceAndName.DataBind();
                return;
            }
            catch { }
        }
        //load applicants shortlisted againiststhe a selected job
        protected void ddlJobReferenceAndTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderAddInterviewAppointment.Show();
            try
            {
                int _jobID = 0;
                if (ddlJobReferenceAndTitle.SelectedIndex != 0)
                    _jobID = Convert.ToInt32(ddlJobReferenceAndTitle.SelectedValue);
                GetShortlistedJobApplicantsPerJob(_jobID);//shortlisted job applicants based on the slected job
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString() + ". Please try again!", this, PanelAddInterviewAppointment);
                return;
            }
        }

        //validate add interview appointment controls
        private string ValidateAddInterviewApointmentControls()
        {
            if (ddlJobReferenceAndTitle.SelectedIndex == 0)
            {
                ddlJobReferenceAndTitle.Focus();
                return "Select job reference & title!";
            }
            else if (ddlApplicantReferenceAndName.SelectedIndex == 0)
            {
                ddlApplicantReferenceAndName.Focus();
                return "Select a job applicant!";
            }
            else if (txtAppointmentDate.Text.Trim() == "")
            {
                txtAppointmentDate.Focus();
                return "Enter interview appointment date!";
            }
            else if (_hrClass.isDateValid(txtAppointmentDate.Text.Trim()) == false)
            {
                txtAppointmentDate.Focus();
                return "Invalid interview appointment date entered!";
            }
            else if (Convert.ToDateTime(txtAppointmentDate.Text.Trim()).Date < DateTime.Now.Date)
            {
                txtAppointmentDate.Focus();
                return "Interview appointment cannot be a lesser date as compared to the current date!";
            }
            else if (_hrClass.is24HourTimeValid(txtAppointmentTime.Text.Trim()) == false)//
            {
                txtAppointmentTime.Focus();
                return "Invalid appointment time entered. Time should be in 24 hour format!";
            }
            else
            {
                return "";
            }
        }
        //methods for saviong an intervire appointment
        private void SaveInterviewAppointment()
        {
            ModalPopupExtenderAddInterviewAppointment.Show();
            try
            {
                string _error = ValidateAddInterviewApointmentControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    int _jobID = Convert.ToInt32(ddlJobReferenceAndTitle.SelectedValue);
                    Guid _applicantID = _hrClass.ReturnGuid(ddlApplicantReferenceAndName.SelectedValue);
                    int _jobApplicationID = Convert.ToInt32(db.JobApplications.Single(p => p.ApplicantID == _applicantID && p.JobID == _jobID).JobApplicationID);
                    //check if the applicant is already set for an interview
                    if (db.InterviewAppointments.Any(p => p.JobApplicationID == _jobApplicationID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Interview appointment could not be set. The selected applicant is already scheduled for an interview!", this, PanelInterviewAppointments);
                        return;
                    }
                    else
                    {
                        //save the interview appointment
                        InterviewAppointment newInterviewAppoinment = new InterviewAppointment();
                        newInterviewAppoinment.JobApplicationID = _jobApplicationID;
                        newInterviewAppoinment.AppointmentDate = Convert.ToDateTime(txtAppointmentDate.Text.Trim()).Date;
                        newInterviewAppoinment.AppointmentTime = txtAppointmentTime.Text.Trim();
                        newInterviewAppoinment.AppointmentRemarks = txtAppointmentComments.Text.Trim();
                        newInterviewAppoinment.AppointmentDoneByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                        newInterviewAppoinment.DateRecorded = DateTime.Now;
                        newInterviewAppoinment.IsInterviewConducted = false;
                        db.InterviewAppointments.InsertOnSubmit(newInterviewAppoinment);
                        db.SubmitChanges();
                        HiddenFieldInterviewAppointmentID.Value = newInterviewAppoinment.InterviewAppointmentID.ToString();
                        lnkBtnUpdateInterviewAppointment.Enabled = true;
                        DisplayInterviewAppointments();//referesh appointments
                        _hrClass.LoadHRManagerMessageBox(2, "The interview appointment has been successfully saved.", this, PanelInterviewAppointments);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. An error occured while saving the interview appointment." + ex.Message.ToString() + ".", this, PanelInterviewAppointments);
                return;
            }
        }
        //methods for updating an interview appointment
        private void UpdateInterviewAppointment()
        {
            ModalPopupExtenderAddInterviewAppointment.Show();
            try
            {
                string _error = ValidateAddInterviewApointmentControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    int _jobID = Convert.ToInt32(ddlJobReferenceAndTitle.SelectedValue);
                    Guid _applicantID = _hrClass.ReturnGuid(ddlApplicantReferenceAndName.SelectedValue);
                    int _jobApplicationID = Convert.ToInt32(db.JobApplications.Single(p => p.ApplicantID == _applicantID && p.JobID == _jobID).JobApplicationID);
                    int _interviewAppointmentID = Convert.ToInt32(HiddenFieldInterviewAppointmentID.Value);
                    //check if the applicant is already set for an interview
                    if (db.InterviewAppointments.Any(p => p.JobApplicationID == _jobApplicationID && p.InterviewAppointmentID != _interviewAppointmentID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Interview appointment could not be updated. The selected applicant is already scheduled for an interview!", this, PanelInterviewAppointments);
                        return;
                    }
                    else
                    {
                        //update the interview appointment
                        InterviewAppointment updateInterviewAppoinment = db.InterviewAppointments.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
                        updateInterviewAppoinment.JobApplicationID = _jobApplicationID;
                        updateInterviewAppoinment.AppointmentDate = Convert.ToDateTime(txtAppointmentDate.Text.Trim()).Date;
                        updateInterviewAppoinment.AppointmentTime = txtAppointmentTime.Text.Trim();
                        updateInterviewAppoinment.AppointmentRemarks = txtAppointmentComments.Text.Trim();
                        updateInterviewAppoinment.AppointmentDoneByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                        updateInterviewAppoinment.DateRecorded = DateTime.Now;
                        //updateInterviewAppoinment.IsInterviewCompleted = false;
                        db.SubmitChanges();
                        lnkBtnUpdateInterviewAppointment.Enabled = true;
                        DisplayInterviewAppointments();//referesh interview appointments
                        ExpandACollapsiblePanelExtenderAccessed();//exapnd the accessed row
                        _hrClass.LoadHRManagerMessageBox(2, "The interview appointment has been successfully updated.", this, PanelInterviewAppointments);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. An error occured while saving the interview appointment." + ex.Message.ToString() + ".", this, PanelInterviewAppointments);
                return;
            }
        }
        //expand interview appointment applicant collapsible panel extender
        private void ExpandACollapsiblePanelExtenderAccessed()
        {
            //get the collapsible panel that was accessed and expand it
            int _gvRowIndex = Convert.ToInt32(HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex.Value);
            CollapsiblePanelExtender _collapsiblePanel = gvInterviewAppointments.Rows[_gvRowIndex].FindControl("CollapsiblePanelExtenderExpandCollapseDetails") as CollapsiblePanelExtender;
            _collapsiblePanel.Collapsed = false;
            _collapsiblePanel.ClientState = "false";
            return;
        }
        //save interview appointment
        protected void lnkBtnSaveInterviewAppointment_Click(object sender, EventArgs e)
        {
            SaveInterviewAppointment();
            return;
        }
        //update interview appointments
        protected void lnkBtnUpdateInterviewAppointment_Click(object sender, EventArgs e)
        {
            UpdateInterviewAppointment();
            return;
        }
        //clear add interview appointment controls
        protected void lnkBtnClearInterviewAppointment_Click(object sender, EventArgs e)
        {
            ClearAddInterviewAppointmentControls();
            return;
        }

        ////display interview appointments
        //private void DisplayInterviewAppointments()
        //{
        //    try
        //    {
        //        object _display;
        //        _display = db.InterviewAppointment_Views.Select(p => p).OrderByDescending(p => p.InterviewAppointmentID);
        //        gvInterviewAppointments.DataSourceID = null;
        //        gvInterviewAppointments.DataSource = _display;
        //        gvInterviewAppointments.DataBind();
        //        return;
        //    }
        //    catch { }
        //}

        //display interview appointments
        private void DisplayInterviewAppointments()
        {
            try
            {
                object _display;
                _display = db.DistinctJobWithInterviewAppointments_Views.Select(p => p).OrderByDescending(p => p.JobID);
                gvInterviewAppointments.DataSourceID = null;
                gvInterviewAppointments.DataSource = _display;
                gvInterviewAppointments.DataBind();
                return;
            }
            catch { }
        }
        //detailed budget row data bound
        protected void gvInterviewAppointments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    int _jobID = Convert.ToInt32(gvInterviewAppointments.DataKeys[e.Row.RowIndex].Value.ToString());
                    //check if the budget target has some budget lines
                    if (db.InterviewAppointment_Views.Any(p => p.JobID == Convert.ToInt32(_jobID)))
                    {
                        GridView _gvInterviewAppointmentApplicants = e.Row.FindControl("gvInterviewAppointmentApplicants") as GridView;
                        DisplayInterviewAppointmentApplicantsGridView(_jobID, _gvInterviewAppointmentApplicants);//load the interview appointment applicants griview data
                        return;
                    }
                    else
                    {
                        //disable the edit and delete budget item buttons
                        //Button _btnDetailedBudgetEditItem = (Button)e.Row.FindControl("btnDetailedBudgetEditItem");
                        //_btnDetailedBudgetEditItem.Visible = false;// disable the edit item button
                        Button _btnDeleteApplicantInterviewSchedule = (Button)e.Row.FindControl("btnDeleteApplicantInterviewSchedule");
                        _btnDeleteApplicantInterviewSchedule.Visible = false;//disable delete applicant's interview schedule

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Failed in loading applicant sheduled for interview !" + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }
        //load applicants data
        private void DisplayInterviewAppointmentApplicantsGridView(int _jobID, GridView _gvInterviewAppointmentApplicants)
        {
            _gvInterviewAppointmentApplicants.DataSourceID = null;
            object _getInterviewAppointmentApplicants = db.InterviewAppointment_Views.Where(p => p.JobID == Convert.ToInt32(_jobID)).OrderBy(p => p.ApplicantReference);//
            _gvInterviewAppointmentApplicants.ToolTip = _jobID.ToString();
            _gvInterviewAppointmentApplicants.DataSource = _getInterviewAppointmentApplicants;
            _gvInterviewAppointmentApplicants.DataBind();
        }
        //check if any interview appointment is selected before editing
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvInterviewAppointments))//check if an interview appointment ight is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no interview appointment selected. Select an interview appointment to edit!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvInterviewAppointments.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one interview appointment. Select only one interview appointment to edit at a time!", this, PanelInterviewAppointments);
                        return;
                    }
                    foreach (GridViewRow _grv in gvInterviewAppointments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFInterviewAppointmentID = (HiddenField)_grv.FindControl("HiddenField1");
                            LoadInterviewAppointmentControlsForEdit(Convert.ToInt32(_HFInterviewAppointmentID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }

        //load interview appointment controls for edit purpose
        private void LoadInterviewAppointmentControlsForEdit(int _interviewAppointmentID)
        {
            InterviewAppointment_View getInterviewAppointment = db.InterviewAppointment_Views.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
            HiddenFieldInterviewAppointmentID.Value = _interviewAppointmentID.ToString();
            ddlJobReferenceAndTitle.SelectedValue = getInterviewAppointment.JobID.ToString();
            GetShortlistedJobApplicantsPerJob((int)getInterviewAppointment.JobID);//get applicants for the job
            ddlApplicantReferenceAndName.SelectedValue = getInterviewAppointment.ApplicantID.ToString();
            txtAppointmentDate.Text = _hrClass.ShortDateDayStart(getInterviewAppointment.AppointmentDate.Value.Date);
            txtAppointmentTime.Text = getInterviewAppointment.AppointmentTime.ToString();
            txtAppointmentComments.Text = getInterviewAppointment.AppointmentRemarks.Trim();
            ModalPopupExtenderAddInterviewAppointment.Show();
            lnkBtnSaveInterviewAppointment.Visible = false;
            lnkBtnUpdateInterviewAppointment.Enabled = true;
            lbAddInterviewAppointmentHeader.Text = "Edit Interview Appointment";
            ImageAddInterviewAppointmentHeader.ImageUrl = "~/Images/icons/Medium/Modify.png";
            ModalPopupExtenderAddInterviewAppointment.Show();
            return;
        }
        //load add inerview comments against an interview appointment
        protected void LinkButtonOverallInterviewComments_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewRadioButtonSelected(gvInterviewAppointments))//check if an interview appointment ight is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no interview appointment selected. Select a job to add the overall interview comments!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    foreach (GridViewRow _grv in gvInterviewAppointments.Rows)
                    {
                        RadioButton _rb = (RadioButton)_grv.FindControl("RadioButton1");
                        //if (_cb.Checked) ++x;

                        if (_rb.Checked == true)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenFieldJobID");

                            //_hrClass.LoadHRManagerMessageBox(1, _HFJobID.Value, this, PanelInterviewAppointments);
                            LoadAddOverallInterviewCommentsControls(Convert.ToInt32(_HFJobID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }

        //populate placed candidate dropdown with the interviewed camdidates
        public void GetInterviewedJobApplicantsPerJob(int _jobID)
        {
            try
            {
                ddlPlacedCandidate.Items.Clear();
                object _getJobs;
                _getJobs = db.PROC_GetInterviewedApplicantsPerJob(_jobID);
                ddlPlacedCandidate.DataSource = _getJobs;
                ddlPlacedCandidate.Items.Insert(0, new ListItem("-Select Placed Candidate-"));
                ddlPlacedCandidate.DataTextField = "ApplicantReferenceAndName";
                ddlPlacedCandidate.DataValueField = "ApplicantID";
                ddlPlacedCandidate.AppendDataBoundItems = true;
                ddlPlacedCandidate.DataBind();
                return;
            }
            catch { }
        }
        //method for loading add overall inetrview comments against a job
        private void LoadAddOverallInterviewCommentsControls(int _jobID)
        {
            //check if there is any interviewed applicant
            if (db.PROC_GetInterviewedApplicantsPerJob(_jobID).Any())
            {
                _hrClass.ClearEntryControls(PanelAddOverallInterviewComments);
                //pupulate placed candidate
                GetInterviewedJobApplicantsPerJob(_jobID);
                //get interview comments details
                PROC_JobInterviewCommentsResult getInterviewComments = db.PROC_JobInterviewComments(_jobID).FirstOrDefault();
                txtOverallInterviewComments.Text = Convert.ToString(getInterviewComments.OverallInterviewComments);
                if (getInterviewComments.IsJobClosed == true) rblCloseTheJob.SelectedIndex = 0;
                else if (getInterviewComments.IsJobClosed == false) rblCloseTheJob.SelectedIndex = 1;
                else rblCloseTheJob.SelectedIndex = -1;
                if (getInterviewComments.PlacedApplicantID != null)
                    ddlPlacedCandidate.SelectedValue = getInterviewComments.PlacedApplicantID.ToString();
                else ddlPlacedCandidate.SelectedIndex = 0;
                if (getInterviewComments.ReportingDate != null && Convert.ToString(getInterviewComments.ReportingDate) != "")
                    txtReportingDate.Text = _hrClass.ShortDateDayStart(getInterviewComments.ReportingDate.Value.Date);
                else txtReportingDate.Text = "";
                lbAddOverallInterviewCommentsHeader.Text = "Overall Interview Comments & Resolutions for Job Ref::" + getInterviewComments.JobReference + ", Job Title::" + getInterviewComments.JobTitle;
                HiddenFieldJobID.Value = _jobID.ToString();
                ModalPopupExtenderAddOverallInterviewComments.Show();
                return;
            }
            else
            {
                _hrClass.LoadHRManagerMessageBox(1, "There is no job applicant who has been set as interviewed. First set the applicant who has been interviewed before you can add the job overall interview comments!", this, PanelInterviewAppointments);
                return;
            }
        }

        //validate save interview overal coments
        private string ValidateAddOverallInterviewComments()
        {
            if (txtOverallInterviewComments.Text.Trim() == "")
            {
                txtOverallInterviewComments.Focus();
                return "Enter overall interview comments!";
            }
            else if (rblCloseTheJob.SelectedIndex == -1)
            {
                rblCloseTheJob.Focus();
                return "Select if you want to close the job or not!";
            }

            else if (rblCloseTheJob.SelectedIndex == 0 && ddlPlacedCandidate.SelectedIndex == 0)
            {
                ddlPlacedCandidate.Focus();
                return "Select the placed candidate!";
            }
            else if (rblCloseTheJob.SelectedIndex == 0 && txtReportingDate.Text.Trim() == "")
            {
                txtReportingDate.Focus();
                return "Enter reporting  date!";
            }
            else if (rblCloseTheJob.SelectedIndex == 0 && _hrClass.isDateValid(txtReportingDate.Text.Trim()) == false)
            {
                txtReportingDate.Focus();
                return "Invalid interview reporting date entered!";
            }
            else if (ddlPlacedCandidate.SelectedIndex != 0 && _hrClass.isDateValid(txtReportingDate.Text.Trim()) == false)
            {
                txtReportingDate.Focus();
                return "Invalid interview reporting date entered!";
            }
            else return "";
        }
        //method for saving overall interview commmets
        private void SaveOverallInterviewComments()
        {
            ModalPopupExtenderAddOverallInterviewComments.Show();
            try
            {
                string _error = ValidateAddOverallInterviewComments();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    int _jobID = Convert.ToInt32(HiddenFieldJobID.Value);
                    //save overall interview comments
                    Job addInterviewComment = db.Jobs.Single(p => p.JobID == _jobID);
                    addInterviewComment.OverallInterviewComments = txtOverallInterviewComments.Text.Trim();
                    if (rblCloseTheJob.SelectedIndex == 0)//close the job
                    {
                        addInterviewComment.IsJobClosed = true;
                        addInterviewComment.DateClosed = DateTime.Now;

                        addInterviewComment.ClosedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    }
                    else//do not close the job
                    {
                        addInterviewComment.IsJobClosed = false;
                        addInterviewComment.DateClosed = null;
                        addInterviewComment.ClosedByStaffID = null;
                    }
                    //set placed candidate
                    if (ddlPlacedCandidate.SelectedIndex != 0)//place the candidate
                    {
                        //interview appointment associated with the candidate selected for placement
                        Guid _applicantID = _hrClass.ReturnGuid(ddlPlacedCandidate.SelectedValue);
                        InterviewAppointment_View getInterviewAppointment = db.InterviewAppointment_Views.Single(p => p.JobID == _jobID && p.ApplicantID == _applicantID);
                        //check if the placement date is less that the interview date
                        DateTime _reportingDate = Convert.ToDateTime(txtReportingDate.Text.Trim());
                        if (_reportingDate.Date <= getInterviewAppointment.AppointmentDate)
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "Saving overall interview comments failed. Reporting date for the selected candidate should be after his interview appointment date of " + _hrClass.ConvertDateDayStart(_reportingDate.ToString()), this, PanelInterviewAppointments);
                            return;
                        }
                        else
                        {
                            addInterviewComment.PlacedApplicantID = _applicantID;
                            addInterviewComment.ReportingDate = _reportingDate.Date;
                        }
                    }
                    else//do not place the candidate
                    {
                        addInterviewComment.PlacedApplicantID = null;
                        addInterviewComment.ReportingDate = null;
                    }
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "Job overall comments and resolutions have been successfully saved!", this, PanelInterviewAppointments);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Saving overall interview comments failed. " + ex.Message.ToString() + ". Please try again!", this, PanelInterviewAppointments);
                return;
            }
        }
        //save overall interview comments
        protected void lnkBtnSaveOverallInterviewComments_Click(object sender, EventArgs e)
        {
            SaveOverallInterviewComments(); return;
        }

        //row command for interview appointment
        protected void gvInterviewAppointments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.CompareTo("AddNewInterviewAppointment") == 0)//add new interview appointment
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex.Value = _selectedRow.RowIndex.ToString();
                    HiddenField _hfJobID = gvInterviewAppointments.Rows[_rowIndex].FindControl("HiddenFieldJobID") as HiddenField;
                    string _jobID = _hfJobID.Value;
                    //display controls for adding a new interview appointment
                    //LoadAddNewInterviewAppointmentControls(_jobID);
                    return;
                }
                else if (e.CommandName.CompareTo("EditApplicantInterviewSchedule") == 0)//edit interview appointment
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex.Value = _selectedRow.RowIndex.ToString();
                    //get child grid view(i.e. interview appointment applicant grid view)
                    GridView _gvInterviewAppointmentApplicants = gvInterviewAppointments.Rows[_rowIndex].FindControl("gvInterviewAppointmentApplicants") as GridView;
                    //load edit details for the selected interview appointment applicant
                    LoadEditInterviewAppointmentApplicantControls(_gvInterviewAppointmentApplicants);
                    return;
                }
                else if (e.CommandName.CompareTo("AddApplicantInterviewComments") == 0)//add applicant's interview comments
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex.Value = _selectedRow.RowIndex.ToString();
                    //get child grid view(i.e. interview appointment applicant grid view)
                    GridView _gvInterviewAppointmentApplicants = gvInterviewAppointments.Rows[_rowIndex].FindControl("gvInterviewAppointmentApplicants") as GridView;
                    //load addm interview comments for the selected interview appointment applicant
                    LoadAddApplicantsInterviewCommentsControls(_gvInterviewAppointmentApplicants);
                    return;
                }
                else if (e.CommandName.CompareTo("DeleteApplicantInterviewSchedule") == 0)//delete interview appointment applicant
                {
                    GridViewRow _selectedRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int _rowIndex = _selectedRow.RowIndex;
                    HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex.Value = _selectedRow.RowIndex.ToString();
                    //get child grid view(i.e. interview appointment applicant grid view)
                    GridView _gvInterviewAppointmentApplicants = gvInterviewAppointments.Rows[_rowIndex].FindControl("gvInterviewAppointmentApplicants") as GridView;
                    DeleteInterviewAppointmentApplicant(_gvInterviewAppointmentApplicants);
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error ocurred ! " + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }

        }
        //load interface for editing an interview appointment
        private void LoadEditInterviewAppointmentApplicantControls(GridView _gvInterviewAppointmentApplicants)
        {
            if (!_hrClass.isGridviewItemSelected(_gvInterviewAppointmentApplicants))
            {
                _hrClass.LoadHRManagerMessageBox(1, "There is no interview appointment selected. Select an interview appointment to edit!", this, PanelInterviewAppointments);
                return;
            }
            else
            {
                int x = 0;
                foreach (GridViewRow _grvRow in _gvInterviewAppointmentApplicants.Rows)
                {
                    CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                    if (_cb.Checked) ++x;
                }
                if (x > 1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one interview appointment. <br>Select only one interview appointment to edit at a time!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    foreach (GridViewRow _grvRow in _gvInterviewAppointmentApplicants.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                        if (_cb.Checked)
                        {
                            HiddenField _HFInterviewAppointmentID = (HiddenField)_grvRow.FindControl("HiddenFieldInterviewAppointmentID");
                            HiddenFieldInterviewAppointmentID.Value = _HFInterviewAppointmentID.Value;
                            //load interview appointment being edited
                            LoadInterviewAppointmentControlsForEdit(Convert.ToInt32(_HFInterviewAppointmentID.Value));
                            return;
                        }
                    }
                }
            }
        }
        //load interface for adding an applicant's interview comments
        private void LoadAddApplicantsInterviewCommentsControls(GridView _gvInterviewAppointmentApplicants)
        {
            if (!_hrClass.isGridviewItemSelected(_gvInterviewAppointmentApplicants))
            {
                _hrClass.LoadHRManagerMessageBox(1, "There is no interview appointment selected. Select an interview appointment for you to be able to add the interview comments!", this, PanelInterviewAppointments);
                return;
            }
            else
            {
                int x = 0;
                foreach (GridViewRow _grvRow in _gvInterviewAppointmentApplicants.Rows)
                {
                    CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                    if (_cb.Checked) ++x;
                }
                if (x > 1)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one interview appointment. <br>Select only one interview appointment to add interview comments at a time!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    foreach (GridViewRow _grvRow in _gvInterviewAppointmentApplicants.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                        if (_cb.Checked)
                        {
                            HiddenField _HFInterviewAppointmentID = (HiddenField)_grvRow.FindControl("HiddenFieldInterviewAppointmentID");
                            HiddenFieldInterviewAppointmentID.Value = _HFInterviewAppointmentID.Value;
                            //load add applicant's interview comments
                            LoadAddInterviewCommentsControls(Convert.ToInt32(_HFInterviewAppointmentID.Value));
                            return;
                        }
                    }
                }
            }
        }
        //method for loading controls for adding interview comments
        private void LoadAddInterviewCommentsControls(int _interviewAppointmentID)
        {
            _hrClass.ClearEntryControls(PanelAddInterviewComments);//clear 
            InterviewAppointment_View getInterviewAppointment = db.InterviewAppointment_Views.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
            HiddenFieldInterviewAppointmentID.Value = _interviewAppointmentID.ToString();
            HiddenFieldJobID.Value = getInterviewAppointment.JobID.ToString();
            lbAddInterviewCommentsHeader.Text = "Interview Comments for " + getInterviewAppointment.ApplicantName + ", Job Title: " + getInterviewAppointment.JobTitle;
            rblInterviewComplete.SelectedValue = getInterviewAppointment.IsInterviewConducted.ToString();
            txtInterviewComments.Text = getInterviewAppointment.InterviewComments;
           
            if (getInterviewAppointment.InterviewRecommendation != null)
                rblInterviewRecommendation.SelectedValue = getInterviewAppointment.InterviewRecommendation;
            else rblInterviewRecommendation.SelectedIndex = -1;
            if (getInterviewAppointment.InterviewChoice != null)
                rblInterviewChoice.SelectedValue = getInterviewAppointment.InterviewChoice.ToString();
            else
                rblInterviewChoice.SelectedIndex = -1;
            //populate interviewers
            string _interviewersID = "";
            if (getInterviewAppointment.InterviewConductedBy != null && getInterviewAppointment.InterviewConductedBy != "")//check if there are any interviewers saved
            {
                _interviewersID = getInterviewAppointment.InterviewConductedBy.ToString();
                PopulateInterviewersListBox(_interviewersID);
            }
            else
                lstBoxInterviewedBy.Items.Clear();

            ModalPopupExtenderAddInterviewComments.Show();
            return;
        }
        //delete interview appointment applicant details
        private void DeleteInterviewAppointmentApplicant(GridView _gvInterviewAppointmentApplicants)
        {
            //if (!_cdtfClasses.CheckSelectGridCheckBox(_gvBudgetDetails))
            //{
            //    _cdtfClasses.LoadCDTFMessageBox(1, 1, "There is no budget item that is selected!. Select record(s) to delete!", this, PanelFinanceAndLaunchNewContract);
            //    return;
            //}
            //else
            //{
            //    foreach (GridViewRow _grvRow in _gvBudgetDetails.Rows)
            //    {
            //        CheckBox _cb = (CheckBox)_grvRow.FindControl("CheckBox1");
            //        if (_cb.Checked)
            //        {
            //            HiddenField _HFBudgetItemID = (HiddenField)_grvRow.FindControl("HiddenFieldBudgetItemID");
            //            int _budgetItemID = Convert.ToInt32(_HFBudgetItemID.Value);
            //            //check if the budget item is addend to the planned installment targets
            //            if (db.Finance_Contract_PlannedInstallmentTargets.Any(p => p.Finance_Contract_BudgetItemsID == _budgetItemID))
            //            {
            //                //delete the records 
            //                foreach (Finance_Contract_PlannedInstallmentTarget deletePlannedInstallment in db.Finance_Contract_PlannedInstallmentTargets.Where(p => p.Finance_Contract_BudgetItemsID == _budgetItemID))
            //                {
            //                    db.Finance_Contract_PlannedInstallmentTargets.DeleteOnSubmit(deletePlannedInstallment);
            //                    db.SubmitChanges();
            //                }
            //                gvPlannedInstallmentsData.DataBind();
            //            }
            //            //delete a budget item
            //            Finance_Contract_BudgetItem deleteBudgetItem = db.Finance_Contract_BudgetItems.Single(p => p.Finance_Contract_BudgetItemsID == _budgetItemID);
            //            db.Finance_Contract_BudgetItems.DeleteOnSubmit(deleteBudgetItem);
            //            db.SubmitChanges();
            //        }
            //    }
            //    //update total master amounts after delete
            //    UpdateDetailedBudgetTotalAmountsAfterDelete();
            //    _cdtfClasses.LoadCDTFMessageBox(1, 3, "Budget item has been successfully deleted ! ", this, PanelFinanceAndLaunchNewContract);
            //    gvDetailedBudget.DataBind();
            //    ExpandACollapsiblePanelExtenderAccessed();
            //    RefreshSourceOfFundingControls();//refresh source of funding controls
            //    return;
            //}
        }
        //load interviewers list box
        private void PopulateInterviewersListBox(string _interviewersID)
        {
            lstBoxInterviewedBy.Items.Clear();
            string[] ids = _interviewersID.Split(',');
            foreach (string interviewerID in ids)
            {
                if (interviewerID.Trim() != "")
                {
                    //get inetrviewer name
                    string interviewerName = _hrClass.getStaffNames(_hrClass.ReturnGuid(interviewerID));
                    ListItem item = new ListItem();
                    item.Value = interviewerID;
                    item.Text = interviewerName;
                    lstBoxInterviewedBy.Items.Add(item);
                }
            }
        }
        //validate add interview comments
        private string ValidateAddInterviewCommentsControls()
        {

            if (rblInterviewComplete.SelectedIndex == -1)
            {
                rblInterviewComplete.Focus();
                return "Select whether the interview has been conducted or not!";
            }
            else if (txtInterviewComments.Text.Trim() == "")
            {
                txtInterviewComments.Focus();
                return "Enter interview comments!";
            }
            else if (rblInterviewRecommendation.SelectedIndex == -1)
            {
                rblInterviewRecommendation.Focus();
                return "Select interview recommendations!";
            }
            else if (rblInterviewChoice.SelectedIndex == -1)
            {
                rblInterviewChoice.Focus();
                return "Select applicant position choice!";
            }
            else if (lstBoxInterviewedBy.Items.Count == 0)
            {
                ddlInterviewedBy.Focus();
                return "There is no interviewer added. Please select atleast one interviwer!";
            }
            else return "";
        }
        //method for saving interview comments
        private void SaveInterviewComments()
        {
            ModalPopupExtenderAddInterviewComments.Show();
            try
            {
                //check for errors
                string _error = ValidateAddInterviewCommentsControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    //save the interview comments
                    int _interviewAppointmentID = Convert.ToInt32(HiddenFieldInterviewAppointmentID.Value);
                    InterviewAppointment interviewComments = db.InterviewAppointments.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
                    if (rblInterviewComplete.SelectedIndex == 0)
                        interviewComments.IsInterviewConducted = true;
                    else interviewComments.IsInterviewConducted = false;
                    interviewComments.InterviewComments = txtInterviewComments.Text.Trim();
                    interviewComments.InterviewRecommendation = rblInterviewRecommendation.SelectedValue;
                    interviewComments.InterviewChoice = Convert.ToInt32(rblInterviewChoice.SelectedValue);
                    //interviewComments.InterviewConductedBy
                    interviewComments.DateCommentsAdded = DateTime.Now;
                    interviewComments.CommentsAddedByStaffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "Interview comments have been successfully added!", this, PanelInterviewAppointments);
                    return;
                }

            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured. " + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }

        //save interview comments
        protected void lnkBtnSaveInterviewComments_Click(object sender, EventArgs e)
        {
            SaveInterviewComments();
            return;
        }
        //method for adding an interviewer
        private void AddInterviewer()
        {
            ModalPopupExtenderAddInterviewComments.Show();
            try
            {
                if (ddlInterviewedBy.SelectedIndex == 0)
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No interviewer selected. Please an interviewer!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    List<string> _addedInterviewerIDs = new List<string>();//create an array string
                    foreach (ListItem itemInterviewer in lstBoxInterviewedBy.Items)
                    {
                        _addedInterviewerIDs.Add(itemInterviewer.Value);
                    }
                    //check if the selected interviewer is already added
                    if (_addedInterviewerIDs.Contains(ddlInterviewedBy.SelectedItem.Value))//
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "The selected interviewer is already added in the interviewers' list!", this, PanelInterviewAppointments);
                        return;
                    }
                    else
                    {
                        // int _memberID = Convert.ToInt32(ddlBOTMember.SelectedItem.Value);
                        ListItem item = new ListItem();
                        item.Value = ddlInterviewedBy.SelectedItem.Value;
                        item.Text = ddlInterviewedBy.SelectedItem.Text;
                        lstBoxInterviewedBy.Items.Add(item);
                        //save the interviewer details in the database
                        SaveInterviewer();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Adding interviewer Failed. " + ex.Message.ToString() + ". Please try again!", this, PanelInterviewAppointments);
                return;
            }
        }
        //add new interviewer
        protected void lnkBtnAddInterviewer_Click(object sender, EventArgs e)
        {
            AddInterviewer(); return;
        }
        //remove interviewer
        private void RemoveInterviewer()
        {
            ModalPopupExtenderAddInterviewComments.Show();
            try
            {
                //check if we are editing and remove it from the db
                //save the new tec meeting attendee if we are performing updating
                foreach (ListItem item in lstBoxInterviewedBy.Items)
                {
                    if (item.Selected)
                    {
                        lstBoxInterviewedBy.Items.Remove(lstBoxInterviewedBy.SelectedItem);
                        //delete the selected item from the db
                        SaveInterviewer();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Removing interviewer failed. " + ex.Message.ToString() + ". Please try again!", this, PanelInterviewAppointments);
                return;
            }
        }
        //remove interviewer
        protected void lnkBtnRemoveInterviewer_Click(object sender, EventArgs e)
        {
            RemoveInterviewer(); return;
        }
        //save interviewre
        private void SaveInterviewer()
        {
            string _addedInterviewers = "";
            if (lstBoxInterviewedBy.Items.Count != 0)//check if there are any items in the list box
            {
                foreach (ListItem item in lstBoxInterviewedBy.Items)
                {
                    _addedInterviewers += item.Value + ",";
                }
            }
            int _interviewAppointmentID = Convert.ToInt32(HiddenFieldInterviewAppointmentID.Value);
            //add the interviewers
            InterviewAppointment interviewer = db.InterviewAppointments.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
            interviewer.InterviewConductedBy = _addedInterviewers;
            db.SubmitChanges();
        }
        //load delete an interview appointment
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvInterviewAppointments))//check if there is any grid view that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "No record selected. Please select an interview appointment record to delete!", this, PanelInterviewAppointments);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvInterviewAppointments.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected interview appointment?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected interview appointments?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }

        //delete selected interview appointmnet
        protected void DeleteInterviewAppointment()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvInterviewAppointments.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;

                    if (_cb.Checked)
                    {
                        HiddenField _HFInterviewAppointmentID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _interviewAppointmentID = Convert.ToInt32(_HFInterviewAppointmentID.Value);

                        //delete the interview appointment
                        InterviewAppointment deleteInterviewAppointment = db.InterviewAppointments.Single(p => p.InterviewAppointmentID == _interviewAppointmentID);
                        db.InterviewAppointments.DeleteOnSubmit(deleteInterviewAppointment);
                        db.SubmitChanges();
                    }
                }
                //refresh  after delete is done
                DisplayInterviewAppointments();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected interview appointment has been deleted.", this, PanelInterviewAppointments);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected interview appointments have been deleted.", this, PanelInterviewAppointments);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete failed . " + ex.Message.ToString(), this, PanelInterviewAppointments);
                return;
            }
        }
        //delete the selected interview appointment records
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteInterviewAppointment();
            return;
        }

        ////search for an interview by use of interview appointment date
        //private void SearchForInterviewAppointmentByAppointmentDate()
        //{
        //    if (IsPostBack)
        //    {
        //        try
        //        {
        //            //if (txtSearchByAppointmentDate.Text.Trim() == "")
        //            //{
        //            //    txtSearchByAppointmentDate.Focus();
        //            //    _hrClass.LoadHRManagerMessageBox(1, "Enter interview appointment date!", this, PanelInterviewAppointments);
        //            //    return;
        //            //}
        //            if (txtSearchByAppointmentDate.Text.Trim() == "__/___/____")
        //            {
        //                // DisplayInterviewAppointments(); return;
        //            }
        //            else if (_hrClass.isDateValid(txtSearchByAppointmentDate.Text.Trim()) == false)
        //            {
        //                txtSearchByAppointmentDate.Focus();
        //                _hrClass.LoadHRManagerMessageBox(1, "Invalid interview appointment date entered!", this, PanelInterviewAppointments);
        //                return;
        //            }
        //            else
        //            {
        //                object _searchDetails;
        //                string _searchText = txtSearchByAppointmentDate.Text.Trim().ToLower();
        //                _searchDetails = db.InterviewAppointment_Views.Where(p => p.AppointmentDate.Value.Date == Convert.ToDateTime(_searchText).Date).OrderByDescending(p => p.InterviewAppointmentID);
        //                gvInterviewAppointments.DataSourceID = null;
        //                gvInterviewAppointments.DataSource = _searchDetails;
        //                Session["gvInterviewAppointmentsData"] = _searchDetails;
        //                gvInterviewAppointments.DataBind();
        //                txtSearchByAppointmentDate.Focus();
        //                return;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelInterviewAppointments);
        //            return;
        //        }
        //    }
        //}
        ////search for appointment by appointment date
        //protected void txtSearchByAppointmentDate_TextChanged(object sender, EventArgs e)
        //{
        //    SearchForInterviewAppointmentByAppointmentDate();
        //    return;
        //}
    }
}