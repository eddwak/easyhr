﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Recruitment_Module
{
    public partial class Recruitment_Settings : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetListingItems(ddlCountryLocation, 1000, "Country");//display countries(1000) available
                PopulateGridViews();//populate various gridviews
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        //clear entry controls
        private void ClearEntryControls()
        {
            //clear entry controls
            txtRecruitmentName.Text = "";
            HiddenFieldRecruitmentSettingID.Value = "";
            lnkBtnSaveRecruitmentSetting.Visible = true;
            lnkBtnUpdateRecruitmentSetting.Enabled = false;
            lnkBtnClearRecruitmentSetting.Visible = true;
            lbAddRecruitmentSettingHeader.Text = "Add New " + lbRecruitmentSettingName.Text;
            ImageAddRecruitmentSettingHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //setting form
        private void SetEntryForm(int _listingMasterID, string _settingType)
        {
            HiddenFieldListingMasterID.Value = _listingMasterID.ToString();
            lbAddRecruitmentSettingHeader.Text = "Add New " + _settingType;
            lbRecruitmentSettingName.Text = _settingType;
            legendRecruitmentSettings.InnerText = _settingType + " Details";
        }
        //load add new recruitment setting item
        private void LoadAddNewRecruitmentSetting()
        {
            if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                SetEntryForm(10000, "Job Type");//master ID 10000 for job types
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                SetEntryForm(11000, "Job Category");//listing master id 11000 for job categories
            //else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
            //    SetEntryForm(12000, "Job Location");//listing master is 12000 for job locations
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                SetEntryForm(13000, "Job Status");//listing master id 13000 for job status
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                SetEntryForm(14000, "Currency");//listing master id 14000 for currencies
            ModalPopupExtenderAddRecruitmentSetting.Show();
            return;
        }
        protected void LinkButtonNewRecruitmentSetting_Click(object sender, EventArgs e)
        {
            ClearEntryControls();
            LoadAddNewRecruitmentSetting();
            return;
        }
        //validate settings entry controls
        private string ValidateEntryControls()
        {
            if (txtRecruitmentName.Text.Trim() == "")
            {
                if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                    return "Enter job type!";
                else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                    return "Enter job category!";
                else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
                    return "Enter job location!";
                else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                    return "Enter job status!";
                else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                    return "Enter currency!";
                else return "";
            }
            else return "";
        }
        //save rectruitement Setting
        private void SaveRecruitmentSetting()
        {
            ModalPopupExtenderAddRecruitmentSetting.Show();
            try
            {
                string _error = ValidateEntryControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    //get listing master id
                    int _listingMasterID = Convert.ToInt32(HiddenFieldListingMasterID.Value);
                    string _listingItemValue = txtRecruitmentName.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _listingItemValue))//check if the valee item name exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, lbRecruitmentSettingName.Text + " you are trying to add already exists!", this, PanelRecruitmentSettings);
                        return;
                    }
                    else
                    {
                        //save details
                        string _listingType = lbRecruitmentSettingName.Text;
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _listingItemValue, null, null, null, null, null, null, null, null, HiddenFieldRecruitmentSettingID);

                        //refersh gridview
                        RefreshCurrentTabGridView();
                        _hrClass.LoadHRManagerMessageBox(2, lbRecruitmentSettingName.Text + " has been successfully saved.", this, PanelRecruitmentSettings);
                        lnkBtnUpdateRecruitmentSetting.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save failed. " + ex.Message.ToString(), this, PanelRecruitmentSettings);
            }
        }
        //update a recruitment setting
        private void UpdateRecruitmentSetting()
        {
            ModalPopupExtenderAddRecruitmentSetting.Show();
            string _error = ValidateEntryControls();//check for errors
            if (_error != "")
            {
                _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelRecruitmentSettings);
                return;
            }
            else
            {
                //get listing master id
                int _listingMasterID = Convert.ToInt32(HiddenFieldListingMasterID.Value);
                int _listingItemID = Convert.ToInt32(HiddenFieldRecruitmentSettingID.Value);
                string _listingItemValue = txtRecruitmentName.Text.Trim();
                if (_hrClass.DoesListingItemExist(_listingMasterID, _listingItemID, _listingItemValue))//check if the record  already exists
                {
                    lbError.Text = "Save failed, the role you are trying to update with already exists!";
                    lbInfo.Text = "";
                }
                else
                {
                    //update
                    _hrClass.UpdateListingItem(_listingMasterID, _listingItemID, lbRecruitmentSettingName.Text, _listingItemValue, null, null, null, null, null, null, null, null);
                    //refresh gridview
                    RefreshCurrentTabGridView();
                    _hrClass.LoadHRManagerMessageBox(2, lbRecruitmentSettingName.Text + " has been successfully updated.", this, PanelRecruitmentSettings);
                    return;
                }
            }
        }
        //save details
        protected void lnkBtnSaveRecruitmentSetting_Click(object sender, EventArgs e)
        {
            SaveRecruitmentSetting(); return;
        }
        //update details
        protected void lnkBtnUpdateRecruitmentSetting_Click(object sender, EventArgs e)
        {
            UpdateRecruitmentSetting(); return;
        }
        //clear controls
        protected void lnkBtnClearRecruitmentSetting_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderAddRecruitmentSetting.Show();
            ClearEntryControls(); return;
        }
        //refresh gridview after new additio, edit or delet
        private void RefreshCurrentTabGridView()
        {
            if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                DisplayJobTypes();
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                DisplayJobCategories();
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
                DisplayLocations();
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                DisplayJobStatus();
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                DisplayCurrencies();
        }
        //populate all grid views
        private void PopulateGridViews()
        {
            DisplayJobTypes();//load job types
            DisplayJobCategories();//load job categories
            DisplayLocations();//load locations
            DisplayJobStatus();//load job status
            DisplayCurrencies();//load currencies
            return;
        }
        //display job types
        private void DisplayJobTypes()
        {
            try
            {
                object _display;
                _display = db.JobTypes_Views.Select(p => p).OrderBy(p => p.JobType);
                gvJobTypes.DataSourceID = null;
                gvJobTypes.DataSource = _display;
                gvJobTypes.DataBind();
                return;
            }
            catch { }
        }
        //display job categories
        private void DisplayJobCategories()
        {
            try
            {
                object _display;
                _display = db.JobCategories_Views.Select(p => p).OrderBy(p => p.JobCategory);
                gvJobCategories.DataSourceID = null;
                gvJobCategories.DataSource = _display;
                gvJobCategories.DataBind();
                return;
            }
            catch { }
        }
        //display locations
        private void DisplayLocations()
        {
            try
            {
                object _display;
                _display = db.Locations_Views.Select(p => p).OrderBy(p => p.Country).ThenBy(p => p.Location);
                gvLocations.DataSourceID = null;
                gvLocations.DataSource = _display;
                gvLocations.DataBind();
                return;
            }
            catch { }
        }
        //display job status
        private void DisplayJobStatus()
        {
            try
            {
                object _display;
                _display = db.JobStatus_Views.Select(p => p).OrderBy(p => p.JobStatus);
                gvJobStatus.DataSourceID = null;
                gvJobStatus.DataSource = _display;
                gvJobStatus.DataBind();
                return;
            }
            catch { }
        }
        //display currencies
        private void DisplayCurrencies()
        {
            try
            {
                object _display;
                _display = db.Currencies_Views.Select(p => p).OrderBy(p => p.Currency);
                gvCurrencies.DataSourceID = null;
                gvCurrencies.DataSource = _display;
                gvCurrencies.DataBind();
                return;
            }
            catch { }
        }
        //load edit recruitment settings
        private void LoadEditRecruitment(GridView _gvRecruitmentSettings, string _listingItem)
        {
            try
            {
                _listingItem = _listingItem.ToLower();
                if (!_hrClass.isGridviewItemSelected(_gvRecruitmentSettings))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a " + _listingItem + " to edit!", this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in _gvRecruitmentSettings.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one " + _listingItem + ". Select only one " + _listingItem + " to edit at a time!", this, PanelRecruitmentSettings);
                        return;
                    }
                    foreach (GridViewRow _grv in _gvRecruitmentSettings.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFListingItemID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit record
                            LoadEditRecordControls(Convert.ToInt32(_HFListingItemID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelRecruitmentSettings);
            }
        }

        // populate the edit record controls
        private void LoadEditRecordControls(int _listingItemID)
        {
            ClearEntryControls();
            ListingItem getListingItem = db.ListingItems.Single(p => p.ListingItemIID == _listingItemID);
            HiddenFieldListingMasterID.Value = getListingItem.ListingMasterID.ToString();
            HiddenFieldRecruitmentSettingID.Value = getListingItem.ListingItemIID.ToString();
            txtRecruitmentName.Text = getListingItem.LisitingDetailValue;

            lnkBtnSaveRecruitmentSetting.Visible = false;
            lnkBtnClearRecruitmentSetting.Visible = true;
            lnkBtnUpdateRecruitmentSetting.Enabled = true;
            ImageAddRecruitmentSettingHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddRecruitmentSetting.Show();
            return;
        }
        //setting for edit form
        private void SetEditEntryForm(GridView _gvRecruitmentSettings, string _settingType)
        {
            lbRecruitmentSettingName.Text = _settingType;
            legendRecruitmentSettings.InnerText = _settingType + " Details";
            LoadEditRecruitment(_gvRecruitmentSettings, _settingType);
            lbAddRecruitmentSettingHeader.Text = "Edit " + _settingType;
            return;
        }
        //load edit recruitment setting item
        private void LoadEditRecruitmentSetting()
        {
            if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                SetEditEntryForm(gvJobTypes, "Job Type");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                SetEditEntryForm(gvJobCategories, "Job Category");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
                SetEditEntryForm(gvLocations, "Job Location");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                SetEditEntryForm(gvJobStatus, "Job Status");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                SetEditEntryForm(gvCurrencies, "Currency");
            return;
        }
        //click event for loading edit controls
        protected void LinkButtonEditRecruitmentSetting_Click(object sender, EventArgs e)
        {
            LoadEditRecruitmentSetting();
            return;
        }
        //load delete recruitment setting
        private void LoadDeleteRecruitmentSettings(GridView _gvRecruitmentSettings, string _settingType)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(_gvRecruitmentSettings))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in _gvRecruitmentSettings.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelRecruitmentSettings);
                return;
            }
        }
        //load delete recruitment setting item
        private void LoadDeleteRecruitmentSetting()
        {
            if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                LoadDeleteRecruitmentSettings(gvJobTypes, "Job Type");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                LoadDeleteRecruitmentSettings(gvJobCategories, "Job Category");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
                LoadDeleteRecruitmentSettings(gvLocations, "Job Location");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                LoadDeleteRecruitmentSettings(gvJobStatus, "Job Status");
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                LoadDeleteRecruitmentSettings(gvCurrencies, "Currency");
            return;
        }
        //click event for loading delete details
        protected void LinkButtonDeleteRecruitmentSetting_Click(object sender, EventArgs e)
        {
            LoadDeleteRecruitmentSetting();
            return;
        }
        //delete a recruitment setting item
        private void DeleteRecruitmentSettings(GridView _gvRecruitmentSetting)
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in _gvRecruitmentSetting.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFListingItemID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _listingItemID = Convert.ToInt32(_HFListingItemID.Value);

                        ListingItem deleteListingItem = db.ListingItems.Single(p => p.ListingItemIID == _listingItemID);

                        //delete the role if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteListingItem);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                RefreshCurrentTabGridView();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected record has been deleted.", this, PanelRecruitmentSettings);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected records have been deleted.", this, PanelRecruitmentSettings);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelRecruitmentSettings);
                return;
            }
        }
        //perform delete recruitment setting item
        private void DeleteRecruitmentSettings()
        {
            if (TabContainerRecruitmentSettings.ActiveTabIndex == 0)//job types
                DeleteRecruitmentSettings(gvJobTypes);
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 1)//job categories
                DeleteRecruitmentSettings(gvJobCategories);
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 2)//job locations
                DeleteRecruitmentSettings(gvLocations);
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 3)//job Status
                DeleteRecruitmentSettings(gvJobStatus);
            else if (TabContainerRecruitmentSettings.ActiveTabIndex == 4)//job Currency
                DeleteRecruitmentSettings(gvCurrencies);
            return;
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteRecruitmentSettings();//delete selected records(s)
            return;
        }
        //load and new job location
        protected void LinkButtonNewJobLocation_Click(object sender, EventArgs e)
        {
            ClearAddJobLocationControls();
            ModalPopupExtenderAddLocation.Show();
            return;
        }
        //clear add job location controls
        private void ClearAddJobLocationControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddLocation);
            HiddenFieldLocationID.Value = "";
            lnkBtnSaveLocation.Visible = true;
            lnkBtnUpdateLocation.Enabled = false;
            lnkBtnUpdateLocation.Visible = true;
            lnkBtnClearLocation.Visible = true;
            lbAddLocationHeader.Text = "Add New Location";
            ImageAddLocationHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add job location controls
        private string ValidateAddJobLocationControls()
        {
            if (ddlCountryLocation.SelectedIndex == 0)
                return "Select country!";
            else if (txtJobLocationName.Text.Trim() == "")
                return "Enter location name!";
            else return "";
        }
        //meathod for saving a job location
        private void SaveNewJobLocation()
        {
            ModalPopupExtenderAddLocation.Show();
            try
            {
                string _error = ValidateAddJobLocationControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    //save the new role details
                    const int _listingMasterID = 12000;//Master ID 12000 for locations
                    const string _listingType = "location";
                    int _countryID = Convert.ToInt32(ddlCountryLocation.SelectedValue);
                    string _locationName = txtJobLocationName.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _locationName))//check if the location name exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the location name you are trying to add already exists!", this, PanelRecruitmentSettings);
                        return;
                    }
                    else
                    {
                        //save the new location
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _locationName, null, null, null, null, null, _countryID, null, null, HiddenFieldLocationID);
                        DisplayLocations();
                        lbError.Text = "";
                        _hrClass.LoadHRManagerMessageBox(2, "New job location has been successfully saved.", this, PanelRecruitmentSettings);
                        lnkBtnUpdateLocation.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed. " + ex.Message.ToString() + ". Please try again !", this, PanelRecruitmentSettings);
                return;
            }
        }
        //METHOD FOR UPDATING A JOB LOCATION
        private void UpdateJobLocation()
        {
            ModalPopupExtenderAddLocation.Show();
            try
            {
                string _error = ValidateAddJobLocationControls();
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    int _locationID = Convert.ToInt32(HiddenFieldLocationID.Value);
                    const int _listingMasterID = 12000;//Master ID 12000 for locations
                    const string _listingType = "location";
                    int _countryID = Convert.ToInt32(ddlCountryLocation.SelectedValue);
                    string _locationName = txtJobLocationName.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _locationID, _locationName))//check if the location already exists
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save failed, the location you are trying to update with already exists!", this, PanelRecruitmentSettings);
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _locationID, _listingType, _locationName, null, null, null, null, null, _countryID, null, null);
                        DisplayLocations();
                        _hrClass.LoadHRManagerMessageBox(2, "Location has been successfully updated", this, PanelRecruitmentSettings);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Update failed. " + ex.Message.ToString() + ". Please try again!", this, PanelRecruitmentSettings);
                return;
            }
        }
        //save location
        protected void lnkBtnSaveLocation_Click(object sender, EventArgs e)
        {
            SaveNewJobLocation();
            return;
        }
        //update location
        protected void lnkBtnUpdateLocation_Click(object sender, EventArgs e)
        {
            UpdateJobLocation();
            return;
        }
        //clearlocation  entry controls
        protected void lnkBtnClearLocation_Click(object sender, EventArgs e)
        {
            ClearAddJobLocationControls();
            ModalPopupExtenderAddLocation.Show();
            return;
        }
        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEditJobLocation_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvLocations))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a location to edit!", this, PanelRecruitmentSettings);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvLocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one location to edit at a time!", this, PanelRecruitmentSettings);
                        return;
                    }
                    foreach (GridViewRow _grv in gvLocations.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFLocationID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit location
                            LoadEditLocationControls(Convert.ToInt32(_HFLocationID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelRecruitmentSettings);
            }
        }
        // populate the edit location controls
        private void LoadEditLocationControls(int _locationID)
        {
            ClearAddJobLocationControls();
            Locations_View getLocation = db.Locations_Views.Single(p => p.LocationID == _locationID);
            HiddenFieldLocationID.Value = _locationID.ToString();
            ddlCountryLocation.SelectedValue = getLocation.CountryID.ToString();
            txtJobLocationName.Text = getLocation.Location;
            lnkBtnSaveLocation.Visible = false;
            lnkBtnClearLocation.Visible = true;
            lnkBtnUpdateLocation.Enabled = true;
            lbAddLocationHeader.Text = "Edit Location";
            ImageAddLocationHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            ModalPopupExtenderAddLocation.Show();
            return;
        }
    }
}