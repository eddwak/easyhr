﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Recruitment_Module
{
    public partial class Applicants : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayAdvertisedJobsAndNotClosed();//diplay advertised jobs

                //LoadSelectedApplicantDetails(_hrClass.ReturnGuid("1009926d-2c34-e411-bb78-42d0948f3a4c"));//load applicant's details
                //load applicant additional controls
                //LoadApplicantAdditionalControls();
            }
        }
        //display job advertised and not yet closed
        private void DisplayAdvertisedJobsAndNotClosed()
        {
            try
            {
                object _display;
                _display = db.PROC_JobsAdvertisedAndNotClosed();
                gvJobsAdvertised.DataSourceID = null;
                gvJobsAdvertised.DataSource = _display;
                gvJobsAdvertised.DataBind();
                return;
            }
            catch { }
        }
        //method for searching for an advertised job and not yet closed
        private void SearchForAJobAdvertisedAndNotClosed()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();

                //check if the staff selected contains : character
                if (_searchText.Contains(":"))
                {
                    _searchText = _searchText.Trim().Replace(":", "");
                }
                _searchDetails = db.PROC_JobsAdvertisedAndNotClosed().Where(p => (p.JobReference.ToLower() + p.JobType.ToLower() + p.JobCategory.ToLower() + p.JobTitle.ToLower()).Contains(_searchText)).OrderByDescending(p => p.JobReference).ToList();
                gvJobsAdvertised.DataSourceID = null;
                gvJobsAdvertised.DataSource = _searchDetails;
                Session["gvJobsAdvertisedData"] = _searchDetails;
                gvJobsAdvertised.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelShortlistApplicants);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForAJobAdvertisedAndNotClosed();
            return;
        }
        //check if there is a job record that is selected before pulling job applications from the Adept Systems Job Site
        protected void LinkButtonPullJobApplicants_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobsAdvertised))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the job that you want to pull applications sent via the Adept Systems jobsite!", this, PanelShortlistApplicants);
                    return;
                }
                else
                {
                    //int x = 0;
                    //foreach (GridViewRow _grv in gvJobsAdvertised.Rows)
                    //{
                    //    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    //    if (_cb.Checked) ++x;
                    //}
                    //if (x > 1)
                    //{
                    //    _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one job description!. Select only one job description to pull applicantion sent!", this, PanelShortlistApplicants);
                    //    return;
                    //}
                    foreach (GridViewRow _grv in gvJobsAdvertised.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenField1");
                            //string _confirmationMessage = "Are you sure you want to to push the selected job with the following details for advertisement at the Adept Systems Jobsite?";

                            //retrive applications sent via adept systems hobsite
                            //create method for retrieving the job applications
                            _hrClass.LoadHRManagerMessageBox(2, "Available applications for the selected job(s) have been retrieved!", this, PanelShortlistApplicants);


                            //if (_confirmationMessage != "")//check if there is a message i.e the job is not already advertised
                            //    _hrClass.LoadHRManagerConfirmationMessageBox(ucPushJobForAdvertisement, _confirmationMessage);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while retrieving information for job application! " + ex.Message.ToString(), this, PanelShortlistApplicants);
                return;
            }
        }
        //
        //check if there is select job so as to view the applicants
        protected void LinkButtonViewJobApplicants_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvJobsAdvertised))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select the job that you want to view the applications received!", this, PanelShortlistApplicants);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvJobsAdvertised.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one job description!. Select only one job description to view the applications against the job!", this, PanelShortlistApplicants);
                        return;
                    }
                    foreach (GridViewRow _grv in gvJobsAdvertised.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");

                        if (_cb.Checked)
                        {
                            HiddenField _HFJobID = (HiddenField)_grv.FindControl("HiddenField1");
                            int _jobID = Convert.ToInt32(_HFJobID.Value);
                            HiddenFieldJobID.Value = _jobID.ToString();
                            //load job applications received against the job
                            LoadApplicationsReceivedPerJob(_jobID);
                            ModalPopupExtenderJobApplicationReceived.Show();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while retrieving information for job application! " + ex.Message.ToString(), this, PanelShortlistApplicants);
                return;
            }
        }
        //load application received against a job
        private void LoadApplicationsReceivedPerJob(int _jobID)
        {
            //get the job
            var getJob = db.PROC_JobApplicationPerJob(_jobID).FirstOrDefault();
            lbJobApplicationReceivedHeader.Text = "Applications Received for Job Reference: " + getJob.JobReference + "; Job Title: " + getJob.JobTitle;
            object _display;
            _display = db.PROC_JobApplicationPerJob(_jobID);

            gvJobApplicationReceived.DataSourceID = null;
            gvJobApplicationReceived.DataSource = _display;
            gvJobApplicationReceived.DataBind();

            // _hrClass.LoadHRManagerMessageBox(1, "Here wer should display jobs received against a job.", this, PanelShortlistApplicants);
            return;
        }

        //load job applicant's cv when the viecz button is clicked against a row record
        protected void gvJobApplicationReceived_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ModalPopupExtenderJobApplicationReceived.Show();
            try
            {
                if (e.CommandName.CompareTo("ViewCV") == 0)
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    GridViewRow Rowselected = ((GridView)e.CommandSource).Rows[ID];
                    GridView gdv = ((GridView)e.CommandSource);
                    HiddenField _HFJobApplicationID = (HiddenField)gvJobApplicationReceived.Rows[ID].FindControl("HiddenField2");
                    HiddenFieldJobApplicationID.Value = _HFJobApplicationID.Value;//get the job applicant id
                    HiddenField _HFApplicantID = (HiddenField)gvJobApplicationReceived.Rows[ID].FindControl("HiddenField1");
                    Guid _applicantID = _hrClass.ReturnGuid(_HFApplicantID.Value);
                    if (e.CommandName.CompareTo("ViewCV") == 0)//check if were are view the applicant's cv
                    {
                        LoadSelectedApplicantDetails(_applicantID);
                        return;
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        //load selected applicant details
        private void LoadSelectedApplicantDetails(Guid _applicantID)
        {
            //get the job 

            LoadJobApplicantBasicInformation(_applicantID);//load applicant's basic information
            DisplayApplicantEmploymentHistory(_applicantID);//load applicant's employement history
            DisplayApplicantEducationBackground(_applicantID);//load applicant's education background
            ModalPopupExtenderJobApplicationReceived.Hide();
            PanelJobList.Visible = false;
            PanelApplicantDetails.Visible = true;
            return;
        }
        //load applicant basic information
        private void LoadJobApplicantBasicInformation(Guid _applicantID)
        {
            //get the job
            int _jobApplicationID = Convert.ToInt32(HiddenFieldJobApplicationID.Value);
            var getJobApplication = db.PROC_JobApplicationPerJob(Convert.ToInt32(HiddenFieldJobID.Value)).Single(p => p.JobApplicationID == _jobApplicationID);
            lbApplicantDetailsHeader.Text = "Applicant Details for Job Reference: " + getJobApplication.JobReference + "; Job Title: " + getJobApplication.JobTitle;
            txtApplicationDate.Text = _hrClass.ShortDateDayStart(getJobApplication.ApplicationDate.ToString()) + " at " + _hrClass.ReturnLongTime(getJobApplication.ApplicationDate.ToString());
            ///
            //get applicant's basic information
            var getBasicInformation = db.PROC_JobApplicantBasicInfo(_applicantID).FirstOrDefault();
            HiddenFieldApplicantID.Value = _applicantID.ToString();
            txtApplicantReference.Text = getBasicInformation.ApplicantReference;
            //txtJobReference.Text = getBasicInformation.JobReference;
            txtApplicantTitle.Text = getBasicInformation.Title;
            txtApplicantName.Text = getBasicInformation.ApplicantName;
            txtIDNumber.Text = getBasicInformation.IDNumber;
            txtPassportNumber.Text = getBasicInformation.PassportNumber;
            if (getBasicInformation.DateOfBirth != null)
                txtDateOfBirth.Text = _hrClass.ShortDateDayStart(getBasicInformation.DateOfBirth.Value);
            else txtDateOfBirth.Text = "";
            txtGender.Text = getBasicInformation.Gender;
            txtPrimaryEmailAddress.Text = getBasicInformation.EmailAddress;
            txtAlternativeEmailAddress.Text = getBasicInformation.AlternativeEmailAddress;
            txtCountryOfResidence.Text = getBasicInformation.Country;
            txtCityOrTown.Text = getBasicInformation.City;
            txtFirstCitizenship.Text = getBasicInformation.FirstNationality;
            txtSecondCitizenship.Text = getBasicInformation.SecondNationality;
            txtPhoneNumber1.Text = getBasicInformation.PhoneNo;
            txtPhoneNumber2.Text = getBasicInformation.AlternativePhoneNo;
            txtPostalCode.Text = getBasicInformation.PostalCode;
            txtPostalAddress.Text = getBasicInformation.PostalAddress;
            txtPlaceOfResidence.Text = getBasicInformation.Residence;
            //check if the applicant's job application is already shortlisted
            if (getJobApplication.IsShortlisted == "Yes")
            {
                lnkBtnShortlistApplicantAndGoBack.Text = "De-Shortlist Applicant & Go Back";
                lnkBtnShortlistApplicant.Text = "De-Shortlist Applicant";
            }
            else
            {
                lnkBtnShortlistApplicantAndGoBack.Text = "Shortlist Applicant & Go Back";
                lnkBtnShortlistApplicant.Text = "Shortlist Applicant";
            }
            return;
        }

        //display applicant employment details
        private void DisplayApplicantEmploymentHistory(Guid _applicantID)
        {
            try
            {
                object _display;
                _display = db.PROC_ApplicantEmployment(_applicantID);
                gvApplicantEmploymentHistory.DataSourceID = null;
                gvApplicantEmploymentHistory.DataSource = _display;
                gvApplicantEmploymentHistory.DataBind();
                return;
            }
            catch { }
        }
        //display applicant's education history details
        private void DisplayApplicantEducationBackground(Guid _applicantID)
        {
            try
            {
                object _display;
                _display = db.PROC_ApplicantEducation(_applicantID);
                gvApplicantEducationBackground.DataSourceID = null;
                gvApplicantEducationBackground.DataSource = _display;
                gvApplicantEducationBackground.DataBind();
                return;
            }
            catch { }
        }
        //method for going back whewn accessing th e applicant cv
        private void GoBackFromApplicantCV()
        {
            ModalPopupExtenderJobApplicationReceived.Show();
            PanelJobList.Visible = true;
            PanelApplicantDetails.Visible = false;
            return;
        }
        //close applicant cv details
        protected void ImageButtonCloseApplicantDetails_Click(object sender, EventArgs e)
        {
            GoBackFromApplicantCV();
            return;
        }
        //shortlist the applicant
        private void ShortListApplicant()
        {
            try
            {
                int _jobApplicationID = Convert.ToInt32(HiddenFieldJobApplicationID.Value);
                JobApplication shortlistApplicant = db.JobApplications.Single(p => p.JobApplicationID == _jobApplicationID);
                if (shortlistApplicant.IsShortlisted == false)//cheeck if shortlisted
                {
                    shortlistApplicant.IsShortlisted = true;
                    db.SubmitChanges();
                    lnkBtnShortlistApplicantAndGoBack.Text = "De-Shortlist Applicant & Go Back";
                    lnkBtnShortlistApplicant.Text = "De-Shortlist Applicant";
                    _hrClass.LoadHRManagerMessageBox(2, "The applicant has been shortlisted successfully.", this, PanelShortlistApplicants);
                }
                else//de-shortlist
                {
                    shortlistApplicant.IsShortlisted = false;
                    db.SubmitChanges();
                    lnkBtnShortlistApplicantAndGoBack.Text = "Shortlist Applicant & Go Back";
                    lnkBtnShortlistApplicant.Text = "Shortlist Applicant";
                    _hrClass.LoadHRManagerMessageBox(2, "The applicant has been de-shortlisted successfully.", this, PanelShortlistApplicants);
                }
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "A system error occured. " + ex.Message.ToString(), this, PanelShortlistApplicants);
                return;
            }
        }
        //only shortlist the applicant
        protected void lnkBtnShortlistApplicant_Click(object sender, EventArgs e)
        {
            ShortListApplicant();
            LoadApplicationsReceivedPerJob(Convert.ToInt32(HiddenFieldJobID.Value));//refersh received job applications

            return;
        }
        //shortlist the applicant and go back
        protected void lnkBtnShortlistApplicantAndGoBack_Click(object sender, EventArgs e)
        {
            ShortListApplicant();//shortlist the candidate
            LoadApplicationsReceivedPerJob(Convert.ToInt32(HiddenFieldJobID.Value));//refersh received job applications

            ModalPopupExtenderJobApplicationReceived.Show();
            GoBackFromApplicantCV();//go back applicants lit
            return;
        }













        ////display applicant additional controls allowed
        //private void LoadApplicantAdditionalControls()
        //{
        //    XDocument xmlDoc = XDocument.Load(Server.MapPath("~/xml_Files/Applicant_AdditionalInformation.xml"));//read the document
        //    var applicantAdditionalControls = from r in xmlDoc.Descendants("control")//get the required details
        //                                      select new
        //                                      {
        //                                          ControlID = r.Element("controlid").Value.Trim(),
        //                                          ControlName = r.Element("controlname").Value.Trim(),
        //                                          IsNumeric = r.Element("isnumeric").Value.Trim(),
        //                                          Active = r.Element("active").Value.Trim(),
        //                                      };
        //    foreach (var _control in applicantAdditionalControls)
        //    {
        //        if (_control.Active == "Yes")
        //        {
        //            //get text box associated with the control
        //            TextBox _textbox = (TextBox)PanelApplicantAdditionalFields.FindControl(_control.ControlID);
        //            _textbox.Visible = true;
        //            //get label associated with the control
        //            Label _label = (Label)PanelApplicantAdditionalFields.FindControl(_control.ControlID.Replace("txt", "lb"));
        //            _label.Text = _control.ControlName + ":";
        //            _label.Visible = true;
        //            //get hiddenfield associated with the control to set id it is numeric or not
        //            HiddenField _hiddenField = (HiddenField)PanelApplicantAdditionalFields.FindControl(_control.ControlID.Replace("txt", "HiddenField"));
        //            _hiddenField.Value = _control.IsNumeric;
        //        }
        //    }
        //    return;

        //}
    }
}