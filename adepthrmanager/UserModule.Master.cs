﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager
{
    public partial class UserModule : System.Web.UI.MasterPage
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Init(object sender, EventArgs e)
        {
            _hrClass.ChangeModuleMainMenuActiveLinkCSS(this.Page, PanelUserModuleMainMenu);//change the css of the active menu
            ////session for testing
            ////Session["LoggedStaffID"] = "25af7b04-076f-e411-8c43-68a3c4aec686";//dickson
            //Session["LoggedStaffID"] = "4fd8425c-1297-e411-b314-68a3c4aec686";//k
            //Session["LoggedStaffPostedAtID"] = 2001;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetLoggedUser();//get looged in user
                LoadStaffInfo();//load header details
            }
        }
        private void GetLoggedUser()
        {
            try
            {
                var _staffSession = HttpContext.Current.Session["LoggedStaffID"];
                //check if the staff is logged in
                if (_staffSession == null)
                {
                    //HttpContext.Current.Response.Redirect("~/Login.aspx");
                    HttpContext.Current.Response.Redirect("~/default.aspx");
                    return;
                }
                else
                {
                    Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                    string _staffName = _hrClass.ReturnFirstName(Session["LoggedStaffName"].ToString());
                    lbLoggedUserName.InnerText = "Welcome, " + _staffName;
                    //load staff photo
                    //load staff image 
                    _hrClass.LoadStaffPhoto(_staffID, ImageStaffPhoto);
                    return;
                }
            }
            catch { }
        }
        //chage the css of the main menu link bassed on the link beiong viewed
        private void ChangeMainMenuActiveLinkCSS()
        {
            foreach (Control _control in PanelUserModuleMainMenu.Controls)
            {
                // Check and see if it's a linkbutton
                if ((_control.GetType() == typeof(LinkButton)))
                {
                    LinkButton _menulink = ((LinkButton)(_control));
                    if (Page.AppRelativeVirtualPath.ToLower() == _menulink.PostBackUrl.ToString().ToLower())
                    {
                        _menulink.CssClass = "active-module-menu";

                        txtEmployeeName.Text = Page.AppRelativeVirtualPath;
                        txtStaffReference.Text = _menulink.PostBackUrl.ToString();
                        break;
                    }
                    else { }
                }
            }
        }
        //load staff header details
        private void LoadStaffInfo()
        {
            try
            {
                Guid _staffID = _hrClass.ReturnGuid(Session["LoggedStaffID"].ToString());
                PROC_StaffPersonalInformationResult getStaff = db.PROC_StaffPersonalInformation(_staffID).FirstOrDefault();
                txtStaffReference.Text = getStaff.StaffRef;
                txtEmployeeName.Text = getStaff.StaffName;
                _hrClass.DisableEntryControls(PanelUserModuleHeader);
            }
            catch { }
        }
        //clear session
        public void ClearSessions()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session["LoggedUserID"] = null;
            HttpContext.Current.Session["LoggedStaffID"] = null;
            HttpContext.Current.Session["LoggedStaffName"] = null;
            HttpContext.Current.Response.Redirect("~/default.aspx");
        }
        //sign out logged user
        public void StaffLogOut()
        {
            // ClearSessions(); return;
            try
            {
                string _pageURL = Page.AppRelativeVirtualPath;
                int _userLogId = Convert.ToInt32(Session["LoggedUserLogID"]);
                short _pageID = 0;
                //get page id
                _pageID = db.AppPages.Single(p => p.apppages_Url.ToString().ToLower().Trim() == _pageURL.ToLower().Trim()).apppages_PageID;
                _hrClass.UpdateUserLog(_userLogId, _pageID);
                ClearSessions();
                return;
            }
            catch
            {
                ClearSessions(); return;
            }
        }
        protected void lnkBtnSignOut_Click(object sender, EventArgs e)
        {
            StaffLogOut();
            return;
        }
    }
}