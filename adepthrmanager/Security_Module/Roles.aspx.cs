﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Security_Module
{
    public partial class Roles : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayRoles();//load roles
            }
            //add confirm record event 
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }
        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddRoleControls();
            ModalPopupExtenderAddRole.Show();
            return;
        }
        //clear add role controls
        private void ClearAddRoleControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddRole);
            HiddenFieldRoleID.Value = "";
            lnkBtnSaveRole.Visible = true;
            lnkBtnUpdateRole.Enabled = false;
            lnkBtnUpdateRole.Visible = true;
            lnkBtnClearRole.Visible = true;
            lbAddRoleHeader.Text = "Add New Role";
            ImageAddRoleHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
            lbError.Text = "";
            lbInfo.Text = "";
        }
        //validate add roles controls
        private string ValidateAddRoleControls()
        {
            if (txtRoleName.Text.Trim() == "")
            {
                txtRoleName.Focus();
                return "Enter role name!";
            }
            else return "";
        }
        //save new role name
        private void SaveNewRole()
        {
            ModalPopupExtenderAddRole.Show();
            try
            {
                string _error = ValidateAddRoleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    //save the new role details
                    const int _listingMasterID = 6000;//Master ID 6000 for roles
                    const string _listingType = "role";
                    string _roleName = "", _roleDescription = "";
                    _roleName = txtRoleName.Text.Trim();
                    _roleDescription = txtRoleDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _roleName))//check if the role name exists
                    {
                        lbError.Text = "Save failed, the role name you are trying to add already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        //save the new role
                        _hrClass.AddNewListingItem(_listingMasterID, _listingType, _roleName, _roleDescription, null, null, null, null, null, null, null, HiddenFieldRoleID);
                        DisplayRoles();
                        lbError.Text = "";
                        lbInfo.Text = "New role has been successfully saved.";
                        lnkBtnUpdateRole.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Save Failed. " + ex.Message.ToString() + ". Please try again !";
                return;
            }
        }
        //METHOD FOR UPDATING 
        private void UpdateRole()
        {
            ModalPopupExtenderAddRole.Show();
            try
            {
                string _error = ValidateAddRoleControls();
                if (_error != "")
                {
                    lbError.Text = _error;
                    return;
                }
                else
                {
                    int _roleID = Convert.ToInt32(HiddenFieldRoleID.Value);
                    const int _listingMasterID = 6000;//Master ID 6000 for role
                    const string _listingType = "role";
                    string _roleName = "", _roleDescription = "";
                    _roleName = txtRoleName.Text.Trim();
                    _roleDescription = txtRoleDescription.Text.Trim();
                    if (_hrClass.DoesListingItemExist(_listingMasterID, _roleID, _roleName))//check if the role already exists
                    {
                        lbError.Text = "Save failed, the role you are trying to update with already exists!";
                        lbInfo.Text = "";
                    }
                    else
                    {
                        _hrClass.UpdateListingItem(_listingMasterID, _roleID, _listingType, _roleName, _roleDescription, null, null, null, null, null, null, null);
                        DisplayRoles();
                        lbError.Text = "";
                        lbInfo.Text = "Role has been successfully updated.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lbError.Text = "Update failed. " + ex.Message.ToString() + ". Please try again!";
                return;
            }
        }
        protected void lnkBtnSaveRole_Click(object sender, EventArgs e)
        {
            SaveNewRole();
            return;
        }

        protected void lnkBtnUpdateRole_Click(object sender, EventArgs e)
        {
            UpdateRole();
            return;
        }
        //clear entry controls
        protected void lnkBtnClearRole_Click(object sender, EventArgs e)
        {
            ClearAddRoleControls();
            ModalPopupExtenderAddRole.Show();
            return;
        }
        //display roles
        private void DisplayRoles()
        {
            object _display;
            _display = db.Roles_Views.Select(p => p).OrderBy(p => p.RoleName);
            gvRoles.DataSourceID = null;
            gvRoles.DataSource = _display;
            gvRoles.DataBind();
            return;
        }
        // populate the edit role controls
        private void LoadEditRoleControls(int _roleID)
        {
            ClearAddRoleControls();
            Roles_View getRole = db.Roles_Views.Single(p => p.RoleID == _roleID);
            HiddenFieldRoleID.Value = _roleID.ToString();
            txtRoleName.Text = getRole.RoleName;
            txtRoleDescription.Text = getRole.RoleDescription;
            lnkBtnSaveRole.Visible = false;
            lnkBtnClearRole.Visible = true;
            lnkBtnUpdateRole.Enabled = true;
            lbAddRoleHeader.Text = "Edit Role";
            ImageAddRoleHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
            lbError.Text = "";
            lbInfo.Text = "";
            ModalPopupExtenderAddRole.Show();
            return;
        }
        //check if a record is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvRoles))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to edit!", this,PanelRoles);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvRoles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record. Select only one record to edit at a time!", this, PanelRoles);
                        return;
                    }
                    foreach (GridViewRow _grv in gvRoles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFRoleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit role
                            LoadEditRoleControls(Convert.ToInt32(_HFRoleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading the record for edit! " + ex.Message.ToString(), this, PanelRoles);
            }
        }
        //check if a role record is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvRoles))//check if a record is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to view the details!", this, PanelRoles);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvRoles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one record!. Select only one record to view the details at a time !", this, PanelRoles);
                        return;
                    }
                    foreach (GridViewRow _grv in gvRoles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFRoleID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view role
                            LoadViewRoleControls(Convert.ToInt32(_HFRoleID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the record for view purpose ! " + ex.Message.ToString(), this, PanelRoles);
                return;
            }
        }
        // populate the view role
        private void LoadViewRoleControls(int _roleID)
        {
            ClearAddRoleControls();
            Roles_View getRole = db.Roles_Views.Single(p => p.RoleID == _roleID);
            HiddenFieldRoleID.Value = _roleID.ToString();
            txtRoleName.Text = getRole.RoleName;
            txtRoleDescription.Text = getRole.RoleDescription;
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddRole);
            lnkBtnSaveRole.Visible = false;
            lnkBtnClearRole.Visible = false;
            lnkBtnUpdateRole.Visible = false;
            lbAddRoleHeader.Text = "View Role";
            ImageAddRoleHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddRole.Show();
        }
        //load delete role
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvRoles))
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a record to delete!", this, PanelRoles);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvRoles.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected record?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected records?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelRoles);
                return;
            }
        }
        //delete crolerecord
        private void DeleteRoles()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvRoles.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFRoleID = (HiddenField)_grv.FindControl("HiddenField1");
                        int _roleID = Convert.ToInt32(_HFRoleID.Value);

                        ListingItem deleteRole = db.ListingItems.Single(p => p.ListingItemIID == _roleID);
                        //check if there is any nationality used

                        //do not delete --- to be done

                        //delete the role if there is no conflict
                        db.ListingItems.DeleteOnSubmit(deleteRole);
                        db.SubmitChanges();
                    }
                }
                //refresh records after the delete is done
                DisplayRoles();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected record has been deleted.", this, PanelRoles);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected records have been deleted.", this, PanelRoles);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelRoles);
                return;
            }
        }
        //delete the selected record(s)
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteRoles();//delete selected records(s)
            return;
        }
        ////load roles
        //private void DisplayRoles()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add(new DataColumn("RoleID", typeof(int)));
        //    dt.Columns.Add(new DataColumn("RoleName", typeof(string)));
        //    dt.Columns.Add(new DataColumn("RoleDescription", typeof(string)));
        //    DataRow dr = dt.NewRow();
        //    dr[0] = 1;
        //    dr[1] = "Administrator";
        //    dr[2] = "Manages application security";
        //    dt.Rows.Add(dr);

        //    DataRow dr2 = dt.NewRow();
        //    dr2[0] = 2;
        //    dr2[1] = "Manager";
        //    dr2[2] = "Oversees a department";
        //    dt.Rows.Add(dr2);

        //    DataRow dr3 = dt.NewRow();
        //    dr3[0] = 3;
        //    dr3[1] = "Staff";
        //    dr3[2] = "General user";
        //    dt.Rows.Add(dr3);

        //    gvRoles.DataSource = dt;
        //    gvRoles.DataBind();
        //}
    }
}