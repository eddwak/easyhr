﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Security_Module/SecurityModule.master"
    AutoEventWireup="true" CodeBehind="Roles.aspx.cs" Inherits="AdeptHRManager.Security_Module.Roles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SecurityModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelRoles" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelRoles" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new role"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit role details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View role details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete role" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvRoles" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No roles available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("RoleID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="RoleName" HeaderText="Role" />
                            <asp:BoundField DataField="RoleDescription" HeaderText="Role Description" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddRole" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddRole" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddRoleHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddRoleHeader" runat="server" Text="Add New Role"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddRole" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Role Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Role Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRoleName" runat="server"></asp:TextBox><span class="errorMessage"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Role Description:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRoleDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldRoleID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddRolePopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveRole" OnClick="lnkBtnSaveRole_Click" ToolTip="Save Roles"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateRole" OnClick="lnkBtnUpdateRole_Click" CausesValidation="false"
                                ToolTip="Update Roles" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearRole" OnClick="lnkBtnClearRole_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="ModalPopupExtenderAddRole" RepositionMode="None" X="270"
                    Y="80" TargetControlID="HiddenFieldAddRolePopup" PopupControlID="PanelAddRole"
                    CancelControlID="ImageButtonCloseAddRole" BackgroundCssClass="modalback" runat="server">
                </cc1:ModalPopupExtender>
                <cc1:DragPanelExtender ID="DragPanelExtenderAddRole" TargetControlID="PanelAddRole"
                    DragHandleID="PanelDragAddRole" runat="server">
                </cc1:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
