﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Security_Module
{
    public partial class Users : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _hrClass.GetStaffNames(ddlStaffName, "Staff Name");//populate staff nane dropdown
                _hrClass.GetListingItems(ddlUserRole, 6000, "User Role");//display job user roles(6000) available
                DisplayApplicationUsers();//load users
                LoadApplicationPages();//load application pages
            }
            //add delete record event to delete a user
            ucConfirm.lnkBtnYesConfirmationClick += new EventHandler(lnkBtnYesConfirmation_Click);
        }

        protected void LinkButtonNew_Click(object sender, EventArgs e)
        {
            ClearAddUserControls();
            ModalPopupExtenderAddUser.Show();
            return;
        }
        //clear add user controls
        private void ClearAddUserControls()
        {
            //clear entry controls
            _hrClass.ClearEntryControls(PanelAddUser);
            txtPassword.Attributes.Add("value", "");
            txtConfirmPassword.Attributes.Add("value", "");
            HiddenFieldUserID.Value = "";
            lnkBtnSaveUser.Visible = true;
            lnkBtnUpdateUser.Enabled = false;
            lnkBtnUpdateUser.Visible = true;
            lnkBtnClearUser.Visible = true;
            lbAddUserHeader.Text = "Add New User";
            ImageAddUserHeader.ImageUrl = "~/Images/icons/Medium/Create.png";
        }

        //validate add user controls
        private string ValidateAddUserControls()
        {
            if (ddlStaffName.SelectedIndex == 0)
                return "Select staff name!";
            else if (txtUserName.Text.Trim() == "")
                return "Enter user name!";
            else if (ddlUserRole.SelectedIndex == 0)
                return "Select user role !";
            else if (txtPassword.Text.Trim() == "")
                return "Enter user password !";
            else if (txtConfirmPassword.Text == "")
                return "Enter confirmation password !";
            else if (txtPassword.Text.Trim() != txtConfirmPassword.Text.Trim())
                return "Password and confirmation password entered do not match !";
            else if (rblUserStatus.SelectedIndex == -1)
                return "Select user status !";
            else return "";
        }
        //method for saving the user
        private void SaveApplicationUser()
        {
            ModalPopupExtenderAddUser.Show();
            try
            {
                string _error = ValidateAddUserControls();//check for errors
                Guid _staffID = Guid.Empty;
                string _username = txtUserName.Text.Trim(), _password = txtPassword.Text.Trim(), _confirmPassword = txtConfirmPassword.Text.Trim();
                txtPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                txtConfirmPassword.Attributes.Add("value", _confirmPassword);//maintain the password entered in case of a postback
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelUsers); return;
                }
                else if (_hrClass.IsValidPassword(txtPassword.Text.Trim()) == false)
                {
                    txtPassword.Focus();
                    _error = "Password must be 6 characters long with at least one numeric,one uppercase character and one special character!";
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelUsers); return;
                }
                else
                {
                    //get the selected staff id
                    _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                    if (db.Users.Any(p => p.StaffID == _staffID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! The selected staff is already added as a user!", this, PanelUsers); return;
                    }
                    else if (db.Users.Any(p => p.UserName.ToLower().Trim() == _username.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! Another user exists with the username entered.", this, PanelUsers); return;
                    }
                    else
                    {
                        //save the user
                        User newUser = new User();
                        newUser.StaffID = _staffID;
                        newUser.UserName = _username;
                        newUser.UserRoleID = Convert.ToInt32(ddlUserRole.SelectedValue);
                        newUser.Password = _hrClass.GenerateHashSaltedEnctryptedPwd(_password);
                        newUser.IsActive = Convert.ToBoolean(rblUserStatus.SelectedValue);
                        newUser.LoginAttempts = 0;
                        db.Users.InsertOnSubmit(newUser);
                        db.SubmitChanges();
                        HiddenFieldUserID.Value = newUser.UserID.ToString();
                        //display users
                        DisplayApplicationUsers();
                        _hrClass.LoadHRManagerMessageBox(2, "User details has been successfully saved !", this, PanelUsers);
                        lnkBtnUpdateUser.Enabled = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! " + ex.Message.ToString(), this, PanelUsers); return;
            }
        }
        //method for update user
        private void UpdateApplicationUser(Guid _userID)
        {
            ModalPopupExtenderAddUser.Show();
            try
            {
                string _error = ValidateAddUserControls();//check for errors
                Guid _staffID = Guid.Empty;
                string _username = txtUserName.Text.Trim(), _password = txtPassword.Text.Trim(), _confirmPassword = txtConfirmPassword.Text.Trim();
                if (_password != "")
                {
                    txtPassword.Attributes.Add("value", _password);//maintain the password entered in case of a postback
                    txtConfirmPassword.Attributes.Add("value", _confirmPassword);//maintain the password entered in case of a postback
                }
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelUsers); return;
                }
                else
                {
                    //get the selected staff id
                    _staffID = _hrClass.ReturnGuid(ddlStaffName.SelectedValue);
                    if (db.Users.Any(p => p.UserID != _userID && p.StaffID == _staffID))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! Another user account already set for the selected staff.", this, PanelUsers); return;
                    }
                    else if (db.Users.Any(p => p.UserID != _userID && p.UserName.ToLower().Trim() == _username.ToLower()))
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! Another user exists with the username entered.", this, PanelUsers); return;
                    }
                    else
                    {
                        User updateUser = db.Users.Single(p => p.UserID == _userID);
                        updateUser.StaffID = _staffID;
                        updateUser.UserName = _username;
                        updateUser.UserRoleID = Convert.ToInt32(ddlUserRole.SelectedValue);
                        //updateUser.Password =
                        updateUser.IsActive = Convert.ToBoolean(rblUserStatus.SelectedValue);
                        db.SubmitChanges();
                        //display application users
                        DisplayApplicationUsers();
                        _hrClass.LoadHRManagerMessageBox(2, "User details has been successfully updated !", this, PanelUsers);

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Save Failed ! " + ex.Message.ToString(), this, PanelUsers); return;
            }
        }
        //save application user
        protected void lnkBtnSaveUser_Click(object sender, EventArgs e)
        {
            SaveApplicationUser();
            return;
        }
        //update application user details
        protected void lnkBtnUpdateUser_Click(object sender, EventArgs e)
        {
            UpdateApplicationUser(_hrClass.ReturnGuid(HiddenFieldUserID.Value));
            return;
        }
        //clear entry controls
        protected void lnkBtnClearUser_Click(object sender, EventArgs e)
        {
            ClearAddUserControls();
            ModalPopupExtenderAddUser.Show();
            return;
        }
        //display application users
        private void DisplayApplicationUsers()
        {

            try
            {
                object _display;
                _display = db.Users_Views.Select(p => p).OrderBy(p => p.StaffName);
                gvUsers.DataSourceID = null;
                gvUsers.DataSource = _display;
                Session["gvUsersData"] = _display;
                gvUsers.DataBind();
                return;
            }
            catch { }
        }
        //page index changing
        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvUsers.PageIndex = e.NewPageIndex;
                Session["gvUsersPageIndex"] = e.NewPageIndex;
                gvUsers.DataSource = (object)Session["gvUsersData"];
                gvUsers.DataBind();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelUsers); return;
            }
        }
        protected void gvUsers_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (Session["gvUsersPageIndex"] == null)//check if page index is empty
                {
                    gvUsers.PageIndex = 0;
                    return;
                }
                else
                {
                    //get grid view's page index from the session
                    int pageIndex = Convert.ToInt32(Session["gvUsersPageIndex"]);
                    gvUsers.PageIndex = pageIndex;
                    return;
                }
            }
            catch { }
        }
        //check if a user is selected before edit details controls are loaded
        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if a user is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a user to edit !", this, PanelUsers);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one user !. Select only one user to edit at a time !", this, PanelUsers);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load edit user details
                            LoadEditUserControls(_hrClass.ReturnGuid(_HFUserID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An occured while loading user details for edit ! " + ex.Message.ToString(), this, PanelUsers);
            }
        }
        // populate the edit user controls
        private void LoadEditUserControls(Guid _userID)
        {

            ClearAddUserControls();
            User getUser = db.Users.Single(p => p.UserID == _userID);
            if (getUser.UserName.Trim().ToLower() == "admin")//check if were are trying to update server admin
            {
                _hrClass.LoadHRManagerMessageBox(1, "Super Administrator details cannot be edited !", this, PanelUsers);
                return;
            }
            else
            {
                HiddenFieldUserID.Value = _userID.ToString();
                ddlStaffName.SelectedValue = getUser.StaffID.ToString();
                txtUserName.Text = getUser.UserName;
                ddlUserRole.SelectedValue = getUser.UserRoleID.ToString();
                txtPassword.Attributes.Add("value", getUser.Password);
                txtConfirmPassword.Attributes.Add("value", getUser.Password);
                txtPassword.Enabled = false;
                txtConfirmPassword.Enabled = false;
                rblUserStatus.SelectedValue = getUser.IsActive.ToString();

                lnkBtnSaveUser.Visible = false;
                lnkBtnClearUser.Visible = true;
                lnkBtnUpdateUser.Enabled = true;
                lbAddUserHeader.Text = "Edit User Details";
                ImageAddUserHeader.ImageUrl = "~/Images/icons/Medium/modify.png";
                ModalPopupExtenderAddUser.Show();
                return;
            }
        }
        //check if a user is selected before viewing the details
        protected void LinkButtonView_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if a user is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a user to view !", this, PanelUsers);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one user !. Select only one user to view the user details at a time !", this, PanelUsers);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load view user details
                            LoadViewUserControls(_hrClass.ReturnGuid(_HFUserID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while loading the user details for view purpose ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        // populate the view user controls
        private void LoadViewUserControls(Guid _userID)
        {
            User getUser = db.Users.Single(p => p.UserID == _userID);
            HiddenFieldUserID.Value = _userID.ToString();
            ddlStaffName.SelectedValue = getUser.StaffID.ToString();
            txtUserName.Text = getUser.UserName;
            ddlUserRole.SelectedValue = getUser.UserRoleID.ToString();
            txtPassword.Attributes.Add("value", getUser.Password);
            txtConfirmPassword.Attributes.Add("value", getUser.Password);
            txtPassword.Enabled = false;
            txtConfirmPassword.Enabled = false;
            rblUserStatus.SelectedValue = getUser.IsActive.ToString();
            //disable entry controls
            _hrClass.DisableEntryControls(PanelAddUser);
            lnkBtnSaveUser.Visible = false;
            lnkBtnClearUser.Visible = false;
            lnkBtnUpdateUser.Visible = false;
            lbAddUserHeader.Text = "View User Details";
            ImageAddUserHeader.ImageUrl = "~/Images/icons/Medium/List.png";
            ModalPopupExtenderAddUser.Show();
        }
        //load delete application user details
        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if there is any user that is selected before delete is done
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select user to delete !", this, PanelUsers);
                    return;
                }
                else
                {
                    int _selectedRecordCount = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) _selectedRecordCount++;
                    }
                    string _deleteMessage = "";
                    if (_selectedRecordCount == 1)
                        _deleteMessage = "Are you sure that you want to delete the selected user?";
                    else if (_selectedRecordCount > 1)
                        _deleteMessage = "Are you sure that you want to delete the selected users?";
                    _hrClass.LoadHRManagerConfirmationMessageBox(ucConfirm, _deleteMessage);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //method for deleting an application user
        private void DeleteApplicationUser()
        {
            try
            {
                int _selectedRecordCount = 0;
                foreach (GridViewRow _grv in gvUsers.Rows)
                {
                    CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                    if (_cb.Checked) _selectedRecordCount++;
                    if (_cb.Checked)
                    {
                        HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                        Guid _userID = _hrClass.ReturnGuid(_HFUserID.Value);

                        User deleteUser = db.Users.Single(p => p.UserID == _userID);
                        if (db.Users.Single(p => p.UserID == _userID).UserName.Trim().ToLower() == "admin")
                        {
                            _hrClass.LoadHRManagerMessageBox(1, "The selected user cannot been deleted.", this, PanelUsers);
                            return;
                        }
                        //check if the user has any user rights assigned to him/her
                        if (db.UserAccessRights.Any(p => p.UserID == _userID))
                        {
                            //delete the user rights associated with the user
                            db.UserAccessRights.DeleteOnSubmit(db.UserAccessRights.Single(p => p.UserID == _userID));
                        }
                        ////check if the user is assigned to ant access pages
                        //if (db.UserPagesAccesses.Any(p => p.userpages_UserID == _userID))
                        //{
                        //    //delete the pages assigned to the user
                        //    foreach (UserPagesAccess deleteUserPageAccess in hrms_db.UserPagesAccesses.Where(p => p.userpages_UserID == _userID))
                        //    {
                        //        hrms_db.UserPagesAccesses.DeleteOnSubmit(deleteUserPageAccess);
                        //    }
                        //}
                        //do not delete --- to be done

                        //delete the user if there is no conflict
                        db.Users.DeleteOnSubmit(deleteUser);
                        db.SubmitChanges();
                    }
                }
                //refresh users after the delete is done
                DisplayApplicationUsers();
                if (_selectedRecordCount == 1)
                    _hrClass.LoadHRManagerMessageBox(2, "The selected user has been deleted.", this, PanelUsers);
                else if (_selectedRecordCount > 1)//
                    _hrClass.LoadHRManagerMessageBox(2, "The selected users have been deleted.", this, PanelUsers);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Delete Failed !" + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //delete the selected application user or users
        protected void lnkBtnYesConfirmation_Click(object sender, EventArgs e)
        {
            DeleteApplicationUser();
            return;
        }
        //check if a user is selected before reseting user password
        protected void LinkButtonResetUserPassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if an application user is selected before performing reset password
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a user to reset his or her password !", this, PanelUsers);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one user.<br>Select only one user to reset his/her password at a time !", this, PanelUsers);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                            //load reset user passord
                            LoadResetUserPasswordControls(_hrClass.ReturnGuid(_HFUserID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Load Failed ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //method for loading reset user password controls
        private void LoadResetUserPasswordControls(Guid _userID)
        {
            ClearResetUserPasswordControls();
            Users_View getUser = db.Users_Views.Single(p => p.UserID == _userID);
            HiddenFieldUserID.Value = _userID.ToString();
            lbResetUserPasswordHeader.Text = "Reset " + getUser.StaffName + "'s Password";
            ModalPopupExtenderResetUserPassword.Show();
            return;
        }
        //clear reset passsword controls
        public void ClearResetUserPasswordControls()
        {
            txtResetUserNewPassword.Text = "";
            txtResetUserConfirmationPassword.Text = "";
            lbResetPasswordError.Text = "";
            txtResetUserNewPassword.Attributes.Add("value", "");
            txtResetUserConfirmationPassword.Attributes.Add("value", "");
            return;
        }

        //reset selected user password
        public void ResetUserPassword(Guid _userID)
        {
            ModalPopupExtenderResetUserPassword.Show();
            try
            {
                string _newPassword = txtResetUserNewPassword.Text.Trim(), _confirmationPassword = txtResetUserConfirmationPassword.Text.Trim();
                string _encryptedNewPassword = _hrClass.GenerateHashSaltedEnctryptedPwd(_newPassword);
                //maintain the new password entered in case of a postback
                if (_newPassword != "")
                    txtResetUserNewPassword.Attributes.Add("value", _newPassword);
                //maintain the old password entered in case of a postback
                if (_confirmationPassword != "")
                    txtResetUserConfirmationPassword.Attributes.Add("value", _confirmationPassword);

                //validate change password controls
                if (_newPassword == "")
                {
                    lbResetPasswordError.Text = "Enter New Password!!";
                    txtResetUserNewPassword.Focus();
                    return;
                }
                else if (_hrClass.IsValidPassword(txtResetUserNewPassword.Text.Trim()) == false)
                {
                    //lbResetPasswordError.Text = "Password must be 6 characters long with at least one numeric,one uppercase character and one special character!";
                    lbResetPasswordError.Text = "";
                    txtResetUserNewPassword.Focus();
                    return;
                }
                else if (_confirmationPassword == "")
                {
                    lbResetPasswordError.Text = "Enter Confirmation Password!!";
                    txtResetUserConfirmationPassword.Focus();
                    return;
                }
                else if (_confirmationPassword != _newPassword)
                {
                    lbResetPasswordError.Text = "Confirmation password does not match with the new password entered!";
                    txtResetUserConfirmationPassword.Focus();
                    return;
                }
                else
                {
                    //update the user table if there no errors by changing the password
                    User _changePassword = db.Users.Single(p => p.UserID == _userID);
                    string _getPassword = _changePassword.Password;
                    _changePassword.Password = _encryptedNewPassword;
                    _changePassword.LoginAttempts = 0;//reset login attempts to zero
                    db.SubmitChanges();
                    lbResetPasswordError.Text = "The user password has been successfully changed !";
                    ModalPopupExtenderResetUserPassword.Hide();
                    _hrClass.LoadHRManagerMessageBox(2, "The user password has been successfully changed ! ", this, PanelUsers);
                    return;
                }
            }
            catch (Exception ex)
            {
                lbResetPasswordError.Text = "Password change failed ! " + ex.Message.ToString();
                return;
            }
        }
        //save user password changes
        protected void lnkBtnSavePasswordChanges_Click(object sender, EventArgs e)
        {
            ResetUserPassword(_hrClass.ReturnGuid(HiddenFieldUserID.Value)); return;
        }
        //check if there is a user that is selected before assigning access rights to him/her
        protected void LinkButtonAssignRights_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if there is a user that is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a user to assign access rights to !", this, PanelUsers);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one user. You can only select one user to assign access rights to at a time!", this, PanelUsers);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                            LoadAssignUserAccessRightToUserControls(_hrClass.ReturnGuid(_HFUserID.Value));
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Load assign access rights to the selected user failed ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //load assign user access rights to a user
        private void LoadAssignUserAccessRightToUserControls(Guid _userID)
        {
            HiddenFieldUserID.Value = _userID.ToString();
            string _fullName = db.Users_Views.Single(p => p.UserID == _userID).StaffName;
            if (db.UserAccessRights.Any(p => p.UserID == _userID))//check if the user has access rights assigned to him\her
            {
                //get user access right details
                UserAccessRight getUserAccessRight = db.UserAccessRights.Single(p => p.UserID == _userID);
                rblAccessRights.SelectedValue = getUserAccessRight.AccessRightValue;
                string _assignedModules = getUserAccessRight.AccessRightModules;
                cblApplicationModules.SelectedIndex = -1;//clear previous selected modules
                var _listModulues = _assignedModules.Split(',').Select(p => p).ToArray();//get array for the assigned modules
                foreach (ListItem _module in cblApplicationModules.Items)
                {
                    foreach (string _assignedModule in _listModulues)
                    {
                        if (_module.Value == _assignedModule)//check if the select the assigned modules
                            _module.Selected = true;
                    }
                }
                lbAddUserAccessRightHeader.Text = "Update Access Rights to " + _fullName;
                lnkBtnSaveAccessRight.Text = "Update Access Rights";
            }
            else//no saved access against the user
            {
                lbAddUserAccessRightHeader.Text = "Assign Access Rights to " + _fullName;
                rblAccessRights.SelectedIndex = -1;
                cblApplicationModules.SelectedIndex = -1;
                lnkBtnSaveAccessRight.Text = "Save Access Rights";
            }
            ModalPopupExtenderAddUserAccessRight.Show();
        }
        //validate assign access rights to the user
        private string ValidateAssignAccessRightsControls()
        {
            if (rblAccessRights.SelectedIndex == -1)
                return "Select access right to assign to the user !";
            else if (cblApplicationModules.SelectedIndex == -1)
                return "Select module to asssign to the user !";
            else return "";
        }
        //save user access right details
        private void SaveUserAccessRights(Guid _userID)
        {
            ModalPopupExtenderAddUserAccessRight.Show();
            try
            {
                string _error = ValidateAssignAccessRightsControls();//check for errors
                if (_error != "")
                {
                    _hrClass.LoadHRManagerMessageBox(1, _error, this, PanelUsers);
                    return;
                }
                else if (db.UserAccessRights.Any(p => p.UserID == _userID))//check if the user has any access rights and update the rights
                {
                    //get user access right id
                    int _userAccessRightID = Convert.ToInt32(db.UserAccessRights.Single(p => p.UserID == _userID).useraccess_RightID);
                    //update the user access right details
                    UserAccessRight updateUserAccessRight = db.UserAccessRights.Single(p => p.useraccess_RightID == _userAccessRightID);
                    updateUserAccessRight.AccessRightValue = rblAccessRights.SelectedValue;
                    updateUserAccessRight.AccessRightModules = GetSelectUserAccessModules();
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "User access rights have been successfully updated against the user. ", this, PanelUsers);
                    return;
                }
                else//create new user access rights
                {
                    UserAccessRight newAccessRight = new UserAccessRight();
                    newAccessRight.UserID = _userID;
                    newAccessRight.AccessRightValue = rblAccessRights.SelectedValue;
                    newAccessRight.AccessRightModules = GetSelectUserAccessModules();
                    db.UserAccessRights.InsertOnSubmit(newAccessRight);
                    db.SubmitChanges();
                    lnkBtnSaveAccessRight.Text = "Update Access Rights";
                    _hrClass.LoadHRManagerMessageBox(2, "User access rights have been successfully saved against the user. ", this, PanelUsers);
                    return;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "An error occured while saving the user access rights ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //get selected user modules selected in the check box list
        private string GetSelectUserAccessModules()
        {
            string _selectedModules = "";
            foreach (ListItem _module in cblApplicationModules.Items)
            {
                if (_module.Selected)
                    _selectedModules += _module.Value.ToString() + ",";
            }
            return _selectedModules;
        }
        protected void lnkBtnSaveAccessRight_Click(object sender, EventArgs e)
        {
            //save user access rights
            SaveUserAccessRights(_hrClass.ReturnGuid(HiddenFieldUserID.Value));
            return;
        }

        //Load application pages
        public void LoadApplicationPages()
        {
            string Mainxx = Request.ApplicationPath.ToString();
            string ss = "~/";
            string applicationPath = Page.MapPath(ss);
            Session["ApplicationPagesData"] = null;
            string[] fileDir = System.IO.Directory.GetDirectories(applicationPath);

            dt.Columns.Add(new DataColumn("Dir", typeof(string)));
            dt.Columns.Add(new DataColumn("Application Page URL", typeof(string)));
            dt.Columns.Add(new DataColumn("Application Page Link Name", typeof(string)));
            DataRow dr;
            int count = fileDir.Count();
            for (int i = 0; i < count; i++)
            {
                if (fileDir[i].Contains(".aspx") == false)
                {
                }
                else if (fileDir[i].Contains(".aspx") == true)
                {
                    dr = dt.NewRow();
                    dr[0] = fileDir[i].ToString();
                    dr[1] = "-";
                    dt.Rows.Add(dr);
                }
                string[] Allfiles = System.IO.Directory.GetFiles(fileDir[i].ToString());
                int countj = Allfiles.Count();
                for (int j = 0; j < countj; j++)
                {
                    if (Allfiles[j].ToString().EndsWith(".aspx"))
                    {
                        //remove pages in  Account and User_Module folders
                        //if (Allfiles[j].ToString().Contains("\\Account\\") == false && Allfiles[j].ToString().Contains("\\User_Module\\") == false)
                        if (Allfiles[j].ToString().Contains("\\Account\\") == false)
                        {
                            dr = dt.NewRow();
                            string Linkname = Allfiles[j].ToString().Substring(fileDir[i].Length + 1).Replace(".aspx", "");
                            dr[1] = "~/" + Allfiles[j].ToString().Replace(applicationPath, "").Replace("\\", "/");
                            dr[2] = Linkname;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }

            Session["ApplicationPagesData"] = dt;
            gvApplicationPages.DataSource = dt;
            gvApplicationPages.DataBind();
            return;
        }
        //save all the application pages
        private void SaveApplicationPages()
        {
            try
            {
                DataTable dt = (DataTable)Session["ApplicationPagesData"];
                int ApplicationPageCount = 1, _lastPageCount = 0;//initialize 0 to the last page count
                //auto generate page code for a an aplication page by geting the last page code
                try
                {
                    string _lastPageCode = db.AppPages.Single(p => p.apppages_PageID == Convert.ToInt16(db.AppPages.Max(m => m.apppages_PageID))).apppages_Code;//get last page code
                    _lastPageCount = Convert.ToInt32(_lastPageCode.Replace("HR_Page_", ""));//convert the last part of page as an integer
                }
                catch { }
                foreach (DataRow r in dt.Rows)
                {
                    if (r[1].ToString() != "-")
                    {
                        if (db.AppPages.Where(p => p.apppages_Url == r[1].ToString()).Count() == 0)
                        {
                            AppPage newApplicationPage = new AppPage();
                            newApplicationPage.apppages_Code = "HR_Page_" + (_lastPageCount + ApplicationPageCount);
                            newApplicationPage.apppages_Url = r[1].ToString();
                            newApplicationPage.apppages_LinkName = r[2].ToString().Replace("_", " ");
                            db.AppPages.InsertOnSubmit(newApplicationPage);
                            ApplicationPageCount++;
                        }
                    }
                }
                db.SubmitChanges();
                _hrClass.LoadHRManagerMessageBox(2, "HR Manager application pages have been successfully saved.", this, PanelUsers);
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //save the application pages
        protected void lnkBtnSaveAppPages_Click(object sender, EventArgs e)
        {
            SaveApplicationPages();
            return;
        }
        //check if there is a user who is selected before assigning acess pages to the user
        protected void LinkButtonAssignAppPages_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvUsers))//check if there is a user that is selected
                {
                    _hrClass.LoadHRManagerMessageBox(1, "Please select a user to assign access to application pages!", this, PanelUsers);
                    return;
                }
                else
                {
                    int x = 0;
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {

                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;
                    }
                    if (x > 1)
                    {
                        _hrClass.LoadHRManagerMessageBox(1, "You have selected more than one user!. You can only select one user to allow access to application pages at a time!", this, PanelUsers);
                        return;
                    }
                    foreach (GridViewRow _grv in gvUsers.Rows)
                    {
                        CheckBox _cb = (CheckBox)_grv.FindControl("CheckBox1");
                        if (_cb.Checked) ++x;

                        if (_cb.Checked)
                        {
                            HiddenField _HFUserID = (HiddenField)_grv.FindControl("HiddenField1");
                            HiddenFieldUserID.Value = _HFUserID.Value;
                            //display assigned and unassigned pages for the user selected
                            gvAccessPages.DataSource = DisplayUserAccessPages(_hrClass.ReturnGuid(_HFUserID.Value));
                            gvAccessPages.DataBind();
                            ModalPopupExtenderAssignAccessPages.Show();
                            string _fullName = db.Users_Views.Single(p => p.UserID == _hrClass.ReturnGuid(_HFUserID.Value)).StaffName;
                            labelAssignAccessPages.Text = "Allow Access to Application Pages to " + _fullName;
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Load assign access pages to the selected user failed ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //get access pages for the user by checking the access pages that have been assigned to the user
        protected DataTable DisplayUserAccessPages(Guid _userID)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("AccessPageName", typeof(string)));
            dt.Columns.Add(new DataColumn("AccessPageUrl", typeof(string)));
            dt.Columns.Add(new DataColumn("Checked", typeof(bool)));
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("No", typeof(string)));
            int sNo = 1;
            DataRow dr;
            foreach (AppPage _applicationPage in db.AppPages.Select(p => p).OrderBy(p => p.apppages_Url))
            {

                dr = dt.NewRow();
                dr[0] = _applicationPage.apppages_LinkName;
                dr[1] = _applicationPage.apppages_Url;
                dr[2] = false;
                dr[3] = _applicationPage.apppages_PageID;
                if (db.UserPagesAccesses.Where(p => p.UserID == _userID && p.apppages_PageID == _applicationPage.apppages_PageID).Count() > 0)
                {
                    dr[2] = true;
                }
                else if (db.UserPagesAccesses.Where(p => p.UserID == _userID && p.apppages_PageID == _applicationPage.apppages_PageID).Count() == 0)
                {
                    dr[2] = false;
                }
                dr[4] = sNo + ".";
                dt.Rows.Add(dr);
                sNo++;
            }
            return dt;
        }
        //save access pages to the user selected
        private void SaveUserAccessPages()
        {
            ModalPopupExtenderAssignAccessPages.Show();
            try
            {
                if (!_hrClass.isGridviewItemSelected(gvAccessPages))//check if there is a page that is selected to assign access to the user
                {
                    _hrClass.LoadHRManagerMessageBox(1, "There is no page selected!. Select atleast a page to allow user access !", this, PanelUsers);
                    return;
                }
                else
                {
                    Guid _userID = _hrClass.ReturnGuid(HiddenFieldUserID.Value);
                    string _userName = db.Users_Views.Single(p => p.UserID == _userID).StaffName;
                    //loop on al the pages in the grid view
                    for (int i = 0; i < gvAccessPages.Rows.Count; i++)
                    {
                        HiddenField _HFID = (HiddenField)gvAccessPages.Rows[i].FindControl("HFID");
                        CheckBox _cbAssign = (CheckBox)gvAccessPages.Rows[i].FindControl("CheckBox1");
                        short _applicationPageID = Convert.ToInt16(_HFID.Value); ;
                        bool _Checked = _cbAssign.Checked;
                        if (_Checked == true)
                        {
                            if (db.UserPagesAccesses.Where(p => p.apppages_PageID == _applicationPageID && p.UserID == _userID).Count() == 0)
                            {
                                //create new access for the user if the page is selected
                                UserPagesAccess newUserPageAccess = new UserPagesAccess();
                                newUserPageAccess.UserID = _userID;
                                newUserPageAccess.apppages_PageID = _applicationPageID;
                                db.UserPagesAccesses.InsertOnSubmit(newUserPageAccess);
                            }
                        }
                        else
                        {
                            if (db.UserPagesAccesses.Where(p => p.apppages_PageID == _applicationPageID && p.UserID == _userID).Count() == 1)
                            {
                                //delete access for the user if the page is not checked
                                UserPagesAccess deleteUserPageAcess = db.UserPagesAccesses.Single(p => p.apppages_PageID == _applicationPageID && p.UserID == _userID);
                                db.UserPagesAccesses.DeleteOnSubmit(deleteUserPageAcess);
                            }
                        }
                    }
                    db.SubmitChanges();
                    _hrClass.LoadHRManagerMessageBox(2, "The selected page / pages have been allowed access to " + _userName + ".", this, PanelUsers);
                    int count = gvAccessPages.Rows.Count;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Failed in allowing page access to the user ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        protected void lnkBtnSaveAccessPages_Click(object sender, EventArgs e)
        {
            SaveUserAccessPages();
            ModalPopupExtenderAssignAccessPages.Show();
            return;
        }
        protected void CheckBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            ModalPopupExtenderAssignAccessPages.Show();
            try
            {
                CheckBox _cbHeader = (CheckBox)gvAccessPages.HeaderRow.FindControl("CheckBoxSelectAll");
                foreach (GridViewRow row in gvAccessPages.Rows)
                {
                    CheckBox _rowCheckBox = (CheckBox)row.FindControl("CheckBox1");
                    if (_cbHeader.Checked == true)
                        _rowCheckBox.Checked = true;
                    else _rowCheckBox.Checked = false;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Select All Failed ! " + ex.Message.ToString(), this, PanelUsers);
                return;
            }
        }
        //method for searching for a user record by use of staff name
        private void SearchForAppUser()
        {
            try
            {
                object _searchDetails;
                string _searchText = txtSearch.Text.Trim().ToLower();
                _searchDetails = db.Users_Views.Where(p => p.StaffName.ToLower().Contains(_searchText)).OrderBy(p => p.StaffName);
                gvUsers.DataSourceID = null;
                gvUsers.DataSource = _searchDetails;
                Session["gvUsersData"] = _searchDetails;
                gvUsers.DataBind();
                txtSearch.Focus();
                return;
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, "Search failed. " + ex.Message.ToString() + ". Please try again.", this, PanelUsers);
                return;
            }
        }
        //search by the text entered 
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchForAppUser();
            return;
        }
    }
}