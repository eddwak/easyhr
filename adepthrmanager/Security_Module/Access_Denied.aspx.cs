﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Security_Module
{
    public partial class Access_Denied : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            //check user access 
            var _errorMessage = HttpContext.Current.Session["UserAccessDeniedMessage"];
            try
            {
                if (_errorMessage != null)//check if the error message exists
                {
                    string _errorAccessMessage = HttpContext.Current.Session["UserAccessDeniedMessage"].ToString();
                    _hrClass.LoadHRManagerMessageBox(1, _errorAccessMessage, this, PanelAccessDenied);
                    HttpContext.Current.Session["UserAccessDeniedMessage"] = null;
                }
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelAccessDenied);
                return;
            }
        }
    }
}