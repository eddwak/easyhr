﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdeptHRManager.Master" AutoEventWireup="true" CodeBehind="UploadDetails.aspx.cs" Inherits="AdeptHRManager.Test_Items.UploadDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <asp:UpdatePanel ID="UpdatePanelNewConceptNote" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelTesting" runat="server">
                <asp:Label ID="Label1" runat="server" CssClass="labelCss" Text="Upload EADB employees"></asp:Label>
                <br />
                <asp:Button ID="btnUploadStaffDetails" runat="server" CssClass="buttonNavigations"
                    Text="Upload Staff Details" OnClick="btnUploadStaffDetails_Click" />
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <asp:Image ID="ImageSaving" runat="server" ImageUrl="~/Images/background/loading.gif" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
