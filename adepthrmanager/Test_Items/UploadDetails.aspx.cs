﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.OleDb;
using System.Data.Linq;
using AdeptHRManager.HRManagerClasses;

namespace AdeptHRManager.Test_Items
{
    public partial class UploadDetails : System.Web.UI.Page
    {
        HRManagerClass _hrClass = new HRManagerClass();
        HRManagerDataContext db = new HRManagerDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //test for inserting imported data from excel to mssql concept note (CDI Projects)
        private void TestForUploadingStaffDetails()
        {
            //try
            //{
            //string filename = @"D:\\Workz\\V8\\Adept Work\\EADB\\Documents\\Employee _Records.xls";
            string filename = @"D:\\Workz\\V8\\Adept Work\\EADB\\Documents\\EADB_Staff_Records.xls";
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 8.0;";
            OleDbConnection excelConn = new OleDbConnection(strConn);
            excelConn.Open();
            //get all the sheet names in the excel file
            DataTable dtSheetNames = excelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //get the first sheet name of the excel file 
            string firstSheetName = dtSheetNames.Rows[0].Field<string>("TABLE_NAME");
            excelConn.Close();
            //select all the details from the first sheet of the excel file
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", strConn);
            DataSet myDataSet = new DataSet();
            dataAdapter.Fill(myDataSet, "ExcelInfo");
            DataTable dataTable = myDataSet.Tables["ExcelInfo"];
            Guid _defaultHRID = _hrClass.ReturnGuid("c8f451c1-b573-e411-8a52-68a3c4aec686");//id for super administrator

            int _departmentID = 0;

            ////get distinct department  and save the new ones
            //var queryDepartments = (from r in dataTable.AsEnumerable()
            //                        select new
            //                        {
            //                            DepartmentName = r.Field<string>("dept"),
            //                        }).Distinct();
            ////save the dept that do not exists
            //foreach (var v_Department in queryDepartments)
            //{
            //    if (v_Department.DepartmentName != null && (db.ListingItems.Any(p => p.LisitingDetailValue.ToLower().Replace(" ", "") == v_Department.DepartmentName.Trim().ToLower().Replace(" ", "")) == false))
            //    {
            //        string _departmentCode = v_Department.DepartmentName.Substring(0, 2);

            //        //check if the country already exists
            //        _hrClass.AddNewListingItem(3000, "Department", v_Department.DepartmentName, _departmentCode, null, null, null, null, null, null, _defaultHRID, HiddenField1);
            //    }
            //}
            //get distinct staff and save the new ones
            var queryStaffDetails = (from r in dataTable.AsEnumerable()
                                     select new
                                     {
                                         StaffRef = r.Field<string>("Staff No"),
                                         LastName = r.Field<string>("Last Name"),
                                         FirstName = r.Field<string>("First Name"),
                                         MiddleName = r.Field<string>("Middle Name"),
                                         JobTitle = r.Field<string>("Job Title"),
                                         Department = r.Field<string>("Department"),
                                         Unit = r.Field<string>("Unit"),
                                         JobGrade = r.Field<string>("Job Grade"),
                                         Supervisor = r.Field<string>("Supervisor"),
                                         ContractType = r.Field<string>("Contract type"),
                                         IsNational = r.Field<string>("local expat"),
                                         AppointmentDate = r.Field<DateTime>("Date of appointment"),
                                         //ContractStartDate = r.Field<DateTime>("Contract Start Date"),
                                         //ContractEndDate = r.Field<DateTime>("Contract end date"),
                                         Nationality = r.Field<string>("Nationality"),
                                         Gender = r.Field<string>("Gender"),
                                         Office = r.Field<string>("Office"),
                                         DOB = r.Field<DateTime>("Date of Birth"),
                                         IsProfessional = r.Field<string>("Professional"),

                                     }).Distinct();
            int _nationalityID = 1001;
            int _jobTitleID = 7001;
            int? _unitID = null;
            int _jobGradeID = 20001;
            int _contractTypeID = 21001;
            Boolean _isNational = true, _isProfessional = true;
            int _postingOfficeID = 2001;
            //save the staff that do not exists
            foreach (var v_Staff in queryStaffDetails)
            {
                //get the department id for the staaff
                //try
                //{
                _departmentID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 3000 && p.LisitingDetailValue.Trim() == v_Staff.Department.Trim()).ListingItemIID);
                //}
                //catch { }
                ////get the nationality id for the staff
                //try
                //{
                _nationalityID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 1000 && p.LisitingDetailValue1.Trim() == v_Staff.Nationality.Trim()).ListingItemIID);
                //}
                //catch { }
                ////get the job title id for the staff
                //try
                //{
                _jobTitleID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 7000 && p.LisitingDetailValue.Trim() == v_Staff.JobTitle.Trim()).ListingItemIID);
                //}
                //catch { }
                ////get the unit id for the staff
                //try
                //{
                if (v_Staff.Unit != null)
                    _unitID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 4000 && p.LisitingDetailValue.Trim() == v_Staff.Unit.Trim()).ListingItemIID);
                //}
                //catch { }
                ////get the unit id for the staff
                //try
                //{
                _jobGradeID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 20000 && p.LisitingDetailValue.Trim() == v_Staff.JobGrade.Trim()).ListingItemIID);
                //}
                //catch { }
                _contractTypeID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 21000 && p.LisitingDetailValue.Trim() == v_Staff.ContractType.Trim()).ListingItemIID);
                if (v_Staff.IsNational.Trim() != "National")
                    _isNational = false;

                if (v_Staff.IsProfessional.Trim() != "Professional")
                    _isProfessional = false;

                _postingOfficeID = Convert.ToInt32(db.ListingItems.Single(p => p.ListingMasterID == 2000 && p.LisitingDetailValue.Trim() == v_Staff.Office.Trim()).ListingItemIID);
                //save staff record
                StaffMaster newStaff = new StaffMaster();
                if (v_Staff.StaffRef.Trim() != null && (db.StaffMasters.Any(p => p.StaffRef.ToLower().Replace(" ", "") == v_Staff.StaffRef.Trim().ToLower().Replace(" ", "")) == false))
                {
                    //string _lastName = "", _firstName = "", _middleName = "";
                    //_lastName = Convert.ToString(v_Staff.LastName).Trim();
                    //_firstName = Convert.ToString(v_Staff.FirstName).Trim();
                    //if (v_Staff.MiddleName != null)
                    //    _middleName = Convert.ToString(v_Staff.MiddleName).Trim();
                    //newStaff.StaffRef = v_Staff.StaffRef.Trim();
                    //newStaff.StaffName = _lastName + " " + _firstName + " " + _middleName;
                    //newStaff.LastName = _lastName;
                    //newStaff.FirstName = _firstName;
                    //newStaff.MiddleName = _middleName;
                    //newStaff.JobTitleID = _jobTitleID;
                    //newStaff.DepartmentID = _departmentID;
                    //newStaff.UnitID = _unitID;
                    //newStaff.JobGradeID = _jobGradeID;
                    //newStaff.ContractTypeID = _contractTypeID;
                    //newStaff.IsNational = _isNational;
                    //newStaff.JoinDate = v_Staff.AppointmentDate.Date;
                    //newStaff.NationalityID = _nationalityID;
                    //newStaff.Gender = v_Staff.Gender.Trim();
                    //newStaff.PostedAtID = _postingOfficeID;

                    ////newStaff.BadgeNumber = v_Staff.BadgeNumber;
                    ////newStaff.PostedAtID = Convert.ToInt32(ddlPostedAt.SelectedValue);
                    ////if (ddlReportsTo.SelectedIndex != 0)
                    ////    newStaff.ReportingToStaffID = _hrClass.ReturnGuid(ddlReportsTo.SelectedValue);
                    ////newStaff.JoinDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    ////if (txtExitDate.Text.Trim() != "" && txtExitDate.Text.Trim() != "__/___/____")
                    ////    newStaff.ExitDate = Convert.ToDateTime(txtJoinDate.Text.Trim()).Date;
                    ////else newStaff.ExitDate = null;
                    //newStaff.DateOfBirth = Convert.ToDateTime(v_Staff.DOB).Date;
                    //newStaff.IsProfessional = _isProfessional;

                    //newStaff.IDNumber = "";
                    //newStaff.PinNumber = "";
                    //newStaff.NSSFNumber = "";
                    ////newStaff.NHIFNumber = txtNHIFNumber.Text.Trim();
                    //newStaff.PhoneNumber = "";
                    ////if (txtAlternativeMobileNumber.Text.Trim() != "")
                    ////    newStaff.AlternativePhoneNumber = txtAlternativeMobileNumber.Text.Trim();
                    ////else newStaff.AlternativePhoneNumber = null;
                    ////newStaff.EmailAddress = txtEmailAddress.Text.Trim();
                    ////if (txtAlternativeEmailAddress.Text.Trim() != "")
                    ////    newStaff.AlternativeEmailAddress = txtAlternativeEmailAddress.Text.Trim();
                    ////else newStaff.AlternativeEmailAddress = null;
                    ////newStaff.Address = txtAddress.Text.Trim();
                    ////if (txtSalary.Text.Trim() != "")
                    ////    newStaff.Salary = Convert.ToDecimal(txtSalary.Text.Trim());
                    ////newStaff.EmergencyContactPerson = txtEmergencyContactPerson.Text.Trim();
                    ////newStaff.EmergencyContactPhoneNo = txtEmergencyContactPersonPhoneNumber.Text.Trim();
                    ////newStaff.NextOfKinName = txtNextOfKin.Text.Trim();
                    ////newStaff.NextOfKinPhone = txtNextOfKinPhoneNumber.Text.Trim();
                    ////newStaff.NextOfKinAddress = txtNextOfKinAddress.Text.Trim();
                    ////newStaff.DisabilityDetails = txtDisabilityDetails.Text.Trim();
                    ////newStaff.Remarks = txtStaffRecordRemarks.Text.Trim();
                    //db.StaffMasters.InsertOnSubmit(newStaff);
                }
                else
                {
                    //update the staff
                    StaffMaster updateStaff = db.StaffMasters.Single(p => p.StaffRef == v_Staff.StaffRef.Trim());
                    updateStaff.JobGradeID = _jobGradeID;
                }
                db.SubmitChanges();
            }
            ////get distinct staff and save the new ones
            //var queryStaff = (from r in dataTable.AsEnumerable()
            //                  select new
            //                  {
            //                      StaffName = r.Field<string>("DoneBy"),
            //                  }).Distinct();
            ////save the staff that do not exists
            //foreach (var v_Staffs in queryStaff)
            //{
            //    //check if the Staff already exists
            //    //add a new staff
            //    Staff newStaff = new Staff();
            //    if (v_Staffs.StaffName != null && (db.Staffs.Any(p => p.FirstName.ToLower().Replace(" ", "") == v_Staffs.StaffName.Trim().ToLower().Replace(" ", "")) == false))
            //    {
            //        newStaff.FirstName = v_Staffs.StaffName.Trim();
            //        newStaff.Active = true;
            //        newStaff.Password = v_Staffs.StaffName.Trim().Replace(" ", "") + "pwd";
            //        newStaff.Username = v_Staffs.StaffName.Trim().Replace(" ", "");
            //        newStaff.Roles = "User";
            //        db.Staffs.InsertOnSubmit(newStaff);
            //    }
            //    db.SubmitChanges();
            //}

            ////get CDI projects to save into concept note
            ////var queryCDIProjects = dataTable.AsEnumerable();
            //var queryCDIProjects = from r in dataTable.AsEnumerable()
            //                       select new
            //                       {
            //                           SerialNumber = r.Field<string>("SerialNumber"),
            //                           ProposalName = r.Field<string>("ProposalName"),
            //                           GroupName = r.Field<string>("GroupName"),
            //                           PostalAddress = r.Field<string>("PostalAddress"),
            //                           CountyName = r.Field<string>("CountyName"),
            //                           DateReceived = r.Field<DateTime>("DateReceived"),
            //                           TimeReceived = r.Field<DateTime>("TimeReceived"),
            //                           Comments = r.Field<string>("Comments"),
            //                           DoneBy = r.Field<string>("DoneBy"),
            //                       };
            ////get program id
            //int _programID = 0;
            //try
            //{
            //    _programID = Convert.ToInt32(db.ProgramInformations.Single(p => p.ProgramAcronym == "CEF2").ProgramInformationID);
            //}
            //catch { }

            ////save the concept_applications
            //foreach (var v_ConceptApplications in queryCDIProjects)
            //{
            //    int _groupID = 0, _staffID = 0, _statusID = 2;
            //    //get the group id
            //    try
            //    {
            //        _groupID = Convert.ToInt32(db.Groups.Single(p => p.GroupName.Trim() == v_ConceptApplications.GroupName.Trim()).GroupID);
            //    }
            //    catch { }
            //    //get staff id
            //    try
            //    {
            //        _staffID = Convert.ToInt32(db.Staffs.Single(p => p.FirstName.Trim() == v_ConceptApplications.DoneBy.Trim()).StaffID);
            //    }
            //    catch { }
            //    //get the time received
            //    //TimeSpan _timeReceived=
            //    //check if the application already exists
            //    //add a new concept application
            //    Concept_Application newConceptApplication = new Concept_Application();
            //    if (v_ConceptApplications.SerialNumber != null && (db.Concept_Applications.Any(p => p.SerialNumberShort.ToLower().Replace(" ", "") == v_ConceptApplications.SerialNumber.Trim().ToLower().Replace(" ", "")) == false))
            //    {
            //        newConceptApplication.SerialNumberShort = v_ConceptApplications.SerialNumber.Trim();
            //        if (v_ConceptApplications.ProposalName == null)//check if the proposal name
            //            newConceptApplication.ProposalName = "";
            //        else
            //            newConceptApplication.ProposalName = v_ConceptApplications.ProposalName.Trim();
            //        newConceptApplication.GroupID = _groupID;
            //        newConceptApplication.ProgramID = _programID;
            //        newConceptApplication.DateReceived = Convert.ToDateTime(v_ConceptApplications.DateReceived);
            //        newConceptApplication.TimeReceived = v_ConceptApplications.TimeReceived.TimeOfDay;
            //        newConceptApplication.DateAcknowledged = Convert.ToDateTime("01/Jan/1900");
            //        newConceptApplication.ApplicationEntered = false;
            //        newConceptApplication.DeliveryMethod = "Hand Delivary";
            //        newConceptApplication.ConceptApplicationAcknowledgeStaffID = _staffID;
            //        newConceptApplication.StatusID = _statusID;
            //        if (v_ConceptApplications.Comments == null)//check if the comments are null
            //            newConceptApplication.comments = "";
            //        else
            //            newConceptApplication.comments = Convert.ToString(v_ConceptApplications.Comments).Trim();
            //        db.Concept_Applications.InsertOnSubmit(newConceptApplication);
            //    }
            //    db.SubmitChanges();
            //}
            //////get counties
            ////GridView gvExcelImport = new GridView();
            ////gvExcelImport.CssClass = "mGrid";
            ////gvExcelImport.DataSource = queryCDIProjects;
            ////gvExcelImport.DataBind();
            ////PanelTesting.Controls.Add(gvExcelImport);


            // LabelTest.Text = "Fleet consumption details have been imported";
            _hrClass.LoadHRManagerMessageBox(2, "Staff details have been successfully uploaded", this, PanelTesting);

            //}
            //catch (Exception ex)
            //{
            //    _cdtfClasses.LoadCDTFMessageBox(1, 1, ex.Message.ToString(), this, PanelTesting); 
            //    return;
            //}
        }

        //save job titles
        //test for inserting imported data from excel to mssql concept note (CDI Projects)
        private void TestForUploadingJobTitles()
        {
            try
            {
                //string filename = @"D:\\Workz\\V8\\Adept Work\\EADB\\Documents\\Employee _Records.xls";
                //string filename = @"D:\\Workz\\V8\\Adept Work\\EADB\\Documents\\EADB_Job_Titles.xls";
                string filename = @"D:\\Workz\\V8\\Adept Work\\EADB\\Documents\\EADB_Staff_Records.xls";
                string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 8.0;";
                OleDbConnection excelConn = new OleDbConnection(strConn);
                excelConn.Open();
                //get all the sheet names in the excel file
                DataTable dtSheetNames = excelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                //get the first sheet name of the excel file 
                string firstSheetName = dtSheetNames.Rows[0].Field<string>("TABLE_NAME");
                excelConn.Close();
                //select all the details from the first sheet of the excel file
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", strConn);
                DataSet myDataSet = new DataSet();
                dataAdapter.Fill(myDataSet, "ExcelInfo");
                DataTable dataTable = myDataSet.Tables["ExcelInfo"];
                Guid _defaultHRID = _hrClass.ReturnGuid("c8f451c1-b573-e411-8a52-68a3c4aec686");//id for super administrator

                int _jobTitleID = 0;

                //get distinct department  and save the new ones
                var queryJobTitles = (from r in dataTable.AsEnumerable()
                                      select new
                                      {
                                          JobTitle = r.Field<string>("Job Title"),
                                      }).Distinct();
                //save the dept that do not exists
                foreach (var v_JobTitle in queryJobTitles)
                {
                    if (v_JobTitle.JobTitle != null && (db.ListingItems.Any(p => p.LisitingDetailValue.ToLower().Replace(" ", "") == v_JobTitle.JobTitle.Trim().ToLower().Replace(" ", "")) == false))
                    {


                        //check if the calready exists
                        _hrClass.AddNewListingItem(7000, "Job Title", v_JobTitle.JobTitle, null, null, null, null, null, null, null, null, HiddenField1);
                    }
                }
                ////get distinct staff and save the new ones
                //var queryStaffDetails = (from r in dataTable.AsEnumerable()
                //                         select new
                //                         {
                //                             StaffRef = r.Field<string>("Employee No"),
                //                             StaffName = r.Field<string>("Name"),
                //                             Sex = r.Field<string>("Sex"),
                //                             DOB = r.Field<DateTime>("Birth Date"),
                //                             Nationality = r.Field<string>("Nationality"),
                //                             BadgeNumber = r.Field<string>("badgeno"),
                //                             Department = r.Field<string>("dept"),

                //                         }).Distinct();
                _hrClass.LoadHRManagerMessageBox(2, "Staff details have been successfully uploaded", this, PanelTesting);
            }
            catch (Exception ex)
            {
                _hrClass.LoadHRManagerMessageBox(1, ex.Message.ToString(), this, PanelTesting);
                return;
            }
        }
        protected void btnUploadStaffDetails_Click(object sender, EventArgs e)
        {
            TestForUploadingStaffDetails();
            //TestForUploadingJobTitles();
            // _cdtfClasses.LoadCDTFMessageBox(1, 1, "Here we goo..", this, PanelTesting);
            return;
        }
    }
}