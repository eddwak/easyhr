﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Employment_Terms.aspx.cs" Inherits="AdeptHRManager.User_Module.Employment_Terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelEmploymentTerms" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelEmploymentTerms" runat="server">
                <div class="panel-details">
                    <fieldset>
                        <legend>Employment Terms Details</legend>
                        <table width="100%">
                            <tr valign="top">
                                <td>
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsID" runat="server" />
                                    <asp:HiddenField ID="HiddenFieldEmploymentTermsPerksDetailID" runat="server" />
                                    Date of Join:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsDateOfJoin" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Date Until:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsDateUntil" Enabled="false" CssClass="textBoxDetails"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Type of Employment:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTypeOfEmployment" Enabled="false" CssClass="textBoxDetails" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Basic Pay:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsBasicPay" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Notice Period:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsNoticePeriod" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Liquidated Damage:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmploymentTermsLiquidatedDamages" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                                <tr>
                                    <td>
                                        Medical Scheme:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMedicalScheme" Enabled="false" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
