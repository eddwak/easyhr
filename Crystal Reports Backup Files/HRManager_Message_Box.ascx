﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRManager_Message_Box.ascx.cs"
    Inherits="AdeptHRManager.HRManagerClasses.HRManagerUserControls.HRManager_Message_Box" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="PanelHRManagerMessageBox" Style="display: none;" runat="server" Width="400px">
    <div class="popup-header">
        <asp:Panel ID="PanelDragHRManagerMessageBox" runat="server">
            <table>
                <tr>
                    <td style="width: 99%;">
                        <asp:Label ID="lbMessageBoxHeader" runat="server" Text="HR Manager Message"></asp:Label>
                    </td>
                    <td>
                        <asp:ImageButton ID="ImageCloseMessageBox" Style="cursor: pointer;" runat="server"
                            ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Remove.png" ToolTip="Close" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div class="popup-details">
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 10px;">
                    <asp:Image ID="ImageMessageBox" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Alert.png" />
                </td>
                <td>
                    <div style="max-height: 150px; overflow: auto;">
                        <asp:Label ID="lbMessageBoxMessage" CssClass="messageBoxLabel" runat="server" Text="?"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    <asp:Label ID="lbMessageBoxPopup" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div class="small-linkBtn">
            <asp:LinkButton ID="lnkBtnOK" ToolTip="OK" runat="server">OK</asp:LinkButton>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtenderHRManagerMessageBox" TargetControlID="lbMessageBoxPopup"
    PopupControlID="PanelHRManagerMessageBox" CancelControlID="ImageCloseMessageBox"
    OkControlID="lnkBtnOK" BackgroundCssClass="modalback" runat="server">
</asp:ModalPopupExtender>
<%--<asp:DragPanelExtender ID="DragPanelExtenderHRManagerMessageBox" TargetControlID="PanelHRManagerMessageBox"
    DragHandleID="PanelDragHRManagerMessageBox" runat="server">
</asp:DragPanelExtender>--%>
