﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login2.aspx.cs" Inherits="AdeptHRManager.Login2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="~/Styles/login2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" class="login-form" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanelLogin" runat="server">
        <ContentTemplate>
            <div class="login-form-header">
            </div>
            <div class="login-form-left">
                Login to access
                <div class="login-image">
                    <asp:Image ID="Image1" ImageUrl="~/images/background/lock.png" runat="server" />
                </div>
            </div>
            <div class="login-form-right">
                <asp:TabContainer ID="TabContainerLogin" CssClass="ajax-tab" runat="server" ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelUserLogin" runat="server" CssClass="ajax-tab" HeaderText="User Login">
                        <HeaderTemplate>
                            User Login
                        </HeaderTemplate>
                        <ContentTemplate>
                            <%--  <div class="login-form-header">
                            <h1>
                                User Login</h1>
                        </div>--%>
                            <p>
                                <%--Username--%></p>
                            <p>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                <asp:TextBoxWatermarkExtender ID="txtUserName_TextBoxWatermarkExtender" runat="server"
                                    Enabled="True" TargetControlID="txtUserName" WatermarkText="enter username" WatermarkCssClass="water-mark-text-extender">
                                </asp:TextBoxWatermarkExtender>
                            </p>
                            <%--  <p>
        Password</p>--%>
                            <p>
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                <asp:TextBoxWatermarkExtender ID="txtPassword_TextBoxWatermarkExtender" runat="server"
                                    Enabled="True" TargetControlID="txtPassword" WatermarkText="enter password" WatermarkCssClass="water-mark-text-extender">
                                </asp:TextBoxWatermarkExtender>
                            </p>
                            <p>
                                <asp:CheckBox ID="cbRememberMe" CssClass="remember-check" Text="Remember my login details"
                                    runat="server" />
                            </p>
                            <p>
                                <asp:Label ID="lbError" runat="server" CssClass="error-label" Text=""></asp:Label>
                                <asp:Label ID="lbLoginAttempts" runat="server" CssClass="login-attempts-label" Text=""></asp:Label>
                            </p>
                            <p>
                                <asp:LinkButton ID="lnkBtnLogin" OnClick="lnkBtnLogin_Click" CssClass="login-form-btn"
                                    ToolTip="Click to login" runat="server">Login</asp:LinkButton>
                            </p>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelAdminLogin" runat="server" CssClass="ajax-tab" HeaderText="HR/Admin Login">
                        <HeaderTemplate>
                            HR /Admin Login
                        </HeaderTemplate>
                        <ContentTemplate>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
