﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Interview_Appointments.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Interview_Appointments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="RecruitmentModuleHeadContent" runat="server">
    <script language="javascript" type="text/javascript">
        function RadioCheck(rb) {
            var gv = document.getElementById("<%=gvInterviewAppointments.ClientID%>");
            var rbs = gv.getElementsByTagName("input");

            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }    
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelInterviewAppointments" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelInterviewAppointments" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new interview appointment"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <%--    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit interview appointment details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>--%>
                    <asp:LinkButton ID="LinkButtonOverallInterviewComments" OnClick="LinkButtonOverallInterviewComments_Click"
                        ToolTip="Overall Interview comments" CausesValidation="false" runat="server">
                        <asp:Image ID="ImageOverallInterviewComments" runat="server" ImageAlign="AbsMiddle"
                            ImageUrl="~/Images/icons/Small/List.png" />
                        Overall Interview Comments</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete interview appointment" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="search-div">
                    Search:
                    <asp:TextBox ID="txtSearch" Width="550px" AutoPostBack="true" runat="server"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by job reference/type/category/title:"
                        WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                    <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetJobsAdvertisedAndNotClosed"
                        TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                        CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                        CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                    </asp:AutoCompleteExtender>
                    <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                        width: 543px !important;">
                    </div>
                    <%-- Appointment Date:
                    <asp:TextBox ID="txtSearchByAppointmentDate" OnTextChanged="txtSearchByAppointmentDate_TextChanged"
                        Width="180px" AutoPostBack="true" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="ImageButtonSearchByAppointmentDate" ToolTip="Pick appointment date"
                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                    <asp:CalendarExtender ID="txtSearchByAppointmentDate_CalendarExtender" runat="server"
                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtSearchByAppointmentDate"
                        PopupButtonID="ImageButtonSearchByAppointmentDate" PopupPosition="TopRight">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="txtSearchByAppointmentDate_MaskedEditExtender" runat="server"
                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtSearchByAppointmentDate"
                        UserDateFormat="DayMonthYear">
                    </asp:MaskedEditExtender>
                    <asp:MaskedEditValidator ID="txtSearchByAppointmentDate_MaskedEditValidator" runat="server"
                        ControlExtender="txtSearchByAppointmentDate_MaskedEditExtender" ControlToValidate="txtSearchByAppointmentDate"
                        CssClass="errorMessage" ErrorMessage="txtSearchByAppointmentDate_MaskedEditValidator"
                        InvalidValueMessage="<br>Invalid appointment date entered" Display="Dynamic">
                    </asp:MaskedEditValidator>--%>
                </div>
                <div class="panel-details">
                    <%--      <asp:GridView ID="gvInterviewAppointments" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                        EmptyDataText="No interview appointments available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("InterviewAppointmentID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ApplicantReference" HeaderText="Applicant" />
                            <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name" />
                            <asp:BoundField DataField="JobReference" HeaderText="Job Ref:" />
                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                            <asp:BoundField DataField="AppointmentDate" HeaderText="Appointment Date" DataFormatString="{0:dd/MMM/yyyy}" />
                            <asp:BoundField DataField="AppointmentTime" HeaderText="Appointment Time" />
                            <asp:BoundField DataField="IsInterviewConducted" HeaderText="Is Interview Conducted?" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>--%>
                    <asp:GridView ID="gvInterviewAppointments" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" DataKeyNames="JobID" PageSize="6"
                        OnRowDataBound="gvInterviewAppointments_RowDataBound" OnRowCommand="gvInterviewAppointments_RowCommand"
                        EmptyDataText="No interview appointments">
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                        <Columns>
                            <asp:TemplateField ControlStyle-Width="18px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButtonExpandCollapseDetails" ImageUrl="~/images/plus.png"
                                        runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:RadioButton ID="RadioButton1" runat="server" onclick="RadioCheck(this);" />
                                    <asp:HiddenField ID="HiddenFieldJobID" runat="server" Value='<%# Bind("JobID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="15px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="JobReference" HeaderText="Job Ref:" />
                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                            <asp:BoundField DataField="JobCategory" HeaderText="Job Category" />
                            <asp:BoundField DataField="JobType" HeaderText="Job Type" />
                            <asp:BoundField DataField="TotalApplications" HeaderText="#Applications" />
                            <asp:BoundField DataField="TotalShortlisted" HeaderText="#Shortlisted" />
                            <asp:BoundField DataField="TotalInterviewed" HeaderText="#Interviewed" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    </td> </tr>
                                    <tr>
                                        <td colspan="100%" style="padding-left: 28px; padding-bottom: 0px; padding-top: 0px;
                                            padding-right: 0px; height: 0px;">
                                            <asp:Panel ID="PanelExpandCollapseDetails" Style="display: none;" runat="server">
                                                <asp:GridView ID="gvInterviewAppointmentApplicants" runat="server" AllowPaging="True"
                                                    AllowSorting="True" AutoGenerateColumns="False" CssClass="Inner-GridViewStyle"
                                                    EmptyDataText="No applicant's scheduled for an interview" PageSize="5">
                                                    <FooterStyle CssClass="PagerStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                <asp:HiddenField ID="HiddenFieldInterviewAppointmentID" runat="server" Value='<%# Bind("InterviewAppointmentID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="5px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="ApplicantReference" HeaderText="Applicant" />
                                                        <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name" />
                                                        <asp:BoundField DataField="AppointmentDate" HeaderText="Appointment Date" DataFormatString="{0:dd/MMM/yyyy}" />
                                                        <asp:BoundField DataField="AppointmentTime" HeaderText="Appointment Time" />
                                                        <asp:BoundField DataField="IsInterviewConducted" HeaderText="Is Interview Conducted?" />
                                                        <asp:BoundField DataField="InterviewChoiceValue" HeaderText="Choice" />
                                                    </Columns>
                                                    <%--   <HeaderStyle Font-Size="11px" Height="10px" />
                                                    <RowStyle Font-Size="11px" />--%>
                                                </asp:GridView>
                                                <asp:Panel ID="PanelApplicantsScheduledEntryButtons" CssClass="grid-linkBtn" runat="server">
                                                    <asp:UpdatePanel ID="UpdatePanelApplicantsScheduledEntryButtons" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnEditApplicantInterviewSchedule" runat="server" CssClass="buttonNavigations"
                                                                CommandName="EditApplicantInterviewSchedule" Text="Edit Interview Appointment"
                                                                ToolTip="Edit interview appointment" CausesValidation="false" />
                                                            <asp:Button ID="btnAddApplicantInterviewComments" runat="server" CommandName="AddApplicantInterviewComments"
                                                                Text="Add Interview Comments" ToolTip="Add applicant's interview comments" CausesValidation="false" />
                                                            <%-- <asp:Button ID="btnAddNewItem" runat="server" CssClass="buttonNavigations" CommandName="AddBudgetDetailItem"
                                                                Text="Add New Item" CausesValidation="false" />--%>
                                                            <asp:Button ID="btnDeleteApplicantInterviewSchedule" runat="server" CssClass="buttonNavigations"
                                                                Text="Delete" CommandName="DeleteApplicantInterviewSchedule" ToolTip="Delete interview appointment"
                                                                CausesValidation="false" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </asp:Panel>
                                            </asp:Panel>
                                            <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtenderExpandCollapseDetails"
                                                runat="Server" TargetControlID="PanelExpandCollapseDetails" CollapsedSize="0"
                                                Collapsed="True" ExpandControlID="ImageButtonExpandCollapseDetails" CollapseControlID="ImageButtonExpandCollapseDetails"
                                                AutoCollapse="False" AutoExpand="False" ScrollContents="false" TextLabelID="Label1"
                                                CollapsedText="Show applicants scheduled for interview..." ExpandedText="Hide applicants scheduled for interview"
                                                ImageControlID="ImageButtonExpandCollapseDetails" ExpandedImage="~/images/minus.png"
                                                CollapsedImage="~/images/plus.png" ExpandDirection="Vertical" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <script language="javascript" type="text/javascript">
                        select_first_gvInterviewAppointment();</script>
                </div>
                <asp:Panel ID="PanelAddInterviewAppointment" Style="display: none; width: 55%;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddInterviewAppointment" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddInterviewAppointmentHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddInterviewAppointmentHeader" runat="server" Text="Add New Interview Appointment"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddInterviewAppointment" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Interview Appointment Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Job Reference & Title:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlJobReferenceAndTitle" Width="400px" OnSelectedIndexChanged="ddlJobReferenceAndTitle_SelectedIndexChanged"
                                            AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Applicant Reference & Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlApplicantReferenceAndName" Width="400px" runat="server">
                                            <asp:ListItem Value="0">-Select Job Applicant-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Appointment Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAppointmentDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonAppointmentDate" ToolTip="Pick appointment date"
                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtAppointmentDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtAppointmentDate" PopupButtonID="ImageButtonAppointmentDate"
                                            PopupPosition="TopRight"></asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtAppointmentDate_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtAppointmentDate"
                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtAppointmentDate_MaskedEditValidator" runat="server"
                                            ControlExtender="txtAppointmentDate_MaskedEditExtender" ControlToValidate="txtAppointmentDate"
                                            CssClass="errorMessage" ErrorMessage="txtAppointmentDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid appointment date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Appointment Time:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAppointmentTime" Width="100px" runat="server"></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtendertxtAppointmentTime" TargetControlID="txtAppointmentTime"
                                            ClearMaskOnLostFocus="false" UserTimeFormat="TwentyFourHour" CultureName="en-GB"
                                            Mask="99:99" MaskType="Time" InputDirection="LeftToRight" runat="server"></asp:MaskedEditExtender>
                                        <span class="info-message">[24hr Format e.g. 08:30]</span>
                                    </td>
                                </tr>
                                <%--<tr>
                                <td>
                                    Interview Complete:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblInterviewComplete" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Commments:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>--%>
                                <tr>
                                    <td>
                                        Remarks:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAppointmentComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldInterviewAppointmentID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddInterviewAppointmentPopup" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAccessedgvInterviewAppointmentApplicantsRowIndex"
                                            runat="server" />
                                        <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSaveInterviewAppointment" OnClick="lnkBtnSaveInterviewAppointment_Click"
                                ToolTip="Save Interview Appointment" runat="server">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateInterviewAppointment" OnClick="lnkBtnUpdateInterviewAppointment_Click"
                                CausesValidation="false" ToolTip="Update Interview Appointment" Enabled="false"
                                runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearInterviewAppointment" OnClick="lnkBtnClearInterviewAppointment_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddInterviewAppointment" RepositionMode="None"
                    TargetControlID="HiddenFieldAddInterviewAppointmentPopup" PopupControlID="PanelAddInterviewAppointment"
                    CancelControlID="ImageButtonCloseAddInterviewAppointment" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <%--<asp:DragPanelExtender ID="DragPanelExtenderAddInterviewAppointment" TargetControlID="PanelAddInterviewAppointment"
                DragHandleID="PanelDragAddInterviewAppointment" runat="server">
            </asp:DragPanelExtender>--%>
                <asp:Panel ID="PanelAddInterviewComments" Style="display: none; width: 60%;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddInterviewComments" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddInterviewCommentsHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddInterviewCommentsHeader" runat="server" Text="Interview Comments"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddInterviewComments" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Interview Comments</legend>
                            <table>
                                <tr>
                                    <td>
                                        Interview Complete?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblInterviewComplete" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                            <asp:ListItem Value="No">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Commments:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <div style="height: 140px; width: 582px;" class="div-html-ajax-editor">
                                            <asp:TextBox ID="txtInterviewComments" TextMode="MultiLine" Width="580" Height="130"
                                                runat="server"></asp:TextBox>
                                            <asp:HtmlEditorExtender ID="txtInterviewComments_HtmlEditorExtender" EnableSanitization="false"
                                                TargetControlID="txtInterviewComments" runat="server">
                                                <Toolbar>
                                                    <asp:Bold />
                                                    <asp:Italic />
                                                    <asp:Underline />
                                                    <asp:Undo />
                                                    <asp:Redo />
                                                    <asp:InsertOrderedList />
                                                    <asp:InsertUnorderedList />
                                                    <asp:ForeColorSelector />
                                                    <asp:FontSizeSelector />
                                                </Toolbar>
                                            </asp:HtmlEditorExtender>
                                        </div>
                                    </td>
                                </tr>
                                <%--  <tr>
                                    <td valign="top">
                                        Challenges:
                                    </td>
                                    <td colspan="3">
                                        <div style="height: 140px; width: 582px;" class="div-html-ajax-editor">
                                            <asp:TextBox ID="txtInterviewChallenges" TextMode="MultiLine" Width="580" Height="130"
                                                runat="server"></asp:TextBox>
                                            <asp:HtmlEditorExtender ID="txtInterviewChallenges_HtmlEditorExtender" TargetControlID="txtInterviewChallenges"
                                                runat="server">
                                                <Toolbar>
                                                    <asp:Bold />
                                                    <asp:Italic />
                                                    <asp:Underline />
                                                    <asp:Undo />
                                                    <asp:Redo />
                                                    <asp:InsertOrderedList />
                                                    <asp:InsertUnorderedList />
                                                    <asp:ForeColorSelector />
                                                    <asp:FontSizeSelector />
                                                </Toolbar>
                                            </asp:HtmlEditorExtender>
                                        </div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        Recommendation:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblInterviewRecommendation" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem>Employ</asp:ListItem>
                                            <asp:ListItem>Reject</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Choice:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblInterviewChoice" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="1">1<sup>st</sup></asp:ListItem>
                                            <asp:ListItem Value="2">2<sup>nd</sup></asp:ListItem>
                                            <asp:ListItem Value="3">3<sup>rd</sup></asp:ListItem>
                                            <asp:ListItem Value="4">4<sup>th</sup></asp:ListItem>
                                            <asp:ListItem Value="5">5<sup>th</sup></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        Interviewed By:<span class="errorMessage">*</span>
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlInterviewedBy" runat="server">
                                                        <asp:ListItem Value="0">-Select Interviewed By-</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <div class="small-linkBtn" style="text-align: left;">
                                                        <asp:LinkButton ID="lnkBtnAddInterviewer" OnClick="lnkBtnAddInterviewer_Click" ToolTip="Add interviewer"
                                                            runat="server">
                                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Create.png"
                                                                ImageAlign="AbsMiddle" />
                                                            Add</asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:ListBox ID="lstBoxInterviewedBy" runat="server" Height="86px" SelectionMode="Multiple"
                                                        Width="305px"></asp:ListBox>
                                                </td>
                                                <td>
                                                    <div class="small-linkBtn" style="text-align: left;">
                                                        <asp:LinkButton ID="lnkBtnRemoveInterviewer" OnClick="lnkBtnRemoveInterviewer_Click"
                                                            ToolTip="Remove interviewer" runat="server">
                                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/erase.png" ImageAlign="AbsMiddle" />
                                                            Remove</asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldAddInterviewCommentsPopup" runat="server" />
                                        <asp:Image ID="Image6" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSaveInterviewComments" OnClick="lnkBtnSaveInterviewComments_Click"
                                ToolTip="Save interview comments" runat="server">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save Comments</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddInterviewComments" RepositionMode="None"
                    TargetControlID="HiddenFieldAddInterviewCommentsPopup" PopupControlID="PanelAddInterviewComments"
                    CancelControlID="ImageButtonCloseAddInterviewComments" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:Panel ID="PanelAddOverallInterviewComments" Style="display: none; width: 60%;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddOverallInterviewComments" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddOverallInterviewCommentsHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddOverallInterviewCommentsHeader" runat="server" Text="Overall Interview Comments & Resolutions"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddOverallInterviewComments" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Overall Interview Comments</legend>
                            <table>
                                <tr>
                                    <td valign="top">
                                        Overall Comments:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <div style="height: 140px; width: 582px;" class="div-html-ajax-editor">
                                            <asp:TextBox ID="txtOverallInterviewComments" TextMode="MultiLine" Width="580" Height="130"
                                                runat="server"></asp:TextBox>
                                            <asp:HtmlEditorExtender ID="txtOverallInterviewComments_HtmlEditorExtender" EnableSanitization="false"
                                                TargetControlID="txtOverallInterviewComments" runat="server">
                                                <Toolbar>
                                                    <asp:Bold />
                                                    <asp:Italic />
                                                    <asp:Underline />
                                                    <asp:Undo />
                                                    <asp:Redo />
                                                    <asp:InsertOrderedList />
                                                    <asp:InsertUnorderedList />
                                                    <asp:ForeColorSelector />
                                                    <asp:FontSizeSelector />
                                                </Toolbar>
                                            </asp:HtmlEditorExtender>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Close the Job?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblCloseTheJob" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                            <asp:ListItem Value="No">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Placed Candidate:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPlacedCandidate" Width="400px" runat="server">
                                            <asp:ListItem Value="0">-Select Placed Candidate-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Reporting Date?
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtReportingDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonReportingDate" ToolTip="Pick reporting date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtReportingDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtReportingDate" PopupButtonID="ImageButtonReportingDate"
                                            PopupPosition="TopRight"></asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtReportingDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtReportingDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtReportingDate_MaskedEditValidator" runat="server"
                                            ControlExtender="txtAppointmentDate_MaskedEditExtender" ControlToValidate="txtReportingDate"
                                            CssClass="errorMessage" ErrorMessage="txtAppointmentDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid reporting date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldAddOverallInterviewCommentsPopup" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldJobID" runat="server" />
                                        <asp:Image ID="Image9" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="lbErrorMessage" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSaveOverallInterviewComments" OnClick="lnkBtnSaveOverallInterviewComments_Click"
                                ToolTip="Save overall interview comments" runat="server">
                                <asp:Image ID="Image10" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save Overall Comments</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddOverallInterviewComments" RepositionMode="None"
                    TargetControlID="HiddenFieldAddOverallInterviewCommentsPopup" PopupControlID="PanelAddOverallInterviewComments"
                    CancelControlID="ImageButtonCloseAddOverallInterviewComments" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
