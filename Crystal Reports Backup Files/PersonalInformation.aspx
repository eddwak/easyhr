﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="PersonalInformation.aspx.cs" Inherits="AdeptHRManager.User_Module.PersonalInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel-details">
        <asp:Panel ID="PanelStafPersonalInformation" runat="server">
            <table width="100%">
                <tr>
                    <td>
                        Employee Reference:
                    </td>
                    <td>
                        <asp:TextBox ID="txtStaffReference" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Last Name: 
                    </td>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        First Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Middle Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date of Birth:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDOB" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Gender:
                    </td>
                    <td>
                             <asp:TextBox ID="txtGender" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Marital Status:
                    </td>
                    <td>
                       
                        <asp:TextBox ID="txtMaritalStatus" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Mobile Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nationality:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNationality" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Passport Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassportNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Passport Expiry Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassportExpiryDate" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        National ID Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNationalID" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Diplomatic ID Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDiplomaticIDNumber" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Driving Permit Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDrivingPermitNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        NSSF Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNSSFNumber" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Pin Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPinNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        TIN Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTINNumber" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Permanent Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPermanentAddress" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Current Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCurrentAddress" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Telephone Contact:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelephoneContact" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Work Email Address:<span class="errorMessage">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Personal Email Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAlternativeEmailAddress" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Postal Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPostalAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Languages:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLanguages" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        1<sup>st</sup> Next of Kin:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKinName" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        1<sup>st</sup> Next of Kin Phone Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKinPhoneNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        1<sup>st</sup> Next of Kin Relationship:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKinRelationship" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        1<sup>st</sup> Next of Kin Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKinAddress" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        2<sup>nd</sup> Next of Kin:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKin2Name" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        2<sup>nd</sup> Next of Kin Phone Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKin2PhoneNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        2<sup>nd</sup> Next of Kin Relationship:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKin2Relationship" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        2<sup>nd</sup> Next of Kin Address:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNextOfKin2Address" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Details of Any Disability:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisabilityDetails" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Referee Details 1:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRefereeDetails1" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Referee Details 2:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRefereeDetails2" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Referee Details 3:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRefereeDetails3" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PanelStaffAdditionalFields" runat="server">
            <hr />
            <b>Additional Information</b>
            <hr />
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lbHRDescription01" Visible="false" runat="server" Text="HR Description 01:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription01" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lbHRDescription06" Visible="false" runat="server" Text="HR Description 06:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription06" Visible="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbHRDescription02" Visible="false" runat="server" Text="HR Description 02:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription02" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lbHRDescription07" Visible="false" runat="server" Text="HR Description 07:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription07" Visible="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbHRDescription03" Visible="false" runat="server" Text="HR Description 03:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription03" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lbHRDescription08" Visible="false" runat="server" Text="HR Description 08:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription08" Visible="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbHRDescription04" Visible="false" runat="server" Text="HR Description 09:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription04" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lbHRDescription09" Visible="false" runat="server" Text="HR Description 09:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription09" Visible="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbHRDescription05" Visible="false" runat="server" Text="HR Description 05:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription05" Visible="false" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lbHRDescription10" Visible="false" runat="server" Text="HR Description 10:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHRDescription10" Visible="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Remarks:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
