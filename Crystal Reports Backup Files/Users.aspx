﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Security_Module/SecurityModule.master"
    AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="AdeptHRManager.Security_Module.Users" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SecurityModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelUsers" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelUsers" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new user"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit user details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View user details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelAssignRights" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonAssignRights" OnClick="LinkButtonAssignRights_Click"
                        CausesValidation="false" ToolTip="Assign access rights to user" runat="server">
                        <asp:Image ID="ImageAssignRights" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Notepad.png" />
                        Assign Rights</asp:LinkButton>
                    <asp:Label ID="LabelSaveAppPages" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonAppPages" ToolTip="Save application pages" CausesValidation="false"
                        runat="server">
                        <asp:Image ID="ImageSaveAppPages" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Download.png" />
                        Save App Pages</asp:LinkButton>
                    <asp:Label ID="LabelAssignAppPages" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonAssignAppPAges" OnClick="LinkButtonAssignAppPages_Click"
                        CausesValidation="false" ToolTip="Assign application pages to a user" runat="server">
                        <asp:Image ID="ImageAssignAppPages" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Accept.png" />
                        Assign App Pages</asp:LinkButton>
                    <asp:Label ID="LabelResetUserPassword" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonResetUserPassword" OnClick="LinkButtonResetUserPassword_Click"
                        CausesValidation="false" ToolTip="Reset user password" runat="server">
                        <asp:Image ID="ImageResetUserPassword" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Modify.png" />
                        Reset Password</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete user" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="search-div">
                    Search:
                    <asp:TextBox ID="txtSearch" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"
                        runat="server"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by staff name:"
                        WatermarkCssClass="water-mark-text-extender">
                    </asp:TextBoxWatermarkExtender>
                    <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetAppUsers"
                        TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                        CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                        CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                    </asp:AutoCompleteExtender>
                    <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                        width: 343px !important;">
                    </div>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvUsers" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No users available!"
                        OnPageIndexChanging="gvUsers_PageIndexChanging" OnLoad="gvUsers_OnLoad">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("UserID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="StaffName" HeaderText="Staff Name" />
                            <asp:BoundField DataField="UserName" HeaderText="User Name" />
                            <asp:BoundField DataField="UserRole" HeaderText="Role" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddUser" Style="display: none; width: 580px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddUser" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddUserHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddUserHeader" runat="server" Text="Add New User"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddUser" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend id="legendUserDetails" runat="server">User Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Staff Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStaffName" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Username:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Role:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlUserRole" runat="server">
                                            <asp:ListItem Value="Administrator">Administrator</asp:ListItem>
                                            <asp:ListItem Value="Manager">Manager</asp:ListItem>
                                            <asp:ListItem Value="Staff">Staff</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Password:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Confirm Password:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblUserStatus" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Active" Value="True"></asp:ListItem>
                                            <asp:ListItem Text="Inactive" Value="False"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="HiddenFieldUserID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddUserPopup" runat="server" />
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSaveUser" OnClick="lnkBtnSaveUser_Click" ToolTip="Save user"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateUser" OnClick="lnkBtnUpdateUser_Click" CausesValidation="false"
                                ToolTip="Update User" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearUser" OnClick="lnkBtnClearUser_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddUser" RepositionMode="None" X="270"
                    Y="50" TargetControlID="HiddenFieldAddUserPopup" PopupControlID="PanelAddUser"
                    CancelControlID="ImageButtonCloseAddUser" BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddUser" TargetControlID="PanelAddUser"
                    DragHandleID="PanelDragAddUser" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelResetUserPassword" Style="display: none; width: 550px" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragResetUserPassword" runat="server">
                            <table width="100%">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/note_edit.png" />
                                        <asp:Label ID="lbResetUserPasswordHeader" runat="server" Text="Reset Password"></asp:Label>
                                    </td>
                                    <td style="width: 3%;">
                                        <asp:ImageButton ID="ImageButtonCloseResetUserPassword" ImageUrl="~/images/icons/Small/Remove.png"
                                            runat="server" ToolTip="Close" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Password Details </legend>
                            <table>
                                <tr>
                                    <td width="140px">
                                        <asp:Label ID="Laggbel3" runat="server" Text="New Password"></asp:Label>
                                        <asp:Label ID="Label7" runat="server" CssClass="errorMessage" Text="*"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtResetUserNewPassword" CssClass="textBoxDetails" runat="server"
                                            TextMode="Password"></asp:TextBox><br />
                                        <asp:Label ID="lbResetPasswordStrengthHelp" CssClass="passwordHelpLabel" runat="server" />
                                        <asp:PasswordStrength ID="txtResetUserNewPassword_Strength" TargetControlID="txtResetUserNewPassword"
                                            DisplayPosition="RightSide" StrengthIndicatorType="Text" PreferredPasswordLength="6"
                                            PrefixText="Strength:" MinimumNumericCharacters="1" MinimumSymbolCharacters="1"
                                            MinimumUpperCaseCharacters="1" RequiresUpperAndLowerCaseCharacters="true" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"
                                            TextStrengthDescriptionStyles="strength_label_poor;strength_label_weak;strength_label_average;strength_label_strong;strength_label_excellent"
                                            CalculationWeightings="50;15;15;20" HelpStatusLabelID="lbResetPasswordStrengthHelp"
                                            runat="server" />
                                        <asp:RegularExpressionValidator ID="txtResetUserNewPassword_RegularExpressionValidator"
                                            ControlToValidate="txtResetUserNewPassword" ValidationExpression="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
                                            Display="Dynamic" EnableClientScript="false" ErrorMessage="<br>Password must be 6 characters long with at least one numeric,one uppercase character and one special character."
                                            CssClass="errorMessage" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="Confirm Password"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" CssClass="errorMessage" Text="*"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtResetUserConfirmationPassword" CssClass="textBoxDetails" runat="server"
                                            TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbResetPasswordError" runat="server" CssClass="errorMessage"></asp:Label>
                                        <asp:HiddenField ID="HiddenFieldResetUserPasswordPopup" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSavePasswordChanges" CssClass="linkBtn" ToolTip="Save Password Changes"
                                OnClick="lnkBtnSavePasswordChanges_Click" runat="server">
                                <asp:Image ID="ImagePASSWORDCHANGES" runat="server" ImageUrl="~/images/icons/Small/Save.png"
                                    ImageAlign="AbsMiddle" />
                                Save Password Changes</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderResetUserPassword" RepositionMode="None"
                    X="270" Y="100" BackgroundCssClass="modalback" TargetControlID="HiddenFieldResetUserPasswordPopup"
                    PopupControlID="PanelResetUserPassword" CancelControlID="ImageButtonCloseResetUserPassword"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderResetUserPassword" TargetControlID="PanelResetUserPassword"
                    DragHandleID="PanelDragResetUserPassword" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelAddUserAccessRight" Style="display: none; width: 675px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddUserAccessRight" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="97%">
                                        <asp:Image ID="ImageAddUserAccessRightHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Accept.png" />
                                        <asp:Label ID="lbAddUserAccessRightHeader" runat="server" Text="Add Access Right"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:ImageButton ID="ImageButtonAddUserAccessRightClose" ImageUrl="~/images/icons/Small/Remove.png"
                                            runat="server" ToolTip="Close" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Access Right Details </legend>
                            <table width="100%">
                                <tr valign="top">
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="Access Rights"></asp:Label>
                                        <asp:Label ID="Label17" runat="server" Text="*" CssClass="errorMessage"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblAccessRights" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Add</asp:ListItem>
                                            <asp:ListItem>Add / Alter</asp:ListItem>
                                            <asp:ListItem>Alter</asp:ListItem>
                                            <asp:ListItem>Delete</asp:ListItem>
                                            <asp:ListItem>Read</asp:ListItem>
                                            <asp:ListItem>Full</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        <asp:Label ID="Label18" runat="server" Text="Modules"></asp:Label>
                                        <asp:Label ID="Label13" runat="server" Text="*" CssClass="errorMessage"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBoxList ID="cblApplicationModules" RepeatDirection="Vertical" RepeatColumns="2"
                                            runat="server">
                                            <asp:ListItem Text="HR Module" Value="HR Module"></asp:ListItem>
                                            <asp:ListItem Text="Leave and Attendance" Value="Leave and Attendance"></asp:ListItem>
                                            <asp:ListItem Text="Recruitment" Value="Recruitment"></asp:ListItem>
                                            <asp:ListItem Text="Payroll" Value="Payroll"></asp:ListItem>
                                            <asp:ListItem Text="Training" Value="Training"></asp:ListItem>
                                            <asp:ListItem Text="Performance Review" Value="Performance Review"></asp:ListItem>
                                            <asp:ListItem Text="Settings" Value="Settings"></asp:ListItem>
                                            <asp:ListItem Text="Security Module" Value="Security Module"></asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image4" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label20" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldUserRightID" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveAccessRight" OnClick="lnkBtnSaveAccessRight_Click"
                                CssClass="linkBtn" ToolTip="Save access right" runat="server">
                                <asp:Image ID="ImageSaveAccessRight" runat="server" ImageUrl="~/images/icons/Small/Save.png"
                                    ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddUserAccessRight" RepositionMode="None"
                    X="240" Y="60" BackgroundCssClass="modalback" TargetControlID="HiddenFieldUserRightID"
                    PopupControlID="PanelAddUserAccessRight" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddUserAccessRight" TargetControlID="PanelAddUserAccessRight"
                    DragHandleID="PanelDragAddUserAccessRight" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelApplicationPages" runat="server" Style="display: none; width: 800px">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragApplicationPages" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="97%">
                                        <asp:Image ID="Image8" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Download.png" />
                                        <asp:Label ID="Label11" runat="server" Text="Save Application Pages"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:ImageButton ID="ImageCloseAppPages" ImageUrl="~/images/icons/Small/Remove.png"
                                            runat="server" ToolTip="Close" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div class="div-app-pages">
                                        <asp:GridView ID="gvApplicationPages" Width="100%" runat="server" CssClass="GridViewStyle"
                                            AllowPaging="True" PageSize="100">
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <FooterStyle CssClass="PagerStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label12" runat="server" CssClass="errorMessage" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="linkBtn">
                                        <asp:LinkButton ID="lnkBtnSaveAppPages" OnClick="lnkBtnSaveAppPages_Click" CssClass="linkBtn"
                                            ToolTip="Save Application pages" runat="server">
                                            <asp:Image ID="ImageBtnSaveAppPages" runat="server" ImageUrl="~/images/icons/Small/Save.png"
                                                ImageAlign="AbsMiddle" />
                                            Save Pages</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderApplicationPages" RepositionMode="None"
                    X="100" Y="10" BackgroundCssClass="modalback" TargetControlID="LinkButtonAppPages"
                    PopupControlID="PanelApplicationPages" CancelControlID="ImageCloseAppPages" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderApplicationPages" TargetControlID="PanelApplicationPages"
                    DragHandleID="PanelDragApplicationPages" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelAssignAccessPages" Style="display: none; width: 800px" runat="server"
                    CssClass="popupDetails">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAssignAceesPages" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="97%">
                                        <asp:Image ID="Image9" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/images/icons/Medium/Accept.png" />
                                        <asp:Label ID="labelAssignAccessPages" runat="server" Text="Assign Access Pages"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:ImageButton ID="ImageButtonCloseAssignPages" ImageUrl="~/images/icons/Small/Remove.png"
                                            runat="server" ToolTip="Close" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div class="div-app-pages">
                                        <asp:GridView ID="gvAccessPages" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle">
                                            <PagerStyle CssClass="PagerStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <Columns>
                                                <asp:BoundField DataField="No" HeaderText="No:" ItemStyle-Width="4px" />
                                                <asp:TemplateField ItemStyle-Width="4px">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="CheckBoxSelectAll" AutoPostBack="true" OnCheckedChanged="CheckBoxSelectAll_CheckedChanged"
                                                            ToolTip="Assign all pages to the user" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" ToolTip="Assign this page to the user" runat="server"
                                                            Checked='<%# Bind("Checked") %>' />
                                                        <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AccessPageName" ItemStyle-Width="30%" HeaderText="Access Page Name" />
                                                <asp:BoundField DataField="AccessPageUrl" HeaderText="Page URL" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="linkBtn">
                                        <asp:Label ID="labelAssignAccessPagesPopup" runat="server"></asp:Label>
                                        <asp:LinkButton ID="lnkBtnSaveAccessPages" OnClick="lnkBtnSaveAccessPages_Click"
                                            CssClass="linkBtn" ToolTip="Save Access pages" runat="server">
                                            <asp:Image ID="ImageSaveAccessPages" runat="server" ImageUrl="~/images/icons/Small/Save.png"
                                                ImageAlign="AbsMiddle" />
                                            Save Access Pages</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAssignAccessPages" RepositionMode="None"
                    X="100" Y="10" runat="server" PopupControlID="PanelAssignAccessPages" TargetControlID="labelAssignAccessPagesPopup"
                    CancelControlID="ImageButtonCloseAssignPages" BackgroundCssClass="modalback">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAssignAccessPages" TargetControlID="PanelAssignAccessPages"
                    DragHandleID="PanelDragAssignAceesPages" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
