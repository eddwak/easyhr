﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestUploadImage.aspx.cs"
    Inherits="AdeptHRManager.TestUploadImage" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Scripts/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.7.1.min.js"></script>
    <script src="Scripts/jquery.Jcrop.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $('#<%=imgUpload.ClientID%>').Jcrop({
                onSelect: SelectCropArea
            });
        });
        function SelectCropArea(c) {
            $('#<%=X.ClientID%>').val(parseInt(c.x));
            $('#<%=Y.ClientID%>').val(parseInt(c.y));
            $('#<%=W.ClientID%>').val(parseInt(c.w));
            $('#<%=H.ClientID%>').val(parseInt(c.h));
        }</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Image Upload, Crop & Save using ASP.NET & Jquery</h3>
        <%-- HTML Code --%>
        <table>
            <tr>
                <td>
                    Select Image File :
                </td>
                <td>
                    <asp:FileUpload ID="FU1" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="panCrop" runat="server" Visible="false">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="imgUpload" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCrop" runat="server" Text="Crop & Save" OnClick="btnCrop_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%-- Hidden field for store cror area --%>
                        <asp:HiddenField ID="X" runat="server" />
                        <asp:HiddenField ID="Y" runat="server" />
                        <asp:HiddenField ID="W" runat="server" />
                        <asp:HiddenField ID="H" runat="server" />
                        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
                            AutoDataBind="true" />
                        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                        </CR:CrystalReportSource>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
