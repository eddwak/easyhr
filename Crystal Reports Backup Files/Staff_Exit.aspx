﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Exit.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Exit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffExit" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffExit" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelAddStaffExit" runat="server">
                    <div class="panel-details">
                        <table>
                            <tr>
                                <td>
                                    Mode of Exit:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlModeOfExit" runat="server">
                                        <asp:ListItem Value="0">-Select Mode of Exit-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Exit Date:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonExitDate" ToolTip="Pick exit date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtExitDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtExitDate" PopupButtonID="ImageButtonExitDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtExitDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtExitDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtExitDate_MaskedEditValidator" runat="server" ControlExtender="txtExitDate_MaskedEditExtender"
                                        ControlToValidate="txtExitDate" CssClass="errorMessage" ErrorMessage="txtExitDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid exit date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitReason" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Exit Interview Comments
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitInterviewComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Clearance:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtClearance" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Interview By:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlInterviewBy" runat="server">
                                        <asp:ListItem Value="0">-Select Intervieed By-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Has exit interview been submitted?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblExitInterviewSubmitted" AutoPostBack="true" RepeatDirection="Horizontal"
                                        runat="server">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Has clearance form been fully signed?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblClearanceFormSigned" AutoPostBack="true" RepeatDirection="Horizontal"
                                        runat="server">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Has handover report been submitted?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblHandOverReportSubmitted" AutoPostBack="true" RepeatDirection="Horizontal"
                                        runat="server">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <%-- <tr>
                                <td valign="top">
                                    Attach Document:
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUploadDocument" runat="server" />
                                    <br />
                                    <asp:Label ID="lbD" CssClass="doc-info" runat="server" Text="File types allowed: Word and PDF(i.e .doc,.docx,.rtf & .pdf).<br>Maximum file size: 3mb."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Uploaded Document:
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnUploadedDocumentLink" OnClick="lnkBtnUploadedDocumentLink_OnClick"
                                        runat="server">No uploaded document</asp:LinkButton>
                                </td>
                            </tr>--%>
                            <tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbCarReturned" Visible="false" runat="server" Text="">Has car been returned?<span class="errorMessage">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblCarReturned" Visible="false" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbCarReturnDate" Visible="false" runat="server" Text="Date:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCarReturnDate" Visible="false" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonCarReturnDate" Visible="false" ToolTip="Pick car return date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtCarReturnDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtCarReturnDate" PopupButtonID="ImageButtonCarReturnDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtCarReturnDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                                        Mask="99/LLL/9999" TargetControlID="txtCarReturnDate" UserDateFormat="DayMonthYear">
                                                    </asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtCarReturnDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtCarReturnDate_MaskedEditExtender" ControlToValidate="txtCarReturnDate"
                                                        CssClass="errorMessage" ErrorMessage="txtCarReturnDate_MaskedEditValidator" InvalidValueMessage="<br>Invalid car return date entered"
                                                        Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbCarReturnedComments" Visible="false" runat="server" Text="Comments:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCarReturnedComments" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbHouseReturned" Visible="false" runat="server" Text="">Has the house been returned?<span class="errorMessage">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblHouseReturned" Visible="false" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbHouseReturnDate" Visible="false" runat="server" Text="Date:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtHouseReturnDate" Visible="false" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonHouseReturnDate" Visible="false" ToolTip="Pick house return date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtHouseReturnDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtHouseReturnDate" PopupButtonID="ImageButtonHouseReturnDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtHouseReturnDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtHouseReturnDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtHouseReturnDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtHouseReturnDate_MaskedEditExtender" ControlToValidate="txtHouseReturnDate"
                                                        CssClass="errorMessage" ErrorMessage="txtHouseReturnDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid house return date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbHouseReturnedComments" Visible="false" runat="server" Text="Comments:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtHouseReturnedComments" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbTelephoneReturned" Visible="false" runat="server" Text="">Has the telephone been returned?<span class="errorMessage">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblTelephoneReturned" Visible="false" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbTelephoneReturnDate" Visible="false" runat="server" Text="Date:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTelephoneReturnDate" Visible="false" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonTelephoneReturnDate" Visible="false" ToolTip="Pick telephone return date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtTelephoneReturnDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtTelephoneReturnDate"
                                                        PopupButtonID="ImageButtonTelephoneReturnDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtTelephoneReturnDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtTelephoneReturnDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtTelephoneReturnDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtTelephoneReturnDate_MaskedEditExtender" ControlToValidate="txtTelephoneReturnDate"
                                                        CssClass="errorMessage" ErrorMessage="txtTelephoneReturnDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid telephone return date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbTelephoneReturnedComments" Visible="false" runat="server" Text="Comments:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTelephoneReturnedComments" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbLaptopReturned" Visible="false" runat="server" Text="">Has laptop been returned?<span class="errorMessage">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblLaptopReturned" Visible="false" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbLaptopReturnDate" Visible="false" runat="server" Text="Date:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLaptopReturnDate" Visible="false" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLaptopReturnDate" Visible="false" ToolTip="Pick laptop return date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLaptopReturnDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtLaptopReturnDate" PopupButtonID="ImageButtonLaptopReturnDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtLaptopReturnDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtLaptopReturnDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtLaptopReturnDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtLaptopReturnDate_MaskedEditExtender" ControlToValidate="txtLaptopReturnDate"
                                                        CssClass="errorMessage" ErrorMessage="txtLaptopReturnDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid laptop return date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbLaptopReturnedComments" Visible="false" runat="server" Text="Comments:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLaptopReturnedComments" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbLoanGuranteedReturned" Visible="false" runat="server" Text="">Has loan guarantee cleared?<span class="errorMessage">*</span></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblLoanGuranteedReturned" Visible="false" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbLoanGuranteedReturnDate" Visible="false" runat="server" Text="Date:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLoanGuranteedReturnDate" Visible="false" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonLoanGuranteedReturnDate" Visible="false" ToolTip="Pick date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtLoanGuranteedReturnDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtLoanGuranteedReturnDate"
                                                        PopupButtonID="ImageButtonLoanGuranteedReturnDate" PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtLoanGuranteedReturnDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtLoanGuranteedReturnDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtLoanGuranteedReturnDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtLoanGuranteedReturnDate_MaskedEditExtender" ControlToValidate="txtLoanGuranteedReturnDate"
                                                        CssClass="errorMessage" ErrorMessage="txtLoanGuranteedReturnDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid loan guranteed  date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbLoanGuranteedReturnedComments" Visible="false" runat="server" Text="Comments:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLoanGuranteedReturnedComments" Visible="false" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Has 90% terminal benefits been paid?<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblHas90TerminalBenefitsBeenPaid" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    Date:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDate90PercentPaid" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonDate90PercentPaid" ToolTip="Pick date the 90% was paid"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtDate90PercentPaid_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtDate90PercentPaid" PopupButtonID="ImageButtonDate90PercentPaid"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtDate90PercentPaid_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtDate90PercentPaid"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtDate90PercentPaid_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtDate90PercentPaid_MaskedEditExtender" ControlToValidate="txtDate90PercentPaid"
                                                        CssClass="errorMessage" ErrorMessage="txtDate90PercentPaid_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date for 90% terminal benefits entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt90PercentComments" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Has 10% terminal benefits been paid?<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblHas10TerminalBenefitsBeenPaid" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    Date:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDate10PercentPaid" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonDate10PercentPaid" ToolTip="Pick date 10% terminal benefit was paid"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtDate10PercentPaid_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtDate10PercentPaid" PopupButtonID="ImageButtonDate10PercentPaid"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtDate10PercentPaid_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtDate10PercentPaid"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtDate10PercentPaid_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtDate10PercentPaid_MaskedEditExtender" ControlToValidate="txtDate10PercentPaid"
                                                        CssClass="errorMessage" ErrorMessage="txtDate10PercentPaid_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid date for 10%  terminal benefits entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt10PercentComments" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Has certificate of service been issued?<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblCertificateOfServiceIssued" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    Date:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDateCertificateOfServiceIssued" Width="120px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonDateCertificateOfServiceIssued" ToolTip="Pick date certificate of service issued"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtDateCertificateOfServiceIssued_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtDateCertificateOfServiceIssued"
                                                        PopupButtonID="ImageButtonDateCertificateOfServiceIssued" PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtDateCertificateOfServiceIssued_MaskedEditExtender"
                                                        runat="server" ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtDateCertificateOfServiceIssued"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtDateCertificateOfServiceIssued_MaskedEditValidator"
                                                        runat="server" ControlExtender="txtDateCertificateOfServiceIssued_MaskedEditExtender"
                                                        ControlToValidate="txtDateCertificateOfServiceIssued" CssClass="errorMessage"
                                                        ErrorMessage="txtDateCertificateOfServiceIssued_MaskedEditValidator" InvalidValueMessage="<br>Invalid certificate issued date entered"
                                                        Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCertificateOfServiceIssuedComments" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="linkBtn">
                                            <asp:HiddenField ID="HiddenFieldStaffExitID" runat="server" />
                                            <asp:LinkButton ID="lnkBtnSaveStaffExit" OnClick="lnkBtnSaveStaffExit_Click" ToolTip="Save staff exit"
                                                runat="server">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                Save Staff Exit</asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
   <%--     <Triggers>
            <asp:PostBackTrigger ControlID="lnkBtnSaveStaffExit" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
