﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Performance_Review/PerformanceReview.master"
    AutoEventWireup="true" CodeBehind="Performance_Review_Reports.aspx.cs" Inherits="AdeptHRManager.Performance_Review.Performance_Review_Reports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PerformanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelPerformanceReviewReports" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelPerformanceReviewReports" runat="server">
                <asp:TabContainer ID="TabContainerPerformanceReviewReports" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelPerformanceReviewReports" runat="server" CssClass="ajax-tab"
                        HeaderText="Leave Reports">
                        <HeaderTemplate>
                            Performance Review Reports
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table class="GridViewStyle">
                                    <tr class="AltRowStyle">
                                        <th>
                                        </th>
                                        <th>
                                            <b>Report Type</b>
                                        </th>
                                        <th>
                                            <b>Description</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="2%">
                                            1.
                                        </td>
                                        <td width="50%">
                                            <asp:LinkButton ID="lnkBtnSingleStaffPerformanceReport" runat="server">Single Staff Performance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays performance report for a selected single staff.
                                        </td>
                                    </tr>
                                    <%--  <tr class="AltRowStyle">
                                        <td>
                                            2.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnDepartmentalPerformanceReport" runat="server">Departmental Performance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays performance report for a selected department.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnOrganisationPerformanceReport" runat="server">Organization Performance Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays the organisation's performance for a series of years or a particular year.
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelReportViewer" runat="server" CssClass="ajax-tab" HeaderText="Report Viewer">
                        <HeaderTemplate>
                            Report Viewer
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divReportViewer" class="divReportViewerCSS">
                                <CR:CrystalReportViewer ID="CrystalReportViewerPerformanceReviewReports" runat="server"
                                    AutoDataBind="true" OnUnload="CrystalReportViewerPerformanceReviewReports_Unload"
                                    ToolPanelView="None" HasToggleGroupTreeButton="false" HasPageNavigationButtons="True"
                                    HasDrilldownTabs="False" SeparatePages="False" BestFitPage="False" GroupTreeStyle-ShowLines="False"
                                    PageZoomFactor="100" HasCrystalLogo="False" PrintMode="Pdf" Enabled="true" ToolbarStyle-CssClass="report-ToolbarStyle"
                                    BorderWidth="0px" ViewStateMode="Enabled" DisplayStatusbar="false" Width="100%" />
                                <CR:CrystalReportSource ID="CrystalReportSourcePerformanceReviewReports" runat="server">
                                </CR:CrystalReportSource>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelOrganisationPerformanceReport" Style="display: none; width: 500px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragOrganisationPerformanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageOrganisationPerformanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbOrganisationPerformanceReport" runat="server" Text="Organisation Performance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseOrganisationPerformanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbOrganisationPerformancePerYear" OnCheckedChanged="cbOrganisationPerformancePerYear_CheckedChanged"
                                            Text="Check the box to view the organisation's performance report for a particular year."
                                            AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Year:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOrganisationPerformancePerYear" Enabled="false" Width="100px"
                                            runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewOrganisationPerformanceReport" OnClick="lnkBtnViewOrganisationPerformanceReport_Click"
                                ToolTip="View the organisation's performamnce report" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                            <asp:HiddenField ID="HiddenFieldOrganisationPerformanceReportPopup" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderOrganisationPerformanceReport" RepositionMode="None"
                    X="300" Y="70" TargetControlID="HiddenFieldOrganisationPerformanceReportPopup"
                    PopupControlID="PanelOrganisationPerformanceReport" CancelControlID="ImageButtonCloseOrganisationPerformanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderOrganisationPerformanceReport" TargetControlID="PanelOrganisationPerformanceReport"
                    DragHandleID="PanelDragOrganisationPerformanceReport" runat="server"></asp:DragPanelExtender>
                <asp:Panel ID="PanelDepartmentPerformanceReport" Style="display: none; width: 560px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragDepartmentPerformanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageDepartmentPerformanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbDepartmentPerformanceReport" runat="server" Text="Departmental Performance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseDepartmentPerformanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Department Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartmentalPerformanceReportDepartmentName" AppendDataBoundItems="True"
                                            runat="server">
                                            <asp:ListItem Value="0">Select Department</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbDepartmentalPerformanceReportPerYear" OnCheckedChanged="cbDepartmentalReportPerYear_CheckedChanged"
                                            Text="Check to view departmental report for a particular year." AutoPostBack="true"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">
                                        Year:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartmentalPerformanceReportPerYear" Enabled="false" Width="100px"
                                            runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress2" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewDepartmentPerformanceReport" OnClick="lnkBtnViewDepartmentPerformanceReport_Click"
                                ToolTip="View departmental performance report" runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                            <asp:HiddenField ID="HiddenFieldDepartmentalPerformanceReportPopup" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderDepartmentPerformanceReport" RepositionMode="None"
                    X="270" Y="70" TargetControlID="HiddenFieldDepartmentalPerformanceReportPopup"
                    PopupControlID="PanelDepartmentPerformanceReport" CancelControlID="ImageButtonCloseDepartmentPerformanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderDepartmentPerformanceReport" TargetControlID="PanelDepartmentPerformanceReport"
                    DragHandleID="PanelDragDepartmentPerformanceReport" runat="server"></asp:DragPanelExtender>
                <asp:Panel ID="PanelSingleStaffPerformanceReport" Style="display: none; width: 580px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragSingleStaffPerformanceReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageSingleStaffPerformanceReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbSingleStaffPerformanceeportHeader" runat="server" Text="Single Staff Performance Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseSingleStaffPerformanceReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Staff Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleStaffPerformanceReportStaffName" runat="server">
                                            <asp:ListItem Value="0">Select Staff Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbSingleStaffPerformanceReportPerYear" OnCheckedChanged="cbSingleStaffPerformanceReportPerYear_CheckedChanged"
                                            Text="Check to view single staff report for a particular year." AutoPostBack="true"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">
                                        Year:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleStaffPerformanceReportPerYear" Enabled="false" Width="100px"
                                            runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress3" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewSingleStaffPerformanceReport" OnClick="lnkBtnViewSingleStaffPerformanceReport_Click"
                                ToolTip="View single staff performance report" runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderSingleStaffPerformanceReport" RepositionMode="None"
                    X="270" Y="70" TargetControlID="TabContainerPerformanceReviewReports$TabPanelPerformanceReviewReports$lnkBtnSingleStaffPerformanceReport"
                    PopupControlID="PanelSingleStaffPerformanceReport" CancelControlID="ImageButtonCloseSingleStaffPerformanceReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderSingleStaffPerformanceReport" TargetControlID="PanelSingleStaffPerformanceReport"
                    DragHandleID="PanelDragSingleStaffPerformanceReport" runat="server"></asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
