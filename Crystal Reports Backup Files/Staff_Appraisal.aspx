﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Performance_Review/PerformanceReview.master"
    AutoEventWireup="true" CodeBehind="Staff_Appraisal.aspx.cs" Inherits="AdeptHRManager.Performance_Review.Staff_Appraisal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PerformanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffTargetAndApraisal" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffTargetAndAppraisal" runat="server">
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                <b>Department:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                    AutoPostBack="true" Width="200px" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>Staff Name:</b><span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStaffName" OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:TabContainer ID="TabContainerStaffAppraisal" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelQuarterlyReview" runat="server" CssClass="ajax-tab" HeaderText="Quarterly Review">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewQuarterlyReview" OnClick="LinkButtonNewQuarterlyReview_OnClick"
                                    ToolTip="Add new quarterly review" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewQuarterlyReview" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewQuaterlyReview" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditQuarterlyReview" ToolTip="Edit end of contract details"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditQuarterlyReview" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditQuarterlyReview" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteQuarterlyReview" CausesValidation="false" ToolTip="Delete"
                                    runat="server">
                                    <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteQuarterlyReview" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                                <asp:GridView ID="gvQuarterlyReviewDocuments" runat="server" OnRowCommand="gvQuarterlyReviewDocuments_RowCommand"
                                    AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                    AllowSorting="True" PageSize="20" EmptyDataText="No uploaded quarterly review documents available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("QuarterlyReviewID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff Name" />
                                        <asp:BoundField DataField="Year" HeaderText="Year" />
                                        <asp:BoundField DataField="Quarter" HeaderText="Quarter" />
                                        <asp:ButtonField DataTextField="StaffDocumentName" HeaderText="Document Name" CommandName="ViewDocument" />
                                      <%--  <asp:ButtonField HeaderText="Delete" CommandName="DeleteDocument" ItemStyle-Width="4px"
                                            Text="Delete" />--%>
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PanelAddQuarterlyReview" Style="display: none; width: 950px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddQuarterlyReview" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddQuarterlyReviewHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddQuarterlyReviewHeader" runat="server" Text="Add Quarterly Review"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddQuarterlyReview" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Quarterly Review Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Year:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlQuarterlyReviewYear" Width="100px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    Quarter:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlQuarterlyReviewQuarter" runat="server">
                                                        <asp:ListItem Text="-Select Quarter-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Quarter 1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Quarter 2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Quarter 3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Quarter 4" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Staff Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlQuarterlyReviewStaffName" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    Quarterly Review Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraised By:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlQuarterlyReviewAppraisedBy" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    Target:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewTarget" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Score:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewScore" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Days Absent / Late:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewDaysAbsent" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sick Leave Days:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewSickLeaveDays" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Date of Appraisal:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDateOfAppraisal" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonDateOfAppraisal" ToolTip="Pick appraisal date" CssClass="date-image"
                                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtDateOfAppraisal_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtDateOfAppraisal" PopupButtonID="ImageButtonDateOfAppraisal"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtDateOfAppraisal_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtDateOfAppraisal"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtDateOfAppraisal_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtDateOfAppraisal_MaskedEditExtender" ControlToValidate="txtDateOfAppraisal"
                                                        CssClass="errorMessage" ErrorMessage="txtDateOfAppraisal_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Document:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="FileUploadQuarterlyReviewAppraisalDocument" runat="server" />
                                                </td>
                                                <td>
                                                    Appraisal Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuarterlyReviewAppraisalComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldQuarterlyReviewID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddQuarterlyReviewPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveQuarterlyReview" ToolTip="Save ruartely review" runat="server">
                                            <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Quarterly Review</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddQuarterlyReview" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddQuarterlyReviewPopup" PopupControlID="PanelAddQuarterlyReview"
                                CancelControlID="ImageButtonCloseAddQuarterlyReview" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelAnnualAppraisal" runat="server" CssClass="ajax-tab" HeaderText="Annual Appraisal">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewAnnualAppraisal" OnClick="LinkButtonNewAnnualAppraisal_OnClick"
                                    ToolTip="Add new annual appraisal" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewAnnualAppraisal" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewAnnualAppraisal" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditAnnualAppraisal" ToolTip="Edit end of contract details"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditAnnualAppraisal" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditAnnualAppraisal" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteAnnualAppraisal" CausesValidation="false" ToolTip="Delete"
                                    runat="server">
                                    <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteAnnualAppraisal" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                            </div>
                            <asp:Panel ID="PanelAddAnnualAppraisal" Style="display: none; width: 950px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddAnnualAppraisal" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddAnnualAppraisalHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddAnnualAppraisalHeader" runat="server" Text="Add Annual Appraisal"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddAnnualAppraisal" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>Annual Appraisal Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Year:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAnnualAppraisalYear" Width="100px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    Staff Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAnnualAppraisalStaffName" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Annual Appraisal Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalName" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Appraised By:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAnnualAppraisalAppraisedBy" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Target:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalTarget" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Score:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalScore" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Days Absent / Late:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalDaysAbsent" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Sick Leave Days:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalSickLeaveDays" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date of Appraisal:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalDate" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonAnnualAppraisalDate" ToolTip="Pick annual appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtAnnualAppraisalDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtAnnualAppraisalDate"
                                                        PopupButtonID="ImageButtonAnnualAppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtAnnualAppraisalDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtAnnualAppraisalDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtAnnualAppraisalDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtAnnualAppraisalDate_MaskedEditExtender" ControlToValidate="txtAnnualAppraisalDate"
                                                        CssClass="errorMessage" ErrorMessage="txtAnnualAppraisalDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid annual appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                                <td>
                                                    Appraisal Document:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="FileUploadAnnualAppraisalAppraisalDocument" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualAppraisalAppraisalComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="Label1" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldAnnualAppraisalID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddAnnualAppraisalPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveAnnualAppraisal" ToolTip="Save ruartely review" runat="server">
                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Annual Appraisal</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddAnnualAppraisal" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddAnnualAppraisalPopup" PopupControlID="PanelAddAnnualAppraisal"
                                CancelControlID="ImageButtonCloseAddAnnualAppraisal" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelEndOfProbation" runat="server" CssClass="ajax-tab" HeaderText="End of Probation">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewEndOfProbation" OnClick="LinkButtonNewEndOfProbation_OnClick"
                                    ToolTip="Add new end of probation" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewEndOfProbation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewEndOfProbation" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditEndOfProbation" ToolTip="Edit end of contract details"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditEndOfProbation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditEndOfProbation" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteEndOfProbation" CausesValidation="false" ToolTip="Delete"
                                    runat="server">
                                    <asp:Image ID="Image5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteEndOfProbation" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                            </div>
                            <asp:Panel ID="PanelAddEndOfProbation" Style="display: none; width: 650px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddEndOfProbation" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddEndOfProbationHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddEndOfProbationHeader" runat="server" Text="Add End of Probation"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddEndOfProbation" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>End of Probation Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Staff Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEndOfProbationStaffName" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    End of Probation Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfProbationName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Target:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfProbationTarget" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Score:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfProbationScore" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraised By:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEndOfProbationAppraisedBy" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date of Appraisal:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfProbationDate" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonEndOfProbationDate" ToolTip="Pick appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtEndOfProbationDate_CalendarExtender" runat="server"
                                                        Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtEndOfProbationDate" PopupButtonID="ImageButtonEndOfProbationDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtEndOfProbationDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEndOfProbationDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtEndOfProbationDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtEndOfProbationDate_MaskedEditExtender" ControlToValidate="txtEndOfProbationDate"
                                                        CssClass="errorMessage" ErrorMessage="txtEndOfProbationDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Document:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="FileUploadEndOfProbationAppraisalDocument" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfProbationAppraisalComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label3" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldEndOfProbationID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddEndOfProbationPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveEndOfProbation" ToolTip="Save ruartely review" runat="server">
                                            <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save End of Probation</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddEndOfProbation" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddEndOfProbationPopup" PopupControlID="PanelAddEndOfProbation"
                                CancelControlID="ImageButtonCloseAddEndOfProbation" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelEndOfContract" runat="server" CssClass="ajax-tab" HeaderText="End of Contract">
                        <ContentTemplate>
                            <div class="content-menu-bar">
                                <asp:LinkButton ID="LinkButtonNewEndOfContract" OnClick="LinkButtonNewEndOfContact_OnClick"
                                    ToolTip="Add new end of contract" CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageNewEndOfContract" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                    Add New</asp:LinkButton>
                                <asp:Label ID="LabelNewEndOfContract" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonEditEndOfContract" ToolTip="Edit end of contract details"
                                    CausesValidation="false" runat="server">
                                    <asp:Image ID="ImageEditEndOfContract" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                    Edit</asp:LinkButton>
                                <asp:Label ID="LabelEditEndOfContract" runat="server" Text="|"></asp:Label>
                                <asp:LinkButton ID="LinkButtonDeleteEndOfContract" CausesValidation="false" ToolTip="Delete"
                                    runat="server">
                                    <asp:Image ID="Image6" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                    Delete</asp:LinkButton>
                                <asp:Label ID="LabelDeleteEndOfContract" runat="server" Text="|"></asp:Label>
                            </div>
                            <div class="panel-details">
                            </div>
                            <asp:Panel ID="PanelAddEndOfContract" Style="display: none; width: 650px;" runat="server">
                                <div class="popup-header">
                                    <asp:Panel ID="PanelDragAddEndOfContract" runat="server">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Image ID="ImageAddEndOfContractHeader" runat="server" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                                    <asp:Label ID="lbAddEndOfContractHeader" runat="server" Text="Add End of Contract"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddEndOfContract" ImageUrl="~/images/icons/small/Remove.png"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div class="popup-details">
                                    <fieldset>
                                        <legend>End of Contract Details</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    Year:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEndOfContractYear" Width="100px" runat="server">
                                                        <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Staff Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEndOfContractStaffName" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    End of Contract Name:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfContractName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Target:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfContractTarget" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Score:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfContractScore" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraised By:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEndOfContractAppraisedBy" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date of Appraisal:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfContractDate" Width="150px" Enabled="true" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButtonEndOfContractDate" ToolTip="Pick appraisal date"
                                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                    <asp:CalendarExtender ID="txtEndOfContractDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MMM/yyyy" TargetControlID="txtEndOfContractDate" PopupButtonID="ImageButtonEndOfContractDate"
                                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="txtEndOfContractDate_MaskedEditExtender" runat="server"
                                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtEndOfContractDate"
                                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                    <asp:MaskedEditValidator ID="txtEndOfContractDate_MaskedEditValidator" runat="server"
                                                        ControlExtender="txtEndOfContractDate_MaskedEditExtender" ControlToValidate="txtEndOfContractDate"
                                                        CssClass="errorMessage" ErrorMessage="txtEndOfContractDate_MaskedEditValidator"
                                                        InvalidValueMessage="<br>Invalid appraisal date entered" Display="Dynamic">
                                                    </asp:MaskedEditValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Document:<span class="errorMessage">*</span>
                                                </td>
                                                <td>
                                                    <asp:FileUpload ID="FileUploadEndOfContractAppraisalDocument" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appraisal Comments:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndOfContractAppraisalComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="Label4" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div class="linkBtn">
                                        <asp:HiddenField ID="HiddenFieldEndOfContractID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddEndOfContractPopup" runat="server" />
                                        <asp:LinkButton ID="lnkBtnSaveEndOfContract" ToolTip="Save ruartely review" runat="server">
                                            <asp:Image ID="Image11" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save End of Contract</asp:LinkButton>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderAddEndOfContract" RepositionMode="RepositionOnWindowResize"
                                TargetControlID="HiddenFieldAddEndOfContractPopup" PopupControlID="PanelAddEndOfContract"
                                CancelControlID="ImageButtonCloseAddEndOfContract" BackgroundCssClass="modalback"
                                runat="server">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <%-- <asp:Panel ID="PanelAddStaffTargetAndAppraisalDetails" runat="server">
                        <table class="GridViewStyle">
                            <tr class="AltRowStyle">
                                <th>
                                </th>
                                <th>
                                    <b>Target</b>
                                </th>
                                <th>
                                    <b>Actual</b>
                                </th>
                                <th>
                                    <b>Appraised By</b>
                                </th>
                                <th>
                                    <b>Staff Response</b>
                                </th>
                                <th>
                                    <b>Appraisal Form</b>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    Quarter 1:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter1Target" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter1Actual" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlQuarter1AppraisedBy" Width="200px" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter1StaffResponse" Width="320px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnQuarter1FormLink" OnClick="lnkBtnQuarter1FormLink_Click"
                                        runat="server">Form Link</asp:LinkButton>
                                </td>
                            </tr>
                            <tr class="AltRowStyle">
                                <td>
                                    Quarter 2:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter2Target" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter2Actual" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlQuarter2AppraisedBy" Width="200px" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter2StaffResponse" Width="320px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnQuarter2FormLink" OnClick="lnkBtnQuarter2FormLink_Click"
                                        runat="server">Form Link</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Quarter 3:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter3Target" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter3Actual" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlQuarter3AppraisedBy" Width="200px" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter3StaffResponse" Width="320px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnQuarter3FormLink" OnClick="lnkBtnQuarter3FormLink_Click"
                                        runat="server">Form Link</asp:LinkButton>
                                </td>
                            </tr>
                            <tr class="AltRowStyle">
                                <td>
                                    Quarter 4:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter4Target" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter4Actual" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlQuarter4AppraisedBy" Width="200px" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQuarter4StaffResponse" Width="320px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnQuarter4FormLink" OnClick="lnkBtnQuarter4FormLink_Click"
                                        runat="server">Form Link</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbStaffOverall" runat="server" Text="Staff Overall"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtStaffOverallTarget" ReadOnly="true" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtStaffOverallActual" ReadOnly="true" Width="110px" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="AltRowStyle">
                                <td>
                                    Comments:
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtTargetComments" Width="465px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtActualComments" Width="445px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:HiddenField ID="HiddenFieldStaffTargetID" runat="server" />
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                    <asp:Label ID="lbRequiredFields" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div class="linkBtn" style="text-align: right;">
                        <asp:LinkButton ID="lnkBtnSave" OnClick="lnkBtnSave_Click" ToolTip="Save target/appraisal details"
                            runat="server">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                            Save Target/Appraisal Details</asp:LinkButton>
                    </div>--%>
                <asp:Panel ID="PanelAppraisalForm" Style="display: none; width: 550px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAppraisalForm" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAppraisalFormHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/List.png" />
                                        <asp:Label ID="lbAppraisalFormHeader" runat="server" Text="Appraisal Form"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAppraisalForm" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Appraisal Form Details</legend>
                            <table>
                                <tr>
                                    <td valign="top">
                                        Select Appraisal Form:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="FileUploadDocument" ToolTip="Browse for a document" runat="server" />
                                        <br />
                                        <asp:Label ID="lbD" CssClass="doc-info" runat="server" Text="File types allowed: Word and PDF(i.e .doc,.docx,.rtf & .pdf).<br>Maximum file size: 3mb."></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Uploaded Document:
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkBtnUploadedDocumentLink" OnClick="lnkBtnUploadedDocumentLink_OnClick"
                                            runat="server">No uploaded document</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbDocumentsError" runat="server" CssClass="errorMessage" Text=""></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldAppraisalFormQuarter" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAppraisalFormPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnUploadAppraisalForm" OnClick="lnkBtnUploadAppraisalForm_OnClick"
                                ToolTip="Save upload staff appraisal form" runat="server">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Upload.png"
                                    ImageAlign="AbsMiddle" />
                                Upload Appraisal Form</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAppraisalForm" RepositionMode="None"
                    X="270" Y="80" TargetControlID="HiddenFieldAppraisalFormPopup" PopupControlID="PanelAppraisalForm"
                    CancelControlID="ImageButtonCloseAppraisalForm" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAppraisalForm" TargetControlID="PanelAppraisalForm"
                    DragHandleID="PanelDragAppraisalForm" runat="server"></asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkBtnUploadAppraisalForm" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
