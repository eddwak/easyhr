﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Settings_Module/SettingsModule.master"
    AutoEventWireup="true" CodeBehind="Company_Information.aspx.cs" Inherits="AdeptHRManager.Settings_Module.Company_Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingsModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelCompanyInformation" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelCompanyInformation" CssClass="panel-details" runat="server">
                <table>
                    <tr>
                        <td>
                            Company Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                            <asp:HiddenField ID="HiddenFieldCompanyID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            HQ Address:
                        </td>
                        <td>
                            <asp:TextBox ID="txtHQAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Location:
                        </td>
                        <td>
                            <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Country:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCountry" runat="server">
                                <asp:ListItem Value="0">-Select Country-</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div class="linkBtn">
                                <asp:LinkButton ID="lnkBtnSave" OnClick="lnkBtnSave_Click" ToolTip="Save company information"
                                    runat="server">
                                    <asp:Image ID="ImageSave" runat="server" ImageUrl="~/images/icons/Small/Save.png"
                                        ImageAlign="AbsMiddle" />
                                    Save Company Information</asp:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
