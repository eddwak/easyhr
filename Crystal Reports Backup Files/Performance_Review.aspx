﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Performance_Review.aspx.cs" Inherits="AdeptHRManager.User_Module.Performance_Review" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelPerformanceReview" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelPerformanceReview" runat="server">
                <div class="panel-details">
                    <table>
                        <tr>
                            <td>
                                Performance Year:<span class="errorMessage">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAnnualPerformancePlanYear" OnSelectedIndexChanged="ddlAnnualPerformancePlanYear_SelectedIndexChanged"
                                    AutoPostBack="true" Width="100px" runat="server">
                                    <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Download Template:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAppraisalTemplates" OnSelectedIndexChanged="ddlAppraisalTemplates_SelectedIndexChanged"
                                    AutoPostBack="true" runat="server">
                                    <asp:ListItem Text="--" Value="0" Selected="true"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:TabContainer ID="TabContainerPerformanceReview" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="1">
                    <asp:TabPanel ID="TabPanelPerformancePlan" runat="server" CssClass="ajax-tab" HeaderText="Performance Plan">
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:Panel ID="PanelPerformanceObjectiveListing" Visible="true" runat="server">
                                    <asp:Panel ID="PanelPerformanceObjectiveMenu" Visible="true" runat="server">
                                        <div class="content-menu-bar">
                                            <asp:LinkButton ID="LinkButtonNewPerformanceObjective" OnClick="LinkButtonNewPerformanceObjective_Click"
                                                ToolTip="Add performance objective" CausesValidation="false" runat="server">
                                                <asp:Image ID="ImageNewPerformanceObjective" runat="server" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Images/icons/Small/Create.png" />
                                                Add Performance Objective</asp:LinkButton>
                                            <asp:Label ID="LabelNewPerformanceObjective" runat="server" Text="|"></asp:Label>
                                            <asp:LinkButton ID="LinkButtonEditPerformanceObjective" OnClick="LinkButtonEditPerformanceObjective_Click"
                                                ToolTip="Edit perfomance objective" CausesValidation="false" runat="server">
                                                <asp:Image ID="ImageEditPerformanceObjective" runat="server" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Images/icons/Small/note_edit.png" />
                                                Edit</asp:LinkButton>
                                            <asp:Label ID="LabeEditPerformanceObjective" runat="server" Text="|"></asp:Label>
                                            <asp:LinkButton ID="LinkButtonDeletePerformanceObjective" OnClick="LinkButtonDeletePerformanceObjective_Click"
                                                CausesValidation="false" ToolTip="Delete perfomance objective" runat="server">
                                                <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                                Delete</asp:LinkButton>
                                            <asp:Label ID="LabelDeletePerformanceObjective" runat="server" Text="|"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="PanelStaffToAppraiseHeader" Visible="false" runat="server">
                                        <div class="content-menu-bar">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 97%;">
                                                        <asp:Label ID="lbStaffToAppraiseHeader" runat="server" Text="Performance Objective Plan And Achieved for xxxx"></asp:Label>
                                                        <asp:HiddenField ID="HiddenFieldStaffToAppraiseID" runat="server" />
                                                        Select Quarter to Review:
                                                        <asp:DropDownList ID="ddlStaffQuarterToReview" OnSelectedIndexChanged="ddlStaffQuarterToReview_SelectedIndexChanged"
                                                            AutoPostBack="true" Width="150px" runat="server">
                                                            <asp:ListItem Text="----" Value="0" Selected="true"></asp:ListItem>
                                                            <asp:ListItem Text="Quarter 1" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Quarter 2" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Quarter 3" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="Quarter 4" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonCloseStaffToAppraiseDetails" OnClick="ImageButtonCloseStaffToAppraiseDetails_Click"
                                                            ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:GridView ID="gvPerformanceObjectives" OnRowCommand="gvPerformanceObjectives_RowCommand"
                                        ShowFooter="true" OnRowDataBound="gvPerformanceObjectives_RowDataBound" runat="server"
                                        AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                        AllowSorting="True" PageSize="20" EmptyDataText="No performance objective available!">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("PerformanceObjectiveID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PerformanceObjectiveName" HeaderText="Performance Objective" />
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" SortExpression="PerformanceObjectiveWeight">
                                                <HeaderTemplate>
                                                    % Weight
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelWeight" runat="server" Text='<%# Bind("PerformanceObjectiveWeight") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="LabelTotalWeight" runat="server" Text="Label"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField HeaderText="" CommandName="LoadPerformanceObjectiveKPIs" Text="KPIs" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddPerformanceObjective" Style="display: none; width: 600px;"
                                    runat="server">
                                    <div class="popup-header">
                                        <asp:Panel ID="PanelDragAddPerformanceObjective" runat="server">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 97%;">
                                                        <asp:Image ID="ImageAddPerformanceObjectiveHeader" runat="server" ImageAlign="AbsMiddle"
                                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                                        <asp:Label ID="lbAddPerformanceObjectiveHeader" runat="server" Text="Add Performance Objective"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonCloseAddPerformanceObjective" ImageUrl="~/images/icons/small/Remove.png"
                                                            ToolTip="Close" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                    <div class="popup-details">
                                        <fieldset>
                                            <legend>Performance Objective Details</legend>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Performance Objective:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPerformanceObjective" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Weight:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPerformanceObjectiveWeight" Width="100px" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Image ID="ImageError" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="lbError" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lbInfo" CssClass="info-message" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                        <div class="linkBtn">
                                            <asp:HiddenField ID="HiddenFieldPerformanceObjectiveID" runat="server" />
                                            <asp:HiddenField ID="HiddenFieldAddPerformanceObjectivePopup" runat="server" />
                                            <asp:LinkButton ID="lnkBtnSavePerformanceObjective" OnClick="lnkBtnSavePerformanceObjective_Click"
                                                ToolTip="Save performance objective" runat="server">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                Save</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnUpdatePerformanceObjective" OnClick="lnkBtnUpdatePerformanceObjective_Click"
                                                CausesValidation="false" ToolTip="Update performance objective" Enabled="false"
                                                runat="server">
                                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                    ImageAlign="AbsMiddle" />
                                                Update</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnClearPerformanceObjective" OnClick="lnkBtnClearPerformanceObjective_Click"
                                                CausesValidation="false" ToolTip="Clear" runat="server">
                                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                    ImageAlign="AbsMiddle" />
                                                Clear</asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:ModalPopupExtender ID="ModalPopupExtenderAddPerformanceObjective" RepositionMode="RepositionOnWindowResize"
                                    TargetControlID="HiddenFieldAddPerformanceObjectivePopup" PopupControlID="PanelAddPerformanceObjective"
                                    CancelControlID="ImageButtonCloseAddPerformanceObjective" BackgroundCssClass="modalback"
                                    runat="server">
                                </asp:ModalPopupExtender>
                                <%-- <asp:DragPanelExtender ID="DragPanelExtenderAddPerformanceObjective"  TargetControlID="PanelAddPerformanceObjective"
                                    DragHandleID="PanelDragAddPerformanceObjective" runat="server"></asp:DragPanelExtender>--%>
                                <asp:Panel ID="PanelAddPerformanceObjectiveKPI" Visible="false" runat="server">
                                    <div class="content-menu-bar">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Label ID="lbAddPerformanceObjectiveKPIHeader" runat="server" Text="Add Performance Objective KPI"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseAddPerformanceObjectiveKPI" OnClick="lnkBtnClosePerformanceObjectiveKPIs_Click"
                                                        ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="content-menu-bar">
                                        <asp:LinkButton ID="LinkButtonNewKPI" OnClick="LinkButtonNewKPI_Click" ToolTip="Add KPI"
                                            CausesValidation="false" runat="server">
                                            <asp:Image ID="ImageNewKPI" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                                            Add KPI</asp:LinkButton>
                                        <asp:Label ID="LabelNewKPI" runat="server" Text="|"></asp:Label>
                                        <asp:LinkButton ID="LinkButtonEditKPI" OnClick="LinkButtonEditKPI_Click" ToolTip="Edit KPI"
                                            CausesValidation="false" runat="server">
                                            <asp:Image ID="ImageEditKPI" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                                            Edit KPI</asp:LinkButton>
                                        <asp:Label ID="LabeEditKPI" runat="server" Text="|"></asp:Label>
                                        <asp:LinkButton ID="LinkButtonDeleteKPI" OnClick="LinkButtonDeleteKPI_Click" CausesValidation="false"
                                            ToolTip="Delete KPI" runat="server">
                                            <asp:Image ID="ImageDeleteKPI" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                                            Delete</asp:LinkButton>
                                        <asp:Label ID="LabelDeleteKPI" runat="server" Text="|"></asp:Label>
                                    </div>
                                    <div class="panel-details">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    Performance Objective:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualPerformanceObjective" ReadOnly="true" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    Percentage Weight:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnnualPerformanceObjectiveWeight" ReadOnly="true" Width="100px"
                                                        runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvPerformanceObjectiveKPIs" ShowFooter="true" runat="server" AllowPaging="True"
                                            Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                                            PageSize="20" EmptyDataText="No key performance indicators set!">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1+"." %>
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("PerformanceObjectiveKPIID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="4px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="4px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PerformanceKPI" HeaderText="KPI" />
                                                <asp:BoundField DataField="TargetMeasure" HeaderText="Target Measure" />
                                                <asp:BoundField DataField="AnnualTarget" HeaderText="Annual Target" />
                                                <asp:BoundField DataField="Quarter1Target" HeaderText="Quarter 1 Target" />
                                                <asp:BoundField DataField="Quarter2Target" HeaderText="Quarter 2 Target" />
                                                <asp:BoundField DataField="Quarter3Target" HeaderText="Quarter 3 Target" />
                                                <asp:BoundField DataField="Quarter4Target" HeaderText="Quarter 4 Target" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <FooterStyle CssClass="FooterStyle" />
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddKPI" Style="display: none; width: 600px;" runat="server">
                                    <div class="popup-header">
                                        <asp:Panel ID="PanelDragAddKPI" runat="server">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 97%;">
                                                        <asp:Image ID="ImageAddKPIHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                                        <asp:Label ID="lbAddKPIHeader" runat="server" Text="Add Key Performance Indicator"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonCloseAddKPI" ImageUrl="~/images/icons/small/Remove.png"
                                                            ToolTip="Close" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                    <div class="popup-details">
                                        <fieldset>
                                            <legend>KPI Details</legend>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Key Performance Indicator:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIName" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Target Measure:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlKPITargetMeasure" OnSelectedIndexChanged="ddlKPITargetMeasure_SelectedIndexChanged"
                                                            AutoPostBack="true" runat="server">
                                                            <asp:ListItem Text="-Select Target Measure-" Value="0" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Amount/Currency" Value="Amount"></asp:ListItem>
                                                            <asp:ListItem Text="Date" Value="Date"></asp:ListItem>
                                                            <asp:ListItem Text="Number" Value="Number"></asp:ListItem>
                                                            <asp:ListItem Text="Percentage" Value="Percentage"></asp:ListItem>
                                                            <asp:ListItem Text="Time(Hours)" Value="Time"></asp:ListItem>
                                                            <asp:ListItem Text="Other Naration" Value="Other"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Annual Target:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIAnnualTarget" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtKPIAnnualTargetDate" Width="180px" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageButtonKPIAnnualTargetDate" Visible="false" ToolTip="Pick annual target date"
                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                        <asp:CalendarExtender ID="txtKPIAnnualTargetDate_CalendarExtender" runat="server"
                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtKPIAnnualTargetDate"
                                                            PopupButtonID="ImageButtonKPIAnnualTargetDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                        <asp:MaskedEditExtender ID="txtKPIAnnualTargetDate_MaskedEditExtender" runat="server"
                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtKPIAnnualTargetDate"
                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                        <asp:MaskedEditValidator ID="txtKPIAnnualTargetDate_MaskedEditValidator" runat="server"
                                                            ControlExtender="txtKPIAnnualTargetDate_MaskedEditExtender" ControlToValidate="txtKPIAnnualTargetDate"
                                                            CssClass="errorMessage" ErrorMessage="txtKPIAnnualTargetDate_MaskedEditValidator"
                                                            InvalidValueMessage="<br>Invalid annual target date entered" Display="Dynamic">
                                                        </asp:MaskedEditValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quarter 1 Target:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIQuarter1Target" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtKPIQuarter1TargetDate" Width="180px" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageButtonKPIQuarter1TargetDate" Visible="false" ToolTip="Pick quarter 1 target date"
                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                        <asp:CalendarExtender ID="txtKPIQuarter1TargetDate_CalendarExtender" runat="server"
                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtKPIQuarter1TargetDate"
                                                            PopupButtonID="ImageButtonKPIQuarter1TargetDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                        <asp:MaskedEditExtender ID="txtKPIQuarter1TargetDate_MaskedEditExtender" runat="server"
                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtKPIQuarter1TargetDate"
                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                        <asp:MaskedEditValidator ID="txtKPIQuarter1TargetDate_MaskedEditValidator" runat="server"
                                                            ControlExtender="txtKPIQuarter1TargetDate_MaskedEditExtender" ControlToValidate="txtKPIQuarter1TargetDate"
                                                            CssClass="errorMessage" ErrorMessage="txtKPIQuarter1TargetDate_MaskedEditValidator"
                                                            InvalidValueMessage="<br>Invalid quarter 1 target date entered" Display="Dynamic">
                                                        </asp:MaskedEditValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quarter 2 Target:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIQuarter2Target" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtKPIQuarter2TargetDate" Width="180px" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageButtonKPIQuarter2TargetDate" Visible="false" ToolTip="Pick quarter 2 target date"
                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                        <asp:CalendarExtender ID="txtKPIQuarter2TargetDate_CalendarExtender" runat="server"
                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtKPIQuarter2TargetDate"
                                                            PopupButtonID="ImageButtonKPIQuarter2TargetDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                        <asp:MaskedEditExtender ID="txtKPIQuarter2TargetDate_MaskedEditExtender" runat="server"
                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtKPIQuarter2TargetDate"
                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                        <asp:MaskedEditValidator ID="txtKPIQuarter2TargetDate_MaskedEditValidator" runat="server"
                                                            ControlExtender="txtKPIQuarter2TargetDate_MaskedEditExtender" ControlToValidate="txtKPIQuarter2TargetDate"
                                                            CssClass="errorMessage" ErrorMessage="txtKPIQuarter2TargetDate_MaskedEditValidator"
                                                            InvalidValueMessage="<br>Invalid quarter 2 target date entered" Display="Dynamic">
                                                        </asp:MaskedEditValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quater 3 Target:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIQuarter3Target" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtKPIQuarter3TargetDate" Width="180px" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageButtonKPIQuarter3TargetDate" Visible="false" ToolTip="Pick quarter 3 target date"
                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                        <asp:CalendarExtender ID="txtKPIQuarter3TargetDate_CalendarExtender" runat="server"
                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtKPIQuarter3TargetDate"
                                                            PopupButtonID="ImageButtonKPIQuarter3TargetDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                        <asp:MaskedEditExtender ID="txtKPIQuarter3TargetDate_MaskedEditExtender" runat="server"
                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtKPIQuarter3TargetDate"
                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                        <asp:MaskedEditValidator ID="txtKPIQuarter3TargetDate_MaskedEditValidator" runat="server"
                                                            ControlExtender="txtKPIQuarter3TargetDate_MaskedEditExtender" ControlToValidate="txtKPIQuarter3TargetDate"
                                                            CssClass="errorMessage" ErrorMessage="txtKPIQuarter3TargetDate_MaskedEditValidator"
                                                            InvalidValueMessage="<br>Invalid quarter 3 target date entered" Display="Dynamic">
                                                        </asp:MaskedEditValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quarter 4 Target:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKPIQuarter4Target" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtKPIQuarter4TargetDate" Width="180px" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="ImageButtonKPIQuarter4TargetDate" Visible="false" ToolTip="Pick quarter 4 target date"
                                                            CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                        <asp:CalendarExtender ID="txtKPIQuarter4TargetDate_CalendarExtender" runat="server"
                                                            Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtKPIQuarter4TargetDate"
                                                            PopupButtonID="ImageButtonKPIQuarter4TargetDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                        <asp:MaskedEditExtender ID="txtKPIQuarter4TargetDate_MaskedEditExtender" runat="server"
                                                            ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                            CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtKPIQuarter4TargetDate"
                                                            UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                        <asp:MaskedEditValidator ID="txtKPIQuarter4TargetDate_MaskedEditValidator" runat="server"
                                                            ControlExtender="txtKPIQuarter4TargetDate_MaskedEditExtender" ControlToValidate="txtKPIQuarter4TargetDate"
                                                            CssClass="errorMessage" ErrorMessage="txtKPIQuarter4TargetDate_MaskedEditValidator"
                                                            InvalidValueMessage="<br>Invalid quarter 4 target date entered" Display="Dynamic">
                                                        </asp:MaskedEditValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                                        <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="Label3" CssClass="errorMessage" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="Label4" CssClass="info-message" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                        <div class="linkBtn">
                                            <asp:HiddenField ID="HiddenFieldKPIID" runat="server" />
                                            <asp:HiddenField ID="HiddenFieldAddKPIPopup" runat="server" />
                                            <asp:LinkButton ID="lnkBtnSaveKPI" OnClick="lnkBtnSaveKPI_Click" ToolTip="Save KPI"
                                                runat="server">
                                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                Save</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnUpdateKPI" OnClick="lnkBtnUpdateKPI_Click" CausesValidation="false"
                                                ToolTip="Update KPI" Enabled="false" runat="server">
                                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                                    ImageAlign="AbsMiddle" />
                                                Update</asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnClearKPI" OnClick="lnkBtnClearKPI_Click" CausesValidation="false"
                                                ToolTip="Clear" runat="server">
                                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                                    ImageAlign="AbsMiddle" />
                                                Clear</asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:ModalPopupExtender ID="ModalPopupExtenderAddKPI" RepositionMode="RepositionOnWindowResize"
                                    TargetControlID="HiddenFieldAddKPIPopup" PopupControlID="PanelAddKPI" CancelControlID="ImageButtonCloseAddKPI"
                                    BackgroundCssClass="modalback" runat="server">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="PanelStaffPerformancePlanQuarteryReview" Visible="false" runat="server">
                                    <div class="content-menu-bar">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 97%;">
                                                    <asp:Label ID="lbReviewStaffQuarterHeader" runat="server" Text="Review Staff Quarter xxxx"></asp:Label>
                                                    <asp:HiddenField ID="HiddenFieldQuarter" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonCloseReviewStaffQuarter" OnClick="ImageButtonCloseReviewStaffQuarter_Click"
                                                        ImageUrl="~/images/icons/small/Remove.png" ToolTip="Close" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <asp:GridView ID="gvStaffQuarterlyPerformanceReview" runat="server" AllowPaging="True"
                                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                                        PageSize="10" EmptyDataText="No perfromance details available!">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1+"." %>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("PerformancePlanID") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="4px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PerformanceObjective" HeaderText="Performance Objective" />
                                            <asp:TemplateField HeaderText="Annual Key Performance Indicator">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblResponse" runat="server" Text='<%# "<pre>"+ Eval("KeyPerformanceIndicators")+"</pre>"  %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PercentageWeight" HeaderStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" HeaderText="% Weight" />
                                            <asp:BoundField DataField="QuarterTarget" HeaderText="KPI Quarter Target" HtmlEncode="False" />
                                            <asp:BoundField DataField="QuarterAchieved" HeaderText="Actual Achieved" HtmlEncode="False" />
                                            <asp:BoundField DataField="QuarterKPIRating" HeaderText="Rating" HtmlEncode="False" />
                                            <asp:BoundField DataField="AverageObjectiveRating" HeaderText="Average Rating" HeaderStyle-HorizontalAlign="Right"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="QuarterComment" HeaderText="Comment" />
                                            <asp:BoundField DataField="NextQuarterActionPlan" HeaderText="Action Plan fo Next Quarter" />
                                        </Columns>
                                        <FooterStyle CssClass="PagerStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                    <table>
                                        <tr>
                                            <td>
                                                Overall Average Rating:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQuarterOverallAverageRating" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Appraisal Date:<span class="errorMessage">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQuarterAppraisalDate" Width="180px" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButtonQuarterAppraisalDate" ToolTip="Pick quarter date"
                                                    CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                                <asp:CalendarExtender ID="txtQuarterAppraisalDate_CalendarExtender" runat="server"
                                                    Enabled="True" Format="dd/MMM/yyyy" TargetControlID="txtQuarterAppraisalDate"
                                                    PopupButtonID="ImageButtonQuarterAppraisalDate" PopupPosition="TopRight"></asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="txtQuarterAppraisalDate_MaskedEditExtender" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                    CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtQuarterAppraisalDate"
                                                    UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                                <asp:MaskedEditValidator ID="txtQuarterAppraisalDate_MaskedEditValidator" runat="server"
                                                    ControlExtender="txtQuarterAppraisalDate_MaskedEditExtender" ControlToValidate="txtQuarterAppraisalDate"
                                                    CssClass="errorMessage" ErrorMessage="txtQuarterAppraisalDate_MaskedEditValidator"
                                                    InvalidValueMessage="<br>Invalid quarter appraisal date entered" Display="Dynamic">
                                                </asp:MaskedEditValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Appraiser's Comments:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQuarterAppraiserComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <div class="linkBtn">
                                                    <asp:LinkButton ID="lnkBtnSaveQuarterAppraisalDetails" OnClick="lnkBtnSaveQuarterAppraisalDetails_Click"
                                                        ToolTip="Save appraisal details" runat="server">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                        Save Appraisal Details</asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelQuarterlyReview" runat="server" CssClass="ajax-tab" HeaderText="Quarterly Review">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table>
                                    <tr>
                                        <td>
                                            Quarter Under Review:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlQuarterlyReviewQuarter" OnSelectedIndexChanged="ddlQuarterlyReviewQuarter_SelectedIndexChanged"
                                                AutoPostBack="true" Width="150px" runat="server">
                                                <asp:ListItem Text="-Select Quarter-" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Quarter 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:TabContainer ID="TabContainerQuarterlyReview" CssClass="ajax-tab" runat="server"
                                ActiveTabIndex="0">
                                <asp:TabPanel ID="TabPanelQuarterlyReviewDetails" runat="server" CssClass="ajax-tab"
                                    HeaderText="Quarter Review">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelStaffQuarterlyReviewDetails" Visible="false" CssClass="panel-details"
                                            runat="server">
                                            <asp:GridView ID="gvQuarterlyReview" runat="server" AllowPaging="True" Width="100%"
                                                AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" OnRowDataBound="gvQuarterlyReview_RowDataBound"
                                                PageSize="30" EmptyDataText="No quarterly review details available!">
                                                <Columns>
                                       
                                                    <asp:BoundField DataField="PerformanceObjectiveID" ItemStyle-Font-Bold="true" HeaderText="" />
                                                    <asp:BoundField DataField="PerformanceObjectiveName" ItemStyle-Font-Bold="true" HeaderText="Performance Objective" />
                                                    <asp:BoundField DataField="PerformanceObjectiveWeight" ItemStyle-Font-Bold="true"
                                                        HeaderText="% Weight" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-Width="50px" />
                                                    <asp:BoundField DataField="PerformanceKPI" HeaderText="KPI" />
                                                    <asp:TemplateField HeaderText="Target Measure">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HiddenFieldPerformanceObjectiveID" runat="server" Value='<%# Bind("PerformanceObjectiveID") %>' />
                                                            <asp:HiddenField ID="HiddenFieldPerformanceObjectiveKPIID" runat="server" Value='<%# Bind("PerformanceObjectiveKPIID") %>' />
                                                            <asp:Label ID="LabelTargetMeasure" runat="server" Text='<%# Bind("TargetMeasure") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AnnualTarget" HeaderText="Annual Target" />
                                                    <asp:BoundField DataField="QuarterTarget" HeaderText="QuarterTarget" />
                                                    <asp:TemplateField HeaderText="Actual Achieved">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQuarterAchieved" runat="server" Text='<%# Bind("QuarterAchieved") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rating">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HiddenFieldQuarterStaffRating" runat="server" Value='<%# Bind("QuarterStaffRating") %>' />
                                                            <asp:DropDownList ID="ddlQuarterStaffRating" Width="50px" runat="server">
                                                                <asp:ListItem Text="--" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQuarterStaffComment" runat="server" Text='<%# Bind("QuarterStaffComment") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action Plan for Next Quarter">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtNextQuarterActionPlan" runat="server" Text='<%# Bind("NextQuarterActionPlan") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                            <p>
                                                Comments<span class="errorMessage">*</span>
                                                <asp:TextBox ID="txtQuarterlyReviewStaffComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                <div class="linkBtn">
                                                    <asp:HiddenField ID="HiddenFieldStaffQuarterlyReviewID" runat="server" />
                                                    <asp:LinkButton ID="lnkBtnSaveStaffQuarteryReview" OnClick="lnkBtnSaveStaffQuarteryReview_Click"
                                                        ToolTip="Save " runat="server">Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnSubmitStaffQuarterlyReview" CausesValidation="false" ToolTip="Submit for review"
                                                        Enabled="false" runat="server">
                                                        Submit for Review</asp:LinkButton>
                                                </div>
                                            </p>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel ID="TabPanelQuarterlyReviewDocuments" runat="server" CssClass="ajax-tab"
                                    HeaderText="Documents">
                                    <ContentTemplate>
                                        <div class="panel-details">
                                            <table>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Upload Form:<span class="errorMessage">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:FileUpload ID="FileUploadQuarterlyReviewForm" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:HiddenField ID="HiddenFieldQuarterlyReviewFormID" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div class="linkBtn">
                                                            <asp:LinkButton ID="lnkBtnUploadQuarterlyReviewForm" OnClick="lnkBtnUploadQuarterlyReviewForm_OnClick"
                                                                ToolTip="Upload quarterly form" runat="server">Upload Quarterly Form</asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr />
                                            <asp:GridView ID="gvQuarterlyReviewDocuments" runat="server" OnRowCommand="gvQuarterlyReviewDocuments_RowCommand"
                                                AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                                AllowSorting="True" PageSize="20" EmptyDataText="No uploaded quarterly review documents available!">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1+"." %>
                                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("QuarterlyReviewID") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="4px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Year" HeaderText="Year" />
                                                    <asp:BoundField DataField="Quarter" HeaderText="Quarter" />
                                                    <asp:ButtonField DataTextField="StaffDocumentName" HeaderText="Document Name" CommandName="ViewDocument" />
                                                    <asp:ButtonField HeaderText="Delete" CommandName="DeleteDocument" ItemStyle-Width="4px"
                                                        Text="Delete" />
                                                </Columns>
                                                <FooterStyle CssClass="PagerStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelAnnualAppraisal" runat="server" CssClass="ajax-tab" HeaderText="Annual Appraisal">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table>
                                    <tr>
                                        <td>
                                            Year:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAnnualAppraisalYear" Width="100px" runat="server">
                                                <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Upload Form:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="FileUploadAnnualAppraisalForm" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="HiddenFieldAnnualAppraisalFormID" runat="server" />
                                        </td>
                                        <td>
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnUploadAnnualAppraisalForm" ToolTip="Upload annual appraisal form"
                                                    runat="server">
                                                    Upload Annual Appraisal Form</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelEndOfProbation" runat="server" CssClass="ajax-tab" HeaderText="End of Probation">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table>
                                    <tr>
                                        <td>
                                            Upload Form:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="FileUploadEndOfProbationForm" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="HiddenFieldEndOfProbationFormID" runat="server" />
                                        </td>
                                        <td>
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnUploadEndOfProbationForm" ToolTip="Upload end of probation form"
                                                    runat="server">Upload End of Probation Form</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelEndOfContract" runat="server" CssClass="ajax-tab" HeaderText="End of Contract">
                        <ContentTemplate>
                            <div class="panel-details">
                                <table>
                                    <tr>
                                        <td>
                                            Year:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlEndOfContractYear" Width="100px" runat="server">
                                                <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Upload Form:<span class="errorMessage">*</span>
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="FileUploadEndOfContractForm" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="HiddenFieldEndOfContractFormID" runat="server" />
                                        </td>
                                        <td>
                                            <div class="linkBtn">
                                                <asp:LinkButton ID="lnkBtnUploadEndOfContractForm" ToolTip="Upload end of contract form"
                                                    runat="server">
                                                  
                                                    Upload End of Contract Form</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelStaffToAppraise" runat="server" CssClass="ajax-tab" HeaderText="Staff To Appraise">
                        <ContentTemplate>
                            <div class="panel-details">
                                <asp:GridView ID="gvStaffToAppraise" OnRowCommand="gvStaffToAppraise_RowCommand"
                                    runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle"
                                    AllowSorting="True" PageSize="15" EmptyDataText="Staff to appraise not available!">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1+"." %>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="4px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="StaffName" HeaderText="Staff Name" />
                                        <asp:BoundField DataField="StaffRef" HeaderText="Employee Reference" />
                                        <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                                        <asp:ButtonField HeaderText="" CommandName="ReviewPerformance" Text="REVIEW PERFORMANCE" />
                                    </Columns>
                                    <FooterStyle CssClass="PagerStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
                <uc:ConfirmMessageBox ID="ucConfirmDeleteKPI" runat="server" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="TabContainerPerformanceReview$TabPanelQuarterlyReview$TabContainerQuarterlyReview$TabPanelQuarterlyReviewDocuments$lnkBtnUploadQuarterlyReviewForm" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
