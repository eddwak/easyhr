﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Leave_Types.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Leave_Types" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent"
    runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaveTypes" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeaveTypes" runat="server">
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new leave type"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit leave type details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonView" OnClick="LinkButtonView_Click" ToolTip="View leave type details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete leave type" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvLeaveTypes" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No leave type records available!">
                        <Columns>
                            <asp:BoundField DataField="SNo" HeaderText="S.No" ItemStyle-Width="4px" />
                            <asp:TemplateField ItemStyle-Width="1px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Checked") %>' />
                                    <asp:HiddenField ID="HFID" runat="server" EnableViewState="False" Value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="LeaveType" HeaderText="Leave Type" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="ApplicableTo" HeaderText="Applicable To" ItemStyle-Width="40px" />
                            <asp:BoundField DataField="MaxDays" HeaderText="Max. Days" ItemStyle-Width="10px"
                                ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="DaysCalc" HeaderText="Days Calc." ItemStyle-Width="60px" />
                            <asp:BoundField DataField="Continous" HeaderText="Continous" ItemStyle-Width="60px" />
                            <asp:BoundField DataField="CarryFwd" HeaderText="Carry Fwd" ItemStyle-Width="40px" />
                            <asp:BoundField DataField="CarriedDays" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                HeaderText="Carried Days" />
                            <asp:BoundField DataField="ReapplicablePerYear" HeaderText="Re-applicable  Per Year"
                                ItemStyle-Width="75px" />
                            <asp:BoundField DataField="CalculateLeaveBalance" HeaderText="Calculate Balance"
                                ItemStyle-Width="60px" />
                            <asp:BoundField DataField="RequiresApproval" HeaderText="Requires Approval" ItemStyle-Width="50px" />
                            <asp:BoundField DataField="HasApplicationLimitTime" HeaderText="Has Limit Time" ItemStyle-Width="55px" />
                            <asp:BoundField DataField="LimitHours" HeaderText="Limit Hours" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="40px" />
                            <asp:BoundField DataField="ApplicableInHours" HeaderText="Applicable in Hours" ItemStyle-Width="55px" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddLeaveType" Style="display: none; width: 945px;" runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddLeaveType" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddLeaveTypeHeader" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddLeaveTypeHeader" runat="server" Text="Add New Leave Type"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddLeaveType" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Leave Type Details </legend>
                            <table>
                                <tr style="white-space: nowrap;">
                                    <td>
                                        Leave Type Name<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLeaveTypeName" CssClass="textBoxDetails" runat="server" Width="170px"></asp:TextBox>
                                    </td>
                                    <td>
                                        Applicable To<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblApplicableTo" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                            <asp:ListItem>Male</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Maximum Leave Days<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaximumLeaveDays" CssClass="textBoxDetails" Width="100px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Leave Days Calculation<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblLeaveDaysType" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem>Calendar</asp:ListItem>
                                            <asp:ListItem>Working</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Is Leave Type Continous?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblIsContinous" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        Carry Foward Leave Days?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblCarryForwardLeaveDays" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="Carried Days"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDaysLimit" CssClass="textBoxDetails" Width="100px" Text="0" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Is Re-Applicable Per Year?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblIsReapplicablePerYear" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr style="white-space: nowrap;">
                                    <td>
                                        Calculate Leave Days Balance?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblCalculateLeaveDaysBalance" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        Requires HR/Manager Approval?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblRequiresHRorManagerApproval" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Has Application Limit Time?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblHasApplicationLimitTime" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label20" runat="server" Text="Limit Hours"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApplicationLimitHours" CssClass="textBoxDetails" Width="100px"
                                            Text="0" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Is Leave Applicable in Hours?<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblIsLeaveApplicableInHours" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                        Description:
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtDescription" CssClass="textAreaDetails" Width="500px" TextMode="MultiLine"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:HiddenField ID="HiddenFieldLeaveTypeID" runat="server" />
                                        <asp:HiddenField ID="HiddenFieldAddLeaveTypePopup" runat="server" />
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnSaveLeaveType" OnClick="lnkBtnSaveLeaveType_Click" ToolTip="Save Job Title"
                                runat="server">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateLeaveType" OnClick="lnkBtnUpdateLeaveType_Click"
                                CausesValidation="false" ToolTip="Update Branch" Enabled="false" runat="server">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearLeaveType" OnClick="lnkBtnClearLeaveType_Click" CausesValidation="false"
                                ToolTip="Clear" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddLeaveType" RepositionMode="None"
                    X="80" Y="20" TargetControlID="HiddenFieldAddLeaveTypePopup" PopupControlID="PanelAddLeaveType"
                    CancelControlID="ImageButtonCloseAddLeaveType" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderAddLeaveType" TargetControlID="PanelAddLeaveType"
                    DragHandleID="PanelDragAddLeaveType" runat="server">
                </asp:DragPanelExtender>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
