﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRManager_Confirmation_Message_Box.ascx.cs"
    Inherits="AdeptHRManager.HRManagerClasses.HRManagerUserControls.HRManager_Confirmation_Message_Box" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="PanelHRManagerConfirmationMessageBox" Style="display: none;" runat="server"
    Width="400px">
    <div class="popup-header">
        <asp:Panel ID="PanelDragHRManagerConfirmationMessageBox" runat="server">
            <table width="100%">
                <tr>
                    <td style="width: 98%;">
                        <asp:Label ID="lbConfirmationMessageBoxHeader" runat="server" Text="Confirmation Message?"></asp:Label>
                    </td>
                    <td style="width: 2%;">
                        <asp:ImageButton ID="ImageCloseConfirmationMessageBox" Style="cursor: pointer;" runat="server"
                            ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Remove.png" ToolTip="Close" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div class="popup-details">
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 10px;">
                    <asp:Image ID="ImageConfirmationMessageBox" runat="server" ImageAlign="AbsMiddle"
                        ImageUrl="~/Images/icons/Medium/Question.png" />
                </td>
                <td>
                    <div style="max-height: 150px; overflow: auto;">
                        <asp:Label ID="lbConfirmationMessageBoxMessage" CssClass="messageBoxLabel" runat="server"
                            Text="Are you sure you want to delete the record?"></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
        <div class="small-linkBtn">
            <asp:LinkButton ID="lnkBtnYes" ToolTip="Yes" OnClick="lnkBtnYes_Click" runat="server">
                     Yes</asp:LinkButton>&nbsp;
            <asp:LinkButton ID="lnkBtnNo" ToolTip="No" runat="server">
                      No</asp:LinkButton>
            <asp:Label ID="lbConfirmationMessageBoxPopup" runat="server"></asp:Label>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtenderHRManagerConfirmationMessageBox" TargetControlID="lbConfirmationMessageBoxPopup"
    PopupControlID="PanelHRManagerConfirmationMessageBox" CancelControlID="ImageCloseConfirmationMessageBox"
    OkControlID="lnkBtnNo" BackgroundCssClass="modalback" runat="server">
</asp:ModalPopupExtender>
<%--<asp:DragPanelExtender ID="DragPanelExtenderHRManagerConfirmationMessageBox" TargetControlID="PanelHRManagerConfirmationMessageBox"
    DragHandleID="PanelDragHRManagerConfirmationMessageBox" runat="server">
</asp:DragPanelExtender>--%>
