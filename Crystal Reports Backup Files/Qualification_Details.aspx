﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Qualification_Details.aspx.cs" Inherits="AdeptHRManager.HR_Module.Qualification_Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelQualificationDetails" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelQualificationDetails" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNewEducationBackground" OnClick="LinkButtonNewEducationBackground_Click"
                        ToolTip="Add education background record" CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNewEducationBackground" runat="server" ImageAlign="AbsMiddle"
                            ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNewEducatoinBackground" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEditEducationBackground" OnClick="LinkButtonEditEducationBackground_Click"
                        ToolTip="Edit education background details" CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEditEducationBackground" runat="server" ImageAlign="AbsMiddle"
                            ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabeEditEducationBackground" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonDeleteEducation" OnClick="LinkButtonDeleteEducationBackground_Click"
                        CausesValidation="false" ToolTip="Delete education background" runat="server">
                        <asp:Image ID="Image10" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                    <asp:Label ID="LabelDeleteEducationBackground" runat="server" Text="|"></asp:Label>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvEducationBackground" runat="server" AllowPaging="True" Width="100%"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="10"
                        EmptyDataText="No qualification details available!" OnPageIndexChanging="gvEducationBackground_PageIndexChanging"
                        OnLoad="gvEducationBackground_OnLoad">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffEducationBackgroundID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="EducationQualificationType" HeaderText="Qualification" />
                            <asp:BoundField DataField="InstitutionName" HeaderText="Institution" />
                            <asp:BoundField DataField="LevelAttained" HeaderText="LevelAttained" />
                            <asp:BoundField DataField="Award" HeaderText="Award" />
                            <asp:BoundField DataField="QualificationStatus" HeaderText="Status" />
                            <asp:BoundField DataField="AwardYear" HeaderText="Award Year" />
                            <asp:BoundField DataField="GradeAttained" HeaderText="Grade/GPA" />
                            <asp:BoundField DataField="ProfessionalMembership" HeaderText="Professional Membership" />
                            <asp:BoundField DataField="IsCertificateSubmitted" HeaderText="Certificate Submitted?" />
                            <asp:BoundField DataField="IsTranscriptSubmitted" HeaderText="Transcript Submitted?" />
                            <asp:BoundField DataField="IsVerificationDone" HeaderText="Verification Done?" />
                            <asp:BoundField DataField="IsAJobRequirement" HeaderText="Is A Job Requirement?" />
                            <asp:BoundField DataField="IsHighestQualification" HeaderText="Is Highest Qualification?" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <asp:Panel ID="PanelAddEducationBackground" Style="display: none; width: 700px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragAddEducationBackground" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageAddEducationBackgroundHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Create.png" />
                                        <asp:Label ID="lbAddEducationBackgroundHeader" runat="server" Text="Add Qualification"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseAddEducationBackground" ImageUrl="~/images/icons/small/Remove.png"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Qualification Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Qualification Type:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblEducationBackgroundQualificationType" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem Value="Academic" Selected="True">Academic</asp:ListItem>
                                            <asp:ListItem Value="Professional">Professional</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Institution Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEducationBackgroundInstitutionName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Level Attained:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEducationBackgroundLevelAttained" runat="server">
                                            <asp:ListItem Text="-Select Attained Level-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Award:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAward" Width="420px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rblEducationBackgroundStatus" RepeatDirection="Horizontal"
                                            runat="server">
                                            <asp:ListItem Value="Completed" Selected="True">Completed</asp:ListItem>
                                            <asp:ListItem Value="Ongoing">Ongoing</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Award Year:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <%--<asp:DropDownList ID="ddlEducationBackgroundCompletionDateMonth" Width="95px" runat="server">
                                            <asp:ListItem Text="-Month-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>--%>
                                        <asp:DropDownList ID="ddlEducationBackgroundCompletionDateYear" Width="100px" runat="server">
                                            <asp:ListItem Text="-Year-" Value="0" Selected="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Grade/GPA:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEducationBackgroundGrade" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Professional Membership:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProfessionalMembership" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Check any of the box below where the situation is true.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbTranscriptSubmitted" Text="Has transcript been submitted?" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbCerificateSubmitted" Text="Has certificate been submitted?" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbVerificationDone" Text="Has verification been done?" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbIsItAJobRequirement" Text="Is it a job requirement?" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="cbIsItHighestQualification" Text="Is it the highest qualification?"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label3" CssClass="errorMessage" runat="server" Text="Fields marked with asterisk(*) are required."></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbEducationBackgroundError" CssClass="errorMessage" runat="server"
                                            Text=""></asp:Label>
                                        <asp:Label ID="lbEducationBackgroundInfo" CssClass="info-message" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <div class="linkBtn">
                            <asp:HiddenField ID="HiddenFieldEducationBackgroundID" runat="server" />
                            <asp:HiddenField ID="HiddenFieldAddEducationBackgroundPopup" runat="server" />
                            <asp:LinkButton ID="lnkBtnSaveEducationBackground" OnClick="lnkBtnSaveEducationBackground_Click"
                                ToolTip="Save education background" runat="server">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnUpdateEducationBackground" OnClick="lnkBtnUpdateEducationBackground_Click"
                                CausesValidation="false" ToolTip="Update education background" Enabled="false"
                                runat="server">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                    ImageAlign="AbsMiddle" />
                                Update</asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnClearEducationBackground" OnClick="lnkBtnClearEducationBackground_Click"
                                CausesValidation="false" ToolTip="Clear" runat="server">
                                <asp:Image ID="Image11" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                    ImageAlign="AbsMiddle" />
                                Clear</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderAddEducationBackground" RepositionMode="RepositionOnWindowResize"
                    TargetControlID="HiddenFieldAddEducationBackgroundPopup" PopupControlID="PanelAddEducationBackground"
                    CancelControlID="ImageButtonCloseAddEducationBackground" BackgroundCssClass="modalback"
                    runat="server">
                </asp:ModalPopupExtender>
                <%-- <asp:DragPanelExtender ID="DragPanelExtenderAddEducationBackground" TargetControlID="PanelAddEducationBackground"
                    DragHandleID="PanelDragAddEducationBackground" runat="server"></asp:DragPanelExtender>--%>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
