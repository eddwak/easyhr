﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Recruitment_Module/RecruitmentModule.master"
    AutoEventWireup="true" CodeBehind="Applicants.aspx.cs" Inherits="AdeptHRManager.Recruitment_Module.Applicants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="RecruitmentModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelApplicants" runat="server">
        <ContentTemplate>
            <div class="panel-details">
                <table>
                    <tr>
                        <td>
                            Applicant Reference:
                        </td>
                        <td>
                            <asp:TextBox ID="txtApplicantReference" Width="150px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            Job Reference:
                        </td>
                        <td>
                            <asp:TextBox ID="txtJobReference" Width="150px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Applicant Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtApplicantName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            Job Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtJobTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <hr />
                <table>
                    <tr>
                        <td>
                            Address:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            Education Background:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date of Birth:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDateOfBirth" runat="server"></asp:TextBox>
                        </td>
                        <td rowspan="3">
                            <asp:TextBox ID="txtEducationBackground" Width="500px" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gender:
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblGender" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Value="Male">Male</asp:ListItem>
                                <asp:ListItem Value="Female">Female</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mobile Number:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            National ID:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNationalID" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            Work Experience:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PIN:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPIN" runat="server"></asp:TextBox>
                        </td>
                        <td rowspan="3">
                            <asp:TextBox ID="txtWorkExperience" Width="500px" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nationality:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlNationality" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email Address:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email Adress 2:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAddress2" runat="server"></asp:TextBox>
                        </td>
                        <td rowspan="3">
                            <div class="div-checkboxlist">
                                <asp:CheckBox ID="cbIsQualified" Text="Qualified?" runat="server" TextAlign="Left" /><br />
                                <asp:CheckBox ID="cbEducation" Text="Education?" runat="server" TextAlign="Left" /><br />
                                <asp:CheckBox ID="cbWorkExperience" Text="Work Experience?" runat="server" TextAlign="Left" /><br />
                                <asp:CheckBox ID="cbPresentation" Text="Presentation?" runat="server" TextAlign="Left" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Attach CV:
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUploadCV" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Attach Cerificate:
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUploadCertificate" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PanelApplicantAdditionalFields" runat="server">
                    <hr />
                    <b>Additional Information:</b>
                    <hr />
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lbReDescription01" Visible="false" runat="server" Text="RE-Description-01:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription01" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription01" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lbReDescription06" Visible="false" runat="server" Text="RE-Description-06:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription06" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription06" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbReDescription02" Visible="false" runat="server" Text="RE-Description-02:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription02" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription02" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lbReDescription07" Visible="false" runat="server" Text="RE-Description-07:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription07" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription07" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbReDescription03" Visible="false" runat="server" Text="RE-Description-03:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription03" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription03" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lbReDescription08" Visible="false" runat="server" Text="RE-Description-08:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription08" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription08" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbReDescription04" Visible="false" runat="server" Text="RE-Description-04:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription04" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription04" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lbReDescription09" Visible="false" runat="server" Text="RE-Description-09:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription09" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription09" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbReDescription05" Visible="false" runat="server" Text="RE-Description-05:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription05" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription05" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lbReDescription10" Visible="false" runat="server" Text="RE-Description-10:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReDescription10" Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="HiddenFieldReDescription10" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Remarks/Comments:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtAdditionalInfoRemarks" TextMode="MultiLine" Width="775px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <div class="linkBtn" style="text-align: right;">
                                    <asp:LinkButton ID="lnkBtnSave" ToolTip="Save" runat="server">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                        Save</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnUpdate" CausesValidation="false" ToolTip="Update" Enabled="false"
                                        runat="server">
                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                            ImageAlign="AbsMiddle" />
                                        Update</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnClear" CausesValidation="false" ToolTip="Clear" runat="server">
                                        <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                            ImageAlign="AbsMiddle" />
                                        Clear</asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
