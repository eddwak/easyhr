﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Leave_and_Attendance/LeaveAndAttendance.master"
    AutoEventWireup="true" CodeBehind="Leave_Approval_Process.aspx.cs" Inherits="AdeptHRManager.Leave_and_Attendance.Leave_Approval_Process" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeaveAndAttendanceModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelLeaveApprovalProcess" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelLeaveApprovalProcess" CssClass="mainPanels" runat="server">
                <div class="popup-header">
                    Leave Approval Process</div>
                <div class="panel-details">
                    <fieldset>
                        <legend runat="server">Leave Approval Process Details </legend>
                        <table>
                            <tr>
                                <td>
                                    Transaction Name<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTransactionName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Approval Level 1<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblApprovalLevel1" OnSelectedIndexChanged="rblApprovalLevel1_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="0">Immediate Manager</asp:ListItem>
                                        <asp:ListItem Value="1">Another Staff</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlApprovalLevel1" Enabled="false" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    Approval Level 2
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblApprovalLevel2" OnSelectedIndexChanged="rblApprovalLevel2_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="0">Next Manager</asp:ListItem>
                                        <asp:ListItem Value="1">Another Staff</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlApprovalLevel2" Enabled="false" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Approval Level 3
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblApprovalLevel3" OnSelectedIndexChanged="rblApprovalLevel3_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="0">Next Manager</asp:ListItem>
                                        <asp:ListItem Value="1">Another Staff</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlApprovalLevel3" Enabled="false" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Optional Approval Level 1
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlOptionalApprovalLevel1" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Optional Approval Level 2
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlOptionalApprovalLevel2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Optional Approval Level 3
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlOptionalApprovalLevel3" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Keep Staff 1 Informed<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlKeepStaff1Informed" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="cblKeepStaff1InformedDuration" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                        <asp:ListItem Text="Always" Value="Always"></asp:ListItem>
                                        <asp:ListItem Text="Daily" Value="Daily"></asp:ListItem>
                                        <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Keep Staff 2 Informed
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlKeepStaff2Informed" OnSelectedIndexChanged="ddlKeepStaff2Informed_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="cblKeepStaff2InformedDuration" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                        <asp:ListItem Text="Always" Value="Always"></asp:ListItem>
                                        <asp:ListItem Text="Daily" Value="Daily"></asp:ListItem>
                                        <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Keep Staff 3 Informed
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlKeepStaff3Informed" OnSelectedIndexChanged="ddlKeepStaff3Informed_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="cblKeepStaff3InformedDuration" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                        <asp:ListItem Text="Always" Value="Always"></asp:ListItem>
                                        <asp:ListItem Text="Daily" Value="Daily"></asp:ListItem>
                                        <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Escalation Alert 1 Time<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEscalationAlert1Time" Width="100px" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label21" runat="server" Text="[Hours]" CssClass="info-message"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Escalation Alert 2 Time
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEscalationAlert2Time" Width="100px" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label18" runat="server" Text="[Hours]" CssClass="info-message"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Escalation Alert 3 Time
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEscalationAlert3Time" Width="100px" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label20" runat="server" Text="[Hours]" CssClass="info-message"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Default Notification Time<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDefaultNotificationTime" Width="100px" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label22" runat="server" Text="[Hours]" CssClass="info-message"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                    <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <div class="linkBtn">
                        <asp:HiddenField ID="HiddenFieldLeaveApprovalProcessID" runat="server" />
                        <asp:LinkButton ID="lnkBtnSaveLeaveApprocalProcess" OnClick="lnkBtnSaveLeaveApprocalProcess_Click"
                            ToolTip="Set leave approval process" runat="server">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                            Set Leave Approval Process</asp:LinkButton>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
