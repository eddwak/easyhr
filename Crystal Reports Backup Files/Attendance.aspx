﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserModule.Master" AutoEventWireup="true"
    CodeBehind="Attendance.aspx.cs" Inherits="AdeptHRManager.User_Module.Attendance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelAttendance" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelAttendance" runat="server">
                <div class="panel-details">
                    <fieldset>
                        <legend>Attendance</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    Between Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFromDate" OnTextChanged="txtFromDate_TextChanged" AutoPostBack="true"
                                        Width="180px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonFromDate" ToolTip="Pick from date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtFromDate" PopupButtonID="ImageButtonFromDate"
                                        PopupPosition="TopRight">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtFromDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtFromDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtFromDate_MaskedEditValidator" runat="server" ControlExtender="txtFromDate_MaskedEditExtender"
                                        ControlToValidate="txtFromDate" CssClass="errorMessage" ErrorMessage="txtFromDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                <td>
                                    To Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtToDate" OnTextChanged="txtFromDate_TextChanged" AutoPostBack="true"
                                        Width="180px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonToDate" ToolTip="Pick to date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="ImageButtonToDate"
                                        PopupPosition="TopRight">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtToDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtToDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtToDate_MaskedEditValidator" runat="server" ControlExtender="txtToDate_MaskedEditExtender"
                                        ControlToValidate="txtToDate" CssClass="errorMessage" ErrorMessage="txtToDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid to date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                <td>
                                    <asp:Button ID="btnViewAllAttendance" OnClick="btnViewAllAttendance_Click" runat="server"
                                        Text="View All Attendance Details" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div class="panel-details">
                    <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                    <asp:HiddenField ID="HiddenFieldEmployeeUnderStaffID" runat="server" />
                    <asp:GridView ID="gvEmployeeAttendanceDetails" runat="server" AllowPaging="True"
                        Width="100%" AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True"
                        OnPageIndexChanging="gvEmployeeAttendanceDetails_PageIndexChanging" OnLoad="gvEmployeeAttendanceDetails_OnLoad"
                        PageSize="15" EmptyDataText="No attendance records available!">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Date" HeaderText="Date" />
                            <asp:BoundField DataField="CheckType" HeaderText="Type" />
                            <asp:BoundField DataField="Time" HeaderText="Time" />
                            <asp:BoundField DataField="CheckPlace" HeaderText="CheckPlace" />
                            <%--<asp:BoundField DataField="MinutesLate" HeaderText="MinutesLate" />--%>
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
