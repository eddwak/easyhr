﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Exit_Interview.aspx.cs" Inherits="AdeptHRManager.HR_Module.Exit_Interview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelExitInterview" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelExitInteview" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelAddExitInterview" runat="server">
                    <div class="panel-details">
                        <table>
                            <tr>
                                <td>
                                    Mode of Exit:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlModeOfExit" runat="server">
                                        <asp:ListItem Value="0">-Select Mode of Exit-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Exit Date:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonExitDate" ToolTip="Pick exit date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtExitDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtExitDate" PopupButtonID="ImageButtonExitDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtExitDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtExitDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtExitDate_MaskedEditValidator" runat="server" ControlExtender="txtExitDate_MaskedEditExtender"
                                        ControlToValidate="txtExitDate" CssClass="errorMessage" ErrorMessage="txtExitDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid exit date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitReason" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Exit Interview Comments
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExitInterviewComments" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Clearance:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtClearance" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Interview By:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlInterviewBy" runat="server">
                                        <asp:ListItem Value="0">-Select Intervieed By-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbExitInterviewSubmitted" Text="Exit interview submitted?" TextAlign="Left"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbClearanceFormSigned" Text="Clearance form fully signed?" TextAlign="Left"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbHandOverReportSubmitted" Text="Handover report submitted?" TextAlign="Left"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Attach Document:
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUploadDocument" runat="server" />
                                    <br />
                                    <asp:Label ID="lbD" CssClass="doc-info" runat="server" Text="File types allowed: Word and PDF(i.e .doc,.docx,.rtf & .pdf).<br>Maximum file size: 3mb."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Uploaded Document:
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkBtnUploadedDocumentLink" OnClick="lnkBtnUploadedDocumentLink_OnClick"
                                        runat="server">No uploaded document</asp:LinkButton>
                                </td>
                                <tr>
                                    <tr>
                                        <td colspan="4">
                                            <table class="GridViewStyle">
                                                <tr class="AltRowStyle">
                                                    <th>
                                                        <b>Asset</b>
                                                    </th>
                                                    <th>
                                                        <b>Returned?</b>
                                                    </th>
                                                    <th>
                                                        <b>Date</b>
                                                    </th>
                                                    <th>
                                                        <b>Comment</b>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Car:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbCarReturned" TextAlign="Left" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCarReturnDate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr class="AltRowStyle">
                                                    <td>
                                                        House:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbHouseReturned" TextAlign="Left" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtHouseReturnDate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Telephone:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbTelephoneReturned" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTelephoneReturnDate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr class="AltRowStyle">
                                                    <td>
                                                        Laptop:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbLaptopReturned" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtLaptopReturnDate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Loan Guarantee:
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbLoanGuranteedReturned" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDateRoneGuranteedRerurnDate" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr class="AltRowStyle">
                                                    <td>
                                                        90% terminal benefits paid?
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        10% terminal benefits paid?
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBox2" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr class="AltRowStyle">
                                                    <td>
                                                        Certificate of service issued?
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBox3" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="linkBtn">
                                                <asp:HiddenField ID="HiddenFieldExitInterviewID" runat="server" />
                                                <asp:LinkButton ID="lnkBtnSaveExitInterview" OnClick="lnkBtnSaveExitInterview_Click"
                                                    ToolTip="Save exit interview" runat="server">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                                    Save Exit Interview</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                        </table>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkBtnSaveExitInterview" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
