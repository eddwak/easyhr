﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Staff_Master.aspx.cs" Inherits="AdeptHRManager.HR_Module.Staff_Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelStaffMaster" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelStaffMaster" runat="server">
                <div class="child-content-header">
                </div>
                <div class="content-menu-bar">
                    <asp:LinkButton ID="LinkButtonNew" PostBackUrl="~/HR_Module/Staff_Record.aspx" ToolTip="Add new staff"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                        Add New</asp:LinkButton>
                    <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                    <asp:LinkButton ID="LinkButtonEdit" OnClick="LinkButtonEdit_Click" ToolTip="Edit staff details"
                        CausesValidation="false" runat="server">
                        <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                        Edit</asp:LinkButton>
                    <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                    <%--  <asp:LinkButton ID="LinkButtonView" ToolTip="View  staffdetails" CausesValidation="false"
                        runat="server">
                        <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                        View</asp:LinkButton>
                    <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>--%>
                    <asp:LinkButton ID="LinkButtonDelete" OnClick="LinkButtonDelete_Click" CausesValidation="false"
                        ToolTip="Delete staff" runat="server">
                        <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                        Delete</asp:LinkButton>
                </div>
                <div class="search-div">
                    Search:
                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged"
                        runat="server"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtSearch" WatermarkText="Search by reference number/name/id number/phone number:"
                        WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                    <asp:AutoCompleteExtender ID="txtSearch_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                        Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx" ServiceMethod="GetStaffRecords"
                        TargetControlID="txtSearch" CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1"
                        CompletionListCssClass="auto-extender" CompletionListItemCssClass="auto-extender-list"
                        CompletionListHighlightedItemCssClass="auto-extender-highlight" CompletionListElementID="divSearchDisplay">
                    </asp:AutoCompleteExtender>
                    <div id="divSearchDisplay" style="max-height: 200px; display: none; overflow: auto;
                        width: 343px !important;">
                    </div>
                </div>
                <div class="panel-details">
                    <asp:GridView ID="gvStaffMaster" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" AllowSorting="True" PageSize="15" EmptyDataText="No staff records found!"
                        OnPageIndexChanging="gvStaffMaster_PageIndexChanging" OnRowCommand="gvStaffMaster_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1+"." %>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("StaffID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="4px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="23px" ItemStyle-Height="26px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" CommandName="ViewStaffDetails" CssClass="gridViewImageDetails"
                                        runat="server" ImageUrl='<%# "~/HRManagerClasses/HRManagerHandler.ashx?staffID=" + Eval("StaffID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:ButtonField DataTextField="StaffName" HeaderText="Staff Name" CommandName="ViewStaffDetails"
                                ItemStyle-CssClass="gridButtons" />--%>
                            <asp:ButtonField DataTextField="StaffRef" HeaderText="Staff No:" CommandName="ViewStaffDetails"
                                ItemStyle-CssClass="gridButtons" />
                            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                            <asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
                            <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                            <asp:BoundField DataField="Department" HeaderText="Department" />
                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
                            <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                            <asp:BoundField DataField="Gender" HeaderText="Gender" />
                        </Columns>
                        <FooterStyle CssClass="PagerStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </div>
                <uc:ConfirmMessageBox ID="ucConfirm" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
