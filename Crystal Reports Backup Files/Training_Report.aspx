﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Training_Module/TrainingModule.master"
    AutoEventWireup="true" CodeBehind="Training_Report.aspx.cs" Inherits="AdeptHRManager.Training_Module.Training_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<%@ Register TagPrefix="uc" TagName="HR_UpdateProgress" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_UpdateProgress.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrainingModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelTrainingReports" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelTrainingReports" runat="server">
                <asp:TabContainer ID="TabContainerTrainingReports" CssClass="ajax-tab" runat="server"
                    ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanelTrainingReports" runat="server" CssClass="ajax-tab" HeaderText="Leave Reports">
                        <HeaderTemplate>
                            Training Reports
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="panel-details">
                                <table class="GridViewStyle">
                                    <tr class="AltRowStyle">
                                        <th>
                                        </th>
                                        <th>
                                            <b>Report Type</b>
                                        </th>
                                        <th>
                                            <b>Description</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="2%">
                                            1.
                                        </td>
                                        <td width="50%">
                                            <asp:LinkButton ID="lnkBtnTrainingsAttendedPerDurationReport" runat="server">Training Attended Per Duration Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays course trainings attended during a specified period.
                                        </td>
                                    </tr>
                                    <tr class="AltRowStyle">
                                        <td>
                                            2.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnSingleStaffTrainingAttendedReport" runat="server">Single Staff Training Attended Report</asp:LinkButton>
                                        </td>
                                        <td>
                                            Displays training attended by a selected single employee
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3.
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnLeaveApplicationsApprovedPerDurationReport" runat="server"></asp:LinkButton>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanelReportViewer" runat="server" CssClass="ajax-tab" HeaderText="Report Viewer">
                        <HeaderTemplate>
                            Report Viewer
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divReportViewer" class="divReportViewerCSS">
                                <CR:CrystalReportViewer ID="CrystalReportViewerTrainingReports" runat="server" AutoDataBind="true"
                                    OnUnload="CrystalReportVieweeTrainingReports_Unload" ToolPanelView="None" HasToggleGroupTreeButton="false"
                                    HasPageNavigationButtons="True" HasDrilldownTabs="False" SeparatePages="False"
                                    BestFitPage="False" GroupTreeStyle-ShowLines="False" PageZoomFactor="100" HasCrystalLogo="False"
                                    PrintMode="Pdf" Enabled="true" ToolbarStyle-CssClass="report-ToolbarStyle" BorderWidth="0px"
                                    ViewStateMode="Enabled" DisplayStatusbar="false" Width="100%" />
                                <CR:CrystalReportSource ID="CrystalReportSourceTrainigReports" runat="server">
                                </CR:CrystalReportSource>
                            </div>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <asp:Panel ID="PanelTrainingsAttendedPerDurationReport" Style="display: none; width: 560px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragTrainingsAttendedPerDurationReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageTrainingsAttendedPerDurationReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbTrainingsAttendedPerDurationReport" runat="server" Text="Trainings Attended Per Duration Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseTrainingsAttendedPerDurationReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Training Attended Between Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFromDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonFromDate" ToolTip="Pick from date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtFromDate" PopupButtonID="ImageButtonFromDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtFromDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtFromDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtFromDate_MaskedEditValidator" runat="server" ControlExtender="txtFromDate_MaskedEditExtender"
                                            ControlToValidate="txtFromDate" CssClass="errorMessage" ErrorMessage="txtFromDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid from date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        To Date:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtToDate" Width="180px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButtonToDate" ToolTip="Pick to date" CssClass="date-image"
                                            ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                        <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="ImageButtonToDate"
                                            PopupPosition="TopRight">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="txtToDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                            Mask="99/LLL/9999" TargetControlID="txtToDate" UserDateFormat="DayMonthYear">
                                        </asp:MaskedEditExtender>
                                        <asp:MaskedEditValidator ID="txtToDate_MaskedEditValidator" runat="server" ControlExtender="txtToDate_MaskedEditExtender"
                                            ControlToValidate="txtToDate" CssClass="errorMessage" ErrorMessage="txtToDate_MaskedEditValidator"
                                            InvalidValueMessage="<br>Invalid to date entered" Display="Dynamic">
                                        </asp:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label2" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress1" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewTrainingsAttendedPerDurationReport" OnClick="lnkBtnViewTrainingsAttendedPerDurationReport_Click"
                                ToolTip="View trainings attended per duration report" runat="server">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderTrainingsAttendedPerDurationReport"
                    RepositionMode="None" X="270" Y="80" TargetControlID="TabContainerTrainingReports$TabPanelTrainingReports$lnkBtnTrainingsAttendedPerDurationReport"
                    PopupControlID="PanelTrainingsAttendedPerDurationReport" CancelControlID="ImageButtonCloseTrainingsAttendedPerDurationReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderTrainingsAttendedPerDurationReport" TargetControlID="PanelTrainingsAttendedPerDurationReport"
                    DragHandleID="PanelDragTrainingsAttendedPerDurationReport" runat="server">
                </asp:DragPanelExtender>
                <asp:Panel ID="PanelSingleStaffTrainingAttendedReport" Style="display: none; width: 500px;"
                    runat="server">
                    <div class="popup-header">
                        <asp:Panel ID="PanelDragSingleStaffTrainingAttendedReport" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 97%;">
                                        <asp:Image ID="ImageSingleStaffTrainingAttendedReportHeader" runat="server" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/icons/Medium/Report.png" />
                                        <asp:Label ID="lbSingleStaffTrainingAttendedReportHeader" runat="server" Text="Single Staff Training Attended Report"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonCloseSingleStaffTrainingAttendedReport" ImageUrl="~/images/icons/small/Remove.png"
                                            ToolTip="Close" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div class="popup-details">
                        <fieldset>
                            <legend>Report Details</legend>
                            <table>
                                <tr>
                                    <td>
                                        Staff Name:<span class="errorMessage">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSingleStaffTrainingAttendedReportStaffName" runat="server">
                                            <asp:ListItem Value="0">Select Staff Name</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Danger.png" />
                                        <asp:Label ID="Label87" runat="server" CssClass="errorMessage" Text="Fields marked with Asterisk (*) are required"></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <uc:HR_UpdateProgress ID="HR_UpdateProgress3" runat="server" />
                        <div class="linkBtn">
                            <asp:LinkButton ID="lnkBtnViewSingleStaffTrainingAttendedReport" OnClick="lnkBtnViewSingleStaffTrainingAttendedReport_Click"
                                ToolTip="View single staff training attended report" runat="server">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons/Small/3d bar chart.png"
                                    ImageAlign="AbsMiddle" />
                                View Report</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="ModalPopupExtenderSingleStaffTrainingAttendedReport"
                    RepositionMode="None" X="270" Y="80" TargetControlID="TabContainerTrainingReports$TabPanelTrainingReports$lnkBtnSingleStaffTrainingAttendedReport"
                    PopupControlID="PanelSingleStaffTrainingAttendedReport" CancelControlID="ImageButtonCloseSingleStaffTrainingAttendedReport"
                    BackgroundCssClass="modalback" runat="server">
                </asp:ModalPopupExtender>
                <asp:DragPanelExtender ID="DragPanelExtenderSingleStaffTrainingAttendedReport" TargetControlID="PanelSingleStaffTrainingAttendedReport"
                    DragHandleID="PanelDragSingleStaffTrainingAttendedReport" runat="server">
                </asp:DragPanelExtender>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
