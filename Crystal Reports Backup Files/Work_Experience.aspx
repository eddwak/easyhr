﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Work_Experience.aspx.cs" Inherits="AdeptHRManager.HR_Module.Work_Experience" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelWorkExperience" runat="server">
        <ContentTemplate>
            <div class="content-menu-bar">
                <asp:LinkButton ID="LinkButtonNew" OnClick="LinkButtonNew_Click" ToolTip="Add new employment record"
                    CausesValidation="false" runat="server">
                    <asp:Image ID="ImageNew" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Create.png" />
                    Add New</asp:LinkButton>
                <asp:Label ID="LabelNew" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonEdit" ToolTip="Edit employment history details" CausesValidation="false"
                    runat="server">
                    <asp:Image ID="ImageEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/note_edit.png" />
                    Edit</asp:LinkButton>
                <asp:Label ID="LabelEdit" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonView" ToolTip="View  employment history details" CausesValidation="false"
                    runat="server">
                    <asp:Image ID="ImageView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/List.png" />
                    View</asp:LinkButton>
                <asp:Label ID="LabelDelete" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="LinkButtonDelete" CausesValidation="false" ToolTip="Delete employment history"
                    runat="server">
                    <asp:Image ID="ImageDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/icons/Small/Erase.png" />
                    Delete</asp:LinkButton>
            </div>
            <div class="panel-details">
                <asp:GridView ID="gvEmploymentHistory" runat="server" AllowPaging="True" Width="100%"
                    AutoGenerateColumns="False" CssClass="GridViewStyle" AllowSorting="True" PageSize="15"
                    EmptyDataText="No employement history available!">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1+"." %>
                            </ItemTemplate>
                            <ItemStyle Width="4px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="StartDate" HeaderText="Start Date" />
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" />
                        <asp:BoundField DataField="EmployerName" HeaderText="Employer's Name" />
                        <asp:BoundField DataField="Position" HeaderText="Position/Title Held" />
                    </Columns>
                    <FooterStyle CssClass="PagerStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                </asp:GridView>
            </div>
            <asp:Panel ID="PanelAddWorkExperience" Style="display: none; width: 550px;" runat="server">
                <div class="popup-header">
                    <asp:Panel ID="PanelDragAddWorkExperience" runat="server">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 97%;">
                                    <asp:Image ID="ImageAddWorkExperienceHeader" runat="server" ImageAlign="AbsMiddle"
                                        ImageUrl="~/Images/icons/Medium/Create.png" />
                                    <asp:Label ID="lbAddNationalityHeader" runat="server" Text="Add Work Experience"></asp:Label>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButtonCloseAddWorkExperience" ImageUrl="~/images/icons/small/Remove.png"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <div class="popup-details">
                    <table>
                        <tr>
                            <td>
                                Employer Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmployeName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Position/Title Held:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPostionHeld" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="HiddenFieldWorkExperienceID" runat="server" />
                                <asp:HiddenField ID="HiddenFieldAddWorkExperiencePopup" runat="server" />
                            </td>
                            <td>
                                <div class="linkBtn">
                                    <asp:LinkButton ID="lnkBtnSave" ToolTip="Save" runat="server">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                        Save</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnUpdate" CausesValidation="false" ToolTip="Update" Enabled="false"
                                        runat="server">
                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icons/Small/Modify.png"
                                            ImageAlign="AbsMiddle" />
                                        Update</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnClear" CausesValidation="false" ToolTip="Clear" runat="server">
                                        <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icons/Small/Delete.png"
                                            ImageAlign="AbsMiddle" />
                                        Clear</asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddWorkExperience" RepositionMode="None"
                X="270" Y="130" TargetControlID="HiddenFieldAddWorkExperiencePopup" PopupControlID="PanelAddWorkExperience"
                CancelControlID="ImageButtonCloseAddWorkExperience" BackgroundCssClass="modalback"
                runat="server">
            </cc1:ModalPopupExtender>
            <cc1:DragPanelExtender ID="DragPanelExtenderAddWorkExperience" TargetControlID="PanelAddWorkExperience"
                DragHandleID="PanelDragAddWorkExperience" runat="server">
            </cc1:DragPanelExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
