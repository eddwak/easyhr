﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdeptHRManager.Master" AutoEventWireup="true"
    CodeBehind="TestImageUploader2.aspx.cs" Inherits="AdeptHRManager.TestImageUploader2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="Scripts/jquery.Jcrop.css" />
    <%--<script src="Scripts/jquery-1.7.1.min.js"></script>--%>
    <%--<script src="Scripts/jquery-1.7.1.min.js"></script>--%>
    <script src="Scripts/jquery.min.js"></script>
    <script src="Scripts/jquery.Jcrop.js"></script>
    <script language="javascript">
        $(document).ready(function () {
            $('#<%=imgUpload.ClientID%>').Jcrop({
                onSelect: SelectCropArea
            });
        });
        function SelectCropArea(c) {
            $('#<%=X.ClientID%>').val(parseInt(c.x));
            $('#<%=Y.ClientID%>').val(parseInt(c.y));
            $('#<%=W.ClientID%>').val(parseInt(c.w));
            $('#<%=H.ClientID%>').val(parseInt(c.h));
        }</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3>
     Photo   Image Upload, Crop & Save using ASP.NET & Jquery</h3>

    <table>
        <tr>
            <td>
                Select Image File :
            </td>
            <td>
                <asp:FileUpload ID="FU1" runat="server" />
            </td>
            <td>
                <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="panCrop" runat="server" Style="width: 75%; height: 75%;"  Visible="false">
        <table>
            <tr>
                <td>
                    <asp:Image ID="imgUpload" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnCrop" runat="server" Text="Crop & Save" OnClick="btnCrop_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <%-- Hidden field for store cror area --%>
                    <asp:HiddenField ID="X" runat="server" />
                    <asp:HiddenField ID="Y" runat="server" />
                    <asp:HiddenField ID="W" runat="server" />
                    <asp:HiddenField ID="H" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
