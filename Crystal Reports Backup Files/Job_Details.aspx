﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HR_Module/HRModule.master" AutoEventWireup="true"
    CodeBehind="Job_Details.aspx.cs" Inherits="AdeptHRManager.HR_Module.Job_Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="ConfirmMessageBox" Src="~/HRManagerClasses/HRManagerUserControls/HRManager_Confirmation_Message_Box.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StaffModuleHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StaffModuleMainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelJobDetails" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelJobDetails" runat="server">
                <asp:Panel ID="PanelStaffModuleHeader" class="child-content-header" runat="server">
                    <table>
                        <tr align="top">
                            <td>
                                <asp:Image ID="ImageStaffPhoto" CssClass="staff-image" ImageUrl="~/images/background/person.png"
                                    runat="server" />
                                <asp:HiddenField ID="HiddenFieldStaffID" runat="server" />
                            </td>
                            <td>
                                <fieldset width="100%">
                                    <legend>Search Staff By:</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffName" OnTextChanged="txtSearchByStaffName_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffName_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffName" WatermarkText="Search by staff name:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffName_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffName" TargetControlID="txtSearchByStaffName" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffNameDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffNameDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                National ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByIDNumber" OnTextChanged="txtSearchByIDNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSrachByIDNumber_TextBoxWatermarkExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtSearchByIDNumber" WatermarkText="Search by id number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByIDNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByIDNumber" TargetControlID="txtSearchByIDNumber" CompletionInterval="0"
                                                    CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByIDNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByIDNumberDisplay" style="max-height: 200px; display: none; overflow: auto;
                                                    width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Reference:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByStaffReference" OnTextChanged="txtSearchByStaffReference_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByStaffReference_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByStaffReference" WatermarkText="Search by staff reference:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByStaffReference_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByStaffReference" TargetControlID="txtSearchByStaffReference"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByStaffReferenceDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByStaffReferenceDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                            <td>
                                                Mobile Number:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSearchByMobileNumber" OnTextChanged="txtSearchByMobileNumber_TextChanged"
                                                    AutoPostBack="true" runat="server"></asp:TextBox>
                                                <asp:TextBoxWatermarkExtender ID="txtSearchByMobileNumber_TextBoxWatermarkExtender"
                                                    runat="server" Enabled="True" TargetControlID="txtSearchByMobileNumber" WatermarkText="Search by mobile number:"
                                                    WatermarkCssClass="water-mark-text-extender"></asp:TextBoxWatermarkExtender>
                                                <asp:AutoCompleteExtender ID="txtSearchByMobileNumber_AutoCompleteExtender" runat="server"
                                                    DelimiterCharacters="" Enabled="True" ServicePath="~/HRManagerClasses/HRManagerService.asmx"
                                                    ServiceMethod="GetStaffByMobileNumber" TargetControlID="txtSearchByMobileNumber"
                                                    CompletionInterval="0" CompletionSetCount="300" MinimumPrefixLength="1" CompletionListCssClass="auto-extender"
                                                    CompletionListItemCssClass="auto-extender-list" CompletionListHighlightedItemCssClass="auto-extender-highlight"
                                                    CompletionListElementID="divSearchByMobileNumberDisplay">
                                                </asp:AutoCompleteExtender>
                                                <div id="divSearchByMobileNumberDisplay" style="max-height: 200px; display: none;
                                                    overflow: auto; width: 293px !important;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="panel-details">
                    <asp:Panel ID="PanelAddJobDetails" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    Joining Date:<span class="errorMessage">*</span>
                                </td>
                                <td style="vertical-align: bottom;">
                                    <asp:TextBox ID="txtJoinDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonJoinDate" ToolTip="Pick join date" CssClass="date-image"
                                        ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtJoinDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtJoinDate" PopupButtonID="ImageButtonJoinDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtJoinDate_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                        CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureName="en-GB" Enabled="True"
                                        Mask="99/LLL/9999" TargetControlID="txtJoinDate" UserDateFormat="DayMonthYear">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtJoinDate_MaskedEditValidator" runat="server" ControlExtender="txtJoinDate_MaskedEditExtender"
                                        ControlToValidate="txtJoinDate" CssClass="errorMessage" ErrorMessage="txtJoinDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid join date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                    <br />
                                    <asp:Label ID="Label166" runat="server" CssClass="small-info-message" Text="Tip: Date Format: 'dd/MMM/yyyy' e.g. 01/Jan/2015"></asp:Label>
                                </td>
                                <td>
                                    Job Title:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobTitle" runat="server">
                                        <asp:ListItem Value="0">-Select Job TItle-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Grade: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlJobGrade" runat="server">
                                        <asp:ListItem Value="0">-Select Job Grade-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Department: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDepartment" runat="server">
                                        <asp:ListItem Value="0">-Select Department-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Geographical Location:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLocationPosted" runat="server">
                                        <asp:ListItem Value="0">-Select Location Posted-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Duty Station/ Office:<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPostedAt" runat="server">
                                        <asp:ListItem Value="0">-Select Posted At-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contract Type: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlContractType" runat="server">
                                        <asp:ListItem Value="0">-Select Contract Type-</asp:ListItem>
                                        <asp:ListItem Value="1">Full-Time</asp:ListItem>
                                        <asp:ListItem Value="2">Contract</asp:ListItem>
                                        <asp:ListItem Value="3">Temporary</asp:ListItem>
                                        <asp:ListItem Value="">Intern</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Contract Start Date: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContractStartDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonContractStartDate" ToolTip="Pick contract start date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtContractStartDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtContractStartDate" PopupButtonID="ImageButtonContractStartDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtContractStartDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtContractStartDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtContractStartDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtContractStartDate_MaskedEditExtender" ControlToValidate="txtContractStartDate"
                                        CssClass="errorMessage" ErrorMessage="txtContractStartDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid contract start date" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contract End Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContractEndDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonContractEndDate" ToolTip="Pick contract end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtContractEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtContractEndDate" PopupButtonID="ImageButtonContractEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtContractEndDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtContractEndDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtContractEndDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtContractEndDate_MaskedEditExtender" ControlToValidate="txtContractEndDate"
                                        CssClass="errorMessage" ErrorMessage="txtContractEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid contract end date" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                                <td>
                                    Type of Work:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTypeOfWork" runat="server">
                                        <asp:ListItem Value="0">-Select Type of Work-</asp:ListItem>
                                        <asp:ListItem Value="1">Operational</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Job Category: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <%--  <asp:DropDownList ID="ddlJobCategory" runat="server">
                                    <asp:ListItem Value="0">-Select Job Category-</asp:ListItem>
                                    <asp:ListItem Value="1">General</asp:ListItem>
                                    <asp:ListItem Value="2">Professional</asp:ListItem>
                                </asp:DropDownList>--%>
                                    <asp:RadioButtonList ID="rblProfessional" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Value="False">General Support</asp:ListItem>
                                        <asp:ListItem Value="True">Professional</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Supervisor: <span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlReportsTo" runat="server">
                                        <asp:ListItem Value="0">-Select Reports To-</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Under Probition?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblStaffUnderProbation" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Value="True">Yes</asp:ListItem>
                                        <asp:ListItem Value="False">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    Probation End Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProbationEndDate" Width="200px" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButtonProbationEndDate" ToolTip="Pick probation end date"
                                        CssClass="date-image" ImageUrl="~/images/icons/medium/date.png" runat="server" />
                                    <asp:CalendarExtender ID="txtProbationEndDate_CalendarExtender" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtProbationEndDate" PopupButtonID="ImageButtonProbationEndDate"
                                        PopupPosition="TopRight"></asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="txtProbationEndDate_MaskedEditExtender" runat="server"
                                        ClearMaskOnLostFocus="false" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                        CultureName="en-GB" Enabled="True" Mask="99/LLL/9999" TargetControlID="txtProbationEndDate"
                                        UserDateFormat="DayMonthYear"></asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="txtProbationEndDate_MaskedEditValidator" runat="server"
                                        ControlExtender="txtProbationEndDate_MaskedEditExtender" ControlToValidate="txtProbationEndDate"
                                        CssClass="errorMessage" ErrorMessage="txtProbationEndDate_MaskedEditValidator"
                                        InvalidValueMessage="<br>Invalid probation end date entered" Display="Dynamic">
                                    </asp:MaskedEditValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Badge Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBadgeNumber" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    Local / Expat?<span class="errorMessage">*</span>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblInternationalOrNational" RepeatDirection="Horizontal"
                                        runat="server">
                                        <asp:ListItem Value="True">National</asp:ListItem>
                                        <asp:ListItem Value="False">International</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lbRequired" runat="server" CssClass="errorMessage" Text="Fields marked with asterisk(*) are required."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="linkBtn" style="text-align: right;">
                                        <asp:LinkButton ID="lnkBtnSaveJobDetails" OnClick="lnkBtnSaveJobDetails_Click" ToolTip="Save job details"
                                            runat="server">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons/Small/Save.png" ImageAlign="AbsMiddle" />
                                            Save Job Details</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
